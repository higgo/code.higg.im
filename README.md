## Experiments / Lab / Playground / Sandbox / Failed Projects

### Random scraps of coding I've done

Just a little dumping ground for projects which never really took off. They were briefly popular and then quickly forgotten about. If I have the time, I would like to revive some of them and give them a new lease of life. Until then, anybody is free to poke around and learn from them.

- http://code.higg.im/
- http://code.higg.im/adn/
- http://code.higg.im/asciimation/
- http://code.higg.im/chromeless/
- http://code.higg.im/favorites/
- http://code.higg.im/mjex/
- http://code.higg.im/tweets/
- http://code.higg.im/twitter.cleaner/
- http://open.higg.im/pintools/
- http://open.higg.im/http/
- http://open.higg.im/files/stream/


![...](http://f.cl.ly/items/0E121z1x212X16091Y1k/s.png "...")

[![...](http://f.cl.ly/items/42161a2n23111o2F2O0t/tip.png "Fund me on Gittip")](https://www.gittip.com/SoHiggo/)


###[Donate - gittip.com/SoHiggo](https://www.gittip.com/SoHiggo/)

- [higg.im](http://www.higg.im/)
- [higg.tel](http://higg.tel/)
- [public@higg.im](mailto:public@higg.im)
