function arrToRegExp(arr){

var str = '';

for(i=0;i<arr.length;i++){
str += arr[i]+'|'
}

str = str.substring(0,str.length-1)
str = new RegExp(''+str+'','ig')

return str

}

var keywords = arrToRegExp(keywordsArr)
var domains = arrToRegExp(domainsArr)
var months = /january|february|march|april|may|june|july|august|september|october|november|december/ig

function postProcess(val){


val = val.replace(/\. http:\/\/|\. https:\/\//ig,' http://');
val = val.replace(/ - /ig,' ― ');


return val

}

function purgeLine(val){

hazLink = val.match(/http:\/\/|https:\/\//ig) ? true:false
hazKeyword = val.match(keywords) ? true:false
hazDomain = val.match(domains) ? true:false
hazMonth = val.match(months) ? true:false
allASCII = /^[\000-\177]*$/.test(val)


// DEBUG SIZE

if(hazLink && allASCII && !hazKeyword && !hazDomain && !hazMonth)
$('#output').val(postProcess($('#output').val()+val+'\n'))


}

/*
1.) Remove apostrophes - '
2.) Replace smart quotes with normal “ - "
3.) Replace smart quotes with normal ” - "
4.) Replace `&amp;` with &
5.) Replace `&lt;` with <
6.) Replace `&gt;` with >
7.) Replace ’ with '
8/) Replace … with ...
9.) Remove double spaces!
*/

function cleanupLine(line){

line = line.replace(/“/ig,'"')
line = line.replace(/”/ig,'"')
line = line.replace(/&amp;/ig,'&')
line = line.replace(/&lt;/ig,'<')
line = line.replace(/&gt;/ig,'>')
line = line.replace(/’/ig,"'")
line = line.replace(/…/ig,"...")


var hashtag = new RegExp('#([^\\s]*)','g');
line = line.replace(hashtag, '');

var trimmed = S(line).collapseWhitespace().s;

return trimmed;

}


function processText(){

input = $('#input').val()
splitLines = input.split('\n');

for(a=0;a<splitLines.length;a++){

purgeLine(cleanupLine(splitLines[a]))

}

}

$(function() {

$('#processButton').click(function(){

processText();

})

});

