Grailbird.data.tweets_2013_03 = 
 [ {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Spotify",
      "screen_name" : "Spotify",
      "indices" : [ 130, 138 ],
      "id_str" : "17230018",
      "id" : 17230018
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 125 ],
      "url" : "http://t.co/g0TcK8iBwb",
      "expanded_url" : "http://isharefil.es/NyK8",
      "display_url" : "isharefil.es/NyK8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "318481857777913857",
  "text" : "\"Try Spotify Premium for 30 days!\" ... \"\nAlmost there... Please select a payment method\" Kinda silly?! http://t.co/g0TcK8iBwb cc/ @spotify",
  "id" : 318481857777913857,
  "created_at" : "Sun Mar 31 21:56:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Padolsey",
      "screen_name" : "padolsey",
      "indices" : [ 0, 9 ],
      "id_str" : "17107025",
      "id" : 17107025
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "318467574402797568",
  "in_reply_to_user_id" : 17107025,
  "text" : "@padolsey - For the sake of posterity, the charset tag in your throbber generator should be &lt;meta&gt;, not &lt;mata&gt;",
  "id" : 318467574402797568,
  "created_at" : "Sun Mar 31 20:59:21 +0000 2013",
  "in_reply_to_screen_name" : "padolsey",
  "in_reply_to_user_id_str" : "17107025",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http://t.co/IncliVBRAX",
      "expanded_url" : "http://myshar.es/114EGu0",
      "display_url" : "myshar.es/114EGu0"
    } ]
  },
  "geo" : {
  },
  "id_str" : "318093452007059457",
  "text" : "\"All major (social) networks have a URL you can pass stuff to,\" http://t.co/IncliVBRAX",
  "id" : 318093452007059457,
  "created_at" : "Sat Mar 30 20:12:43 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "318087431708631042",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog Any 'password generator' tools I would be suspicious of because they attach you IP to a password and then you're doomed.",
  "id" : 318087431708631042,
  "created_at" : "Sat Mar 30 19:48:48 +0000 2013",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http://t.co/cn9VydLXg2",
      "expanded_url" : "http://davidwalsh.name/web-development-tools",
      "display_url" : "davidwalsh.name/web-developmen\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "318087083916947456",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog These kind of tools annoy me because now I have to audit the code to see you're not logging p/w's http://t.co/cn9VydLXg2",
  "id" : 318087083916947456,
  "created_at" : "Sat Mar 30 19:47:25 +0000 2013",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u260046\u263E Jorge D.",
      "screen_name" : "doktorerossi",
      "indices" : [ 3, 16 ],
      "id_str" : "134889077",
      "id" : 134889077
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 29, 36 ]
    }, {
      "text" : "plugins",
      "indices" : [ 37, 45 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http://t.co/J2UNZxyTcz",
      "expanded_url" : "http://iwantaneff.in/repo/",
      "display_url" : "iwantaneff.in/repo/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "318079295501242368",
  "text" : "RT @doktorerossi: Collection #jquery #plugins http://t.co/J2UNZxyTcz",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "jquery",
        "indices" : [ 11, 18 ]
      }, {
        "text" : "plugins",
        "indices" : [ 19, 27 ]
      } ],
      "urls" : [ {
        "indices" : [ 28, 50 ],
        "url" : "http://t.co/J2UNZxyTcz",
        "expanded_url" : "http://iwantaneff.in/repo/",
        "display_url" : "iwantaneff.in/repo/"
      } ]
    },
    "geo" : {
    },
    "id_str" : "317039529313841153",
    "text" : "Collection #jquery #plugins http://t.co/J2UNZxyTcz",
    "id" : 317039529313841153,
    "created_at" : "Wed Mar 27 22:24:48 +0000 2013",
    "user" : {
      "name" : "\u260046\u263E Jorge D.",
      "screen_name" : "doktorerossi",
      "protected" : false,
      "id_str" : "134889077",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000034896366/469a78169eb2c5c18c41a44e388a38a6_normal.png",
      "id" : 134889077,
      "verified" : false
    }
  },
  "id" : 318079295501242368,
  "created_at" : "Sat Mar 30 19:16:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 47 ],
      "url" : "http://t.co/VQ9a25Lvqr",
      "expanded_url" : "http://panzi.github.com/SocialSharePrivacy/",
      "display_url" : "panzi.github.com/SocialSharePri\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "318059294530609152",
  "text" : "My lord this is awesome: http://t.co/VQ9a25Lvqr",
  "id" : 318059294530609152,
  "created_at" : "Sat Mar 30 17:56:59 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Young",
      "screen_name" : "rcyou",
      "indices" : [ 3, 9 ],
      "id_str" : "26770448",
      "id" : 26770448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "317938619966963712",
  "text" : "RT @rcyou: Whoever said Lobster is the new Comic Sans was right. Can we please stop the constant misuse?",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "317846009600434177",
    "text" : "Whoever said Lobster is the new Comic Sans was right. Can we please stop the constant misuse?",
    "id" : 317846009600434177,
    "created_at" : "Sat Mar 30 03:49:28 +0000 2013",
    "user" : {
      "name" : "Ryan Young",
      "screen_name" : "rcyou",
      "protected" : false,
      "id_str" : "26770448",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3452145818/9716ef4fa74e933b1bfb147d5a84bb89_normal.jpeg",
      "id" : 26770448,
      "verified" : false
    }
  },
  "id" : 317938619966963712,
  "created_at" : "Sat Mar 30 09:57:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "skuda technologies",
      "screen_name" : "skudatech",
      "indices" : [ 0, 10 ],
      "id_str" : "549551317",
      "id" : 549551317
    }, {
      "name" : "Felix Turner",
      "screen_name" : "felixturner",
      "indices" : [ 11, 23 ],
      "id_str" : "38215107",
      "id" : 38215107
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 120 ],
      "url" : "http://t.co/3BpGCD4OLi",
      "expanded_url" : "http://higg.im/twitter/",
      "display_url" : "higg.im/twitter/"
    } ]
  },
  "in_reply_to_status_id_str" : "317870176504803328",
  "geo" : {
  },
  "id_str" : "317907313153425409",
  "in_reply_to_user_id" : 549551317,
  "text" : "@skudatech @felixturner Great for when you reach the 3200 mark on Twitter. No tweet death for me! http://t.co/3BpGCD4OLi",
  "id" : 317907313153425409,
  "in_reply_to_status_id" : 317870176504803328,
  "created_at" : "Sat Mar 30 07:53:04 +0000 2013",
  "in_reply_to_screen_name" : "skudatech",
  "in_reply_to_user_id_str" : "549551317",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Kammerer",
      "screen_name" : "mkammerer",
      "indices" : [ 0, 10 ],
      "id_str" : "13226232",
      "id" : 13226232
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 69 ],
      "url" : "http://t.co/GBYcabsJXA",
      "expanded_url" : "http://unicod.es/",
      "display_url" : "unicod.es"
    } ]
  },
  "in_reply_to_status_id_str" : "317806623861133312",
  "geo" : {
  },
  "id_str" : "317807960501940224",
  "in_reply_to_user_id" : 13226232,
  "text" : "@mkammerer You can have even more fun on this: http://t.co/GBYcabsJXA",
  "id" : 317807960501940224,
  "in_reply_to_status_id" : 317806623861133312,
  "created_at" : "Sat Mar 30 01:18:17 +0000 2013",
  "in_reply_to_screen_name" : "mkammerer",
  "in_reply_to_user_id_str" : "13226232",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Kammerer",
      "screen_name" : "mkammerer",
      "indices" : [ 0, 10 ],
      "id_str" : "13226232",
      "id" : 13226232
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "317806623861133312",
  "geo" : {
  },
  "id_str" : "317807859691835392",
  "in_reply_to_user_id" : 13226232,
  "text" : "@mkammerer He striketh!",
  "id" : 317807859691835392,
  "in_reply_to_status_id" : 317806623861133312,
  "created_at" : "Sat Mar 30 01:17:52 +0000 2013",
  "in_reply_to_screen_name" : "mkammerer",
  "in_reply_to_user_id_str" : "13226232",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hunie",
      "screen_name" : "hunieco",
      "indices" : [ 84, 92 ],
      "id_str" : "17333679",
      "id" : 17333679
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http://t.co/yg1xOjuFOO",
      "expanded_url" : "http://myshar.es/15FMbql",
      "display_url" : "myshar.es/15FMbql"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317807785612034048",
  "text" : "Combining icon font glyphs to create complex mega-icons. http://t.co/yg1xOjuFOO via @hunieco",
  "id" : 317807785612034048,
  "created_at" : "Sat Mar 30 01:17:35 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Larry Hynes",
      "screen_name" : "larry_hynes",
      "indices" : [ 0, 12 ],
      "id_str" : "578272794",
      "id" : 578272794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "317801473239040000",
  "in_reply_to_user_id" : 578272794,
  "text" : "@larry_hynes Just DM'd you",
  "id" : 317801473239040000,
  "created_at" : "Sat Mar 30 00:52:30 +0000 2013",
  "in_reply_to_screen_name" : "larry_hynes",
  "in_reply_to_user_id_str" : "578272794",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/VqJEzcdX7E",
      "expanded_url" : "http://cssdb.co/",
      "display_url" : "cssdb.co"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317798993155461121",
  "text" : "http://t.co/VqJEzcdX7E",
  "id" : 317798993155461121,
  "created_at" : "Sat Mar 30 00:42:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http://t.co/NpHySYAp1K",
      "expanded_url" : "http://higg.im/sites/",
      "display_url" : "higg.im/sites/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317785302825566208",
  "text" : "Hey. These are all the projects I worked on:\n\nhttp://t.co/NpHySYAp1K",
  "id" : 317785302825566208,
  "created_at" : "Fri Mar 29 23:48:15 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "317772509497942018",
  "text" : "Code blogs annoy me because the authors always come across as ninjas. If I codeblogged since 2001 I would be an \u1E7Bbermensch in the industry.",
  "id" : 317772509497942018,
  "created_at" : "Fri Mar 29 22:57:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "317741459925250049",
  "geo" : {
  },
  "id_str" : "317750695753293824",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar postmaster@nameofsite.com works too if the server runs cPanel. cPanel has `postmaster` as a catch-all account for E-Mails.",
  "id" : 317750695753293824,
  "in_reply_to_status_id" : 317741459925250049,
  "created_at" : "Fri Mar 29 21:30:44 +0000 2013",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chrome Experiments",
      "screen_name" : "ChromeExp",
      "indices" : [ 0, 10 ],
      "id_str" : "163975783",
      "id" : 163975783
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 103 ],
      "url" : "http://t.co/JPCXe8PYdr",
      "expanded_url" : "http://www.chromeexperiments.com/detail/asciimations/",
      "display_url" : "chromeexperiments.com/detail/asciima\u2026"
    }, {
      "indices" : [ 115, 137 ],
      "url" : "http://t.co/DsuGdNpE3i",
      "expanded_url" : "http://iwantaneff.in/projects/asciimation/",
      "display_url" : "iwantaneff.in/projects/ascii\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317741116868939776",
  "in_reply_to_user_id" : 163975783,
  "text" : "@chromeexp Hi there. Can you update the URL on this experiment to the new one... http://t.co/JPCXe8PYdr \u2026 New URL: http://t.co/DsuGdNpE3i",
  "id" : 317741116868939776,
  "created_at" : "Fri Mar 29 20:52:40 +0000 2013",
  "in_reply_to_screen_name" : "ChromeExp",
  "in_reply_to_user_id_str" : "163975783",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chrome Experiments",
      "screen_name" : "ChromeExp",
      "indices" : [ 0, 10 ],
      "id_str" : "163975783",
      "id" : 163975783
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "317736964906438657",
  "in_reply_to_user_id" : 163975783,
  "text" : "@ChromeExp I resubmitted the Asciimation demo. Please update your records! And if possible do a redirect from the old submission to the new",
  "id" : 317736964906438657,
  "created_at" : "Fri Mar 29 20:36:10 +0000 2013",
  "in_reply_to_screen_name" : "ChromeExp",
  "in_reply_to_user_id_str" : "163975783",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 125, 135 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 120 ],
      "url" : "http://t.co/NpHySYAp1K",
      "expanded_url" : "http://higg.im/sites/",
      "display_url" : "higg.im/sites/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317718490473713664",
  "text" : "Made something real quick to showcase a real-time always updated list of everything I work(ed) on http://t.co/NpHySYAp1K cc/ @simevidas",
  "id" : 317718490473713664,
  "created_at" : "Fri Mar 29 19:22:45 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 111 ],
      "url" : "http://t.co/w3LQZnWADD",
      "expanded_url" : "http://myshar.es/164GGSi",
      "display_url" : "myshar.es/164GGSi"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317695943694753792",
  "text" : "\"We all like to shout from the rooftops when we release a piece of open source software\" http://t.co/w3LQZnWADD",
  "id" : 317695943694753792,
  "created_at" : "Fri Mar 29 17:53:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 27 ],
      "url" : "http://t.co/3BpGCD4OLi",
      "expanded_url" : "http://higg.im/twitter/",
      "display_url" : "higg.im/twitter/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317672730424135680",
  "text" : "this http://t.co/3BpGCD4OLi",
  "id" : 317672730424135680,
  "created_at" : "Fri Mar 29 16:20:55 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "317378730425204736",
  "text" : "&lt;img src=\"/\" =_=\" title=\"onerror='prompt(1)'\"&gt; \u3010\u30C4\u3011",
  "id" : 317378730425204736,
  "created_at" : "Thu Mar 28 20:52:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/317339855728279553/photo/1",
      "indices" : [ 110, 132 ],
      "url" : "http://t.co/HtX53Vr7V5",
      "media_url" : "http://pbs.twimg.com/media/BGdq6t6CAAAjoZe.jpg",
      "id_str" : "317339855740862464",
      "id" : 317339855740862464,
      "media_url_https" : "https://pbs.twimg.com/media/BGdq6t6CAAAjoZe.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 449,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 255,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 549,
        "resize" : "fit",
        "w" : 733
      }, {
        "h" : 549,
        "resize" : "fit",
        "w" : 733
      } ],
      "display_url" : "pic.twitter.com/HtX53Vr7V5"
    } ],
    "hashtags" : [ {
      "text" : "testtwf",
      "indices" : [ 101, 109 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "317339855728279553",
  "text" : "Responsive design is not enough. Your site will break on the myriad of devices that exist out there. #testtwf http://t.co/HtX53Vr7V5",
  "id" : 317339855728279553,
  "created_at" : "Thu Mar 28 18:18:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CSS",
      "indices" : [ 0, 4 ]
    } ],
    "urls" : [ {
      "indices" : [ 7, 29 ],
      "url" : "http://t.co/Oa9bAS0UVE",
      "expanded_url" : "http://i.imgur.com/Q3cUg29.gif",
      "display_url" : "i.imgur.com/Q3cUg29.gif"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317332840075386881",
  "text" : "#CSS \u2192 http://t.co/Oa9bAS0UVE Warning, phat GIF coming up.",
  "id" : 317332840075386881,
  "created_at" : "Thu Mar 28 17:50:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 89 ],
      "url" : "http://t.co/PucnpmXuqX",
      "expanded_url" : "http://myshar.es/14yO3p9",
      "display_url" : "myshar.es/14yO3p9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317331534925418496",
  "text" : "Maxmertkit - \"Css framework based on widget-modifier coding style\" http://t.co/PucnpmXuqX",
  "id" : 317331534925418496,
  "created_at" : "Thu Mar 28 17:45:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 140 ],
      "url" : "http://t.co/98pokHBiul",
      "expanded_url" : "http://bit.ly/11SLOsp",
      "display_url" : "bit.ly/11SLOsp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317330655077875712",
  "text" : "PeerCDN uses WebRTC to serve a site's static resources over a peer-to-peer network of visitors currently on the site. http://t.co/98pokHBiul",
  "id" : 317330655077875712,
  "created_at" : "Thu Mar 28 17:41:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://flickr.com/services/twitter/\" rel=\"nofollow\">Flickr</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 44 ],
      "url" : "http://t.co/Z8mk8ZaQVe",
      "expanded_url" : "http://flic.kr/p/e6NQCd",
      "display_url" : "flic.kr/p/e6NQCd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317309222222184448",
  "text" : "\"Virtual Supermarket\" http://t.co/Z8mk8ZaQVe I prefer to touch and feel the product?!",
  "id" : 317309222222184448,
  "created_at" : "Thu Mar 28 16:16:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://flickr.com/services/twitter/\" rel=\"nofollow\">Flickr</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 90 ],
      "url" : "http://t.co/hmN0DmKrsv",
      "expanded_url" : "http://flic.kr/p/e6NQud",
      "display_url" : "flic.kr/p/e6NQud"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317309008635625474",
  "text" : "Does anybody ever use this kind of screen setup? Coding like a baws http://t.co/hmN0DmKrsv",
  "id" : 317309008635625474,
  "created_at" : "Thu Mar 28 16:15:37 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://flickr.com/services/twitter/\" rel=\"nofollow\">Flickr</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lea Verou",
      "screen_name" : "LeaVerou",
      "indices" : [ 60, 69 ],
      "id_str" : "22199970",
      "id" : 22199970
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 49 ],
      "url" : "http://t.co/3orwRyDPQx",
      "expanded_url" : "http://flic.kr/p/e6LoTj",
      "display_url" : "flic.kr/p/e6LoTj"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317306923076702209",
  "text" : "\"Or, as they say in Maths\" http://t.co/3orwRyDPQx \u2665 it. via @LeaVerou",
  "id" : 317306923076702209,
  "created_at" : "Thu Mar 28 16:07:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 36 ],
      "url" : "https://t.co/rloTYeVUnS",
      "expanded_url" : "https://github.com/damirfoy/iCheck",
      "display_url" : "github.com/damirfoy/iCheck"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317253335109468160",
  "text" : "Lickable \uD83C\uDF6D \n\nhttps://t.co/rloTYeVUnS",
  "id" : 317253335109468160,
  "created_at" : "Thu Mar 28 12:34:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Lewis",
      "screen_name" : "aerotwist",
      "indices" : [ 0, 10 ],
      "id_str" : "42614508",
      "id" : 42614508
    }, {
      "name" : "romancortes",
      "screen_name" : "romancortes",
      "indices" : [ 11, 23 ],
      "id_str" : "14755750",
      "id" : 14755750
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 99 ],
      "url" : "http://t.co/I3lSIEOuDZ",
      "expanded_url" : "http://js1k.com/2013-spring/demo/1337",
      "display_url" : "js1k.com/2013-spring/de\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "317218916642480128",
  "geo" : {
  },
  "id_str" : "317220341317521408",
  "in_reply_to_user_id" : 42614508,
  "text" : "@aerotwist @romancortes Yeah. That one is mind blowing! I submitted one too! http://t.co/I3lSIEOuDZ",
  "id" : 317220341317521408,
  "in_reply_to_status_id" : 317218916642480128,
  "created_at" : "Thu Mar 28 10:23:17 +0000 2013",
  "in_reply_to_screen_name" : "aerotwist",
  "in_reply_to_user_id_str" : "42614508",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "317201607907479552",
  "text" : "The S3 hack is not big news. I've been plundering things from public S3 buckets since AWS first launched.",
  "id" : 317201607907479552,
  "created_at" : "Thu Mar 28 09:08:51 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Thrasher",
      "screen_name" : "thrashr888",
      "indices" : [ 3, 14 ],
      "id_str" : "1173",
      "id" : 1173
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 38 ],
      "url" : "http://t.co/tix5y2aIeT",
      "expanded_url" : "http://peercdn.com/",
      "display_url" : "peercdn.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317187306740125698",
  "text" : "RT @thrashr888: http://t.co/tix5y2aIeT\u00A0is kinda brilliant. I think we can also solve map-reduce type problems w/ the same concept.",
  "id" : 317187306740125698,
  "created_at" : "Thu Mar 28 08:12:01 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hakim El Hattab",
      "screen_name" : "hakimel",
      "indices" : [ 0, 8 ],
      "id_str" : "73339662",
      "id" : 73339662
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 81 ],
      "url" : "http://t.co/ioNnyHUamv",
      "expanded_url" : "http://myshar.es/15Wea5t",
      "display_url" : "myshar.es/15Wea5t"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317045178613522432",
  "in_reply_to_user_id" : 73339662,
  "text" : "@hakimel Just discovered my last name .com was available \u2192 http://t.co/ioNnyHUamv You should try a .IM equivalent. Like hak\u2026",
  "id" : 317045178613522432,
  "created_at" : "Wed Mar 27 22:47:15 +0000 2013",
  "in_reply_to_screen_name" : "hakimel",
  "in_reply_to_user_id_str" : "73339662",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 45 ],
      "url" : "http://t.co/cPbjBy3nJK",
      "expanded_url" : "http://isharefil.es/NsXV",
      "display_url" : "isharefil.es/NsXV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "317043348252794880",
  "text" : "Let's get things done: http://t.co/cPbjBy3nJK",
  "id" : 317043348252794880,
  "created_at" : "Wed Mar 27 22:39:59 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 46 ],
      "url" : "http://t.co/dQqfTIDq2r",
      "expanded_url" : "http://dribbble.com/shots/961152-Github-Pirate-Octocat",
      "display_url" : "dribbble.com/shots/961152-G\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316996854774657024",
  "text" : "Github Pirate Octocat \u2192 http://t.co/dQqfTIDq2r",
  "id" : 316996854774657024,
  "created_at" : "Wed Mar 27 19:35:14 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dribbble",
      "screen_name" : "dribbble",
      "indices" : [ 57, 66 ],
      "id_str" : "14351575",
      "id" : 14351575
    }, {
      "name" : "Alex",
      "screen_name" : "amont_",
      "indices" : [ 110, 117 ],
      "id_str" : "764786257",
      "id" : 764786257
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 104 ],
      "url" : "http://t.co/iCMvxWcQb3",
      "expanded_url" : "http://drbl.in/hbGN",
      "display_url" : "drbl.in/hbGN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316994760994873344",
  "text" : "My favorite color is Hotpink. What's yours? If you're on @dribbble, do a rebound! http://t.co/iCMvxWcQb3  cc/ @amont_",
  "id" : 316994760994873344,
  "created_at" : "Wed Mar 27 19:26:55 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeffrey Short",
      "screen_name" : "Jeffrey_Short",
      "indices" : [ 0, 14 ],
      "id_str" : "90793987",
      "id" : 90793987
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https://t.co/ahlCeksC3J",
      "expanded_url" : "https://github.com/bebraw/jswiki/wiki/File-Formats",
      "display_url" : "github.com/bebraw/jswiki/\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "316910711018233856",
  "geo" : {
  },
  "id_str" : "316990975677431809",
  "in_reply_to_user_id" : 90793987,
  "text" : "@Jeffrey_Short It's not jQuery, but vanilla JS instead. There's more JS file format utils here: https://t.co/ahlCeksC3J",
  "id" : 316990975677431809,
  "in_reply_to_status_id" : 316910711018233856,
  "created_at" : "Wed Mar 27 19:11:52 +0000 2013",
  "in_reply_to_screen_name" : "Jeffrey_Short",
  "in_reply_to_user_id_str" : "90793987",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeffrey Short",
      "screen_name" : "Jeffrey_Short",
      "indices" : [ 0, 14 ],
      "id_str" : "90793987",
      "id" : 90793987
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https://t.co/ZnUwqX6Uiq",
      "expanded_url" : "https://github.com/stephen-hardy/DOCX.js",
      "display_url" : "github.com/stephen-hardy/\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "316910711018233856",
  "geo" : {
  },
  "id_str" : "316990653198381056",
  "in_reply_to_user_id" : 90793987,
  "text" : "@Jeffrey_Short There is this: https://t.co/ZnUwqX6Uiq",
  "id" : 316990653198381056,
  "in_reply_to_status_id" : 316910711018233856,
  "created_at" : "Wed Mar 27 19:10:35 +0000 2013",
  "in_reply_to_screen_name" : "Jeffrey_Short",
  "in_reply_to_user_id_str" : "90793987",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 44 ],
      "url" : "http://t.co/HDsEflQ0rB",
      "expanded_url" : "http://isharefil.es/NqJf",
      "display_url" : "isharefil.es/NqJf"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316988339997786112",
  "text" : "Wow Gistbox Looks Fab http://t.co/HDsEflQ0rB",
  "id" : 316988339997786112,
  "created_at" : "Wed Mar 27 19:01:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/60vlutqPyV",
      "expanded_url" : "http://uri.lv/",
      "display_url" : "uri.lv"
    } ]
  },
  "in_reply_to_status_id_str" : "316920522250285056",
  "geo" : {
  },
  "id_str" : "316971671615590400",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 I really hope a mass diaspora to http://t.co/60vlutqPyV occurs. Man I love that service.",
  "id" : 316971671615590400,
  "in_reply_to_status_id" : 316920522250285056,
  "created_at" : "Wed Mar 27 17:55:10 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 103 ],
      "url" : "http://t.co/EGMREndF4z",
      "expanded_url" : "http://isharefil.es/NqWq",
      "display_url" : "isharefil.es/NqWq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316732529010958336",
  "text" : "Okay so Google+ is pulling a Facebook and not letting me do mega posts like this http://t.co/EGMREndF4z G+ WHY U SO STRICT?!",
  "id" : 316732529010958336,
  "created_at" : "Wed Mar 27 02:04:54 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "souders",
      "screen_name" : "souders",
      "indices" : [ 3, 11 ],
      "id_str" : "15431118",
      "id" : 15431118
    }, {
      "name" : "DreamHost",
      "screen_name" : "DreamHost",
      "indices" : [ 41, 51 ],
      "id_str" : "14217022",
      "id" : 14217022
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "316729769771282434",
  "text" : "RT @souders: uhoh - not just my site but @dreamhost.com itself is down - can't even get to the status page. Sorry folks. Plz check back  ...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DreamHost",
        "screen_name" : "DreamHost",
        "indices" : [ 28, 38 ],
        "id_str" : "14217022",
        "id" : 14217022
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "316726479843123200",
    "text" : "uhoh - not just my site but @dreamhost.com itself is down - can't even get to the status page. Sorry folks. Plz check back later.",
    "id" : 316726479843123200,
    "created_at" : "Wed Mar 27 01:40:51 +0000 2013",
    "user" : {
      "name" : "souders",
      "screen_name" : "souders",
      "protected" : false,
      "id_str" : "15431118",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2029465935/4089930308-closeup-219x281_normal.jpg",
      "id" : 15431118,
      "verified" : false
    }
  },
  "id" : 316729769771282434,
  "created_at" : "Wed Mar 27 01:53:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "316723861016150016",
  "geo" : {
  },
  "id_str" : "316724491914985473",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert What does n1 mean? Perhaps you could bake this functionality into Rhetoricus so others can avail?",
  "id" : 316724491914985473,
  "in_reply_to_status_id" : 316723861016150016,
  "created_at" : "Wed Mar 27 01:32:57 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 39 ],
      "url" : "https://t.co/UuwcdYBEkj",
      "expanded_url" : "https://gist.github.com/higgo/5250819",
      "display_url" : "gist.github.com/higgo/5250819"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316723403660869632",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert https://t.co/UuwcdYBEkj",
  "id" : 316723403660869632,
  "created_at" : "Wed Mar 27 01:28:38 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http://t.co/BIRZEGWjM3",
      "expanded_url" : "http://myshar.es/15B7VDQ",
      "display_url" : "myshar.es/15B7VDQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316639081104826368",
  "text" : "Rye is a lightweight javascript library for DOM manipulation and events - http://t.co/BIRZEGWjM3",
  "id" : 316639081104826368,
  "created_at" : "Tue Mar 26 19:53:34 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 37 ],
      "url" : "http://t.co/GBYcabsJXA",
      "expanded_url" : "http://unicod.es/",
      "display_url" : "unicod.es"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316637457858834434",
  "text" : "\uFF28\uFF25\uFF2C\uFF2C\uFF2F \uFF29\uFF2E\uFF34\uFF25\uFF32\uFF2E\uFF25\uFF34 http://t.co/GBYcabsJXA",
  "id" : 316637457858834434,
  "created_at" : "Tue Mar 26 19:47:07 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brandon Satrom",
      "screen_name" : "BrandonSatrom",
      "indices" : [ 45, 59 ],
      "id_str" : "6708512",
      "id" : 6708512
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "JavaScript",
      "indices" : [ 19, 30 ]
    } ],
    "urls" : [ {
      "indices" : [ 60, 82 ],
      "url" : "http://t.co/z16IBPLaDu",
      "expanded_url" : "http://myshar.es/15B7RUz",
      "display_url" : "myshar.es/15B7RUz"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316611208285388800",
  "text" : "Secrets of Awesome #JavaScript API Design by @brandonsatrom http://t.co/z16IBPLaDu",
  "id" : 316611208285388800,
  "created_at" : "Tue Mar 26 18:02:49 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 82 ],
      "url" : "http://t.co/jf2pLJLY65",
      "expanded_url" : "http://iwantaneff.in/blog/all-posts/",
      "display_url" : "iwantaneff.in/blog/all-posts/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316610061554302976",
  "text" : "Here's a list of all the posts. Who needs Google Reader eh? http://t.co/jf2pLJLY65 Some crazy amount of work went into this.",
  "id" : 316610061554302976,
  "created_at" : "Tue Mar 26 17:58:15 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Irish",
      "screen_name" : "paul_irish",
      "indices" : [ 117, 128 ],
      "id_str" : "1671811",
      "id" : 1671811
    }, {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 129, 137 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http://t.co/EE7R4V01Vj",
      "expanded_url" : "http://iwantaneff.in/blog/",
      "display_url" : "iwantaneff.in/blog/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316609692493299712",
  "text" : "So here it is: Devnull. 4000 posts spanning back to 2004. http://t.co/EE7R4V01Vj All related to Web Development. cc/ @paul_irish @codepo8",
  "id" : 316609692493299712,
  "created_at" : "Tue Mar 26 17:56:47 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rich Clark",
      "screen_name" : "Rich_Clark",
      "indices" : [ 28, 39 ],
      "id_str" : "17558555",
      "id" : 17558555
    }, {
      "name" : "html5doctor",
      "screen_name" : "html5doctor",
      "indices" : [ 44, 56 ],
      "id_str" : "39765560",
      "id" : 39765560
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HTML5",
      "indices" : [ 0, 6 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http://t.co/OXpnsUMGSx",
      "expanded_url" : "http://myshar.es/14i26iP",
      "display_url" : "myshar.es/14i26iP"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316596922515337217",
  "text" : "#HTML5 forms input types by @Rich_Clark via @HTML5Doctor http://t.co/OXpnsUMGSx",
  "id" : 316596922515337217,
  "created_at" : "Tue Mar 26 17:06:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mashable",
      "screen_name" : "mashable",
      "indices" : [ 21, 30 ],
      "id_str" : "972651",
      "id" : 972651
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/JOhmdxBNIe",
      "expanded_url" : "http://mashable.com/robots.txt",
      "display_url" : "mashable.com/robots.txt"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316590441652953088",
  "text" : "Was poking around in @mashable's robots.txt: http://t.co/JOhmdxBNIe Only learning now you can put a `sitemap:` directive in it ;)",
  "id" : 316590441652953088,
  "created_at" : "Tue Mar 26 16:40:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JetBrains",
      "screen_name" : "jetbrains",
      "indices" : [ 69, 79 ],
      "id_str" : "14146389",
      "id" : 14146389
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 33, 44 ]
    } ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/t8pQzvVZMF",
      "expanded_url" : "http://myshar.es/15B7Pw2",
      "display_url" : "myshar.es/15B7Pw2"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316521202652950528",
  "text" : "TypeScript Support in WebStorm 6 #javascript http://t.co/t8pQzvVZMF +@jetbrains",
  "id" : 316521202652950528,
  "created_at" : "Tue Mar 26 12:05:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "html5rocks",
      "screen_name" : "html5rocks",
      "indices" : [ 66, 77 ],
      "id_str" : "155347903",
      "id" : 155347903
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 100 ],
      "url" : "http://t.co/dAf4QVjHgF",
      "expanded_url" : "http://myshar.es/15B7Lwk",
      "display_url" : "myshar.es/15B7Lwk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316473860105043969",
  "text" : "HTML's new template tag: standardizing client-side templating via @HTML5Rocks http://t.co/dAf4QVjHgF",
  "id" : 316473860105043969,
  "created_at" : "Tue Mar 26 08:57:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "316379884287045632",
  "geo" : {
  },
  "id_str" : "316381671450624000",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas I do indeed stay up at night! The best time to code ;) I sent you an E-Mail.",
  "id" : 316381671450624000,
  "in_reply_to_status_id" : 316379884287045632,
  "created_at" : "Tue Mar 26 02:50:43 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "unicode",
      "indices" : [ 75, 83 ]
    }, {
      "text" : "js",
      "indices" : [ 84, 87 ]
    } ],
    "urls" : [ {
      "indices" : [ 52, 74 ],
      "url" : "http://t.co/GBYcabsJXA",
      "expanded_url" : "http://unicod.es/",
      "display_url" : "unicod.es"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316368136414851072",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas Another one for your records - Unitools. http://t.co/GBYcabsJXA #unicode #js",
  "id" : 316368136414851072,
  "created_at" : "Tue Mar 26 01:56:56 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 128, 138 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 62 ],
      "url" : "http://t.co/Nb1985ASyl",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    }, {
      "indices" : [ 100, 123 ],
      "url" : "https://t.co/fb2BXqOQQN",
      "expanded_url" : "https://join.app.net/from/dh",
      "display_url" : "join.app.net/from/dh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316348496900931584",
  "text" : "I don't know what all the fuss is about http://t.co/Nb1985ASyl invites. Just sign up using this LOL https://t.co/fb2BXqOQQN cc/ @film_girl",
  "id" : 316348496900931584,
  "created_at" : "Tue Mar 26 00:38:53 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MG Siegler",
      "screen_name" : "parislemon",
      "indices" : [ 78, 89 ],
      "id_str" : "652193",
      "id" : 652193
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http://t.co/iN7imI4wvB",
      "expanded_url" : "http://myshar.es/15N6RNs",
      "display_url" : "myshar.es/15N6RNs"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316335429307731969",
  "text" : "IE11 to appear as Firefox to avoid legacy IE CSS http://t.co/iN7imI4wvB\u00A0\u2026 via @parislemon",
  "id" : 316335429307731969,
  "created_at" : "Mon Mar 25 23:46:58 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "316332301153873920",
  "text" : "Okay so reblogging is not scraping, it's content curation, and remixing the web. Exactly what RSS was intended for.",
  "id" : 316332301153873920,
  "created_at" : "Mon Mar 25 23:34:32 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "video",
      "indices" : [ 88, 94 ]
    } ],
    "urls" : [ {
      "indices" : [ 65, 87 ],
      "url" : "http://t.co/O8co89j8s8",
      "expanded_url" : "http://myshar.es/14qHCnY",
      "display_url" : "myshar.es/14qHCnY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316321149929791488",
  "text" : "This is a nice ADN iPhone app similar to Vine, but built for ADN http://t.co/O8co89j8s8 #video",
  "id" : 316321149929791488,
  "created_at" : "Mon Mar 25 22:50:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/316282447220334592/photo/1",
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/tfu1kaVLcW",
      "media_url" : "http://pbs.twimg.com/media/BGOpNcOCYAA1oTM.jpg",
      "id_str" : "316282447224528896",
      "id" : 316282447224528896,
      "media_url_https" : "https://pbs.twimg.com/media/BGOpNcOCYAA1oTM.jpg",
      "sizes" : [ {
        "h" : 683,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 227,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 683,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 600
      } ],
      "display_url" : "pic.twitter.com/tfu1kaVLcW"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "316282447220334592",
  "text" : "Now that's what I call responsive design: http://t.co/tfu1kaVLcW",
  "id" : 316282447220334592,
  "created_at" : "Mon Mar 25 20:16:26 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jim Cowart",
      "screen_name" : "ifandelse",
      "indices" : [ 44, 54 ],
      "id_str" : "57074451",
      "id" : 57074451
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 55, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 67, 89 ],
      "url" : "http://t.co/EXuU0wvTB5",
      "expanded_url" : "http://myshar.es/15B771M",
      "display_url" : "myshar.es/15B771M"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316276581406478336",
  "text" : "Cross Frame Messaging with postal.xframe by @ifandelse #javascript http://t.co/EXuU0wvTB5",
  "id" : 316276581406478336,
  "created_at" : "Mon Mar 25 19:53:07 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 3, 13 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 70 ],
      "url" : "http://t.co/IBJRvv8oVI",
      "expanded_url" : "http://webplatformdaily.org/links_txt/",
      "display_url" : "webplatformdaily.org/links_txt/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316269893085429760",
  "text" : "RT @simevidas: @_higg Here, clickable links :-) http://t.co/IBJRvv8oVI",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 33, 55 ],
        "url" : "http://t.co/IBJRvv8oVI",
        "expanded_url" : "http://webplatformdaily.org/links_txt/",
        "display_url" : "webplatformdaily.org/links_txt/"
      } ]
    },
    "in_reply_to_status_id_str" : "316187549070659584",
    "geo" : {
    },
    "id_str" : "316261059474358273",
    "in_reply_to_user_id" : 468853739,
    "text" : "@_higg Here, clickable links :-) http://t.co/IBJRvv8oVI",
    "id" : 316261059474358273,
    "in_reply_to_status_id" : 316187549070659584,
    "created_at" : "Mon Mar 25 18:51:27 +0000 2013",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "protected" : false,
      "id_str" : "175727560",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000065125916/326dca11327b0e650c17bb7f5722e393_normal.jpeg",
      "id" : 175727560,
      "verified" : false
    }
  },
  "id" : 316269893085429760,
  "created_at" : "Mon Mar 25 19:26:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "JavaScript",
      "indices" : [ 20, 31 ]
    } ],
    "urls" : [ {
      "indices" : [ 52, 74 ],
      "url" : "http://t.co/DiBW0nExMs",
      "expanded_url" : "http://myshar.es/15B6Yvd",
      "display_url" : "myshar.es/15B6Yvd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316248778329972736",
  "text" : "Douglas Crockford's #JavaScript Function Challenges http://t.co/DiBW0nExMs //A nice free 10 minute lab",
  "id" : 316248778329972736,
  "created_at" : "Mon Mar 25 18:02:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 46, 53 ]
    } ],
    "urls" : [ {
      "indices" : [ 54, 76 ],
      "url" : "http://t.co/MLminQZFFB",
      "expanded_url" : "http://myshar.es/15B6V2I",
      "display_url" : "myshar.es/15B6V2I"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316234542476296193",
  "text" : "10 Must-Have Chrome Extensions for Developers #webdev http://t.co/MLminQZFFB",
  "id" : 316234542476296193,
  "created_at" : "Mon Mar 25 17:06:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 105 ],
      "url" : "http://t.co/6xSue38Soc",
      "expanded_url" : "http://sweatingcommas.com/",
      "display_url" : "sweatingcommas.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316203033925144576",
  "text" : "Interesting Idea. Tempted to load up on Gin, write, and submit the article to him: http://t.co/6xSue38Soc",
  "id" : 316203033925144576,
  "created_at" : "Mon Mar 25 15:00:52 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Dave DeSandro",
      "screen_name" : "desandro",
      "indices" : [ 39, 48 ],
      "id_str" : "1584511",
      "id" : 1584511
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http://t.co/2vjeAlSQh6",
      "expanded_url" : "http://code.ovidiu.ch/dragdealer/",
      "display_url" : "code.ovidiu.ch/dragdealer/"
    } ]
  },
  "in_reply_to_status_id_str" : "316162071102369793",
  "geo" : {
  },
  "id_str" : "316188081885691905",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Also there is Dragdealer. cc/ @desandro http://t.co/2vjeAlSQh6",
  "id" : 316188081885691905,
  "in_reply_to_status_id" : 316162071102369793,
  "created_at" : "Mon Mar 25 14:01:27 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https://t.co/NBUEbKhZYt",
      "expanded_url" : "https://gist.github.com/higgo/4666848",
      "display_url" : "gist.github.com/higgo/4666848"
    } ]
  },
  "in_reply_to_status_id_str" : "316175103983489024",
  "geo" : {
  },
  "id_str" : "316187549070659584",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas Don't for get to include this in your next update ;) https://t.co/NBUEbKhZYt",
  "id" : 316187549070659584,
  "in_reply_to_status_id" : 316175103983489024,
  "created_at" : "Mon Mar 25 13:59:20 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 3, 13 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 85 ],
      "url" : "http://t.co/871tMnVawb",
      "expanded_url" : "http://webplatformdaily.org",
      "display_url" : "webplatformdaily.org"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316186825196720130",
  "text" : "RT @simevidas: I've moved my OWP daily digest to a new domain: http://t.co/871tMnVawb",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 48, 70 ],
        "url" : "http://t.co/871tMnVawb",
        "expanded_url" : "http://webplatformdaily.org",
        "display_url" : "webplatformdaily.org"
      } ]
    },
    "geo" : {
    },
    "id_str" : "316175103983489024",
    "text" : "I've moved my OWP daily digest to a new domain: http://t.co/871tMnVawb",
    "id" : 316175103983489024,
    "created_at" : "Mon Mar 25 13:09:53 +0000 2013",
    "user" : {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "protected" : false,
      "id_str" : "175727560",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000065125916/326dca11327b0e650c17bb7f5722e393_normal.jpeg",
      "id" : 175727560,
      "verified" : false
    }
  },
  "id" : 316186825196720130,
  "created_at" : "Mon Mar 25 13:56:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan Creamer",
      "screen_name" : "jcreamer898",
      "indices" : [ 50, 62 ],
      "id_str" : "52417305",
      "id" : 52417305
    }, {
      "name" : "Nettuts+",
      "screen_name" : "nettuts",
      "indices" : [ 67, 75 ],
      "id_str" : "14490962",
      "id" : 14490962
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 76, 87 ]
    } ],
    "urls" : [ {
      "indices" : [ 88, 110 ],
      "url" : "http://t.co/VFGE4VyVXb",
      "expanded_url" : "http://myshar.es/15vL38J",
      "display_url" : "myshar.es/15vL38J"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316158841396002816",
  "text" : "Event Based Programming: What Async Has Over Sync @jcreamer898 via @nettuts #javascript http://t.co/VFGE4VyVXb",
  "id" : 316158841396002816,
  "created_at" : "Mon Mar 25 12:05:16 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rob Larsen",
      "screen_name" : "robreact",
      "indices" : [ 39, 48 ],
      "id_str" : "7579292",
      "id" : 7579292
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 49, 56 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http://t.co/JNw5fFIB3h",
      "expanded_url" : "http://myshar.es/15vKBr8",
      "display_url" : "myshar.es/15vKBr8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316111509627482113",
  "text" : "H5BP Ant Build Script v1.0 Released by @robreact #webdev http://t.co/JNw5fFIB3h",
  "id" : 316111509627482113,
  "created_at" : "Mon Mar 25 08:57:11 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex",
      "screen_name" : "amont_",
      "indices" : [ 0, 7 ],
      "id_str" : "764786257",
      "id" : 764786257
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http://t.co/OVOqNq1sEZ",
      "expanded_url" : "http://APP.NET",
      "display_url" : "APP.NET"
    } ]
  },
  "in_reply_to_status_id_str" : "316010694262800386",
  "geo" : {
  },
  "id_str" : "316011687801151489",
  "in_reply_to_user_id" : 764786257,
  "text" : "@amont_ Tell me about it. http://t.co/OVOqNq1sEZ FTW",
  "id" : 316011687801151489,
  "in_reply_to_status_id" : 316010694262800386,
  "created_at" : "Mon Mar 25 02:20:32 +0000 2013",
  "in_reply_to_screen_name" : "amont_",
  "in_reply_to_user_id_str" : "764786257",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http://t.co/BYK5e07LuA",
      "expanded_url" : "http://isharefil.es/Nn6i",
      "display_url" : "isharefil.es/Nn6i"
    } ]
  },
  "geo" : {
  },
  "id_str" : "316005566281547776",
  "text" : "That annoying College Girl you see on every failed project of Teh Internet immortalized in animated GIF form: http://t.co/BYK5e07LuA",
  "id" : 316005566281547776,
  "created_at" : "Mon Mar 25 01:56:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "316000771768479744",
  "text" : "Those people that put badly chosen images as their Twitter cover image are the same peeps who have messy desktops and can't design.",
  "id" : 316000771768479744,
  "created_at" : "Mon Mar 25 01:37:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "315999370451492865",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas BTW - I love your stuff. Really digging the JS blog viewer and your Twitter media gallery. Mad props",
  "id" : 315999370451492865,
  "created_at" : "Mon Mar 25 01:31:35 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "315997121054638080",
  "geo" : {
  },
  "id_str" : "315998194259599360",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas I have considered doing that alright. I will let you know when I have done that ;)",
  "id" : 315998194259599360,
  "in_reply_to_status_id" : 315997121054638080,
  "created_at" : "Mon Mar 25 01:26:55 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "315993865683345408",
  "geo" : {
  },
  "id_str" : "315995491768864768",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas I know it doesn't have descriptions like yours. TBH - I don't have the time to document each link with short descriptions ;)",
  "id" : 315995491768864768,
  "in_reply_to_status_id" : 315993865683345408,
  "created_at" : "Mon Mar 25 01:16:10 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brian Rinaldi",
      "screen_name" : "remotesynth",
      "indices" : [ 0, 12 ],
      "id_str" : "6773532",
      "id" : 6773532
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 52 ],
      "url" : "http://t.co/Gw3Xk1KJxn",
      "expanded_url" : "http://iwantaneff.in/link/",
      "display_url" : "iwantaneff.in/link/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315992197763854336",
  "in_reply_to_user_id" : 6773532,
  "text" : "@remotesynth Hey Brian. This: http://t.co/Gw3Xk1KJxn That is all. KTHXBAI",
  "id" : 315992197763854336,
  "created_at" : "Mon Mar 25 01:03:05 +0000 2013",
  "in_reply_to_screen_name" : "remotesynth",
  "in_reply_to_user_id_str" : "6773532",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DevLinks",
      "indices" : [ 35, 44 ]
    } ],
    "urls" : [ {
      "indices" : [ 86, 108 ],
      "url" : "http://t.co/Gw3Xk1KJxn",
      "expanded_url" : "http://iwantaneff.in/link/",
      "display_url" : "iwantaneff.in/link/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315988879578644480",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas Hi there. Have you seen #DevLinks? It's very similar to your w3viewer app. http://t.co/Gw3Xk1KJxn Care to include it?",
  "id" : 315988879578644480,
  "created_at" : "Mon Mar 25 00:49:54 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 55, 65 ],
      "id_str" : "175727560",
      "id" : 175727560
    }, {
      "name" : "Lea Verou",
      "screen_name" : "LeaVerou",
      "indices" : [ 107, 116 ],
      "id_str" : "22199970",
      "id" : 22199970
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 102 ],
      "url" : "http://t.co/cG7QCN5iHW",
      "expanded_url" : "http://myshar.es/14o2egM",
      "display_url" : "myshar.es/14o2egM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315987401770151936",
  "text" : "This Open Web Platform Daily Digest is fantastic. Hope @simevidas continues it. http://t.co/cG7QCN5iHW via @LeaVerou",
  "id" : 315987401770151936,
  "created_at" : "Mon Mar 25 00:44:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Masuga",
      "screen_name" : "masuga",
      "indices" : [ 3, 10 ],
      "id_str" : "7080302",
      "id" : 7080302
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "315985688296304640",
  "text" : "RT @masuga: Facebook. For when there is absolutely, positively nothing else to read or do. The nadir of my day.",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/tweetbot\" rel=\"nofollow\">Tweetbot for iOS</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "315976900067610624",
    "text" : "Facebook. For when there is absolutely, positively nothing else to read or do. The nadir of my day.",
    "id" : 315976900067610624,
    "created_at" : "Mon Mar 25 00:02:18 +0000 2013",
    "user" : {
      "name" : "Ryan Masuga",
      "screen_name" : "masuga",
      "protected" : false,
      "id_str" : "7080302",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3237432277/8509644c046d4b2b81a1916789a4a180_normal.jpeg",
      "id" : 7080302,
      "verified" : false
    }
  },
  "id" : 315985688296304640,
  "created_at" : "Mon Mar 25 00:37:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 65, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/EEudse9xQY",
      "expanded_url" : "http://j.mp/ZhMxCT",
      "display_url" : "j.mp/ZhMxCT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315983513172193280",
  "text" : "The 11 Phases of a Web Developer\u2019s Career http://t.co/EEudse9xQY\u00A0#webdev",
  "id" : 315983513172193280,
  "created_at" : "Mon Mar 25 00:28:34 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "315981400421584897",
  "text" : "I follow 420 'geek' people.Expecting my stream to be awash with innovation and cool things, but alas Sunday is the Geek's downtime it seems.",
  "id" : 315981400421584897,
  "created_at" : "Mon Mar 25 00:20:11 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 0, 9 ],
      "id_str" : "14139773",
      "id" : 14139773
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "screencast",
      "indices" : [ 60, 71 ]
    } ],
    "urls" : [ {
      "indices" : [ 37, 59 ],
      "url" : "http://t.co/wjswG3ogao",
      "expanded_url" : "http://www.youtube.com/watch?v=EIJZIG5IdxA",
      "display_url" : "youtube.com/watch?v=EIJZIG\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "315916774140952576",
  "geo" : {
  },
  "id_str" : "315925793920389120",
  "in_reply_to_user_id" : 14139773,
  "text" : "@jdorfman I review the service here: http://t.co/wjswG3ogao #screencast",
  "id" : 315925793920389120,
  "in_reply_to_status_id" : 315916774140952576,
  "created_at" : "Sun Mar 24 20:39:13 +0000 2013",
  "in_reply_to_screen_name" : "jdorfman",
  "in_reply_to_user_id_str" : "14139773",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 0, 9 ],
      "id_str" : "14139773",
      "id" : 14139773
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 41 ],
      "url" : "http://t.co/SW1jEh9dPp",
      "expanded_url" : "http://get.gaug.es/",
      "display_url" : "get.gaug.es"
    } ]
  },
  "in_reply_to_status_id_str" : "315916774140952576",
  "geo" : {
  },
  "id_str" : "315923389426565121",
  "in_reply_to_user_id" : 14139773,
  "text" : "@jdorfman Gauges - http://t.co/SW1jEh9dPp",
  "id" : 315923389426565121,
  "in_reply_to_status_id" : 315916774140952576,
  "created_at" : "Sun Mar 24 20:29:40 +0000 2013",
  "in_reply_to_screen_name" : "jdorfman",
  "in_reply_to_user_id_str" : "14139773",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html5",
      "indices" : [ 53, 59 ]
    }, {
      "text" : "a11y",
      "indices" : [ 60, 65 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 52 ],
      "url" : "http://t.co/cbrtU6EFPd",
      "expanded_url" : "http://licorize.com/read/5378915",
      "display_url" : "licorize.com/read/5378915"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315907276810629120",
  "text" : "Interview with Steve Faulkner\nhttp://t.co/cbrtU6EFPd #html5 #a11y",
  "id" : 315907276810629120,
  "created_at" : "Sun Mar 24 19:25:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lea Verou",
      "screen_name" : "LeaVerou",
      "indices" : [ 65, 74 ],
      "id_str" : "22199970",
      "id" : 22199970
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 60 ],
      "url" : "http://t.co/OGy7cezM9b",
      "expanded_url" : "http://licorize.com/read/5378916",
      "display_url" : "licorize.com/read/5378916"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315906594133127168",
  "text" : "Use MathML today, with CSS fallback! \nhttp://t.co/OGy7cezM9b cc/ @LeaVerou",
  "id" : 315906594133127168,
  "created_at" : "Sun Mar 24 19:22:55 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 27 ],
      "url" : "http://t.co/14oA3SVL2t",
      "expanded_url" : "http://iwantaneff.in/software",
      "display_url" : "iwantaneff.in/software"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315901238204067840",
  "text" : "This http://t.co/14oA3SVL2t",
  "id" : 315901238204067840,
  "created_at" : "Sun Mar 24 19:01:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 107 ],
      "url" : "http://t.co/S1BBhVnyY4",
      "expanded_url" : "http://isharefil.es/NnYN",
      "display_url" : "isharefil.es/NnYN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315897855984410625",
  "text" : "CDN77's Log feature is so Bond-Villain. Somebody's doing exploit scans on my server: http://t.co/S1BBhVnyY4",
  "id" : 315897855984410625,
  "created_at" : "Sun Mar 24 18:48:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Larry Marburger",
      "screen_name" : "lmarburger",
      "indices" : [ 0, 11 ],
      "id_str" : "2355631",
      "id" : 2355631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "315895755158200320",
  "in_reply_to_user_id" : 2355631,
  "text" : "@lmarburger Hey. Why does the Twittercard feature not seem to work with custom domains?",
  "id" : 315895755158200320,
  "created_at" : "Sun Mar 24 18:39:51 +0000 2013",
  "in_reply_to_screen_name" : "lmarburger",
  "in_reply_to_user_id_str" : "2355631",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 101 ],
      "url" : "http://t.co/PVHYB3M3IC",
      "expanded_url" : "http://cl.ly/NmpK",
      "display_url" : "cl.ly/NmpK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315894673694666753",
  "text" : "So my Unitools project isn't getting much love. Gonna have to promote it more! http://t.co/PVHYB3M3IC",
  "id" : 315894673694666753,
  "created_at" : "Sun Mar 24 18:35:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/3lLkOQE9bf",
      "expanded_url" : "http://datalove.me/",
      "display_url" : "datalove.me"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315873809351712769",
  "text" : "http://t.co/3lLkOQE9bf",
  "id" : 315873809351712769,
  "created_at" : "Sun Mar 24 17:12:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rolling Stone",
      "screen_name" : "RollingStone",
      "indices" : [ 91, 104 ],
      "id_str" : "14780915",
      "id" : 14780915
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WPO",
      "indices" : [ 134, 138 ]
    } ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http://t.co/WNlMdkn2kd",
      "expanded_url" : "http://isharefil.es/NmeM",
      "display_url" : "isharefil.es/NmeM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315567917108453376",
  "text" : "ONE minute to fully load an article on Rolling Stone's website: http://t.co/WNlMdkn2kd cc/ @rollingstone Can I talk to your dev team? #WPO",
  "id" : 315567917108453376,
  "created_at" : "Sat Mar 23 20:57:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 53 ],
      "url" : "http://t.co/ezK9URc71B",
      "expanded_url" : "http://en.wikipedia.org/wiki/Dijkstra's_algorithm",
      "display_url" : "en.wikipedia.org/wiki/Dijkstra'\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "315543564027322370",
  "geo" : {
  },
  "id_str" : "315554489090338816",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Dijkstra's path FTW \u2192 http://t.co/ezK9URc71B",
  "id" : 315554489090338816,
  "in_reply_to_status_id" : 315543564027322370,
  "created_at" : "Sat Mar 23 20:03:47 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "XSS",
      "indices" : [ 39, 43 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "315522419911716864",
  "geo" : {
  },
  "id_str" : "315523597739360257",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar Ha Remember the Myspace #XSS worms of old? Clever folk bypassed the many JS filters that Myspace had employed. Same story now",
  "id" : 315523597739360257,
  "in_reply_to_status_id" : 315522419911716864,
  "created_at" : "Sat Mar 23 18:01:02 +0000 2013",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/60vlutqPyV",
      "expanded_url" : "http://uri.lv/",
      "display_url" : "uri.lv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315500312339292160",
  "text" : "Is Feedburner getting sunsetted soon too? http://t.co/60vlutqPyV seems like a nice replacement for it.",
  "id" : 315500312339292160,
  "created_at" : "Sat Mar 23 16:28:31 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http://t.co/XyS2vfpdaF",
      "expanded_url" : "http://myshar.es/14kihfw",
      "display_url" : "myshar.es/14kihfw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315491203766419457",
  "text" : "Here's a list of all full-time Google Reader team members http://t.co/XyS2vfpdaF\n\nOkay so complain to them, not randomers on Twitter.",
  "id" : 315491203766419457,
  "created_at" : "Sat Mar 23 15:52:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "phishing",
      "indices" : [ 72, 81 ]
    } ],
    "urls" : [ {
      "indices" : [ 44, 66 ],
      "url" : "http://t.co/7IjxWRLABh",
      "expanded_url" : "http://myshar.es/15Ee426",
      "display_url" : "myshar.es/15Ee426"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315490799829786624",
  "text" : "Hacking the &lt;a&gt; tag in 100 characters http://t.co/7IjxWRLABh Nice #phishing vector.",
  "id" : 315490799829786624,
  "created_at" : "Sat Mar 23 15:50:42 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 57 ],
      "url" : "http://t.co/NHdp3Y0wFk",
      "expanded_url" : "http://xn--55gaaaaaa281gfaqg86dja792anqa.ws/",
      "display_url" : "xn--55gaaaaaa281gfaqg86dja792anqa.ws"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315469965773897729",
  "text" : "So punycoded URLs look really ugly http://t.co/NHdp3Y0wFk I could imagine the awkardness of explaining \u2603.net over the phone. \"Snowman . net\"",
  "id" : 315469965773897729,
  "created_at" : "Sat Mar 23 14:27:55 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "315469268013690880",
  "text" : "Look what this URL redirects to \u2192 \u2601\u2192\u2744\u2192\u2603\u2192\u2600\u2192\u263A\u2192\u2602\u2192\u2639\u2192\u271D.ws",
  "id" : 315469268013690880,
  "created_at" : "Sat Mar 23 14:25:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 135 ],
      "url" : "http://t.co/3e8xhTVJNQ",
      "expanded_url" : "http://iphoneresolution.com/",
      "display_url" : "iphoneresolution.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315443814791774208",
  "text" : "I do worry about these single serving sites going down one day.I myself am a victim of owning too many domains \u2192 http://t.co/3e8xhTVJNQ",
  "id" : 315443814791774208,
  "created_at" : "Sat Mar 23 12:44:00 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http://t.co/ajXES1OW9p",
      "expanded_url" : "http://myshar.es/15BHBcH",
      "display_url" : "myshar.es/15BHBcH"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315239461506129920",
  "text" : "Application Load Testing Tools for API Endpoints with loader.io http://t.co/ajXES1OW9p DDOS Your site for free ;)",
  "id" : 315239461506129920,
  "created_at" : "Fri Mar 22 23:11:59 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "315230284599685121",
  "text" : "Not liking the contrived, Title-Bait articles on ARS Technica. I drip-feed Pastebin into my Inbox. I get there first, before you ARS",
  "id" : 315230284599685121,
  "created_at" : "Fri Mar 22 22:35:31 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 28 ],
      "url" : "http://t.co/mJlBZpgxk0",
      "expanded_url" : "http://isharefil.es/Nm26",
      "display_url" : "isharefil.es/Nm26"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315213387695017984",
  "text" : "LOL \u2192 http://t.co/mJlBZpgxk0",
  "id" : 315213387695017984,
  "created_at" : "Fri Mar 22 21:28:22 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 140 ],
      "url" : "http://t.co/XaFPB2YWeI",
      "expanded_url" : "http://dribbble.com/shots/997282-Antipixel-Buttons-80-x-15",
      "display_url" : "dribbble.com/shots/997282-A\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315206607573225472",
  "text" : "I re-done my Antipixel collection on Dribbble. I had to nail the alignment of the buttons, and clean up the comment \u2192 http://t.co/XaFPB2YWeI",
  "id" : 315206607573225472,
  "created_at" : "Fri Mar 22 21:01:26 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 51, 63 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 64, 71 ]
    } ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http://t.co/9UEAYgQ3UD",
      "expanded_url" : "http://myshar.es/14ezE1g",
      "display_url" : "myshar.es/14ezE1g"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315189399719653376",
  "text" : "A Thorough Introduction To Backbone.Marionette via @smashingmag #webdev http://t.co/9UEAYgQ3UD",
  "id" : 315189399719653376,
  "created_at" : "Fri Mar 22 19:53:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HTML5",
      "indices" : [ 18, 24 ]
    }, {
      "text" : "javascript",
      "indices" : [ 67, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 79, 101 ],
      "url" : "http://t.co/Kww9fmDrzg",
      "expanded_url" : "http://myshar.es/14ezKWF",
      "display_url" : "myshar.es/14ezKWF"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315161492867649536",
  "text" : "joy.js - A joyful #HTML5 2D Game Engine designed to be easy to use #javascript http://t.co/Kww9fmDrzg",
  "id" : 315161492867649536,
  "created_at" : "Fri Mar 22 18:02:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http://t.co/G6IMa5AmrD",
      "expanded_url" : "http://myshar.es/15vKVWV",
      "display_url" : "myshar.es/15vKVWV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315147385984655360",
  "text" : "Markdown.css - make HTML look like plain-text http://t.co/G6IMa5AmrD",
  "id" : 315147385984655360,
  "created_at" : "Fri Mar 22 17:06:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http://t.co/iZY5vgTwCy",
      "expanded_url" : "http://1lineart.kulaone.com/",
      "display_url" : "1lineart.kulaone.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315102655070470144",
  "text" : "Love these one-line Unicode art snippets \u2192 http://t.co/iZY5vgTwCy Very tempted to post at least one per day on my Twitters.",
  "id" : 315102655070470144,
  "created_at" : "Fri Mar 22 14:08:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 72 ],
      "url" : "http://t.co/grT48gKMY9",
      "expanded_url" : "http://myshar.es/15zUa8A",
      "display_url" : "myshar.es/15zUa8A"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315102429228183552",
  "text" : "Rather Splendid: On staying current with WebDev \u2192 http://t.co/grT48gKMY9",
  "id" : 315102429228183552,
  "created_at" : "Fri Mar 22 14:07:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "315099664045178881",
  "text" : "There is nothing bad I can say about Chrome Devtools, other than I never use them and I can't wait for the improved Firefox Devtools to ship",
  "id" : 315099664045178881,
  "created_at" : "Fri Mar 22 13:56:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Victor \u2603",
      "screen_name" : "_victa",
      "indices" : [ 3, 10 ],
      "id_str" : "47319826",
      "id" : 47319826
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "protip",
      "indices" : [ 123, 130 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "315089947289845761",
  "text" : "RT @_victa: Find a character unicode:\n\n'\u2603'.charCodeAt(0).toString(16); // &gt;\"2603\"\n\nthen:\n\n.foo:after{content:\"\\2603\";}\n\n#protip",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "protip",
        "indices" : [ 111, 118 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "315043697047773184",
    "text" : "Find a character unicode:\n\n'\u2603'.charCodeAt(0).toString(16); // &gt;\"2603\"\n\nthen:\n\n.foo:after{content:\"\\2603\";}\n\n#protip",
    "id" : 315043697047773184,
    "created_at" : "Fri Mar 22 10:14:05 +0000 2013",
    "user" : {
      "name" : "Victor \u2603",
      "screen_name" : "_victa",
      "protected" : false,
      "id_str" : "47319826",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2538689839/6wmc0w7lgchqetttyxa6_normal.jpeg",
      "id" : 47319826,
      "verified" : false
    }
  },
  "id" : 315089947289845761,
  "created_at" : "Fri Mar 22 13:17:52 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TalksLab",
      "screen_name" : "TalksLab",
      "indices" : [ 55, 64 ],
      "id_str" : "125804733",
      "id" : 125804733
    }, {
      "name" : "Guilherme Ferreira",
      "screen_name" : "gsferreira",
      "indices" : [ 97, 108 ],
      "id_str" : "50387401",
      "id" : 50387401
    }, {
      "name" : "Nelson Reis",
      "screen_name" : "NelsonReis",
      "indices" : [ 109, 120 ],
      "id_str" : "14554183",
      "id" : 14554183
    }, {
      "name" : "Rui Neves",
      "screen_name" : "ruimlneves",
      "indices" : [ 121, 132 ],
      "id_str" : "31781390",
      "id" : 31781390
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 65, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 95 ],
      "url" : "http://t.co/9wGsAWbOb6",
      "expanded_url" : "http://myshar.es/15vKPyw",
      "display_url" : "myshar.es/15vKPyw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "315071683297939456",
  "text" : "Metro-Bootstrap: Twitter Bootstrap with Metro style by @talkslab #webdev http://t.co/9wGsAWbOb6 +@gsferreira @nelsonreis @ruimlneves",
  "id" : 315071683297939456,
  "created_at" : "Fri Mar 22 12:05:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    }, {
      "name" : "Neil Matatall",
      "screen_name" : "ndm",
      "indices" : [ 15, 19 ],
      "id_str" : "43220757",
      "id" : 43220757
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "XSS",
      "indices" : [ 103, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http://t.co/g1OF8tXRKW",
      "expanded_url" : "http://iwantaneff.in/xss/",
      "display_url" : "iwantaneff.in/xss/"
    } ]
  },
  "in_reply_to_status_id_str" : "314838215234297857",
  "geo" : {
  },
  "id_str" : "314840402786144258",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar @ndm Filter angle brackets, and you win. On the other side: there is angle bracket free #XSS \u2192 http://t.co/g1OF8tXRKW",
  "id" : 314840402786144258,
  "in_reply_to_status_id" : 314838215234297857,
  "created_at" : "Thu Mar 21 20:46:16 +0000 2013",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http://t.co/KnPjOmx9Z7",
      "expanded_url" : "http://myshar.es/14ezGWV",
      "display_url" : "myshar.es/14ezGWV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314827056011694081",
  "text" : "Why does the jQuery Object behave like an array? http://t.co/KnPjOmx9Z7",
  "id" : 314827056011694081,
  "created_at" : "Thu Mar 21 19:53:14 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 54, 62 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 121, 143 ],
      "url" : "http://t.co/rOTZLmBc2t",
      "expanded_url" : "http://myshar.es/14f1IBt",
      "display_url" : "myshar.es/14f1IBt"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314819486513500161",
  "text" : "Shadow DOM 301 - Advanced Concepts &amp; DOM APIs via @codepo8 - So shadow DOM is not an esoteric concept anymore? \u2026 WIN http://t.co/rOTZLmBc2t",
  "id" : 314819486513500161,
  "created_at" : "Thu Mar 21 19:23:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 3, 11 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https://t.co/DORNm55F4l",
      "expanded_url" : "https://careers.mozilla.org/en-US/position/opLgXfw7",
      "display_url" : "careers.mozilla.org/en-US/position\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314816304815153152",
  "text" : "RT @codepo8: Job: Senior Technical Writer for Firefox OS / MDN https://t.co/DORNm55F4l",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 73 ],
        "url" : "https://t.co/DORNm55F4l",
        "expanded_url" : "https://careers.mozilla.org/en-US/position/opLgXfw7",
        "display_url" : "careers.mozilla.org/en-US/position\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "314812049966391296",
    "text" : "Job: Senior Technical Writer for Firefox OS / MDN https://t.co/DORNm55F4l",
    "id" : 314812049966391296,
    "created_at" : "Thu Mar 21 18:53:36 +0000 2013",
    "user" : {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "protected" : false,
      "id_str" : "13567",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1666904408/codepo8_normal.png",
      "id" : 13567,
      "verified" : false
    }
  },
  "id" : 314816304815153152,
  "created_at" : "Thu Mar 21 19:10:30 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 32, 43 ]
    } ],
    "urls" : [ {
      "indices" : [ 44, 66 ],
      "url" : "http://t.co/3JFddFMYQJ",
      "expanded_url" : "http://myshar.es/14ezF5f",
      "display_url" : "myshar.es/14ezF5f"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314799160492695552",
  "text" : "JSPatterns: Optional parameters #javascript http://t.co/3JFddFMYQJ",
  "id" : 314799160492695552,
  "created_at" : "Thu Mar 21 18:02:23 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Craig Buckler",
      "screen_name" : "craigbuckler",
      "indices" : [ 47, 60 ],
      "id_str" : "18670151",
      "id" : 18670151
    }, {
      "name" : "SitePoint",
      "screen_name" : "sitepointdotcom",
      "indices" : [ 65, 81 ],
      "id_str" : "15743570",
      "id" : 15743570
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 82, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 90, 112 ],
      "url" : "http://t.co/If8iASI9DO",
      "expanded_url" : "http://myshar.es/14ezDKz",
      "display_url" : "myshar.es/14ezDKz"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314785008315400192",
  "text" : "How to Edit Source Files Directly in Chrome by @craigbuckler via @sitepointdotcom #webdev http://t.co/If8iASI9DO",
  "id" : 314785008315400192,
  "created_at" : "Thu Mar 21 17:06:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bitly.com\" rel=\"nofollow\">bitly</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http://t.co/7l4hsjD8ss",
      "expanded_url" : "http://myshar.es/10nBrgQ",
      "display_url" : "myshar.es/10nBrgQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314776090046517248",
  "text" : "Creative People Say No \u2014 Thoughts on creativity \u2014 Medium http://t.co/7l4hsjD8ss",
  "id" : 314776090046517248,
  "created_at" : "Thu Mar 21 16:30:42 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bitly.com\" rel=\"nofollow\">bitly</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 51 ],
      "url" : "http://t.co/lRNmE9gNAR",
      "expanded_url" : "http://myshar.es/10nB2eg",
      "display_url" : "myshar.es/10nB2eg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314775726438088704",
  "text" : "jQuery.Feedback by siong1987 http://t.co/lRNmE9gNAR",
  "id" : 314775726438088704,
  "created_at" : "Thu Mar 21 16:29:16 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "St\u00E9phane Bortzmeyer",
      "screen_name" : "bortzmeyer",
      "indices" : [ 72, 83 ],
      "id_str" : "75263632",
      "id" : 75263632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/PQUcRB61ds",
      "expanded_url" : "http://myshar.es/14eEITg",
      "display_url" : "myshar.es/14eEITg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314770932046626816",
  "text" : "Identifying tweets in lesser-used languages\u2192 http://t.co/PQUcRB61ds via @bortzmeyer",
  "id" : 314770932046626816,
  "created_at" : "Thu Mar 21 16:10:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "314757540820951040",
  "geo" : {
  },
  "id_str" : "314758023283347456",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Isn't the point of the service that you _discard_ your current mailbox in favor of a remote / virtual mailbox?",
  "id" : 314758023283347456,
  "in_reply_to_status_id" : 314757540820951040,
  "created_at" : "Thu Mar 21 15:18:55 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 119 ],
      "url" : "http://t.co/d4vyoqfkYN",
      "expanded_url" : "http://youtube.decenturl.com/how-to-dubstep",
      "display_url" : "youtube.decenturl.com/how-to-dubstep"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314757703589322752",
  "text" : "So I signed up for this service called Decent URL. Let's test it out with a crazy Youtube video: http://t.co/d4vyoqfkYN",
  "id" : 314757703589322752,
  "created_at" : "Thu Mar 21 15:17:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https://t.co/fYXUeqDvRH",
      "expanded_url" : "https://www.outboxmail.com/",
      "display_url" : "outboxmail.com"
    } ]
  },
  "in_reply_to_status_id_str" : "314741250974695425",
  "geo" : {
  },
  "id_str" : "314756921427107840",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Outbox mail FTW \u2192 https://t.co/fYXUeqDvRH",
  "id" : 314756921427107840,
  "in_reply_to_status_id" : 314741250974695425,
  "created_at" : "Thu Mar 21 15:14:32 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 68, 76 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 60 ],
      "url" : "http://t.co/cVnUN9R7K8",
      "expanded_url" : "http://myshar.es/14ek85v",
      "display_url" : "myshar.es/14ek85v"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314730784370094084",
  "text" : "Ubuntu Release Party - With a twist \u2192 http://t.co/cVnUN9R7K8 ;) via @codepo8",
  "id" : 314730784370094084,
  "created_at" : "Thu Mar 21 13:30:41 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jQuery",
      "indices" : [ 23, 30 ]
    } ],
    "urls" : [ {
      "indices" : [ 40, 62 ],
      "url" : "http://t.co/dWGoVi6Sv5",
      "expanded_url" : "http://myshar.es/14811d9",
      "display_url" : "myshar.es/14811d9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314464626156916736",
  "text" : "Swipebox \u2192 A touchable #jQuery lightbox http://t.co/dWGoVi6Sv5 Yes, I know. Another lightbox. But this is a _2013_ lightbox!",
  "id" : 314464626156916736,
  "created_at" : "Wed Mar 20 19:53:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 36 ],
      "url" : "http://t.co/ZGwmto6UWA",
      "expanded_url" : "http://myshar.es/1480Sql",
      "display_url" : "myshar.es/1480Sql"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314436733385207809",
  "text" : "jQuery Nested http://t.co/ZGwmto6UWA",
  "id" : 314436733385207809,
  "created_at" : "Wed Mar 20 18:02:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 39 ],
      "url" : "http://t.co/004LxDfpDH",
      "expanded_url" : "http://myshar.es/1480Nmo",
      "display_url" : "myshar.es/1480Nmo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314422597410512896",
  "text" : "W3C Link Checker http://t.co/004LxDfpDH",
  "id" : 314422597410512896,
  "created_at" : "Wed Mar 20 17:06:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 63, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 40, 62 ],
      "url" : "http://t.co/Y3a8t93dL6",
      "expanded_url" : "http://myshar.es/1480Fn4",
      "display_url" : "myshar.es/1480Fn4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314346860523438080",
  "text" : "Messenger - Alerts for the 21st Century http://t.co/Y3a8t93dL6 #javascript",
  "id" : 314346860523438080,
  "created_at" : "Wed Mar 20 12:05:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "JavaScript",
      "indices" : [ 29, 40 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 77 ],
      "url" : "http://t.co/VHQBEHZ3aQ",
      "expanded_url" : "http://myshar.es/1480Bna",
      "display_url" : "myshar.es/1480Bna"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314299603912642560",
  "text" : "\"Welcome to your community's #JavaScript package host\" http://t.co/VHQBEHZ3aQ",
  "id" : 314299603912642560,
  "created_at" : "Wed Mar 20 08:57:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 56 ],
      "url" : "http://t.co/jJxIPX1MQ7",
      "expanded_url" : "http://myshar.es/1580GTC",
      "display_url" : "myshar.es/1580GTC"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314102245581008896",
  "text" : "\"How Twitter Got Its Line Breaks\" http://t.co/jJxIPX1MQ7",
  "id" : 314102245581008896,
  "created_at" : "Tue Mar 19 19:53:05 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Addy Osmani",
      "screen_name" : "addyosmani",
      "indices" : [ 64, 75 ],
      "id_str" : "35432643",
      "id" : 35432643
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 76, 83 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 106 ],
      "url" : "http://t.co/5s19azjvcz",
      "expanded_url" : "http://myshar.es/141uUMb",
      "display_url" : "myshar.es/141uUMb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314074383276724224",
  "text" : "DevTools: Visually Re-engineering CSS For Faster Paint Times by @addyosmani #webdev http://t.co/5s19azjvcz",
  "id" : 314074383276724224,
  "created_at" : "Tue Mar 19 18:02:22 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 57 ],
      "url" : "http://t.co/sL07dcOoay",
      "expanded_url" : "http://myshar.es/157Q8nv",
      "display_url" : "myshar.es/157Q8nv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "314060263873736704",
  "text" : "Playing around with CSS3 animation http://t.co/sL07dcOoay",
  "id" : 314060263873736704,
  "created_at" : "Tue Mar 19 17:06:16 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Resig",
      "screen_name" : "jeresig",
      "indices" : [ 39, 47 ],
      "id_str" : "752673",
      "id" : 752673
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 70 ],
      "url" : "http://t.co/Z1jHFlgxad",
      "expanded_url" : "http://myshar.es/141uUvK",
      "display_url" : "myshar.es/141uUvK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313984520443555840",
  "text" : "Keeping Passwords in Source Control by @jeresig http://t.co/Z1jHFlgxad",
  "id" : 313984520443555840,
  "created_at" : "Tue Mar 19 12:05:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HTML5",
      "indices" : [ 0, 6 ]
    } ],
    "urls" : [ {
      "indices" : [ 90, 112 ],
      "url" : "http://t.co/5GcRhhyWHL",
      "expanded_url" : "http://myshar.es/157Q8Ux",
      "display_url" : "myshar.es/157Q8Ux"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313937167539904512",
  "text" : "#HTML5 Labs Test Jam - Hosted by Microsoft Open Tech: Pointer Events prototype for WebKit http://t.co/5GcRhhyWHL",
  "id" : 313937167539904512,
  "created_at" : "Tue Mar 19 08:57:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "souders",
      "screen_name" : "souders",
      "indices" : [ 0, 8 ],
      "id_str" : "15431118",
      "id" : 15431118
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 78 ],
      "url" : "http://t.co/zqnS2IETAH",
      "expanded_url" : "http://www.stevesouders.com/blog/2013/03/18/http-archive-jquery/#comment-36579",
      "display_url" : "stevesouders.com/blog/2013/03/1\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "313798244763308033",
  "geo" : {
  },
  "id_str" : "313808609186295809",
  "in_reply_to_user_id" : 15431118,
  "text" : "@souders That's a great post Steve. Here's my thoughts: http://t.co/zqnS2IETAH Regards ;)",
  "id" : 313808609186295809,
  "in_reply_to_status_id" : 313798244763308033,
  "created_at" : "Tue Mar 19 00:26:17 +0000 2013",
  "in_reply_to_screen_name" : "souders",
  "in_reply_to_user_id_str" : "15431118",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 99 ],
      "url" : "http://t.co/dndkir9JAC",
      "expanded_url" : "http://myshar.es/15jrjFm",
      "display_url" : "myshar.es/15jrjFm"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313791089867890688",
  "text" : "\"This is a handy site for shortening links and making them more trustworthy\" http://t.co/dndkir9JAC",
  "id" : 313791089867890688,
  "created_at" : "Mon Mar 18 23:16:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 70 ],
      "url" : "http://t.co/In4qCrE2ed",
      "expanded_url" : "http://myshar.es/15jnSi3",
      "display_url" : "myshar.es/15jnSi3"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313786416180109313",
  "text" : "The danger of the trailing dots in domain names http://t.co/In4qCrE2ed",
  "id" : 313786416180109313,
  "created_at" : "Mon Mar 18 22:58:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 0, 8 ],
      "id_str" : "532923",
      "id" : 532923
    }, {
      "name" : "JavaScript Daily",
      "screen_name" : "JavaScriptDaily",
      "indices" : [ 9, 25 ],
      "id_str" : "459275531",
      "id" : 459275531
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 110 ],
      "url" : "http://t.co/BP81xhaX3A",
      "expanded_url" : "http://f.cl.ly/items/1N213k172Q2r3c2w330f/Image%202013-03-18%20at%209.59.37%20PM.png",
      "display_url" : "f.cl.ly/items/1N213k17\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "313767281870110720",
  "geo" : {
  },
  "id_str" : "313771973639536640",
  "in_reply_to_user_id" : 532923,
  "text" : "@mathias @JavaScriptDaily Also doesn't ignore Zalgo. \"HE COMES\" Should be 8 characters. http://t.co/BP81xhaX3A",
  "id" : 313771973639536640,
  "in_reply_to_status_id" : 313767281870110720,
  "created_at" : "Mon Mar 18 22:00:42 +0000 2013",
  "in_reply_to_screen_name" : "mathias",
  "in_reply_to_user_id_str" : "532923",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 74 ],
      "url" : "http://t.co/sL07dcOoay",
      "expanded_url" : "http://myshar.es/157Q8nv",
      "display_url" : "myshar.es/157Q8nv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313739851411369985",
  "text" : "How to Animating a Twitter BootStrap Icon with CSS3 http://t.co/sL07dcOoay",
  "id" : 313739851411369985,
  "created_at" : "Mon Mar 18 19:53:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GoSquared",
      "screen_name" : "GoSquared",
      "indices" : [ 0, 10 ],
      "id_str" : "12987712",
      "id" : 12987712
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "313711415145930752",
  "geo" : {
  },
  "id_str" : "313714025861111808",
  "in_reply_to_user_id" : 12987712,
  "text" : "@GoSquared If I had the time, I would create applications built with GoSquared for sure. But my time and money is invested in Gauges ;)",
  "id" : 313714025861111808,
  "in_reply_to_status_id" : 313711415145930752,
  "created_at" : "Mon Mar 18 18:10:26 +0000 2013",
  "in_reply_to_screen_name" : "GoSquared",
  "in_reply_to_user_id_str" : "12987712",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Hostetler",
      "screen_name" : "mikehostetler",
      "indices" : [ 57, 71 ],
      "id_str" : "784290",
      "id" : 784290
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 72, 79 ]
    } ],
    "urls" : [ {
      "indices" : [ 80, 102 ],
      "url" : "http://t.co/Fz9h8JL1Qc",
      "expanded_url" : "http://myshar.es/Yh7ZW6",
      "display_url" : "myshar.es/Yh7ZW6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313711999303417857",
  "text" : "5 reasons a CEO should champion Responsive Web Design by @mikehostetler #webdev http://t.co/Fz9h8JL1Qc",
  "id" : 313711999303417857,
  "created_at" : "Mon Mar 18 18:02:23 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rob Hawkes",
      "screen_name" : "robhawkes",
      "indices" : [ 0, 10 ],
      "id_str" : "14442542",
      "id" : 14442542
    }, {
      "name" : "GoSquared",
      "screen_name" : "GoSquared",
      "indices" : [ 11, 21 ],
      "id_str" : "12987712",
      "id" : 12987712
    }, {
      "name" : "Gaug.es",
      "screen_name" : "gaugesapp",
      "indices" : [ 37, 47 ],
      "id_str" : "245369690",
      "id" : 245369690
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 133 ],
      "url" : "http://t.co/kFX7Hqjvfv",
      "expanded_url" : "http://www.youtube.com/watch?v=CblG1JWJKKE",
      "display_url" : "youtube.com/watch?v=CblG1J\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "313708465157718016",
  "geo" : {
  },
  "id_str" : "313711006977253376",
  "in_reply_to_user_id" : 14442542,
  "text" : "@robhawkes @GoSquared Have you tried @gaugesapp? I made a screencast explaining the app I built with its API \u2192 http://t.co/kFX7Hqjvfv",
  "id" : 313711006977253376,
  "in_reply_to_status_id" : 313708465157718016,
  "created_at" : "Mon Mar 18 17:58:27 +0000 2013",
  "in_reply_to_screen_name" : "robhawkes",
  "in_reply_to_user_id_str" : "14442542",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Louis Lazaris",
      "screen_name" : "ImpressiveWebs",
      "indices" : [ 52, 67 ],
      "id_str" : "39445335",
      "id" : 39445335
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WebDev",
      "indices" : [ 0, 7 ]
    } ],
    "urls" : [ {
      "indices" : [ 68, 90 ],
      "url" : "http://t.co/xayqDGwGqf",
      "expanded_url" : "http://myshar.es/141uR2Z",
      "display_url" : "myshar.es/141uR2Z"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313697829833031681",
  "text" : "#WebDev &amp; Tech Email Newsletters I Subscribe by @ImpressiveWebs http://t.co/xayqDGwGqf",
  "id" : 313697829833031681,
  "created_at" : "Mon Mar 18 17:06:05 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andrew Auernheimer",
      "screen_name" : "rabite",
      "indices" : [ 3, 10 ],
      "id_str" : "15707229",
      "id" : 15707229
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ResponsibleDisclosure",
      "indices" : [ 116, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "313691603065786368",
  "text" : "If @rabite responsibly disclosed the AT&amp;T leak he wouldn't be arrested today. That's why we now have Wikileaks! #ResponsibleDisclosure",
  "id" : 313691603065786368,
  "created_at" : "Mon Mar 18 16:41:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 66, 78 ],
      "id_str" : "793830",
      "id" : 793830
    }, {
      "name" : "CSS-Tricks",
      "screen_name" : "Real_CSS_Tricks",
      "indices" : [ 83, 99 ],
      "id_str" : "285019665",
      "id" : 285019665
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HTML5",
      "indices" : [ 0, 6 ]
    } ],
    "urls" : [ {
      "indices" : [ 100, 122 ],
      "url" : "http://t.co/NT85EpuJWA",
      "expanded_url" : "http://myshar.es/141uS7c",
      "display_url" : "myshar.es/141uS7c"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313622113456119808",
  "text" : "#HTML5 Drag and Drop Avatar Changer with Resizing and Cropping by @chriscoyier via @real_css_tricks http://t.co/NT85EpuJWA",
  "id" : 313622113456119808,
  "created_at" : "Mon Mar 18 12:05:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/hmXdGcR5ee",
      "expanded_url" : "http://myshar.es/157Pvuj",
      "display_url" : "myshar.es/157Pvuj"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313574763392024577",
  "text" : "DailyJS: Backbone.js Tutorial: Editing Lists http://t.co/hmXdGcR5ee",
  "id" : 313574763392024577,
  "created_at" : "Mon Mar 18 08:57:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Todd Wolfson",
      "screen_name" : "twolfsn",
      "indices" : [ 0, 8 ],
      "id_str" : "578325784",
      "id" : 578325784
    }, {
      "name" : "Lea Verou",
      "screen_name" : "LeaVerou",
      "indices" : [ 9, 18 ],
      "id_str" : "22199970",
      "id" : 22199970
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 89 ],
      "url" : "http://t.co/RXVX6b6NMn",
      "expanded_url" : "http://isharefil.es/NdDD",
      "display_url" : "isharefil.es/NdDD"
    } ]
  },
  "in_reply_to_status_id_str" : "313392042472521729",
  "geo" : {
  },
  "id_str" : "313401975159021570",
  "in_reply_to_user_id" : 578325784,
  "text" : "@twolfsn @LeaVerou Opera's Reader is still as badass as they come: http://t.co/RXVX6b6NMn",
  "id" : 313401975159021570,
  "in_reply_to_status_id" : 313392042472521729,
  "created_at" : "Sun Mar 17 21:30:28 +0000 2013",
  "in_reply_to_screen_name" : "twolfsn",
  "in_reply_to_user_id_str" : "578325784",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rob O'Dwyer",
      "screen_name" : "odwyerrob",
      "indices" : [ 0, 10 ],
      "id_str" : "20828908",
      "id" : 20828908
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 90 ],
      "url" : "http://t.co/ikstcWVbvP",
      "expanded_url" : "http://shiflett.org/blog/2013/mar/xss-is-still-tricky",
      "display_url" : "shiflett.org/blog/2013/mar/\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "313379698963001345",
  "geo" : {
  },
  "id_str" : "313381144303378433",
  "in_reply_to_user_id" : 20828908,
  "text" : "@odwyerrob Yeah it's just werd is all. I got the snippet from here: http://t.co/ikstcWVbvP",
  "id" : 313381144303378433,
  "in_reply_to_status_id" : 313379698963001345,
  "created_at" : "Sun Mar 17 20:07:41 +0000 2013",
  "in_reply_to_screen_name" : "odwyerrob",
  "in_reply_to_user_id_str" : "20828908",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "parser",
      "indices" : [ 20, 27 ]
    }, {
      "text" : "DOMMagic",
      "indices" : [ 161, 170 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "313371673766264833",
  "text" : "This is for all the #parser snobs \u2192 How is this alert executing?\n\n&lt;script&gt;\n\nxss= \"&lt;/script&gt;&lt;script&gt;alert(1);&lt;/script&gt;\"\n\n&lt;/script&gt;\n\n#DOMMagic",
  "id" : 313371673766264833,
  "created_at" : "Sun Mar 17 19:30:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "XSS",
      "indices" : [ 10, 14 ]
    }, {
      "text" : "SWF",
      "indices" : [ 80, 84 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http://t.co/g1OF8tXRKW",
      "expanded_url" : "http://iwantaneff.in/xss/",
      "display_url" : "iwantaneff.in/xss/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313360713932484610",
  "text" : "Here's an #XSS vector that uses no angle brackets at all http://t.co/g1OF8tXRKW #SWF (Latest Chrome Only)",
  "id" : 313360713932484610,
  "created_at" : "Sun Mar 17 18:46:30 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 53 ],
      "url" : "http://t.co/MmG1DaQDor",
      "expanded_url" : "http://drbl.in/hbGq",
      "display_url" : "drbl.in/hbGq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313306443803680768",
  "text" : "80 x 15 Antipixel Collection \u2192 http://t.co/MmG1DaQDor",
  "id" : 313306443803680768,
  "created_at" : "Sun Mar 17 15:10:51 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter Beverloo",
      "screen_name" : "beverloo",
      "indices" : [ 0, 9 ],
      "id_str" : "49739243",
      "id" : 49739243
    }, {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 106, 114 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https://t.co/QeiRZHnynE",
      "expanded_url" : "https://github.com/higgo/php.url.shortener.with.bitly",
      "display_url" : "github.com/higgo/php.url.\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313026648201125888",
  "in_reply_to_user_id" : 49739243,
  "text" : "@beverloo Your PHP URL Shortener script you worked on has Bitly support. Yay! https://t.co/QeiRZHnynE cc/ @mathias",
  "id" : 313026648201125888,
  "created_at" : "Sat Mar 16 20:39:03 +0000 2013",
  "in_reply_to_screen_name" : "beverloo",
  "in_reply_to_user_id_str" : "49739243",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Prinzhorn",
      "screen_name" : "Prinzhorn",
      "indices" : [ 0, 10 ],
      "id_str" : "187226449",
      "id" : 187226449
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tip",
      "indices" : [ 136, 140 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "313016457321660417",
  "geo" : {
  },
  "id_str" : "313024199616757761",
  "in_reply_to_user_id" : 187226449,
  "text" : "@Prinzhorn The barrier to entry of learning all these tools proves too much every-time. You are best just going with your gut instinct. #tip",
  "id" : 313024199616757761,
  "in_reply_to_status_id" : 313016457321660417,
  "created_at" : "Sat Mar 16 20:29:19 +0000 2013",
  "in_reply_to_screen_name" : "Prinzhorn",
  "in_reply_to_user_id_str" : "187226449",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 82, 94 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 77 ],
      "url" : "http://t.co/pmlRlYajKA",
      "expanded_url" : "http://webplatformtools.org/",
      "display_url" : "webplatformtools.org"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313023871324405760",
  "text" : "In the pipeline \u2192 Submit everything I've worked on to: http://t.co/pmlRlYajKA via @smashingmag",
  "id" : 313023871324405760,
  "created_at" : "Sat Mar 16 20:28:01 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.mobypicture.com\" rel=\"nofollow\">Mobypicture</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WebStandards",
      "indices" : [ 38, 51 ]
    } ],
    "urls" : [ {
      "indices" : [ 54, 76 ],
      "url" : "http://t.co/pOpJuEwSHi",
      "expanded_url" : "http://moby.to/7b9s1s",
      "display_url" : "moby.to/7b9s1s"
    } ]
  },
  "geo" : {
  },
  "id_str" : "313012877881647107",
  "text" : "Rocking the blue beanie in support of #WebStandards - http://t.co/pOpJuEwSHi",
  "id" : 313012877881647107,
  "created_at" : "Sat Mar 16 19:44:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "313001573695516672",
  "geo" : {
  },
  "id_str" : "313005074035122176",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert Basically telling the world. Mention all you favorite devs asking them to promote it. The only way is up!",
  "id" : 313005074035122176,
  "in_reply_to_status_id" : 313001573695516672,
  "created_at" : "Sat Mar 16 19:13:19 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    }, {
      "name" : "meme n",
      "screen_name" : "mentions",
      "indices" : [ 112, 121 ],
      "id_str" : "222450427",
      "id" : 222450427
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ADN",
      "indices" : [ 134, 138 ]
    } ],
    "urls" : [ {
      "indices" : [ 60, 82 ],
      "url" : "http://t.co/Nb1985ASyl",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    } ]
  },
  "in_reply_to_status_id_str" : "312982622320087041",
  "geo" : {
  },
  "id_str" : "312999935933026305",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert Have you done a huge shout-out to people on http://t.co/Nb1985ASyl I am quite a fan of multiple @mentions to peeps on #ADN",
  "id" : 312999935933026305,
  "in_reply_to_status_id" : 312982622320087041,
  "created_at" : "Sat Mar 16 18:52:54 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 3, 18 ],
      "id_str" : "276581825",
      "id" : 276581825
    }, {
      "name" : "backbone.js",
      "screen_name" : "backbone_js",
      "indices" : [ 40, 52 ],
      "id_str" : "565922261",
      "id" : 565922261
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 111 ],
      "url" : "http://t.co/lWlFBTpKG4",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    }, {
      "indices" : [ 112, 135 ],
      "url" : "https://t.co/NyCa3Hp82N",
      "expanded_url" : "https://github.com/damienklinnert/rhetoricus",
      "display_url" : "github.com/damienklinnert\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312999691598036992",
  "text" : "RT @damienklinnert: if you are learning @backbone_js, check out this simple demo app for http://t.co/lWlFBTpKG4 https://t.co/NyCa3Hp82N",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "backbone.js",
        "screen_name" : "backbone_js",
        "indices" : [ 20, 32 ],
        "id_str" : "565922261",
        "id" : 565922261
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 91 ],
        "url" : "http://t.co/lWlFBTpKG4",
        "expanded_url" : "http://app.net",
        "display_url" : "app.net"
      }, {
        "indices" : [ 92, 115 ],
        "url" : "https://t.co/NyCa3Hp82N",
        "expanded_url" : "https://github.com/damienklinnert/rhetoricus",
        "display_url" : "github.com/damienklinnert\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "312982622320087041",
    "text" : "if you are learning @backbone_js, check out this simple demo app for http://t.co/lWlFBTpKG4 https://t.co/NyCa3Hp82N",
    "id" : 312982622320087041,
    "created_at" : "Sat Mar 16 17:44:06 +0000 2013",
    "user" : {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "protected" : false,
      "id_str" : "276581825",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3708896355/7f659cf0f11eb370a9ca02a2b08a372b_normal.jpeg",
      "id" : 276581825,
      "verified" : false
    }
  },
  "id" : 312999691598036992,
  "created_at" : "Sat Mar 16 18:51:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 111 ],
      "url" : "http://t.co/lhwDKcBhcr",
      "expanded_url" : "http://higg.im/dox/",
      "display_url" : "higg.im/dox/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312998504916213760",
  "text" : "These are all the most important social networks I am on. For those who want to dox me \u2192 http://t.co/lhwDKcBhcr",
  "id" : 312998504916213760,
  "created_at" : "Sat Mar 16 18:47:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "notetoself",
      "indices" : [ 0, 11 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "312976857547030530",
  "text" : "#notetoself \u2192\n\n&lt;meta name=\"HandheldFriendly\" content=\"True\"&gt;",
  "id" : 312976857547030530,
  "created_at" : "Sat Mar 16 17:21:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "312962263222992896",
  "text" : "-function(){ +function(){ ~function(){ !function(){\n\nvar super_sandboxed_variable = \"super sandboxed\"\n\n}(); }(); }(); }();",
  "id" : 312962263222992896,
  "created_at" : "Sat Mar 16 16:23:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samuel Clyde Jones",
      "screen_name" : "OrangutanClyde",
      "indices" : [ 0, 15 ],
      "id_str" : "20141702",
      "id" : 20141702
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "312958290629898241",
  "geo" : {
  },
  "id_str" : "312959817406885889",
  "in_reply_to_user_id" : 20141702,
  "text" : "@OrangutanClyde What I mean is there's no little buttons within the reader itself for sharing. I have to link hop into Opera",
  "id" : 312959817406885889,
  "in_reply_to_status_id" : 312958290629898241,
  "created_at" : "Sat Mar 16 16:13:29 +0000 2013",
  "in_reply_to_screen_name" : "OrangutanClyde",
  "in_reply_to_user_id_str" : "20141702",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samuel Clyde Jones",
      "screen_name" : "OrangutanClyde",
      "indices" : [ 0, 15 ],
      "id_str" : "20141702",
      "id" : 20141702
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "312958290629898241",
  "geo" : {
  },
  "id_str" : "312959281672634368",
  "in_reply_to_user_id" : 20141702,
  "text" : "@OrangutanClyde I only use it now because of the RSS Reader. How crazy right? I really hope aut-update doesn't set in and ruin it for us all",
  "id" : 312959281672634368,
  "in_reply_to_status_id" : 312958290629898241,
  "created_at" : "Sat Mar 16 16:11:21 +0000 2013",
  "in_reply_to_screen_name" : "OrangutanClyde",
  "in_reply_to_user_id_str" : "20141702",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 138 ],
      "url" : "http://t.co/RXVX6b6NMn",
      "expanded_url" : "http://isharefil.es/NdDD",
      "display_url" : "isharefil.es/NdDD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312956969776455681",
  "text" : "So Opera's RSS Reader is as badass as they come. Only issue is I can't send to Instapaper, and share on Twitter etc http://t.co/RXVX6b6NMn",
  "id" : 312956969776455681,
  "created_at" : "Sat Mar 16 16:02:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http://t.co/c3pzZx9aGI",
      "expanded_url" : "http://isharefil.es/Nd7k",
      "display_url" : "isharefil.es/Nd7k"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312956128654925824",
  "text" : "Just installed a program called Fences for Windows. Let's get sh1t done \u2192 http://t.co/c3pzZx9aGI",
  "id" : 312956128654925824,
  "created_at" : "Sat Mar 16 15:58:50 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 72, 80 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/hnYmkDjgRF",
      "expanded_url" : "http://myshar.es/142Ewqa",
      "display_url" : "myshar.es/142Ewqa"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312937206991953922",
  "text" : "The Google graveyard of closed down services http://t.co/hnYmkDjgRF\u00A0via @codepo8",
  "id" : 312937206991953922,
  "created_at" : "Sat Mar 16 14:43:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.thefancy.com\" rel=\"nofollow\">   Fancy</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fancy",
      "screen_name" : "thefancy",
      "indices" : [ 61, 70 ],
      "id_str" : "1730261",
      "id" : 1730261
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 56 ],
      "url" : "http://t.co/Wvi7cJ45Sa",
      "expanded_url" : "http://fancy.to/qkx7mn",
      "display_url" : "fancy.to/qkx7mn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312790087039586305",
  "text" : "Fountain Pen with Highlighter Ink http://t.co/Wvi7cJ45Sa via @thefancy",
  "id" : 312790087039586305,
  "created_at" : "Sat Mar 16 04:59:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 45, 56 ]
    } ],
    "urls" : [ {
      "indices" : [ 22, 44 ],
      "url" : "http://t.co/Ubh6DGh8eP",
      "expanded_url" : "http://myshar.es/141Akae",
      "display_url" : "myshar.es/141Akae"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312738473167691776",
  "text" : "Tetris in 140 bytes \u2192 http://t.co/Ubh6DGh8eP #javascript",
  "id" : 312738473167691776,
  "created_at" : "Sat Mar 16 01:33:57 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Fever",
      "indices" : [ 58, 64 ]
    }, {
      "text" : "Licorize",
      "indices" : [ 77, 86 ]
    }, {
      "text" : "Buffer",
      "indices" : [ 101, 108 ]
    } ],
    "urls" : [ {
      "indices" : [ 109, 131 ],
      "url" : "http://t.co/4AD42ehb31",
      "expanded_url" : "http://isharefil.es/NaTD",
      "display_url" : "isharefil.es/NaTD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312736360266084353",
  "text" : "Current online sharing workflow \u2192 Read article I found in #Fever. Save it to #Licorize. Tweet it via #Buffer http://t.co/4AD42ehb31",
  "id" : 312736360266084353,
  "created_at" : "Sat Mar 16 01:25:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http://t.co/9KXcQCexEM",
      "expanded_url" : "http://myshar.es/141zlGP",
      "display_url" : "myshar.es/141zlGP"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312735194186657792",
  "text" : "\"The Era of Symbol Fonts\" http://t.co/9KXcQCexEM",
  "id" : 312735194186657792,
  "created_at" : "Sat Mar 16 01:20:55 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http://t.co/4Ng3mIm7ui",
      "expanded_url" : "http://myshar.es/141wT3g",
      "display_url" : "myshar.es/141wT3g"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312728052549427200",
  "text" : "Improving UX Through Front-End Performance http://t.co/4Ng3mIm7ui",
  "id" : 312728052549427200,
  "created_at" : "Sat Mar 16 00:52:32 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 91 ],
      "url" : "http://t.co/b7f86dijn4",
      "expanded_url" : "http://myshar.es/141uPIB",
      "display_url" : "myshar.es/141uPIB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312721716361187329",
  "text" : "Instead of hot-fix patching IE6-8 today try upgrading to IE9 or IE10 http://t.co/b7f86dijn4",
  "id" : 312721716361187329,
  "created_at" : "Sat Mar 16 00:27:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 47, 54 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 46 ],
      "url" : "http://t.co/Ww8gwrA6Px",
      "expanded_url" : "http://myshar.es/157PzKt",
      "display_url" : "myshar.es/157PzKt"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312721199878774784",
  "text" : "CORS isn\u2019t just for XHR http://t.co/Ww8gwrA6Px #webdev",
  "id" : 312721199878774784,
  "created_at" : "Sat Mar 16 00:25:18 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex R. Young",
      "screen_name" : "alex_young",
      "indices" : [ 48, 59 ],
      "id_str" : "8051942",
      "id" : 8051942
    }, {
      "name" : "dailyjs",
      "screen_name" : "dailyjs",
      "indices" : [ 64, 72 ],
      "id_str" : "87709774",
      "id" : 87709774
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 73, 84 ]
    } ],
    "urls" : [ {
      "indices" : [ 85, 107 ],
      "url" : "http://t.co/cQbYMg8qOL",
      "expanded_url" : "http://myshar.es/141uwNT",
      "display_url" : "myshar.es/141uwNT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312720866330963968",
  "text" : "DailyJS: Backbone.js Tutorial: Editing Lists by @alex_young via @dailyjs #javascript http://t.co/cQbYMg8qOL",
  "id" : 312720866330963968,
  "created_at" : "Sat Mar 16 00:23:59 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "feedafever",
      "indices" : [ 42, 53 ]
    } ],
    "urls" : [ {
      "indices" : [ 98, 120 ],
      "url" : "http://t.co/yyo3GtXgQ9",
      "expanded_url" : "http://iwantaneff.in/file/yYC00K9u697yAo5",
      "display_url" : "iwantaneff.in/file/yYC00K9u6\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312671114130235392",
  "text" : "Putting a list of OPML files together for #feedafever Spent about an hour compiling this. Enjoy \u2192 http://t.co/yyo3GtXgQ9",
  "id" : 312671114130235392,
  "created_at" : "Fri Mar 15 21:06:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 55 ],
      "url" : "http://t.co/LIufA4e6XA",
      "expanded_url" : "http://myshar.es/13Q1gtd",
      "display_url" : "myshar.es/13Q1gtd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312650685369942016",
  "text" : "Utility vs. Beauty \u2013 Edan Hewitt http://t.co/LIufA4e6XA",
  "id" : 312650685369942016,
  "created_at" : "Fri Mar 15 19:45:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/312640284230037504/photo/1",
      "indices" : [ 114, 136 ],
      "url" : "http://t.co/zmX9tWeo0n",
      "media_url" : "http://pbs.twimg.com/media/BFa4rpyCMAA2LQb.png",
      "id_str" : "312640284238426112",
      "id" : 312640284238426112,
      "media_url_https" : "https://pbs.twimg.com/media/BFa4rpyCMAA2LQb.png",
      "sizes" : [ {
        "h" : 1040,
        "resize" : "fit",
        "w" : 1068
      }, {
        "h" : 331,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 997,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 584,
        "resize" : "fit",
        "w" : 600
      } ],
      "display_url" : "pic.twitter.com/zmX9tWeo0n"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 100 ],
      "url" : "http://t.co/WeFODmV9si",
      "expanded_url" : "http://wordpress.org/extend/plugins/mp6/",
      "display_url" : "wordpress.org/extend/plugins\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312640284230037504",
  "text" : "Here's a plugin that makes the Wordpress dashboard have a 'flat' / Metro UI \u2192 http://t.co/WeFODmV9si Interesting! http://t.co/zmX9tWeo0n",
  "id" : 312640284230037504,
  "created_at" : "Fri Mar 15 19:03:47 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 55 ],
      "url" : "http://t.co/xmDv03VDz2",
      "expanded_url" : "http://myshar.es/12FRvOT",
      "display_url" : "myshar.es/12FRvOT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312605623646822400",
  "text" : "\"Hackpad is the Best Wiki Ever.\" http://t.co/xmDv03VDz2",
  "id" : 312605623646822400,
  "created_at" : "Fri Mar 15 16:46:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http://t.co/n0s2yovMdS",
      "expanded_url" : "http://myshar.es/YlSCL9",
      "display_url" : "myshar.es/YlSCL9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312535937429147648",
  "text" : "Symbolset - Turn words into icons using font magic http://t.co/n0s2yovMdS",
  "id" : 312535937429147648,
  "created_at" : "Fri Mar 15 12:09:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jQuery",
      "indices" : [ 9, 16 ]
    } ],
    "urls" : [ {
      "indices" : [ 48, 70 ],
      "url" : "http://t.co/fHGHaHVga3",
      "expanded_url" : "http://myshar.es/YlSwmE",
      "display_url" : "myshar.es/YlSwmE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312486844321366016",
  "text" : "Sidr - A #jQuery plugin for creating side menus http://t.co/fHGHaHVga3",
  "id" : 312486844321366016,
  "created_at" : "Fri Mar 15 08:54:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 111, 120 ],
      "id_str" : "14139773",
      "id" : 14139773
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BootstrapCDN",
      "indices" : [ 0, 13 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 106 ],
      "url" : "http://t.co/YsAKFMjGRe",
      "expanded_url" : "http://www.bootstrapcdn.com/pulse.html",
      "display_url" : "bootstrapcdn.com/pulse.html"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312365683222667264",
  "text" : "#BootstrapCDN has just launched Pulse - A better way to view usage of the service \u2192 http://t.co/YsAKFMjGRe via @jdorfman",
  "id" : 312365683222667264,
  "created_at" : "Fri Mar 15 00:52:37 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http://t.co/gc7RjKsJtn",
      "expanded_url" : "http://alxgbsn.co.uk/2013/02/20/notify-js-a-handy-wrapper-for-the-web-notifications-api",
      "display_url" : "alxgbsn.co.uk/2013/02/20/not\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312358992842219520",
  "text" : "Notify.js - A handy wrapper for the Web Notifications\u00A0API http://t.co/gc7RjKsJtn",
  "id" : 312358992842219520,
  "created_at" : "Fri Mar 15 00:26:01 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 56, 64 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Shaun Inman",
      "screen_name" : "shauninman",
      "indices" : [ 65, 76 ],
      "id_str" : "12047992",
      "id" : 12047992
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "feedafever",
      "indices" : [ 40, 51 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 39 ],
      "url" : "http://t.co/jw5pHrZfUG",
      "expanded_url" : "http://isharefil.es/NbT9",
      "display_url" : "isharefil.es/NbT9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312356565904666626",
  "text" : "And we're live \u2192 http://t.co/jw5pHrZfUG #feedafever cc/ @codepo8 @shauninman",
  "id" : 312356565904666626,
  "created_at" : "Fri Mar 15 00:16:23 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shaun Inman",
      "screen_name" : "shauninman",
      "indices" : [ 0, 11 ],
      "id_str" : "12047992",
      "id" : 12047992
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "PING",
      "indices" : [ 57, 62 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "312316993338503169",
  "in_reply_to_user_id" : 12047992,
  "text" : "@shauninman You've got a support E-Mail regarding Fever. #PING",
  "id" : 312316993338503169,
  "created_at" : "Thu Mar 14 21:39:08 +0000 2013",
  "in_reply_to_screen_name" : "shauninman",
  "in_reply_to_user_id_str" : "12047992",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 93 ],
      "url" : "http://t.co/FSSpfVzNV6",
      "expanded_url" : "http://myshar.es/12FRw5c",
      "display_url" : "myshar.es/12FRw5c"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312288388629811200",
  "text" : "Making culture for the internets\u2014all of them \u2014 Editors' Picks \u2014 Medium http://t.co/FSSpfVzNV6",
  "id" : 312288388629811200,
  "created_at" : "Thu Mar 14 19:45:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mr. Reader",
      "screen_name" : "mrreaderapp",
      "indices" : [ 3, 15 ],
      "id_str" : "233593436",
      "id" : 233593436
    }, {
      "name" : "Chan Xin",
      "screen_name" : "chanxin",
      "indices" : [ 17, 25 ],
      "id_str" : "90582376",
      "id" : 90582376
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "312282480084393984",
  "text" : "RT @mrreaderapp: @chanxin Of course I'll support an alternative. But it's impossible to say which one a few hours after the Google info.",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.osfoora.com/mac\" rel=\"nofollow\">Osfoora for Mac</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Chan Xin",
        "screen_name" : "chanxin",
        "indices" : [ 0, 8 ],
        "id_str" : "90582376",
        "id" : 90582376
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "312134295298928642",
    "geo" : {
    },
    "id_str" : "312135105017683968",
    "in_reply_to_user_id" : 90582376,
    "text" : "@chanxin Of course I'll support an alternative. But it's impossible to say which one a few hours after the Google info.",
    "id" : 312135105017683968,
    "in_reply_to_status_id" : 312134295298928642,
    "created_at" : "Thu Mar 14 09:36:22 +0000 2013",
    "in_reply_to_screen_name" : "chanxin",
    "in_reply_to_user_id_str" : "90582376",
    "user" : {
      "name" : "Mr. Reader",
      "screen_name" : "mrreaderapp",
      "protected" : false,
      "id_str" : "233593436",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1272280594/mrreader_flat_450_normal.png",
      "id" : 233593436,
      "verified" : false
    }
  },
  "id" : 312282480084393984,
  "created_at" : "Thu Mar 14 19:21:59 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoogleReader",
      "indices" : [ 15, 28 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "312278960304185345",
  "text" : "Loving all the #GoogleReader alternatives suggested by others in my time-line. If only I could get a refund for Mr Reader on iOS.",
  "id" : 312278960304185345,
  "created_at" : "Thu Mar 14 19:08:00 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 30 ],
      "url" : "http://t.co/uu8GNnUhWf",
      "expanded_url" : "http://buff.ly/pVvzUK",
      "display_url" : "buff.ly/pVvzUK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312243278517002240",
  "text" : "Onswipe http://t.co/uu8GNnUhWf",
  "id" : 312243278517002240,
  "created_at" : "Thu Mar 14 16:46:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "311985545444208642",
  "geo" : {
  },
  "id_str" : "312242140841402368",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 I usually discover great links on Pinboard.in. You have to have a serendipitous browsing of /recent or /popular at least once a day",
  "id" : 312242140841402368,
  "in_reply_to_status_id" : 311985545444208642,
  "created_at" : "Thu Mar 14 16:41:42 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Brondbjerg",
      "screen_name" : "mikebrondbjerg",
      "indices" : [ 3, 18 ],
      "id_str" : "16908692",
      "id" : 16908692
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "312236682596466689",
  "text" : "RT @mikebrondbjerg: @_higg hey, I notice you you favorited my drag &amp; drop UI experiment. FYI this has been removed and replaced with ...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http://t.co/Hwroki1YaK",
        "expanded_url" : "http://www.brondbjerg.co.uk/demos/labs/html5_sequencer/",
        "display_url" : "brondbjerg.co.uk/demos/labs/htm\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "312215265620418561",
    "in_reply_to_user_id" : 468853739,
    "text" : "@_higg hey, I notice you you favorited my drag &amp; drop UI experiment. FYI this has been removed and replaced with http://t.co/Hwroki1YaK",
    "id" : 312215265620418561,
    "created_at" : "Thu Mar 14 14:54:54 +0000 2013",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "Mike Brondbjerg",
      "screen_name" : "mikebrondbjerg",
      "protected" : false,
      "id_str" : "16908692",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/639043599/eggman_normal.jpg",
      "id" : 16908692,
      "verified" : false
    }
  },
  "id" : 312236682596466689,
  "created_at" : "Thu Mar 14 16:20:00 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 47 ],
      "url" : "http://t.co/ISkDtKm4IM",
      "expanded_url" : "http://myshar.es/14KMShK",
      "display_url" : "myshar.es/14KMShK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312173547101515777",
  "text" : "Connecting \u2013 Edan Hewitt http://t.co/ISkDtKm4IM",
  "id" : 312173547101515777,
  "created_at" : "Thu Mar 14 12:09:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 40 ],
      "url" : "http://t.co/yv3JR4nhKw",
      "expanded_url" : "http://myshar.es/12FRBWz",
      "display_url" : "myshar.es/12FRBWz"
    } ]
  },
  "geo" : {
  },
  "id_str" : "312124467893649409",
  "text" : "\"The Amazing Web\" http://t.co/yv3JR4nhKw",
  "id" : 312124467893649409,
  "created_at" : "Thu Mar 14 08:54:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    }, {
      "name" : "Conor Haining",
      "screen_name" : "conhaining",
      "indices" : [ 9, 20 ],
      "id_str" : "79261681",
      "id" : 79261681
    }, {
      "name" : "Calum McAlinden",
      "screen_name" : "calum_mcalinden",
      "indices" : [ 21, 37 ],
      "id_str" : "253135971",
      "id" : 253135971
    }, {
      "name" : "Galen Gidman",
      "screen_name" : "galengidman",
      "indices" : [ 38, 50 ],
      "id_str" : "109966028",
      "id" : 109966028
    }, {
      "name" : "Will Smidlein",
      "screen_name" : "ws",
      "indices" : [ 51, 54 ],
      "id_str" : "18194756",
      "id" : 18194756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "311942210595745796",
  "geo" : {
  },
  "id_str" : "311946548479156224",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn @conhaining @calum_mcalinden @galengidman @ws Not about to dive into that code. Why not use Gravatar? Open Web APIs FTW",
  "id" : 311946548479156224,
  "in_reply_to_status_id" : 311942210595745796,
  "created_at" : "Wed Mar 13 21:07:07 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hipster",
      "indices" : [ 82, 90 ]
    }, {
      "text" : "linebreaks",
      "indices" : [ 91, 102 ]
    } ],
    "urls" : [ {
      "indices" : [ 59, 81 ],
      "url" : "http://t.co/Nb1985ASyl",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    } ]
  },
  "in_reply_to_status_id_str" : "311938943341981696",
  "geo" : {
  },
  "id_str" : "311941133662040064",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar I knew about line break before it was cool. http://t.co/Nb1985ASyl #hipster #linebreaks",
  "id" : 311941133662040064,
  "in_reply_to_status_id" : 311938943341981696,
  "created_at" : "Wed Mar 13 20:45:36 +0000 2013",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u2003\u2003\u2003\u2003\u2003\u2003\u2003\u2003\u2003\u2003\u2003\u2003\u2003 140art",
      "screen_name" : "140Artist",
      "indices" : [ 25, 35 ],
      "id_str" : "72639760",
      "id" : 72639760
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "311940564922798080",
  "text" : "Line breaks\nFTW?\nOr will @140Artist have a field day?",
  "id" : 311940564922798080,
  "created_at" : "Wed Mar 13 20:43:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 56 ],
      "url" : "http://t.co/ivqkdQYlSa",
      "expanded_url" : "http://batshiitinsane.appspot.com/github.com/higgo/the.github.pirates",
      "display_url" : "batshiitinsane.appspot.com/github.com/hig\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311931401949814784",
  "text" : "Another proxy tunnel on Appspot \u2192 http://t.co/ivqkdQYlSa How many have I come across now, like 10? Don't enter login info to these BTW",
  "id" : 311931401949814784,
  "created_at" : "Wed Mar 13 20:06:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 53 ],
      "url" : "http://t.co/fLKqjAR88E",
      "expanded_url" : "http://myshar.es/13Q1dxy",
      "display_url" : "myshar.es/13Q1dxy"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311925921978413056",
  "text" : "CSS3 Ribbon Menu \u2013 Edan Hewitt http://t.co/fLKqjAR88E",
  "id" : 311925921978413056,
  "created_at" : "Wed Mar 13 19:45:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "indices" : [ 3, 14 ],
      "id_str" : "464887553",
      "id" : 464887553
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 77 ],
      "url" : "http://t.co/Sllg3uTSM9",
      "expanded_url" : "http://ednh.ca/13W8Fr2",
      "display_url" : "ednh.ca/13W8Fr2"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311893434485067776",
  "text" : "RT @EdanHewitt: Share localhost over the Web \u2014 Forward http://t.co/Sllg3uTSM9",
  "retweeted_status" : {
    "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 39, 61 ],
        "url" : "http://t.co/Sllg3uTSM9",
        "expanded_url" : "http://ednh.ca/13W8Fr2",
        "display_url" : "ednh.ca/13W8Fr2"
      } ]
    },
    "geo" : {
    },
    "id_str" : "311892196158763008",
    "text" : "Share localhost over the Web \u2014 Forward http://t.co/Sllg3uTSM9",
    "id" : 311892196158763008,
    "created_at" : "Wed Mar 13 17:31:08 +0000 2013",
    "user" : {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "protected" : false,
      "id_str" : "464887553",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3646301696/1956e43caf6c2d6981f2e222e4fbd6ba_normal.png",
      "id" : 464887553,
      "verified" : false
    }
  },
  "id" : 311893434485067776,
  "created_at" : "Wed Mar 13 17:36:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http://t.co/VQ6nApoz9Z",
      "expanded_url" : "http://myshar.es/12FRrOW",
      "display_url" : "myshar.es/12FRrOW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311880969252642817",
  "text" : "Mailappapp \u00B7 Visual Idiot http://t.co/VQ6nApoz9Z",
  "id" : 311880969252642817,
  "created_at" : "Wed Mar 13 16:46:32 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/qnUhC4cIGe",
      "expanded_url" : "http://myshar.es/14KMS1n",
      "display_url" : "myshar.es/14KMS1n"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311811294799990786",
  "text" : "Against chrome: a manifesto \u2013 Edan Hewitt http://t.co/qnUhC4cIGe",
  "id" : 311811294799990786,
  "created_at" : "Wed Mar 13 12:09:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/SaVFSXazZp",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 41, 63 ],
      "url" : "http://t.co/y3pRsR0iKi",
      "expanded_url" : "http://buff.ly/WnrdbW",
      "display_url" : "buff.ly/WnrdbW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311762066027466752",
  "text" : "http://t.co/SaVFSXazZp\u2019s new direction \u2013 http://t.co/y3pRsR0iKi",
  "id" : 311762066027466752,
  "created_at" : "Wed Mar 13 08:54:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Frances Berriman",
      "screen_name" : "phae",
      "indices" : [ 50, 55 ],
      "id_str" : "13255",
      "id" : 13255
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 78 ],
      "url" : "http://t.co/3BpGCD4OLi",
      "expanded_url" : "http://higg.im/twitter/",
      "display_url" : "higg.im/twitter/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311677320786223104",
  "text" : "So here's my ground rules on Twitter, courtesy of @phae http://t.co/3BpGCD4OLi Read and follow them ;)",
  "id" : 311677320786223104,
  "created_at" : "Wed Mar 13 03:17:18 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 53 ],
      "url" : "http://t.co/al9i0ZmeJP",
      "expanded_url" : "http://buff.ly/YlS1sU",
      "display_url" : "buff.ly/YlS1sU"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311563519915065344",
  "text" : "Best captcha is exotic captcha http://t.co/al9i0ZmeJP",
  "id" : 311563519915065344,
  "created_at" : "Tue Mar 12 19:45:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 83 ],
      "url" : "http://t.co/anww9e5D9d",
      "expanded_url" : "http://buff.ly/12FQSos",
      "display_url" : "buff.ly/12FQSos"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311518592824901632",
  "text" : "Content Marketing Tips to Make Content Pop on the Social Web http://t.co/anww9e5D9d",
  "id" : 311518592824901632,
  "created_at" : "Tue Mar 12 16:46:34 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 78 ],
      "url" : "http://t.co/EkCsjLEKkR",
      "expanded_url" : "http://buff.ly/Sh7DkX",
      "display_url" : "buff.ly/Sh7DkX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311448765926109184",
  "text" : "Leaflet - a JavaScript library for mobile-friendly maps http://t.co/EkCsjLEKkR",
  "id" : 311448765926109184,
  "created_at" : "Tue Mar 12 12:09:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 38 ],
      "url" : "http://t.co/Ri2g62gE67",
      "expanded_url" : "http://buff.ly/Wg1a7b",
      "display_url" : "buff.ly/Wg1a7b"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311399682356166657",
  "text" : "webpage capture http://t.co/Ri2g62gE67",
  "id" : 311399682356166657,
  "created_at" : "Tue Mar 12 08:54:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 0, 8 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "311214019430207489",
  "geo" : {
  },
  "id_str" : "311225704790519810",
  "in_reply_to_user_id" : 532923,
  "text" : "@mathias Yeah I submitted a pull request to it.",
  "id" : 311225704790519810,
  "in_reply_to_status_id" : 311214019430207489,
  "created_at" : "Mon Mar 11 21:22:44 +0000 2013",
  "in_reply_to_screen_name" : "mathias",
  "in_reply_to_user_id_str" : "532923",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 116 ],
      "url" : "http://t.co/TePuYT5FEZ",
      "expanded_url" : "http://isharefil.es/NTir",
      "display_url" : "isharefil.es/NTir"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311218599543050240",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert Also, Chrome is giving errors after I unfollow a lot of people: Have a look \u2192 http://t.co/TePuYT5FEZ",
  "id" : 311218599543050240,
  "created_at" : "Mon Mar 11 20:54:30 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "311216492622528512",
  "geo" : {
  },
  "id_str" : "311216938384769024",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert I'm in, and the 1st thing I'm doing is clicking really fast on unfollow buttons. Is there a way to \"Select all\" people?",
  "id" : 311216938384769024,
  "in_reply_to_status_id" : 311216492622528512,
  "created_at" : "Mon Mar 11 20:47:54 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "311216001926709249",
  "geo" : {
  },
  "id_str" : "311216265958150146",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert Gonna try now",
  "id" : 311216265958150146,
  "in_reply_to_status_id" : 311216001926709249,
  "created_at" : "Mon Mar 11 20:45:14 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 0, 8 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 98 ],
      "url" : "http://t.co/U66xN8lNDt",
      "expanded_url" : "http://developer.yahoo.com/performance/rules.html",
      "display_url" : "developer.yahoo.com/performance/ru\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "311193207327293440",
  "geo" : {
  },
  "id_str" : "311213628617531392",
  "in_reply_to_user_id" : 532923,
  "text" : "@mathias It's just a nicely presented and better promoted version of this \u2192 http://t.co/U66xN8lNDt",
  "id" : 311213628617531392,
  "in_reply_to_status_id" : 311193207327293440,
  "created_at" : "Mon Mar 11 20:34:45 +0000 2013",
  "in_reply_to_screen_name" : "mathias",
  "in_reply_to_user_id_str" : "532923",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http://t.co/eAdNZgrqvq",
      "expanded_url" : "http://j.mp/XVL823",
      "display_url" : "j.mp/XVL823"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311201143520894976",
  "text" : "The most complete CSS tools for web designers | CSSmatic http://t.co/eAdNZgrqvq",
  "id" : 311201143520894976,
  "created_at" : "Mon Mar 11 19:45:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 3, 19 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http://t.co/iOMr3qu2If",
      "expanded_url" : "http://codepen.io/vpegado/pen/daugx",
      "display_url" : "codepen.io/vpegado/pen/da\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311188200112672770",
  "text" : "RT @jalbertbowdenii: schweet :focus tricks http://t.co/iOMr3qu2If",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 22, 44 ],
        "url" : "http://t.co/iOMr3qu2If",
        "expanded_url" : "http://codepen.io/vpegado/pen/daugx",
        "display_url" : "codepen.io/vpegado/pen/da\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "311182900236259329",
    "text" : "schweet :focus tricks http://t.co/iOMr3qu2If",
    "id" : 311182900236259329,
    "created_at" : "Mon Mar 11 18:32:39 +0000 2013",
    "user" : {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "protected" : false,
      "id_str" : "14465889",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3695791723/732f20561dd10d78f8cc88ee48d15ffa_normal.png",
      "id" : 14465889,
      "verified" : false
    }
  },
  "id" : 311188200112672770,
  "created_at" : "Mon Mar 11 18:53:43 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "indices" : [ 3, 14 ],
      "id_str" : "464887553",
      "id" : 464887553
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 131 ],
      "url" : "http://t.co/01q8yDmpim",
      "expanded_url" : "http://ednh.ca/13S04pb",
      "display_url" : "ednh.ca/13S04pb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311187825708130304",
  "text" : "RT @EdanHewitt: If you code HTML5, you owe it to the Internet to listen to Brad Hill talk about its security http://t.co/01q8yDmpim",
  "retweeted_status" : {
    "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http://t.co/01q8yDmpim",
        "expanded_url" : "http://ednh.ca/13S04pb",
        "display_url" : "ednh.ca/13S04pb"
      } ]
    },
    "geo" : {
    },
    "id_str" : "311187164098621440",
    "text" : "If you code HTML5, you owe it to the Internet to listen to Brad Hill talk about its security http://t.co/01q8yDmpim",
    "id" : 311187164098621440,
    "created_at" : "Mon Mar 11 18:49:36 +0000 2013",
    "user" : {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "protected" : false,
      "id_str" : "464887553",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3646301696/1956e43caf6c2d6981f2e222e4fbd6ba_normal.png",
      "id" : 464887553,
      "verified" : false
    }
  },
  "id" : 311187825708130304,
  "created_at" : "Mon Mar 11 18:52:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 42 ],
      "url" : "http://t.co/ilO54NLw9e",
      "expanded_url" : "http://buff.ly/XNG4zX",
      "display_url" : "buff.ly/XNG4zX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311156093684436994",
  "text" : "Discreet Twitter UI http://t.co/ilO54NLw9e",
  "id" : 311156093684436994,
  "created_at" : "Mon Mar 11 16:46:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 46 ],
      "url" : "http://t.co/qY7AWUyVNC",
      "expanded_url" : "http://buff.ly/XEDZTN",
      "display_url" : "buff.ly/XEDZTN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311086505110167553",
  "text" : "Solving The Back Button http://t.co/qY7AWUyVNC",
  "id" : 311086505110167553,
  "created_at" : "Mon Mar 11 12:09:37 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 41 ],
      "url" : "http://t.co/PF16eMPgon",
      "expanded_url" : "http://buff.ly/WpFbKk",
      "display_url" : "buff.ly/WpFbKk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "311037286437486592",
  "text" : "Simple user styles http://t.co/PF16eMPgon",
  "id" : 311037286437486592,
  "created_at" : "Mon Mar 11 08:54:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 3, 19 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 107 ],
      "url" : "http://t.co/pX64BojU29",
      "expanded_url" : "http://www.welcometointernettimeout.com/",
      "display_url" : "welcometointernettimeout.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "310869330093015040",
  "text" : "RT @jalbertbowdenii: internet time out, for your friends that still rawk yahoo email http://t.co/pX64BojU29",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 86 ],
        "url" : "http://t.co/pX64BojU29",
        "expanded_url" : "http://www.welcometointernettimeout.com/",
        "display_url" : "welcometointernettimeout.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "310856350504722432",
    "text" : "internet time out, for your friends that still rawk yahoo email http://t.co/pX64BojU29",
    "id" : 310856350504722432,
    "created_at" : "Sun Mar 10 20:55:04 +0000 2013",
    "user" : {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "protected" : false,
      "id_str" : "14465889",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3695791723/732f20561dd10d78f8cc88ee48d15ffa_normal.png",
      "id" : 14465889,
      "verified" : false
    }
  },
  "id" : 310869330093015040,
  "created_at" : "Sun Mar 10 21:46:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http://t.co/j0LxMqAv0m",
      "expanded_url" : "http://buff.ly/WMrGoJ",
      "display_url" : "buff.ly/WMrGoJ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "310838768858980352",
  "text" : "\"Build curated lists of related links with Sets\" http://t.co/j0LxMqAv0m",
  "id" : 310838768858980352,
  "created_at" : "Sun Mar 10 19:45:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ivan Herman",
      "screen_name" : "ivan_herman",
      "indices" : [ 3, 15 ],
      "id_str" : "17270693",
      "id" : 17270693
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 84 ],
      "url" : "http://t.co/h1dgEZ1zq5",
      "expanded_url" : "http://www.w3.org/blog/SW/2013/03/09/linked-data-platform-1-0-draft-published-2/",
      "display_url" : "w3.org/blog/SW/2013/0\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "310453695559122944",
  "text" : "RT @ivan_herman: W3C Linked Data Platform 1.0 Draft Published http://t.co/h1dgEZ1zq5",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.apple.com/\" rel=\"nofollow\">OS X</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 45, 67 ],
        "url" : "http://t.co/h1dgEZ1zq5",
        "expanded_url" : "http://www.w3.org/blog/SW/2013/03/09/linked-data-platform-1-0-draft-published-2/",
        "display_url" : "w3.org/blog/SW/2013/0\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "310432033065287680",
    "text" : "W3C Linked Data Platform 1.0 Draft Published http://t.co/h1dgEZ1zq5",
    "id" : 310432033065287680,
    "created_at" : "Sat Mar 09 16:48:58 +0000 2013",
    "user" : {
      "name" : "Ivan Herman",
      "screen_name" : "ivan_herman",
      "protected" : false,
      "id_str" : "17270693",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/634881838/twitterProfilePhoto_normal.jpg",
      "id" : 17270693,
      "verified" : false
    }
  },
  "id" : 310453695559122944,
  "created_at" : "Sat Mar 09 18:15:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 3, 12 ],
      "id_str" : "14139773",
      "id" : 14139773
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BootstrapCDN",
      "indices" : [ 14, 27 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http://t.co/5vSBwQTsV7",
      "expanded_url" : "http://blog.netdna.com/opensource/bootstrapcdn/bootstrapcdn-gets-a-makeover/",
      "display_url" : "blog.netdna.com/opensource/boo\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "309455675480752128",
  "text" : "RT @jdorfman: #BootstrapCDN Gets a Makeover - http://t.co/5vSBwQTsV7",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BootstrapCDN",
        "indices" : [ 0, 13 ]
      } ],
      "urls" : [ {
        "indices" : [ 32, 54 ],
        "url" : "http://t.co/5vSBwQTsV7",
        "expanded_url" : "http://blog.netdna.com/opensource/bootstrapcdn/bootstrapcdn-gets-a-makeover/",
        "display_url" : "blog.netdna.com/opensource/boo\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "309446731475611648",
    "text" : "#BootstrapCDN Gets a Makeover - http://t.co/5vSBwQTsV7",
    "id" : 309446731475611648,
    "created_at" : "Wed Mar 06 23:33:44 +0000 2013",
    "user" : {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "protected" : false,
      "id_str" : "14139773",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000022089014/6856897bd33cacb205b161437b1b1c0f_normal.png",
      "id" : 14139773,
      "verified" : false
    }
  },
  "id" : 309455675480752128,
  "created_at" : "Thu Mar 07 00:09:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 51 ],
      "url" : "http://t.co/J8c6sNivCE",
      "expanded_url" : "http://theaxx.net/actionpage/",
      "display_url" : "theaxx.net/actionpage/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "309285776691195905",
  "text" : "The Action Page. \u2014 The Axx - http://t.co/J8c6sNivCE",
  "id" : 309285776691195905,
  "created_at" : "Wed Mar 06 12:54:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Young",
      "screen_name" : "rcyou",
      "indices" : [ 133, 139 ],
      "id_str" : "26770448",
      "id" : 26770448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "309063380902244352",
  "text" : "Design is not about making something look good or spectacular it\u2019s about finding the most efficient way to communicate information - @rcyou",
  "id" : 309063380902244352,
  "created_at" : "Tue Mar 05 22:10:26 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "309055990530191360",
  "geo" : {
  },
  "id_str" : "309058496748466176",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Yes that's a wonderful tool. No doubt you'll be using it for your next disruptive blogpost ;)",
  "id" : 309058496748466176,
  "in_reply_to_status_id" : 309055990530191360,
  "created_at" : "Tue Mar 05 21:51:02 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "309037484791189504",
  "geo" : {
  },
  "id_str" : "309042558900764672",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert Hi. Sounds great. My E-mail \u2192 david [at] higg.im",
  "id" : 309042558900764672,
  "in_reply_to_status_id" : 309037484791189504,
  "created_at" : "Tue Mar 05 20:47:42 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "309037293203763200",
  "text" : "Just figured out a way to turn off the Retweets of everyone I'm following. It's like reading the ramblings of a Zen Monk in my timeline.",
  "id" : 309037293203763200,
  "created_at" : "Tue Mar 05 20:26:46 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Otto",
      "screen_name" : "mdo",
      "indices" : [ 3, 7 ],
      "id_str" : "8207832",
      "id" : 8207832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "308964947461808128",
  "text" : "RT @mdo: TIL the technical name for the # symbol is the octothorp.",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/software/tweetbot/mac\" rel=\"nofollow\">Tweetbot for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "308387886867304448",
    "text" : "TIL the technical name for the # symbol is the octothorp.",
    "id" : 308387886867304448,
    "created_at" : "Mon Mar 04 01:26:16 +0000 2013",
    "user" : {
      "name" : "Mark Otto",
      "screen_name" : "mdo",
      "protected" : false,
      "id_str" : "8207832",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1422223766/mdo-upside-down-shades_normal.jpg",
      "id" : 8207832,
      "verified" : false
    }
  },
  "id" : 308964947461808128,
  "created_at" : "Tue Mar 05 15:39:18 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HN Firehose",
      "screen_name" : "hnfirehose",
      "indices" : [ 3, 14 ],
      "id_str" : "213117318",
      "id" : 213117318
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Devlinks",
      "indices" : [ 16, 25 ]
    } ],
    "urls" : [ {
      "indices" : [ 86, 108 ],
      "url" : "http://t.co/gOHncnMX65",
      "expanded_url" : "http://bit.ly/WHPtey",
      "display_url" : "bit.ly/WHPtey"
    } ]
  },
  "geo" : {
  },
  "id_str" : "308704393614745600",
  "text" : "RT @hnfirehose: #Devlinks - A curated roundup of links for the discerning developer:  http://t.co/gOHncnMX65",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitterfeed.com\" rel=\"nofollow\">twitterfeed</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Devlinks",
        "indices" : [ 0, 9 ]
      } ],
      "urls" : [ {
        "indices" : [ 70, 92 ],
        "url" : "http://t.co/gOHncnMX65",
        "expanded_url" : "http://bit.ly/WHPtey",
        "display_url" : "bit.ly/WHPtey"
      } ]
    },
    "geo" : {
    },
    "id_str" : "308696634651451394",
    "text" : "#Devlinks - A curated roundup of links for the discerning developer:  http://t.co/gOHncnMX65",
    "id" : 308696634651451394,
    "created_at" : "Mon Mar 04 21:53:07 +0000 2013",
    "user" : {
      "name" : "HN Firehose",
      "screen_name" : "hnfirehose",
      "protected" : false,
      "id_str" : "213117318",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1218527213/clipartfireservicefirefighterattack_normal.gif",
      "id" : 213117318,
      "verified" : false
    }
  },
  "id" : 308704393614745600,
  "created_at" : "Mon Mar 04 22:23:57 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 0, 8 ],
      "id_str" : "532923",
      "id" : 532923
    }, {
      "name" : "Anywhere",
      "screen_name" : "anywhere",
      "indices" : [ 9, 18 ],
      "id_str" : "9576402",
      "id" : 9576402
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "308663215691276289",
  "geo" : {
  },
  "id_str" : "308673653803986944",
  "in_reply_to_user_id" : 532923,
  "text" : "@mathias @anywhere was good, but always made the page load really slow. It seems the advertising API is what they're focusing on now.",
  "id" : 308673653803986944,
  "in_reply_to_status_id" : 308663215691276289,
  "created_at" : "Mon Mar 04 20:21:48 +0000 2013",
  "in_reply_to_screen_name" : "mathias",
  "in_reply_to_user_id_str" : "532923",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NetDNA Status",
      "screen_name" : "NetDNAStatus",
      "indices" : [ 3, 16 ],
      "id_str" : "989258700",
      "id" : 989258700
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 75 ],
      "url" : "http://t.co/NtTpbufGNe",
      "expanded_url" : "http://goo.gl/C7f1Y",
      "display_url" : "goo.gl/C7f1Y"
    } ]
  },
  "geo" : {
  },
  "id_str" : "308324877750915073",
  "text" : "RT @NetDNAStatus: NetDNA Status Alert: Edge18 Issues http://t.co/NtTpbufGNe",
  "retweeted_status" : {
    "source" : "<a href=\"http://status.netdna.com\" rel=\"nofollow\">NSTT</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 35, 57 ],
        "url" : "http://t.co/NtTpbufGNe",
        "expanded_url" : "http://goo.gl/C7f1Y",
        "display_url" : "goo.gl/C7f1Y"
      } ]
    },
    "geo" : {
    },
    "id_str" : "308312786054836225",
    "text" : "NetDNA Status Alert: Edge18 Issues http://t.co/NtTpbufGNe",
    "id" : 308312786054836225,
    "created_at" : "Sun Mar 03 20:27:51 +0000 2013",
    "user" : {
      "name" : "NetDNA Status",
      "screen_name" : "NetDNAStatus",
      "protected" : false,
      "id_str" : "989258700",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2932139709/fb78677774685617675b04fc8f4b9702_normal.png",
      "id" : 989258700,
      "verified" : false
    }
  },
  "id" : 308324877750915073,
  "created_at" : "Sun Mar 03 21:15:53 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The HTML5 Douche",
      "screen_name" : "html5douche",
      "indices" : [ 3, 15 ],
      "id_str" : "198612383",
      "id" : 198612383
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "308324504034234368",
  "text" : "RT @html5douche: HOW IS COMPETING ON STANDARDS A GOOD THING?\n\nWE SHOULD POOL RESOURCES ON SUPPORTING THEM AND COMPETE ON EVERYTHING ELSE ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/software/tweetbot/mac\" rel=\"nofollow\">Tweetbot for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "308312365752012801",
    "text" : "HOW IS COMPETING ON STANDARDS A GOOD THING?\n\nWE SHOULD POOL RESOURCES ON SUPPORTING THEM AND COMPETE ON EVERYTHING ELSE.\n\nREAD: RIP OPERA.",
    "id" : 308312365752012801,
    "created_at" : "Sun Mar 03 20:26:10 +0000 2013",
    "user" : {
      "name" : "The HTML5 Douche",
      "screen_name" : "html5douche",
      "protected" : false,
      "id_str" : "198612383",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1138106603/66_A_Douchebag_normal.jpg",
      "id" : 198612383,
      "verified" : false
    }
  },
  "id" : 308324504034234368,
  "created_at" : "Sun Mar 03 21:14:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 42 ],
      "url" : "http://t.co/hK2JfORckT",
      "expanded_url" : "http://isharefil.es/NJCy",
      "display_url" : "isharefil.es/NJCy"
    }, {
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/J03efJm9fx",
      "expanded_url" : "http://photos.app.net/3454573/1",
      "display_url" : "photos.app.net/3454573/1"
    } ]
  },
  "geo" : {
  },
  "id_str" : "308274187833835520",
  "text" : "And so it begins... http://t.co/hK2JfORckT \u2014 http://t.co/J03efJm9fx",
  "id" : 308274187833835520,
  "created_at" : "Sun Mar 03 17:54:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http://t.co/VFITSoY3bF",
      "expanded_url" : "http://myshar.es/ZZFrq2",
      "display_url" : "myshar.es/ZZFrq2"
    } ]
  },
  "geo" : {
  },
  "id_str" : "308262584413458432",
  "text" : "Ace - The High Performance Code Editor for the Web http://t.co/VFITSoY3bF",
  "id" : 308262584413458432,
  "created_at" : "Sun Mar 03 17:08:22 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http://t.co/cwR2Qh3HKj",
      "expanded_url" : "http://myshar.es/XMaLpd",
      "display_url" : "myshar.es/XMaLpd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "308262584283447296",
  "text" : "The three levels of HTML5 usage \u00B7 Mathias Bynens http://t.co/cwR2Qh3HKj",
  "id" : 308262584283447296,
  "created_at" : "Sun Mar 03 17:08:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Equum Vidit",
      "screen_name" : "sindresorhus",
      "indices" : [ 0, 13 ],
      "id_str" : "170686450",
      "id" : 170686450
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 88 ],
      "url" : "http://t.co/mdqAQbDmmp",
      "expanded_url" : "http://isharefil.es/NIvI",
      "display_url" : "isharefil.es/NIvI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "308250538942803970",
  "in_reply_to_user_id" : 170686450,
  "text" : "@sindresorhus Now that's what contributing to Open Source means \u2192 http://t.co/mdqAQbDmmp",
  "id" : 308250538942803970,
  "created_at" : "Sun Mar 03 16:20:30 +0000 2013",
  "in_reply_to_screen_name" : "sindresorhus",
  "in_reply_to_user_id_str" : "170686450",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 31 ],
      "url" : "http://t.co/vyXXjSwmnQ",
      "expanded_url" : "http://myshar.es/ZRsfzC",
      "display_url" : "myshar.es/ZRsfzC"
    } ]
  },
  "geo" : {
  },
  "id_str" : "308243644987625473",
  "text" : "RT Photo http://t.co/vyXXjSwmnQ",
  "id" : 308243644987625473,
  "created_at" : "Sun Mar 03 15:53:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CloudFlare",
      "screen_name" : "CloudFlare",
      "indices" : [ 0, 11 ],
      "id_str" : "32499999",
      "id" : 32499999
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "308213859699798016",
  "geo" : {
  },
  "id_str" : "308217576452915202",
  "in_reply_to_user_id" : 32499999,
  "text" : "@CloudFlare Posterous are shutting down soon. You might want to migrate old posts to a new system.",
  "id" : 308217576452915202,
  "in_reply_to_status_id" : 308213859699798016,
  "created_at" : "Sun Mar 03 14:09:31 +0000 2013",
  "in_reply_to_screen_name" : "CloudFlare",
  "in_reply_to_user_id_str" : "32499999",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alvaro Graves",
      "screen_name" : "alvarograves",
      "indices" : [ 0, 13 ],
      "id_str" : "39816942",
      "id" : 39816942
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "technorati",
      "indices" : [ 105, 116 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "307969260624420864",
  "geo" : {
  },
  "id_str" : "307969746542936066",
  "in_reply_to_user_id" : 39816942,
  "text" : "@alvarograves I agree, the replies always astound me. But it only works if you're super connected to the #technorati ;)",
  "id" : 307969746542936066,
  "in_reply_to_status_id" : 307969260624420864,
  "created_at" : "Sat Mar 02 21:44:44 +0000 2013",
  "in_reply_to_screen_name" : "alvarograves",
  "in_reply_to_user_id_str" : "39816942",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alvaro Graves",
      "screen_name" : "alvarograves",
      "indices" : [ 127, 140 ],
      "id_str" : "39816942",
      "id" : 39816942
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SocialSearchEngine",
      "indices" : [ 0, 19 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "307969092097306624",
  "text" : "#SocialSearchEngine Makes me wonder why people ask questions on Twitter. I only use Google to test my Internet connection? cc/ @alvarograves",
  "id" : 307969092097306624,
  "created_at" : "Sat Mar 02 21:42:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Hobbs",
      "screen_name" : "markhobbsdesign",
      "indices" : [ 0, 16 ],
      "id_str" : "35821839",
      "id" : 35821839
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 113 ],
      "url" : "http://t.co/hXoKQ6l91S",
      "expanded_url" : "http://en.wikipedia.org/wiki/Shotgun_debugging",
      "display_url" : "en.wikipedia.org/wiki/Shotgun_d\u2026"
    }, {
      "indices" : [ 116, 138 ],
      "url" : "http://t.co/VHb8QuwyPZ",
      "expanded_url" : "http://www.bugzilla.org/",
      "display_url" : "bugzilla.org"
    } ]
  },
  "in_reply_to_status_id_str" : "307963303429279744",
  "geo" : {
  },
  "id_str" : "307963983049154560",
  "in_reply_to_user_id" : 35821839,
  "text" : "@markhobbsdesign File a bug, or do shotgun programming until you perfect it. Not my issue. http://t.co/hXoKQ6l91S + http://t.co/VHb8QuwyPZ",
  "id" : 307963983049154560,
  "in_reply_to_status_id" : 307963303429279744,
  "created_at" : "Sat Mar 02 21:21:49 +0000 2013",
  "in_reply_to_screen_name" : "markhobbsdesign",
  "in_reply_to_user_id_str" : "35821839",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Visual Idiot",
      "screen_name" : "idiot",
      "indices" : [ 0, 6 ],
      "id_str" : "202571491",
      "id" : 202571491
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 7, 29 ],
      "url" : "http://t.co/SWNQZ3bU3I",
      "expanded_url" : "http://myshar.es/YgRUBA",
      "display_url" : "myshar.es/YgRUBA"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307960706945396736",
  "in_reply_to_user_id" : 202571491,
  "text" : "@idiot http://t.co/SWNQZ3bU3I",
  "id" : 307960706945396736,
  "created_at" : "Sat Mar 02 21:08:48 +0000 2013",
  "in_reply_to_screen_name" : "idiot",
  "in_reply_to_user_id_str" : "202571491",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Visual Idiot",
      "screen_name" : "idiot",
      "indices" : [ 10, 16 ],
      "id_str" : "202571491",
      "id" : 202571491
    }, {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "indices" : [ 91, 102 ],
      "id_str" : "464887553",
      "id" : 464887553
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "307958783253041152",
  "text" : "If me and @idiot joined forces, we could disrupt the Internet thricefold, with the help of @edanhewitt. Yeah that would be a superhero team!",
  "id" : 307958783253041152,
  "created_at" : "Sat Mar 02 21:01:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "browser",
      "indices" : [ 0, 8 ]
    }, {
      "text" : "hacks",
      "indices" : [ 9, 15 ]
    } ],
    "urls" : [ {
      "indices" : [ 16, 38 ],
      "url" : "http://t.co/A3Ju0Mue2D",
      "expanded_url" : "http://myshar.es/15pTfct",
      "display_url" : "myshar.es/15pTfct"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307956909934256130",
  "text" : "#browser #hacks http://t.co/A3Ju0Mue2D",
  "id" : 307956909934256130,
  "created_at" : "Sat Mar 02 20:53:43 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Hobbs",
      "screen_name" : "markhobbsdesign",
      "indices" : [ 0, 16 ],
      "id_str" : "35821839",
      "id" : 35821839
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "307949744020402176",
  "geo" : {
  },
  "id_str" : "307955770681614336",
  "in_reply_to_user_id" : 35821839,
  "text" : "@markhobbsdesign Unless you mess with paddings or margins in the :hover, then I can't see why a :hover pseudo elm. should change anything ;)",
  "id" : 307955770681614336,
  "in_reply_to_status_id" : 307949744020402176,
  "created_at" : "Sat Mar 02 20:49:11 +0000 2013",
  "in_reply_to_screen_name" : "markhobbsdesign",
  "in_reply_to_user_id_str" : "35821839",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Hobbs",
      "screen_name" : "markhobbsdesign",
      "indices" : [ 0, 16 ],
      "id_str" : "35821839",
      "id" : 35821839
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "307953253050638336",
  "geo" : {
  },
  "id_str" : "307955432620707841",
  "in_reply_to_user_id" : 35821839,
  "text" : "@markhobbsdesign Why should a hover pesudo element affect line-height if you set the line-height correctly in the first place?",
  "id" : 307955432620707841,
  "in_reply_to_status_id" : 307953253050638336,
  "created_at" : "Sat Mar 02 20:47:51 +0000 2013",
  "in_reply_to_screen_name" : "markhobbsdesign",
  "in_reply_to_user_id_str" : "35821839",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Hobbs",
      "screen_name" : "markhobbsdesign",
      "indices" : [ 0, 16 ],
      "id_str" : "35821839",
      "id" : 35821839
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "307949744020402176",
  "geo" : {
  },
  "id_str" : "307951782947082240",
  "in_reply_to_user_id" : 35821839,
  "text" : "@markhobbsdesign Experienced WAT?",
  "id" : 307951782947082240,
  "in_reply_to_status_id" : 307949744020402176,
  "created_at" : "Sat Mar 02 20:33:21 +0000 2013",
  "in_reply_to_screen_name" : "markhobbsdesign",
  "in_reply_to_user_id_str" : "35821839",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "140bytes",
      "indices" : [ 0, 9 ]
    }, {
      "text" : "javascript",
      "indices" : [ 10, 21 ]
    } ],
    "urls" : [ {
      "indices" : [ 22, 44 ],
      "url" : "http://t.co/zCZzyccPJq",
      "expanded_url" : "http://myshar.es/VVwCtx",
      "display_url" : "myshar.es/VVwCtx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307946807340707840",
  "text" : "#140bytes #javascript http://t.co/zCZzyccPJq",
  "id" : 307946807340707840,
  "created_at" : "Sat Mar 02 20:13:34 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "307945542892277760",
  "text" : "function(a,b,c){setInterval(function(){for(b=0;b&lt;8;c||(a.innerHTML+='&lt;b&gt;\u2022'),a.childNodes[b].className='b'+b+' o'+(++b-~c)%8);c=-~c},99)}",
  "id" : 307945542892277760,
  "created_at" : "Sat Mar 02 20:08:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 28 ],
      "url" : "http://t.co/SaVFSXazZp",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 29, 51 ],
      "url" : "http://t.co/xYSG4JUH7b",
      "expanded_url" : "http://myshar.es/15pNKL3",
      "display_url" : "myshar.es/15pNKL3"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307940479352332288",
  "text" : "dh on http://t.co/SaVFSXazZp http://t.co/xYSG4JUH7b",
  "id" : 307940479352332288,
  "created_at" : "Sat Mar 02 19:48:26 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chuck Haines",
      "screen_name" : "termleech",
      "indices" : [ 3, 13 ],
      "id_str" : "6749372",
      "id" : 6749372
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "adn",
      "indices" : [ 86, 90 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "307940270945738753",
  "text" : "RT @termleech: So it seems java really is the write once exploit everywhere language. #adn",
  "retweeted_status" : {
    "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "adn",
        "indices" : [ 71, 75 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "307926135021395968",
    "text" : "So it seems java really is the write once exploit everywhere language. #adn",
    "id" : 307926135021395968,
    "created_at" : "Sat Mar 02 18:51:26 +0000 2013",
    "user" : {
      "name" : "Chuck Haines",
      "screen_name" : "termleech",
      "protected" : false,
      "id_str" : "6749372",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1427481105/Code-Monkey-Java-Logo-small_normal.jpg",
      "id" : 6749372,
      "verified" : false
    }
  },
  "id" : 307940270945738753,
  "created_at" : "Sat Mar 02 19:47:36 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Miao",
      "screen_name" : "liumiao",
      "indices" : [ 0, 8 ],
      "id_str" : "9136842",
      "id" : 9136842
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "306307661467619328",
  "geo" : {
  },
  "id_str" : "307940134127534080",
  "in_reply_to_user_id" : 9136842,
  "text" : "@liumiao First 1000? What exactly does that mean?",
  "id" : 307940134127534080,
  "in_reply_to_status_id" : 306307661467619328,
  "created_at" : "Sat Mar 02 19:47:03 +0000 2013",
  "in_reply_to_screen_name" : "liumiao",
  "in_reply_to_user_id_str" : "9136842",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Github",
      "indices" : [ 0, 7 ]
    } ],
    "urls" : [ {
      "indices" : [ 8, 30 ],
      "url" : "http://t.co/Mi7aUfaHFf",
      "expanded_url" : "http://codepirat.es/",
      "display_url" : "codepirat.es"
    }, {
      "indices" : [ 31, 53 ],
      "url" : "http://t.co/HaXEI02ZgM",
      "expanded_url" : "http://myshar.es/ZVBy5a",
      "display_url" : "myshar.es/ZVBy5a"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307919006097092608",
  "text" : "#Github http://t.co/Mi7aUfaHFf http://t.co/HaXEI02ZgM",
  "id" : 307919006097092608,
  "created_at" : "Sat Mar 02 18:23:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 0, 7 ]
    } ],
    "urls" : [ {
      "indices" : [ 8, 30 ],
      "url" : "http://t.co/01eghu0rdt",
      "expanded_url" : "http://iwantaneff.in/repo/",
      "display_url" : "iwantaneff.in/repo/"
    }, {
      "indices" : [ 31, 53 ],
      "url" : "http://t.co/BcfPkdxkxq",
      "expanded_url" : "http://myshar.es/Y5LWAB",
      "display_url" : "myshar.es/Y5LWAB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307919006004834304",
  "text" : "#jquery http://t.co/01eghu0rdt http://t.co/BcfPkdxkxq",
  "id" : 307919006004834304,
  "created_at" : "Sat Mar 02 18:23:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rob Hawkes",
      "screen_name" : "robhawkes",
      "indices" : [ 0, 10 ],
      "id_str" : "14442542",
      "id" : 14442542
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http://t.co/I0mwjRwqAk",
      "expanded_url" : "http://iwantaneff.in/file/b8BV9Trt7Yk9433",
      "display_url" : "iwantaneff.in/file/b8BV9Trt7\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "307903662104117249",
  "geo" : {
  },
  "id_str" : "307912446801235969",
  "in_reply_to_user_id" : 14442542,
  "text" : "@robhawkes Bands like Fluke / Underworld are ideal for coding. I have listened to this at least 1000 times coding http://t.co/I0mwjRwqAk",
  "id" : 307912446801235969,
  "in_reply_to_status_id" : 307903662104117249,
  "created_at" : "Sat Mar 02 17:57:02 +0000 2013",
  "in_reply_to_screen_name" : "robhawkes",
  "in_reply_to_user_id_str" : "14442542",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 117 ],
      "url" : "http://t.co/ti2K7bi5GL",
      "expanded_url" : "http://www.jsdelivr.com/",
      "display_url" : "jsdelivr.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307906270923485186",
  "text" : "jsDelivr - Free CDN for javascript libraries, jQuery plugins, CSS frameworks, Fonts and more \u2192 http://t.co/ti2K7bi5GL",
  "id" : 307906270923485186,
  "created_at" : "Sat Mar 02 17:32:30 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mariano Omar Morales",
      "screen_name" : "theclabs",
      "indices" : [ 0, 9 ],
      "id_str" : "255937037",
      "id" : 255937037
    }, {
      "name" : ".mario",
      "screen_name" : "0x6D6172696F",
      "indices" : [ 10, 23 ],
      "id_str" : "18780838",
      "id" : 18780838
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "307881450986958848",
  "geo" : {
  },
  "id_str" : "307884157256753152",
  "in_reply_to_user_id" : 255937037,
  "text" : "@theclabs @0x6D6172696F Try running it on latest Chrome!",
  "id" : 307884157256753152,
  "in_reply_to_status_id" : 307881450986958848,
  "created_at" : "Sat Mar 02 16:04:37 +0000 2013",
  "in_reply_to_screen_name" : "theclabs",
  "in_reply_to_user_id_str" : "255937037",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : ".mario",
      "screen_name" : "0x6D6172696F",
      "indices" : [ 0, 13 ],
      "id_str" : "18780838",
      "id" : 18780838
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "307834459556810752",
  "geo" : {
  },
  "id_str" : "307866040333918208",
  "in_reply_to_user_id" : 18780838,
  "text" : "@0x6D6172696F Also works \u2192 &lt;svg&gt;&lt;j onload=alert(1)&gt;",
  "id" : 307866040333918208,
  "in_reply_to_status_id" : 307834459556810752,
  "created_at" : "Sat Mar 02 14:52:38 +0000 2013",
  "in_reply_to_screen_name" : "0x6D6172696F",
  "in_reply_to_user_id_str" : "18780838",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "307865796451893248",
  "text" : "&lt;svg&gt;&lt;j\"\\'\"/onload=alert(1)&gt;",
  "id" : 307865796451893248,
  "created_at" : "Sat Mar 02 14:51:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 76 ],
      "url" : "http://t.co/etlami5Hrq",
      "expanded_url" : "http://myshar.es/YT5tbG",
      "display_url" : "myshar.es/YT5tbG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307863464808968194",
  "text" : "How Search Works - The Story \u2013 Inside Search \u2013 Google http://t.co/etlami5Hrq",
  "id" : 307863464808968194,
  "created_at" : "Sat Mar 02 14:42:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 45 ],
      "url" : "http://t.co/Cw8eJ9frfN",
      "expanded_url" : "http://myshar.es/15pkoMA",
      "display_url" : "myshar.es/15pkoMA"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307863464779603969",
  "text" : "Inside Search \u2013 Google http://t.co/Cw8eJ9frfN",
  "id" : 307863464779603969,
  "created_at" : "Sat Mar 02 14:42:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Resig",
      "screen_name" : "jeresig",
      "indices" : [ 3, 11 ],
      "id_str" : "752673",
      "id" : 752673
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 81 ],
      "url" : "http://t.co/Ei5pWVVcIc",
      "expanded_url" : "http://blog.jquery.com/2013/03/01/jquery-2-0-beta-2-released/",
      "display_url" : "blog.jquery.com/2013/03/01/jqu\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307861699652907008",
  "text" : "RT @jeresig: jQuery 2.0 Beta 2 is looking really exciting: http://t.co/Ei5pWVVcIc so small with the new build options!",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 68 ],
        "url" : "http://t.co/Ei5pWVVcIc",
        "expanded_url" : "http://blog.jquery.com/2013/03/01/jquery-2-0-beta-2-released/",
        "display_url" : "blog.jquery.com/2013/03/01/jqu\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "307848889577377792",
    "text" : "jQuery 2.0 Beta 2 is looking really exciting: http://t.co/Ei5pWVVcIc so small with the new build options!",
    "id" : 307848889577377792,
    "created_at" : "Sat Mar 02 13:44:29 +0000 2013",
    "user" : {
      "name" : "John Resig",
      "screen_name" : "jeresig",
      "protected" : false,
      "id_str" : "752673",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3710418056/50890b833c898cab45237a461e873807_normal.jpeg",
      "id" : 752673,
      "verified" : true
    }
  },
  "id" : 307861699652907008,
  "created_at" : "Sat Mar 02 14:35:23 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 129 ],
      "url" : "http://t.co/igDABIvaNl",
      "expanded_url" : "http://relogo.org/",
      "display_url" : "relogo.org"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307577511842304001",
  "text" : "rel=\"logo\" looks like an interesting proposal. I am going to include this in the source of all my pages ;) http://t.co/igDABIvaNl",
  "id" : 307577511842304001,
  "created_at" : "Fri Mar 01 19:46:07 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 60, 70 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 55 ],
      "url" : "http://t.co/NDISxmS3xC",
      "expanded_url" : "http://techcrunch.com/2013/03/01/most-twitter-users-have-now-flocked-to-appdotnet-after-promoted-tweets-prove-far-too-much-hassle-and-bother",
      "display_url" : "techcrunch.com/2013/03/01/mos\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307571272307589121",
  "text" : "Fake URL trolling is very funny: http://t.co/NDISxmS3xC cc/ @film_girl",
  "id" : 307571272307589121,
  "created_at" : "Fri Mar 01 19:21:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael[tm] Smith",
      "screen_name" : "sideshowbarker",
      "indices" : [ 94, 109 ],
      "id_str" : "6251532",
      "id" : 6251532
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 89 ],
      "url" : "http://t.co/mzF6PjI3yn",
      "expanded_url" : "http://someonewhocares.org/hosts",
      "display_url" : "someonewhocares.org/hosts"
    } ]
  },
  "in_reply_to_status_id_str" : "307308271704895489",
  "geo" : {
  },
  "id_str" : "307567475506155520",
  "in_reply_to_user_id" : 381990922,
  "text" : "@jeffblagdon Who needs Do Not Track when we have things like this? http://t.co/mzF6PjI3yn cc/ @sideshowbarker",
  "id" : 307567475506155520,
  "in_reply_to_status_id" : 307308271704895489,
  "created_at" : "Fri Mar 01 19:06:15 +0000 2013",
  "in_reply_to_screen_name" : "blagdon",
  "in_reply_to_user_id_str" : "381990922",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "dustin curtis",
      "screen_name" : "dcurtis",
      "indices" : [ 0, 8 ],
      "id_str" : "9395832",
      "id" : 9395832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 78 ],
      "url" : "http://t.co/SaVFSXazZp",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 117, 140 ],
      "url" : "https://t.co/U1cFYtvNSJ",
      "expanded_url" : "https://alpha.app.net/dh/post/3381729",
      "display_url" : "alpha.app.net/dh/post/3381729"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307565686316736513",
  "in_reply_to_user_id" : 9395832,
  "text" : "@dcurtis RE: \"'I'm going to start posting a lot more to http://t.co/SaVFSXazZp , so you should follow me there\" ...\u2026 https://t.co/U1cFYtvNSJ",
  "id" : 307565686316736513,
  "created_at" : "Fri Mar 01 18:59:08 +0000 2013",
  "in_reply_to_screen_name" : "dcurtis",
  "in_reply_to_user_id_str" : "9395832",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 66, 73 ]
    }, {
      "text" : "plugins",
      "indices" : [ 74, 82 ]
    } ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http://t.co/01eghu0rdt",
      "expanded_url" : "http://iwantaneff.in/repo/",
      "display_url" : "iwantaneff.in/repo/"
    }, {
      "indices" : [ 83, 105 ],
      "url" : "http://t.co/01eghu0rdt",
      "expanded_url" : "http://iwantaneff.in/repo/",
      "display_url" : "iwantaneff.in/repo/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "307541505759928320",
  "text" : "The Handpicked jQuery Plugins Repository \u2192 http://t.co/01eghu0rdt #jquery #plugins http://t.co/01eghu0rdt",
  "id" : 307541505759928320,
  "created_at" : "Fri Mar 01 17:23:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
} ]