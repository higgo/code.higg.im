Grailbird.data.tweets_2013_08 = 
[ {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Tauber",
      "screen_name" : "jtauber",
      "indices" : [ 0, 8 ],
      "id_str" : "1583811",
      "id" : 1583811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http://t.co/llXp8bA8TH",
      "expanded_url" : "http://jtauber.github.io/",
      "display_url" : "jtauber.github.io"
    } ]
  },
  "in_reply_to_status_id_str" : "367404801459359744",
  "geo" : {
  },
  "id_str" : "367405646397718531",
  "in_reply_to_user_id" : 468853739,
  "text" : "@jtauber You could present them like your GH page: http://t.co/llXp8bA8TH",
  "id" : 367405646397718531,
  "in_reply_to_status_id" : 367404801459359744,
  "created_at" : "Tue Aug 13 22:01:46 +0000 2013",
  "in_reply_to_screen_name" : "alphenic",
  "in_reply_to_user_id_str" : "468853739",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Tauber",
      "screen_name" : "jtauber",
      "indices" : [ 0, 8 ],
      "id_str" : "1583811",
      "id" : 1583811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367404178366545920",
  "geo" : {
  },
  "id_str" : "367404801459359744",
  "in_reply_to_user_id" : 1583811,
  "text" : "@jtauber Kind of in similar vein to Twitter's export feature where the tweets look like the Twitter UI",
  "id" : 367404801459359744,
  "in_reply_to_status_id" : 367404178366545920,
  "created_at" : "Tue Aug 13 21:58:25 +0000 2013",
  "in_reply_to_screen_name" : "jtauber",
  "in_reply_to_user_id_str" : "1583811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Tauber",
      "screen_name" : "jtauber",
      "indices" : [ 0, 8 ],
      "id_str" : "1583811",
      "id" : 1583811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367404178366545920",
  "geo" : {
  },
  "id_str" : "367404612669550592",
  "in_reply_to_user_id" : 1583811,
  "text" : "@jtauber Yes, JSON's fine. I would be thankful for a HTML file with the cards there all pretty and styled with CSS too ;)",
  "id" : 367404612669550592,
  "in_reply_to_status_id" : 367404178366545920,
  "created_at" : "Tue Aug 13 21:57:40 +0000 2013",
  "in_reply_to_screen_name" : "jtauber",
  "in_reply_to_user_id_str" : "1583811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Tauber",
      "screen_name" : "jtauber",
      "indices" : [ 0, 8 ],
      "id_str" : "1583811",
      "id" : 1583811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367403458942730240",
  "geo" : {
  },
  "id_str" : "367403817861517313",
  "in_reply_to_user_id" : 1583811,
  "text" : "@jtauber For now it's okay but I have to isolate each post locally in a text file. I prefer to back-up on a regular basis (each month or so)",
  "id" : 367403817861517313,
  "in_reply_to_status_id" : 367403458942730240,
  "created_at" : "Tue Aug 13 21:54:30 +0000 2013",
  "in_reply_to_screen_name" : "jtauber",
  "in_reply_to_user_id_str" : "1583811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Tauber",
      "screen_name" : "jtauber",
      "indices" : [ 0, 8 ],
      "id_str" : "1583811",
      "id" : 1583811
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OwnYourData",
      "indices" : [ 25, 37 ]
    } ],
    "urls" : [ {
      "indices" : [ 38, 60 ],
      "url" : "http://t.co/4MJeJo08rP",
      "expanded_url" : "http://indiewebcamp.com/2013",
      "display_url" : "indiewebcamp.com/2013"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367403016501993472",
  "in_reply_to_user_id" : 1583811,
  "text" : "@jtauber More reading on #OwnYourData http://t.co/4MJeJo08rP",
  "id" : 367403016501993472,
  "created_at" : "Tue Aug 13 21:51:19 +0000 2013",
  "in_reply_to_screen_name" : "jtauber",
  "in_reply_to_user_id_str" : "1583811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Tauber",
      "screen_name" : "jtauber",
      "indices" : [ 0, 8 ],
      "id_str" : "1583811",
      "id" : 1583811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367402341869162496",
  "in_reply_to_user_id" : 1583811,
  "text" : "@jtauber I know I paid, and there's no ADs etc but I am thinking long term with Thoughtstreams. What's your long term plan for it?",
  "id" : 367402341869162496,
  "created_at" : "Tue Aug 13 21:48:38 +0000 2013",
  "in_reply_to_screen_name" : "jtauber",
  "in_reply_to_user_id_str" : "1583811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Tauber",
      "screen_name" : "jtauber",
      "indices" : [ 0, 8 ],
      "id_str" : "1583811",
      "id" : 1583811
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ownyourdata",
      "indices" : [ 53, 65 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367402160209666048",
  "in_reply_to_user_id" : 1583811,
  "text" : "@jtauber My only problem with TS is I believe in the #ownyourdata idea, and want an export feature so I can migrate if it gets shut down",
  "id" : 367402160209666048,
  "created_at" : "Tue Aug 13 21:47:55 +0000 2013",
  "in_reply_to_screen_name" : "jtauber",
  "in_reply_to_user_id_str" : "1583811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Tauber",
      "screen_name" : "jtauber",
      "indices" : [ 0, 8 ],
      "id_str" : "1583811",
      "id" : 1583811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367401388667830272",
  "geo" : {
  },
  "id_str" : "367401664967241729",
  "in_reply_to_user_id" : 1583811,
  "text" : "@jtauber Yeah sorry, I didn't look hard enough. The service is a great idea by the way",
  "id" : 367401664967241729,
  "in_reply_to_status_id" : 367401388667830272,
  "created_at" : "Tue Aug 13 21:45:57 +0000 2013",
  "in_reply_to_screen_name" : "jtauber",
  "in_reply_to_user_id_str" : "1583811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Tauber",
      "screen_name" : "jtauber",
      "indices" : [ 0, 8 ],
      "id_str" : "1583811",
      "id" : 1583811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https://t.co/1Gdfh12oWp",
      "expanded_url" : "https://thoughtstreams.io/higgins/twitter-tldr-alternatives/",
      "display_url" : "thoughtstreams.io/higgins/twitte\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "366930727595556864",
  "geo" : {
  },
  "id_str" : "367399616737579010",
  "in_reply_to_user_id" : 1583811,
  "text" : "@jtauber Oh I see. Testing now with this: https://t.co/1Gdfh12oWp",
  "id" : 367399616737579010,
  "in_reply_to_status_id" : 366930727595556864,
  "created_at" : "Tue Aug 13 21:37:49 +0000 2013",
  "in_reply_to_screen_name" : "jtauber",
  "in_reply_to_user_id_str" : "1583811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Heliom Inc.",
      "screen_name" : "heliomqc",
      "indices" : [ 0, 9 ],
      "id_str" : "476432160",
      "id" : 476432160
    }, {
      "name" : "Tristan L\u2019Abb\u00E9",
      "screen_name" : "_Tristan",
      "indices" : [ 10, 19 ],
      "id_str" : "18424229",
      "id" : 18424229
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https://t.co/BDWSVltXg8",
      "expanded_url" : "https://twitter.com/favehigg/status/367342434188918784",
      "display_url" : "twitter.com/favehigg/statu\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "365174760553971712",
  "geo" : {
  },
  "id_str" : "367395173208051712",
  "in_reply_to_user_id" : 476432160,
  "text" : "@heliomqc @_Tristan https://t.co/BDWSVltXg8",
  "id" : 367395173208051712,
  "in_reply_to_status_id" : 365174760553971712,
  "created_at" : "Tue Aug 13 21:20:09 +0000 2013",
  "in_reply_to_screen_name" : "heliomqc",
  "in_reply_to_user_id_str" : "476432160",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ChrisUeland",
      "screen_name" : "ChrisUeland",
      "indices" : [ 3, 15 ],
      "id_str" : "54479903",
      "id" : 54479903
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367377339803770880",
  "text" : "RT @ChrisUeland: SpeedAwarenessMonth Article Round Up http://t.co/8pjXXl1M9n",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 37, 59 ],
        "url" : "http://t.co/8pjXXl1M9n",
        "expanded_url" : "http://blog.netdna.com/maxcdn/speedawarenessmonth-article-round-up/",
        "display_url" : "blog.netdna.com/maxcdn/speedaw\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "367376324542742528",
    "text" : "SpeedAwarenessMonth Article Round Up http://t.co/8pjXXl1M9n",
    "id" : 367376324542742528,
    "created_at" : "Tue Aug 13 20:05:15 +0000 2013",
    "user" : {
      "name" : "ChrisUeland",
      "screen_name" : "ChrisUeland",
      "protected" : false,
      "id_str" : "54479903",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1560398234/mirror_normal.jpg",
      "id" : 54479903,
      "verified" : false
    }
  },
  "id" : 367377339803770880,
  "created_at" : "Tue Aug 13 20:09:18 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 61 ],
      "url" : "http://t.co/r6sgiwas94",
      "expanded_url" : "http://youtu.be/N2vARzvWxwY",
      "display_url" : "youtu.be/N2vARzvWxwY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367376231441760258",
  "text" : "Smartphone pictures pose privacy risks http://t.co/r6sgiwas94 The geek in me screams in me turn off EXIF but sadly everyday joe wont do that",
  "id" : 367376231441760258,
  "created_at" : "Tue Aug 13 20:04:53 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dries Vints",
      "screen_name" : "DriesVints",
      "indices" : [ 0, 11 ],
      "id_str" : "226496901",
      "id" : 226496901
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367368492728975360",
  "geo" : {
  },
  "id_str" : "367368844647481344",
  "in_reply_to_user_id" : 226496901,
  "text" : "@DriesVints &lt;span style=\"text-transform:uppercase\"&gt;Thanks for the tip!&lt;/span&gt;",
  "id" : 367368844647481344,
  "in_reply_to_status_id" : 367368492728975360,
  "created_at" : "Tue Aug 13 19:35:32 +0000 2013",
  "in_reply_to_screen_name" : "DriesVints",
  "in_reply_to_user_id_str" : "226496901",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dries Vints",
      "screen_name" : "DriesVints",
      "indices" : [ 3, 14 ],
      "id_str" : "226496901",
      "id" : 226496901
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Tip",
      "indices" : [ 16, 20 ]
    }, {
      "text" : "uppercase",
      "indices" : [ 63, 73 ]
    }, {
      "text" : "CSS",
      "indices" : [ 111, 115 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367368633300684800",
  "text" : "RT @DriesVints: #Tip: if you explicitly want to make your text #uppercase for styling purposes then please use #CSS and don\u2019t type your wor\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/software/tweetbot/mac\" rel=\"nofollow\">Tweetbot for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Tip",
        "indices" : [ 0, 4 ]
      }, {
        "text" : "uppercase",
        "indices" : [ 47, 57 ]
      }, {
        "text" : "CSS",
        "indices" : [ 95, 99 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "367368492728975360",
    "text" : "#Tip: if you explicitly want to make your text #uppercase for styling purposes then please use #CSS and don\u2019t type your words in uppercase.",
    "id" : 367368492728975360,
    "created_at" : "Tue Aug 13 19:34:08 +0000 2013",
    "user" : {
      "name" : "Dries Vints",
      "screen_name" : "DriesVints",
      "protected" : false,
      "id_str" : "226496901",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000261478615/2f3e3f3e6024a18f4156c993458f46b8_normal.jpeg",
      "id" : 226496901,
      "verified" : false
    }
  },
  "id" : 367368633300684800,
  "created_at" : "Tue Aug 13 19:34:42 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morten Siebuhr",
      "screen_name" : "msiebuhr",
      "indices" : [ 1, 10 ],
      "id_str" : "173061455",
      "id" : 173061455
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 34 ],
      "url" : "https://t.co/sv0TjRj0BV",
      "expanded_url" : "https://twitter.com/alphenic/status/367361005698813952",
      "display_url" : "twitter.com/alphenic/statu\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367362216598577153",
  "text" : "+@msiebuhr https://t.co/sv0TjRj0BV",
  "id" : 367362216598577153,
  "created_at" : "Tue Aug 13 19:09:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "john holt ripley",
      "screen_name" : "johnholtripley",
      "indices" : [ 0, 15 ],
      "id_str" : "305202261",
      "id" : 305202261
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https://t.co/kGdHIigki2",
      "expanded_url" : "https://github.com/higgo/higgo.github.com/blob/master/index.html#L255",
      "display_url" : "github.com/higgo/higgo.gi\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "367361408544948224",
  "geo" : {
  },
  "id_str" : "367361716109066241",
  "in_reply_to_user_id" : 305202261,
  "text" : "@johnholtripley Look: https://t.co/kGdHIigki2 ;)",
  "id" : 367361716109066241,
  "in_reply_to_status_id" : 367361408544948224,
  "created_at" : "Tue Aug 13 19:07:13 +0000 2013",
  "in_reply_to_screen_name" : "johnholtripley",
  "in_reply_to_user_id_str" : "305202261",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "john holt ripley",
      "screen_name" : "johnholtripley",
      "indices" : [ 0, 15 ],
      "id_str" : "305202261",
      "id" : 305202261
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367361408544948224",
  "geo" : {
  },
  "id_str" : "367361527516381184",
  "in_reply_to_user_id" : 305202261,
  "text" : "@johnholtripley Yeah I know it well!",
  "id" : 367361527516381184,
  "in_reply_to_status_id" : 367361408544948224,
  "created_at" : "Tue Aug 13 19:06:28 +0000 2013",
  "in_reply_to_screen_name" : "johnholtripley",
  "in_reply_to_user_id_str" : "305202261",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/AM6YuXyqKR",
      "expanded_url" : "http://charcod.es/",
      "display_url" : "charcod.es"
    }, {
      "indices" : [ 25, 47 ],
      "url" : "http://t.co/GBYcabsJXA",
      "expanded_url" : "http://unicod.es/",
      "display_url" : "unicod.es"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367361005698813952",
  "text" : "http://t.co/AM6YuXyqKR + http://t.co/GBYcabsJXA A match made in heaven \u2665",
  "id" : 367361005698813952,
  "created_at" : "Tue Aug 13 19:04:23 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "indices" : [ 3, 12 ],
      "id_str" : "606610405",
      "id" : 606610405
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367351995402682369",
  "text" : "RT @favehigg: http://t.co/IPHJKlAymB",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 0, 22 ],
        "url" : "http://t.co/IPHJKlAymB",
        "expanded_url" : "http://f.cl.ly/items/0Q1b290t0j250q383F30/-ot-crew.html",
        "display_url" : "f.cl.ly/items/0Q1b290t\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "367350426611363840",
    "text" : "http://t.co/IPHJKlAymB",
    "id" : 367350426611363840,
    "created_at" : "Tue Aug 13 18:22:21 +0000 2013",
    "user" : {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "protected" : false,
      "id_str" : "606610405",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000007208981/fee678a7c91e39839aa9ca27489b1b1f_normal.png",
      "id" : 606610405,
      "verified" : false
    }
  },
  "id" : 367351995402682369,
  "created_at" : "Tue Aug 13 18:28:35 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 50 ],
      "url" : "http://t.co/ji7MK16EEC",
      "expanded_url" : "http://example.com/.DS_Store",
      "display_url" : "example.com/.DS_Store"
    }, {
      "indices" : [ 51, 73 ],
      "url" : "http://t.co/EbuSc2fwY6",
      "expanded_url" : "http://example.com/thumbs.db",
      "display_url" : "example.com/thumbs.db"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367347330283880449",
  "text" : "Always a funny experience:\n\nhttp://t.co/ji7MK16EEC\nhttp://t.co/EbuSc2fwY6\n\nFeel free to scan for these on other domains.",
  "id" : 367347330283880449,
  "created_at" : "Tue Aug 13 18:10:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Web Tools Weekly",
      "screen_name" : "WebToolsWeekly",
      "indices" : [ 1, 16 ],
      "id_str" : "1158519540",
      "id" : 1158519540
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 115 ],
      "url" : "http://t.co/ENOg0L9Ofi",
      "expanded_url" : "http://myshar.es/13hJhgl",
      "display_url" : "myshar.es/13hJhgl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367340265230827520",
  "text" : ".@WebToolsWeekly is a front-end development and web design newsletter with a focus on tools. http://t.co/ENOg0L9Ofi",
  "id" : 367340265230827520,
  "created_at" : "Tue Aug 13 17:41:58 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367339822798880768",
  "geo" : {
  },
  "id_str" : "367340085223489536",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert Oh I see. Post a screenshot, or paste the text. What's there. Do tell!",
  "id" : 367340085223489536,
  "in_reply_to_status_id" : 367339822798880768,
  "created_at" : "Tue Aug 13 17:41:15 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https://t.co/GmPV6KVBPO",
      "expanded_url" : "https://balloon.io/",
      "display_url" : "balloon.io"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367334102690566144",
  "text" : "Really like Balloon. https://t.co/GmPV6KVBPO Never liked creating forms.",
  "id" : 367334102690566144,
  "created_at" : "Tue Aug 13 17:17:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 49 ],
      "url" : "http://t.co/AqLJDzRePL",
      "expanded_url" : "http://devour.com/",
      "display_url" : "devour.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367333954640019456",
  "text" : "There goes my whole night: http://t.co/AqLJDzRePL",
  "id" : 367333954640019456,
  "created_at" : "Tue Aug 13 17:16:54 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Klinnert",
      "screen_name" : "damienklinnert",
      "indices" : [ 0, 15 ],
      "id_str" : "276581825",
      "id" : 276581825
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 44 ],
      "url" : "http://t.co/4m9aS7vzcJ",
      "expanded_url" : "http://isharefil.es/Qo4s",
      "display_url" : "isharefil.es/Qo4s"
    } ]
  },
  "in_reply_to_status_id_str" : "367233698095464448",
  "geo" : {
  },
  "id_str" : "367305858960474112",
  "in_reply_to_user_id" : 276581825,
  "text" : "@damienklinnert What? http://t.co/4m9aS7vzcJ",
  "id" : 367305858960474112,
  "in_reply_to_status_id" : 367233698095464448,
  "created_at" : "Tue Aug 13 15:25:15 +0000 2013",
  "in_reply_to_screen_name" : "damienklinnert",
  "in_reply_to_user_id_str" : "276581825",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Crazy Egg",
      "screen_name" : "CrazyEgg",
      "indices" : [ 4, 13 ],
      "id_str" : "2499341",
      "id" : 2499341
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 102 ],
      "url" : "http://t.co/ZUDKJ0os88",
      "expanded_url" : "http://isharefil.es/QmqW/o",
      "display_url" : "isharefil.es/QmqW/o"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367116639625416707",
  "text" : "The @crazyegg heatmap for my homepage over the last few days. Very interesting! http://t.co/ZUDKJ0os88",
  "id" : 367116639625416707,
  "created_at" : "Tue Aug 13 02:53:22 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "indices" : [ 3, 14 ],
      "id_str" : "464887553",
      "id" : 464887553
    }, {
      "name" : "pineapple.io",
      "screen_name" : "pineappleio",
      "indices" : [ 34, 46 ],
      "id_str" : "711006900",
      "id" : 711006900
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "dev",
      "indices" : [ 118, 122 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367111265686007808",
  "text" : "RT @EdanHewitt: Hacker Links! cc/ @pineappleio https://t.co/ouZqXtxK5B Note: Opinionated list. http://t.co/K9lZQ5dfZa #dev",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "pineapple.io",
        "screen_name" : "pineappleio",
        "indices" : [ 18, 30 ],
        "id_str" : "711006900",
        "id" : 711006900
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "dev",
        "indices" : [ 102, 106 ]
      } ],
      "urls" : [ {
        "indices" : [ 31, 54 ],
        "url" : "https://t.co/ouZqXtxK5B",
        "expanded_url" : "https://twitter.com/alphenic/status/367109900842696707",
        "display_url" : "twitter.com/alphenic/statu\u2026"
      }, {
        "indices" : [ 79, 101 ],
        "url" : "http://t.co/K9lZQ5dfZa",
        "expanded_url" : "http://pineapple.io/resources/hacker-links-github-repo",
        "display_url" : "pineapple.io/resources/hack\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "367111169661603845",
    "text" : "Hacker Links! cc/ @pineappleio https://t.co/ouZqXtxK5B Note: Opinionated list. http://t.co/K9lZQ5dfZa #dev",
    "id" : 367111169661603845,
    "created_at" : "Tue Aug 13 02:31:38 +0000 2013",
    "user" : {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "protected" : false,
      "id_str" : "464887553",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3646301696/1956e43caf6c2d6981f2e222e4fbd6ba_normal.png",
      "id" : 464887553,
      "verified" : false
    }
  },
  "id" : 367111265686007808,
  "created_at" : "Tue Aug 13 02:32:01 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "indices" : [ 45, 56 ],
      "id_str" : "464887553",
      "id" : 464887553
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https://t.co/vol1Yjogcp",
      "expanded_url" : "https://github.com/edanhewitt/hacker-links",
      "display_url" : "github.com/edanhewitt/hac\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367109900842696707",
  "text" : "Fresh out of the oven. Hacker links Repo via @edanhewitt https://t.co/vol1Yjogcp\n\nPlenty to feast on for all you weirdos and tinkerers",
  "id" : 367109900842696707,
  "created_at" : "Tue Aug 13 02:26:35 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http://t.co/qXnT00pRPA",
      "expanded_url" : "http://bd808.com/blog/",
      "display_url" : "bd808.com/blog/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367102111445557248",
  "text" : "Hacking the Github activity graph to spell out arbitrary words: http://t.co/qXnT00pRPA Awesome!",
  "id" : 367102111445557248,
  "created_at" : "Tue Aug 13 01:55:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damon Oehlman",
      "screen_name" : "DamonOehlman",
      "indices" : [ 0, 13 ],
      "id_str" : "22446676",
      "id" : 22446676
    }, {
      "name" : "GitHub",
      "screen_name" : "github",
      "indices" : [ 14, 21 ],
      "id_str" : "13334762",
      "id" : 13334762
    }, {
      "name" : "Equum Vidit",
      "screen_name" : "sindresorhus",
      "indices" : [ 22, 35 ],
      "id_str" : "170686450",
      "id" : 170686450
    }, {
      "name" : "will leinweber",
      "screen_name" : "leinweber",
      "indices" : [ 97, 107 ],
      "id_str" : "7524442",
      "id" : 7524442
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https://t.co/0CywDxiWyX",
      "expanded_url" : "https://github.com/will",
      "display_url" : "github.com/will"
    } ]
  },
  "in_reply_to_status_id_str" : "367097730843160579",
  "geo" : {
  },
  "id_str" : "367098451076452354",
  "in_reply_to_user_id" : 468853739,
  "text" : "@DamonOehlman @github @sindresorhus Also: Look - He spelt out his name: https://t.co/0CywDxiWyX +@leinweber LOL",
  "id" : 367098451076452354,
  "in_reply_to_status_id" : 367097730843160579,
  "created_at" : "Tue Aug 13 01:41:05 +0000 2013",
  "in_reply_to_screen_name" : "alphenic",
  "in_reply_to_user_id_str" : "468853739",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damon Oehlman",
      "screen_name" : "DamonOehlman",
      "indices" : [ 0, 13 ],
      "id_str" : "22446676",
      "id" : 22446676
    }, {
      "name" : "GitHub",
      "screen_name" : "github",
      "indices" : [ 14, 21 ],
      "id_str" : "13334762",
      "id" : 13334762
    }, {
      "name" : "Equum Vidit",
      "screen_name" : "sindresorhus",
      "indices" : [ 22, 35 ],
      "id_str" : "170686450",
      "id" : 170686450
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 70 ],
      "url" : "http://t.co/dZOxrzxQwJ",
      "expanded_url" : "http://bd808.com/blog/2013/04/17/hacking-github-contributions-graph/",
      "display_url" : "bd808.com/blog/2013/04/1\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "367097368983769088",
  "geo" : {
  },
  "id_str" : "367097730843160579",
  "in_reply_to_user_id" : 22446676,
  "text" : "@DamonOehlman @github @sindresorhus Check this: http://t.co/dZOxrzxQwJ Seems great minds think alike",
  "id" : 367097730843160579,
  "in_reply_to_status_id" : 367097368983769088,
  "created_at" : "Tue Aug 13 01:38:14 +0000 2013",
  "in_reply_to_screen_name" : "DamonOehlman",
  "in_reply_to_user_id_str" : "22446676",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damon Oehlman",
      "screen_name" : "DamonOehlman",
      "indices" : [ 0, 13 ],
      "id_str" : "22446676",
      "id" : 22446676
    }, {
      "name" : "GitHub",
      "screen_name" : "github",
      "indices" : [ 14, 21 ],
      "id_str" : "13334762",
      "id" : 13334762
    }, {
      "name" : "Equum Vidit",
      "screen_name" : "sindresorhus",
      "indices" : [ 97, 110 ],
      "id_str" : "170686450",
      "id" : 170686450
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https://t.co/9lCCaqXInc",
      "expanded_url" : "https://github.com/sindresorhus",
      "display_url" : "github.com/sindresorhus"
    } ]
  },
  "in_reply_to_status_id_str" : "367093927620255745",
  "geo" : {
  },
  "id_str" : "367095697809485826",
  "in_reply_to_user_id" : 22446676,
  "text" : "@DamonOehlman @github Also, first person to have it saturated in green wins an Internet. Look at @sindresorhus: https://t.co/9lCCaqXInc",
  "id" : 367095697809485826,
  "in_reply_to_status_id" : 367093927620255745,
  "created_at" : "Tue Aug 13 01:30:09 +0000 2013",
  "in_reply_to_screen_name" : "DamonOehlman",
  "in_reply_to_user_id_str" : "22446676",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http://t.co/scj2rGR9Fb",
      "expanded_url" : "http://ifttt.com/",
      "display_url" : "ifttt.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367092369872924672",
  "text" : "The universe is run on a super-advanced version of http://t.co/scj2rGR9Fb",
  "id" : 367092369872924672,
  "created_at" : "Tue Aug 13 01:16:55 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "indices" : [ 0, 13 ],
      "id_str" : "154206772",
      "id" : 154206772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367082068129636354",
  "in_reply_to_user_id" : 154206772,
  "text" : "@a_antonellis I want to favorite every one of your tweets. You're such a child of The Internet.",
  "id" : 367082068129636354,
  "created_at" : "Tue Aug 13 00:35:59 +0000 2013",
  "in_reply_to_screen_name" : "a_antonellis",
  "in_reply_to_user_id_str" : "154206772",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "indices" : [ 3, 16 ],
      "id_str" : "154206772",
      "id" : 154206772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367080660537999360",
  "text" : "RT @a_antonellis: some people arent as cool as their website",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "367079861812076544",
    "text" : "some people arent as cool as their website",
    "id" : 367079861812076544,
    "created_at" : "Tue Aug 13 00:27:13 +0000 2013",
    "user" : {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "protected" : false,
      "id_str" : "154206772",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1904673216/cmy_normal.gif",
      "id" : 154206772,
      "verified" : false
    }
  },
  "id" : 367080660537999360,
  "created_at" : "Tue Aug 13 00:30:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 0, 10 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367078813697851392",
  "geo" : {
  },
  "id_str" : "367079819861049344",
  "in_reply_to_user_id" : 9866582,
  "text" : "@film_girl Oh you mean the hybrid tablet/pc thing?",
  "id" : 367079819861049344,
  "in_reply_to_status_id" : 367078813697851392,
  "created_at" : "Tue Aug 13 00:27:03 +0000 2013",
  "in_reply_to_screen_name" : "film_girl",
  "in_reply_to_user_id_str" : "9866582",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 0, 10 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367078813697851392",
  "geo" : {
  },
  "id_str" : "367079350493249536",
  "in_reply_to_user_id" : 9866582,
  "text" : "@film_girl What, hybrid drive? Yuck, stick with pure SSD all the way. No  sluggish mechanical drives for me thanks.",
  "id" : 367079350493249536,
  "in_reply_to_status_id" : 367078813697851392,
  "created_at" : "Tue Aug 13 00:25:11 +0000 2013",
  "in_reply_to_screen_name" : "film_girl",
  "in_reply_to_user_id_str" : "9866582",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 0, 10 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367075844566835200",
  "geo" : {
  },
  "id_str" : "367078705614839808",
  "in_reply_to_user_id" : 9866582,
  "text" : "@film_girl Is the HD and SSD? Win8 updates go 100 times faster when you have an SSD. I have an SSD, and this is where I gloat... :p",
  "id" : 367078705614839808,
  "in_reply_to_status_id" : 367075844566835200,
  "created_at" : "Tue Aug 13 00:22:38 +0000 2013",
  "in_reply_to_screen_name" : "film_girl",
  "in_reply_to_user_id_str" : "9866582",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http://t.co/Mv1Dgjustl",
      "expanded_url" : "http://www.forbes.com/sites/kashmirhill/2013/08/12/every-important-person-in-bitcoin-just-got-subpoenaed-by-new-yorks-financial-regulator/",
      "display_url" : "forbes.com/sites/kashmirh\u2026"
    }, {
      "indices" : [ 81, 103 ],
      "url" : "http://t.co/nzyScL7l9b",
      "expanded_url" : "http://monocle.io/posts/every-important-person-in-bitcoin-just-got-subpoenaed-by-new-york-s-financial-regulator",
      "display_url" : "monocle.io/posts/every-im\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367077706430951424",
  "text" : "So The Man is regulating our digital currency. Not cool! http://t.co/Mv1Dgjustl (http://t.co/nzyScL7l9b)\u201D",
  "id" : 367077706430951424,
  "created_at" : "Tue Aug 13 00:18:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Julianna Grogan",
      "screen_name" : "MAKEUPARTISTJG",
      "indices" : [ 0, 15 ],
      "id_str" : "526343785",
      "id" : 526343785
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367042949240328192",
  "in_reply_to_user_id" : 526343785,
  "text" : "@MAKEUPARTISTJG Love the site Julie!",
  "id" : 367042949240328192,
  "created_at" : "Mon Aug 12 22:00:33 +0000 2013",
  "in_reply_to_screen_name" : "MAKEUPARTISTJG",
  "in_reply_to_user_id_str" : "526343785",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Henzel",
      "screen_name" : "davidhenzel",
      "indices" : [ 3, 15 ],
      "id_str" : "98993727",
      "id" : 98993727
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Adobe",
      "indices" : [ 55, 61 ]
    }, {
      "text" : "Illustrator",
      "indices" : [ 62, 74 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367041202803466240",
  "text" : "RT @davidhenzel: Looking for somebody who is good with #Adobe #Illustrator for a quick job. Paying well. Please RT",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Adobe",
        "indices" : [ 38, 44 ]
      }, {
        "text" : "Illustrator",
        "indices" : [ 45, 57 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "367036918589960192",
    "text" : "Looking for somebody who is good with #Adobe #Illustrator for a quick job. Paying well. Please RT",
    "id" : 367036918589960192,
    "created_at" : "Mon Aug 12 21:36:35 +0000 2013",
    "user" : {
      "name" : "David Henzel",
      "screen_name" : "davidhenzel",
      "protected" : false,
      "id_str" : "98993727",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3469565692/eaba8d3ea5299f50e3d1439ddf01c2e3_normal.jpeg",
      "id" : 98993727,
      "verified" : false
    }
  },
  "id" : 367041202803466240,
  "created_at" : "Mon Aug 12 21:53:36 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Taylor Jasko",
      "screen_name" : "tjasko",
      "indices" : [ 3, 10 ],
      "id_str" : "17503752",
      "id" : 17503752
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367041185648754688",
  "text" : "RT @tjasko: If any of my followers know someone who\u2019s awesome at Illustrator and spliicing up files for the web, hit me up. Need a job done\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/software/tweetbot/mac\" rel=\"nofollow\">Tweetbot for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "367036279692603392",
    "text" : "If any of my followers know someone who\u2019s awesome at Illustrator and spliicing up files for the web, hit me up. Need a job done quickly. :)",
    "id" : 367036279692603392,
    "created_at" : "Mon Aug 12 21:34:02 +0000 2013",
    "user" : {
      "name" : "Taylor Jasko",
      "screen_name" : "tjasko",
      "protected" : false,
      "id_str" : "17503752",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1177887607/c4291dd2586acaed3498f2946c9afe1c_normal.png",
      "id" : 17503752,
      "verified" : false
    }
  },
  "id" : 367041185648754688,
  "created_at" : "Mon Aug 12 21:53:32 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367039511676874752",
  "text" : "I wish I had scalability problems",
  "id" : 367039511676874752,
  "created_at" : "Mon Aug 12 21:46:53 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "keming",
      "indices" : [ 12, 19 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367026016750804992",
  "text" : "I'm so homy #keming",
  "id" : 367026016750804992,
  "created_at" : "Mon Aug 12 20:53:16 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CrowdSourcing",
      "indices" : [ 72, 86 ]
    } ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https://t.co/GRkGzi3MzT",
      "expanded_url" : "https://docs.google.com/document/d/1nK7zTRhRTjKULqKIsEYhls3lRAtBmFM7XzN9PrqSMc0/mobilebasic?pli=1",
      "display_url" : "docs.google.com/document/d/1nK\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367024601324597248",
  "text" : "The Internet is creating its own Bill of Rights https://t.co/GRkGzi3MzT #CrowdSourcing",
  "id" : 367024601324597248,
  "created_at" : "Mon Aug 12 20:47:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ownyourdata",
      "indices" : [ 96, 108 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https://t.co/tXcwCE1XDH",
      "expanded_url" : "https://medium.com/indieweb-thoughts/9d0e36524dbf",
      "display_url" : "medium.com/indieweb-thoug\u2026"
    }, {
      "indices" : [ 48, 71 ],
      "url" : "https://t.co/gli6VPzY46",
      "expanded_url" : "https://medium.com/surveillance-state/19a5db211e47",
      "display_url" : "medium.com/surveillance-s\u2026"
    }, {
      "indices" : [ 72, 95 ],
      "url" : "https://t.co/02I7sNvaaR",
      "expanded_url" : "https://medium.com/writers-on-writing/336300490cbb",
      "display_url" : "medium.com/writers-on-wri\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "367023119363350528",
  "text" : "Why not post on Medium?\nhttps://t.co/tXcwCE1XDH\nhttps://t.co/gli6VPzY46\nhttps://t.co/02I7sNvaaR\n#ownyourdata",
  "id" : 367023119363350528,
  "created_at" : "Mon Aug 12 20:41:45 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u00E9pinards & caramel",
      "screen_name" : "epinardscaramel",
      "indices" : [ 0, 16 ],
      "id_str" : "28838984",
      "id" : 28838984
    }, {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 17, 29 ],
      "id_str" : "15736190",
      "id" : 15736190
    }, {
      "name" : "Tantek \u00C7elik",
      "screen_name" : "t",
      "indices" : [ 30, 32 ],
      "id_str" : "11628",
      "id" : 11628
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "361553655184375808",
  "geo" : {
  },
  "id_str" : "367021238008299520",
  "in_reply_to_user_id" : 28838984,
  "text" : "@epinardscaramel @smashingmag @t Insider knowledge of Twitter being launched? Also this is Tantek who lives and breathes web...",
  "id" : 367021238008299520,
  "in_reply_to_status_id" : 361553655184375808,
  "created_at" : "Mon Aug 12 20:34:16 +0000 2013",
  "in_reply_to_screen_name" : "epinardscaramel",
  "in_reply_to_user_id_str" : "28838984",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "indices" : [ 3, 13 ],
      "id_str" : "605929797",
      "id" : 605929797
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367020397314994178",
  "text" : "RT @lunchduty: \"~ http://t.co/O4dNBLI2fN ~ \u2022\" https://t.co/u0ylVrLpe8",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/feedly/id396069556?mt=8&uo=4\" rel=\"nofollow\">feedly on iOS</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 3, 25 ],
        "url" : "http://t.co/O4dNBLI2fN",
        "expanded_url" : "http://cryptocloud.org",
        "display_url" : "cryptocloud.org"
      }, {
        "indices" : [ 31, 54 ],
        "url" : "https://t.co/u0ylVrLpe8",
        "expanded_url" : "https://www.cryptocloud.org/index.php",
        "display_url" : "cryptocloud.org/index.php"
      } ]
    },
    "geo" : {
    },
    "id_str" : "364547013947707392",
    "text" : "\"~ http://t.co/O4dNBLI2fN ~ \u2022\" https://t.co/u0ylVrLpe8",
    "id" : 364547013947707392,
    "created_at" : "Tue Aug 06 00:42:35 +0000 2013",
    "user" : {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "protected" : false,
      "id_str" : "605929797",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3566749477/5025656cb423825e1d0748cf13c87ca3_normal.png",
      "id" : 605929797,
      "verified" : false
    }
  },
  "id" : 367020397314994178,
  "created_at" : "Mon Aug 12 20:30:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Martin N.",
      "screen_name" : "AVGP",
      "indices" : [ 0, 5 ],
      "id_str" : "15381265",
      "id" : 15381265
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367017999380643841",
  "geo" : {
  },
  "id_str" : "367018509093441536",
  "in_reply_to_user_id" : 15381265,
  "text" : "@AVGP Oh I see. Well thanks for shedding light on it. gotta love these 140 character discussions about science!",
  "id" : 367018509093441536,
  "in_reply_to_status_id" : 367017999380643841,
  "created_at" : "Mon Aug 12 20:23:26 +0000 2013",
  "in_reply_to_screen_name" : "AVGP",
  "in_reply_to_user_id_str" : "15381265",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Martin N.",
      "screen_name" : "AVGP",
      "indices" : [ 0, 5 ],
      "id_str" : "15381265",
      "id" : 15381265
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367014980312236032",
  "geo" : {
  },
  "id_str" : "367015725879144448",
  "in_reply_to_user_id" : 15381265,
  "text" : "@AVGP Not doubting his other achievements, but that theory makes me doubt his credibility. It borders too much on philosophy not raw science",
  "id" : 367015725879144448,
  "in_reply_to_status_id" : 367014980312236032,
  "created_at" : "Mon Aug 12 20:12:22 +0000 2013",
  "in_reply_to_screen_name" : "AVGP",
  "in_reply_to_user_id_str" : "15381265",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Martin N.",
      "screen_name" : "AVGP",
      "indices" : [ 1, 6 ],
      "id_str" : "15381265",
      "id" : 15381265
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "367013705705205761",
  "geo" : {
  },
  "id_str" : "367013901767933953",
  "in_reply_to_user_id" : 15381265,
  "text" : ".@AVGP Annoying that Schrodinger is heralded as a physics luminary. The cat theory is mere piffle and pseudoscience as far as I'm concerned",
  "id" : 367013901767933953,
  "in_reply_to_status_id" : 367013705705205761,
  "created_at" : "Mon Aug 12 20:05:07 +0000 2013",
  "in_reply_to_screen_name" : "AVGP",
  "in_reply_to_user_id_str" : "15381265",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "367011302499024896",
  "text" : "One of these days I will understand Schr\u00F6dinger's Cat. I keep reading articles and never being able to get a \"takeaway\" from it. WTF is it?",
  "id" : 367011302499024896,
  "created_at" : "Mon Aug 12 19:54:47 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "indices" : [ 1, 7 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Voodoo",
      "indices" : [ 129, 136 ]
    } ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http://t.co/DNC4ax2Qol",
      "expanded_url" : "http://en.wikipedia.org/wiki/%E2%80%8B",
      "display_url" : "en.wikipedia.org/wiki/%E2%80%8B"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366994530735624193",
  "text" : ".@mikko Worth mentioning that this behaves strangely in latest Chrome:\n\nhttp://t.co/DNC4ax2Qol \n\n- Percentage encodes disappear! #Voodoo",
  "id" : 366994530735624193,
  "created_at" : "Mon Aug 12 18:48:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "indices" : [ 1, 7 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 41 ],
      "url" : "http://t.co/DNC4ax2Qol",
      "expanded_url" : "http://en.wikipedia.org/wiki/%E2%80%8B",
      "display_url" : "en.wikipedia.org/wiki/%E2%80%8B"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366991518281637890",
  "text" : ".@mikko Try this:\n\nhttp://t.co/DNC4ax2Qol",
  "id" : 366991518281637890,
  "created_at" : "Mon Aug 12 18:36:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "indices" : [ 1, 7 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 31 ],
      "url" : "http://t.co/9ceV2z6ytO",
      "expanded_url" : "http://en.wikipedia.org/wiki/%200B",
      "display_url" : "en.wikipedia.org/wiki/%200B"
    }, {
      "indices" : [ 53, 75 ],
      "url" : "http://t.co/qUUSAuPowt",
      "expanded_url" : "http://en.wikipedia.org/wiki/Zero-width_space",
      "display_url" : "en.wikipedia.org/wiki/Zero-widt\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366991339902091264",
  "text" : ".@mikko\n\nhttp://t.co/9ceV2z6ytO\n\nShould redirect to\n\nhttp://t.co/qUUSAuPowt\n\nBut when you paste the char. into the URL it redirects properly",
  "id" : 366991339902091264,
  "created_at" : "Mon Aug 12 18:35:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "indices" : [ 3, 9 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366989470425284608",
  "text" : "RT @mikko: URLs of the day:\nhttp://t.co/vBPlp6X4jm\nhttp://t.co/wxgw9jsKym\nhttp://t.co/U7vZw4wvYP\nhttp://t.co/2GQKEI1D4V\nhttp://t.co/uSEDTep\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 17, 39 ],
        "url" : "http://t.co/vBPlp6X4jm",
        "expanded_url" : "http://en.wikipedia.org/wiki/%E2%99%AB",
        "display_url" : "en.wikipedia.org/wiki/%E2%99%AB"
      }, {
        "indices" : [ 40, 62 ],
        "url" : "http://t.co/wxgw9jsKym",
        "expanded_url" : "http://en.wikipedia.org/wiki/%E2%98%81",
        "display_url" : "en.wikipedia.org/wiki/%E2%98%81"
      }, {
        "indices" : [ 63, 85 ],
        "url" : "http://t.co/U7vZw4wvYP",
        "expanded_url" : "http://en.wikipedia.org/wiki/%E2%98%82",
        "display_url" : "en.wikipedia.org/wiki/%E2%98%82"
      }, {
        "indices" : [ 86, 108 ],
        "url" : "http://t.co/2GQKEI1D4V",
        "expanded_url" : "http://en.wikipedia.org/wiki/%E2%98%8E",
        "display_url" : "en.wikipedia.org/wiki/%E2%98%8E"
      }, {
        "indices" : [ 109, 131 ],
        "url" : "http://t.co/uSEDTepRss",
        "expanded_url" : "http://en.wikipedia.org/wiki/%E2%98%84",
        "display_url" : "en.wikipedia.org/wiki/%E2%98%84"
      } ]
    },
    "geo" : {
    },
    "id_str" : "366980636369629184",
    "text" : "URLs of the day:\nhttp://t.co/vBPlp6X4jm\nhttp://t.co/wxgw9jsKym\nhttp://t.co/U7vZw4wvYP\nhttp://t.co/2GQKEI1D4V\nhttp://t.co/uSEDTepRss\n\u266B \u2602\u2601 \u260E \u2604",
    "id" : 366980636369629184,
    "created_at" : "Mon Aug 12 17:52:56 +0000 2013",
    "user" : {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "protected" : false,
      "id_str" : "23566038",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3428497729/8a03810d1c84ab31c512fb6660e85477_normal.jpeg",
      "id" : 23566038,
      "verified" : false
    }
  },
  "id" : 366989470425284608,
  "created_at" : "Mon Aug 12 18:28:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366967050209476610",
  "geo" : {
  },
  "id_str" : "366976081279991808",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 OMG - BonziBuddy! My favorite type of crapware.",
  "id" : 366976081279991808,
  "in_reply_to_status_id" : 366967050209476610,
  "created_at" : "Mon Aug 12 17:34:50 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashleigh ",
      "screen_name" : "Ashleigh1Kelly",
      "indices" : [ 0, 15 ],
      "id_str" : "235705822",
      "id" : 235705822
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366888053203156992",
  "geo" : {
  },
  "id_str" : "366918289089241090",
  "in_reply_to_user_id" : 235705822,
  "text" : "@Ashleigh1Kelly Whats wrong with her asking that?",
  "id" : 366918289089241090,
  "in_reply_to_status_id" : 366888053203156992,
  "created_at" : "Mon Aug 12 13:45:11 +0000 2013",
  "in_reply_to_screen_name" : "Ashleigh1Kelly",
  "in_reply_to_user_id_str" : "235705822",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/8pSqECkTgc",
      "expanded_url" : "http://makemydesignflat.com/",
      "display_url" : "makemydesignflat.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366829351607607298",
  "text" : "http://t.co/8pSqECkTgc",
  "id" : 366829351607607298,
  "created_at" : "Mon Aug 12 07:51:47 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mozilla",
      "screen_name" : "mozilla",
      "indices" : [ 3, 11 ],
      "id_str" : "106682853",
      "id" : 106682853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366808647067639808",
  "text" : "RT @mozilla: Mozilla's Persona Web site log-in system adds Gmail to its list of sign-in credentials you can use: http://t.co/u9DzLEZT6S (vi\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.hootsuite.com\" rel=\"nofollow\">HootSuite</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "CNET",
        "screen_name" : "CNET",
        "indices" : [ 128, 133 ],
        "id_str" : "30261067",
        "id" : 30261067
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 100, 122 ],
        "url" : "http://t.co/u9DzLEZT6S",
        "expanded_url" : "http://ow.ly/nMTcO",
        "display_url" : "ow.ly/nMTcO"
      } ]
    },
    "geo" : {
    },
    "id_str" : "365878467679617024",
    "text" : "Mozilla's Persona Web site log-in system adds Gmail to its list of sign-in credentials you can use: http://t.co/u9DzLEZT6S (via @CNET)",
    "id" : 365878467679617024,
    "created_at" : "Fri Aug 09 16:53:19 +0000 2013",
    "user" : {
      "name" : "Mozilla",
      "screen_name" : "mozilla",
      "protected" : false,
      "id_str" : "106682853",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1577180475/Screen_Shot_2011-10-07_at_11.52.39_AM_normal.png",
      "id" : 106682853,
      "verified" : false
    }
  },
  "id" : 366808647067639808,
  "created_at" : "Mon Aug 12 06:29:31 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mozilla",
      "screen_name" : "mozilla",
      "indices" : [ 30, 38 ],
      "id_str" : "106682853",
      "id" : 106682853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366808429962076160",
  "text" : "I want the web saturated with @Mozilla Persona. It solves the identity problem. I want every site to implement it. Now.",
  "id" : 366808429962076160,
  "created_at" : "Mon Aug 12 06:28:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/6ynQ1Y3d0u",
      "expanded_url" : "http://sdbr.co/16DNYOF",
      "display_url" : "sdbr.co/16DNYOF"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366798881192812545",
  "text" : "Visual issue tracking for front-end web devs http://t.co/6ynQ1Y3d0u",
  "id" : 366798881192812545,
  "created_at" : "Mon Aug 12 05:50:42 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/hCNGN6M8Bo",
      "expanded_url" : "http://sdbr.co/1cs7y7h",
      "display_url" : "sdbr.co/1cs7y7h"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366798617882804225",
  "text" : "Designing User Interfaces for Your Mother http://t.co/hCNGN6M8Bo",
  "id" : 366798617882804225,
  "created_at" : "Mon Aug 12 05:49:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https://t.co/9B8yyGddMM",
      "expanded_url" : "https://thoughtstreams.io/higgins/interests/",
      "display_url" : "thoughtstreams.io/higgins/intere\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366792645021929472",
  "text" : "Interests by higgins https://t.co/9B8yyGddMM I am not going to invent buzzwords and put them in my CV. I am proficient in all those areas.",
  "id" : 366792645021929472,
  "created_at" : "Mon Aug 12 05:25:55 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366774178046672896",
  "geo" : {
  },
  "id_str" : "366775432261009408",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Some nice points. So many Twitter bios have that famous line ...my thoughts are my own, not my employer's etc",
  "id" : 366775432261009408,
  "in_reply_to_status_id" : 366774178046672896,
  "created_at" : "Mon Aug 12 04:17:32 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 135 ],
      "url" : "http://t.co/p1hgHoMjeN",
      "expanded_url" : "http://higg.im/",
      "display_url" : "higg.im"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366767454191489026",
  "text" : "Another day, another iteration of my homepage. This is hosted on GH, so it's forkable, and easy to maintain, etc http://t.co/p1hgHoMjeN",
  "id" : 366767454191489026,
  "created_at" : "Mon Aug 12 03:45:49 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marquee",
      "screen_name" : "marquee",
      "indices" : [ 28, 36 ],
      "id_str" : "372906907",
      "id" : 372906907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366763536304709633",
  "text" : "Just asked for an invite to @marquee Looks like a really promising publishing platform.",
  "id" : 366763536304709633,
  "created_at" : "Mon Aug 12 03:30:15 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https://t.co/G6QZOgNMWH",
      "expanded_url" : "https://thoughtstreams.io/jtauber/the-new-tldrs/",
      "display_url" : "thoughtstreams.io/jtauber/the-ne\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366760225769922563",
  "text" : "Awesome! The New TL;DRs by jtauber https://t.co/G6QZOgNMWH Also, Instagram Link, Didn't View IL;DV",
  "id" : 366760225769922563,
  "created_at" : "Mon Aug 12 03:17:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http://t.co/2MHEyNrYqG",
      "expanded_url" : "http://toddmotto.com/labs/echo/",
      "display_url" : "toddmotto.com/labs/echo/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366718707088625665",
  "text" : "Echo.js, simple JavaScript image lazy loading http://t.co/2MHEyNrYqG",
  "id" : 366718707088625665,
  "created_at" : "Mon Aug 12 00:32:07 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "indices" : [ 0, 6 ],
      "id_str" : "2367111",
      "id" : 2367111
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 111 ],
      "url" : "http://t.co/qOCWBsWKAX",
      "expanded_url" : "http://username.github.io",
      "display_url" : "username.github.io"
    }, {
      "indices" : [ 115, 137 ],
      "url" : "http://t.co/jhU1EYl2zb",
      "expanded_url" : "http://labnotes.org",
      "display_url" : "labnotes.org"
    } ]
  },
  "in_reply_to_status_id_str" : "366716267589148674",
  "geo" : {
  },
  "id_str" : "366718031231074304",
  "in_reply_to_user_id" : 2367111,
  "text" : "@assaf Got it working. I don't know how your setup is working. It looks like you renamed http://t.co/qOCWBsWKAX to http://t.co/jhU1EYl2zb!",
  "id" : 366718031231074304,
  "in_reply_to_status_id" : 366716267589148674,
  "created_at" : "Mon Aug 12 00:29:26 +0000 2013",
  "in_reply_to_screen_name" : "assaf",
  "in_reply_to_user_id_str" : "2367111",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "indices" : [ 0, 6 ],
      "id_str" : "2367111",
      "id" : 2367111
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366714815995056128",
  "geo" : {
  },
  "id_str" : "366715278870065152",
  "in_reply_to_user_id" : 2367111,
  "text" : "@assaf I just wanted to know if you done anything special, or unorthodox to get it working",
  "id" : 366715278870065152,
  "in_reply_to_status_id" : 366714815995056128,
  "created_at" : "Mon Aug 12 00:18:30 +0000 2013",
  "in_reply_to_screen_name" : "assaf",
  "in_reply_to_user_id_str" : "2367111",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "indices" : [ 0, 6 ],
      "id_str" : "2367111",
      "id" : 2367111
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http://t.co/RMAigRb8xF",
      "expanded_url" : "http://assaf.github.io",
      "display_url" : "assaf.github.io"
    }, {
      "indices" : [ 71, 93 ],
      "url" : "http://t.co/jhU1EYl2zb",
      "expanded_url" : "http://labnotes.org",
      "display_url" : "labnotes.org"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366713687811170307",
  "in_reply_to_user_id" : 2367111,
  "text" : "@assaf Also, how come the http://t.co/RMAigRb8xF repo redirects to the http://t.co/jhU1EYl2zb repo?",
  "id" : 366713687811170307,
  "created_at" : "Mon Aug 12 00:12:11 +0000 2013",
  "in_reply_to_screen_name" : "assaf",
  "in_reply_to_user_id_str" : "2367111",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "indices" : [ 0, 6 ],
      "id_str" : "2367111",
      "id" : 2367111
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 60 ],
      "url" : "http://t.co/jhU1EYl2zb",
      "expanded_url" : "http://labnotes.org",
      "display_url" : "labnotes.org"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366713458034606083",
  "in_reply_to_user_id" : 2367111,
  "text" : "@assaf Hey. Was wondering how you got http://t.co/jhU1EYl2zb hosted on github without using a subdomain like www.labnotes etc",
  "id" : 366713458034606083,
  "created_at" : "Mon Aug 12 00:11:16 +0000 2013",
  "in_reply_to_screen_name" : "assaf",
  "in_reply_to_user_id_str" : "2367111",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "indices" : [ 3, 13 ],
      "id_str" : "605929797",
      "id" : 605929797
    }, {
      "name" : "YouTube",
      "screen_name" : "YouTube",
      "indices" : [ 25, 33 ],
      "id_str" : "10228272",
      "id" : 10228272
    }, {
      "name" : "Kyle",
      "screen_name" : "getify",
      "indices" : [ 105, 112 ],
      "id_str" : "16686076",
      "id" : 16686076
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 62 ],
      "url" : "http://t.co/XfwtiOyPXm",
      "expanded_url" : "http://myshar.es/140Hk2H",
      "display_url" : "myshar.es/140Hk2H"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366679209256558593",
  "text" : "RT @lunchduty: I liked a @YouTube video http://t.co/XfwtiOyPXm\u00A0 Browser Versions Are Dead (Fluent 2013) +@getify",
  "id" : 366679209256558593,
  "created_at" : "Sun Aug 11 21:55:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Keming",
      "indices" : [ 105, 112 ]
    }, {
      "text" : "Pom",
      "indices" : [ 113, 117 ]
    }, {
      "text" : "Pr0n",
      "indices" : [ 118, 123 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366677707796381697",
  "text" : "TIL - pom looks like porn depending on what kerning the font has. This is my new favorite word for 2013. #Keming #Pom #Pr0n",
  "id" : 366677707796381697,
  "created_at" : "Sun Aug 11 21:49:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 89 ],
      "url" : "http://t.co/OBnowmhdPx",
      "expanded_url" : "http://fancy.com/davidhiggins",
      "display_url" : "fancy.com/davidhiggins"
    } ]
  },
  "in_reply_to_status_id_str" : "366669611418132480",
  "geo" : {
  },
  "id_str" : "366671249147047936",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 The site has a good selection, but pales in comparison to http://t.co/OBnowmhdPx (Fancy). I rarely go elsewhere now to find gadgets",
  "id" : 366671249147047936,
  "in_reply_to_status_id" : 366669611418132480,
  "created_at" : "Sun Aug 11 21:23:32 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ven Portman",
      "screen_name" : "venportman",
      "indices" : [ 0, 11 ],
      "id_str" : "502182983",
      "id" : 502182983
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Bitters",
      "indices" : [ 116, 124 ]
    } ],
    "urls" : [ {
      "indices" : [ 86, 108 ],
      "url" : "http://t.co/alNYVPKo80",
      "expanded_url" : "http://higg.tel/",
      "display_url" : "higg.tel"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366652499962236928",
  "in_reply_to_user_id" : 502182983,
  "text" : "@venportman Perhaps you can hook me up with an installation. You can contact me here: http://t.co/alNYVPKo80 I want #Bitters so bad. Ping me",
  "id" : 366652499962236928,
  "created_at" : "Sun Aug 11 20:09:02 +0000 2013",
  "in_reply_to_screen_name" : "venportman",
  "in_reply_to_user_id_str" : "502182983",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ven Portman",
      "screen_name" : "venportman",
      "indices" : [ 0, 11 ],
      "id_str" : "502182983",
      "id" : 502182983
    }, {
      "name" : "DigitalOcean",
      "screen_name" : "digitalocean",
      "indices" : [ 33, 46 ],
      "id_str" : "457033547",
      "id" : 457033547
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Bitters",
      "indices" : [ 103, 111 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 75 ],
      "url" : "http://t.co/hwaFyMGR2s",
      "expanded_url" : "http://www.evbogue.com/digitalocean",
      "display_url" : "evbogue.com/digitalocean"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366652282621804544",
  "in_reply_to_user_id" : 502182983,
  "text" : "@venportman Hey man. I read your @digitalocean post (http://t.co/hwaFyMGR2s) Impressed! Looking to run #Bitters without much hassle ...",
  "id" : 366652282621804544,
  "created_at" : "Sun Aug 11 20:08:10 +0000 2013",
  "in_reply_to_screen_name" : "venportman",
  "in_reply_to_user_id_str" : "502182983",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 45 ],
      "url" : "http://t.co/MY4GPbo6VW",
      "expanded_url" : "http://sdbr.co/1bmnm7K",
      "display_url" : "sdbr.co/1bmnm7K"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366651313028734976",
  "text" : "Kraken Image Optimizer http://t.co/MY4GPbo6VW",
  "id" : 366651313028734976,
  "created_at" : "Sun Aug 11 20:04:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "indices" : [ 3, 16 ],
      "id_str" : "154206772",
      "id" : 154206772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366649514083364864",
  "text" : "RT @a_antonellis: All likes should be double tap likes",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "366647928187654145",
    "text" : "All likes should be double tap likes",
    "id" : 366647928187654145,
    "created_at" : "Sun Aug 11 19:50:52 +0000 2013",
    "user" : {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "protected" : false,
      "id_str" : "154206772",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1904673216/cmy_normal.gif",
      "id" : 154206772,
      "verified" : false
    }
  },
  "id" : 366649514083364864,
  "created_at" : "Sun Aug 11 19:57:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dmitriy A.",
      "screen_name" : "jimaek",
      "indices" : [ 0, 7 ],
      "id_str" : "141747302",
      "id" : 141747302
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366635383225724930",
  "geo" : {
  },
  "id_str" : "366636082554609665",
  "in_reply_to_user_id" : 141747302,
  "text" : "@jimaek Yeah. Could do that! Automation is the end goal of all computing :)",
  "id" : 366636082554609665,
  "in_reply_to_status_id" : 366635383225724930,
  "created_at" : "Sun Aug 11 19:03:48 +0000 2013",
  "in_reply_to_screen_name" : "jimaek",
  "in_reply_to_user_id_str" : "141747302",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CDNProblems",
      "indices" : [ 128, 140 ]
    } ],
    "urls" : [ {
      "indices" : [ 105, 127 ],
      "url" : "http://t.co/eOIi3SQZ7n",
      "expanded_url" : "http://myshar.es/13c57lh",
      "display_url" : "myshar.es/13c57lh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366633730648969216",
  "text" : "\"Dealing with versions is tricky if you use a CDN. You need to keep someone glued to the CDN purge tool\" http://t.co/eOIi3SQZ7n #CDNProblems",
  "id" : 366633730648969216,
  "created_at" : "Sun Aug 11 18:54:27 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GithubProblems",
      "indices" : [ 119, 134 ]
    } ],
    "urls" : [ {
      "indices" : [ 96, 118 ],
      "url" : "http://t.co/k9nDgmsQls",
      "expanded_url" : "http://isharefil.es/QkNd",
      "display_url" : "isharefil.es/QkNd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366580971136499713",
  "text" : "I am 'shell averse', but still use the shell if I need to. But part of me dies when I see this: http://t.co/k9nDgmsQls #GithubProblems",
  "id" : 366580971136499713,
  "created_at" : "Sun Aug 11 15:24:48 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https://t.co/PHi95VXxpt",
      "expanded_url" : "https://twitter.com/samkottler/status/353137064969125888",
      "display_url" : "twitter.com/samkottler/sta\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366576374498070528",
  "text" : "\"Daddy, how is software made?\" https://t.co/PHi95VXxpt",
  "id" : 366576374498070528,
  "created_at" : "Sun Aug 11 15:06:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Curtis Chambers",
      "screen_name" : "curtischambers",
      "indices" : [ 0, 15 ],
      "id_str" : "14182149",
      "id" : 14182149
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 140 ],
      "url" : "http://t.co/aXCle4PPnA",
      "expanded_url" : "http://isharefil.es/QlWN",
      "display_url" : "isharefil.es/QlWN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366575497401995264",
  "in_reply_to_user_id" : 14182149,
  "text" : "@curtischambers Hey can you pass on this layout bug to your team. The appstore buttons are overlapping the select box http://t.co/aXCle4PPnA",
  "id" : 366575497401995264,
  "created_at" : "Sun Aug 11 15:03:03 +0000 2013",
  "in_reply_to_screen_name" : "curtischambers",
  "in_reply_to_user_id_str" : "14182149",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Uber",
      "screen_name" : "Uber",
      "indices" : [ 80, 85 ],
      "id_str" : "19103481",
      "id" : 19103481
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 76 ],
      "url" : "http://t.co/aXCle4PPnA",
      "expanded_url" : "http://isharefil.es/QlWN",
      "display_url" : "isharefil.es/QlWN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366573882481721344",
  "text" : "It's tiny inconsistencies like this that drive me mad http://t.co/aXCle4PPnA If @uber's site was on Github I would be sending a pull request",
  "id" : 366573882481721344,
  "created_at" : "Sun Aug 11 14:56:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 0, 12 ],
      "id_str" : "15736190",
      "id" : 15736190
    }, {
      "name" : "Uber",
      "screen_name" : "Uber",
      "indices" : [ 21, 26 ],
      "id_str" : "19103481",
      "id" : 19103481
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366507481595256832",
  "geo" : {
  },
  "id_str" : "366546080877383680",
  "in_reply_to_user_id" : 15736190,
  "text" : "@smashingmag The new @uber redesign is sweet!",
  "id" : 366546080877383680,
  "in_reply_to_status_id" : 366507481595256832,
  "created_at" : "Sun Aug 11 13:06:10 +0000 2013",
  "in_reply_to_screen_name" : "smashingmag",
  "in_reply_to_user_id_str" : "15736190",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364477301687189507",
  "geo" : {
  },
  "id_str" : "366346159184285696",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas Yes, but they sniff and serve device-specific pages. None of this media-query ballyhoo infecting the web. Real engineers sniff :)",
  "id" : 366346159184285696,
  "in_reply_to_status_id" : 364477301687189507,
  "created_at" : "Sat Aug 10 23:51:45 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "~foomandoonian",
      "screen_name" : "foomandoonian",
      "indices" : [ 1, 15 ],
      "id_str" : "703673",
      "id" : 703673
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 63 ],
      "url" : "http://t.co/yA23ocRAJs",
      "expanded_url" : "http://isharefil.es/QkqE",
      "display_url" : "isharefil.es/QkqE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366331457297326080",
  "text" : "\u201C@foomandoonian: The Scunthorpe problem: http://t.co/yA23ocRAJs\u201D",
  "id" : 366331457297326080,
  "created_at" : "Sat Aug 10 22:53:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http://t.co/sMCOAHY4SE",
      "expanded_url" : "http://www.vagrantbox.es/",
      "display_url" : "vagrantbox.es"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366325664443404288",
  "text" : "I genuinely want to know how many of these are rootkitted http://t.co/sMCOAHY4SE",
  "id" : 366325664443404288,
  "created_at" : "Sat Aug 10 22:30:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bountysource",
      "screen_name" : "Bountysource",
      "indices" : [ 0, 13 ],
      "id_str" : "774713040",
      "id" : 774713040
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366213128050049025",
  "geo" : {
  },
  "id_str" : "366298059979751424",
  "in_reply_to_user_id" : 774713040,
  "text" : "@Bountysource Notepad2 by Flo",
  "id" : 366298059979751424,
  "in_reply_to_status_id" : 366213128050049025,
  "created_at" : "Sat Aug 10 20:40:37 +0000 2013",
  "in_reply_to_screen_name" : "Bountysource",
  "in_reply_to_user_id_str" : "774713040",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 95 ],
      "url" : "http://t.co/QnhI9U2asP",
      "expanded_url" : "http://m.theatlanticcities.com/politics/2013/08/recycling-bin-following-you/6475/",
      "display_url" : "m.theatlanticcities.com/politics/2013/\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366282611557023745",
  "text" : "Yup. Forget RFID chips. Your phone is a beacon that reveals who you are. http://t.co/QnhI9U2asP MAC address vulns are not new though...",
  "id" : 366282611557023745,
  "created_at" : "Sat Aug 10 19:39:14 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "john holt ripley",
      "screen_name" : "johnholtripley",
      "indices" : [ 3, 18 ],
      "id_str" : "305202261",
      "id" : 305202261
    }, {
      "name" : "Jason Grigsby, \u26014",
      "screen_name" : "grigs",
      "indices" : [ 129, 135 ],
      "id_str" : "5774462",
      "id" : 5774462
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366279617444716545",
  "text" : "RT @johnholtripley: That's the One Web broken - Google TV guidelines say not to use anchor elements. https://t.co/EPg9f4F7yL via @grigs",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jason Grigsby, \u26014",
        "screen_name" : "grigs",
        "indices" : [ 109, 115 ],
        "id_str" : "5774462",
        "id" : 5774462
      } ],
      "media" : [ {
        "expanded_url" : "https://twitter.com/grigs/status/345225895843352577/photo/1",
        "indices" : [ 81, 104 ],
        "url" : "https://t.co/EPg9f4F7yL",
        "media_url" : "http://pbs.twimg.com/media/BMp9H4lCUAApymr.png",
        "id_str" : "345225895847546880",
        "id" : 345225895847546880,
        "media_url_https" : "https://pbs.twimg.com/media/BMp9H4lCUAApymr.png",
        "sizes" : [ {
          "h" : 224,
          "resize" : "fit",
          "w" : 340
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 674,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 395,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 869,
          "resize" : "fit",
          "w" : 1321
        } ],
        "display_url" : "pic.twitter.com/EPg9f4F7yL"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "345448853501272064",
    "text" : "That's the One Web broken - Google TV guidelines say not to use anchor elements. https://t.co/EPg9f4F7yL via @grigs",
    "id" : 345448853501272064,
    "created_at" : "Fri Jun 14 07:53:19 +0000 2013",
    "user" : {
      "name" : "john holt ripley",
      "screen_name" : "johnholtripley",
      "protected" : false,
      "id_str" : "305202261",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1937936625/me_normal.jpg",
      "id" : 305202261,
      "verified" : false
    }
  },
  "id" : 366279617444716545,
  "created_at" : "Sat Aug 10 19:27:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366279236895518722",
  "text" : "So the TPB releases a 'Pirate Browser'. In my lifetime I could have released hundreds, if not thousands of these? Silly project is silly.",
  "id" : 366279236895518722,
  "created_at" : "Sat Aug 10 19:25:49 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "(\u0E3F) \u1550 \u0306\u0308\u0361\u035C \u1550 \u0ACB\u0941 \u301C",
      "screen_name" : "01101O10",
      "indices" : [ 0, 9 ],
      "id_str" : "546382113",
      "id" : 546382113
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366172571609219073",
  "geo" : {
  },
  "id_str" : "366276173363281921",
  "in_reply_to_user_id" : 546382113,
  "text" : "@01101O10 you are an insane human.",
  "id" : 366276173363281921,
  "in_reply_to_status_id" : 366172571609219073,
  "created_at" : "Sat Aug 10 19:13:39 +0000 2013",
  "in_reply_to_screen_name" : "01101O10",
  "in_reply_to_user_id_str" : "546382113",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "India shanelle Rhode",
      "screen_name" : "india_rhodes",
      "indices" : [ 3, 16 ],
      "id_str" : "330671171",
      "id" : 330671171
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "PrayforChrisbrown",
      "indices" : [ 18, 36 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366273445077598208",
  "text" : "RT @india_rhodes: #PrayforChrisbrown is trending because everyone is in shock that it is trending and everyone tweeting bout their shock is\u2026",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "PrayforChrisbrown",
        "indices" : [ 0, 18 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "366271130153127937",
    "text" : "#PrayforChrisbrown is trending because everyone is in shock that it is trending and everyone tweeting bout their shock is hashtagging it",
    "id" : 366271130153127937,
    "created_at" : "Sat Aug 10 18:53:37 +0000 2013",
    "user" : {
      "name" : "India shanelle Rhode",
      "screen_name" : "india_rhodes",
      "protected" : false,
      "id_str" : "330671171",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000050458915/b6e49b34a22fa5d4d0d108024d48a0d7_normal.jpeg",
      "id" : 330671171,
      "verified" : false
    }
  },
  "id" : 366273445077598208,
  "created_at" : "Sat Aug 10 19:02:49 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 1, 15 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "366244730062913536",
  "geo" : {
  },
  "id_str" : "366252087555792897",
  "in_reply_to_user_id" : 277735240,
  "text" : ".@soaj1664ashar Damn. When I saw the Chrome logo, I thought Google Accounts had a 10 char limit. Was about to fire up AccessDiver.",
  "id" : 366252087555792897,
  "in_reply_to_status_id" : 366244730062913536,
  "created_at" : "Sat Aug 10 17:37:56 +0000 2013",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "prayforchrisbrown",
      "indices" : [ 35, 53 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366202544353058817",
  "text" : "And let the snarky tweets commence #prayforchrisbrown",
  "id" : 366202544353058817,
  "created_at" : "Sat Aug 10 14:21:05 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http://t.co/ZYdVpFO8ES",
      "expanded_url" : "http://www.behance.net/gallery/bariol/4015111",
      "display_url" : "behance.net/gallery/bariol\u2026"
    }, {
      "indices" : [ 80, 102 ],
      "url" : "http://t.co/WhlppMKRCS",
      "expanded_url" : "http://www.myfonts.com/fonts/underware/bello/",
      "display_url" : "myfonts.com/fonts/underwar\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366201231271665666",
  "text" : "Currently in love with these two fonts:\n\nBariol: http://t.co/ZYdVpFO8ES\n\nBello: http://t.co/WhlppMKRCS\n\nGoing to use them for _everything_",
  "id" : 366201231271665666,
  "created_at" : "Sat Aug 10 14:15:51 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Rodriguez",
      "screen_name" : "funkyboy",
      "indices" : [ 0, 9 ],
      "id_str" : "11328742",
      "id" : 11328742
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https://t.co/Te9y5qFH9o",
      "expanded_url" : "https://alpha.app.net/dh/post/8714702",
      "display_url" : "alpha.app.net/dh/post/8714702"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366199576065081344",
  "in_reply_to_user_id" : 11328742,
  "text" : "@funkyboy Yes. Huge bias towards Javascript &amp; CSS. Some other categories don't warrant the 'dev' label though.\u2026 https://t.co/Te9y5qFH9o",
  "id" : 366199576065081344,
  "created_at" : "Sat Aug 10 14:09:17 +0000 2013",
  "in_reply_to_screen_name" : "funkyboy",
  "in_reply_to_user_id_str" : "11328742",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http://t.co/AJxRMCHViO",
      "expanded_url" : "http://blog.sucuri.net",
      "display_url" : "blog.sucuri.net"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366170881799630849",
  "text" : "Open Source Backdoor \u2013 Copyrighted Under GNU GPL | Sucuri Blog [http://t.co/AJxRMCHViO]",
  "id" : 366170881799630849,
  "created_at" : "Sat Aug 10 12:15:16 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http://t.co/Ecx1byaViB",
      "expanded_url" : "http://isharefil.es/QkTo",
      "display_url" : "isharefil.es/QkTo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366167114672898048",
  "text" : "5 Habits of Highly Effective Communicators http://t.co/Ecx1byaViB",
  "id" : 366167114672898048,
  "created_at" : "Sat Aug 10 12:00:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http://t.co/MO5r0SxX8g",
      "expanded_url" : "http://ashesapp.com/",
      "display_url" : "ashesapp.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366163708663185409",
  "text" : "Giggling like a schoolgirl with the newly released Ashes app for Fever. http://t.co/MO5r0SxX8g This is epic news.",
  "id" : 366163708663185409,
  "created_at" : "Sat Aug 10 11:46:45 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 4, 26 ],
      "url" : "http://t.co/PyNslFeyPS",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    }, {
      "indices" : [ 80, 102 ],
      "url" : "http://t.co/wDtoOU2Gcq",
      "expanded_url" : "http://f.cl.ly/items/1u1c1h3V1u2z3G0u0T1t/devlinks.txt",
      "display_url" : "f.cl.ly/items/1u1c1h3V\u2026"
    }, {
      "indices" : [ 113, 136 ],
      "url" : "https://t.co/Fuq6Yj86X1",
      "expanded_url" : "https://alpha.app.net/dh/post/8710891",
      "display_url" : "alpha.app.net/dh/post/8710891"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366159933974130688",
  "text" : "Hey http://t.co/PyNslFeyPS? Being surfing the web like a madman compiling this. http://t.co/wDtoOU2Gcq It's not\u2026 https://t.co/Fuq6Yj86X1",
  "id" : 366159933974130688,
  "created_at" : "Sat Aug 10 11:31:45 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "john holt ripley",
      "screen_name" : "johnholtripley",
      "indices" : [ 0, 15 ],
      "id_str" : "305202261",
      "id" : 305202261
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https://t.co/SrJGfkutv2",
      "expanded_url" : "https://twitter.com/alphenic/status/366156409928290305",
      "display_url" : "twitter.com/alphenic/statu\u2026"
    }, {
      "indices" : [ 116, 139 ],
      "url" : "https://t.co/tSprdVEZl4",
      "expanded_url" : "https://alpha.app.net/dh/post/8710801",
      "display_url" : "alpha.app.net/dh/post/8710801"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366158800807395328",
  "in_reply_to_user_id" : 305202261,
  "text" : "@johnholtripley That's a very handy resource. Tweeted about it here: https://t.co/SrJGfkutv2 Can you add unicod.es\u2026 https://t.co/tSprdVEZl4",
  "id" : 366158800807395328,
  "created_at" : "Sat Aug 10 11:27:15 +0000 2013",
  "in_reply_to_screen_name" : "johnholtripley",
  "in_reply_to_user_id_str" : "305202261",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ogham F",
      "screen_name" : "Ogham",
      "indices" : [ 0, 6 ],
      "id_str" : "22168966",
      "id" : 22168966
    }, {
      "name" : "Tapbots",
      "screen_name" : "tapbots",
      "indices" : [ 74, 82 ],
      "id_str" : "16669898",
      "id" : 16669898
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https://t.co/IJckEbQbe8",
      "expanded_url" : "https://alpha.app.net/dh/post/8710733",
      "display_url" : "alpha.app.net/dh/post/8710733"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366158171653419008",
  "in_reply_to_user_id" : 22168966,
  "text" : "@ogham It's cool. I grokked it now. I am still learning the ropes. Oh and @tapbots - I expect the URL to be\u2026 https://t.co/IJckEbQbe8",
  "id" : 366158171653419008,
  "created_at" : "Sat Aug 10 11:24:45 +0000 2013",
  "in_reply_to_screen_name" : "Ogham",
  "in_reply_to_user_id_str" : "22168966",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/83extEwI1q",
      "expanded_url" : "http://piratebrowser.com/",
      "display_url" : "piratebrowser.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366157562724364288",
  "text" : "http://t.co/83extEwI1q",
  "id" : 366157562724364288,
  "created_at" : "Sat Aug 10 11:22:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http://t.co/WMjfc0j47n",
      "expanded_url" : "http://isharefil.es/Ql8n",
      "display_url" : "isharefil.es/Ql8n"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366156409928290305",
  "text" : "Unicode support on different devices. Interesting. http://t.co/WMjfc0j47n",
  "id" : 366156409928290305,
  "created_at" : "Sat Aug 10 11:17:45 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Hauser",
      "screen_name" : "dh",
      "indices" : [ 0, 3 ],
      "id_str" : "352553",
      "id" : 352553
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https://t.co/DNb1As4BCI",
      "expanded_url" : "https://alpha.app.net/dh/post/8710495",
      "display_url" : "alpha.app.net/dh/post/8710495"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366155522090598400",
  "in_reply_to_user_id" : 352553,
  "text" : "@dh Hi, just looking at unicod.es - nice work. I\u2019ve been working on a tool for checking support for unicode\u2026 https://t.co/DNb1As4BCI",
  "id" : 366155522090598400,
  "created_at" : "Sat Aug 10 11:14:13 +0000 2013",
  "in_reply_to_screen_name" : "dh",
  "in_reply_to_user_id_str" : "352553",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ogham F",
      "screen_name" : "Ogham",
      "indices" : [ 0, 6 ],
      "id_str" : "22168966",
      "id" : 22168966
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366155278040838144",
  "in_reply_to_user_id" : 22168966,
  "text" : "@ogham Oh I see. I want it to shorten the URL when I do a 'post quote\u2019 Is there some way of doing this?",
  "id" : 366155278040838144,
  "created_at" : "Sat Aug 10 11:13:15 +0000 2013",
  "in_reply_to_screen_name" : "Ogham",
  "in_reply_to_user_id_str" : "22168966",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 76 ],
      "url" : "http://t.co/cLgbKb08ZA",
      "expanded_url" : "http://isharefil.es/QkKX",
      "display_url" : "isharefil.es/QkKX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366154670844018688",
  "text" : "Testing out Cloudapp URL shortening. Hope this works. http://t.co/cLgbKb08ZA",
  "id" : 366154670844018688,
  "created_at" : "Sat Aug 10 11:10:51 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https://t.co/5EhYcDx6D9",
      "expanded_url" : "https://photos.app.net/8710393/1",
      "display_url" : "photos.app.net/8710393/1"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366154039546740736",
  "text" : "I am trying to figure out how short URLs work on Netbot? When/Where does the shortening take place? https://t.co/5EhYcDx6D9",
  "id" : 366154039546740736,
  "created_at" : "Sat Aug 10 11:08:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.apple.com\" rel=\"nofollow\">iOS</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 43 ],
      "url" : "http://t.co/oROwbN4YBJ",
      "expanded_url" : "http://mln.im/15nKbbQ",
      "display_url" : "mln.im/15nKbbQ"
    }, {
      "indices" : [ 47, 70 ],
      "url" : "https://t.co/IIjXu4mUJb",
      "expanded_url" : "https://alpha.app.net/mlane",
      "display_url" : "alpha.app.net/mlane"
    }, {
      "indices" : [ 71, 94 ],
      "url" : "https://t.co/CIcYHzmjJH",
      "expanded_url" : "https://posts.app.net/5430562",
      "display_url" : "posts.app.net/5430562"
    } ]
  },
  "geo" : {
  },
  "id_str" : "366150256976601089",
  "text" : "User Experience Debt http://t.co/oROwbN4YBJ \u2014 (https://t.co/IIjXu4mUJb https://t.co/CIcYHzmjJH)",
  "id" : 366150256976601089,
  "created_at" : "Sat Aug 10 10:53:18 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Klout",
      "screen_name" : "klout",
      "indices" : [ 3, 9 ],
      "id_str" : "15134782",
      "id" : 15134782
    }, {
      "name" : "Barack Obama",
      "screen_name" : "BarackObama",
      "indices" : [ 123, 135 ],
      "id_str" : "813286",
      "id" : 813286
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366147295781199873",
  "text" : "If @Klout took my Github commits into account as part of their social signal analysis, my influence would be on a par with @barackobama",
  "id" : 366147295781199873,
  "created_at" : "Sat Aug 10 10:41:32 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Billy Herrington",
      "screen_name" : "BossOfThisGym",
      "indices" : [ 3, 17 ],
      "id_str" : "548406157",
      "id" : 548406157
    }, {
      "name" : "(\u0E3F) \u1550 \u0306\u0308\u0361\u035C \u1550 \u0ACB\u0941 \u301C",
      "screen_name" : "01101O10",
      "indices" : [ 19, 28 ],
      "id_str" : "546382113",
      "id" : 546382113
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366130023628935168",
  "text" : "RT @BossOfThisGym: @01101O10 Get off the dope get off the booze",
  "retweeted_status" : {
    "source" : "<a href=\"http://twittbot.net/\" rel=\"nofollow\">twittbot.net</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "(\u0E3F) \u1550 \u0306\u0308\u0361\u035C \u1550 \u0ACB\u0941 \u301C",
        "screen_name" : "01101O10",
        "indices" : [ 0, 9 ],
        "id_str" : "546382113",
        "id" : 546382113
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "366120953001947136",
    "geo" : {
    },
    "id_str" : "366122545260412928",
    "in_reply_to_user_id" : 546382113,
    "text" : "@01101O10 Get off the dope get off the booze",
    "id" : 366122545260412928,
    "in_reply_to_status_id" : 366120953001947136,
    "created_at" : "Sat Aug 10 09:03:11 +0000 2013",
    "in_reply_to_screen_name" : "01101O10",
    "in_reply_to_user_id_str" : "546382113",
    "user" : {
      "name" : "Billy Herrington",
      "screen_name" : "BossOfThisGym",
      "protected" : false,
      "id_str" : "548406157",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3347184520/09568dde2facceda2eb58bf8f211df23_normal.jpeg",
      "id" : 548406157,
      "verified" : false
    }
  },
  "id" : 366130023628935168,
  "created_at" : "Sat Aug 10 09:32:54 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Magnus Skog",
      "screen_name" : "ralphtheninja",
      "indices" : [ 3, 17 ],
      "id_str" : "39033726",
      "id" : 39033726
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bitcoin",
      "indices" : [ 22, 30 ]
    }, {
      "text" : "sx",
      "indices" : [ 82, 85 ]
    }, {
      "text" : "libbitcoin",
      "indices" : [ 90, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "366011056805511169",
  "text" : "RT @ralphtheninja: Yo #bitcoin programming geeks out there. Check out Amir Taakis #sx and #libbitcoin http://t.co/nbmhVFY3XL and http://t.c\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "bitcoin",
        "indices" : [ 3, 11 ]
      }, {
        "text" : "sx",
        "indices" : [ 63, 66 ]
      }, {
        "text" : "libbitcoin",
        "indices" : [ 71, 82 ]
      } ],
      "urls" : [ {
        "indices" : [ 83, 105 ],
        "url" : "http://t.co/nbmhVFY3XL",
        "expanded_url" : "http://sx.dyne.org/",
        "display_url" : "sx.dyne.org"
      }, {
        "indices" : [ 110, 132 ],
        "url" : "http://t.co/K1l17jyZFV",
        "expanded_url" : "http://libbitcoin.dyne.org/doc/",
        "display_url" : "libbitcoin.dyne.org/doc/"
      } ]
    },
    "geo" : {
    },
    "id_str" : "365991455396872192",
    "text" : "Yo #bitcoin programming geeks out there. Check out Amir Taakis #sx and #libbitcoin http://t.co/nbmhVFY3XL and http://t.co/K1l17jyZFV",
    "id" : 365991455396872192,
    "created_at" : "Sat Aug 10 00:22:17 +0000 2013",
    "user" : {
      "name" : "Magnus Skog",
      "screen_name" : "ralphtheninja",
      "protected" : false,
      "id_str" : "39033726",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2453119926/g7aqkk2hr8ahta7p1tnm_normal.jpeg",
      "id" : 39033726,
      "verified" : false
    }
  },
  "id" : 366011056805511169,
  "created_at" : "Sat Aug 10 01:40:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "indices" : [ 3, 12 ],
      "id_str" : "606610405",
      "id" : 606610405
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "warez",
      "indices" : [ 91, 97 ]
    } ],
    "urls" : [ {
      "indices" : [ 68, 90 ],
      "url" : "http://t.co/GRVruLg5TC",
      "expanded_url" : "http://code.google.com/p/theafien/downloads/list",
      "display_url" : "code.google.com/p/theafien/dow\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365976925363126272",
  "text" : "RT @favehigg: Of course I know how to get my Sublime Text for free: http://t.co/GRVruLg5TC #warez Jan 04, 2013",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "warez",
        "indices" : [ 77, 83 ]
      } ],
      "urls" : [ {
        "indices" : [ 54, 76 ],
        "url" : "http://t.co/GRVruLg5TC",
        "expanded_url" : "http://code.google.com/p/theafien/downloads/list",
        "display_url" : "code.google.com/p/theafien/dow\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "346726437232992256",
    "text" : "Of course I know how to get my Sublime Text for free: http://t.co/GRVruLg5TC #warez Jan 04, 2013",
    "id" : 346726437232992256,
    "created_at" : "Mon Jun 17 20:29:59 +0000 2013",
    "user" : {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "protected" : false,
      "id_str" : "606610405",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000007208981/fee678a7c91e39839aa9ca27489b1b1f_normal.png",
      "id" : 606610405,
      "verified" : false
    }
  },
  "id" : 365976925363126272,
  "created_at" : "Fri Aug 09 23:24:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "indices" : [ 3, 12 ],
      "id_str" : "606610405",
      "id" : 606610405
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 95 ],
      "url" : "http://t.co/kN84pq7mIi",
      "expanded_url" : "http://myshar.es/10nBrgQ",
      "display_url" : "myshar.es/10nBrgQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365976878932180992",
  "text" : "RT @favehigg: \u25B6 Creative People Say No \u2014 Thoughts on creativity \u2014 Medium http://t.co/kN84pq7mIi Mar 21, 2013",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 81 ],
        "url" : "http://t.co/kN84pq7mIi",
        "expanded_url" : "http://myshar.es/10nBrgQ",
        "display_url" : "myshar.es/10nBrgQ"
      } ]
    },
    "geo" : {
    },
    "id_str" : "346728038676979712",
    "text" : "\u25B6 Creative People Say No \u2014 Thoughts on creativity \u2014 Medium http://t.co/kN84pq7mIi Mar 21, 2013",
    "id" : 346728038676979712,
    "created_at" : "Mon Jun 17 20:36:20 +0000 2013",
    "user" : {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "protected" : false,
      "id_str" : "606610405",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000007208981/fee678a7c91e39839aa9ca27489b1b1f_normal.png",
      "id" : 606610405,
      "verified" : false
    }
  },
  "id" : 365976878932180992,
  "created_at" : "Fri Aug 09 23:24:22 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 81 ],
      "url" : "http://t.co/HNf325KLOg",
      "expanded_url" : "http://pgl.yoyo.org/as/serverlist.php?showintro=0;hostformat=hosts",
      "display_url" : "pgl.yoyo.org/as/serverlist.\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365976259785793536",
  "text" : "Oh Hai, Ad Networks. How about I nuke you from my network? http://t.co/HNf325KLOg",
  "id" : 365976259785793536,
  "created_at" : "Fri Aug 09 23:21:54 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 59 ],
      "url" : "http://t.co/b4ay2fGL9L",
      "expanded_url" : "http://sdbr.co/14u48eu",
      "display_url" : "sdbr.co/14u48eu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365970702064558080",
  "text" : "Why a Custom Website is so Expensive http://t.co/b4ay2fGL9L",
  "id" : 365970702064558080,
  "created_at" : "Fri Aug 09 22:59:49 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 53 ],
      "url" : "http://t.co/kjk2JZy2gS",
      "expanded_url" : "http://sdbr.co/14u3Ns6",
      "display_url" : "sdbr.co/14u3Ns6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365970524993630210",
  "text" : "iOS 7 color scheme inspiration http://t.co/kjk2JZy2gS",
  "id" : 365970524993630210,
  "created_at" : "Fri Aug 09 22:59:07 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 45 ],
      "url" : "http://t.co/eSWkWttTtO",
      "expanded_url" : "http://sdbr.co/14u36PB",
      "display_url" : "sdbr.co/14u36PB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365963348619177985",
  "text" : "CSS Absolute Centering http://t.co/eSWkWttTtO",
  "id" : 365963348619177985,
  "created_at" : "Fri Aug 09 22:30:36 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Prinzhorn",
      "screen_name" : "Prinzhorn",
      "indices" : [ 0, 10 ],
      "id_str" : "187226449",
      "id" : 187226449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "365868042481172480",
  "geo" : {
  },
  "id_str" : "365943203007967234",
  "in_reply_to_user_id" : 187226449,
  "text" : "@Prinzhorn What? Was going to say \"is another day\" like most people. But please expound!",
  "id" : 365943203007967234,
  "in_reply_to_status_id" : 365868042481172480,
  "created_at" : "Fri Aug 09 21:10:33 +0000 2013",
  "in_reply_to_screen_name" : "Prinzhorn",
  "in_reply_to_user_id_str" : "187226449",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "NiggaSayWhat",
      "indices" : [ 93, 106 ]
    } ],
    "urls" : [ {
      "indices" : [ 70, 92 ],
      "url" : "http://t.co/Bqwzqo11CG",
      "expanded_url" : "http://isharefil.es/QjFg",
      "display_url" : "isharefil.es/QjFg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365922693385691137",
  "text" : "Since when did Greasemonkey rebrand and name themselves Tampermonkey? http://t.co/Bqwzqo11CG #NiggaSayWhat?!",
  "id" : 365922693385691137,
  "created_at" : "Fri Aug 09 19:49:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 106 ],
      "url" : "http://t.co/Q7UjV4FIvS",
      "expanded_url" : "http://myshar.es/11HzMnf",
      "display_url" : "myshar.es/11HzMnf"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365883688766013442",
  "text" : "Pure. A set of small, responsive CSS modules that you can use in every web project. http://t.co/Q7UjV4FIvS",
  "id" : 365883688766013442,
  "created_at" : "Fri Aug 09 17:14:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/365882283028258816/photo/1",
      "indices" : [ 53, 75 ],
      "url" : "http://t.co/fHO6aQAerw",
      "media_url" : "http://pbs.twimg.com/media/BRPf_3XCIAI916_.jpg",
      "id_str" : "365882283032453122",
      "id" : 365882283032453122,
      "media_url_https" : "https://pbs.twimg.com/media/BRPf_3XCIAI916_.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 816,
        "resize" : "fit",
        "w" : 612
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 816,
        "resize" : "fit",
        "w" : 612
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 600
      } ],
      "display_url" : "pic.twitter.com/fHO6aQAerw"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "365882283028258816",
  "text" : "This was (and still is) the best phone ever created. http://t.co/fHO6aQAerw",
  "id" : 365882283028258816,
  "created_at" : "Fri Aug 09 17:08:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 135 ],
      "url" : "http://t.co/eh0EfTTMc6",
      "expanded_url" : "http://myshar.es/14xcAJJ",
      "display_url" : "myshar.es/14xcAJJ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365880847775170560",
  "text" : "Bookshelf is a promise based ORM for Node.js that extends the Model &amp; Collection foundations of Backbone.js. http://t.co/eh0EfTTMc6",
  "id" : 365880847775170560,
  "created_at" : "Fri Aug 09 17:02:46 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/IGbe3bvVb8",
      "expanded_url" : "http://tympanus.net/Development/CreativeLinkEffects/",
      "display_url" : "tympanus.net/Development/Cr\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365826686916366336",
  "text" : "It's being shared countless times before: http://t.co/IGbe3bvVb8 Another Codrops bomb resource. Codrops just keep doing this to us.",
  "id" : 365826686916366336,
  "created_at" : "Fri Aug 09 13:27:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "indices" : [ 3, 13 ],
      "id_str" : "605929797",
      "id" : 605929797
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "365799603498516480",
  "text" : "RT @lunchduty: Monsta FTP \u2013 Open Source PHP/Ajax FTP for Browser http://t.co/SSclMCVtJE",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/mr.-reader/id412874834?mt=8&uo=4\" rel=\"nofollow\">Mr. Reader on iOS</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 72 ],
        "url" : "http://t.co/SSclMCVtJE",
        "expanded_url" : "http://isharefil.es/Qdon",
        "display_url" : "isharefil.es/Qdon"
      } ]
    },
    "geo" : {
    },
    "id_str" : "364552200070369280",
    "text" : "Monsta FTP \u2013 Open Source PHP/Ajax FTP for Browser http://t.co/SSclMCVtJE",
    "id" : 364552200070369280,
    "created_at" : "Tue Aug 06 01:03:12 +0000 2013",
    "user" : {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "protected" : false,
      "id_str" : "605929797",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3566749477/5025656cb423825e1d0748cf13c87ca3_normal.png",
      "id" : 605929797,
      "verified" : false
    }
  },
  "id" : 365799603498516480,
  "created_at" : "Fri Aug 09 11:39:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 0, 9 ],
      "id_str" : "14139773",
      "id" : 14139773
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 42 ],
      "url" : "http://t.co/lbqaNAu1RM",
      "expanded_url" : "http://isharefil.es/QiqC",
      "display_url" : "isharefil.es/QiqC"
    }, {
      "indices" : [ 83, 106 ],
      "url" : "https://t.co/luF5YjOIxq",
      "expanded_url" : "https://search.nerdydata.com/",
      "display_url" : "search.nerdydata.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365799249469915136",
  "in_reply_to_user_id" : 14139773,
  "text" : "@jdorfman Check it: http://t.co/lbqaNAu1RM 411 results for NetDNA CDN on NerdyData https://t.co/luF5YjOIxq Awesome!",
  "id" : 365799249469915136,
  "created_at" : "Fri Aug 09 11:38:31 +0000 2013",
  "in_reply_to_screen_name" : "jdorfman",
  "in_reply_to_user_id_str" : "14139773",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https://t.co/luF5YjOIxq",
      "expanded_url" : "https://search.nerdydata.com/",
      "display_url" : "search.nerdydata.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365798853196263424",
  "text" : "Nerdydata is one powerful tool: https://t.co/luF5YjOIxq The only caveats are the limits in place, but it has great potential.",
  "id" : 365798853196263424,
  "created_at" : "Fri Aug 09 11:36:57 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "zachalbert",
      "screen_name" : "zachalbert",
      "indices" : [ 125, 136 ],
      "id_str" : "17636792",
      "id" : 17636792
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 120 ],
      "url" : "http://t.co/qHDqx0nVNp",
      "expanded_url" : "http://myshar.es/17aCJgA",
      "display_url" : "myshar.es/17aCJgA"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365648510663655424",
  "text" : "Yes! I've been waiting for someone to come up with a standards-compliant way to do *sparkle* text http://t.co/qHDqx0nVNp via @zachalbert",
  "id" : 365648510663655424,
  "created_at" : "Fri Aug 09 01:39:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "pineapple.io",
      "screen_name" : "pineappleio",
      "indices" : [ 1, 13 ],
      "id_str" : "711006900",
      "id" : 711006900
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "365640387479617536",
  "geo" : {
  },
  "id_str" : "365640980805844992",
  "in_reply_to_user_id" : 711006900,
  "text" : ".@pineappleio Inception as a service",
  "id" : 365640980805844992,
  "in_reply_to_status_id" : 365640387479617536,
  "created_at" : "Fri Aug 09 01:09:37 +0000 2013",
  "in_reply_to_screen_name" : "pineappleio",
  "in_reply_to_user_id_str" : "711006900",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "pineapple.io",
      "screen_name" : "pineappleio",
      "indices" : [ 3, 15 ],
      "id_str" : "711006900",
      "id" : 711006900
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "365636663583576064",
  "text" : "RT @pineappleio: New \"bookmark on pineapple\" button: http://t.co/kKk9BXDgqu",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 36, 58 ],
        "url" : "http://t.co/kKk9BXDgqu",
        "expanded_url" : "http://pineapple.io/discussion/feature-request-embeddable-button",
        "display_url" : "pineapple.io/discussion/fea\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "365275074707730432",
    "text" : "New \"bookmark on pineapple\" button: http://t.co/kKk9BXDgqu",
    "id" : 365275074707730432,
    "created_at" : "Thu Aug 08 00:55:38 +0000 2013",
    "user" : {
      "name" : "pineapple.io",
      "screen_name" : "pineappleio",
      "protected" : false,
      "id_str" : "711006900",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2429703606/bqruf9ya8ufsalm7uk1x_normal.png",
      "id" : 711006900,
      "verified" : false
    }
  },
  "id" : 365636663583576064,
  "created_at" : "Fri Aug 09 00:52:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "365614981594615809",
  "text" : "SaaS as a service.",
  "id" : 365614981594615809,
  "created_at" : "Thu Aug 08 23:26:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GIFART",
      "indices" : [ 116, 123 ]
    } ],
    "urls" : [ {
      "indices" : [ 93, 115 ],
      "url" : "http://t.co/MiqRlc29H0",
      "expanded_url" : "http://bisouslescopains.tumblr.com/",
      "display_url" : "bisouslescopains.tumblr.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365609530735865856",
  "text" : "I have made a life-decision never to Pocket (formerly Read It Later\u2122) any more Tumblr blogs. http://t.co/MiqRlc29H0 #GIFART",
  "id" : 365609530735865856,
  "created_at" : "Thu Aug 08 23:04:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https://t.co/Q5XsaP3zOB",
      "expanded_url" : "https://lavabit.com/",
      "display_url" : "lavabit.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365581601809240067",
  "text" : "Another compelling reason to self-host all teh things. https://t.co/Q5XsaP3zOB Farewell Lavabit, you have served me well over the years,",
  "id" : 365581601809240067,
  "created_at" : "Thu Aug 08 21:13:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http://t.co/UPzvWSYtyk",
      "expanded_url" : "http://myshar.es/11w4uvE",
      "display_url" : "myshar.es/11w4uvE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365519791752101889",
  "text" : "unveil.js - A very lightweight plugin to lazy load images http://t.co/UPzvWSYtyk",
  "id" : 365519791752101889,
  "created_at" : "Thu Aug 08 17:08:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/365518499608682496/photo/1",
      "indices" : [ 63, 85 ],
      "url" : "http://t.co/BO03NZTOrK",
      "media_url" : "http://pbs.twimg.com/media/BRKVI4iCYAAJdkt.jpg",
      "id_str" : "365518499617071104",
      "id" : 365518499617071104,
      "media_url_https" : "https://pbs.twimg.com/media/BRKVI4iCYAAJdkt.jpg",
      "sizes" : [ {
        "h" : 387,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 387,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 219,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 387,
        "resize" : "fit",
        "w" : 600
      } ],
      "display_url" : "pic.twitter.com/BO03NZTOrK"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "365518499608682496",
  "text" : "Oh China. You love building on top of other things, don't you? http://t.co/BO03NZTOrK",
  "id" : 365518499608682496,
  "created_at" : "Thu Aug 08 17:02:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Front-end Rescue",
      "screen_name" : "frontendrescue",
      "indices" : [ 3, 18 ],
      "id_str" : "1432130520",
      "id" : 1432130520
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "chrome",
      "indices" : [ 90, 97 ]
    }, {
      "text" : "browser",
      "indices" : [ 98, 106 ]
    } ],
    "urls" : [ {
      "indices" : [ 67, 89 ],
      "url" : "http://t.co/bwczOcok3B",
      "expanded_url" : "http://blog.chromium.org/2013/08/an-improved-devtools-editing-workflow.html",
      "display_url" : "blog.chromium.org/2013/08/an-imp\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "365245562523619328",
  "text" : "RT @frontendrescue: An Improved Chrome DevTools Editing Workflow - http://t.co/bwczOcok3B #chrome #browser",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "chrome",
        "indices" : [ 70, 77 ]
      }, {
        "text" : "browser",
        "indices" : [ 78, 86 ]
      } ],
      "urls" : [ {
        "indices" : [ 47, 69 ],
        "url" : "http://t.co/bwczOcok3B",
        "expanded_url" : "http://blog.chromium.org/2013/08/an-improved-devtools-editing-workflow.html",
        "display_url" : "blog.chromium.org/2013/08/an-imp\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "365153957699526658",
    "text" : "An Improved Chrome DevTools Editing Workflow - http://t.co/bwczOcok3B #chrome #browser",
    "id" : 365153957699526658,
    "created_at" : "Wed Aug 07 16:54:22 +0000 2013",
    "user" : {
      "name" : "Front-end Rescue",
      "screen_name" : "frontendrescue",
      "protected" : false,
      "id_str" : "1432130520",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3665884227/8b3c5253712648922b937cefb078c77b_normal.png",
      "id" : 1432130520,
      "verified" : false
    }
  },
  "id" : 365245562523619328,
  "created_at" : "Wed Aug 07 22:58:22 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lorcan dempsey",
      "screen_name" : "lorcanD",
      "indices" : [ 3, 11 ],
      "id_str" : "27106703",
      "id" : 27106703
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364891705000349696",
  "text" : "RT @lorcanD: Google\u2019s new search feature makes it easier to find seminal articles on... http://t.co/Fx4jfclvux",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.WindowsPhone.com/\" rel=\"nofollow\">Windows Phone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 75, 97 ],
        "url" : "http://t.co/Fx4jfclvux",
        "expanded_url" : "http://zite.to/13i8aDa",
        "display_url" : "zite.to/13i8aDa"
      } ]
    },
    "geo" : {
    },
    "id_str" : "364873295696375809",
    "text" : "Google\u2019s new search feature makes it easier to find seminal articles on... http://t.co/Fx4jfclvux",
    "id" : 364873295696375809,
    "created_at" : "Tue Aug 06 22:19:07 +0000 2013",
    "user" : {
      "name" : "lorcan dempsey",
      "screen_name" : "lorcanD",
      "protected" : false,
      "id_str" : "27106703",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/142046232/HPIM5626_normal.jpg",
      "id" : 27106703,
      "verified" : false
    }
  },
  "id" : 364891705000349696,
  "created_at" : "Tue Aug 06 23:32:16 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Boas",
      "screen_name" : "maboa",
      "indices" : [ 0, 6 ],
      "id_str" : "14288038",
      "id" : 14288038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364871359475621888",
  "geo" : {
  },
  "id_str" : "364874552666374145",
  "in_reply_to_user_id" : 14288038,
  "text" : "@maboa Leave it to the web engineers, not the web developers and creatives! There is a distinct difference between these titles.",
  "id" : 364874552666374145,
  "in_reply_to_status_id" : 364871359475621888,
  "created_at" : "Tue Aug 06 22:24:07 +0000 2013",
  "in_reply_to_screen_name" : "maboa",
  "in_reply_to_user_id_str" : "14288038",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 130, 140 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 128 ],
      "url" : "http://t.co/wDtoOU2Gcq",
      "expanded_url" : "http://f.cl.ly/items/1u1c1h3V1u2z3G0u0T1t/devlinks.txt",
      "display_url" : "f.cl.ly/items/1u1c1h3V\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364863921108168705",
  "text" : "Newly revised Devlinks ready for the masses. Currently adding Twitter accounts, adding descriptions, etc. http://t.co/wDtoOU2Gcq +@simevidas",
  "id" : 364863921108168705,
  "created_at" : "Tue Aug 06 21:41:52 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 31 ],
      "url" : "http://t.co/1QxSXfyMzs",
      "expanded_url" : "http://www.intelligentexploit.com/",
      "display_url" : "intelligentexploit.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364855075048402945",
  "text" : "Awesome: http://t.co/1QxSXfyMzs",
  "id" : 364855075048402945,
  "created_at" : "Tue Aug 06 21:06:43 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "indices" : [ 3, 13 ],
      "id_str" : "605929797",
      "id" : 605929797
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364814517781659648",
  "text" : "RT @lunchduty: \"YouTube adds 'playing' icon to browser tabs to track down blaring audio\" http://t.co/zJ209rgaTm",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/feedly/id396069556?mt=8&uo=4\" rel=\"nofollow\">feedly on iOS</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 96 ],
        "url" : "http://t.co/zJ209rgaTm",
        "expanded_url" : "http://www.theverge.com/2013/8/5/4589766/youtube-adds-play-icon-to-browser-tabs",
        "display_url" : "theverge.com/2013/8/5/45897\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "364549694741303297",
    "text" : "\"YouTube adds 'playing' icon to browser tabs to track down blaring audio\" http://t.co/zJ209rgaTm",
    "id" : 364549694741303297,
    "created_at" : "Tue Aug 06 00:53:14 +0000 2013",
    "user" : {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "protected" : false,
      "id_str" : "605929797",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3566749477/5025656cb423825e1d0748cf13c87ca3_normal.png",
      "id" : 605929797,
      "verified" : false
    }
  },
  "id" : 364814517781659648,
  "created_at" : "Tue Aug 06 18:25:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Henzel",
      "screen_name" : "davidhenzel",
      "indices" : [ 3, 15 ],
      "id_str" : "98993727",
      "id" : 98993727
    }, {
      "name" : "SpeedAwarenessMonth",
      "screen_name" : "SpeedMonth",
      "indices" : [ 103, 114 ],
      "id_str" : "632445769",
      "id" : 632445769
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 98 ],
      "url" : "http://t.co/slMrNT67Hx",
      "expanded_url" : "http://shar.es/yvuNE",
      "display_url" : "shar.es/yvuNE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364805272877154304",
  "text" : "RT @davidhenzel: WordPress Performance: Four Simple Steps For A Faster Site http://t.co/slMrNT67Hx via @speedmonth",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SpeedAwarenessMonth",
        "screen_name" : "SpeedMonth",
        "indices" : [ 86, 97 ],
        "id_str" : "632445769",
        "id" : 632445769
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 81 ],
        "url" : "http://t.co/slMrNT67Hx",
        "expanded_url" : "http://shar.es/yvuNE",
        "display_url" : "shar.es/yvuNE"
      } ]
    },
    "geo" : {
    },
    "id_str" : "364803565329448962",
    "text" : "WordPress Performance: Four Simple Steps For A Faster Site http://t.co/slMrNT67Hx via @speedmonth",
    "id" : 364803565329448962,
    "created_at" : "Tue Aug 06 17:42:02 +0000 2013",
    "user" : {
      "name" : "David Henzel",
      "screen_name" : "davidhenzel",
      "protected" : false,
      "id_str" : "98993727",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3469565692/eaba8d3ea5299f50e3d1439ddf01c2e3_normal.jpeg",
      "id" : 98993727,
      "verified" : false
    }
  },
  "id" : 364805272877154304,
  "created_at" : "Tue Aug 06 17:48:49 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/ReutersDesign/status/364757809713278976/photo/1",
      "indices" : [ 80, 102 ],
      "url" : "http://t.co/TEH4IXq1Kr",
      "media_url" : "http://pbs.twimg.com/media/BQ_hS52CcAAlsrN.png",
      "id_str" : "364757809721667584",
      "id" : 364757809721667584,
      "media_url_https" : "https://pbs.twimg.com/media/BQ_hS52CcAAlsrN.png",
      "sizes" : [ {
        "h" : 1300,
        "resize" : "fit",
        "w" : 1620
      }, {
        "h" : 273,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 481,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 822,
        "resize" : "fit",
        "w" : 1024
      } ],
      "display_url" : "pic.twitter.com/TEH4IXq1Kr"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364803205512695808",
  "text" : "A graphic showing the results from anti-alias testing across browsers and apps. http://t.co/TEH4IXq1Kr",
  "id" : 364803205512695808,
  "created_at" : "Tue Aug 06 17:40:36 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 0, 8 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "343631907839438848",
  "geo" : {
  },
  "id_str" : "364788820257943553",
  "in_reply_to_user_id" : 532923,
  "text" : "@mathias I've always maintained that if code looks ugly, and terrifying, then it is. CC classes are a hack, and don't belong in my editor!",
  "id" : 364788820257943553,
  "in_reply_to_status_id" : 343631907839438848,
  "created_at" : "Tue Aug 06 16:43:26 +0000 2013",
  "in_reply_to_screen_name" : "mathias",
  "in_reply_to_user_id_str" : "532923",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https://t.co/QEhdBYjB4u",
      "expanded_url" : "https://github.com/TedGoas/Responsive-Email-XX",
      "display_url" : "github.com/TedGoas/Respon\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364769303888793600",
  "text" : "You like responsive? You like HTML email? Check out https://t.co/QEhdBYjB4u and help make it better!",
  "id" : 364769303888793600,
  "created_at" : "Tue Aug 06 15:25:53 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http://t.co/byR397h4VB",
      "expanded_url" : "http://bit.ly/13CAa88",
      "display_url" : "bit.ly/13CAa88"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364762575696707587",
  "text" : "Chrome\u2019s insane password security strategy http://t.co/byR397h4VB",
  "id" : 364762575696707587,
  "created_at" : "Tue Aug 06 14:59:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CustomDevGuy",
      "screen_name" : "CustomDevGuy",
      "indices" : [ 0, 13 ],
      "id_str" : "1333508286",
      "id" : 1333508286
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364751984542220293",
  "geo" : {
  },
  "id_str" : "364758772368949248",
  "in_reply_to_user_id" : 1333508286,
  "text" : "@CustomDevGuy No problem!",
  "id" : 364758772368949248,
  "in_reply_to_status_id" : 364751984542220293,
  "created_at" : "Tue Aug 06 14:44:02 +0000 2013",
  "in_reply_to_screen_name" : "CustomDevGuy",
  "in_reply_to_user_id_str" : "1333508286",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 52 ],
      "url" : "https://t.co/XZHTZqMcIZ",
      "expanded_url" : "https://www.youtube.com/watch?v=XTjeoQ8gRmQ",
      "display_url" : "youtube.com/watch?v=XTjeoQ\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364747988763033601",
  "text" : "English spelling is D.U.M.B. https://t.co/XZHTZqMcIZ",
  "id" : 364747988763033601,
  "created_at" : "Tue Aug 06 14:01:11 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Harwood",
      "screen_name" : "irPhunky",
      "indices" : [ 0, 9 ],
      "id_str" : "14406543",
      "id" : 14406543
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364745274729566209",
  "geo" : {
  },
  "id_str" : "364746765427146753",
  "in_reply_to_user_id" : 14406543,
  "text" : "@irPhunky I use hotkeys for coding so I don't need more abstraction. I have a penchant for assigning 'B' and it spits the whole Bible out!",
  "id" : 364746765427146753,
  "in_reply_to_status_id" : 364745274729566209,
  "created_at" : "Tue Aug 06 13:56:20 +0000 2013",
  "in_reply_to_screen_name" : "irPhunky",
  "in_reply_to_user_id_str" : "14406543",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Kinlan",
      "screen_name" : "Paul_Kinlan",
      "indices" : [ 0, 12 ],
      "id_str" : "101025176",
      "id" : 101025176
    }, {
      "name" : "Chad Whitacre",
      "screen_name" : "whit537",
      "indices" : [ 13, 21 ],
      "id_str" : "34175404",
      "id" : 34175404
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364662607065518082",
  "geo" : {
  },
  "id_str" : "364744936849022976",
  "in_reply_to_user_id" : 101025176,
  "text" : "@Paul_Kinlan @whit537 It is feasible to use Gittip earnings to get Redbull delivered in bulk to your home which means less trips to the shop",
  "id" : 364744936849022976,
  "in_reply_to_status_id" : 364662607065518082,
  "created_at" : "Tue Aug 06 13:49:04 +0000 2013",
  "in_reply_to_screen_name" : "Paul_Kinlan",
  "in_reply_to_user_id_str" : "101025176",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Harwood",
      "screen_name" : "irPhunky",
      "indices" : [ 0, 9 ],
      "id_str" : "14406543",
      "id" : 14406543
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364742263177347072",
  "geo" : {
  },
  "id_str" : "364743258020130816",
  "in_reply_to_user_id" : 14406543,
  "text" : "@irPhunky I am all for jumping ship to learn new things, but this also affects muscle memory too. I can type &lt;footer&gt; quicker than 'ftr'",
  "id" : 364743258020130816,
  "in_reply_to_status_id" : 364742263177347072,
  "created_at" : "Tue Aug 06 13:42:23 +0000 2013",
  "in_reply_to_screen_name" : "irPhunky",
  "in_reply_to_user_id_str" : "14406543",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 119, 141 ],
      "url" : "http://t.co/niAsGgQHXI",
      "expanded_url" : "http://docs.emmet.io/cheat-sheet/",
      "display_url" : "docs.emmet.io/cheat-sheet/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364741154341785601",
  "text" : "Emmet seems like a nice idea, but meh. \nI already have HTML &amp; CSS burned into memory. Not about to rewire it again http://t.co/niAsGgQHXI",
  "id" : 364741154341785601,
  "created_at" : "Tue Aug 06 13:34:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/TTIwDDdulj",
      "expanded_url" : "http://dbinbox.com/",
      "display_url" : "dbinbox.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364699280927232000",
  "text" : "http://t.co/TTIwDDdulj allows anybody to drop files in your dropbox",
  "id" : 364699280927232000,
  "created_at" : "Tue Aug 06 10:47:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nico Prat",
      "screen_name" : "nicooprat",
      "indices" : [ 0, 10 ],
      "id_str" : "84362217",
      "id" : 84362217
    }, {
      "name" : "Alexander Prinzhorn",
      "screen_name" : "Prinzhorn",
      "indices" : [ 11, 21 ],
      "id_str" : "187226449",
      "id" : 187226449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364696524573249536",
  "geo" : {
  },
  "id_str" : "364697291262664706",
  "in_reply_to_user_id" : 84362217,
  "text" : "@nicooprat @Prinzhorn Most cron scripts use GET to achieve a result. Just sabotage the header information with POST, and you can do it?",
  "id" : 364697291262664706,
  "in_reply_to_status_id" : 364696524573249536,
  "created_at" : "Tue Aug 06 10:39:44 +0000 2013",
  "in_reply_to_screen_name" : "nicooprat",
  "in_reply_to_user_id_str" : "84362217",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Prinzhorn",
      "screen_name" : "Prinzhorn",
      "indices" : [ 1, 11 ],
      "id_str" : "187226449",
      "id" : 187226449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364692796055101440",
  "geo" : {
  },
  "id_str" : "364694931815661570",
  "in_reply_to_user_id" : 187226449,
  "text" : ".@Prinzhorn Interesting idea. Cronjobs As A Service.",
  "id" : 364694931815661570,
  "in_reply_to_status_id" : 364692796055101440,
  "created_at" : "Tue Aug 06 10:30:22 +0000 2013",
  "in_reply_to_screen_name" : "Prinzhorn",
  "in_reply_to_user_id_str" : "187226449",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dries Vints",
      "screen_name" : "DriesVints",
      "indices" : [ 1, 12 ],
      "id_str" : "226496901",
      "id" : 226496901
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "semver",
      "indices" : [ 98, 105 ]
    } ],
    "urls" : [ {
      "indices" : [ 75, 97 ],
      "url" : "http://t.co/i1uOpfUeW5",
      "expanded_url" : "http://semver.org/spec/v2.0.0.html",
      "display_url" : "semver.org/spec/v2.0.0.ht\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "364658422760747011",
  "geo" : {
  },
  "id_str" : "364662465050591234",
  "in_reply_to_user_id" : 226496901,
  "text" : "\u201C@DriesVints: If you release software, you would do well to memorise this: http://t.co/i1uOpfUeW5 #semver\u201D",
  "id" : 364662465050591234,
  "in_reply_to_status_id" : 364658422760747011,
  "created_at" : "Tue Aug 06 08:21:21 +0000 2013",
  "in_reply_to_screen_name" : "DriesVints",
  "in_reply_to_user_id_str" : "226496901",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Garrett Dimon",
      "screen_name" : "garrettdimon",
      "indices" : [ 3, 16 ],
      "id_str" : "12707",
      "id" : 12707
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364660450073784320",
  "text" : "RT @garrettdimon: One of the underrated benefits of SaaS: A forced 4+ week hiatus due to unforeseen health problems has no negative impact \u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/software/tweetbot/mac\" rel=\"nofollow\">Tweetbot for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "364513224923684865",
    "text" : "One of the underrated benefits of SaaS: A forced 4+ week hiatus due to unforeseen health problems has no negative impact on income.",
    "id" : 364513224923684865,
    "created_at" : "Mon Aug 05 22:28:19 +0000 2013",
    "user" : {
      "name" : "Garrett Dimon",
      "screen_name" : "garrettdimon",
      "protected" : false,
      "id_str" : "12707",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3493437726/fe95e0a31234182ba71e393cbbfa5447_normal.jpeg",
      "id" : 12707,
      "verified" : false
    }
  },
  "id" : 364660450073784320,
  "created_at" : "Tue Aug 06 08:13:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "rzzo",
      "screen_name" : "_rzzo",
      "indices" : [ 3, 9 ],
      "id_str" : "397405076",
      "id" : 397405076
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http://t.co/BpVF4LJXBP",
      "expanded_url" : "http://sdbr.co/1b7WacG",
      "display_url" : "sdbr.co/1b7WacG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364659523593969664",
  "text" : "RT @_rzzo: Unicorn, Shmunicorn \u2014 Be a Pegasus http://t.co/BpVF4LJXBP",
  "retweeted_status" : {
    "source" : "<a href=\"http://ifttt.com\" rel=\"nofollow\">IFTTT</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 35, 57 ],
        "url" : "http://t.co/BpVF4LJXBP",
        "expanded_url" : "http://sdbr.co/1b7WacG",
        "display_url" : "sdbr.co/1b7WacG"
      } ]
    },
    "geo" : {
    },
    "id_str" : "364572987317428224",
    "text" : "Unicorn, Shmunicorn \u2014 Be a Pegasus http://t.co/BpVF4LJXBP",
    "id" : 364572987317428224,
    "created_at" : "Tue Aug 06 02:25:48 +0000 2013",
    "user" : {
      "name" : "rzzo",
      "screen_name" : "_rzzo",
      "protected" : false,
      "id_str" : "397405076",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3442963921/e8aa4a71a6f651add0f68bd99cc6a0d5_normal.png",
      "id" : 397405076,
      "verified" : false
    }
  },
  "id" : 364659523593969664,
  "created_at" : "Tue Aug 06 08:09:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364651791939403776",
  "geo" : {
  },
  "id_str" : "364653001308246018",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Have China done another one of their hardware hacks on these yet? Pretty sure they already have :)",
  "id" : 364653001308246018,
  "in_reply_to_status_id" : 364651791939403776,
  "created_at" : "Tue Aug 06 07:43:45 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Angelina Fabbro",
      "screen_name" : "angelinamagnum",
      "indices" : [ 0, 15 ],
      "id_str" : "58708498",
      "id" : 58708498
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364593459098685442",
  "in_reply_to_user_id" : 58708498,
  "text" : "@angelinamagnum Think of the 4 in CSS4 as being infinite and all other versions like CSS3.1, CSS3.2 are failed attempts to reach it :)",
  "id" : 364593459098685442,
  "created_at" : "Tue Aug 06 03:47:09 +0000 2013",
  "in_reply_to_screen_name" : "angelinamagnum",
  "in_reply_to_user_id_str" : "58708498",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dion Almaer",
      "screen_name" : "dalmaer",
      "indices" : [ 0, 8 ],
      "id_str" : "4216361",
      "id" : 4216361
    }, {
      "name" : "Kyle",
      "screen_name" : "getify",
      "indices" : [ 9, 16 ],
      "id_str" : "16686076",
      "id" : 16686076
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364557474260451329",
  "geo" : {
  },
  "id_str" : "364590920089350144",
  "in_reply_to_user_id" : 4216361,
  "text" : "@dalmaer @getify Syntax lube?",
  "id" : 364590920089350144,
  "in_reply_to_status_id" : 364557474260451329,
  "created_at" : "Tue Aug 06 03:37:03 +0000 2013",
  "in_reply_to_screen_name" : "dalmaer",
  "in_reply_to_user_id_str" : "4216361",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kyle",
      "screen_name" : "getify",
      "indices" : [ 0, 7 ],
      "id_str" : "16686076",
      "id" : 16686076
    }, {
      "name" : "Dustin Diaz",
      "screen_name" : "ded",
      "indices" : [ 8, 12 ],
      "id_str" : "1199081",
      "id" : 1199081
    }, {
      "name" : "Dion Almaer",
      "screen_name" : "dalmaer",
      "indices" : [ 13, 21 ],
      "id_str" : "4216361",
      "id" : 4216361
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364559832642101248",
  "geo" : {
  },
  "id_str" : "364590432925130752",
  "in_reply_to_user_id" : 16686076,
  "text" : "@getify @ded @dalmaer Pretty sure aspartame is poison too. I reinforced my belief it was after Googling for Aspartame effects many times.",
  "id" : 364590432925130752,
  "in_reply_to_status_id" : 364559832642101248,
  "created_at" : "Tue Aug 06 03:35:07 +0000 2013",
  "in_reply_to_screen_name" : "getify",
  "in_reply_to_user_id_str" : "16686076",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 1, 11 ],
      "id_str" : "175727560",
      "id" : 175727560
    }, {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 72, 80 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https://t.co/Z88wbXv9ak",
      "expanded_url" : "https://blog.mozilla.org/theden/2013/07/12/what-firefox-os-means-for-you/",
      "display_url" : "blog.mozilla.org/theden/2013/07\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "364540403149582337",
  "geo" : {
  },
  "id_str" : "364541288064249856",
  "in_reply_to_user_id" : 175727560,
  "text" : "\u201C@simevidas: Why is Firefox OS important? Here are some good reasons by @codepo8: https://t.co/Z88wbXv9ak\u201D",
  "id" : 364541288064249856,
  "in_reply_to_status_id" : 364540403149582337,
  "created_at" : "Tue Aug 06 00:19:50 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jamie Mason",
      "screen_name" : "GotNoSugarBaby",
      "indices" : [ 0, 15 ],
      "id_str" : "242235575",
      "id" : 242235575
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 38 ],
      "url" : "http://t.co/bKN3xhbS49",
      "expanded_url" : "http://userscripts.org/scripts/show/164326",
      "display_url" : "userscripts.org/scripts/show/1\u2026"
    }, {
      "indices" : [ 39, 61 ],
      "url" : "http://t.co/HlpVzN4vPz",
      "expanded_url" : "http://userscripts.org/scripts/show/173603",
      "display_url" : "userscripts.org/scripts/show/1\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "364275258032795649",
  "geo" : {
  },
  "id_str" : "364540677230977024",
  "in_reply_to_user_id" : 242235575,
  "text" : "@GotNoSugarBaby http://t.co/bKN3xhbS49 http://t.co/HlpVzN4vPz There's more on the site. Search around :)",
  "id" : 364540677230977024,
  "in_reply_to_status_id" : 364275258032795649,
  "created_at" : "Tue Aug 06 00:17:24 +0000 2013",
  "in_reply_to_screen_name" : "GotNoSugarBaby",
  "in_reply_to_user_id_str" : "242235575",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PowerUp",
      "screen_name" : "powerupio",
      "indices" : [ 1, 11 ],
      "id_str" : "1305939649",
      "id" : 1305939649
    }, {
      "name" : "Chad Whitacre",
      "screen_name" : "whit537",
      "indices" : [ 13, 21 ],
      "id_str" : "34175404",
      "id" : 34175404
    }, {
      "name" : "Gittip",
      "screen_name" : "Gittip",
      "indices" : [ 116, 123 ],
      "id_str" : "1313765257",
      "id" : 1313765257
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 54 ],
      "url" : "http://t.co/dCfEgt3gRW",
      "expanded_url" : "http://thx.io",
      "display_url" : "thx.io"
    } ]
  },
  "in_reply_to_status_id_str" : "364469337395961856",
  "geo" : {
  },
  "id_str" : "364539997216849920",
  "in_reply_to_user_id" : 1305939649,
  "text" : "\u201C@powerupio: @whit537 check out http://t.co/dCfEgt3gRW. Started this weekend. We're thinking about integrating with @Gittip.\u201D",
  "id" : 364539997216849920,
  "in_reply_to_status_id" : 364469337395961856,
  "created_at" : "Tue Aug 06 00:14:42 +0000 2013",
  "in_reply_to_screen_name" : "powerupio",
  "in_reply_to_user_id_str" : "1305939649",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigitalOcean",
      "screen_name" : "digitalocean",
      "indices" : [ 3, 16 ],
      "id_str" : "457033547",
      "id" : 457033547
    }, {
      "name" : "DigitalOcean",
      "screen_name" : "digitalocean",
      "indices" : [ 88, 101 ],
      "id_str" : "457033547",
      "id" : 457033547
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https://t.co/AD0C4P3p1k",
      "expanded_url" : "https://www.digitalocean.com/blog_posts/get-paid-to-write-tutorials",
      "display_url" : "digitalocean.com/blog_posts/get\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364520477131157505",
  "text" : "RT @digitalocean: Get Paid to Write Tutorials ($50 a piece) https://t.co/AD0C4P3p1k via @DigitalOcean",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DigitalOcean",
        "screen_name" : "digitalocean",
        "indices" : [ 70, 83 ],
        "id_str" : "457033547",
        "id" : 457033547
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 42, 65 ],
        "url" : "https://t.co/AD0C4P3p1k",
        "expanded_url" : "https://www.digitalocean.com/blog_posts/get-paid-to-write-tutorials",
        "display_url" : "digitalocean.com/blog_posts/get\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "364518534472142848",
    "text" : "Get Paid to Write Tutorials ($50 a piece) https://t.co/AD0C4P3p1k via @DigitalOcean",
    "id" : 364518534472142848,
    "created_at" : "Mon Aug 05 22:49:25 +0000 2013",
    "user" : {
      "name" : "DigitalOcean",
      "screen_name" : "digitalocean",
      "protected" : false,
      "id_str" : "457033547",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2623921949/332ck3jglwnubobal3c2_normal.png",
      "id" : 457033547,
      "verified" : false
    }
  },
  "id" : 364520477131157505,
  "created_at" : "Mon Aug 05 22:57:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/15ReVl2fAA",
      "expanded_url" : "http://garfieldminusgarfield.net/",
      "display_url" : "garfieldminusgarfield.net"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364484472420306944",
  "text" : "http://t.co/15ReVl2fAA",
  "id" : 364484472420306944,
  "created_at" : "Mon Aug 05 20:34:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Do Good",
      "screen_name" : "dogood_io",
      "indices" : [ 0, 10 ],
      "id_str" : "1604272256",
      "id" : 1604272256
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "363862657394151424",
  "geo" : {
  },
  "id_str" : "364463217369563136",
  "in_reply_to_user_id" : 1604272256,
  "text" : "@dogood_io Leave my newspaper in a cafe",
  "id" : 364463217369563136,
  "in_reply_to_status_id" : 363862657394151424,
  "created_at" : "Mon Aug 05 19:09:37 +0000 2013",
  "in_reply_to_screen_name" : "dogood_io",
  "in_reply_to_user_id_str" : "1604272256",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 81 ],
      "url" : "http://t.co/RuQBQKiWsg",
      "expanded_url" : "http://www.youtube.com/watch?v=4weIasRJGw8",
      "display_url" : "youtube.com/watch?v=4weIas\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364461578852118528",
  "text" : "Another WYSIWTF (What you see is what the fuck) CSS editor http://t.co/RuQBQKiWsg When are firms going to learn real devs never use these?",
  "id" : 364461578852118528,
  "created_at" : "Mon Aug 05 19:03:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 43 ],
      "url" : "http://t.co/nAoMuyAHmQ",
      "expanded_url" : "http://mtkopone.github.io/zelect/",
      "display_url" : "mtkopone.github.io/zelect/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364420196200488960",
  "text" : "$('select').zelect() http://t.co/nAoMuyAHmQ Yummy. If this was a cake, I would eat it.",
  "id" : 364420196200488960,
  "created_at" : "Mon Aug 05 16:18:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Zeno Rocha",
      "screen_name" : "zenorocha",
      "indices" : [ 130, 140 ],
      "id_str" : "24701368",
      "id" : 24701368
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 100 ],
      "url" : "http://t.co/IpB1nsALSa",
      "expanded_url" : "http://zenorocha.github.io/jquery-github/",
      "display_url" : "zenorocha.github.io/jquery-github/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364417205137444865",
  "text" : "jQuery Github. A jQuery plugin to display your favorite Github Repositories - http://t.co/IpB1nsALSa Will be using this for sure +@zenorocha",
  "id" : 364417205137444865,
  "created_at" : "Mon Aug 05 16:06:46 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 69 ],
      "url" : "http://t.co/KH8Gku6OiS",
      "expanded_url" : "http://www.idaschmidt.dk/blog/2009/11/website-update/",
      "display_url" : "idaschmidt.dk/blog/2009/11/w\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364416507599532032",
  "text" : "Check the comments out on this page. Hilarious http://t.co/KH8Gku6OiS",
  "id" : 364416507599532032,
  "created_at" : "Mon Aug 05 16:04:00 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "indices" : [ 65, 76 ],
      "id_str" : "464887553",
      "id" : 464887553
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 39 ],
      "url" : "http://t.co/RUxszFIFQo",
      "expanded_url" : "http://myshar.es/13TkSYn",
      "display_url" : "myshar.es/13TkSYn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364399925062557696",
  "text" : "The App Dev Wiki http://t.co/RUxszFIFQo\u00A0\u2026 Excellent resource via @EdanHewitt",
  "id" : 364399925062557696,
  "created_at" : "Mon Aug 05 14:58:07 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "rzzo",
      "screen_name" : "_rzzo",
      "indices" : [ 3, 9 ],
      "id_str" : "397405076",
      "id" : 397405076
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http://t.co/G39U9040np",
      "expanded_url" : "http://sdbr.co/18Xzj2Q",
      "display_url" : "sdbr.co/18Xzj2Q"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364267938230325248",
  "text" : "RT @_rzzo: Taking Control of Image\u00A0Loading http://t.co/G39U9040np",
  "retweeted_status" : {
    "source" : "<a href=\"http://ifttt.com\" rel=\"nofollow\">IFTTT</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 32, 54 ],
        "url" : "http://t.co/G39U9040np",
        "expanded_url" : "http://sdbr.co/18Xzj2Q",
        "display_url" : "sdbr.co/18Xzj2Q"
      } ]
    },
    "geo" : {
    },
    "id_str" : "364244589437526016",
    "text" : "Taking Control of Image\u00A0Loading http://t.co/G39U9040np",
    "id" : 364244589437526016,
    "created_at" : "Mon Aug 05 04:40:52 +0000 2013",
    "user" : {
      "name" : "rzzo",
      "screen_name" : "_rzzo",
      "protected" : false,
      "id_str" : "397405076",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3442963921/e8aa4a71a6f651add0f68bd99cc6a0d5_normal.png",
      "id" : 397405076,
      "verified" : false
    }
  },
  "id" : 364267938230325248,
  "created_at" : "Mon Aug 05 06:13:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 63 ],
      "url" : "http://t.co/3xlCQ9rbHB",
      "expanded_url" : "http://isharefil.es/QcuU",
      "display_url" : "isharefil.es/QcuU"
    } ]
  },
  "in_reply_to_status_id_str" : "364235507963658241",
  "geo" : {
  },
  "id_str" : "364236535412948994",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas Got something I'm working on: http://t.co/3xlCQ9rbHB",
  "id" : 364236535412948994,
  "in_reply_to_status_id" : 364235507963658241,
  "created_at" : "Mon Aug 05 04:08:51 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364184021036568576",
  "text" : "I love when huge screen resolutions make 'responsive' sites look borked.",
  "id" : 364184021036568576,
  "created_at" : "Mon Aug 05 00:40:11 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364109033063452672",
  "geo" : {
  },
  "id_str" : "364118952446009344",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 And you have the cedilla and all! Well done. The spam I get has Emails that just spell it Acai. Glad to know somebody cares :)",
  "id" : 364118952446009344,
  "in_reply_to_status_id" : 364109033063452672,
  "created_at" : "Sun Aug 04 20:21:37 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Lema",
      "screen_name" : "chrislema",
      "indices" : [ 0, 10 ],
      "id_str" : "17370471",
      "id" : 17370471
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364113175429390336",
  "geo" : {
  },
  "id_str" : "364114024541061121",
  "in_reply_to_user_id" : 17370471,
  "text" : "@chrislema Oh so you Bufferapp'd it too. Cool. Just thought I'd let you know. Dig your stuff by the way...",
  "id" : 364114024541061121,
  "in_reply_to_status_id" : 364113175429390336,
  "created_at" : "Sun Aug 04 20:02:03 +0000 2013",
  "in_reply_to_screen_name" : "chrislema",
  "in_reply_to_user_id_str" : "17370471",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Lema",
      "screen_name" : "chrislema",
      "indices" : [ 0, 10 ],
      "id_str" : "17370471",
      "id" : 17370471
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364112198588567552",
  "geo" : {
  },
  "id_str" : "364112538058760192",
  "in_reply_to_user_id" : 17370471,
  "text" : "@chrislema Why are you bumping this link? I shared it a few hours ago. Stop firehosing us duplicate links :)",
  "id" : 364112538058760192,
  "in_reply_to_status_id" : 364112198588567552,
  "created_at" : "Sun Aug 04 19:56:08 +0000 2013",
  "in_reply_to_screen_name" : "chrislema",
  "in_reply_to_user_id_str" : "17370471",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Storey",
      "screen_name" : "dstorey",
      "indices" : [ 0, 8 ],
      "id_str" : "72623",
      "id" : 72623
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364109871639052289",
  "geo" : {
  },
  "id_str" : "364111948851318784",
  "in_reply_to_user_id" : 72623,
  "text" : "@dstorey The lack of chocolate is a turn off. Was looking for pics of the actual bar. Would hate to imagine eating big chunks of dried beef",
  "id" : 364111948851318784,
  "in_reply_to_status_id" : 364109871639052289,
  "created_at" : "Sun Aug 04 19:53:48 +0000 2013",
  "in_reply_to_screen_name" : "dstorey",
  "in_reply_to_user_id_str" : "72623",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364099224352194560",
  "text" : "Maybe I'm an information hipster or whatever, but why the sudden storm of Peter-plays-Doctor-Who tweets. Old news mon freres,...Old...",
  "id" : 364099224352194560,
  "created_at" : "Sun Aug 04 19:03:14 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https://t.co/NxqgS8IkLL",
      "expanded_url" : "https://github.com/meltingice/flickr-store",
      "display_url" : "github.com/meltingice/fli\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364071794400964610",
  "text" : "Store arbitrary data with your 1TB Flickr cloud drive by encoding any file as a PNG. https://t.co/NxqgS8IkLL ;)",
  "id" : 364071794400964610,
  "created_at" : "Sun Aug 04 17:14:14 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 47 ],
      "url" : "http://t.co/PiscUHvxdB",
      "expanded_url" : "http://chrislema.com/is-your-website-working/",
      "display_url" : "chrislema.com/is-your-websit\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364067371863916544",
  "text" : "Is your website working? http://t.co/PiscUHvxdB",
  "id" : 364067371863916544,
  "created_at" : "Sun Aug 04 16:56:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/364064822624653312/photo/1",
      "indices" : [ 117, 139 ],
      "url" : "http://t.co/u5SdMwff1Q",
      "media_url" : "http://pbs.twimg.com/media/BQ1rBvwCEAEMIbJ.png",
      "id_str" : "364064822628847617",
      "id" : 364064822628847617,
      "media_url_https" : "https://pbs.twimg.com/media/BQ1rBvwCEAEMIbJ.png",
      "sizes" : [ {
        "h" : 349,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 596,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 198,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 689,
        "resize" : "fit",
        "w" : 1184
      } ],
      "display_url" : "pic.twitter.com/u5SdMwff1Q"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364064822624653312",
  "text" : "Still amazes me to see no CTA on these types of scam pages. Where do I buy the diet pills. Where's the shiny button? http://t.co/u5SdMwff1Q",
  "id" : 364064822624653312,
  "created_at" : "Sun Aug 04 16:46:32 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anuj Agarwal",
      "screen_name" : "_anuj",
      "indices" : [ 0, 6 ],
      "id_str" : "81586048",
      "id" : 81586048
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364062744518987779",
  "geo" : {
  },
  "id_str" : "364064065800245249",
  "in_reply_to_user_id" : 81586048,
  "text" : "@_anuj Were you hacked?",
  "id" : 364064065800245249,
  "in_reply_to_status_id" : 364062744518987779,
  "created_at" : "Sun Aug 04 16:43:31 +0000 2013",
  "in_reply_to_screen_name" : "_anuj",
  "in_reply_to_user_id_str" : "81586048",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "npm",
      "screen_name" : "npmjs",
      "indices" : [ 3, 9 ],
      "id_str" : "309528017",
      "id" : 309528017
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364062544240971776",
  "text" : "RT @npmjs: sorry, been experiencing some sporadic downtime over the last 8 hours. it will be papered over soon, and root-cause fixed in a f\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "364057337419415553",
    "text" : "sorry, been experiencing some sporadic downtime over the last 8 hours. it will be papered over soon, and root-cause fixed in a few hours.",
    "id" : 364057337419415553,
    "created_at" : "Sun Aug 04 16:16:47 +0000 2013",
    "user" : {
      "name" : "npm",
      "screen_name" : "npmjs",
      "protected" : false,
      "id_str" : "309528017",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2536744912/lejvzrnlpb308aftn31u_normal.png",
      "id" : 309528017,
      "verified" : false
    }
  },
  "id" : 364062544240971776,
  "created_at" : "Sun Aug 04 16:37:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CSS",
      "indices" : [ 53, 57 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 33 ],
      "url" : "http://t.co/lHY0kEdOFU",
      "expanded_url" : "http://codepen.io/meanyack/pen/KIJdD",
      "display_url" : "codepen.io/meanyack/pen/K\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364043383766917122",
  "text" : "yay this \n\nhttp://t.co/lHY0kEdOFU \n\nExcessive use of #CSS transforms though!?",
  "id" : 364043383766917122,
  "created_at" : "Sun Aug 04 15:21:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 137 ],
      "url" : "https://t.co/gchMceq9BY",
      "expanded_url" : "https://alpha.app.net/dh/post/8445917",
      "display_url" : "alpha.app.net/dh/post/8445917"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364041673984462848",
  "text" : "I rarely visit the Google Plus desktop site now. It's such a turn off. It's clunky, wastes loads of white space,\u2026 https://t.co/gchMceq9BY",
  "id" : 364041673984462848,
  "created_at" : "Sun Aug 04 15:14:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/364036423550111744/photo/1",
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/PM9egFkIz4",
      "media_url" : "http://pbs.twimg.com/media/BQ1RMs9CEAI6YOY.jpg",
      "id_str" : "364036423554306050",
      "id" : 364036423554306050,
      "media_url_https" : "https://pbs.twimg.com/media/BQ1RMs9CEAI6YOY.jpg",
      "sizes" : [ {
        "h" : 450,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 255,
        "resize" : "fit",
        "w" : 340
      } ],
      "display_url" : "pic.twitter.com/PM9egFkIz4"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364025798786695170",
  "geo" : {
  },
  "id_str" : "364036423550111744",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas This might shed some light on it. http://t.co/PM9egFkIz4",
  "id" : 364036423550111744,
  "in_reply_to_status_id" : 364025798786695170,
  "created_at" : "Sun Aug 04 14:53:41 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "364027659455430660",
  "geo" : {
  },
  "id_str" : "364035692466155520",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas Yeah. My `N` key is worn out from using it on Twitter!",
  "id" : 364035692466155520,
  "in_reply_to_status_id" : 364027659455430660,
  "created_at" : "Sun Aug 04 14:50:47 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "FunSundays",
      "indices" : [ 84, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364034856482635777",
  "text" : "Let's play a fun game today called applying your knowledge to real world scenarios! #FunSundays",
  "id" : 364034856482635777,
  "created_at" : "Sun Aug 04 14:47:27 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 63 ],
      "url" : "http://t.co/dOlvj6SKAv",
      "expanded_url" : "http://www.youtube.com/watch?v=6U3z-PqEr5k",
      "display_url" : "youtube.com/watch?v=6U3z-P\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "364034298174636034",
  "text" : "Promoting Yourself as a Developer Online http://t.co/dOlvj6SKAv Twitter is a big one here...",
  "id" : 364034298174636034,
  "created_at" : "Sun Aug 04 14:45:14 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Winning",
      "indices" : [ 108, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "364017475769217025",
  "text" : "My new hobby on Twitter is following accounts that only retweet stuff, and then turning off their retweets. #Winning",
  "id" : 364017475769217025,
  "created_at" : "Sun Aug 04 13:38:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Kosloski",
      "screen_name" : "mttkoski",
      "indices" : [ 56, 65 ],
      "id_str" : "157198470",
      "id" : 157198470
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 51 ],
      "url" : "http://t.co/gHeezyppaM",
      "expanded_url" : "http://matthewkosloski.me/labs/pixem/",
      "display_url" : "matthewkosloski.me/labs/pixem/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363964009999237120",
  "text" : "Pixem \u2014 A PX to EM converter http://t.co/gHeezyppaM via @mttkoski",
  "id" : 363964009999237120,
  "created_at" : "Sun Aug 04 10:05:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "363962281061011456",
  "text" : "The Twitterbots are replying to each other. Skynet is happening people. Shit just got weird.",
  "id" : 363962281061011456,
  "created_at" : "Sun Aug 04 09:59:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http://t.co/OlYuTsch2Z",
      "expanded_url" : "http://vimeo.com/70941166",
      "display_url" : "vimeo.com/70941166"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363958207200636928",
  "text" : "The epic has just increased by a Thousand: http://t.co/OlYuTsch2Z",
  "id" : 363958207200636928,
  "created_at" : "Sun Aug 04 09:42:53 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "pmilkman",
      "screen_name" : "PMilkman",
      "indices" : [ 3, 12 ],
      "id_str" : "15123599",
      "id" : 15123599
    }, {
      "name" : "David H.",
      "screen_name" : "alphenic",
      "indices" : [ 70, 79 ],
      "id_str" : "468853739",
      "id" : 468853739
    }, {
      "name" : "Chris Lema",
      "screen_name" : "chrislema",
      "indices" : [ 80, 90 ],
      "id_str" : "17370471",
      "id" : 17370471
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/HEAHH5lSw9",
      "expanded_url" : "http://www.youtube.com/watch?v=-ReiC_Zh3L0&sns=tw",
      "display_url" : "youtube.com/watch?v=-ReiC_\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363821952752185344",
  "text" : "RT @PMilkman: Clients need to watch this. http://t.co/HEAHH5lSw9 (via @alphenic @chrislema)",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "David H.",
        "screen_name" : "alphenic",
        "indices" : [ 56, 65 ],
        "id_str" : "468853739",
        "id" : 468853739
      }, {
        "name" : "Chris Lema",
        "screen_name" : "chrislema",
        "indices" : [ 66, 76 ],
        "id_str" : "17370471",
        "id" : 17370471
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 28, 50 ],
        "url" : "http://t.co/HEAHH5lSw9",
        "expanded_url" : "http://www.youtube.com/watch?v=-ReiC_Zh3L0&sns=tw",
        "display_url" : "youtube.com/watch?v=-ReiC_\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "363820913147379713",
    "geo" : {
    },
    "id_str" : "363821568503197699",
    "in_reply_to_user_id" : 468853739,
    "text" : "Clients need to watch this. http://t.co/HEAHH5lSw9 (via @alphenic @chrislema)",
    "id" : 363821568503197699,
    "in_reply_to_status_id" : 363820913147379713,
    "created_at" : "Sun Aug 04 00:39:56 +0000 2013",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "pmilkman",
      "screen_name" : "PMilkman",
      "protected" : false,
      "id_str" : "15123599",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000139121353/291ed1e3c3e057d8ec8b18ca693efd80_normal.png",
      "id" : 15123599,
      "verified" : false
    }
  },
  "id" : 363821952752185344,
  "created_at" : "Sun Aug 04 00:41:27 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "indices" : [ 3, 16 ],
      "id_str" : "154206772",
      "id" : 154206772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "363813371575296000",
  "text" : "RT @a_antonellis: Vacationing is such an unintelligent look",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "363812366536171520",
    "text" : "Vacationing is such an unintelligent look",
    "id" : 363812366536171520,
    "created_at" : "Sun Aug 04 00:03:22 +0000 2013",
    "user" : {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "protected" : false,
      "id_str" : "154206772",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1904673216/cmy_normal.gif",
      "id" : 154206772,
      "verified" : false
    }
  },
  "id" : 363813371575296000,
  "created_at" : "Sun Aug 04 00:07:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 3, 12 ],
      "id_str" : "14139773",
      "id" : 14139773
    }, {
      "name" : "DreamHost",
      "screen_name" : "DreamHost",
      "indices" : [ 49, 59 ],
      "id_str" : "14217022",
      "id" : 14217022
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DreamObjects",
      "indices" : [ 62, 75 ]
    }, {
      "text" : "DreamCon",
      "indices" : [ 124, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 101, 123 ],
      "url" : "http://t.co/v9fljnFMil",
      "expanded_url" : "http://blog.netdna.com/developer/getting-started-with-dreamobjects-netdna-maxcdn-api/",
      "display_url" : "blog.netdna.com/developer/gett\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363758941399826433",
  "text" : "RT @jdorfman: If you are looking to add a CDN to @DreamHost's #DreamObjects check out this tutorial: http://t.co/v9fljnFMil #DreamCon",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DreamHost",
        "screen_name" : "DreamHost",
        "indices" : [ 35, 45 ],
        "id_str" : "14217022",
        "id" : 14217022
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DreamObjects",
        "indices" : [ 48, 61 ]
      }, {
        "text" : "DreamCon",
        "indices" : [ 110, 119 ]
      } ],
      "urls" : [ {
        "indices" : [ 87, 109 ],
        "url" : "http://t.co/v9fljnFMil",
        "expanded_url" : "http://blog.netdna.com/developer/getting-started-with-dreamobjects-netdna-maxcdn-api/",
        "display_url" : "blog.netdna.com/developer/gett\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "363721269230841856",
    "text" : "If you are looking to add a CDN to @DreamHost's #DreamObjects check out this tutorial: http://t.co/v9fljnFMil #DreamCon",
    "id" : 363721269230841856,
    "created_at" : "Sat Aug 03 18:01:22 +0000 2013",
    "user" : {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "protected" : false,
      "id_str" : "14139773",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000022089014/6856897bd33cacb205b161437b1b1c0f_normal.png",
      "id" : 14139773,
      "verified" : false
    }
  },
  "id" : 363758941399826433,
  "created_at" : "Sat Aug 03 20:31:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https://t.co/Hne6KClGSM",
      "expanded_url" : "https://github.com/tfrce/social-buttons-server",
      "display_url" : "github.com/tfrce/social-b\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363475443652886528",
  "text" : "Social Buttons Server https://t.co/Hne6KClGSM",
  "id" : 363475443652886528,
  "created_at" : "Sat Aug 03 01:44:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "indices" : [ 3, 9 ],
      "id_str" : "2367111",
      "id" : 2367111
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http://t.co/8e8SF9KHSF",
      "expanded_url" : "http://bit.ly/13BzYkQ",
      "display_url" : "bit.ly/13BzYkQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363439404968579072",
  "text" : "RT @assaf: Success takes luck: how to increase serendipity in your life http://t.co/8e8SF9KHSF",
  "retweeted_status" : {
    "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 83 ],
        "url" : "http://t.co/8e8SF9KHSF",
        "expanded_url" : "http://bit.ly/13BzYkQ",
        "display_url" : "bit.ly/13BzYkQ"
      } ]
    },
    "geo" : {
    },
    "id_str" : "363437320017416193",
    "text" : "Success takes luck: how to increase serendipity in your life http://t.co/8e8SF9KHSF",
    "id" : 363437320017416193,
    "created_at" : "Fri Aug 02 23:13:04 +0000 2013",
    "user" : {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "protected" : false,
      "id_str" : "2367111",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000045323076/8355e0bb4b3b27a90ee35497cf8c34e5_normal.jpeg",
      "id" : 2367111,
      "verified" : false
    }
  },
  "id" : 363439404968579072,
  "created_at" : "Fri Aug 02 23:21:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Taylor Jasko",
      "screen_name" : "tjasko",
      "indices" : [ 0, 7 ],
      "id_str" : "17503752",
      "id" : 17503752
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "363409338930434048",
  "geo" : {
  },
  "id_str" : "363411363692949504",
  "in_reply_to_user_id" : 17503752,
  "text" : "@tjasko Very rare to see a multi-page post that actually inspires. I usually give up at page four. Nice find!",
  "id" : 363411363692949504,
  "in_reply_to_status_id" : 363409338930434048,
  "created_at" : "Fri Aug 02 21:29:55 +0000 2013",
  "in_reply_to_screen_name" : "tjasko",
  "in_reply_to_user_id_str" : "17503752",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 0, 12 ],
      "id_str" : "793830",
      "id" : 793830
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 134 ],
      "url" : "http://t.co/CVKTNEvMg2",
      "expanded_url" : "http://myshar.es/11HzM6R",
      "display_url" : "myshar.es/11HzM6R"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363346966052614144",
  "in_reply_to_user_id" : 793830,
  "text" : "@chriscoyier recreates hover boxes with icons that expand horizontally then vertically, filling the parent box. http://t.co/CVKTNEvMg2",
  "id" : 363346966052614144,
  "created_at" : "Fri Aug 02 17:14:02 +0000 2013",
  "in_reply_to_screen_name" : "chriscoyier",
  "in_reply_to_user_id_str" : "793830",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/363345463010799619/photo/1",
      "indices" : [ 48, 70 ],
      "url" : "http://t.co/duAawU4EJg",
      "media_url" : "http://pbs.twimg.com/media/BQrcxgYCMAAlK5k.jpg",
      "id_str" : "363345463019188224",
      "id" : 363345463019188224,
      "media_url_https" : "https://pbs.twimg.com/media/BQrcxgYCMAAlK5k.jpg",
      "sizes" : [ {
        "h" : 334,
        "resize" : "fit",
        "w" : 825
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 138,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 243,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 334,
        "resize" : "fit",
        "w" : 825
      } ],
      "display_url" : "pic.twitter.com/duAawU4EJg"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "363345463010799619",
  "text" : "My time spent on my devices never ends you know http://t.co/duAawU4EJg",
  "id" : 363345463010799619,
  "created_at" : "Fri Aug 02 17:08:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 90 ],
      "url" : "http://t.co/cr0HhpSsRe",
      "expanded_url" : "http://myshar.es/13DlAu6",
      "display_url" : "myshar.es/13DlAu6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363344197341814785",
  "text" : "On the politics, cargo-culting, and maintainability of JavaScript - http://t.co/cr0HhpSsRe",
  "id" : 363344197341814785,
  "created_at" : "Fri Aug 02 17:03:01 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/363342158285127680/photo/1",
      "indices" : [ 21, 43 ],
      "url" : "http://t.co/VOg002U7LY",
      "media_url" : "http://pbs.twimg.com/media/BQrZxJTCQAEmddE.png",
      "id_str" : "363342158289321985",
      "id" : 363342158289321985,
      "media_url_https" : "https://pbs.twimg.com/media/BQrZxJTCQAEmddE.png",
      "sizes" : [ {
        "h" : 267,
        "resize" : "fit",
        "w" : 525
      }, {
        "h" : 267,
        "resize" : "fit",
        "w" : 525
      }, {
        "h" : 173,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 267,
        "resize" : "fit",
        "w" : 525
      } ],
      "display_url" : "pic.twitter.com/VOg002U7LY"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "363342158285127680",
  "text" : "This is bad, mmmkay. http://t.co/VOg002U7LY",
  "id" : 363342158285127680,
  "created_at" : "Fri Aug 02 16:54:55 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter Boling",
      "screen_name" : "galtzo",
      "indices" : [ 3, 10 ],
      "id_str" : "44917524",
      "id" : 44917524
    }, {
      "name" : "Pingdom",
      "screen_name" : "pingdom",
      "indices" : [ 71, 79 ],
      "id_str" : "15674759",
      "id" : 15674759
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 66 ],
      "url" : "http://t.co/gkwoHPoh1u",
      "expanded_url" : "http://royal.pingdom.com/2012/07/24/best-cdn-for-jquery-in-2012/",
      "display_url" : "royal.pingdom.com/2012/07/24/bes\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363319532720771072",
  "text" : "RT @galtzo: The best CDN for jQuery in 2012 http://t.co/gkwoHPoh1u via @pingdom",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Pingdom",
        "screen_name" : "pingdom",
        "indices" : [ 59, 67 ],
        "id_str" : "15674759",
        "id" : 15674759
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 32, 54 ],
        "url" : "http://t.co/gkwoHPoh1u",
        "expanded_url" : "http://royal.pingdom.com/2012/07/24/best-cdn-for-jquery-in-2012/",
        "display_url" : "royal.pingdom.com/2012/07/24/bes\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "363310353448321025",
    "text" : "The best CDN for jQuery in 2012 http://t.co/gkwoHPoh1u via @pingdom",
    "id" : 363310353448321025,
    "created_at" : "Fri Aug 02 14:48:32 +0000 2013",
    "user" : {
      "name" : "Peter Boling",
      "screen_name" : "galtzo",
      "protected" : false,
      "id_str" : "44917524",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3439447247/dad21147a9a5ada6752de7eaebfe963d_normal.jpeg",
      "id" : 44917524,
      "verified" : false
    }
  },
  "id" : 363319532720771072,
  "created_at" : "Fri Aug 02 15:25:01 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jamie Mason",
      "screen_name" : "GotNoSugarBaby",
      "indices" : [ 0, 15 ],
      "id_str" : "242235575",
      "id" : 242235575
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http://t.co/kdtlrDGq6q",
      "expanded_url" : "http://isharefil.es/QbEI",
      "display_url" : "isharefil.es/QbEI"
    } ]
  },
  "in_reply_to_status_id_str" : "363278230054109184",
  "geo" : {
  },
  "id_str" : "363291965523116033",
  "in_reply_to_user_id" : 242235575,
  "text" : "@GotNoSugarBaby Only works on Smart phones. Asked me to download some dubious app onto this http://t.co/kdtlrDGq6q I knew there was a catch!",
  "id" : 363291965523116033,
  "in_reply_to_status_id" : 363278230054109184,
  "created_at" : "Fri Aug 02 13:35:28 +0000 2013",
  "in_reply_to_screen_name" : "GotNoSugarBaby",
  "in_reply_to_user_id_str" : "242235575",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "meritocracy",
      "indices" : [ 55, 67 ]
    } ],
    "urls" : [ {
      "indices" : [ 32, 54 ],
      "url" : "http://t.co/pCCKfsMCA8",
      "expanded_url" : "http://isharefil.es/QbL6",
      "display_url" : "isharefil.es/QbL6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363092994116685824",
  "text" : "Pretty much sums up Hackernews: http://t.co/pCCKfsMCA8 #meritocracy",
  "id" : 363092994116685824,
  "created_at" : "Fri Aug 02 00:24:50 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matt Sigl",
      "screen_name" : "mattsigl",
      "indices" : [ 3, 12 ],
      "id_str" : "24967313",
      "id" : 24967313
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "363076209011589120",
  "text" : "RT @mattsigl: Tumblr is a search engine for the human subconscious.",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "363063568129732609",
    "text" : "Tumblr is a search engine for the human subconscious.",
    "id" : 363063568129732609,
    "created_at" : "Thu Aug 01 22:27:54 +0000 2013",
    "user" : {
      "name" : "Matt Sigl",
      "screen_name" : "mattsigl",
      "protected" : false,
      "id_str" : "24967313",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1563354165/309498_10150380077060862_720095861_10408740_1972832105_n_normal.jpg",
      "id" : 24967313,
      "verified" : false
    }
  },
  "id" : 363076209011589120,
  "created_at" : "Thu Aug 01 23:18:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 3, 12 ],
      "id_str" : "14139773",
      "id" : 14139773
    }, {
      "name" : "Paul Graham",
      "screen_name" : "paulg",
      "indices" : [ 77, 83 ],
      "id_str" : "183749519",
      "id" : 183749519
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iagree",
      "indices" : [ 84, 91 ]
    } ],
    "urls" : [ {
      "indices" : [ 50, 72 ],
      "url" : "http://t.co/BBMYBIP4SQ",
      "expanded_url" : "http://buff.ly/16n1kk7",
      "display_url" : "buff.ly/16n1kk7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363053211961524224",
  "text" : "RT @jdorfman: Reference for an Imaginary Standard http://t.co/BBMYBIP4SQ via @paulg #iagree",
  "retweeted_status" : {
    "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Paul Graham",
        "screen_name" : "paulg",
        "indices" : [ 63, 69 ],
        "id_str" : "183749519",
        "id" : 183749519
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "iagree",
        "indices" : [ 70, 77 ]
      } ],
      "urls" : [ {
        "indices" : [ 36, 58 ],
        "url" : "http://t.co/BBMYBIP4SQ",
        "expanded_url" : "http://buff.ly/16n1kk7",
        "display_url" : "buff.ly/16n1kk7"
      } ]
    },
    "geo" : {
    },
    "id_str" : "363046399283593216",
    "text" : "Reference for an Imaginary Standard http://t.co/BBMYBIP4SQ via @paulg #iagree",
    "id" : 363046399283593216,
    "created_at" : "Thu Aug 01 21:19:41 +0000 2013",
    "user" : {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "protected" : false,
      "id_str" : "14139773",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000022089014/6856897bd33cacb205b161437b1b1c0f_normal.png",
      "id" : 14139773,
      "verified" : false
    }
  },
  "id" : 363053211961524224,
  "created_at" : "Thu Aug 01 21:46:45 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Francisco Ribeiro",
      "screen_name" : "blackthorne",
      "indices" : [ 1, 13 ],
      "id_str" : "9874712",
      "id" : 9874712
    }, {
      "name" : "Dan Goodin",
      "screen_name" : "dangoodin001",
      "indices" : [ 14, 27 ],
      "id_str" : "14150736",
      "id" : 14150736
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "363050914254295041",
  "geo" : {
  },
  "id_str" : "363052544593256448",
  "in_reply_to_user_id" : 9874712,
  "text" : ".@blackthorne @dangoodin001 Ah yes, my dear friend &lt;IFRAME&gt; up to its usual tricks. What's new?",
  "id" : 363052544593256448,
  "in_reply_to_status_id" : 363050914254295041,
  "created_at" : "Thu Aug 01 21:44:06 +0000 2013",
  "in_reply_to_screen_name" : "blackthorne",
  "in_reply_to_user_id_str" : "9874712",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nick Thompson",
      "screen_name" : "nick_ian",
      "indices" : [ 3, 12 ],
      "id_str" : "14874573",
      "id" : 14874573
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "363035814768291841",
  "text" : "RT @nick_ian: Why is it that every time I go to Starbucks for Internet, I find out it was easier to get online in 1993 with a 14.4k modem?",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "363035305726590976",
    "text" : "Why is it that every time I go to Starbucks for Internet, I find out it was easier to get online in 1993 with a 14.4k modem?",
    "id" : 363035305726590976,
    "created_at" : "Thu Aug 01 20:35:36 +0000 2013",
    "user" : {
      "name" : "Nick Thompson",
      "screen_name" : "nick_ian",
      "protected" : false,
      "id_str" : "14874573",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3729261874/1707fd969cc246782e263f1432e96e4b_normal.jpeg",
      "id" : 14874573,
      "verified" : false
    }
  },
  "id" : 363035814768291841,
  "created_at" : "Thu Aug 01 20:37:37 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 0, 10 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "363033899397832704",
  "geo" : {
  },
  "id_str" : "363034531709718529",
  "in_reply_to_user_id" : 9866582,
  "text" : "@film_girl Now it's your personal mission to upload a Terrabyte of data just so you can make your colleagues jelly.",
  "id" : 363034531709718529,
  "in_reply_to_status_id" : 363033899397832704,
  "created_at" : "Thu Aug 01 20:32:31 +0000 2013",
  "in_reply_to_screen_name" : "film_girl",
  "in_reply_to_user_id_str" : "9866582",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrianne Jeffries",
      "screen_name" : "adrjeffries",
      "indices" : [ 1, 13 ],
      "id_str" : "16131833",
      "id" : 16131833
    }, {
      "name" : "The Verge",
      "screen_name" : "verge",
      "indices" : [ 14, 20 ],
      "id_str" : "275686563",
      "id" : 275686563
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "infosec",
      "indices" : [ 70, 78 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "363024383494721536",
  "geo" : {
  },
  "id_str" : "363028800038117376",
  "in_reply_to_user_id" : 16131833,
  "text" : ".@adrjeffries @verge Yup. Grokked the leak was a catalyst to make the #infosec industry Faster, Better, and Stronger. $NSA's plan has worked",
  "id" : 363028800038117376,
  "in_reply_to_status_id" : 363024383494721536,
  "created_at" : "Thu Aug 01 20:09:45 +0000 2013",
  "in_reply_to_screen_name" : "adrjeffries",
  "in_reply_to_user_id_str" : "16131833",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "363021230535745536",
  "geo" : {
  },
  "id_str" : "363021476129013760",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 LOL. Only copping this now. They are crafty folk indeed!",
  "id" : 363021476129013760,
  "in_reply_to_status_id" : 363021230535745536,
  "created_at" : "Thu Aug 01 19:40:39 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "363021321375981569",
  "geo" : {
  },
  "id_str" : "363021378447876097",
  "in_reply_to_user_id" : 468853739,
  "text" : "@codepo8 Ohhhh. Nice!",
  "id" : 363021378447876097,
  "in_reply_to_status_id" : 363021321375981569,
  "created_at" : "Thu Aug 01 19:40:15 +0000 2013",
  "in_reply_to_screen_name" : "alphenic",
  "in_reply_to_user_id_str" : "468853739",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 31 ],
      "url" : "http://t.co/SOtDrjTAy6",
      "expanded_url" : "http://www.b3tards.com/u/891c6e281fca81bdb92b/skypo.gif",
      "display_url" : "b3tards.com/u/891c6e281fca\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "363021230535745536",
  "geo" : {
  },
  "id_str" : "363021321375981569",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 http://t.co/SOtDrjTAy6",
  "id" : 363021321375981569,
  "in_reply_to_status_id" : 363021230535745536,
  "created_at" : "Thu Aug 01 19:40:02 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "363020763902648320",
  "geo" : {
  },
  "id_str" : "363021083730903040",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Seems like it. I was just curious as to why you didn't hotlink directly, as per your style ;)",
  "id" : 363021083730903040,
  "in_reply_to_status_id" : 363020763902648320,
  "created_at" : "Thu Aug 01 19:39:05 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 83 ],
      "url" : "http://t.co/60d84KKskt",
      "expanded_url" : "http://isharefil.es/QaL9",
      "display_url" : "isharefil.es/QaL9"
    } ]
  },
  "in_reply_to_status_id_str" : "363018721524056064",
  "geo" : {
  },
  "id_str" : "363019469968244737",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Interesting approach to how they display the image: http://t.co/60d84KKskt Says .GIF in URL, but HTML instead :???",
  "id" : 363019469968244737,
  "in_reply_to_status_id" : 363018721524056064,
  "created_at" : "Thu Aug 01 19:32:40 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Travis Wimer",
      "screen_name" : "Travis_Wimer",
      "indices" : [ 0, 13 ],
      "id_str" : "26703797",
      "id" : 26703797
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 140 ],
      "url" : "http://t.co/1agmX6xmNx",
      "expanded_url" : "http://youtu.be/g5mFbgxMHqQ",
      "display_url" : "youtu.be/g5mFbgxMHqQ"
    } ]
  },
  "in_reply_to_status_id_str" : "363014578038259712",
  "geo" : {
  },
  "id_str" : "363015492451041280",
  "in_reply_to_user_id" : 26703797,
  "text" : "@Travis_Wimer User's fault if he/she doesn't mess with default privacy settings ;) I'm more worried about sidejacking http://t.co/1agmX6xmNx",
  "id" : 363015492451041280,
  "in_reply_to_status_id" : 363014578038259712,
  "created_at" : "Thu Aug 01 19:16:52 +0000 2013",
  "in_reply_to_screen_name" : "Travis_Wimer",
  "in_reply_to_user_id_str" : "26703797",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http://t.co/vdlEF8DbNy",
      "expanded_url" : "http://www.wired.com/gadgetlab/2013/07/embeds-facebooks-real-public-offering/?mbid=social10330174",
      "display_url" : "wired.com/gadgetlab/2013\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "363013108983279616",
  "text" : "Wow, this is a great step away from the walled garden effect of Facebook: http://t.co/vdlEF8DbNy Embedded Facebook posts. Sweet!",
  "id" : 363013108983279616,
  "created_at" : "Thu Aug 01 19:07:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "362998433046462465",
  "geo" : {
  },
  "id_str" : "362998748487491585",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 'MURICA! Go red team!",
  "id" : 362998748487491585,
  "in_reply_to_status_id" : 362998433046462465,
  "created_at" : "Thu Aug 01 18:10:20 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SAM2013",
      "indices" : [ 130, 138 ]
    } ],
    "urls" : [ {
      "indices" : [ 107, 129 ],
      "url" : "http://t.co/PQzibqKCIq",
      "expanded_url" : "http://www.speedawarenessmonth.com/",
      "display_url" : "speedawarenessmonth.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362998224774107136",
  "text" : "Speed Awareness Month is here, and the blog needs more authors! Help make the web faster and write a post: http://t.co/PQzibqKCIq #SAM2013",
  "id" : 362998224774107136,
  "created_at" : "Thu Aug 01 18:08:15 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dmitriy A.",
      "screen_name" : "jimaek",
      "indices" : [ 3, 10 ],
      "id_str" : "141747302",
      "id" : 141747302
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http://t.co/poUOhL0sb9",
      "expanded_url" : "http://wpmu.org/web-hosting-review-so-just-who-is-the-best/",
      "display_url" : "wpmu.org/web-hosting-re\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362992743896723456",
  "text" : "RT @jimaek: We secretly reviewed WordPress hosts - here's what we found - http://t.co/poUOhL0sb9",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 84 ],
        "url" : "http://t.co/poUOhL0sb9",
        "expanded_url" : "http://wpmu.org/web-hosting-review-so-just-who-is-the-best/",
        "display_url" : "wpmu.org/web-hosting-re\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "362990559704530946",
    "text" : "We secretly reviewed WordPress hosts - here's what we found - http://t.co/poUOhL0sb9",
    "id" : 362990559704530946,
    "created_at" : "Thu Aug 01 17:37:48 +0000 2013",
    "user" : {
      "name" : "Dmitriy A.",
      "screen_name" : "jimaek",
      "protected" : false,
      "id_str" : "141747302",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000165277746/9b7bc8bc0a28b15cd571fa354b0e8138_normal.jpeg",
      "id" : 141747302,
      "verified" : false
    }
  },
  "id" : 362992743896723456,
  "created_at" : "Thu Aug 01 17:46:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CloudFlare",
      "screen_name" : "CloudFlare",
      "indices" : [ 42, 53 ],
      "id_str" : "32499999",
      "id" : 32499999
    } ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/362984583257534464/photo/1",
      "indices" : [ 105, 127 ],
      "url" : "http://t.co/CWYrvXeDTY",
      "media_url" : "http://pbs.twimg.com/media/BQmUjihCMAMidyH.jpg",
      "id_str" : "362984583261728771",
      "id" : 362984583261728771,
      "media_url_https" : "https://pbs.twimg.com/media/BQmUjihCMAMidyH.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 322,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 183,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 516,
        "resize" : "fit",
        "w" : 961
      }, {
        "h" : 516,
        "resize" : "fit",
        "w" : 961
      } ],
      "display_url" : "pic.twitter.com/CWYrvXeDTY"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "362984583257534464",
  "text" : "TIL - There is a whole array of different @cloudflare errors that mean very different things apparently. http://t.co/CWYrvXeDTY",
  "id" : 362984583257534464,
  "created_at" : "Thu Aug 01 17:14:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Webdesigntuts+",
      "screen_name" : "wdtuts",
      "indices" : [ 63, 70 ],
      "id_str" : "17135931",
      "id" : 17135931
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdesign",
      "indices" : [ 71, 81 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 58 ],
      "url" : "http://t.co/ZqIg4Rhse2",
      "expanded_url" : "http://myshar.es/14x8PUr",
      "display_url" : "myshar.es/14x8PUr"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362983072678948864",
  "text" : "Learning from Historic Web Archives http://t.co/ZqIg4Rhse2 via @wdtuts #webdesign",
  "id" : 362983072678948864,
  "created_at" : "Thu Aug 01 17:08:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 62 ],
      "url" : "http://t.co/lNPM8EqcZW",
      "expanded_url" : "http://myshar.es/11w8nAD",
      "display_url" : "myshar.es/11w8nAD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362981735937478656",
  "text" : "Simple javascript toast notifications - http://t.co/lNPM8EqcZW",
  "id" : 362981735937478656,
  "created_at" : "Thu Aug 01 17:02:44 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "indices" : [ 0, 6 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OHHAINSA",
      "indices" : [ 103, 112 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "362980541995298816",
  "geo" : {
  },
  "id_str" : "362981415027081219",
  "in_reply_to_user_id" : 23566038,
  "text" : "@mikko My favorite Dustin Hoffman movies are \uD835\uDC0E\uD835\uDC2E\uD835\uDC2D\uD835\uDC1B\uD835\uDC2B\uD835\uDC1E\uD835\uDC1A\uD835\uDC24, \uD835\uDC03\uD835\uDC1E\uD835\uDC1A\uD835\uDC2D\uD835\uDC21 of a Salesman &amp; \uD835\uDC13\uD835\uDC1E\uD835\uDC2B\uD835\uDC2B\uD835\uDC28\uD835\uDC2B in the Aisles. #OHHAINSA",
  "id" : 362981415027081219,
  "in_reply_to_status_id" : 362980541995298816,
  "created_at" : "Thu Aug 01 17:01:27 +0000 2013",
  "in_reply_to_screen_name" : "mikko",
  "in_reply_to_user_id_str" : "23566038",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 0, 12 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 35 ],
      "url" : "http://t.co/j0HpiZQe8Q",
      "expanded_url" : "http://miniflux.net/",
      "display_url" : "miniflux.net"
    } ]
  },
  "in_reply_to_status_id_str" : "362975577927462912",
  "geo" : {
  },
  "id_str" : "362976765259427840",
  "in_reply_to_user_id" : 15736190,
  "text" : "@smashingmag http://t.co/j0HpiZQe8Q",
  "id" : 362976765259427840,
  "in_reply_to_status_id" : 362975577927462912,
  "created_at" : "Thu Aug 01 16:42:59 +0000 2013",
  "in_reply_to_screen_name" : "smashingmag",
  "in_reply_to_user_id_str" : "15736190",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Shure",
      "screen_name" : "dan_shure",
      "indices" : [ 3, 13 ],
      "id_str" : "204753119",
      "id" : 204753119
    }, {
      "name" : "Google Analytics",
      "screen_name" : "googleanalytics",
      "indices" : [ 19, 35 ],
      "id_str" : "51263711",
      "id" : 51263711
    }, {
      "name" : "DuckDuckGo",
      "screen_name" : "duckduckgo",
      "indices" : [ 57, 68 ],
      "id_str" : "14504859",
      "id" : 14504859
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DuckDuckNo",
      "indices" : [ 112, 123 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "362928848939122689",
  "text" : "RT @dan_shure: Hey @googleanalytics Just curious, why is @DuckDuckGo a referral and not organic search traffic? #DuckDuckNo http://t.co/cBp\u2026",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Google Analytics",
        "screen_name" : "googleanalytics",
        "indices" : [ 4, 20 ],
        "id_str" : "51263711",
        "id" : 51263711
      }, {
        "name" : "DuckDuckGo",
        "screen_name" : "duckduckgo",
        "indices" : [ 42, 53 ],
        "id_str" : "14504859",
        "id" : 14504859
      } ],
      "media" : [ {
        "expanded_url" : "http://twitter.com/dan_shure/status/256871480770580480/photo/1",
        "indices" : [ 109, 129 ],
        "url" : "http://t.co/cBpcH0TK",
        "media_url" : "http://pbs.twimg.com/media/A5CXQDSCUAAavob.png",
        "id_str" : "256871480774774784",
        "id" : 256871480774774784,
        "media_url_https" : "https://pbs.twimg.com/media/A5CXQDSCUAAavob.png",
        "sizes" : [ {
          "h" : 188,
          "resize" : "fit",
          "w" : 340
        }, {
          "h" : 237,
          "resize" : "fit",
          "w" : 428
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 237,
          "resize" : "fit",
          "w" : 428
        }, {
          "h" : 237,
          "resize" : "fit",
          "w" : 428
        } ],
        "display_url" : "pic.twitter.com/cBpcH0TK"
      } ],
      "hashtags" : [ {
        "text" : "DuckDuckNo",
        "indices" : [ 97, 108 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "256871480770580480",
    "text" : "Hey @googleanalytics Just curious, why is @DuckDuckGo a referral and not organic search traffic? #DuckDuckNo http://t.co/cBpcH0TK",
    "id" : 256871480770580480,
    "created_at" : "Fri Oct 12 21:38:28 +0000 2012",
    "user" : {
      "name" : "Dan Shure",
      "screen_name" : "dan_shure",
      "protected" : false,
      "id_str" : "204753119",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1594613544/new-dan-shure_normal.jpg",
      "id" : 204753119,
      "verified" : false
    }
  },
  "id" : 362928848939122689,
  "created_at" : "Thu Aug 01 13:32:35 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 55 ],
      "url" : "http://t.co/cIuAXoXSkV",
      "expanded_url" : "http://www.downloadcrew.com/article/30114-winrar_500_64-bit",
      "display_url" : "downloadcrew.com/article/30114-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362823665651433472",
  "text" : "TIL, Winrar 5 has just launched. http://t.co/cIuAXoXSkV Another nuance of my computing life I must share...",
  "id" : 362823665651433472,
  "created_at" : "Thu Aug 01 06:34:37 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marco Arment",
      "screen_name" : "marcoarment",
      "indices" : [ 3, 15 ],
      "id_str" : "14231571",
      "id" : 14231571
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "362822384132579328",
  "text" : "RT @marcoarment: Writers: You don\u2019t need Medium. You can host your own blog on your own domain with lots of other tools and hosts.\n\nBe your\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/software/tweetbot/mac\" rel=\"nofollow\">Tweetbot for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "362613401861636098",
    "text" : "Writers: You don\u2019t need Medium. You can host your own blog on your own domain with lots of other tools and hosts.\n\nBe your own \u201Cplatform\u201D.",
    "id" : 362613401861636098,
    "created_at" : "Wed Jul 31 16:39:06 +0000 2013",
    "user" : {
      "name" : "Marco Arment",
      "screen_name" : "marcoarment",
      "protected" : false,
      "id_str" : "14231571",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1282173124/untitled-158-2_normal.jpg",
      "id" : 14231571,
      "verified" : false
    }
  },
  "id" : 362822384132579328,
  "created_at" : "Thu Aug 01 06:29:31 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
} ]