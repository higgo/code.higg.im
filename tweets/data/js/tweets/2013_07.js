Grailbird.data.tweets_2013_07 = 
 [ {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 3, 11 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Mozilla",
      "screen_name" : "mozilla",
      "indices" : [ 34, 42 ],
      "id_str" : "106682853",
      "id" : 106682853
    }, {
      "name" : "Martin N.",
      "screen_name" : "AVGP",
      "indices" : [ 96, 101 ],
      "id_str" : "15381265",
      "id" : 15381265
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 91 ],
      "url" : "http://t.co/Ez9Ba8AG8g",
      "expanded_url" : "http://myshar.es/13Ahrpg",
      "display_url" : "myshar.es/13Ahrpg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362656870642622464",
  "text" : "RT @codepo8 Based on mozPay &amp; @Mozilla is involved there as well http://t.co/Ez9Ba8AG8g via @AVGP",
  "id" : 362656870642622464,
  "created_at" : "Wed Jul 31 19:31:50 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "362654133087518720",
  "geo" : {
  },
  "id_str" : "362655212982370304",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Yes, the bias toward the U.S. annoys me something terrible. But regarding CC info. All they need is CC number, expiry date, and CVV",
  "id" : 362655212982370304,
  "in_reply_to_status_id" : 362654133087518720,
  "created_at" : "Wed Jul 31 19:25:15 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "362653112563007489",
  "geo" : {
  },
  "id_str" : "362653327462375425",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 I understand. IMHO We need a web payments standard. Similar to Moz. Persona, but for payments.",
  "id" : 362653327462375425,
  "in_reply_to_status_id" : 362653112563007489,
  "created_at" : "Wed Jul 31 19:17:45 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "362652590045007873",
  "geo" : {
  },
  "id_str" : "362652792948670467",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 54321 Works everytime!",
  "id" : 362652792948670467,
  "in_reply_to_status_id" : 362652590045007873,
  "created_at" : "Wed Jul 31 19:15:38 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James South",
      "screen_name" : "James_M_South",
      "indices" : [ 3, 17 ],
      "id_str" : "324829234",
      "id" : 324829234
    }, {
      "name" : "David H.",
      "screen_name" : "alphenic",
      "indices" : [ 19, 28 ],
      "id_str" : "468853739",
      "id" : 468853739
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "362652531505102849",
  "text" : "RT @James_M_South: @alphenic I see your three frameworks and I raise you http://t.co/l0un0mPs5x",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "David H.",
        "screen_name" : "alphenic",
        "indices" : [ 0, 9 ],
        "id_str" : "468853739",
        "id" : 468853739
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 76 ],
        "url" : "http://t.co/l0un0mPs5x",
        "expanded_url" : "http://jimbobsquarepants.github.io/Responsive/",
        "display_url" : "jimbobsquarepants.github.io/Responsive/"
      } ]
    },
    "in_reply_to_status_id_str" : "362651052207652864",
    "geo" : {
    },
    "id_str" : "362652378064896000",
    "in_reply_to_user_id" : 468853739,
    "text" : "@alphenic I see your three frameworks and I raise you http://t.co/l0un0mPs5x",
    "id" : 362652378064896000,
    "in_reply_to_status_id" : 362651052207652864,
    "created_at" : "Wed Jul 31 19:13:59 +0000 2013",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "James South",
      "screen_name" : "James_M_South",
      "protected" : false,
      "id_str" : "324829234",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3626304680/fade2396038bee1378def45dd99afd7a_normal.jpeg",
      "id" : 324829234,
      "verified" : false
    }
  },
  "id" : 362652531505102849,
  "created_at" : "Wed Jul 31 19:14:35 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 53 ],
      "url" : "http://t.co/fqyvLbha6T",
      "expanded_url" : "http://ink.sapo.pt/",
      "display_url" : "ink.sapo.pt"
    }, {
      "indices" : [ 62, 84 ],
      "url" : "http://t.co/z0mVwlAWXU",
      "expanded_url" : "http://icalialabs.github.io/furatto/components.html",
      "display_url" : "icalialabs.github.io/furatto/compon\u2026"
    }, {
      "indices" : [ 93, 115 ],
      "url" : "http://t.co/QHenodq4I6",
      "expanded_url" : "http://purecss.io/",
      "display_url" : "purecss.io"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362651052207652864",
  "text" : "Three awesome CSS frameworks:\n\nhttp://t.co/fqyvLbha6T\n\n&amp;\n\nhttp://t.co/z0mVwlAWXU\n\n&amp;\n\nhttp://t.co/QHenodq4I6",
  "id" : 362651052207652864,
  "created_at" : "Wed Jul 31 19:08:43 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "362639380696940544",
  "geo" : {
  },
  "id_str" : "362642029316022272",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Very hard to snag a name with \"sky\" in it. Also hard for \"cloud\". Cloudhub, Cloud-drive, Cloudspot, Cloud Haven etc are all taken",
  "id" : 362642029316022272,
  "in_reply_to_status_id" : 362639380696940544,
  "created_at" : "Wed Jul 31 18:32:52 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Web Platform",
      "screen_name" : "WebPlatform",
      "indices" : [ 3, 15 ],
      "id_str" : "523510988",
      "id" : 523510988
    }, {
      "name" : "Web Platform",
      "screen_name" : "WebPlatform",
      "indices" : [ 108, 120 ],
      "id_str" : "523510988",
      "id" : 523510988
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CSS",
      "indices" : [ 17, 21 ]
    }, {
      "text" : "WPW",
      "indices" : [ 123, 127 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "362637240674942978",
  "text" : "RT @WebPlatform: #CSS text properties need more facts, samples, links. And you need that firestarter badge. @WebPlatform's #WPW http://t.co\u2026",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Web Platform",
        "screen_name" : "WebPlatform",
        "indices" : [ 91, 103 ],
        "id_str" : "523510988",
        "id" : 523510988
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "CSS",
        "indices" : [ 0, 4 ]
      }, {
        "text" : "WPW",
        "indices" : [ 106, 110 ]
      } ],
      "urls" : [ {
        "indices" : [ 111, 133 ],
        "url" : "http://t.co/it3ODzKAD4",
        "expanded_url" : "http://blog.webplatform.org/2013/07/web-platform-wednesday-week-12-emphasizing-text-properties",
        "display_url" : "blog.webplatform.org/2013/07/web-pl\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "362633501348077570",
    "text" : "#CSS text properties need more facts, samples, links. And you need that firestarter badge. @WebPlatform's #WPW http://t.co/it3ODzKAD4",
    "id" : 362633501348077570,
    "created_at" : "Wed Jul 31 17:58:58 +0000 2013",
    "user" : {
      "name" : "Web Platform",
      "screen_name" : "WebPlatform",
      "protected" : false,
      "id_str" : "523510988",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2691396636/9a00b3fec026f64ba02a55adbfbe5ec0_normal.png",
      "id" : 523510988,
      "verified" : false
    }
  },
  "id" : 362637240674942978,
  "created_at" : "Wed Jul 31 18:13:50 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "361243151639396353",
  "geo" : {
  },
  "id_str" : "362627556962344961",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys Wish you all the best in your venture. Crush it",
  "id" : 362627556962344961,
  "in_reply_to_status_id" : 361243151639396353,
  "created_at" : "Wed Jul 31 17:35:21 +0000 2013",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 3, 6 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 110 ],
      "url" : "http://t.co/ac7ENIfLjT",
      "expanded_url" : "http://hypercubeltd.com",
      "display_url" : "hypercubeltd.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362627015049875457",
  "text" : "RT @ys: If anyone is wondering what I'm in San Francisco to raise money for, it's this. http://t.co/ac7ENIfLjT",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 102 ],
        "url" : "http://t.co/ac7ENIfLjT",
        "expanded_url" : "http://hypercubeltd.com",
        "display_url" : "hypercubeltd.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "361243151639396353",
    "text" : "If anyone is wondering what I'm in San Francisco to raise money for, it's this. http://t.co/ac7ENIfLjT",
    "id" : 361243151639396353,
    "created_at" : "Sat Jul 27 21:54:13 +0000 2013",
    "user" : {
      "name" : "Youssef",
      "screen_name" : "ys",
      "protected" : false,
      "id_str" : "19010677",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000122676370/327da5878e48b810986df4829ea25ea6_normal.jpeg",
      "id" : 19010677,
      "verified" : false
    }
  },
  "id" : 362627015049875457,
  "created_at" : "Wed Jul 31 17:33:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 49 ],
      "url" : "http://t.co/Dj6ttszEWp",
      "expanded_url" : "http://mfs.sub.jp/font.html",
      "display_url" : "mfs.sub.jp/font.html"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362620687569002497",
  "text" : "A classic design resource: http://t.co/Dj6ttszEWp Miffies",
  "id" : 362620687569002497,
  "created_at" : "Wed Jul 31 17:08:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "indices" : [ 3, 12 ],
      "id_str" : "606610405",
      "id" : 606610405
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "362459541608218624",
  "text" : "RT @favehigg: Anytime I hear soft, gentle music, I feel like a mean ass Dubstep Bassline is going to kick in and scare the shit out of me.",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "346733513749442560",
    "text" : "Anytime I hear soft, gentle music, I feel like a mean ass Dubstep Bassline is going to kick in and scare the shit out of me.",
    "id" : 346733513749442560,
    "created_at" : "Mon Jun 17 20:58:06 +0000 2013",
    "user" : {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "protected" : false,
      "id_str" : "606610405",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000007208981/fee678a7c91e39839aa9ca27489b1b1f_normal.png",
      "id" : 606610405,
      "verified" : false
    }
  },
  "id" : 362459541608218624,
  "created_at" : "Wed Jul 31 06:27:43 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 140 ],
      "url" : "http://t.co/6LYcZwz2sP",
      "expanded_url" : "http://isharefil.es/QXml/404.gif",
      "display_url" : "isharefil.es/QXml/404.gif"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362389948881580032",
  "text" : "If you have never seen this girl before you are either one of those 'web natives' or haven't browsed the web properly http://t.co/6LYcZwz2sP",
  "id" : 362389948881580032,
  "created_at" : "Wed Jul 31 01:51:11 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 28 ],
      "url" : "http://t.co/izpohdChyW",
      "expanded_url" : "http://wrttn.me/30dbfd/",
      "display_url" : "wrttn.me/30dbfd/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362331909491130370",
  "text" : "\u25D5 \u25E1 \u25D5 http://t.co/izpohdChyW",
  "id" : 362331909491130370,
  "created_at" : "Tue Jul 30 22:00:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 116 ],
      "url" : "http://t.co/qnZMcNevqP",
      "expanded_url" : "http://www.goldenfrog.com/dumptruck",
      "display_url" : "goldenfrog.com/dumptruck"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362315444289413121",
  "text" : "I already use over 20+ different cloud services to store data. But weirdly I still want this: http://t.co/qnZMcNevqP",
  "id" : 362315444289413121,
  "created_at" : "Tue Jul 30 20:55:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigitalOcean",
      "screen_name" : "digitalocean",
      "indices" : [ 3, 16 ],
      "id_str" : "457033547",
      "id" : 457033547
    }, {
      "name" : "DigitalOcean",
      "screen_name" : "digitalocean",
      "indices" : [ 74, 87 ],
      "id_str" : "457033547",
      "id" : 457033547
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https://t.co/AD0C4P3p1k",
      "expanded_url" : "https://www.digitalocean.com/blog_posts/get-paid-to-write-tutorials",
      "display_url" : "digitalocean.com/blog_posts/get\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362314826913038336",
  "text" : "RT @digitalocean: Get Paid to Write Tutorials https://t.co/AD0C4P3p1k via @DigitalOcean",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DigitalOcean",
        "screen_name" : "digitalocean",
        "indices" : [ 56, 69 ],
        "id_str" : "457033547",
        "id" : 457033547
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 28, 51 ],
        "url" : "https://t.co/AD0C4P3p1k",
        "expanded_url" : "https://www.digitalocean.com/blog_posts/get-paid-to-write-tutorials",
        "display_url" : "digitalocean.com/blog_posts/get\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "362302859833917441",
    "text" : "Get Paid to Write Tutorials https://t.co/AD0C4P3p1k via @DigitalOcean",
    "id" : 362302859833917441,
    "created_at" : "Tue Jul 30 20:05:07 +0000 2013",
    "user" : {
      "name" : "DigitalOcean",
      "screen_name" : "digitalocean",
      "protected" : false,
      "id_str" : "457033547",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2623921949/332ck3jglwnubobal3c2_normal.png",
      "id" : 457033547,
      "verified" : false
    }
  },
  "id" : 362314826913038336,
  "created_at" : "Tue Jul 30 20:52:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Butterick",
      "screen_name" : "mbutterick",
      "indices" : [ 48, 59 ],
      "id_str" : "79015105",
      "id" : 79015105
    }, {
      "name" : "Michael[tm] Smith",
      "screen_name" : "sideshowbarker",
      "indices" : [ 98, 113 ],
      "id_str" : "6251532",
      "id" : 6251532
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MoveTheWebForward",
      "indices" : [ 114, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "362259930138423297",
  "text" : "I knew the W3C remarks would cause a storm, but @mbutterick made other interesting points too cc/ @sideshowbarker #MoveTheWebForward",
  "id" : 362259930138423297,
  "created_at" : "Tue Jul 30 17:14:32 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https://t.co/H4qfCuHpBw",
      "expanded_url" : "https://assets.mozillalabs.com/Graphics/Wallpapers/",
      "display_url" : "assets.mozillalabs.com/Graphics/Wallp\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "362254737078366208",
  "geo" : {
  },
  "id_str" : "362257726677598208",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 More here ;) https://t.co/H4qfCuHpBw",
  "id" : 362257726677598208,
  "in_reply_to_status_id" : 362254737078366208,
  "created_at" : "Tue Jul 30 17:05:47 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 3, 11 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https://t.co/X9b5NpuU8Z",
      "expanded_url" : "https://assets.mozillalabs.com/Graphics/Wallpapers/BrowseOn/firefox_browse-on_1920x1200.png",
      "display_url" : "assets.mozillalabs.com/Graphics/Wallp\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362255867497820160",
  "text" : "RT @codepo8: Keep calm and browse on: https://t.co/X9b5NpuU8Z",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 25, 48 ],
        "url" : "https://t.co/X9b5NpuU8Z",
        "expanded_url" : "https://assets.mozillalabs.com/Graphics/Wallpapers/BrowseOn/firefox_browse-on_1920x1200.png",
        "display_url" : "assets.mozillalabs.com/Graphics/Wallp\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "362254737078366208",
    "text" : "Keep calm and browse on: https://t.co/X9b5NpuU8Z",
    "id" : 362254737078366208,
    "created_at" : "Tue Jul 30 16:53:54 +0000 2013",
    "user" : {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "protected" : false,
      "id_str" : "13567",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1666904408/codepo8_normal.png",
      "id" : 13567,
      "verified" : false
    }
  },
  "id" : 362255867497820160,
  "created_at" : "Tue Jul 30 16:58:23 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Vourtsis",
      "screen_name" : "alexandervrs",
      "indices" : [ 0, 13 ],
      "id_str" : "186630199",
      "id" : 186630199
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "362243322892398592",
  "geo" : {
  },
  "id_str" : "362244550821036032",
  "in_reply_to_user_id" : 186630199,
  "text" : "@alexandervrs It won't be disbanded ;) I think he was making a point about 'red tape' and bureaucracy not being compatible with an open web",
  "id" : 362244550821036032,
  "in_reply_to_status_id" : 362243322892398592,
  "created_at" : "Tue Jul 30 16:13:25 +0000 2013",
  "in_reply_to_screen_name" : "alexandervrs",
  "in_reply_to_user_id_str" : "186630199",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 98 ],
      "url" : "http://t.co/AlX9kFu988",
      "expanded_url" : "http://unitscale.com/mb/bomb-in-the-garden/",
      "display_url" : "unitscale.com/mb/bomb-in-the\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362240133153558528",
  "text" : "For all the battle hardened web professionals, you should give this a read: http://t.co/AlX9kFu988 Revolutionary post about the web platform",
  "id" : 362240133153558528,
  "created_at" : "Tue Jul 30 15:55:52 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Zlatan Vasovi\u0107",
      "screen_name" : "ZXeDroid",
      "indices" : [ 0, 9 ],
      "id_str" : "584128179",
      "id" : 584128179
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "362212760664281088",
  "geo" : {
  },
  "id_str" : "362213125270937602",
  "in_reply_to_user_id" : 584128179,
  "text" : "@ZXeDroid Thanks. Fixed also.",
  "id" : 362213125270937602,
  "in_reply_to_status_id" : 362212760664281088,
  "created_at" : "Tue Jul 30 14:08:33 +0000 2013",
  "in_reply_to_screen_name" : "ZXeDroid",
  "in_reply_to_user_id_str" : "584128179",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Zlatan Vasovi\u0107",
      "screen_name" : "ZXeDroid",
      "indices" : [ 0, 9 ],
      "id_str" : "584128179",
      "id" : 584128179
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 39 ],
      "url" : "http://t.co/p1hgHoMjeN",
      "expanded_url" : "http://higg.im/",
      "display_url" : "higg.im"
    } ]
  },
  "in_reply_to_status_id_str" : "362211182330908673",
  "geo" : {
  },
  "id_str" : "362212050375671809",
  "in_reply_to_user_id" : 584128179,
  "text" : "@ZXeDroid Fixed: http://t.co/p1hgHoMjeN",
  "id" : 362212050375671809,
  "in_reply_to_status_id" : 362211182330908673,
  "created_at" : "Tue Jul 30 14:04:17 +0000 2013",
  "in_reply_to_screen_name" : "ZXeDroid",
  "in_reply_to_user_id_str" : "584128179",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "chromeless",
      "indices" : [ 23, 34 ]
    } ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/vWpkhzU573",
      "expanded_url" : "http://iwantaneff.in/chromeless/",
      "display_url" : "iwantaneff.in/chromeless/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "362210056902672384",
  "text" : "http://t.co/vWpkhzU573 #chromeless",
  "id" : 362210056902672384,
  "created_at" : "Tue Jul 30 13:56:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 1, 11 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "361990576457007104",
  "geo" : {
  },
  "id_str" : "362034845750263809",
  "in_reply_to_user_id" : 9866582,
  "text" : ".@film_girl Information wants to be expensive (and free too, but we have ADs for that scenario). Old Reader wants to be acquired, not killed",
  "id" : 362034845750263809,
  "in_reply_to_status_id" : 361990576457007104,
  "created_at" : "Tue Jul 30 02:20:08 +0000 2013",
  "in_reply_to_screen_name" : "film_girl",
  "in_reply_to_user_id_str" : "9866582",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "indices" : [ 3, 16 ],
      "id_str" : "154206772",
      "id" : 154206772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "361975297182146561",
  "text" : "RT @a_antonellis: i should become a data loss grief counsellor, i have lots of experience",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "361939592947048448",
    "text" : "i should become a data loss grief counsellor, i have lots of experience",
    "id" : 361939592947048448,
    "created_at" : "Mon Jul 29 20:01:38 +0000 2013",
    "user" : {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "protected" : false,
      "id_str" : "154206772",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1904673216/cmy_normal.gif",
      "id" : 154206772,
      "verified" : false
    }
  },
  "id" : 361975297182146561,
  "created_at" : "Mon Jul 29 22:23:30 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Czarny",
      "screen_name" : "relikpL",
      "indices" : [ 3, 11 ],
      "id_str" : "214342143",
      "id" : 214342143
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 70 ],
      "url" : "http://t.co/eGIV3DLsqS",
      "expanded_url" : "http://tox.im/",
      "display_url" : "tox.im"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361973680353452032",
  "text" : "RT @relikpL: Tox: secure messaging for everyone http://t.co/eGIV3DLsqS",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 35, 57 ],
        "url" : "http://t.co/eGIV3DLsqS",
        "expanded_url" : "http://tox.im/",
        "display_url" : "tox.im"
      } ]
    },
    "geo" : {
    },
    "id_str" : "361969191668027394",
    "text" : "Tox: secure messaging for everyone http://t.co/eGIV3DLsqS",
    "id" : 361969191668027394,
    "created_at" : "Mon Jul 29 21:59:15 +0000 2013",
    "user" : {
      "name" : "Mike Czarny",
      "screen_name" : "relikpL",
      "protected" : false,
      "id_str" : "214342143",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3551043745/e9091265a84e1fb6c910cd24f52edd06_normal.jpeg",
      "id" : 214342143,
      "verified" : false
    }
  },
  "id" : 361973680353452032,
  "created_at" : "Mon Jul 29 22:17:05 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andrew Clay Shafer",
      "screen_name" : "littleidea",
      "indices" : [ 0, 11 ],
      "id_str" : "14079705",
      "id" : 14079705
    }, {
      "name" : "Adewale Oshineye",
      "screen_name" : "ade_oshineye",
      "indices" : [ 12, 25 ],
      "id_str" : "23254994",
      "id" : 23254994
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "361910790405632002",
  "geo" : {
  },
  "id_str" : "361923032228442113",
  "in_reply_to_user_id" : 14079705,
  "text" : "@littleidea @ade_oshineye The As a Service Meme ... As a service",
  "id" : 361923032228442113,
  "in_reply_to_status_id" : 361910790405632002,
  "created_at" : "Mon Jul 29 18:55:49 +0000 2013",
  "in_reply_to_screen_name" : "littleidea",
  "in_reply_to_user_id_str" : "14079705",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter Nederlof",
      "screen_name" : "peterlof",
      "indices" : [ 62, 71 ],
      "id_str" : "90409884",
      "id" : 90409884
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 53 ],
      "url" : "http://t.co/hGbxbta1Zw",
      "expanded_url" : "http://peterned.home.xs4all.nl/2003/",
      "display_url" : "peterned.home.xs4all.nl/2003/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361891951055876096",
  "text" : "Oldskool DHTML demo from 2003: http://t.co/hGbxbta1Zw made by @peterlof Awesome! For those who don't know DHTML == jQuery before it was cool",
  "id" : 361891951055876096,
  "created_at" : "Mon Jul 29 16:52:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Fun",
      "indices" : [ 38, 42 ]
    } ],
    "urls" : [ {
      "indices" : [ 15, 37 ],
      "url" : "http://t.co/iVCso5I8F8",
      "expanded_url" : "http://imgfave.com/collection/245499/Noseless%20Gifs",
      "display_url" : "imgfave.com/collection/245\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361891450511826946",
  "text" : "Noseless GIFs. http://t.co/iVCso5I8F8 #Fun",
  "id" : 361891450511826946,
  "created_at" : "Mon Jul 29 16:50:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 60 ],
      "url" : "http://t.co/OA7GgPpsPB",
      "expanded_url" : "http://bgr.com/2012/09/21/lexar-256gb-sd-card-release-date-october-900/",
      "display_url" : "bgr.com/2012/09/21/lex\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361891237659291648",
  "text" : "TIL - 256GB SD cards do exist. Oh my. http://t.co/OA7GgPpsPB",
  "id" : 361891237659291648,
  "created_at" : "Mon Jul 29 16:49:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Larry Hynes",
      "screen_name" : "larry_hynes",
      "indices" : [ 0, 12 ],
      "id_str" : "578272794",
      "id" : 578272794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "361889054758940676",
  "geo" : {
  },
  "id_str" : "361890711345430529",
  "in_reply_to_user_id" : 578272794,
  "text" : "@larry_hynes TIOT is already here. We live in the future already. Stop waiting for the future to happen. (Flying cars would be nice though)",
  "id" : 361890711345430529,
  "in_reply_to_status_id" : 361889054758940676,
  "created_at" : "Mon Jul 29 16:47:23 +0000 2013",
  "in_reply_to_screen_name" : "larry_hynes",
  "in_reply_to_user_id_str" : "578272794",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BurningQuestion",
      "indices" : [ 50, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 27, 49 ],
      "url" : "http://t.co/zL4dhAfKSF",
      "expanded_url" : "http://www.bootply.com/migrate-to-bootstrap-3",
      "display_url" : "bootply.com/migrate-to-boo\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361888466558140417",
  "text" : "What's new in Bootstrap 3? http://t.co/zL4dhAfKSF #BurningQuestion",
  "id" : 361888466558140417,
  "created_at" : "Mon Jul 29 16:38:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Kammerer",
      "screen_name" : "mkammerer",
      "indices" : [ 0, 10 ],
      "id_str" : "13226232",
      "id" : 13226232
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 34 ],
      "url" : "https://t.co/S95yvo0uLy",
      "expanded_url" : "https://docs.google.com/document/d/1r4L7ooHDpLHgexsFFJ4NO6lLp5HhhJPJU7ylpsRZbX4/edit?usp=sharing",
      "display_url" : "docs.google.com/document/d/1r4\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "361878675433586689",
  "geo" : {
  },
  "id_str" : "361881461671985153",
  "in_reply_to_user_id" : 13226232,
  "text" : "@mkammerer https://t.co/S95yvo0uLy",
  "id" : 361881461671985153,
  "in_reply_to_status_id" : 361878675433586689,
  "created_at" : "Mon Jul 29 16:10:38 +0000 2013",
  "in_reply_to_screen_name" : "mkammerer",
  "in_reply_to_user_id_str" : "13226232",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "353886276774137858",
  "geo" : {
  },
  "id_str" : "361650503697969153",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas Would be nice to have the official hashtag for each one too. That way I can filter them all out from my timeline.",
  "id" : 361650503697969153,
  "in_reply_to_status_id" : 353886276774137858,
  "created_at" : "Mon Jul 29 00:52:53 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Twitter",
      "screen_name" : "twitter",
      "indices" : [ 9, 17 ],
      "id_str" : "783214",
      "id" : 783214
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "361583377742172160",
  "geo" : {
  },
  "id_str" : "361584786411753473",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 @twitter Are the photos ridiculously large in filesize? High megapixel cameras and flaky, slow mobile connections, don't mix well.",
  "id" : 361584786411753473,
  "in_reply_to_status_id" : 361583377742172160,
  "created_at" : "Sun Jul 28 20:31:45 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OMG",
      "indices" : [ 133, 137 ]
    } ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http://t.co/6XTZoPyWiy",
      "expanded_url" : "http://isharefil.es/QVUH",
      "display_url" : "isharefil.es/QVUH"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361529558043852801",
  "text" : "TIL - Google Analytics are even in my Twitter. Would be quite interesting seeing the stats panel for Twitter. http://t.co/6XTZoPyWiy #OMG",
  "id" : 361529558043852801,
  "created_at" : "Sun Jul 28 16:52:18 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Codepoints.net",
      "screen_name" : "CodepointsNet",
      "indices" : [ 3, 17 ],
      "id_str" : "297945400",
      "id" : 297945400
    }, {
      "name" : "Paul Ford",
      "screen_name" : "ftrain",
      "indices" : [ 64, 71 ],
      "id_str" : "6981492",
      "id" : 6981492
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 60 ],
      "url" : "http://t.co/EGRklhALsn",
      "expanded_url" : "http://j.mp/17N0VXe",
      "display_url" : "j.mp/17N0VXe"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361527541082755074",
  "text" : "RT @CodepointsNet: Codepoint details: http://t.co/EGRklhALsn RT @ftrain: If you paste\nexport PS1=\"\uD83D\uDC7B &gt;\"\ninto your terminal you will have a g\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Paul Ford",
        "screen_name" : "ftrain",
        "indices" : [ 45, 52 ],
        "id_str" : "6981492",
        "id" : 6981492
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 19, 41 ],
        "url" : "http://t.co/EGRklhALsn",
        "expanded_url" : "http://j.mp/17N0VXe",
        "display_url" : "j.mp/17N0VXe"
      } ]
    },
    "geo" : {
    },
    "id_str" : "361514329918554112",
    "text" : "Codepoint details: http://t.co/EGRklhALsn RT @ftrain: If you paste\nexport PS1=\"\uD83D\uDC7B &gt;\"\ninto your terminal you will have a ghost in the shell",
    "id" : 361514329918554112,
    "created_at" : "Sun Jul 28 15:51:47 +0000 2013",
    "user" : {
      "name" : "Codepoints.net",
      "screen_name" : "CodepointsNet",
      "protected" : false,
      "id_str" : "297945400",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1351818301/ucotd_normal.png",
      "id" : 297945400,
      "verified" : false
    }
  },
  "id" : 361527541082755074,
  "created_at" : "Sun Jul 28 16:44:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/n9e9q4nf8f",
      "expanded_url" : "http://f.cl.ly/items/0Q1b290t0j250q383F30/-ot-crew.html",
      "display_url" : "f.cl.ly/items/0Q1b290t\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361474435347255296",
  "text" : "http://t.co/n9e9q4nf8f",
  "id" : 361474435347255296,
  "created_at" : "Sun Jul 28 13:13:15 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 0, 9 ],
      "id_str" : "14139773",
      "id" : 14139773
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 35 ],
      "url" : "http://t.co/GPdqI3x90T",
      "expanded_url" : "http://gli.tc/h/",
      "display_url" : "gli.tc/h/"
    } ]
  },
  "in_reply_to_status_id_str" : "361264372452032512",
  "geo" : {
  },
  "id_str" : "361265873949966336",
  "in_reply_to_user_id" : 14139773,
  "text" : "@jdorfman ;) http://t.co/GPdqI3x90T",
  "id" : 361265873949966336,
  "in_reply_to_status_id" : 361264372452032512,
  "created_at" : "Sat Jul 27 23:24:31 +0000 2013",
  "in_reply_to_screen_name" : "jdorfman",
  "in_reply_to_user_id_str" : "14139773",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 0, 9 ],
      "id_str" : "14139773",
      "id" : 14139773
    }, {
      "name" : "Brad Fitzpatrick",
      "screen_name" : "bradfitz",
      "indices" : [ 10, 19 ],
      "id_str" : "650013",
      "id" : 650013
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http://t.co/ZvIKgGzZhk",
      "expanded_url" : "http://www.youtube.com/watch?v=bjZRAvsZf1g",
      "display_url" : "youtube.com/watch?v=bjZRAv\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "361224948817199104",
  "geo" : {
  },
  "id_str" : "361227048150249473",
  "in_reply_to_user_id" : 14139773,
  "text" : "@jdorfman @bradfitz Dolla Dolla bill y'all http://t.co/ZvIKgGzZhk",
  "id" : 361227048150249473,
  "in_reply_to_status_id" : 361224948817199104,
  "created_at" : "Sat Jul 27 20:50:14 +0000 2013",
  "in_reply_to_screen_name" : "jdorfman",
  "in_reply_to_user_id_str" : "14139773",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Glenn Kelly",
      "screen_name" : "KelCeltic",
      "indices" : [ 0, 10 ],
      "id_str" : "581032441",
      "id" : 581032441
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SharkNado",
      "indices" : [ 11, 21 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "361213816777871361",
  "in_reply_to_user_id" : 581032441,
  "text" : "@KelCeltic #SharkNado Click on the hashtag. Brace yourself.",
  "id" : 361213816777871361,
  "created_at" : "Sat Jul 27 19:57:39 +0000 2013",
  "in_reply_to_screen_name" : "KelCeltic",
  "in_reply_to_user_id_str" : "581032441",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "indices" : [ 3, 14 ],
      "id_str" : "517746290",
      "id" : 517746290
    } ],
    "media" : [ {
      "expanded_url" : "https://twitter.com/molovo/status/360346792602259456/photo/1",
      "indices" : [ 65, 88 ],
      "url" : "https://t.co/kBUmXfwy9F",
      "media_url" : "http://pbs.twimg.com/media/BQA1f6lCUAAF25-.png",
      "id_str" : "360346792606453760",
      "id" : 360346792606453760,
      "media_url_https" : "https://pbs.twimg.com/media/BQA1f6lCUAAF25-.png",
      "sizes" : [ {
        "h" : 689,
        "resize" : "fit",
        "w" : 1103
      }, {
        "h" : 212,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 375,
        "resize" : "fit",
        "w" : 600
      } ],
      "display_url" : "pic.twitter.com/kBUmXfwy9F"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "361211569885032451",
  "text" : "RT @PinPopular: Twitter / molovo: Received this actual email ... https://t.co/kBUmXfwy9F",
  "retweeted_status" : {
    "source" : "<a href=\"http://zhangchi.de/\" rel=\"nofollow\">Pinboard Popular</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https://twitter.com/molovo/status/360346792602259456/photo/1",
        "indices" : [ 49, 72 ],
        "url" : "https://t.co/kBUmXfwy9F",
        "media_url" : "http://pbs.twimg.com/media/BQA1f6lCUAAF25-.png",
        "id_str" : "360346792606453760",
        "id" : 360346792606453760,
        "media_url_https" : "https://pbs.twimg.com/media/BQA1f6lCUAAF25-.png",
        "sizes" : [ {
          "h" : 689,
          "resize" : "fit",
          "w" : 1103
        }, {
          "h" : 212,
          "resize" : "fit",
          "w" : 340
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 375,
          "resize" : "fit",
          "w" : 600
        } ],
        "display_url" : "pic.twitter.com/kBUmXfwy9F"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "361193299081576450",
    "text" : "Twitter / molovo: Received this actual email ... https://t.co/kBUmXfwy9F",
    "id" : 361193299081576450,
    "created_at" : "Sat Jul 27 18:36:07 +0000 2013",
    "user" : {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "protected" : false,
      "id_str" : "517746290",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000170092903/61a9facf585dc87b3054e01059a11988_normal.png",
      "id" : 517746290,
      "verified" : false
    }
  },
  "id" : 361211569885032451,
  "created_at" : "Sat Jul 27 19:48:43 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SpeedAwarenessMonth",
      "screen_name" : "SpeedMonth",
      "indices" : [ 3, 14 ],
      "id_str" : "632445769",
      "id" : 632445769
    }, {
      "name" : "Bootstrap",
      "screen_name" : "twbootstrap",
      "indices" : [ 42, 54 ],
      "id_str" : "372475592",
      "id" : 372475592
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BootstrapCDN",
      "indices" : [ 16, 29 ]
    } ],
    "urls" : [ {
      "indices" : [ 65, 87 ],
      "url" : "http://t.co/xJ6doZtRO4",
      "expanded_url" : "http://blog.netdna.com/opensource/bootstrapcdn/bootstrapcdn-now-serving-3-0-0-rc1/",
      "display_url" : "blog.netdna.com/opensource/boo\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361191991830257664",
  "text" : "RT @SpeedMonth: #BootstrapCDN now serving @twbootstrap 3.0.0-RC1 http://t.co/xJ6doZtRO4",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Bootstrap",
        "screen_name" : "twbootstrap",
        "indices" : [ 26, 38 ],
        "id_str" : "372475592",
        "id" : 372475592
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BootstrapCDN",
        "indices" : [ 0, 13 ]
      } ],
      "urls" : [ {
        "indices" : [ 49, 71 ],
        "url" : "http://t.co/xJ6doZtRO4",
        "expanded_url" : "http://blog.netdna.com/opensource/bootstrapcdn/bootstrapcdn-now-serving-3-0-0-rc1/",
        "display_url" : "blog.netdna.com/opensource/boo\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "361187879042560000",
    "text" : "#BootstrapCDN now serving @twbootstrap 3.0.0-RC1 http://t.co/xJ6doZtRO4",
    "id" : 361187879042560000,
    "created_at" : "Sat Jul 27 18:14:35 +0000 2013",
    "user" : {
      "name" : "SpeedAwarenessMonth",
      "screen_name" : "SpeedMonth",
      "protected" : false,
      "id_str" : "632445769",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000225805619/7491745734d054b4552b573ab8a97fdc_normal.png",
      "id" : 632445769,
      "verified" : false
    }
  },
  "id" : 361191991830257664,
  "created_at" : "Sat Jul 27 18:30:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Jones",
      "screen_name" : "welsh_gas_doc",
      "indices" : [ 3, 17 ],
      "id_str" : "83011510",
      "id" : 83011510
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "361140810311409665",
  "text" : "RT @welsh_gas_doc: I love Twitter campaigns. You might as well write it with lipstick on the side of a bag of dogfood and throw it in the c\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/tweetbot\" rel=\"nofollow\">Tweetbot for iOS</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "361080550771007488",
    "text" : "I love Twitter campaigns. You might as well write it with lipstick on the side of a bag of dogfood and throw it in the canal.",
    "id" : 361080550771007488,
    "created_at" : "Sat Jul 27 11:08:06 +0000 2013",
    "user" : {
      "name" : "Dave Jones",
      "screen_name" : "welsh_gas_doc",
      "protected" : false,
      "id_str" : "83011510",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3586416772/a4260790f6aeb1402c8e658055333e67_normal.png",
      "id" : 83011510,
      "verified" : false
    }
  },
  "id" : 361140810311409665,
  "created_at" : "Sat Jul 27 15:07:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 32 ],
      "url" : "https://t.co/9M7zELciux",
      "expanded_url" : "https://getfedu.com/",
      "display_url" : "getfedu.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361128677204959232",
  "text" : "Awesome: https://t.co/9M7zELciux",
  "id" : 361128677204959232,
  "created_at" : "Sat Jul 27 14:19:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "FlatUI",
      "indices" : [ 78, 85 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 77 ],
      "url" : "http://t.co/2XHTYNEpGK",
      "expanded_url" : "http://getbootstrap.com/getting-started/",
      "display_url" : "getbootstrap.com/getting-starte\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "361128125255516160",
  "text" : "Really digging the new release candidate of Bootstrap: http://t.co/2XHTYNEpGK #FlatUI",
  "id" : 361128125255516160,
  "created_at" : "Sat Jul 27 14:17:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anil Dash",
      "screen_name" : "anildash",
      "indices" : [ 3, 12 ],
      "id_str" : "36823",
      "id" : 36823
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360927483962458112",
  "text" : "RT @anildash: Try explaining to young coders raised on EC2 that we used to buy Dell servers at end of month from salespeople trying to meet\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "360836705500143617",
    "text" : "Try explaining to young coders raised on EC2 that we used to buy Dell servers at end of month from salespeople trying to meet their numbers.",
    "id" : 360836705500143617,
    "created_at" : "Fri Jul 26 18:59:09 +0000 2013",
    "user" : {
      "name" : "Anil Dash",
      "screen_name" : "anildash",
      "protected" : false,
      "id_str" : "36823",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3588056222/0d62af265381d6b5861371e86d0bff23_normal.jpeg",
      "id" : 36823,
      "verified" : true
    }
  },
  "id" : 360927483962458112,
  "created_at" : "Sat Jul 27 00:59:52 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "_",
      "screen_name" : "JenessaLaSota",
      "indices" : [ 3, 17 ],
      "id_str" : "204170687",
      "id" : 204170687
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360915603311173632",
  "text" : "RT @JenessaLaSota: you are the comic sans of human beings",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "348969673846562816",
    "text" : "you are the comic sans of human beings",
    "id" : 348969673846562816,
    "created_at" : "Mon Jun 24 01:03:48 +0000 2013",
    "user" : {
      "name" : "\u3164\u3164\u3164\u3164\u3164\u3164\u3164\u3164\u3164\u3164\u3164\u3164\u3164\u3164\u3164\u3164 \u200F ",
      "screen_name" : "JenessaLaSota",
      "protected" : true,
      "id_str" : "204170687",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000263707439/b770d16018048c6864631a95ee450375_normal.jpeg",
      "id" : 204170687,
      "verified" : false
    }
  },
  "id" : 360915603311173632,
  "created_at" : "Sat Jul 27 00:12:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 138 ],
      "url" : "http://t.co/hxLYgqqsGl",
      "expanded_url" : "http://www.emojitracker.com/details/1F60A",
      "display_url" : "emojitracker.com/details/1F60A"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360906603643408384",
  "text" : "Watching this whilst listening to Underground German Trance Music, before trance was even a genre in record stores: http://t.co/hxLYgqqsGl",
  "id" : 360906603643408384,
  "created_at" : "Fri Jul 26 23:36:54 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "336786932271034368",
  "geo" : {
  },
  "id_str" : "360900391229071360",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 As ever, Christian, you are awesome!",
  "id" : 360900391229071360,
  "in_reply_to_status_id" : 336786932271034368,
  "created_at" : "Fri Jul 26 23:12:13 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 3, 11 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "utf8",
      "indices" : [ 91, 96 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http://t.co/fW1eEJARxb",
      "expanded_url" : "http://unicod.es/",
      "display_url" : "unicod.es"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360900229928730626",
  "text" : "RT @codepo8: Lots and lots of unicode tools - http://t.co/fW1eEJARxb - might come in handy #utf8",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "utf8",
        "indices" : [ 78, 83 ]
      } ],
      "urls" : [ {
        "indices" : [ 33, 55 ],
        "url" : "http://t.co/fW1eEJARxb",
        "expanded_url" : "http://unicod.es/",
        "display_url" : "unicod.es"
      } ]
    },
    "geo" : {
    },
    "id_str" : "336786932271034368",
    "text" : "Lots and lots of unicode tools - http://t.co/fW1eEJARxb - might come in handy #utf8",
    "id" : 336786932271034368,
    "created_at" : "Tue May 21 10:13:56 +0000 2013",
    "user" : {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "protected" : false,
      "id_str" : "13567",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1666904408/codepo8_normal.png",
      "id" : 13567,
      "verified" : false
    }
  },
  "id" : 360900229928730626,
  "created_at" : "Fri Jul 26 23:11:34 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigitalOcean",
      "screen_name" : "digitalocean",
      "indices" : [ 3, 16 ],
      "id_str" : "457033547",
      "id" : 457033547
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360896628602839041",
  "text" : "RT @digitalocean: Our site is temporary down. No virtual servers are affected. We are viewing the code deploy and should be back to normal \u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.hootsuite.com\" rel=\"nofollow\">HootSuite</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "360892573897854977",
    "text" : "Our site is temporary down. No virtual servers are affected. We are viewing the code deploy and should be back to normal soon.",
    "id" : 360892573897854977,
    "created_at" : "Fri Jul 26 22:41:09 +0000 2013",
    "user" : {
      "name" : "DigitalOcean",
      "screen_name" : "digitalocean",
      "protected" : false,
      "id_str" : "457033547",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2623921949/332ck3jglwnubobal3c2_normal.png",
      "id" : 457033547,
      "verified" : false
    }
  },
  "id" : 360896628602839041,
  "created_at" : "Fri Jul 26 22:57:16 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 140 ],
      "url" : "http://t.co/YmvaW4kQAa",
      "expanded_url" : "http://isharefil.es/QTSK",
      "display_url" : "isharefil.es/QTSK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360894816554459136",
  "text" : "Cybercrime continues to surprise me in its techniques. Tweet about Win8 - get a reply with a link to a loaded server: http://t.co/YmvaW4kQAa",
  "id" : 360894816554459136,
  "created_at" : "Fri Jul 26 22:50:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "indices" : [ 3, 13 ],
      "id_str" : "605929797",
      "id" : 605929797
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 40 ],
      "url" : "http://t.co/q5ZF01mvdO",
      "expanded_url" : "http://www.dronestagr.am/",
      "display_url" : "dronestagr.am"
    }, {
      "indices" : [ 66, 88 ],
      "url" : "http://t.co/FtTWaBlOps",
      "expanded_url" : "http://fplus.me/p/2BY8",
      "display_url" : "fplus.me/p/2BY8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360890837023993856",
  "text" : "RT @lunchduty: RT http://t.co/q5ZF01mvdO is Instagram for drones. http://t.co/FtTWaBlOps",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.friendsplus.me\" rel=\"nofollow\">Friends+Me</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 3, 25 ],
        "url" : "http://t.co/q5ZF01mvdO",
        "expanded_url" : "http://www.dronestagr.am/",
        "display_url" : "dronestagr.am"
      }, {
        "indices" : [ 51, 73 ],
        "url" : "http://t.co/FtTWaBlOps",
        "expanded_url" : "http://fplus.me/p/2BY8",
        "display_url" : "fplus.me/p/2BY8"
      } ]
    },
    "geo" : {
    },
    "id_str" : "359884453641846784",
    "text" : "RT http://t.co/q5ZF01mvdO is Instagram for drones. http://t.co/FtTWaBlOps",
    "id" : 359884453641846784,
    "created_at" : "Wed Jul 24 03:55:14 +0000 2013",
    "user" : {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "protected" : false,
      "id_str" : "605929797",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3566749477/5025656cb423825e1d0748cf13c87ca3_normal.png",
      "id" : 605929797,
      "verified" : false
    }
  },
  "id" : 360890837023993856,
  "created_at" : "Fri Jul 26 22:34:15 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Rothenberg",
      "screen_name" : "mroth",
      "indices" : [ 99, 105 ],
      "id_str" : "33883",
      "id" : 33883
    }, {
      "name" : "rocket workshop ",
      "screen_name" : "rocketws",
      "indices" : [ 106, 115 ],
      "id_str" : "1468581242",
      "id" : 1468581242
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "emojitracker",
      "indices" : [ 84, 97 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 52 ],
      "url" : "http://t.co/hxLYgqqsGl",
      "expanded_url" : "http://www.emojitracker.com/details/1F60A",
      "display_url" : "emojitracker.com/details/1F60A"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360880758774173697",
  "text" : "This is a very popular emoji; http://t.co/hxLYgqqsGl Would love to know what #1 is? #emojitracker +@mroth @rocketws",
  "id" : 360880758774173697,
  "created_at" : "Fri Jul 26 21:54:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 0, 8 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360863459899998209",
  "geo" : {
  },
  "id_str" : "360876062487482368",
  "in_reply_to_user_id" : 532923,
  "text" : "@mathias OMG This is beautiful \uD83C\uDF6D",
  "id" : 360876062487482368,
  "in_reply_to_status_id" : 360863459899998209,
  "created_at" : "Fri Jul 26 21:35:32 +0000 2013",
  "in_reply_to_screen_name" : "mathias",
  "in_reply_to_user_id_str" : "532923",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 3, 11 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360875800679034880",
  "text" : "RT @mathias: Just one of the many examples of astral Unicode symbols in the wild \u2014 track real-time emoji usage on Twitter: http://t.co/JqtV\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 110, 132 ],
        "url" : "http://t.co/JqtVPytAwu",
        "expanded_url" : "http://www.emojitracker.com/",
        "display_url" : "emojitracker.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "360863459899998209",
    "text" : "Just one of the many examples of astral Unicode symbols in the wild \u2014 track real-time emoji usage on Twitter: http://t.co/JqtVPytAwu",
    "id" : 360863459899998209,
    "created_at" : "Fri Jul 26 20:45:28 +0000 2013",
    "user" : {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "protected" : false,
      "id_str" : "532923",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1255767431/kung-fu_normal.jpg",
      "id" : 532923,
      "verified" : false
    }
  },
  "id" : 360875800679034880,
  "created_at" : "Fri Jul 26 21:34:30 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Johanna Ruiz",
      "screen_name" : "Johanna_B_Ruiz",
      "indices" : [ 1, 16 ],
      "id_str" : "236945569",
      "id" : 236945569
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 59 ],
      "url" : "http://t.co/GeoPG0hSxI",
      "expanded_url" : "http://www.fancy.com/davidhiggins",
      "display_url" : "fancy.com/davidhiggins"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360847083500077056",
  "text" : ".@Johanna_B_Ruiz Here's mine. Enjoy: http://t.co/GeoPG0hSxI Add me ;)",
  "id" : 360847083500077056,
  "created_at" : "Fri Jul 26 19:40:23 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 62 ],
      "url" : "http://t.co/ukYsrNywEi",
      "expanded_url" : "http://m.afr.com/p/technology/spy_agencies_ban_lenovo_pcs_on_security_HVgcKTHp4bIA4ulCPqC7SL",
      "display_url" : "m.afr.com/p/technology/s\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360843027268370432",
  "text" : "Backdoored circuitry in Lenovo laptops. http://t.co/ukYsrNywEi Interesting. Nothing is safe! Not even our SIM cards.",
  "id" : 360843027268370432,
  "created_at" : "Fri Jul 26 19:24:16 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Victor \u2603",
      "screen_name" : "_victa",
      "indices" : [ 90, 97 ],
      "id_str" : "47319826",
      "id" : 47319826
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 35 ],
      "url" : "http://t.co/JZuCRQdlbc",
      "expanded_url" : "http://give-n-go.tumblr.com/",
      "display_url" : "give-n-go.tumblr.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360834959654653952",
  "text" : "Lickable ... http://t.co/JZuCRQdlbc For all the UX/UI people this will make your day. cc/ @_victa",
  "id" : 360834959654653952,
  "created_at" : "Fri Jul 26 18:52:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 48 ],
      "url" : "https://t.co/cIT6HR4pfG",
      "expanded_url" : "https://github.com/jehna/VerbalExpressions",
      "display_url" : "github.com/jehna/VerbalEx\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360834167862341632",
  "text" : "Cute. Verbal expressions https://t.co/cIT6HR4pfG A lazy man's way to write regular expressions. I won't use it. But as I said... It's Cute.",
  "id" : 360834167862341632,
  "created_at" : "Fri Jul 26 18:49:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "indices" : [ 3, 14 ],
      "id_str" : "517746290",
      "id" : 517746290
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 90 ],
      "url" : "http://t.co/Gz4BDPl7a7",
      "expanded_url" : "http://www.objectplayground.com/",
      "display_url" : "objectplayground.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360832121264291841",
  "text" : "RT @PinPopular: Object Playground: The JavaScript Object Visualizer http://t.co/Gz4BDPl7a7",
  "retweeted_status" : {
    "source" : "<a href=\"http://zhangchi.de/\" rel=\"nofollow\">Pinboard Popular</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 52, 74 ],
        "url" : "http://t.co/Gz4BDPl7a7",
        "expanded_url" : "http://www.objectplayground.com/",
        "display_url" : "objectplayground.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "360830901078343680",
    "text" : "Object Playground: The JavaScript Object Visualizer http://t.co/Gz4BDPl7a7",
    "id" : 360830901078343680,
    "created_at" : "Fri Jul 26 18:36:05 +0000 2013",
    "user" : {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "protected" : false,
      "id_str" : "517746290",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000170092903/61a9facf585dc87b3054e01059a11988_normal.png",
      "id" : 517746290,
      "verified" : false
    }
  },
  "id" : 360832121264291841,
  "created_at" : "Fri Jul 26 18:40:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Julien G",
      "screen_name" : "molokoloco",
      "indices" : [ 34, 45 ],
      "id_str" : "14637220",
      "id" : 14637220
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http://t.co/hoOuYBTcNt",
      "expanded_url" : "http://www.b2bweb.fr/gueznet/gueznet-n51-les-news-du-front-world-code-and-politics-22072013/",
      "display_url" : "b2bweb.fr/gueznet/guezne\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360826902912573441",
  "text" : "Just found this as recommended by @molokoloco http://t.co/hoOuYBTcNt (In French) But still very awesome.",
  "id" : 360826902912573441,
  "created_at" : "Fri Jul 26 18:20:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Julien G",
      "screen_name" : "molokoloco",
      "indices" : [ 0, 11 ],
      "id_str" : "14637220",
      "id" : 14637220
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360813126939185154",
  "geo" : {
  },
  "id_str" : "360826545809530880",
  "in_reply_to_user_id" : 14637220,
  "text" : "@molokoloco My lord that is some great resource. Thanks for the share",
  "id" : 360826545809530880,
  "in_reply_to_status_id" : 360813126939185154,
  "created_at" : "Fri Jul 26 18:18:47 +0000 2013",
  "in_reply_to_screen_name" : "molokoloco",
  "in_reply_to_user_id_str" : "14637220",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 137 ],
      "url" : "http://t.co/X4cNVHJMxA",
      "expanded_url" : "http://myshar.es/11w2O5c",
      "display_url" : "myshar.es/11w2O5c"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360810255120211970",
  "text" : "Some significant new libraries released this week. Details in this week's best of JavaScript, HTML and CSS update. http://t.co/X4cNVHJMxA",
  "id" : 360810255120211970,
  "created_at" : "Fri Jul 26 17:14:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 102 ],
      "url" : "http://t.co/QLOeswAmqF",
      "expanded_url" : "http://myshar.es/11w8k7T",
      "display_url" : "myshar.es/11w8k7T"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360808749440581632",
  "text" : "Allan Berger discusses techniques for getting website ready for Retina display. http://t.co/QLOeswAmqF",
  "id" : 360808749440581632,
  "created_at" : "Fri Jul 26 17:08:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MiniDrive",
      "screen_name" : "The_Mini_Drive",
      "indices" : [ 0, 15 ],
      "id_str" : "1015839480",
      "id" : 1015839480
    }, {
      "name" : "Joshua Gross",
      "screen_name" : "endtwist",
      "indices" : [ 16, 25 ],
      "id_str" : "181344408",
      "id" : 181344408
    }, {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 26, 36 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360803730947444736",
  "geo" : {
  },
  "id_str" : "360804112314540033",
  "in_reply_to_user_id" : 1015839480,
  "text" : "@The_Mini_Drive @endtwist @film_girl Not the Nifty one, so won't buy LOL",
  "id" : 360804112314540033,
  "in_reply_to_status_id" : 360803730947444736,
  "created_at" : "Fri Jul 26 16:49:38 +0000 2013",
  "in_reply_to_screen_name" : "The_Mini_Drive",
  "in_reply_to_user_id_str" : "1015839480",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Joshua Gross",
      "screen_name" : "endtwist",
      "indices" : [ 0, 9 ],
      "id_str" : "181344408",
      "id" : 181344408
    }, {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 10, 20 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http://t.co/opgXuOVyfu",
      "expanded_url" : "http://www.kickstarter.com/projects/1342319572/the-nifty-minidrive",
      "display_url" : "kickstarter.com/projects/13423\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "360791075490762753",
  "geo" : {
  },
  "id_str" : "360792848347578369",
  "in_reply_to_user_id" : 181344408,
  "text" : "@endtwist @film_girl Yeah here's the product: http://t.co/opgXuOVyfu The Nifty Mindrive. Perfect for your situation Christina!",
  "id" : 360792848347578369,
  "in_reply_to_status_id" : 360791075490762753,
  "created_at" : "Fri Jul 26 16:04:52 +0000 2013",
  "in_reply_to_screen_name" : "endtwist",
  "in_reply_to_user_id_str" : "181344408",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Patrick Haney",
      "screen_name" : "notasausage",
      "indices" : [ 3, 15 ],
      "id_str" : "11718",
      "id" : 11718
    }, {
      "name" : "John Resig",
      "screen_name" : "jeresig",
      "indices" : [ 23, 31 ],
      "id_str" : "752673",
      "id" : 752673
    }, {
      "name" : "jQuery",
      "screen_name" : "jquery",
      "indices" : [ 43, 50 ],
      "id_str" : "14538601",
      "id" : 14538601
    }, {
      "name" : "LinkedIn",
      "screen_name" : "LinkedIn",
      "indices" : [ 58, 67 ],
      "id_str" : "13058772",
      "id" : 13058772
    } ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/notasausage/status/360788936282501120/photo/1",
      "indices" : [ 83, 105 ],
      "url" : "http://t.co/UoLBIqp2OS",
      "media_url" : "http://pbs.twimg.com/media/BQHHoEJCcAEa-Vs.png",
      "id_str" : "360788936286695425",
      "id" : 360788936286695425,
      "media_url_https" : "https://pbs.twimg.com/media/BQHHoEJCcAEa-Vs.png",
      "sizes" : [ {
        "h" : 151,
        "resize" : "fit",
        "w" : 304
      }, {
        "h" : 151,
        "resize" : "fit",
        "w" : 304
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 151,
        "resize" : "fit",
        "w" : 304
      }, {
        "h" : 151,
        "resize" : "fit",
        "w" : 304
      } ],
      "display_url" : "pic.twitter.com/UoLBIqp2OS"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360789645887414274",
  "text" : "RT @notasausage: \u201CDoes @jeresig know about @jQuery?\u201D\n\nOh, @LinkedIn, you so funny. http://t.co/UoLBIqp2OS",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/software/tweetbot/mac\" rel=\"nofollow\">Tweetbot for Mac</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "John Resig",
        "screen_name" : "jeresig",
        "indices" : [ 6, 14 ],
        "id_str" : "752673",
        "id" : 752673
      }, {
        "name" : "jQuery",
        "screen_name" : "jquery",
        "indices" : [ 26, 33 ],
        "id_str" : "14538601",
        "id" : 14538601
      }, {
        "name" : "LinkedIn",
        "screen_name" : "LinkedIn",
        "indices" : [ 41, 50 ],
        "id_str" : "13058772",
        "id" : 13058772
      } ],
      "media" : [ {
        "expanded_url" : "http://twitter.com/notasausage/status/360788936282501120/photo/1",
        "indices" : [ 66, 88 ],
        "url" : "http://t.co/UoLBIqp2OS",
        "media_url" : "http://pbs.twimg.com/media/BQHHoEJCcAEa-Vs.png",
        "id_str" : "360788936286695425",
        "id" : 360788936286695425,
        "media_url_https" : "https://pbs.twimg.com/media/BQHHoEJCcAEa-Vs.png",
        "sizes" : [ {
          "h" : 151,
          "resize" : "fit",
          "w" : 304
        }, {
          "h" : 151,
          "resize" : "fit",
          "w" : 304
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 151,
          "resize" : "fit",
          "w" : 304
        }, {
          "h" : 151,
          "resize" : "fit",
          "w" : 304
        } ],
        "display_url" : "pic.twitter.com/UoLBIqp2OS"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "360788936282501120",
    "text" : "\u201CDoes @jeresig know about @jQuery?\u201D\n\nOh, @LinkedIn, you so funny. http://t.co/UoLBIqp2OS",
    "id" : 360788936282501120,
    "created_at" : "Fri Jul 26 15:49:20 +0000 2013",
    "user" : {
      "name" : "Patrick Haney",
      "screen_name" : "notasausage",
      "protected" : false,
      "id_str" : "11718",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3763891173/17614d88fcaffc821dbeefe42c352323_normal.jpeg",
      "id" : 11718,
      "verified" : false
    }
  },
  "id" : 360789645887414274,
  "created_at" : "Fri Jul 26 15:52:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gwen Bell",
      "screen_name" : "gwenbell",
      "indices" : [ 14, 23 ],
      "id_str" : "1250104952",
      "id" : 1250104952
    }, {
      "name" : "Ven Portman",
      "screen_name" : "venportman",
      "indices" : [ 26, 37 ],
      "id_str" : "502182983",
      "id" : 502182983
    }, {
      "name" : "Chad Whitacre",
      "screen_name" : "whit537",
      "indices" : [ 49, 57 ],
      "id_str" : "34175404",
      "id" : 34175404
    }, {
      "name" : "Gittip",
      "screen_name" : "Gittip",
      "indices" : [ 64, 71 ],
      "id_str" : "1313765257",
      "id" : 1313765257
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bitters",
      "indices" : [ 119, 127 ]
    } ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http://t.co/Wamm8wCQcD",
      "expanded_url" : "http://www.youtube.com/watch?v=AmHuTYPt0fE",
      "display_url" : "youtube.com/watch?v=AmHuTY\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360789445206736896",
  "text" : "Watched this: @gwenbell + @venportman talking to @whit537 about @Gittip http://t.co/Wamm8wCQcD They work quite hard on #bitters Well done!",
  "id" : 360789445206736896,
  "created_at" : "Fri Jul 26 15:51:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360786487425503232",
  "geo" : {
  },
  "id_str" : "360787343722020864",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn I am a fan of this one: error_reporting(-1); Reports everything under the sun.",
  "id" : 360787343722020864,
  "in_reply_to_status_id" : 360786487425503232,
  "created_at" : "Fri Jul 26 15:43:00 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 139 ],
      "url" : "http://t.co/M5fVgrAl9r",
      "expanded_url" : "http://php.net/manual/en/function.error-reporting.php",
      "display_url" : "php.net/manual/en/func\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "360786487425503232",
  "geo" : {
  },
  "id_str" : "360787175077462017",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn No idea. But you can turn on error reporting to get a dump of what went wrong (if something's going wrong). http://t.co/M5fVgrAl9r",
  "id" : 360787175077462017,
  "in_reply_to_status_id" : 360786487425503232,
  "created_at" : "Fri Jul 26 15:42:20 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Benjamin Hawkyard",
      "screen_name" : "nhqe",
      "indices" : [ 0, 5 ],
      "id_str" : "44999727",
      "id" : 44999727
    }, {
      "name" : "Joey",
      "screen_name" : "joericho",
      "indices" : [ 66, 75 ],
      "id_str" : "218824452",
      "id" : 218824452
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 39 ],
      "url" : "http://t.co/wHibCj3o4E",
      "expanded_url" : "http://joey.so/west/js/main.js",
      "display_url" : "joey.so/west/js/main.js"
    } ]
  },
  "in_reply_to_status_id_str" : "360782120593002498",
  "geo" : {
  },
  "id_str" : "360782713566932992",
  "in_reply_to_user_id" : 44999727,
  "text" : "@nhqe Yes it is (http://t.co/wHibCj3o4E) But it has swipe events! @joericho actually cares for mobile in his scripts. Yay!",
  "id" : 360782713566932992,
  "in_reply_to_status_id" : 360782120593002498,
  "created_at" : "Fri Jul 26 15:24:36 +0000 2013",
  "in_reply_to_screen_name" : "nhqe",
  "in_reply_to_user_id_str" : "44999727",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360779445717581824",
  "geo" : {
  },
  "id_str" : "360781672851050497",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Are you in the weird part of Youtube again? Get out of there quickly, before it's too late!",
  "id" : 360781672851050497,
  "in_reply_to_status_id" : 360779445717581824,
  "created_at" : "Fri Jul 26 15:20:28 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 30 ],
      "url" : "http://t.co/1dv6mQg9sZ",
      "expanded_url" : "http://joey.so/west/",
      "display_url" : "joey.so/west/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360780601965559810",
  "text" : "Awesome http://t.co/1dv6mQg9sZ",
  "id" : 360780601965559810,
  "created_at" : "Fri Jul 26 15:16:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https://t.co/rKtZ00n4Ck",
      "expanded_url" : "https://news.ycombinator.com/item?id=6105026",
      "display_url" : "news.ycombinator.com/item?id=6105026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360753844193472514",
  "text" : "For those of you on Hackernews, can you upvote this please? https://t.co/rKtZ00n4Ck",
  "id" : 360753844193472514,
  "created_at" : "Fri Jul 26 13:29:53 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Prinzhorn",
      "screen_name" : "Prinzhorn",
      "indices" : [ 0, 10 ],
      "id_str" : "187226449",
      "id" : 187226449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360706673242939393",
  "geo" : {
  },
  "id_str" : "360737416887279616",
  "in_reply_to_user_id" : 187226449,
  "text" : "@Prinzhorn Pushstate FTW",
  "id" : 360737416887279616,
  "in_reply_to_status_id" : 360706673242939393,
  "created_at" : "Fri Jul 26 12:24:37 +0000 2013",
  "in_reply_to_screen_name" : "Prinzhorn",
  "in_reply_to_user_id_str" : "187226449",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashleigh ",
      "screen_name" : "Ashleigh1Kelly",
      "indices" : [ 0, 15 ],
      "id_str" : "235705822",
      "id" : 235705822
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360551161058689024",
  "geo" : {
  },
  "id_str" : "360559238302547969",
  "in_reply_to_user_id" : 235705822,
  "text" : "@Ashleigh1Kelly Taste betaaar melted down. Used to buy boxes of unfrozen cool pops. Cola was me fave",
  "id" : 360559238302547969,
  "in_reply_to_status_id" : 360551161058689024,
  "created_at" : "Fri Jul 26 00:36:35 +0000 2013",
  "in_reply_to_screen_name" : "Ashleigh1Kelly",
  "in_reply_to_user_id_str" : "235705822",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Obney",
      "screen_name" : "justinobney",
      "indices" : [ 3, 15 ],
      "id_str" : "24695744",
      "id" : 24695744
    }, {
      "name" : "Casey Paiz",
      "screen_name" : "caseypaiz",
      "indices" : [ 17, 27 ],
      "id_str" : "21032272",
      "id" : 21032272
    }, {
      "name" : "David H.",
      "screen_name" : "alphenic",
      "indices" : [ 28, 37 ],
      "id_str" : "468853739",
      "id" : 468853739
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360557904555819010",
  "text" : "RT @justinobney: @caseypaiz @alphenic \"he would have used == if it was just for attention ...\"",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Casey Paiz",
        "screen_name" : "caseypaiz",
        "indices" : [ 0, 10 ],
        "id_str" : "21032272",
        "id" : 21032272
      }, {
        "name" : "David H.",
        "screen_name" : "alphenic",
        "indices" : [ 11, 20 ],
        "id_str" : "468853739",
        "id" : 468853739
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "360540450806960128",
    "geo" : {
    },
    "id_str" : "360557771114037251",
    "in_reply_to_user_id" : 21032272,
    "text" : "@caseypaiz @alphenic \"he would have used == if it was just for attention ...\"",
    "id" : 360557771114037251,
    "in_reply_to_status_id" : 360540450806960128,
    "created_at" : "Fri Jul 26 00:30:46 +0000 2013",
    "in_reply_to_screen_name" : "caseypaiz",
    "in_reply_to_user_id_str" : "21032272",
    "user" : {
      "name" : "Justin Obney",
      "screen_name" : "justinobney",
      "protected" : false,
      "id_str" : "24695744",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000239906494/5dff138c544959abb654042ff9e5b9b4_normal.jpeg",
      "id" : 24695744,
      "verified" : false
    }
  },
  "id" : 360557904555819010,
  "created_at" : "Fri Jul 26 00:31:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 3, 13 ],
      "id_str" : "175727560",
      "id" : 175727560
    }, {
      "name" : "Peter McLachlan",
      "screen_name" : "b1tr0t",
      "indices" : [ 16, 23 ],
      "id_str" : "13259922",
      "id" : 13259922
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WebPlatformDaily",
      "indices" : [ 95, 112 ]
    } ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http://t.co/1u23mieuNW",
      "expanded_url" : "http://moz.com/ugc/the-definitive-guide-to-googles-new-mobile-seo-rules",
      "display_url" : "moz.com/ugc/the-defini\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360557291864465409",
  "text" : "RT @simevidas: .@b1tr0t wrote a guide to Google's new mobile SEO rules: http://t.co/1u23mieuNW\n#WebPlatformDaily",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Peter McLachlan",
        "screen_name" : "b1tr0t",
        "indices" : [ 1, 8 ],
        "id_str" : "13259922",
        "id" : 13259922
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "WebPlatformDaily",
        "indices" : [ 80, 97 ]
      } ],
      "urls" : [ {
        "indices" : [ 57, 79 ],
        "url" : "http://t.co/1u23mieuNW",
        "expanded_url" : "http://moz.com/ugc/the-definitive-guide-to-googles-new-mobile-seo-rules",
        "display_url" : "moz.com/ugc/the-defini\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "360511637461008384",
    "text" : ".@b1tr0t wrote a guide to Google's new mobile SEO rules: http://t.co/1u23mieuNW\n#WebPlatformDaily",
    "id" : 360511637461008384,
    "created_at" : "Thu Jul 25 21:27:27 +0000 2013",
    "user" : {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "protected" : false,
      "id_str" : "175727560",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000065125916/326dca11327b0e650c17bb7f5722e393_normal.jpeg",
      "id" : 175727560,
      "verified" : false
    }
  },
  "id" : 360557291864465409,
  "created_at" : "Fri Jul 26 00:28:51 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 31, 39 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "nofilter",
      "indices" : [ 10, 19 ]
    } ],
    "urls" : [ {
      "indices" : [ 102, 124 ],
      "url" : "http://t.co/KcFsPyFRGE",
      "expanded_url" : "http://brandonb.cc/no-filter-the-meanest-thing-paul-graham-said-to-a-startup",
      "display_url" : "brandonb.cc/no-filter-the-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360546679969157122",
  "text" : "TIL - The #nofilter message in @codepo8's profile is not about Instagram. It's about brutal honesty - http://t.co/KcFsPyFRGE",
  "id" : 360546679969157122,
  "created_at" : "Thu Jul 25 23:46:41 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex Muraro",
      "screen_name" : "akmur",
      "indices" : [ 3, 9 ],
      "id_str" : "7426572",
      "id" : 7426572
    }, {
      "name" : "David H.",
      "screen_name" : "alphenic",
      "indices" : [ 11, 20 ],
      "id_str" : "468853739",
      "id" : 468853739
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360541008745865216",
  "text" : "RT @akmur: @alphenic $('body').unbind('life');",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetcaster.com\" rel=\"nofollow\">TweetCaster for Android</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "David H.",
        "screen_name" : "alphenic",
        "indices" : [ 0, 9 ],
        "id_str" : "468853739",
        "id" : 468853739
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "360519743330975745",
    "geo" : {
    },
    "id_str" : "360540885437513729",
    "in_reply_to_user_id" : 468853739,
    "text" : "@alphenic $('body').unbind('life');",
    "id" : 360540885437513729,
    "in_reply_to_status_id" : 360519743330975745,
    "created_at" : "Thu Jul 25 23:23:40 +0000 2013",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "Alex Muraro",
      "screen_name" : "akmur",
      "protected" : false,
      "id_str" : "7426572",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3305998338/8efe54de5f7a6dddcf5c5488e7e32a82_normal.jpeg",
      "id" : 7426572,
      "verified" : false
    }
  },
  "id" : 360541008745865216,
  "created_at" : "Thu Jul 25 23:24:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Casey Paiz",
      "screen_name" : "caseypaiz",
      "indices" : [ 3, 13 ],
      "id_str" : "21032272",
      "id" : 21032272
    }, {
      "name" : "David H.",
      "screen_name" : "alphenic",
      "indices" : [ 15, 24 ],
      "id_str" : "468853739",
      "id" : 468853739
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360540572311760897",
  "text" : "RT @caseypaiz: @alphenic if((typeof lastHope === 'undefined') &amp;&amp; ((typeof nextHope === 'undefined'))){ setTimeout(function(){woodchipper.se\u2026",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "D. H\u0336I\u0336G\u0336G\u0336I\u0336N\u0336S",
        "screen_name" : "alphenic",
        "indices" : [ 0, 9 ],
        "id_str" : "468853739",
        "id" : 468853739
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "360519743330975745",
    "geo" : {
    },
    "id_str" : "360540450806960128",
    "in_reply_to_user_id" : 468853739,
    "text" : "@alphenic if((typeof lastHope === 'undefined') &amp;&amp; ((typeof nextHope === 'undefined'))){ setTimeout(function(){woodchipper.self.feed();},0);}",
    "id" : 360540450806960128,
    "in_reply_to_status_id" : 360519743330975745,
    "created_at" : "Thu Jul 25 23:21:56 +0000 2013",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "Casey Paiz",
      "screen_name" : "caseypaiz",
      "protected" : false,
      "id_str" : "21032272",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/292640264/twit_pic_normal.jpg",
      "id" : 21032272,
      "verified" : false
    }
  },
  "id" : 360540572311760897,
  "created_at" : "Thu Jul 25 23:22:25 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Disqus",
      "screen_name" : "disqus",
      "indices" : [ 89, 96 ],
      "id_str" : "14130628",
      "id" : 14130628
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Chromecast",
      "indices" : [ 36, 47 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360534521055162368",
  "text" : "If you hunt around for the negative #Chromecast comments, you will fail. They are not in @Disqus widgets. They are in tweets just like this.",
  "id" : 360534521055162368,
  "created_at" : "Thu Jul 25 22:58:22 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 108, 119 ]
    }, {
      "text" : "AskTwitter",
      "indices" : [ 120, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360519743330975745",
  "text" : "If you were to write your suicide note in Javascript, how would you code it?\n\n(140 characters max, please)\n\n#javascript #AskTwitter",
  "id" : 360519743330975745,
  "created_at" : "Thu Jul 25 21:59:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Storey",
      "screen_name" : "dstorey",
      "indices" : [ 0, 8 ],
      "id_str" : "72623",
      "id" : 72623
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 70 ],
      "url" : "http://t.co/hb1PElFa2I",
      "expanded_url" : "http://isharefil.es/QVB7",
      "display_url" : "isharefil.es/QVB7"
    } ]
  },
  "in_reply_to_status_id_str" : "360513871468961793",
  "geo" : {
  },
  "id_str" : "360517095403945984",
  "in_reply_to_user_id" : 72623,
  "text" : "@dstorey Here's some images to get you started: http://t.co/hb1PElFa2I",
  "id" : 360517095403945984,
  "in_reply_to_status_id" : 360513871468961793,
  "created_at" : "Thu Jul 25 21:49:08 +0000 2013",
  "in_reply_to_screen_name" : "dstorey",
  "in_reply_to_user_id_str" : "72623",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360514448802316288",
  "text" : "I am starting to think of some open source projects as akin to Government powered projects with unlimited money and talent thrown at them!?",
  "id" : 360514448802316288,
  "created_at" : "Thu Jul 25 21:38:37 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SpeedAwarenessMonth",
      "screen_name" : "SpeedMonth",
      "indices" : [ 3, 14 ],
      "id_str" : "632445769",
      "id" : 632445769
    }, {
      "name" : "SpeedAwarenessMonth",
      "screen_name" : "SpeedMonth",
      "indices" : [ 92, 103 ],
      "id_str" : "632445769",
      "id" : 632445769
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SAM2013",
      "indices" : [ 16, 24 ]
    } ],
    "urls" : [ {
      "indices" : [ 65, 87 ],
      "url" : "http://t.co/R5DrqPUhZm",
      "expanded_url" : "http://shar.es/kpMjC",
      "display_url" : "shar.es/kpMjC"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360513049662201856",
  "text" : "RT @SpeedMonth: #SAM2013 is coming up... - Speed Awareness Month http://t.co/R5DrqPUhZm via @speedmonth",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SpeedAwarenessMonth",
        "screen_name" : "SpeedMonth",
        "indices" : [ 76, 87 ],
        "id_str" : "632445769",
        "id" : 632445769
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SAM2013",
        "indices" : [ 0, 8 ]
      } ],
      "urls" : [ {
        "indices" : [ 49, 71 ],
        "url" : "http://t.co/R5DrqPUhZm",
        "expanded_url" : "http://shar.es/kpMjC",
        "display_url" : "shar.es/kpMjC"
      } ]
    },
    "geo" : {
    },
    "id_str" : "358294674244509697",
    "text" : "#SAM2013 is coming up... - Speed Awareness Month http://t.co/R5DrqPUhZm via @speedmonth",
    "id" : 358294674244509697,
    "created_at" : "Fri Jul 19 18:38:01 +0000 2013",
    "user" : {
      "name" : "SpeedAwarenessMonth",
      "screen_name" : "SpeedMonth",
      "protected" : false,
      "id_str" : "632445769",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000225805619/7491745734d054b4552b573ab8a97fdc_normal.png",
      "id" : 632445769,
      "verified" : false
    }
  },
  "id" : 360513049662201856,
  "created_at" : "Thu Jul 25 21:33:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 63 ],
      "url" : "http://t.co/KDzmSZym1f",
      "expanded_url" : "http://blog.higg.im/590355",
      "display_url" : "blog.higg.im/590355"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360512710674354177",
  "text" : "Did this post for Speed Awareness Month: http://t.co/KDzmSZym1f Powering a jQuery Plugin Repo With a CDN - A Breakdown",
  "id" : 360512710674354177,
  "created_at" : "Thu Jul 25 21:31:42 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://nurph.com\" rel=\"nofollow\">Nurph</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nurph",
      "screen_name" : "Nurph",
      "indices" : [ 27, 33 ],
      "id_str" : "164768029",
      "id" : 164768029
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "5kopp",
      "indices" : [ 103, 109 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360502404459921408",
  "text" : "Gonna have to get off this @nurph thing. Might have to setup an account dedicated to Nurphing with PPL #5kopp",
  "id" : 360502404459921408,
  "created_at" : "Thu Jul 25 20:50:45 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://nurph.com\" rel=\"nofollow\">Nurph</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "5kopp",
      "indices" : [ 95, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360502146103394305",
  "text" : "Yeah, Aphenic is the Alpha Nic, or Number One Name. It is also a tonic for the common cold LOL #5kopp",
  "id" : 360502146103394305,
  "created_at" : "Thu Jul 25 20:49:44 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://nurph.com\" rel=\"nofollow\">Nurph</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "5kopp",
      "indices" : [ 100, 106 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360501664580505600",
  "text" : "This thing could make me lose a lot of followers. Twitter's noise filter will not work for this LOL #5kopp",
  "id" : 360501664580505600,
  "created_at" : "Thu Jul 25 20:47:49 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://nurph.com\" rel=\"nofollow\">Nurph</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "5kopp",
      "indices" : [ 23, 29 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360501365832818689",
  "text" : "This thing is awesome! #5kopp",
  "id" : 360501365832818689,
  "created_at" : "Thu Jul 25 20:46:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://nurph.com\" rel=\"nofollow\">Nurph</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "5kopp",
      "indices" : [ 33, 39 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360501324321796098",
  "text" : "Oh my, it works from Twitter too #5kopp",
  "id" : 360501324321796098,
  "created_at" : "Thu Jul 25 20:46:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "5kopp",
      "indices" : [ 40, 46 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360501251852607488",
  "text" : "Testing out posting from Twitter itself #5kopp",
  "id" : 360501251852607488,
  "created_at" : "Thu Jul 25 20:46:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://nurph.com\" rel=\"nofollow\">Nurph</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "5kopp",
      "indices" : [ 9, 15 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360501090690678786",
  "text" : "Hey brah #5kopp",
  "id" : 360501090690678786,
  "created_at" : "Thu Jul 25 20:45:32 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "indices" : [ 0, 6 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/pEKQdsgOks",
      "expanded_url" : "http://www.unicode.org/reports/tr36/",
      "display_url" : "unicode.org/reports/tr36/"
    } ]
  },
  "in_reply_to_status_id_str" : "360499763084402689",
  "geo" : {
  },
  "id_str" : "360500723542278144",
  "in_reply_to_user_id" : 23566038,
  "text" : "@mikko Oh no! Confusables striketh again! http://t.co/pEKQdsgOks",
  "id" : 360500723542278144,
  "in_reply_to_status_id" : 360499763084402689,
  "created_at" : "Thu Jul 25 20:44:04 +0000 2013",
  "in_reply_to_screen_name" : "mikko",
  "in_reply_to_user_id_str" : "23566038",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "indices" : [ 3, 14 ],
      "id_str" : "517746290",
      "id" : 517746290
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http://t.co/oEUqv8F8ZC",
      "expanded_url" : "http://zenhabits.net/quiet/",
      "display_url" : "zenhabits.net/quiet/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360499199105712129",
  "text" : "RT @PinPopular: Living the Quiet Life :  zenhabits http://t.co/oEUqv8F8ZC",
  "retweeted_status" : {
    "source" : "<a href=\"http://zhangchi.de/\" rel=\"nofollow\">Pinboard Popular</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 35, 57 ],
        "url" : "http://t.co/oEUqv8F8ZC",
        "expanded_url" : "http://zenhabits.net/quiet/",
        "display_url" : "zenhabits.net/quiet/"
      } ]
    },
    "geo" : {
    },
    "id_str" : "360498716496506880",
    "text" : "Living the Quiet Life :  zenhabits http://t.co/oEUqv8F8ZC",
    "id" : 360498716496506880,
    "created_at" : "Thu Jul 25 20:36:06 +0000 2013",
    "user" : {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "protected" : false,
      "id_str" : "517746290",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000170092903/61a9facf585dc87b3054e01059a11988_normal.png",
      "id" : 517746290,
      "verified" : false
    }
  },
  "id" : 360499199105712129,
  "created_at" : "Thu Jul 25 20:38:01 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "indices" : [ 1, 7 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360490117003087873",
  "text" : ".@mikko These work in Firefox only:\n\nwww\uFE12google\uFE12tm\nwww\uFE12microsoft\uFE12tm\nwww\uFE12nokia\uFE12tm",
  "id" : 360490117003087873,
  "created_at" : "Thu Jul 25 20:01:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "indices" : [ 3, 9 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360489245732913155",
  "text" : "RT @mikko: Stupid URL trick of the day - these addresses actually work:\nhttp://www\u2024google.\u2122\nhttp://www\u2024microsoft.\u2122\nhttp://www\u2024nokia.\u2122",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "360451707643236352",
    "text" : "Stupid URL trick of the day - these addresses actually work:\nhttp://www\u2024google.\u2122\nhttp://www\u2024microsoft.\u2122\nhttp://www\u2024nokia.\u2122",
    "id" : 360451707643236352,
    "created_at" : "Thu Jul 25 17:29:18 +0000 2013",
    "user" : {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "protected" : false,
      "id_str" : "23566038",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3428497729/8a03810d1c84ab31c512fb6660e85477_normal.jpeg",
      "id" : 23566038,
      "verified" : false
    }
  },
  "id" : 360489245732913155,
  "created_at" : "Thu Jul 25 19:58:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "indices" : [ 97, 104 ],
      "id_str" : "98478119",
      "id" : 98478119
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 39 ],
      "url" : "http://t.co/KDzmSZym1f",
      "expanded_url" : "http://blog.higg.im/590355",
      "display_url" : "blog.higg.im/590355"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360449108693098496",
  "text" : "My new blogpost: http://t.co/KDzmSZym1f Powering a jQuery Plugin Repo With a CDN - A Breakdown + @MaxCDN",
  "id" : 360449108693098496,
  "created_at" : "Thu Jul 25 17:18:59 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 49 ],
      "url" : "http://t.co/O0rYbH5EA2",
      "expanded_url" : "http://myshar.es/14xcL7H",
      "display_url" : "myshar.es/14xcL7H"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360447886292553728",
  "text" : "I know jQuery. Now what? - http://t.co/O0rYbH5EA2",
  "id" : 360447886292553728,
  "created_at" : "Thu Jul 25 17:14:07 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 97 ],
      "url" : "http://t.co/hdfMOCr5ea",
      "expanded_url" : "http://myshar.es/11w834P",
      "display_url" : "myshar.es/11w834P"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360446357242261504",
  "text" : "Promise &amp; Deferred objects in JavaScript Pt.1: Theory and Semantics. - http://t.co/hdfMOCr5ea",
  "id" : 360446357242261504,
  "created_at" : "Thu Jul 25 17:08:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 131 ],
      "url" : "http://t.co/CBz65j68hK",
      "expanded_url" : "http://myshar.es/1bwDgKT",
      "display_url" : "myshar.es/1bwDgKT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360444960077643777",
  "text" : "The HTML5 VIDEO tag on iOS downloads between 61K to 298K of video data without the user initiating playback. http://t.co/CBz65j68hK",
  "id" : 360444960077643777,
  "created_at" : "Thu Jul 25 17:02:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 134 ],
      "url" : "http://t.co/GOe8lRmeDW",
      "expanded_url" : "http://isharefil.es/QSKg",
      "display_url" : "isharefil.es/QSKg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360432318063710209",
  "text" : "So Google Plus is becoming like Twitter now where random pharma. companies randomly befriend you for no reason. http://t.co/GOe8lRmeDW",
  "id" : 360432318063710209,
  "created_at" : "Thu Jul 25 16:12:15 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https://t.co/SAbQxGwOew",
      "expanded_url" : "https://twitter.com/SoGithub/status/360131052217442305",
      "display_url" : "twitter.com/SoGithub/statu\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360423566115741697",
  "text" : "On a personal mission to get this retweeted as many times as possible: https://t.co/SAbQxGwOew Yes, I created the account. Now RT it or die!",
  "id" : 360423566115741697,
  "created_at" : "Thu Jul 25 15:37:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Max Celko",
      "screen_name" : "MaxCelko",
      "indices" : [ 3, 12 ],
      "id_str" : "28136841",
      "id" : 28136841
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360337036508532737",
  "text" : "RT @MaxCelko: \"Self-promotion triggers more self-promotion, and the world on social media gets further and further from reality.\" http://t.\u2026",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 138 ],
        "url" : "http://t.co/edFLalzjlh",
        "expanded_url" : "http://www.slate.com/articles/technology/technology/2013/07/instagram_and_self_esteem_why_the_photo_sharing_network_is_even_more_depressing.html",
        "display_url" : "slate.com/articles/techn\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "360336112109101056",
    "text" : "\"Self-promotion triggers more self-promotion, and the world on social media gets further and further from reality.\" http://t.co/edFLalzjlh",
    "id" : 360336112109101056,
    "created_at" : "Thu Jul 25 09:49:58 +0000 2013",
    "user" : {
      "name" : "Max Celko",
      "screen_name" : "MaxCelko",
      "protected" : false,
      "id_str" : "28136841",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/344513261569410574/10e199dcca25ed1845658740262f94d0_normal.jpeg",
      "id" : 28136841,
      "verified" : false
    }
  },
  "id" : 360337036508532737,
  "created_at" : "Thu Jul 25 09:53:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pekka Jokinen",
      "screen_name" : "dimangi",
      "indices" : [ 0, 8 ],
      "id_str" : "89481745",
      "id" : 89481745
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360330419054452737",
  "geo" : {
  },
  "id_str" : "360332261691883520",
  "in_reply_to_user_id" : 89481745,
  "text" : "@dimangi Whats wrong with NY?",
  "id" : 360332261691883520,
  "in_reply_to_status_id" : 360330419054452737,
  "created_at" : "Thu Jul 25 09:34:40 +0000 2013",
  "in_reply_to_screen_name" : "dimangi",
  "in_reply_to_user_id_str" : "89481745",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "indices" : [ 16, 23 ],
      "id_str" : "98478119",
      "id" : 98478119
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 104 ],
      "url" : "http://t.co/MaR3ab0xLp",
      "expanded_url" : "http://www.maxcdn.com/forum/?utm_content=buffer2594d&utm_source=buffer&utm_medium=twitter&utm_campaign=Buffer#!/integrations",
      "display_url" : "maxcdn.com/forum/?utm_con\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360331258968027136",
  "text" : "TIL, there is a @MaxCDN Chrome extension being developed. Now I can die in peace. http://t.co/MaR3ab0xLp",
  "id" : 360331258968027136,
  "created_at" : "Thu Jul 25 09:30:41 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 0, 9 ],
      "id_str" : "14139773",
      "id" : 14139773
    }, {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "indices" : [ 10, 17 ],
      "id_str" : "98478119",
      "id" : 98478119
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360263969648283650",
  "geo" : {
  },
  "id_str" : "360330734617100289",
  "in_reply_to_user_id" : 14139773,
  "text" : "@jdorfman @MaxCDN That looks awesome! Great use of the API.",
  "id" : 360330734617100289,
  "in_reply_to_status_id" : 360263969648283650,
  "created_at" : "Thu Jul 25 09:28:36 +0000 2013",
  "in_reply_to_screen_name" : "jdorfman",
  "in_reply_to_user_id_str" : "14139773",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Donovan Hutchinson",
      "screen_name" : "donovanh",
      "indices" : [ 3, 12 ],
      "id_str" : "280135383",
      "id" : 280135383
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 61 ],
      "url" : "http://t.co/IIqVHMIJpv",
      "expanded_url" : "http://blog.jenniferdewalt.com/post/56319597560/im-learning-to-code-by-building-180-websites-in-180",
      "display_url" : "blog.jenniferdewalt.com/post/563195975\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360327815935180800",
  "text" : "RT @donovanh: 180 websites in 180 days http://t.co/IIqVHMIJpv",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 25, 47 ],
        "url" : "http://t.co/IIqVHMIJpv",
        "expanded_url" : "http://blog.jenniferdewalt.com/post/56319597560/im-learning-to-code-by-building-180-websites-in-180",
        "display_url" : "blog.jenniferdewalt.com/post/563195975\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "360318165831516160",
    "text" : "180 websites in 180 days http://t.co/IIqVHMIJpv",
    "id" : 360318165831516160,
    "created_at" : "Thu Jul 25 08:38:39 +0000 2013",
    "user" : {
      "name" : "Donovan Hutchinson",
      "screen_name" : "donovanh",
      "protected" : false,
      "id_str" : "280135383",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3622323378/d8999912845a148f6dab43aa1d1ce733_normal.png",
      "id" : 280135383,
      "verified" : false
    }
  },
  "id" : 360327815935180800,
  "created_at" : "Thu Jul 25 09:17:00 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360299949906345985",
  "text" : "So the NSA uses the phrase \"person of interest\" a lot. There is also... Anomaly, Deviant, Miscreant, and the ever popular: Criminal.",
  "id" : 360299949906345985,
  "created_at" : "Thu Jul 25 07:26:16 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NetDNA Developer",
      "screen_name" : "NetDNADeveloper",
      "indices" : [ 3, 19 ],
      "id_str" : "599635414",
      "id" : 599635414
    }, {
      "name" : "Gittip",
      "screen_name" : "Gittip",
      "indices" : [ 30, 37 ],
      "id_str" : "1313765257",
      "id" : 1313765257
    }, {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "indices" : [ 93, 100 ],
      "id_str" : "98478119",
      "id" : 98478119
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OpenSource",
      "indices" : [ 106, 117 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360297900233867264",
  "text" : "RT @NetDNADeveloper: Today is @Gittip Pay day!  Time to redistribute the love (funds).  Tell @MaxCDN what #OpenSource projects you are work\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Gittip",
        "screen_name" : "Gittip",
        "indices" : [ 9, 16 ],
        "id_str" : "1313765257",
        "id" : 1313765257
      }, {
        "name" : "MaxCDN.com",
        "screen_name" : "MaxCDN",
        "indices" : [ 72, 79 ],
        "id_str" : "98478119",
        "id" : 98478119
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "OpenSource",
        "indices" : [ 85, 96 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "360297527955816448",
    "text" : "Today is @Gittip Pay day!  Time to redistribute the love (funds).  Tell @MaxCDN what #OpenSource projects you are working on and get paid =)",
    "id" : 360297527955816448,
    "created_at" : "Thu Jul 25 07:16:39 +0000 2013",
    "user" : {
      "name" : "NetDNA Developer",
      "screen_name" : "NetDNADeveloper",
      "protected" : false,
      "id_str" : "599635414",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2279049808/architectlogo_normal.jpg",
      "id" : 599635414,
      "verified" : false
    }
  },
  "id" : 360297900233867264,
  "created_at" : "Thu Jul 25 07:18:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https://t.co/xGf4BxbCQC",
      "expanded_url" : "https://en.wikipedia.org/wiki/Shakespeare_authorship_question",
      "display_url" : "en.wikipedia.org/wiki/Shakespea\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "360294409373351937",
  "geo" : {
  },
  "id_str" : "360295736648269825",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii https://t.co/xGf4BxbCQC",
  "id" : 360295736648269825,
  "in_reply_to_status_id" : 360294409373351937,
  "created_at" : "Thu Jul 25 07:09:32 +0000 2013",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360294409373351937",
  "geo" : {
  },
  "id_str" : "360294872231575552",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii Shakespeare was a team of writers. Not one man.",
  "id" : 360294872231575552,
  "in_reply_to_status_id" : 360294409373351937,
  "created_at" : "Thu Jul 25 07:06:06 +0000 2013",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adewale Oshineye",
      "screen_name" : "ade_oshineye",
      "indices" : [ 0, 13 ],
      "id_str" : "23254994",
      "id" : 23254994
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360157380853506048",
  "geo" : {
  },
  "id_str" : "360166067047366656",
  "in_reply_to_user_id" : 23254994,
  "text" : "@ade_oshineye Oshineye's Tweets As A Service",
  "id" : 360166067047366656,
  "in_reply_to_status_id" : 360157380853506048,
  "created_at" : "Wed Jul 24 22:34:16 +0000 2013",
  "in_reply_to_screen_name" : "ade_oshineye",
  "in_reply_to_user_id_str" : "23254994",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 1, 15 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360145289891426305",
  "text" : ".@soaj1664ashar Self proclaimed Gurus are not the ones to look for?! Hunt the ones lurking in the shadows!",
  "id" : 360145289891426305,
  "created_at" : "Wed Jul 24 21:11:42 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 32 ],
      "url" : "https://t.co/SAbQxGwOew",
      "expanded_url" : "https://twitter.com/SoGithub/status/360131052217442305",
      "display_url" : "twitter.com/SoGithub/statu\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360139200542146560",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 https://t.co/SAbQxGwOew",
  "id" : 360139200542146560,
  "created_at" : "Wed Jul 24 20:47:31 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Larry Hynes",
      "screen_name" : "larry_hynes",
      "indices" : [ 124, 136 ],
      "id_str" : "578272794",
      "id" : 578272794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360086853648527360",
  "geo" : {
  },
  "id_str" : "360120571125698560",
  "in_reply_to_user_id" : 578272794,
  "text" : "I stopped using 1337 speek when I realized today's laptops could achieve 1000 times more mayhem than the Cap'n Crunch days +@larry_hynes",
  "id" : 360120571125698560,
  "in_reply_to_status_id" : 360086853648527360,
  "created_at" : "Wed Jul 24 19:33:29 +0000 2013",
  "in_reply_to_screen_name" : "larry_hynes",
  "in_reply_to_user_id_str" : "578272794",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Larry Hynes",
      "screen_name" : "larry_hynes",
      "indices" : [ 3, 15 ],
      "id_str" : "578272794",
      "id" : 578272794
    }, {
      "name" : "David H.",
      "screen_name" : "alphenic",
      "indices" : [ 17, 26 ],
      "id_str" : "468853739",
      "id" : 468853739
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 78 ],
      "url" : "http://t.co/SOsfDbJVEX",
      "expanded_url" : "http://www.getdigital.de/index/start/lng/leet",
      "display_url" : "getdigital.de/index/start/ln\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "360118739615748097",
  "text" : "RT @larry_hynes: @alphenic You can buy here in 1337! :) http://t.co/SOsfDbJVEX",
  "retweeted_status" : {
    "source" : "<a href=\"http://sites.google.com/site/yorufukurou/\" rel=\"nofollow\">YoruFukurou</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "David H.",
        "screen_name" : "alphenic",
        "indices" : [ 0, 9 ],
        "id_str" : "468853739",
        "id" : 468853739
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 39, 61 ],
        "url" : "http://t.co/SOsfDbJVEX",
        "expanded_url" : "http://www.getdigital.de/index/start/lng/leet",
        "display_url" : "getdigital.de/index/start/ln\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "360086853648527360",
    "in_reply_to_user_id" : 468853739,
    "text" : "@alphenic You can buy here in 1337! :) http://t.co/SOsfDbJVEX",
    "id" : 360086853648527360,
    "created_at" : "Wed Jul 24 17:19:30 +0000 2013",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "Larry Hynes",
      "screen_name" : "larry_hynes",
      "protected" : false,
      "id_str" : "578272794",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/344513261565948783/0c2451dff4d313588460c72639fd6ad3_normal.jpeg",
      "id" : 578272794,
      "verified" : false
    }
  },
  "id" : 360118739615748097,
  "created_at" : "Wed Jul 24 19:26:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TheLawOfCrockford",
      "indices" : [ 11, 29 ]
    }, {
      "text" : "FalsyValue",
      "indices" : [ 30, 41 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360057132860702720",
  "text" : "=== != ==\n\n#TheLawOfCrockford\n#FalsyValue",
  "id" : 360057132860702720,
  "created_at" : "Wed Jul 24 15:21:24 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Stevens",
      "screen_name" : "Snetty",
      "indices" : [ 0, 7 ],
      "id_str" : "14783718",
      "id" : 14783718
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "360035129030230017",
  "geo" : {
  },
  "id_str" : "360035598590943232",
  "in_reply_to_user_id" : 14783718,
  "text" : "@Snetty Indeed. I guess that's where the market is. Never underestimate the technical unsaviness of your target audience!",
  "id" : 360035598590943232,
  "in_reply_to_status_id" : 360035129030230017,
  "created_at" : "Wed Jul 24 13:55:50 +0000 2013",
  "in_reply_to_screen_name" : "Snetty",
  "in_reply_to_user_id_str" : "14783718",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "360034540711972864",
  "text" : "\"Send files up to 50GB. And for $20.00 extra - send files up to 100GB\" Oh wow, who does that? That's why we split files into smaller pieces.",
  "id" : 360034540711972864,
  "created_at" : "Wed Jul 24 13:51:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lee Munroe",
      "screen_name" : "leemunroe",
      "indices" : [ 0, 10 ],
      "id_str" : "9445012",
      "id" : 9445012
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "359813239103684609",
  "geo" : {
  },
  "id_str" : "359889696689627139",
  "in_reply_to_user_id" : 9445012,
  "text" : "@leemunroe That site absolutely killed my iPad.",
  "id" : 359889696689627139,
  "in_reply_to_status_id" : 359813239103684609,
  "created_at" : "Wed Jul 24 04:16:04 +0000 2013",
  "in_reply_to_screen_name" : "leemunroe",
  "in_reply_to_user_id_str" : "9445012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NetDNA Status",
      "screen_name" : "NetDNAStatus",
      "indices" : [ 3, 16 ],
      "id_str" : "989258700",
      "id" : 989258700
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 95 ],
      "url" : "http://t.co/fpV3uww6S5",
      "expanded_url" : "http://goo.gl/j0gblw",
      "display_url" : "goo.gl/j0gblw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359888897095905280",
  "text" : "RT @NetDNAStatus: NetDNA Status Alert: Intermittent Control Panel Issues http://t.co/fpV3uww6S5",
  "retweeted_status" : {
    "source" : "<a href=\"http://status.netdna.com\" rel=\"nofollow\">NSTT</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 55, 77 ],
        "url" : "http://t.co/fpV3uww6S5",
        "expanded_url" : "http://goo.gl/j0gblw",
        "display_url" : "goo.gl/j0gblw"
      } ]
    },
    "geo" : {
    },
    "id_str" : "359840555573846016",
    "text" : "NetDNA Status Alert: Intermittent Control Panel Issues http://t.co/fpV3uww6S5",
    "id" : 359840555573846016,
    "created_at" : "Wed Jul 24 01:00:48 +0000 2013",
    "user" : {
      "name" : "NetDNA Status",
      "screen_name" : "NetDNAStatus",
      "protected" : false,
      "id_str" : "989258700",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2932139709/fb78677774685617675b04fc8f4b9702_normal.png",
      "id" : 989258700,
      "verified" : false
    }
  },
  "id" : 359888897095905280,
  "created_at" : "Wed Jul 24 04:12:54 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 3, 13 ],
      "id_str" : "175727560",
      "id" : 175727560
    }, {
      "name" : "David H.",
      "screen_name" : "alphenic",
      "indices" : [ 102, 111 ],
      "id_str" : "468853739",
      "id" : 468853739
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 101 ],
      "url" : "http://t.co/KFDZkQTnSO",
      "expanded_url" : "http://jsfiddle.net/bqEfz/show/",
      "display_url" : "jsfiddle.net/bqEfz/show/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359836989417205760",
  "text" : "RT @simevidas: 10 hidden HTML5 features that you should start using right now: http://t.co/KFDZkQTnSO\n@alphenic",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "David H.",
        "screen_name" : "alphenic",
        "indices" : [ 87, 96 ],
        "id_str" : "468853739",
        "id" : 468853739
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 86 ],
        "url" : "http://t.co/KFDZkQTnSO",
        "expanded_url" : "http://jsfiddle.net/bqEfz/show/",
        "display_url" : "jsfiddle.net/bqEfz/show/"
      } ]
    },
    "geo" : {
    },
    "id_str" : "359836759795843073",
    "text" : "10 hidden HTML5 features that you should start using right now: http://t.co/KFDZkQTnSO\n@alphenic",
    "id" : 359836759795843073,
    "created_at" : "Wed Jul 24 00:45:43 +0000 2013",
    "user" : {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "protected" : false,
      "id_str" : "175727560",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000065125916/326dca11327b0e650c17bb7f5722e393_normal.jpeg",
      "id" : 175727560,
      "verified" : false
    }
  },
  "id" : 359836989417205760,
  "created_at" : "Wed Jul 24 00:46:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/359798173075648512/photo/1",
      "indices" : [ 12, 34 ],
      "url" : "http://t.co/ETUGoNfJQ0",
      "media_url" : "http://pbs.twimg.com/media/BP5CiDlCMAEQH7-.jpg",
      "id_str" : "359798173079842817",
      "id" : 359798173079842817,
      "media_url_https" : "https://pbs.twimg.com/media/BP5CiDlCMAEQH7-.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 406,
        "resize" : "fit",
        "w" : 256
      }, {
        "h" : 406,
        "resize" : "fit",
        "w" : 256
      }, {
        "h" : 406,
        "resize" : "fit",
        "w" : 256
      }, {
        "h" : 406,
        "resize" : "fit",
        "w" : 256
      } ],
      "display_url" : "pic.twitter.com/ETUGoNfJQ0"
    } ],
    "hashtags" : [ {
      "text" : "jQuery",
      "indices" : [ 4, 11 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "359798173075648512",
  "text" : "LOL #jQuery http://t.co/ETUGoNfJQ0",
  "id" : 359798173075648512,
  "created_at" : "Tue Jul 23 22:12:23 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http://t.co/sHKzo5u2CA",
      "expanded_url" : "http://justcreative.com/2008/02/17/ultimate-list-of-blog-heading-templates-titles-for-blogging/",
      "display_url" : "justcreative.com/2008/02/17/ult\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359783393896042496",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Also noteworthy: http://t.co/sHKzo5u2CA",
  "id" : 359783393896042496,
  "created_at" : "Tue Jul 23 21:13:40 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 43 ],
      "url" : "http://t.co/xN90ehYMqo",
      "expanded_url" : "http://www.viralblog.com/buzz-wom/a-guide-to-creating-awesome-blog-titles/",
      "display_url" : "viralblog.com/buzz-wom/a-gui\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359783070074802177",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Check this: http://t.co/xN90ehYMqo",
  "id" : 359783070074802177,
  "created_at" : "Tue Jul 23 21:12:23 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "359781597303681026",
  "geo" : {
  },
  "id_str" : "359783012226969602",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 :) I thought top tens were a fad technique. But there is psychology in them. People know it won't take long to read.",
  "id" : 359783012226969602,
  "in_reply_to_status_id" : 359781597303681026,
  "created_at" : "Tue Jul 23 21:12:09 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "359780182413950977",
  "geo" : {
  },
  "id_str" : "359780969659641857",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 You should title your next blogpost using that approach. \"10 HTML5 facts you didn't know\" would probably get 10x more views ;)",
  "id" : 359780969659641857,
  "in_reply_to_status_id" : 359780182413950977,
  "created_at" : "Tue Jul 23 21:04:02 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Teece",
      "screen_name" : "doctorteece",
      "indices" : [ 0, 12 ],
      "id_str" : "1163980604",
      "id" : 1163980604
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 35 ],
      "url" : "http://t.co/eIfNVp2QoJ",
      "expanded_url" : "http://jasonmayes.com/projects/twitterApi/#sthash.ZZ377KkQ.dpbs",
      "display_url" : "jasonmayes.com/projects/twitt\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "359774416965603328",
  "geo" : {
  },
  "id_str" : "359779866389909504",
  "in_reply_to_user_id" : 1163980604,
  "text" : "@doctorteece http://t.co/eIfNVp2QoJ",
  "id" : 359779866389909504,
  "in_reply_to_status_id" : 359774416965603328,
  "created_at" : "Tue Jul 23 20:59:39 +0000 2013",
  "in_reply_to_screen_name" : "doctorteece",
  "in_reply_to_user_id_str" : "1163980604",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 50 ],
      "url" : "http://t.co/Bfg0FgGKVU",
      "expanded_url" : "http://www.livescience.com/38383-children-security-adversary-microsoft.html",
      "display_url" : "livescience.com/38383-children\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359765180390969344",
  "text" : "TL;DR Version of this post: http://t.co/Bfg0FgGKVU Your Children are the most evil form of Malware ever...",
  "id" : 359765180390969344,
  "created_at" : "Tue Jul 23 20:01:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "indices" : [ 3, 14 ],
      "id_str" : "517746290",
      "id" : 517746290
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http://t.co/Q77mqdUi0L",
      "expanded_url" : "http://blog.cloudflare.com/a-tour-inside-cloudflares-latest-generation-servers",
      "display_url" : "blog.cloudflare.com/a-tour-inside-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359756111219400704",
  "text" : "RT @PinPopular: A Tour Inside CloudFlare's Latest Generation Servers | CloudFlare Blog http://t.co/Q77mqdUi0L",
  "retweeted_status" : {
    "source" : "<a href=\"http://zhangchi.de/\" rel=\"nofollow\">Pinboard Popular</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 93 ],
        "url" : "http://t.co/Q77mqdUi0L",
        "expanded_url" : "http://blog.cloudflare.com/a-tour-inside-cloudflares-latest-generation-servers",
        "display_url" : "blog.cloudflare.com/a-tour-inside-\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "359743728442818560",
    "text" : "A Tour Inside CloudFlare's Latest Generation Servers | CloudFlare Blog http://t.co/Q77mqdUi0L",
    "id" : 359743728442818560,
    "created_at" : "Tue Jul 23 18:36:03 +0000 2013",
    "user" : {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "protected" : false,
      "id_str" : "517746290",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000170092903/61a9facf585dc87b3054e01059a11988_normal.png",
      "id" : 517746290,
      "verified" : false
    }
  },
  "id" : 359756111219400704,
  "created_at" : "Tue Jul 23 19:25:15 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 124 ],
      "url" : "http://t.co/rwADLcI5P4",
      "expanded_url" : "http://isharefil.es/QPsP",
      "display_url" : "isharefil.es/QPsP"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359737253796397057",
  "text" : "Yes, Bye, Aloha editor. Glad to see the end of your 81,000 lines of Javascript. What is this madness? http://t.co/rwADLcI5P4",
  "id" : 359737253796397057,
  "created_at" : "Tue Jul 23 18:10:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 126 ],
      "url" : "http://t.co/v5PcfHiNwP",
      "expanded_url" : "http://isharefil.es/QQ6k",
      "display_url" : "isharefil.es/QQ6k"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359735202454581250",
  "text" : "God damn you Aloha editor, stop interrupting my web development with your bulky 2-3MB Javascript files. http://t.co/v5PcfHiNwP Yuck!",
  "id" : 359735202454581250,
  "created_at" : "Tue Jul 23 18:02:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 46 ],
      "url" : "http://t.co/jtbGR3h10U",
      "expanded_url" : "http://www.brainpickings.org/index.php/2013/01/14/how-to-write-with-style-kurt-vonnegut/",
      "display_url" : "brainpickings.org/index.php/2013\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359722374335180800",
  "text" : "How to Write with Style http://t.co/jtbGR3h10U I agree with the \"sound like yourself\". Too many by-the-book almost robotic essays these days",
  "id" : 359722374335180800,
  "created_at" : "Tue Jul 23 17:11:12 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "want",
      "indices" : [ 117, 122 ]
    } ],
    "urls" : [ {
      "indices" : [ 28, 50 ],
      "url" : "http://t.co/DdsKyoqu9b",
      "expanded_url" : "http://vimeo.com/57142186",
      "display_url" : "vimeo.com/57142186"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359674923930750976",
  "text" : "Electromagnetic Harvester - http://t.co/DdsKyoqu9b Or in other words, a device that steals electricity from devices. #want",
  "id" : 359674923930750976,
  "created_at" : "Tue Jul 23 14:02:38 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mattur",
      "screen_name" : "mattur",
      "indices" : [ 3, 10 ],
      "id_str" : "17042795",
      "id" : 17042795
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "359672766452088835",
  "text" : "RT @mattur: \u2193 &lt;plaintext&gt; is out there. It can't be bargained with, nor reasoned with. It doesn't feel pity, or remorse, or fear. http://t.\u2026",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 124, 146 ],
        "url" : "http://t.co/rcTbJN8S95",
        "expanded_url" : "http://www.the-pope.com/plain.html",
        "display_url" : "the-pope.com/plain.html"
      } ]
    },
    "geo" : {
    },
    "id_str" : "359668050007568384",
    "text" : "\u2193 &lt;plaintext&gt; is out there. It can't be bargained with, nor reasoned with. It doesn't feel pity, or remorse, or fear. http://t.co/rcTbJN8S95",
    "id" : 359668050007568384,
    "created_at" : "Tue Jul 23 13:35:20 +0000 2013",
    "user" : {
      "name" : "mattur",
      "screen_name" : "mattur",
      "protected" : false,
      "id_str" : "17042795",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2670789076/a4dc81b82256a41d51077ed6bfe72d88_normal.jpeg",
      "id" : 17042795,
      "verified" : false
    }
  },
  "id" : 359672766452088835,
  "created_at" : "Tue Jul 23 13:54:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Schultz",
      "screen_name" : "wwahammy",
      "indices" : [ 0, 9 ],
      "id_str" : "19212550",
      "id" : 19212550
    }, {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 10, 18 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "358235425166462976",
  "geo" : {
  },
  "id_str" : "359506244807041024",
  "in_reply_to_user_id" : 19212550,
  "text" : "@wwahammy @codepo8 If the video file passes through my network, I can sniff for the location. No amount of obfuscated JS will conceal it",
  "id" : 359506244807041024,
  "in_reply_to_status_id" : 358235425166462976,
  "created_at" : "Tue Jul 23 02:52:22 +0000 2013",
  "in_reply_to_screen_name" : "wwahammy",
  "in_reply_to_user_id_str" : "19212550",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Isa Githubber",
      "screen_name" : "isagithubber",
      "indices" : [ 3, 16 ],
      "id_str" : "1614000547",
      "id" : 1614000547
    }, {
      "name" : "KANYE WEST",
      "screen_name" : "kanyewest",
      "indices" : [ 18, 28 ],
      "id_str" : "169686021",
      "id" : 169686021
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https://t.co/rhEXbSlYhM",
      "expanded_url" : "https://github.com/blog/kanye-west-joins-team",
      "display_url" : "github.com/blog/kanye-wes\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359494196786249729",
  "text" : "RT @isagithubber: @kanyewest is a githubber! https://t.co/rhEXbSlYhM",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "KANYE WEST",
        "screen_name" : "kanyewest",
        "indices" : [ 0, 10 ],
        "id_str" : "169686021",
        "id" : 169686021
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 27, 50 ],
        "url" : "https://t.co/rhEXbSlYhM",
        "expanded_url" : "https://github.com/blog/kanye-west-joins-team",
        "display_url" : "github.com/blog/kanye-wes\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "359472606891618304",
    "in_reply_to_user_id" : 169686021,
    "text" : "@kanyewest is a githubber! https://t.co/rhEXbSlYhM",
    "id" : 359472606891618304,
    "created_at" : "Tue Jul 23 00:38:42 +0000 2013",
    "in_reply_to_screen_name" : "kanyewest",
    "in_reply_to_user_id_str" : "169686021",
    "user" : {
      "name" : "Isa Githubber",
      "screen_name" : "isagithubber",
      "protected" : false,
      "id_str" : "1614000547",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000173315969/c6729b5aab28850cd18c54797f3fb9d9_normal.png",
      "id" : 1614000547,
      "verified" : false
    }
  },
  "id" : 359494196786249729,
  "created_at" : "Tue Jul 23 02:04:30 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 131 ],
      "url" : "http://t.co/jywCosszUz",
      "expanded_url" : "http://www.gsmsolutionsltd.com/phonevolts/pdf/SIM_Card_Clone_Guide.pdf",
      "display_url" : "gsmsolutionsltd.com/phonevolts/pdf\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359479235712270336",
  "text" : "Was reading about the SIM vulns. TIL the attack can be done remotely? If you have a SIM and want it cloned : http://t.co/jywCosszUz",
  "id" : 359479235712270336,
  "created_at" : "Tue Jul 23 01:05:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://itunes.apple.com/us/app/feedly/id396069556?mt=8&uo=4\" rel=\"nofollow\">feedly on iOS</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 115 ],
      "url" : "http://t.co/iTqcuYACMC",
      "expanded_url" : "http://feeds.arstechnica.com/~r/arstechnica/index/~3/k9MTP7XwKro/story01.htm",
      "display_url" : "feeds.arstechnica.com/~r/arstechnica\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359471195952922624",
  "text" : "Err, how about no, ARS technica. I've been cloning SIM cards for years now. Not hard to do?! http://t.co/iTqcuYACMC",
  "id" : 359471195952922624,
  "created_at" : "Tue Jul 23 00:33:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen \u2718",
      "screen_name" : "mikko",
      "indices" : [ 115, 121 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ovhack",
      "indices" : [ 103, 110 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https://t.co/QWw8GLZe8p",
      "expanded_url" : "https://contest.ovhack.com/",
      "display_url" : "contest.ovhack.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359463518258466816",
  "text" : "TIL - OVH do bug bounties ... https://t.co/QWw8GLZe8p Somebody just got a really good job at OVH today #ovhack cc/ @mikko",
  "id" : 359463518258466816,
  "created_at" : "Tue Jul 23 00:02:35 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "indices" : [ 3, 16 ],
      "id_str" : "154206772",
      "id" : 154206772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "359462164119044097",
  "text" : "RT @a_antonellis: I need Siri elocution lessons",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "359461830978043904",
    "text" : "I need Siri elocution lessons",
    "id" : 359461830978043904,
    "created_at" : "Mon Jul 22 23:55:53 +0000 2013",
    "user" : {
      "name" : "Anthony Antonellis",
      "screen_name" : "a_antonellis",
      "protected" : false,
      "id_str" : "154206772",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1904673216/cmy_normal.gif",
      "id" : 154206772,
      "verified" : false
    }
  },
  "id" : 359462164119044097,
  "created_at" : "Mon Jul 22 23:57:13 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 45 ],
      "url" : "http://t.co/NVTXln1peg",
      "expanded_url" : "http://isharefil.es/QNel",
      "display_url" : "isharefil.es/QNel"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359459722258153472",
  "text" : "Just got news of this: http://t.co/NVTXln1peg Big hack at OVH. Passwords were encrypted, but I don't care about that, They have all my DOX.",
  "id" : 359459722258153472,
  "created_at" : "Mon Jul 22 23:47:30 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 30 ],
      "url" : "http://t.co/dhdfr4hZZU",
      "expanded_url" : "http://www.nasa.gov/mission_pages/cassini/multimedia/pia17171.html#.Ue3AZdi9KSM",
      "display_url" : "nasa.gov/mission_pages/\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359455971753463810",
  "text" : "awesome http://t.co/dhdfr4hZZU",
  "id" : 359455971753463810,
  "created_at" : "Mon Jul 22 23:32:36 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Akala",
      "indices" : [ 96, 102 ]
    } ],
    "urls" : [ {
      "indices" : [ 39, 61 ],
      "url" : "http://t.co/HVCU0eIwAE",
      "expanded_url" : "http://www.youtube.com/watch?feature=related&v=acZSqeVc2Pw",
      "display_url" : "youtube.com/watch?feature=\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359417862407979009",
  "text" : "Could listen to this guy talk all day. http://t.co/HVCU0eIwAE Get educated and informed people! #Akala",
  "id" : 359417862407979009,
  "created_at" : "Mon Jul 22 21:01:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http://t.co/D4TaE22ne1",
      "expanded_url" : "http://www.mozart.io/",
      "display_url" : "mozart.io"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359416632650645505",
  "text" : "My new favorite phrase to put in the footers of websites: \"Not built with http://t.co/D4TaE22ne1\"",
  "id" : 359416632650645505,
  "created_at" : "Mon Jul 22 20:56:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/y5l3gLNgD6",
      "expanded_url" : "http://foaas.com/",
      "display_url" : "foaas.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359413325597122561",
  "text" : "http://t.co/y5l3gLNgD6",
  "id" : 359413325597122561,
  "created_at" : "Mon Jul 22 20:43:09 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 38 ],
      "url" : "https://t.co/Eiwo8YkyUY",
      "expanded_url" : "https://alpha.app.net/dh/post/7943314",
      "display_url" : "alpha.app.net/dh/post/7943314"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359401759053066242",
  "text" : "CSS Rant over: https://t.co/Eiwo8YkyUY",
  "id" : 359401759053066242,
  "created_at" : "Mon Jul 22 19:57:11 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "indices" : [ 3, 14 ],
      "id_str" : "517746290",
      "id" : 517746290
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 52 ],
      "url" : "http://t.co/e6D9OdGpJF",
      "expanded_url" : "http://mock.isssues.com/",
      "display_url" : "mock.isssues.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359367188131495936",
  "text" : "RT @PinPopular: Mock Response http://t.co/e6D9OdGpJF",
  "retweeted_status" : {
    "source" : "<a href=\"http://zhangchi.de/\" rel=\"nofollow\">Pinboard Popular</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 14, 36 ],
        "url" : "http://t.co/e6D9OdGpJF",
        "expanded_url" : "http://mock.isssues.com/",
        "display_url" : "mock.isssues.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "359366267750195200",
    "text" : "Mock Response http://t.co/e6D9OdGpJF",
    "id" : 359366267750195200,
    "created_at" : "Mon Jul 22 17:36:09 +0000 2013",
    "user" : {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "protected" : false,
      "id_str" : "517746290",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000170092903/61a9facf585dc87b3054e01059a11988_normal.png",
      "id" : 517746290,
      "verified" : false
    }
  },
  "id" : 359367188131495936,
  "created_at" : "Mon Jul 22 17:39:49 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 46 ],
      "url" : "http://t.co/1AcEgeyNBJ",
      "expanded_url" : "http://www.ribbonfarm.com/2013/07/10/you-are-not-an-artisan/",
      "display_url" : "ribbonfarm.com/2013/07/10/you\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359365413907341312",
  "text" : "You Are Not an Artisan: http://t.co/1AcEgeyNBJ Same with the web, Stop passing off websites as Art. Automation can do it better...",
  "id" : 359365413907341312,
  "created_at" : "Mon Jul 22 17:32:46 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 57 ],
      "url" : "http://t.co/swfEQqlBb4",
      "expanded_url" : "http://www.maxmasnick.com/2013/07/19/fastmail/",
      "display_url" : "maxmasnick.com/2013/07/19/fas\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "359362986156101632",
  "text" : "Switching from Gmail to FastMail - http://t.co/swfEQqlBb4 Good case made for making the switch. I might consider doing this.",
  "id" : 359362986156101632,
  "created_at" : "Mon Jul 22 17:23:07 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "359054879953649664",
  "geo" : {
  },
  "id_str" : "359054996265902080",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Affordable",
  "id" : 359054996265902080,
  "in_reply_to_status_id" : 359054879953649664,
  "created_at" : "Sun Jul 21 20:59:16 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "359053083130605568",
  "geo" : {
  },
  "id_str" : "359054478386794496",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Also, never use words like 'Don't' when you're marketing your services. Potential customers dont like seeing negative words ;)",
  "id" : 359054478386794496,
  "in_reply_to_status_id" : 359053083130605568,
  "created_at" : "Sun Jul 21 20:57:13 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "359050965665579009",
  "geo" : {
  },
  "id_str" : "359052321373691906",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn And no, my suggestion is not cheesy, its Perfect. Don't mess with it :p Only kidding, do what you want ;)",
  "id" : 359052321373691906,
  "in_reply_to_status_id" : 359050965665579009,
  "created_at" : "Sun Jul 21 20:48:38 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "359050965665579009",
  "geo" : {
  },
  "id_str" : "359051871949815809",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn No, the screenshots are noisy IMHO. Too much going on. Never liked screenshots of sites anyways. Prefer to interact with them...",
  "id" : 359051871949815809,
  "in_reply_to_status_id" : 359050965665579009,
  "created_at" : "Sun Jul 21 20:46:51 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "359048890789535744",
  "geo" : {
  },
  "id_str" : "359050593513381888",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn In place of the sentence next to the check mark say something like \"built with modern web standards\"",
  "id" : 359050593513381888,
  "in_reply_to_status_id" : 359048890789535744,
  "created_at" : "Sun Jul 21 20:41:46 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "359048890789535744",
  "geo" : {
  },
  "id_str" : "359050017069219842",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Try: \"Truely handcrafted web design\" You are offering a service, not a product.",
  "id" : 359050017069219842,
  "in_reply_to_status_id" : 359048890789535744,
  "created_at" : "Sun Jul 21 20:39:29 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "358987509067309056",
  "geo" : {
  },
  "id_str" : "359019038506102786",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Also, phone number is not enough. Need address, or even general location like country,. A CTA would be nice too. Like \"Call us\"",
  "id" : 359019038506102786,
  "in_reply_to_status_id" : 358987509067309056,
  "created_at" : "Sun Jul 21 18:36:23 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "358987509067309056",
  "geo" : {
  },
  "id_str" : "359018456420597762",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn IMHO - Photos shouldn't be there. They detract from clean design. \"Our solutions...\" phrase, etc is too tongue in cheek.",
  "id" : 359018456420597762,
  "in_reply_to_status_id" : 358987509067309056,
  "created_at" : "Sun Jul 21 18:34:04 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adewale Oshineye",
      "screen_name" : "ade_oshineye",
      "indices" : [ 3, 16 ],
      "id_str" : "23254994",
      "id" : 23254994
    }, {
      "name" : "Developer Experience",
      "screen_name" : "devexpftw",
      "indices" : [ 102, 112 ],
      "id_str" : "312218480",
      "id" : 312218480
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "devexp",
      "indices" : [ 90, 97 ]
    } ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https://t.co/yAwlmoQHrv",
      "expanded_url" : "https://plus.google.com/communities/100425122733176875656",
      "display_url" : "plus.google.com/communities/10\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358915725752549376",
  "text" : "RT @ade_oshineye: We've set up a Developer Experience Community : https://t.co/yAwlmoQHrv #devexp /cc @devexpftw",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Developer Experience",
        "screen_name" : "devexpftw",
        "indices" : [ 84, 94 ],
        "id_str" : "312218480",
        "id" : 312218480
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "devexp",
        "indices" : [ 72, 79 ]
      } ],
      "urls" : [ {
        "indices" : [ 48, 71 ],
        "url" : "https://t.co/yAwlmoQHrv",
        "expanded_url" : "https://plus.google.com/communities/100425122733176875656",
        "display_url" : "plus.google.com/communities/10\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "358892137217138690",
    "text" : "We've set up a Developer Experience Community : https://t.co/yAwlmoQHrv #devexp /cc @devexpftw",
    "id" : 358892137217138690,
    "created_at" : "Sun Jul 21 10:12:08 +0000 2013",
    "user" : {
      "name" : "Adewale Oshineye",
      "screen_name" : "ade_oshineye",
      "protected" : false,
      "id_str" : "23254994",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1114549111/n665811742_357292_2319_normal.jpg",
      "id" : 23254994,
      "verified" : false
    }
  },
  "id" : 358915725752549376,
  "created_at" : "Sun Jul 21 11:45:52 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    }, {
      "name" : "\u039Brif Khan",
      "screen_name" : "arifkhan7",
      "indices" : [ 4, 14 ],
      "id_str" : "71302070",
      "id" : 71302070
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "358745215470800897",
  "geo" : {
  },
  "id_str" : "358745663409897472",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys @arifkhan7 I digged around in my cache. I swear I'm not a stalker ;) I like to keep tabs on the people I look up to online.",
  "id" : 358745663409897472,
  "in_reply_to_status_id" : 358745215470800897,
  "created_at" : "Sun Jul 21 00:30:05 +0000 2013",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u039Brif Khan",
      "screen_name" : "arifkhan7",
      "indices" : [ 0, 10 ],
      "id_str" : "71302070",
      "id" : 71302070
    }, {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 11, 14 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/358741009779277825/photo/1",
      "indices" : [ 99, 121 ],
      "url" : "http://t.co/M9WIiGjQsa",
      "media_url" : "http://pbs.twimg.com/media/BPqBDDaCQAIB1aK.jpg",
      "id_str" : "358741009783472130",
      "id" : 358741009783472130,
      "media_url_https" : "https://pbs.twimg.com/media/BPqBDDaCQAIB1aK.jpg",
      "sizes" : [ {
        "h" : 256,
        "resize" : "fit",
        "w" : 256
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 256,
        "resize" : "fit",
        "w" : 256
      }, {
        "h" : 256,
        "resize" : "fit",
        "w" : 256
      }, {
        "h" : 256,
        "resize" : "fit",
        "w" : 256
      } ],
      "display_url" : "pic.twitter.com/M9WIiGjQsa"
    } ],
    "hashtags" : [ {
      "text" : "howtimeschange",
      "indices" : [ 83, 98 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "356060807810916354",
  "geo" : {
  },
  "id_str" : "358741009779277825",
  "in_reply_to_user_id" : 71302070,
  "text" : "@arifkhan7 @ys Wow, it's remarkably different than the older one you used to have: #howtimeschange http://t.co/M9WIiGjQsa",
  "id" : 358741009779277825,
  "in_reply_to_status_id" : 356060807810916354,
  "created_at" : "Sun Jul 21 00:11:36 +0000 2013",
  "in_reply_to_screen_name" : "arifkhan7",
  "in_reply_to_user_id_str" : "71302070",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Netflix US",
      "screen_name" : "netflix",
      "indices" : [ 110, 118 ],
      "id_str" : "16573941",
      "id" : 16573941
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "firstworldproblems",
      "indices" : [ 119, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "358724839474872320",
  "text" : "Oh no, I've ran out of disposable credit card numbers so I can no longer (re)signup for 30 day free trials of @netflix #firstworldproblems",
  "id" : 358724839474872320,
  "created_at" : "Sat Jul 20 23:07:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 14, 22 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "358514472534552577",
  "geo" : {
  },
  "id_str" : "358719455930482688",
  "in_reply_to_user_id" : 60870747,
  "text" : "@Dalt__Taylor @tompntn There I said it.",
  "id" : 358719455930482688,
  "in_reply_to_status_id" : 358514472534552577,
  "created_at" : "Sat Jul 20 22:45:57 +0000 2013",
  "in_reply_to_screen_name" : "ItsDalt_",
  "in_reply_to_user_id_str" : "60870747",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Philip Manavopoulos",
      "screen_name" : "manavo",
      "indices" : [ 0, 7 ],
      "id_str" : "32881834",
      "id" : 32881834
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https://t.co/zl2p6FxgM3",
      "expanded_url" : "https://alpha.app.net/dh/post/7885616",
      "display_url" : "alpha.app.net/dh/post/7885616"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358713812825812993",
  "in_reply_to_user_id" : 32881834,
  "text" : "@manavo Also, I think if you're buying a domain, buy the domain for the long haul, which for me is, at least five\u2026 https://t.co/zl2p6FxgM3",
  "id" : 358713812825812993,
  "created_at" : "Sat Jul 20 22:23:32 +0000 2013",
  "in_reply_to_screen_name" : "manavo",
  "in_reply_to_user_id_str" : "32881834",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Philip Manavopoulos",
      "screen_name" : "manavo",
      "indices" : [ 0, 7 ],
      "id_str" : "32881834",
      "id" : 32881834
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https://t.co/dHdnqKeV6C",
      "expanded_url" : "https://alpha.app.net/dh/post/7885590",
      "display_url" : "alpha.app.net/dh/post/7885590"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358713417525248001",
  "in_reply_to_user_id" : 32881834,
  "text" : "@manavo I agree. I am a 'domainer', and have been for years. That means I watch people buy domains, and then\u2026 https://t.co/dHdnqKeV6C",
  "id" : 358713417525248001,
  "created_at" : "Sat Jul 20 22:21:57 +0000 2013",
  "in_reply_to_screen_name" : "manavo",
  "in_reply_to_user_id_str" : "32881834",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "j l",
      "screen_name" : "jdl",
      "indices" : [ 0, 4 ],
      "id_str" : "6170572",
      "id" : 6170572
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https://t.co/j2kLGAomuq",
      "expanded_url" : "https://alpha.app.net/dh/post/7885557",
      "display_url" : "alpha.app.net/dh/post/7885557"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358712921989193728",
  "in_reply_to_user_id" : 6170572,
  "text" : "@jdl Nothing wrong with .COM. My only gripe with a .COM is, it translates to 'commercial', so if the site is\u2026 https://t.co/j2kLGAomuq",
  "id" : 358712921989193728,
  "created_at" : "Sat Jul 20 22:19:59 +0000 2013",
  "in_reply_to_screen_name" : "jdl",
  "in_reply_to_user_id_str" : "6170572",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randolph West",
      "screen_name" : "rabryst",
      "indices" : [ 0, 8 ],
      "id_str" : "20009233",
      "id" : 20009233
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https://t.co/daCticwqc6",
      "expanded_url" : "https://alpha.app.net/dh/post/7885216",
      "display_url" : "alpha.app.net/dh/post/7885216"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358709024503504896",
  "in_reply_to_user_id" : 20009233,
  "text" : "@rabryst Agreed. .CO TLDs usually have good quality content. My only problem is the price of .CO domains. Far too\u2026 https://t.co/daCticwqc6",
  "id" : 358709024503504896,
  "created_at" : "Sat Jul 20 22:04:30 +0000 2013",
  "in_reply_to_screen_name" : "rabryst",
  "in_reply_to_user_id_str" : "20009233",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https://t.co/RuCetEfkSx",
      "expanded_url" : "https://alpha.app.net/dh/post/7885132",
      "display_url" : "alpha.app.net/dh/post/7885132"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358708013412417536",
  "text" : "Whenever I see a .CO.UK domain, I know it's going to be good quality, and a fun experience. Why is this? Have I\u2026 https://t.co/RuCetEfkSx",
  "id" : 358708013412417536,
  "created_at" : "Sat Jul 20 22:00:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 9, 21 ],
      "id_str" : "15736190",
      "id" : 15736190
    }, {
      "name" : "Peter Nederlof",
      "screen_name" : "peterlof",
      "indices" : [ 125, 134 ],
      "id_str" : "90409884",
      "id" : 90409884
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DHTML",
      "indices" : [ 108, 114 ]
    } ],
    "urls" : [ {
      "indices" : [ 56, 78 ],
      "url" : "http://t.co/mqloi7PMLI",
      "expanded_url" : "http://peterned.home.xs4all.nl/",
      "display_url" : "peterned.home.xs4all.nl"
    } ]
  },
  "in_reply_to_status_id_str" : "358597764214566912",
  "geo" : {
  },
  "id_str" : "358704877041491969",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 @smashingmag Thankfuly we still have Peterned: http://t.co/mqloi7PMLI Can't believe it's still up. #DHTML 4eva! cc/ @peterlof",
  "id" : 358704877041491969,
  "in_reply_to_status_id" : 358597764214566912,
  "created_at" : "Sat Jul 20 21:48:01 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http://t.co/yYF28Gfv15",
      "expanded_url" : "http://f.cl.ly/items/1T3J2d012g1c0o402E28/google.servers.txt",
      "display_url" : "f.cl.ly/items/1T3J2d01\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358701485053329408",
  "text" : "Charge ur lazer at these: http://t.co/yYF28Gfv15",
  "id" : 358701485053329408,
  "created_at" : "Sat Jul 20 21:34:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http://t.co/qA7GHFPf8B",
      "expanded_url" : "http://isharefil.es/QMmc",
      "display_url" : "isharefil.es/QMmc"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358701224796766209",
  "text" : "Point your LOIC at these: http://t.co/qA7GHFPf8B",
  "id" : 358701224796766209,
  "created_at" : "Sat Jul 20 21:33:31 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 9, 21 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 77 ],
      "url" : "http://t.co/tuWvQKoBhs",
      "expanded_url" : "http://isharefil.es/QMQ9",
      "display_url" : "isharefil.es/QMQ9"
    } ]
  },
  "in_reply_to_status_id_str" : "358680280543920129",
  "geo" : {
  },
  "id_str" : "358698170143866881",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 @smashingmag Reminds me of this a while back: http://t.co/tuWvQKoBhs",
  "id" : 358698170143866881,
  "in_reply_to_status_id" : 358680280543920129,
  "created_at" : "Sat Jul 20 21:21:22 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sput",
      "screen_name" : "Sputn1k_",
      "indices" : [ 97, 106 ],
      "id_str" : "1578707072",
      "id" : 1578707072
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "358678943299158016",
  "text" : "None of this \"y3w g0t haxd by albani4 c3bir 4rmy\" stuff. Straight up, you dun goofed ... LOL via @Sputn1k_",
  "id" : 358678943299158016,
  "created_at" : "Sat Jul 20 20:04:58 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Butler",
      "screen_name" : "securitea",
      "indices" : [ 3, 13 ],
      "id_str" : "200535432",
      "id" : 200535432
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 43 ],
      "url" : "http://t.co/ODabt8oSxV",
      "expanded_url" : "http://ubuntuforums.org/",
      "display_url" : "ubuntuforums.org"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358678674238738432",
  "text" : "RT @securitea: Owned http://t.co/ODabt8oSxV",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 6, 28 ],
        "url" : "http://t.co/ODabt8oSxV",
        "expanded_url" : "http://ubuntuforums.org/",
        "display_url" : "ubuntuforums.org"
      } ]
    },
    "geo" : {
    },
    "id_str" : "358677827631063040",
    "text" : "Owned http://t.co/ODabt8oSxV",
    "id" : 358677827631063040,
    "created_at" : "Sat Jul 20 20:00:32 +0000 2013",
    "user" : {
      "name" : "Jon Butler",
      "screen_name" : "securitea",
      "protected" : false,
      "id_str" : "200535432",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000086174790/e6db16c86c1e1af1a210182b7a96683c_normal.jpeg",
      "id" : 200535432,
      "verified" : false
    }
  },
  "id" : 358678674238738432,
  "created_at" : "Sat Jul 20 20:03:54 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "indices" : [ 3, 14 ],
      "id_str" : "517746290",
      "id" : 517746290
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https://t.co/2wUxLcm2Rv",
      "expanded_url" : "https://cloudconvert.org/",
      "display_url" : "cloudconvert.org"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358649251816943618",
  "text" : "RT @PinPopular: CloudConvert - convert anything to anything https://t.co/2wUxLcm2Rv",
  "retweeted_status" : {
    "source" : "<a href=\"http://zhangchi.de/\" rel=\"nofollow\">Pinboard Popular</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 44, 67 ],
        "url" : "https://t.co/2wUxLcm2Rv",
        "expanded_url" : "https://cloudconvert.org/",
        "display_url" : "cloudconvert.org"
      } ]
    },
    "geo" : {
    },
    "id_str" : "358626388934262785",
    "text" : "CloudConvert - convert anything to anything https://t.co/2wUxLcm2Rv",
    "id" : 358626388934262785,
    "created_at" : "Sat Jul 20 16:36:08 +0000 2013",
    "user" : {
      "name" : "Pinboard Popular",
      "screen_name" : "PinPopular",
      "protected" : false,
      "id_str" : "517746290",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000170092903/61a9facf585dc87b3054e01059a11988_normal.png",
      "id" : 517746290,
      "verified" : false
    }
  },
  "id" : 358649251816943618,
  "created_at" : "Sat Jul 20 18:06:59 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/KfOOXzdtgX",
      "expanded_url" : "http://embedresponsively.com/",
      "display_url" : "embedresponsively.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358618902525251584",
  "text" : "http://t.co/KfOOXzdtgX",
  "id" : 358618902525251584,
  "created_at" : "Sat Jul 20 16:06:23 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Glenn Kelly",
      "screen_name" : "KelCeltic",
      "indices" : [ 0, 10 ],
      "id_str" : "581032441",
      "id" : 581032441
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "358609050704953346",
  "in_reply_to_user_id" : 581032441,
  "text" : "@KelCeltic You're some champ",
  "id" : 358609050704953346,
  "created_at" : "Sat Jul 20 15:27:15 +0000 2013",
  "in_reply_to_screen_name" : "KelCeltic",
  "in_reply_to_user_id_str" : "581032441",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 41 ],
      "url" : "http://t.co/5st8A85U7h",
      "expanded_url" : "http://shloosl.com/",
      "display_url" : "shloosl.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358438785987522565",
  "text" : "This is hilarious. http://t.co/5st8A85U7h Get a copy of your house key sent to your house via mail. Well played!",
  "id" : 358438785987522565,
  "created_at" : "Sat Jul 20 04:10:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "358430453037928449",
  "text" : "For a true Random Number Generator, a good fierce wiggle of the mouse is indeed viable. Big brother can't predict spastic muscle movement?!",
  "id" : 358430453037928449,
  "created_at" : "Sat Jul 20 03:37:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "intel",
      "indices" : [ 4, 10 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "358429173385138176",
  "text" : "The #intel + $NSA scenario could happen. I now generate all random input via my mouse. The more you move the mouse the more paranoid you are",
  "id" : 358429173385138176,
  "created_at" : "Sat Jul 20 03:32:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "indices" : [ 3, 12 ],
      "id_str" : "606610405",
      "id" : 606610405
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "frontend",
      "indices" : [ 47, 56 ]
    } ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http://t.co/zx507c5hv9",
      "expanded_url" : "http://myshar.es/WZZiBO",
      "display_url" : "myshar.es/WZZiBO"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358421870980251648",
  "text" : "RT @favehigg: Front End Development Guidelines #frontend  http://t.co/zx507c5hv9",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "frontend",
        "indices" : [ 33, 42 ]
      } ],
      "urls" : [ {
        "indices" : [ 44, 66 ],
        "url" : "http://t.co/zx507c5hv9",
        "expanded_url" : "http://myshar.es/WZZiBO",
        "display_url" : "myshar.es/WZZiBO"
      } ]
    },
    "geo" : {
    },
    "id_str" : "346733087910137857",
    "text" : "Front End Development Guidelines #frontend  http://t.co/zx507c5hv9",
    "id" : 346733087910137857,
    "created_at" : "Mon Jun 17 20:56:24 +0000 2013",
    "user" : {
      "name" : "Best of @_higg",
      "screen_name" : "favehigg",
      "protected" : false,
      "id_str" : "606610405",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000007208981/fee678a7c91e39839aa9ca27489b1b1f_normal.png",
      "id" : 606610405,
      "verified" : false
    }
  },
  "id" : 358421870980251648,
  "created_at" : "Sat Jul 20 03:03:27 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http://t.co/Nh4QPX7xaq",
      "expanded_url" : "http://myshar.es/1732Pqv",
      "display_url" : "myshar.es/1732Pqv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358273550588715008",
  "text" : "Writing Reusable AngularJS Components with Bower http://t.co/Nh4QPX7xaq",
  "id" : 358273550588715008,
  "created_at" : "Fri Jul 19 17:14:05 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 78 ],
      "url" : "http://t.co/Sb8NyAp51d",
      "expanded_url" : "http://myshar.es/14xcKRj",
      "display_url" : "myshar.es/14xcKRj"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358272032716226560",
  "in_reply_to_user_id" : 15759583,
  "text" : "@DavidWalshBlog shows how to use CSS generated content. http://t.co/Sb8NyAp51d",
  "id" : 358272032716226560,
  "created_at" : "Fri Jul 19 17:08:03 +0000 2013",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http://t.co/ea58yOnQXT",
      "expanded_url" : "http://myshar.es/16ey0Lv",
      "display_url" : "myshar.es/16ey0Lv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "358270634796662787",
  "text" : "React | A JavaScript library for building user interfaces http://t.co/ea58yOnQXT",
  "id" : 358270634796662787,
  "created_at" : "Fri Jul 19 17:02:30 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http://t.co/cUIX10R03z",
      "expanded_url" : "http://myshar.es/13HGOHp",
      "display_url" : "myshar.es/13HGOHp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "357911154175123458",
  "text" : "Glad to see the bower endpoints on the cujoJS download page! Excellent! http://t.co/cUIX10R03z",
  "id" : 357911154175123458,
  "created_at" : "Thu Jul 18 17:14:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex Walker",
      "screen_name" : "alexmwalker",
      "indices" : [ 0, 12 ],
      "id_str" : "5545292",
      "id" : 5545292
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 106 ],
      "url" : "http://t.co/lFbZiTXhLY",
      "expanded_url" : "http://myshar.es/11w7I24",
      "display_url" : "myshar.es/11w7I24"
    } ]
  },
  "geo" : {
  },
  "id_str" : "357909643835609089",
  "in_reply_to_user_id" : 5545292,
  "text" : "@alexmwalker shows how he built the functionng Pong game demo in just HTML and CSS. http://t.co/lFbZiTXhLY",
  "id" : 357909643835609089,
  "created_at" : "Thu Jul 18 17:08:03 +0000 2013",
  "in_reply_to_screen_name" : "alexmwalker",
  "in_reply_to_user_id_str" : "5545292",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 57 ],
      "url" : "http://t.co/NA1SVBqZM2",
      "expanded_url" : "http://myshar.es/11w8nAH",
      "display_url" : "myshar.es/11w8nAH"
    } ]
  },
  "geo" : {
  },
  "id_str" : "357908239884623872",
  "text" : "JavaScript Namespaces and Modules  http://t.co/NA1SVBqZM2",
  "id" : 357908239884623872,
  "created_at" : "Thu Jul 18 17:02:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 46 ],
      "url" : "http://t.co/IvbLi1x9tx",
      "expanded_url" : "http://firespotting.com/",
      "display_url" : "firespotting.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "357178144425385984",
  "text" : "Nice Hackernews clone \u2192 http://t.co/IvbLi1x9tx Only this one's for pitching ideas.",
  "id" : 357178144425385984,
  "created_at" : "Tue Jul 16 16:41:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https://t.co/UqKjFYcl6Q",
      "expanded_url" : "https://paste.sh/",
      "display_url" : "paste.sh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "357177938959007744",
  "text" : "https://t.co/UqKjFYcl6Q Encrypted pastebin!",
  "id" : 357177938959007744,
  "created_at" : "Tue Jul 16 16:40:31 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex Cornell",
      "screen_name" : "alexcornell",
      "indices" : [ 44, 56 ],
      "id_str" : "15528431",
      "id" : 15528431
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 99 ],
      "url" : "http://t.co/LVhCncyQOU",
      "expanded_url" : "http://everyfuckingwebsite.com/",
      "display_url" : "everyfuckingwebsite.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "357132630984114179",
  "text" : "In similar vein to Worst Portfolio Ever, by @alexcornell There is also this: http://t.co/LVhCncyQOU We need more parody sites like this!",
  "id" : 357132630984114179,
  "created_at" : "Tue Jul 16 13:40:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 27 ],
      "url" : "http://t.co/yjL70HmviW",
      "expanded_url" : "http://theworstportfolioever.com/",
      "display_url" : "theworstportfolioever.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "357131333253865473",
  "text" : "this http://t.co/yjL70HmviW",
  "id" : 357131333253865473,
  "created_at" : "Tue Jul 16 13:35:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 116 ],
      "url" : "http://t.co/RY7T9DUaQH",
      "expanded_url" : "http://heydonworks.com/auticons-icon-font/",
      "display_url" : "heydonworks.com/auticons-icon-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "357109234917322753",
  "text" : "Auticons is an icon font and CSS set that harnesses the awesome power of attribute selectors. http://t.co/RY7T9DUaQH A thousand times this!",
  "id" : 357109234917322753,
  "created_at" : "Tue Jul 16 12:07:31 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 57 ],
      "url" : "http://t.co/F4enkw3AR2",
      "expanded_url" : "http://monocle.io/",
      "display_url" : "monocle.io"
    } ]
  },
  "geo" : {
  },
  "id_str" : "356777002344263680",
  "text" : "Really digging Monocle right now \u2192 http://t.co/F4enkw3AR2",
  "id" : 356777002344263680,
  "created_at" : "Mon Jul 15 14:07:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 76 ],
      "url" : "http://t.co/j0HpiZQe8Q",
      "expanded_url" : "http://miniflux.net/",
      "display_url" : "miniflux.net"
    } ]
  },
  "geo" : {
  },
  "id_str" : "356462517075189761",
  "text" : "Miniflux is a minimalist and open source news reader. http://t.co/j0HpiZQe8Q Awesome! And PHP too, yay!",
  "id" : 356462517075189761,
  "created_at" : "Sun Jul 14 17:17:41 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 140 ],
      "url" : "http://t.co/Ua43kgmyQs",
      "expanded_url" : "http://chrissimpkins.github.io/tweetledee/index.html",
      "display_url" : "chrissimpkins.github.io/tweetledee/ind\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "356027316800995329",
  "text" : "Tweetledee. A PHP library that provides an incredibly easy way to access Twitter data formatted as JSON or a RSS feed http://t.co/Ua43kgmyQs",
  "id" : 356027316800995329,
  "created_at" : "Sat Jul 13 12:28:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 44 ],
      "url" : "http://t.co/HQwJgoX9Rc",
      "expanded_url" : "http://isharefil.es/QBfP/ascii.bunny.gif",
      "display_url" : "isharefil.es/QBfP/ascii.bun\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "356020937432379393",
  "text" : "Yay for ASCII Bunny \u2192 http://t.co/HQwJgoX9Rc",
  "id" : 356020937432379393,
  "created_at" : "Sat Jul 13 12:03:00 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https://t.co/xMK3flPO9E",
      "expanded_url" : "https://juiiicy.com/",
      "display_url" : "juiiicy.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "355775018682163201",
  "text" : "Dribbble part deux? https://t.co/xMK3flPO9E Juiiicy is a private community for designers to refer and/or receive work from each other.",
  "id" : 355775018682163201,
  "created_at" : "Fri Jul 12 19:45:49 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Sharknado",
      "indices" : [ 6, 16 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "355735708704571393",
  "text" : "Token #Sharknado tweet.",
  "id" : 355735708704571393,
  "created_at" : "Fri Jul 12 17:09:36 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 87 ],
      "url" : "http://t.co/c06b9c2UCy",
      "expanded_url" : "http://myshar.es/14x8Qrx",
      "display_url" : "myshar.es/14x8Qrx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "355735312888119296",
  "text" : "Promise &amp; Deferred Objects in JavaScript Pt.2: in Practice - http://t.co/c06b9c2UCy",
  "id" : 355735312888119296,
  "created_at" : "Fri Jul 12 17:08:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ink FilePicker",
      "screen_name" : "FilePicker",
      "indices" : [ 85, 96 ],
      "id_str" : "558092881",
      "id" : 558092881
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "JavaScript",
      "indices" : [ 34, 45 ]
    } ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http://t.co/g87aUi4GrT",
      "expanded_url" : "http://myshar.es/11w4uvw",
      "display_url" : "myshar.es/11w4uvw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "355733914981117953",
  "text" : "Codeblock.js | Editable, runnable #JavaScript code blocks http://t.co/g87aUi4GrT via @FilePicker",
  "id" : 355733914981117953,
  "created_at" : "Fri Jul 12 17:02:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kyle",
      "screen_name" : "getify",
      "indices" : [ 3, 10 ],
      "id_str" : "16686076",
      "id" : 16686076
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "355711009647898624",
  "text" : "RT @getify: cron-job to push a few trivial github commits per day, just to keep your activity graph green\u2026\u2026. as a service.",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "355684331764719620",
    "text" : "cron-job to push a few trivial github commits per day, just to keep your activity graph green\u2026\u2026. as a service.",
    "id" : 355684331764719620,
    "created_at" : "Fri Jul 12 13:45:27 +0000 2013",
    "user" : {
      "name" : "Kyle",
      "screen_name" : "getify",
      "protected" : false,
      "id_str" : "16686076",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1893021490/legojslogo_normal.png",
      "id" : 16686076,
      "verified" : false
    }
  },
  "id" : 355711009647898624,
  "created_at" : "Fri Jul 12 15:31:28 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http://t.co/QBgsekdXd0",
      "expanded_url" : "http://myshar.es/16exYDf",
      "display_url" : "myshar.es/16exYDf"
    } ]
  },
  "geo" : {
  },
  "id_str" : "355374434422489089",
  "text" : "Shadow DOM 301: Advanced Concepts &amp; DOM APIs http://t.co/QBgsekdXd0",
  "id" : 355374434422489089,
  "created_at" : "Thu Jul 11 17:14:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 123 ],
      "url" : "http://t.co/u6dpN27BKd",
      "expanded_url" : "http://adobe.ly/10u4xHV",
      "display_url" : "adobe.ly/10u4xHV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "355372931729203200",
  "text" : "Alan Stearns posts an example demonstrating how CSS Regions allow you to define where content flows. http://t.co/u6dpN27BKd",
  "id" : 355372931729203200,
  "created_at" : "Thu Jul 11 17:08:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 54 ],
      "url" : "http://t.co/o9zjo8ea4D",
      "expanded_url" : "http://myshar.es/14xcx0o",
      "display_url" : "myshar.es/14xcx0o"
    } ]
  },
  "geo" : {
  },
  "id_str" : "355371653582172160",
  "text" : "Uncovering the Native DOM API - http://t.co/o9zjo8ea4D",
  "id" : 355371653582172160,
  "created_at" : "Thu Jul 11 17:02:59 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 47 ],
      "url" : "http://t.co/9x6zHktcdn",
      "expanded_url" : "http://myshar.es/11a7an8",
      "display_url" : "myshar.es/11a7an8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "355010564264701954",
  "text" : "Mocky: Real HTTP mocking http://t.co/9x6zHktcdn",
  "id" : 355010564264701954,
  "created_at" : "Wed Jul 10 17:08:08 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Raymond Camden",
      "screen_name" : "cfjedimaster",
      "indices" : [ 0, 13 ],
      "id_str" : "7021362",
      "id" : 7021362
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 134 ],
      "url" : "http://t.co/mixL1Kt7QD",
      "expanded_url" : "http://myshar.es/11w2SSv",
      "display_url" : "myshar.es/11w2SSv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "355009210444029952",
  "in_reply_to_user_id" : 7021362,
  "text" : "@cfjedimaster demonstrates how to use progress event to display the status of a file transder in PhoneGap apps. http://t.co/mixL1Kt7QD",
  "id" : 355009210444029952,
  "created_at" : "Wed Jul 10 17:02:46 +0000 2013",
  "in_reply_to_screen_name" : "cfjedimaster",
  "in_reply_to_user_id_str" : "7021362",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wil Wheaton",
      "screen_name" : "wilw",
      "indices" : [ 0, 5 ],
      "id_str" : "1183041",
      "id" : 1183041
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 122 ],
      "url" : "http://t.co/QBNJbGwVwi",
      "expanded_url" : "http://is.gd/stats.php?url=wilwheaton",
      "display_url" : "is.gd/stats.php?url=\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "354740437665316865",
  "in_reply_to_user_id" : 1183041,
  "text" : "@wilw Interesting that you're using is.gd for your profile link. Did you know the stats are public? http://t.co/QBNJbGwVwi",
  "id" : 354740437665316865,
  "created_at" : "Tue Jul 09 23:14:45 +0000 2013",
  "in_reply_to_screen_name" : "wilw",
  "in_reply_to_user_id_str" : "1183041",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http://t.co/892cbxUKaQ",
      "expanded_url" : "http://home.comcast.net/~caetools/unicode/unicode.html",
      "display_url" : "home.comcast.net/~caetools/unic\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "354681831167234049",
  "text" : "Unicode Code Converter v6 http://t.co/892cbxUKaQ",
  "id" : 354681831167234049,
  "created_at" : "Tue Jul 09 19:21:52 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "UX",
      "indices" : [ 82, 85 ]
    } ],
    "urls" : [ {
      "indices" : [ 59, 81 ],
      "url" : "http://t.co/zxkWoYNRln",
      "expanded_url" : "http://www.robertlenne.com/requiredreading/",
      "display_url" : "robertlenne.com/requiredreadin\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "354591986998706176",
  "text" : "Required Reading for Product Designers. Interesting read. \nhttp://t.co/zxkWoYNRln #UX",
  "id" : 354591986998706176,
  "created_at" : "Tue Jul 09 13:24:52 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "infosec",
      "indices" : [ 113, 121 ]
    } ],
    "urls" : [ {
      "indices" : [ 15, 38 ],
      "url" : "https://t.co/fTCym78BcL",
      "expanded_url" : "https://immersion.media.mit.edu/",
      "display_url" : "immersion.media.mit.edu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "354591153879916544",
  "text" : "Tried out this https://t.co/fTCym78BcL Luckily I maintain a permanent inbox zero, and don't keep a contact list. #infosec",
  "id" : 354591153879916544,
  "created_at" : "Tue Jul 09 13:21:33 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 56 ],
      "url" : "http://t.co/qX5QzUoor2",
      "expanded_url" : "http://securityreactions.tumblr.com/post/54015782606/using-wireshark-with-no-filters",
      "display_url" : "securityreactions.tumblr.com/post/540157826\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "354238945267154944",
  "text" : "Using Wireshark With No Filters \u2192 http://t.co/qX5QzUoor2",
  "id" : 354238945267154944,
  "created_at" : "Mon Jul 08 14:02:00 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Outsider",
      "screen_name" : "Outsideris",
      "indices" : [ 47, 58 ],
      "id_str" : "7932892",
      "id" : 7932892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 42 ],
      "url" : "http://t.co/jk5A8hX28y",
      "expanded_url" : "http://sideeffect.kr/popularconvention/",
      "display_url" : "sideeffect.kr/popularconvent\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "354080915808464897",
  "text" : "Popular Conventions http://t.co/jk5A8hX28y via @outsideris Neat idea. Crowdsource coding conventions from Github commits!",
  "id" : 354080915808464897,
  "created_at" : "Mon Jul 08 03:34:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 17, 29 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 106 ],
      "url" : "http://t.co/KajcgWDVYT",
      "expanded_url" : "http://isharefil.es/Q5qT",
      "display_url" : "isharefil.es/Q5qT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353905791679463424",
  "text" : "So replying to a @smashingmag tweet gets you ~200 clicks. Must try that more often! http://t.co/KajcgWDVYT Win!",
  "id" : 353905791679463424,
  "created_at" : "Sun Jul 07 15:58:10 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 124 ],
      "url" : "http://t.co/dC1Vr5ei05",
      "expanded_url" : "http://pocket.co/sroHq",
      "display_url" : "pocket.co/sroHq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353904937660452865",
  "text" : "WPScanner - A collection of tools for checking wordpress installations to make the setup more secure. http://t.co/dC1Vr5ei05",
  "id" : 353904937660452865,
  "created_at" : "Sun Jul 07 15:54:47 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Path",
      "screen_name" : "path",
      "indices" : [ 27, 32 ],
      "id_str" : "106333951",
      "id" : 106333951
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SFO",
      "indices" : [ 77, 81 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 75 ],
      "url" : "http://t.co/OyNaMcmtO4",
      "expanded_url" : "http://isharefil.es/Q5sA",
      "display_url" : "isharefil.es/Q5sA"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353900964727631872",
  "text" : "This guy broke the news on @Path. Just weird is all. http://t.co/OyNaMcmtO4  #SFO",
  "id" : 353900964727631872,
  "created_at" : "Sun Jul 07 15:38:59 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "353591338404548610",
  "text" : "[Hello to all my friends and fans in domestic surveillance] niche, 51, H&amp;K, USP, ^, sardine, bank, EUB, USP, PCS, NRO, Red Cell, Glock 26",
  "id" : 353591338404548610,
  "created_at" : "Sat Jul 06 19:08:39 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 119 ],
      "url" : "http://t.co/iq2vvgZrF9",
      "expanded_url" : "http://attrition.org/misc/keywords.html",
      "display_url" : "attrition.org/misc/keywords.\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353591093079719936",
  "text" : "He he. Love these \"spook words\". Including this as hidden mime-text in every e-mail from now on: http://t.co/iq2vvgZrF9 argus, afsatcom, CQB",
  "id" : 353591093079719936,
  "created_at" : "Sat Jul 06 19:07:40 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 3, 17 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Twitter",
      "indices" : [ 124, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "353590346241933312",
  "text" : "RT @soaj1664ashar: How?\n\"If you type a word in Tweet or Reply that is same as your password, after you will click on Tweet, #Twitter conver\u2026",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Twitter",
        "indices" : [ 105, 113 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "353589643914125312",
    "text" : "How?\n\"If you type a word in Tweet or Reply that is same as your password, after you will click on Tweet, #Twitter converts it to **********\"",
    "id" : 353589643914125312,
    "created_at" : "Sat Jul 06 19:01:55 +0000 2013",
    "user" : {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "protected" : false,
      "id_str" : "277735240",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3466042152/c41edb1dfd837f875f26ce4beb6b9543_normal.jpeg",
      "id" : 277735240,
      "verified" : false
    }
  },
  "id" : 353590346241933312,
  "created_at" : "Sat Jul 06 19:04:42 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/353582771731566592/photo/1",
      "indices" : [ 23, 45 ],
      "url" : "http://t.co/OHcXvVG2QG",
      "media_url" : "http://pbs.twimg.com/media/BOgtqFQCEAA11NJ.jpg",
      "id_str" : "353582771735760896",
      "id" : 353582771735760896,
      "media_url_https" : "https://pbs.twimg.com/media/BOgtqFQCEAA11NJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 863,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 489,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 920,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 920,
        "resize" : "fit",
        "w" : 640
      } ],
      "display_url" : "pic.twitter.com/OHcXvVG2QG"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "353582771731566592",
  "text" : "Keep Calm and Hack On! http://t.co/OHcXvVG2QG",
  "id" : 353582771731566592,
  "created_at" : "Sat Jul 06 18:34:36 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "truestory",
      "indices" : [ 9, 19 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "353571883632955392",
  "text" : "\u2026 != ... #truestory",
  "id" : 353571883632955392,
  "created_at" : "Sat Jul 06 17:51:20 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 124 ],
      "url" : "http://t.co/pNQHmCws0p",
      "expanded_url" : "http://pocket.co/sSnNK",
      "display_url" : "pocket.co/sSnNK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353560588649103360",
  "text" : "How to get your tweets displaying on your website using JavaScript, without using new Twitter 1.1 API http://t.co/pNQHmCws0p",
  "id" : 353560588649103360,
  "created_at" : "Sat Jul 06 17:06:27 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amir Taaki",
      "screen_name" : "AmirTaaki",
      "indices" : [ 53, 63 ],
      "id_str" : "385023039",
      "id" : 385023039
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "B1tc01n",
      "indices" : [ 64, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https://t.co/Oqp0kbm04U",
      "expanded_url" : "https://vimeo.com/64913466",
      "display_url" : "vimeo.com/64913466"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353555018391097345",
  "text" : "Hack/Hackers conference https://t.co/Oqp0kbm04U with @AmirTaaki #B1tc01n",
  "id" : 353555018391097345,
  "created_at" : "Sat Jul 06 16:44:19 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http://twitter.com/alphenic/status/353544223141814272/photo/1",
      "indices" : [ 34, 56 ],
      "url" : "http://t.co/E1dzbj6vwy",
      "media_url" : "http://pbs.twimg.com/media/BOgKmQlCYAAkhBf.jpg",
      "id_str" : "353544223150202880",
      "id" : 353544223150202880,
      "media_url_https" : "https://pbs.twimg.com/media/BOgKmQlCYAAkhBf.jpg",
      "sizes" : [ {
        "h" : 742,
        "resize" : "fit",
        "w" : 525
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 742,
        "resize" : "fit",
        "w" : 525
      }, {
        "h" : 742,
        "resize" : "fit",
        "w" : 525
      }, {
        "h" : 481,
        "resize" : "fit",
        "w" : 340
      } ],
      "display_url" : "pic.twitter.com/E1dzbj6vwy"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "353544223141814272",
  "text" : "Homeless city guide. Clever idea. http://t.co/E1dzbj6vwy",
  "id" : 353544223141814272,
  "created_at" : "Sat Jul 06 16:01:26 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 0, 12 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 35 ],
      "url" : "http://t.co/j5YxgRa9Q2",
      "expanded_url" : "http://iwantaneff.in/textarea/",
      "display_url" : "iwantaneff.in/textarea/"
    } ]
  },
  "in_reply_to_status_id_str" : "353526631433895936",
  "geo" : {
  },
  "id_str" : "353530953341222915",
  "in_reply_to_user_id" : 15736190,
  "text" : "@smashingmag http://t.co/j5YxgRa9Q2 \u2665",
  "id" : 353530953341222915,
  "in_reply_to_status_id" : 353526631433895936,
  "created_at" : "Sat Jul 06 15:08:42 +0000 2013",
  "in_reply_to_screen_name" : "smashingmag",
  "in_reply_to_user_id_str" : "15736190",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Nitty Gritty",
      "screen_name" : "_thenittygritty",
      "indices" : [ 3, 19 ],
      "id_str" : "838416517",
      "id" : 838416517
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http://t.co/SK3YBdCEDj",
      "expanded_url" : "http://thenittygritty.co/feed",
      "display_url" : "thenittygritty.co/feed"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353530762747838464",
  "text" : "RT @_thenittygritty: Did you switch your RSS reader already? Make sure TNG is in it ;) http://t.co/SK3YBdCEDj",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 66, 88 ],
        "url" : "http://t.co/SK3YBdCEDj",
        "expanded_url" : "http://thenittygritty.co/feed",
        "display_url" : "thenittygritty.co/feed"
      } ]
    },
    "geo" : {
    },
    "id_str" : "353529855301791745",
    "text" : "Did you switch your RSS reader already? Make sure TNG is in it ;) http://t.co/SK3YBdCEDj",
    "id" : 353529855301791745,
    "created_at" : "Sat Jul 06 15:04:20 +0000 2013",
    "user" : {
      "name" : "The Nitty Gritty",
      "screen_name" : "_thenittygritty",
      "protected" : false,
      "id_str" : "838416517",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2903705538/5a0d2a1feafdd3d15c9aef862bb56f50_normal.png",
      "id" : 838416517,
      "verified" : false
    }
  },
  "id" : 353530762747838464,
  "created_at" : "Sat Jul 06 15:07:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "353526203883339776",
  "geo" : {
  },
  "id_str" : "353526905552650240",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas :p A bit tongue in cheek there. I always find it weird that nobody retweets my linkdumps. Probably too much info to assimilate.",
  "id" : 353526905552650240,
  "in_reply_to_status_id" : 353526203883339776,
  "created_at" : "Sat Jul 06 14:52:37 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 121, 131 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 116 ],
      "url" : "http://t.co/X4Kq8dQHpJ",
      "expanded_url" : "http://effinroot.eiremedia.netdna-cdn.com/shared/html/links.html",
      "display_url" : "effinroot.eiremedia.netdna-cdn.com/shared/html/li\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353520355509403648",
  "text" : "Meanwhile, if the links are all colored blue, I think that is doubly awful and _not_ awesome: http://t.co/X4Kq8dQHpJ cc/ @simevidas",
  "id" : 353520355509403648,
  "created_at" : "Sat Jul 06 14:26:35 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 39 ],
      "url" : "http://t.co/X4Kq8dQHpJ",
      "expanded_url" : "http://effinroot.eiremedia.netdna-cdn.com/shared/html/links.html",
      "display_url" : "effinroot.eiremedia.netdna-cdn.com/shared/html/li\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353519959705522177",
  "text" : "Gonna bump this: http://t.co/X4Kq8dQHpJ If most of the links are colored purple, I both think that is shameful and awesome at the same time",
  "id" : 353519959705522177,
  "created_at" : "Sat Jul 06 14:25:01 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "indices" : [ 3, 13 ],
      "id_str" : "605929797",
      "id" : 605929797
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http://t.co/Rz9QMs6mDN",
      "expanded_url" : "http://www.theverge.com/2013/7/5/4496852/adblock-plus-eye-google-whitelist",
      "display_url" : "theverge.com/2013/7/5/44968\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353307280428630017",
  "text" : "RT @lunchduty: \"Google reportedly paid Adblock Plus not to block its ads\" http://t.co/Rz9QMs6mDN",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/feedly/id396069556?mt=8&uo=4\" rel=\"nofollow\">feedly on iOS</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 81 ],
        "url" : "http://t.co/Rz9QMs6mDN",
        "expanded_url" : "http://www.theverge.com/2013/7/5/4496852/adblock-plus-eye-google-whitelist",
        "display_url" : "theverge.com/2013/7/5/44968\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "353307183422775301",
    "text" : "\"Google reportedly paid Adblock Plus not to block its ads\" http://t.co/Rz9QMs6mDN",
    "id" : 353307183422775301,
    "created_at" : "Sat Jul 06 00:19:31 +0000 2013",
    "user" : {
      "name" : "Lunch Duty",
      "screen_name" : "lunchduty",
      "protected" : false,
      "id_str" : "605929797",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3566749477/5025656cb423825e1d0748cf13c87ca3_normal.png",
      "id" : 605929797,
      "verified" : false
    }
  },
  "id" : 353307280428630017,
  "created_at" : "Sat Jul 06 00:19:54 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 134 ],
      "url" : "http://t.co/QfV8P2gYl1",
      "expanded_url" : "http://myshar.es/11w2SC0",
      "display_url" : "myshar.es/11w2SC0"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353200118633209856",
  "text" : "Dan Riti discusses TBone, a Backbone extension to remove the complexity of manually managing data dependencies. http://t.co/QfV8P2gYl1",
  "id" : 353200118633209856,
  "created_at" : "Fri Jul 05 17:14:05 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 138 ],
      "url" : "http://t.co/pZsu6j2G74",
      "expanded_url" : "http://myshar.es/11w2Rhu",
      "display_url" : "myshar.es/11w2Rhu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353198614685818882",
  "text" : "JSON Editor Online is a web-based tool to view, edit and format JSON using a side-by-side treeview and code editor. http://t.co/pZsu6j2G74",
  "id" : 353198614685818882,
  "created_at" : "Fri Jul 05 17:08:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Burke H\u272Alland",
      "screen_name" : "burkeholland",
      "indices" : [ 64, 77 ],
      "id_str" : "14679272",
      "id" : 14679272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 101 ],
      "url" : "http://t.co/OL9CD9ADWm",
      "expanded_url" : "http://myshar.es/19r4AO5",
      "display_url" : "myshar.es/19r4AO5"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353197165813829633",
  "text" : "Excellent and thorough walkthrough of RequireJS fundamentals by @burkeholland. http://t.co/OL9CD9ADWm",
  "id" : 353197165813829633,
  "created_at" : "Fri Jul 05 17:02:21 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/ZSrBNoTlUb",
      "expanded_url" : "http://pocket.co/sSu1A",
      "display_url" : "pocket.co/sSu1A"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353169298992267265",
  "text" : "People are lazy. Here\u2019s how I get shit done. http://t.co/ZSrBNoTlUb",
  "id" : 353169298992267265,
  "created_at" : "Fri Jul 05 15:11:37 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chad Whitacre",
      "screen_name" : "whit537",
      "indices" : [ 3, 11 ],
      "id_str" : "34175404",
      "id" : 34175404
    }, {
      "name" : "David Cramer",
      "screen_name" : "zeeg",
      "indices" : [ 20, 25 ],
      "id_str" : "15732699",
      "id" : 15732699
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https://t.co/GXmEhPfcxC",
      "expanded_url" : "https://speakerdeck.com/zeeg/open-source-as-a-business-europython-2013?slide=19",
      "display_url" : "speakerdeck.com/zeeg/open-sour\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353167904029671424",
  "text" : "RT @whit537: Oh man @zeeg, I feel you big time here: https://t.co/GXmEhPfcxC.",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "David Cramer",
        "screen_name" : "zeeg",
        "indices" : [ 7, 12 ],
        "id_str" : "15732699",
        "id" : 15732699
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 40, 63 ],
        "url" : "https://t.co/GXmEhPfcxC",
        "expanded_url" : "https://speakerdeck.com/zeeg/open-source-as-a-business-europython-2013?slide=19",
        "display_url" : "speakerdeck.com/zeeg/open-sour\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "353166426460274690",
    "text" : "Oh man @zeeg, I feel you big time here: https://t.co/GXmEhPfcxC.",
    "id" : 353166426460274690,
    "created_at" : "Fri Jul 05 15:00:12 +0000 2013",
    "user" : {
      "name" : "Chad Whitacre",
      "screen_name" : "whit537",
      "protected" : false,
      "id_str" : "34175404",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3162068031/c6ac89ed6a1de9ddaeb90f394143f5f7_normal.jpeg",
      "id" : 34175404,
      "verified" : false
    }
  },
  "id" : 353167904029671424,
  "created_at" : "Fri Jul 05 15:06:04 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anibal Dami\u00E3o",
      "screen_name" : "damiansen",
      "indices" : [ 0, 10 ],
      "id_str" : "17755595",
      "id" : 17755595
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "353158404203757569",
  "in_reply_to_user_id" : 17755595,
  "text" : "@damiansen You have mail \u2665",
  "id" : 353158404203757569,
  "created_at" : "Fri Jul 05 14:28:19 +0000 2013",
  "in_reply_to_screen_name" : "damiansen",
  "in_reply_to_user_id_str" : "17755595",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 63 ],
      "url" : "http://t.co/GX8Ke1J1zr",
      "expanded_url" : "http://pocket.co/sSKD7",
      "display_url" : "pocket.co/sSKD7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353136301769375745",
  "text" : "Moot \u2192 Forums and commenting re-imagined http://t.co/GX8Ke1J1zr",
  "id" : 353136301769375745,
  "created_at" : "Fri Jul 05 13:00:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pocket",
      "screen_name" : "Pocket",
      "indices" : [ 17, 24 ],
      "id_str" : "27530178",
      "id" : 27530178
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http://t.co/oTWIvK5QXq",
      "expanded_url" : "http://pocket.co/sSKDc",
      "display_url" : "pocket.co/sSKDc"
    } ]
  },
  "geo" : {
  },
  "id_str" : "353136124400635905",
  "text" : "Promises/A+ (via @Pocket) http://t.co/oTWIvK5QXq",
  "id" : 353136124400635905,
  "created_at" : "Fri Jul 05 12:59:47 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AdPacks.com",
      "screen_name" : "adpacks",
      "indices" : [ 0, 8 ],
      "id_str" : "186182105",
      "id" : 186182105
    }, {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 9, 17 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "353110242390835201",
  "geo" : {
  },
  "id_str" : "353135011223961602",
  "in_reply_to_user_id" : 186182105,
  "text" : "@adpacks @codepo8 *nudges*",
  "id" : 353135011223961602,
  "in_reply_to_status_id" : 353110242390835201,
  "created_at" : "Fri Jul 05 12:55:22 +0000 2013",
  "in_reply_to_screen_name" : "adpacks",
  "in_reply_to_user_id_str" : "186182105",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http://t.co/BlSWyPLpHc",
      "expanded_url" : "http://alexwolfe.github.io/Buttons/",
      "display_url" : "alexwolfe.github.io/Buttons/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352912818359640065",
  "text" : "Buttons - a customizable CSS button library built with Sass &amp; Compass http://t.co/BlSWyPLpHc",
  "id" : 352912818359640065,
  "created_at" : "Thu Jul 04 22:12:27 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https://t.co/8N1dvp3m7p",
      "expanded_url" : "https://docs.google.com/forms/d/1Hc6r-RQaF1VLvn5i0-32fZVn8_IKl6uOnQkIxuLjmNk/viewform",
      "display_url" : "docs.google.com/forms/d/1Hc6r-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352907673559244801",
  "text" : "Go forth, my fellow spacemonkey, you will be rewarded in this life. Or in other words. Become a peerCDN beta tester! https://t.co/8N1dvp3m7p",
  "id" : 352907673559244801,
  "created_at" : "Thu Jul 04 21:52:00 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "peerCDN",
      "screen_name" : "peerCDN",
      "indices" : [ 3, 11 ],
      "id_str" : "1229242418",
      "id" : 1229242418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http://t.co/LwjBtk3Sc9",
      "expanded_url" : "http://peercdn.com/beta",
      "display_url" : "peercdn.com/beta"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352907216250077184",
  "text" : "RT @PeerCDN: It's time! We're looking for beta testers to try out peerCDN. Apply here: http://t.co/LwjBtk3Sc9",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 96 ],
        "url" : "http://t.co/LwjBtk3Sc9",
        "expanded_url" : "http://peercdn.com/beta",
        "display_url" : "peercdn.com/beta"
      } ]
    },
    "geo" : {
    },
    "id_str" : "351873869826633728",
    "text" : "It's time! We're looking for beta testers to try out peerCDN. Apply here: http://t.co/LwjBtk3Sc9",
    "id" : 351873869826633728,
    "created_at" : "Tue Jul 02 01:24:02 +0000 2013",
    "user" : {
      "name" : "peerCDN",
      "screen_name" : "peerCDN",
      "protected" : false,
      "id_str" : "1229242418",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3440721416/b5d21dcc336885578196c232041f7b7b_normal.png",
      "id" : 1229242418,
      "verified" : false
    }
  },
  "id" : 352907216250077184,
  "created_at" : "Thu Jul 04 21:50:11 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "indices" : [ 3, 9 ],
      "id_str" : "2367111",
      "id" : 2367111
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http://t.co/eEWD5nb7ea",
      "expanded_url" : "http://www.mademyday.de/css-height-equals-width-with-pure-css.html?utm_source=html5weekly&utm_medium=email",
      "display_url" : "mademyday.de/css-height-equ\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352900564645969920",
  "text" : "RT @assaf: Height equals width with pure CSS http://t.co/eEWD5nb7ea",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.apple.com\" rel=\"nofollow\">iOS</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 34, 56 ],
        "url" : "http://t.co/eEWD5nb7ea",
        "expanded_url" : "http://www.mademyday.de/css-height-equals-width-with-pure-css.html?utm_source=html5weekly&utm_medium=email",
        "display_url" : "mademyday.de/css-height-equ\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "352899925895421953",
    "text" : "Height equals width with pure CSS http://t.co/eEWD5nb7ea",
    "id" : 352899925895421953,
    "created_at" : "Thu Jul 04 21:21:13 +0000 2013",
    "user" : {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "protected" : false,
      "id_str" : "2367111",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000045323076/8355e0bb4b3b27a90ee35497cf8c34e5_normal.jpeg",
      "id" : 2367111,
      "verified" : false
    }
  },
  "id" : 352900564645969920,
  "created_at" : "Thu Jul 04 21:23:45 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 116 ],
      "url" : "http://t.co/BzuUJHiI0R",
      "expanded_url" : "http://myshar.es/13DlMJK",
      "display_url" : "myshar.es/13DlMJK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352836226249797634",
  "text" : "Automatically download &amp; use FrontEnd JS dependencies with Bower + Grunt boilerplate free http://t.co/BzuUJHiI0R",
  "id" : 352836226249797634,
  "created_at" : "Thu Jul 04 17:08:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 61 ],
      "url" : "http://t.co/hytPbpd7E2",
      "expanded_url" : "http://myshar.es/13DlMto",
      "display_url" : "myshar.es/13DlMto"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352834805546758146",
  "text" : "Understanding Monads With JavaScript - http://t.co/hytPbpd7E2",
  "id" : 352834805546758146,
  "created_at" : "Thu Jul 04 17:02:27 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 22 ],
      "url" : "http://t.co/ZVV0aknypg",
      "expanded_url" : "http://unicodeheart.com/",
      "display_url" : "unicodeheart.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352824800139411456",
  "text" : "http://t.co/ZVV0aknypg",
  "id" : 352824800139411456,
  "created_at" : "Thu Jul 04 16:22:42 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "InfluAds",
      "screen_name" : "influads",
      "indices" : [ 28, 37 ],
      "id_str" : "62625073",
      "id" : 62625073
    }, {
      "name" : "AdPacks.com",
      "screen_name" : "adpacks",
      "indices" : [ 41, 49 ],
      "id_str" : "186182105",
      "id" : 186182105
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "352533498139127809",
  "geo" : {
  },
  "id_str" : "352819588406329344",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 You should go with @influads or @adpacks . They pay out much higher rates, and are tailored towards high traffic dev blogs.",
  "id" : 352819588406329344,
  "in_reply_to_status_id" : 352533498139127809,
  "created_at" : "Thu Jul 04 16:01:59 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 60 ],
      "url" : "http://t.co/d9rKFsJAH4",
      "expanded_url" : "http://myshar.es/13DlMts",
      "display_url" : "myshar.es/13DlMts"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352475331099238401",
  "text" : "Dojo Monk: Dojo for jQuery Developers http://t.co/d9rKFsJAH4",
  "id" : 352475331099238401,
  "created_at" : "Wed Jul 03 17:14:02 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nettuts+",
      "screen_name" : "nettuts",
      "indices" : [ 0, 8 ],
      "id_str" : "14490962",
      "id" : 14490962
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 122, 144 ],
      "url" : "http://t.co/oNFHDVJU9O",
      "expanded_url" : "http://myshar.es/1bwzcua",
      "display_url" : "myshar.es/1bwzcua"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352473931615178752",
  "in_reply_to_user_id" : 14490962,
  "text" : "@nettuts: Check out the new site for JsViews, JsRender &amp;amp; JsObservable which replaced the jQuery Templates plugin. http://t.co/oNFHDVJU9O",
  "id" : 352473931615178752,
  "created_at" : "Wed Jul 03 17:08:28 +0000 2013",
  "in_reply_to_screen_name" : "nettuts",
  "in_reply_to_user_id_str" : "14490962",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 82 ],
      "url" : "http://t.co/aYVMKqc3vv",
      "expanded_url" : "http://myshar.es/1732UdN",
      "display_url" : "myshar.es/1732UdN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352472450619019265",
  "text" : "Check out Chardin.js, simple overlay instructions for apps, http://t.co/aYVMKqc3vv",
  "id" : 352472450619019265,
  "created_at" : "Wed Jul 03 17:02:35 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 63 ],
      "url" : "http://t.co/wc7olIMbfi",
      "expanded_url" : "http://brettterpstra.com/2013/07/03/saving-batches-of-links-from-web-pages/",
      "display_url" : "brettterpstra.com/2013/07/03/sav\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352459711456624640",
  "text" : "Saving batches of links from web pages \u2192 http://t.co/wc7olIMbfi Now that's awesome!",
  "id" : 352459711456624640,
  "created_at" : "Wed Jul 03 16:11:58 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https://t.co/LTLPBbzHsC",
      "expanded_url" : "https://www.potluck.it/",
      "display_url" : "potluck.it"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352459287311810560",
  "text" : "Trying out the Potluck thing. https://t.co/LTLPBbzHsC So far, so good.",
  "id" : 352459287311810560,
  "created_at" : "Wed Jul 03 16:10:17 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https://t.co/zr3LNI50OC",
      "expanded_url" : "https://twitter.com/alphenic/status/340216737838551040",
      "display_url" : "twitter.com/alphenic/statu\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352453805562011649",
  "text" : "Here's me posting about $NSA leaks before Snowden even exposes them in The Guardian. https://t.co/zr3LNI50OC",
  "id" : 352453805562011649,
  "created_at" : "Wed Jul 03 15:48:30 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cute",
      "indices" : [ 47, 52 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 46 ],
      "url" : "http://t.co/5Ft55PgjRy",
      "expanded_url" : "http://bjankord.github.io/Style-Guide-Boilerplate/",
      "display_url" : "bjankord.github.io/Style-Guide-Bo\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352247760508030977",
  "text" : "Style Guide Boilerplate http://t.co/5Ft55PgjRy #cute",
  "id" : 352247760508030977,
  "created_at" : "Wed Jul 03 02:09:45 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 2, 10 ],
      "id_str" : "496946720",
      "id" : 496946720
    }, {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 60, 68 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 47 ],
      "url" : "http://t.co/bL1twfLNeK",
      "expanded_url" : "http://isharefil.es/Q2d7",
      "display_url" : "isharefil.es/Q2d7"
    } ]
  },
  "in_reply_to_status_id_str" : "352184942492524545",
  "geo" : {
  },
  "id_str" : "352200567604523009",
  "in_reply_to_user_id" : 496946720,
  "text" : "\u2024 @tompntn Here you go \u2192 http://t.co/bL1twfLNeK Courtesy of @codepo8",
  "id" : 352200567604523009,
  "in_reply_to_status_id" : 352184942492524545,
  "created_at" : "Tue Jul 02 23:02:13 +0000 2013",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http://t.co/p6k0xvr4cK",
      "expanded_url" : "http://www.kaitenbrowser.com/",
      "display_url" : "kaitenbrowser.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352138359256268800",
  "text" : "Kaiten is a jQuery plug-in which offers a new navigation model for web applications  \u2192 http://t.co/p6k0xvr4cK Nice Idea ;)",
  "id" : 352138359256268800,
  "created_at" : "Tue Jul 02 18:55:01 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DRM",
      "indices" : [ 103, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 38, 60 ],
      "url" : "http://t.co/NUeOlDgVh2",
      "expanded_url" : "http://torrentfreak.com/new-drm-changes-text-of-ebooks-to-catch-pirates-130616/",
      "display_url" : "torrentfreak.com/new-drm-change\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352137494340452353",
  "text" : "This is a very clever DRM technique \u2192 http://t.co/NUeOlDgVh2 But luckily we have \uFF35\uFF2E\uFF29\uFF23\uFF2F\uFF24\uFF25 to defeat it. #DRM",
  "id" : 352137494340452353,
  "created_at" : "Tue Jul 02 18:51:35 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 52 ],
      "url" : "https://t.co/RDTFbHP50y",
      "expanded_url" : "https://gist.github.com/endolith/157796",
      "display_url" : "gist.github.com/endolith/157796"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352137055574310913",
  "text" : "\u2588\u2584\u2584 \u2588\u2588\u2588 \u2588\u2584\u2584 \u2588\u2584\u2588\u2584\u2588 \u2588\u2584\u2588 \u2580\u2588\u2580  \n\nhttps://t.co/RDTFbHP50y",
  "id" : 352137055574310913,
  "created_at" : "Tue Jul 02 18:49:51 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 54 ],
      "url" : "http://t.co/9K4cFkQD7N",
      "expanded_url" : "http://isharefil.es/Q0Em",
      "display_url" : "isharefil.es/Q0Em"
    } ]
  },
  "in_reply_to_status_id_str" : "352123372659945473",
  "geo" : {
  },
  "id_str" : "352125671415361536",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Oh, I see it everyday. http://t.co/9K4cFkQD7N ;)",
  "id" : 352125671415361536,
  "in_reply_to_status_id" : 352123372659945473,
  "created_at" : "Tue Jul 02 18:04:36 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 26, 34 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http://t.co/jOQBIiiz5k",
      "expanded_url" : "http://developer-evangelism.com/google-reader-subscriptions.xml",
      "display_url" : "developer-evangelism.com/google-reader-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352067369969000448",
  "text" : "OMG. Only now discovering @codepo8's RSS feed collection. http://t.co/jOQBIiiz5k Yay!",
  "id" : 352067369969000448,
  "created_at" : "Tue Jul 02 14:12:56 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "352065235479314433",
  "geo" : {
  },
  "id_str" : "352067006629023746",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Awesome. Missed that ;) Thanks!",
  "id" : 352067006629023746,
  "in_reply_to_status_id" : 352065235479314433,
  "created_at" : "Tue Jul 02 14:11:30 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Paul Irish",
      "screen_name" : "paul_irish",
      "indices" : [ 24, 35 ],
      "id_str" : "1671811",
      "id" : 1671811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "352065082391408642",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 I already have @paul_irish's `Frontend` OPML. I am interested in the other feeds I saw in the screenshot. Can haz OPML?",
  "id" : 352065082391408642,
  "created_at" : "Tue Jul 02 14:03:51 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 8, 16 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "352064367442935808",
  "text" : "Dear Mr @codepo8. I see you quite enjoy your RSS. Can you export an OPML and release it publicly so everybody can use it?",
  "id" : 352064367442935808,
  "created_at" : "Tue Jul 02 14:01:00 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 129 ],
      "url" : "http://t.co/32fHTchnFN",
      "expanded_url" : "http://daniemon.com/blog/bootstrap-without-jquery/",
      "display_url" : "daniemon.com/blog/bootstrap\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "352059740009930752",
  "text" : "I see an increasing trend of people offering alternative (Vanilla JS) equivalents to jQuery. This is good. http://t.co/32fHTchnFN",
  "id" : 352059740009930752,
  "created_at" : "Tue Jul 02 13:42:37 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 125 ],
      "url" : "http://t.co/Kd1olxsirX",
      "expanded_url" : "http://www.tannr.com/herp-derp-youtube-comments/",
      "display_url" : "tannr.com/herp-derp-yout\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "351842649533186048",
  "geo" : {
  },
  "id_str" : "351855576256483328",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 I like the extension. There is another extension like that called herp derp youtube comments: http://t.co/Kd1olxsirX ;)",
  "id" : 351855576256483328,
  "in_reply_to_status_id" : 351842649533186048,
  "created_at" : "Tue Jul 02 00:11:21 +0000 2013",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "351842010342232065",
  "text" : "So That film Olympus has Fallen - WTF? Some bad guys capture president, president gets rescued, end of story?! Laaaame!",
  "id" : 351842010342232065,
  "created_at" : "Mon Jul 01 23:17:26 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alan",
      "screen_name" : "AlanMorrison",
      "indices" : [ 3, 16 ],
      "id_str" : "7005092",
      "id" : 7005092
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "351812216103763968",
  "text" : "RT @AlanMorrison: Wikipedia in English will start using VisualEditor on July 1st, so contributors won't have to learn wiki markup. http://t\u2026",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 135 ],
        "url" : "http://t.co/gjQqWyKYKr",
        "expanded_url" : "http://en.wikipedia.org/wiki/Wikipedia:VisualEditor",
        "display_url" : "en.wikipedia.org/wiki/Wikipedia\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "350628603983171585",
    "text" : "Wikipedia in English will start using VisualEditor on July 1st, so contributors won't have to learn wiki markup. http://t.co/gjQqWyKYKr",
    "id" : 350628603983171585,
    "created_at" : "Fri Jun 28 14:55:48 +0000 2013",
    "user" : {
      "name" : "Alan",
      "screen_name" : "AlanMorrison",
      "protected" : false,
      "id_str" : "7005092",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1555688513/Alan_Morrison_high_res_photo_normal.JPG",
      "id" : 7005092,
      "verified" : false
    }
  },
  "id" : 351812216103763968,
  "created_at" : "Mon Jul 01 21:19:03 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0160ime Vidas",
      "screen_name" : "simevidas",
      "indices" : [ 0, 10 ],
      "id_str" : "175727560",
      "id" : 175727560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http://t.co/bkbp1OCYyY",
      "expanded_url" : "http://m.youtube.com/watch?v=XxeN56g2U3k",
      "display_url" : "m.youtube.com/watch?v=XxeN56\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "351809721788940291",
  "geo" : {
  },
  "id_str" : "351811824779411456",
  "in_reply_to_user_id" : 175727560,
  "text" : "@simevidas My favorite song for coding is http://t.co/bkbp1OCYyY Probably listened to that well over 10,000 times. Easy to get wired in...",
  "id" : 351811824779411456,
  "in_reply_to_status_id" : 351809721788940291,
  "created_at" : "Mon Jul 01 21:17:30 +0000 2013",
  "in_reply_to_screen_name" : "simevidas",
  "in_reply_to_user_id_str" : "175727560",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "knowledge",
      "indices" : [ 127, 137 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 33 ],
      "url" : "http://t.co/AEA26BpG6B",
      "expanded_url" : "http://labnotes.org/",
      "display_url" : "labnotes.org"
    }, {
      "indices" : [ 47, 69 ],
      "url" : "http://t.co/b0UJx8ux7h",
      "expanded_url" : "http://pineapple.io/",
      "display_url" : "pineapple.io"
    } ]
  },
  "geo" : {
  },
  "id_str" : "351760637568888833",
  "text" : "So reading http://t.co/AEA26BpG6B Coupled with http://t.co/b0UJx8ux7h  - I always feel like Neo when he says \"I know kung-fu\". #knowledge",
  "id" : 351760637568888833,
  "created_at" : "Mon Jul 01 17:54:06 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 24 ],
      "url" : "http://t.co/KNcix9y1Jk",
      "expanded_url" : "http://nativeformelements.com/",
      "display_url" : "nativeformelements.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "351747902860836865",
  "text" : "\u2665 http://t.co/KNcix9y1Jk",
  "id" : 351747902860836865,
  "created_at" : "Mon Jul 01 17:03:29 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 91 ],
      "url" : "http://t.co/7vHkxfZoeJ",
      "expanded_url" : "http://blog.mozilla.org/blog/2013/07/01/mozilla-and-partners-prepare-to-launch-first-firefox-os-smartphones/",
      "display_url" : "blog.mozilla.org/blog/2013/07/0\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "351747521200128002",
  "text" : "Mozilla and Partners Prepare to Launch First Firefox OS\u00A0Smartphones! http://t.co/7vHkxfZoeJ",
  "id" : 351747521200128002,
  "created_at" : "Mon Jul 01 17:01:58 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/#!/download/ipad\" rel=\"nofollow\">Twitter for iPad</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 3, 14 ],
      "id_str" : "104462925",
      "id" : 104462925
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 108 ],
      "url" : "http://t.co/A0NsL7nLwp",
      "expanded_url" : "http://j.mp/126w8PZ",
      "display_url" : "j.mp/126w8PZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "351737073050456064",
  "text" : "RT @techiejayk: Chance.js: Utility library to generate anything random for JavaScript http://t.co/A0NsL7nLwp",
  "retweeted_status" : {
    "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 92 ],
        "url" : "http://t.co/A0NsL7nLwp",
        "expanded_url" : "http://j.mp/126w8PZ",
        "display_url" : "j.mp/126w8PZ"
      } ]
    },
    "geo" : {
    },
    "id_str" : "351729483641925634",
    "text" : "Chance.js: Utility library to generate anything random for JavaScript http://t.co/A0NsL7nLwp",
    "id" : 351729483641925634,
    "created_at" : "Mon Jul 01 15:50:18 +0000 2013",
    "user" : {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "protected" : false,
      "id_str" : "104462925",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3050293497/f330d8ba9496279d1ad37559cdf81a06_normal.jpeg",
      "id" : 104462925,
      "verified" : false
    }
  },
  "id" : 351737073050456064,
  "created_at" : "Mon Jul 01 16:20:27 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 44 ],
      "url" : "http://t.co/vFFK3GJZ8v",
      "expanded_url" : "http://blog.app.net/2013/06/21/what-is-posts-to-adn/?utm_source=rss&utm_medium=rss&utm_campaign=what-is-posts-to-adn",
      "display_url" : "blog.app.net/2013/06/21/wha\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "351486410710204418",
  "text" : "What is Posts to\u00A0ADN? http://t.co/vFFK3GJZ8v",
  "id" : 351486410710204418,
  "created_at" : "Sun Jun 30 23:44:25 +0000 2013",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
} ]