Grailbird.data.tweets_2012_11 = 
 [ {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "@2x",
      "screen_name" : "2x",
      "indices" : [ 0, 3 ],
      "id_str" : "281223516",
      "id" : 281223516
    }, {
      "name" : "Twitter",
      "screen_name" : "twitter",
      "indices" : [ 14, 22 ],
      "id_str" : "783214",
      "id" : 783214
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "responsive",
      "indices" : [ 46, 57 ]
    } ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/o2cDOzUy",
      "expanded_url" : "http://isharefil.es/LG3W",
      "display_url" : "isharefil.es/LG3W"
    } ]
  },
  "geo" : {
  },
  "id_str" : "274598419442565120",
  "in_reply_to_user_id" : 281223516,
  "text" : "@2x Oh really @twitter \u2049 http://t.co/o2cDOzUy #responsive",
  "id" : 274598419442565120,
  "created_at" : "Fri Nov 30 19:38:59 +0000 2012",
  "in_reply_to_screen_name" : "2x",
  "in_reply_to_user_id_str" : "281223516",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Abraham Williams",
      "screen_name" : "abraham",
      "indices" : [ 3, 11 ],
      "id_str" : "9436992",
      "id" : 9436992
    }, {
      "name" : "Twitter",
      "screen_name" : "twitter",
      "indices" : [ 19, 27 ],
      "id_str" : "783214",
      "id" : 783214
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "274596044648636416",
  "text" : "RT @abraham: Since @twitter no longer surfaces tweet sources it's going to cause more apps to append \"via name\" and make tweets more spammy.",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Twitter",
        "screen_name" : "twitter",
        "indices" : [ 6, 14 ],
        "id_str" : "783214",
        "id" : 783214
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "274580596066512896",
    "text" : "Since @twitter no longer surfaces tweet sources it's going to cause more apps to append \"via name\" and make tweets more spammy.",
    "id" : 274580596066512896,
    "created_at" : "Fri Nov 30 18:28:10 +0000 2012",
    "user" : {
      "name" : "Abraham Williams",
      "screen_name" : "abraham",
      "protected" : false,
      "id_str" : "9436992",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3258886660/a2c58de63368c9438bd30b0f4ed2ff36_normal.jpeg",
      "id" : 9436992,
      "verified" : false
    }
  },
  "id" : 274596044648636416,
  "created_at" : "Fri Nov 30 19:29:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 113 ],
      "url" : "http://t.co/Jab7UKpw",
      "expanded_url" : "http://bit.ly/U6CMkY",
      "display_url" : "bit.ly/U6CMkY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "274559526777274368",
  "text" : "\"A dead-simple way to generate dynamic bits of HTML and add subtle effects to your content.\" http://t.co/Jab7UKpw",
  "id" : 274559526777274368,
  "created_at" : "Fri Nov 30 17:04:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http://t.co/Lj1jAJEL",
      "expanded_url" : "http://isharefil.es/LGCj",
      "display_url" : "isharefil.es/LGCj"
    } ]
  },
  "geo" : {
  },
  "id_str" : "274555913359200256",
  "text" : "Stats for iwantaneff.in over the last month. Not bad ;) http://t.co/Lj1jAJEL",
  "id" : 274555913359200256,
  "created_at" : "Fri Nov 30 16:50:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "273955719131308032",
  "geo" : {
  },
  "id_str" : "274496085806968832",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys Those ui / ux proposals could be made redundant when NEW NEW NEW Twitter is released.",
  "id" : 274496085806968832,
  "in_reply_to_status_id" : 273955719131308032,
  "created_at" : "Fri Nov 30 12:52:21 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    }, {
      "name" : "mention",
      "screen_name" : "mention",
      "indices" : [ 93, 101 ],
      "id_str" : "435854974",
      "id" : 435854974
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "273955719131308032",
  "geo" : {
  },
  "id_str" : "274495513204772864",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys You should infiltrate the Twitter Team. E-Mail them, phone them, but by all means, don't @mention them. There is a way ;)",
  "id" : 274495513204772864,
  "in_reply_to_status_id" : 273955719131308032,
  "created_at" : "Fri Nov 30 12:50:04 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "J-Strizzle",
      "screen_name" : "jstrauss",
      "indices" : [ 0, 9 ],
      "id_str" : "13263",
      "id" : 13263
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "awesm",
      "indices" : [ 108, 114 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "274297019701530624",
  "in_reply_to_user_id" : 13263,
  "text" : "@jstrauss Hey mate. You wouldn't have a spare awe.sm account I could use for free lying around, would you \u2049 #awesm",
  "id" : 274297019701530624,
  "created_at" : "Thu Nov 29 23:41:20 +0000 2012",
  "in_reply_to_screen_name" : "jstrauss",
  "in_reply_to_user_id_str" : "13263",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 94 ],
      "url" : "http://t.co/mxPpT2lw",
      "expanded_url" : "http://myshar.es/TwxIdm",
      "display_url" : "myshar.es/TwxIdm"
    } ]
  },
  "geo" : {
  },
  "id_str" : "274260640787476480",
  "text" : "alertifyjs - An unobtrusive customizable JavaScript notification system \u2192 http://t.co/mxPpT2lw",
  "id" : 274260640787476480,
  "created_at" : "Thu Nov 29 21:16:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 106 ],
      "url" : "http://t.co/Aw9rTnKW",
      "expanded_url" : "http://isharefil.es/LEvA",
      "display_url" : "isharefil.es/LEvA"
    } ]
  },
  "geo" : {
  },
  "id_str" : "274259924270325761",
  "text" : "You must be ahead of the game PPL. Just when I got used to webdev, this post appears: http://t.co/Aw9rTnKW :/",
  "id" : 274259924270325761,
  "created_at" : "Thu Nov 29 21:13:56 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "App.net",
      "screen_name" : "AppDotNet",
      "indices" : [ 3, 13 ],
      "id_str" : "104558396",
      "id" : 104558396
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/lcL3xMAx",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 73, 93 ],
      "url" : "http://t.co/lcL3xMAx",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 107, 127 ],
      "url" : "http://t.co/6ec9dTzT",
      "expanded_url" : "http://wordpress.org/extend/plugins/adn-profile/",
      "display_url" : "wordpress.org/extend/plugins\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "274256383686373376",
  "text" : "RT @AppDotNet: New http://t.co/lcL3xMAx Wordpress plugin to display your http://t.co/lcL3xMAx status -&gt; http://t.co/6ec9dTzT",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.echofon.com/\" rel=\"nofollow\">Echofon</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 4, 24 ],
        "url" : "http://t.co/lcL3xMAx",
        "expanded_url" : "http://App.net",
        "display_url" : "App.net"
      }, {
        "indices" : [ 58, 78 ],
        "url" : "http://t.co/lcL3xMAx",
        "expanded_url" : "http://App.net",
        "display_url" : "App.net"
      }, {
        "indices" : [ 92, 112 ],
        "url" : "http://t.co/6ec9dTzT",
        "expanded_url" : "http://wordpress.org/extend/plugins/adn-profile/",
        "display_url" : "wordpress.org/extend/plugins\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "274235031071911936",
    "text" : "New http://t.co/lcL3xMAx Wordpress plugin to display your http://t.co/lcL3xMAx status -&gt; http://t.co/6ec9dTzT",
    "id" : 274235031071911936,
    "created_at" : "Thu Nov 29 19:35:01 +0000 2012",
    "user" : {
      "name" : "App.net",
      "screen_name" : "AppDotNet",
      "protected" : false,
      "id_str" : "104558396",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000173471968/3e29d2f6ae34138d34d62dbb50fcc900_normal.png",
      "id" : 104558396,
      "verified" : false
    }
  },
  "id" : 274256383686373376,
  "created_at" : "Thu Nov 29 20:59:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "domains",
      "indices" : [ 27, 35 ]
    }, {
      "text" : "domainr",
      "indices" : [ 57, 65 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/F1iLOZlJ",
      "expanded_url" : "http://isharefil.es/LErN/o",
      "display_url" : "isharefil.es/LErN/o"
    } ]
  },
  "geo" : {
  },
  "id_str" : "274255004884430848",
  "text" : "Lolwut favicon. S3 server. #domains http://t.co/F1iLOZlJ #domainr",
  "id" : 274255004884430848,
  "created_at" : "Thu Nov 29 20:54:23 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/fPnWtjT6",
      "expanded_url" : "http://myshar.es/YjXGog",
      "display_url" : "myshar.es/YjXGog"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273928005527683073",
  "text" : "Sheetsee http://t.co/fPnWtjT6",
  "id" : 273928005527683073,
  "created_at" : "Wed Nov 28 23:15:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http://t.co/75XqMiJp",
      "expanded_url" : "http://myshar.es/V2qnzB",
      "display_url" : "myshar.es/V2qnzB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273916677245255680",
  "text" : "Sortable Lists With Drag&amp;Drop \u2014 HTML5 Sortable http://t.co/75XqMiJp",
  "id" : 273916677245255680,
  "created_at" : "Wed Nov 28 22:29:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/HfXvWlaw",
      "expanded_url" : "http://myshar.es/YjXOnB",
      "display_url" : "myshar.es/YjXOnB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273902586321580033",
  "text" : "HubInfo http://t.co/HfXvWlaw",
  "id" : 273902586321580033,
  "created_at" : "Wed Nov 28 21:34:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/uVIp6N42",
      "expanded_url" : "http://myshar.es/V2qknq",
      "display_url" : "myshar.es/V2qknq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273886276833406976",
  "text" : "Sort CSS Properties In Specific Order \u2014 CSScomb http://t.co/uVIp6N42",
  "id" : 273886276833406976,
  "created_at" : "Wed Nov 28 20:29:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/lCjHhc6f",
      "expanded_url" : "http://myshar.es/V2qivZ",
      "display_url" : "myshar.es/V2qivZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273872432346054657",
  "text" : "Awesome And Customizable Gauges \u2014 justGage http://t.co/lCjHhc6f",
  "id" : 273872432346054657,
  "created_at" : "Wed Nov 28 19:34:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/M9DS9w1D",
      "expanded_url" : "http://myshar.es/YjXIfZ",
      "display_url" : "myshar.es/YjXIfZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273856826330714113",
  "text" : "Pie Charts For Favicons \u2014 Piecon http://t.co/M9DS9w1D",
  "id" : 273856826330714113,
  "created_at" : "Wed Nov 28 18:32:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tobias Klika",
      "screen_name" : "artistandsocial",
      "indices" : [ 0, 16 ],
      "id_str" : "97317473",
      "id" : 97317473
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "273856115228434432",
  "geo" : {
  },
  "id_str" : "273856550668488704",
  "in_reply_to_user_id" : 97317473,
  "text" : "@artistandsocial True dat ;)",
  "id" : 273856550668488704,
  "in_reply_to_status_id" : 273856115228434432,
  "created_at" : "Wed Nov 28 18:31:04 +0000 2012",
  "in_reply_to_screen_name" : "artistandsocial",
  "in_reply_to_user_id_str" : "97317473",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tobias Klika",
      "screen_name" : "artistandsocial",
      "indices" : [ 0, 16 ],
      "id_str" : "97317473",
      "id" : 97317473
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 38 ],
      "url" : "https://t.co/Ol7WUd5R",
      "expanded_url" : "https://twitter.com/_higg/status/273851094134837248",
      "display_url" : "twitter.com/_higg/status/2\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "273851024752656385",
  "geo" : {
  },
  "id_str" : "273854914306916354",
  "in_reply_to_user_id" : 97317473,
  "text" : "@artistandsocial https://t.co/Ol7WUd5R ;)",
  "id" : 273854914306916354,
  "in_reply_to_status_id" : 273851024752656385,
  "created_at" : "Wed Nov 28 18:24:34 +0000 2012",
  "in_reply_to_screen_name" : "artistandsocial",
  "in_reply_to_user_id_str" : "97317473",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/NsKjHDhp",
      "expanded_url" : "http://myshar.es/YjXHZt",
      "display_url" : "myshar.es/YjXHZt"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273854058064912384",
  "text" : "Pixel-perfect Vector Icons \u2014 Crisp Icon Set http://t.co/NsKjHDhp",
  "id" : 273854058064912384,
  "created_at" : "Wed Nov 28 18:21:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/wwxd01Z1",
      "expanded_url" : "http://myshar.es/V2qfAj",
      "display_url" : "myshar.es/V2qfAj"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273853059447287808",
  "text" : "Slurp Edition (one) http://t.co/wwxd01Z1",
  "id" : 273853059447287808,
  "created_at" : "Wed Nov 28 18:17:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alvaro Graves",
      "screen_name" : "alvarograves",
      "indices" : [ 0, 13 ],
      "id_str" : "39816942",
      "id" : 39816942
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/a1razvYI",
      "expanded_url" : "http://stackoverflow.com/questions/436411/where-is-the-best-place-to-put-script-tags-in-html-markup",
      "display_url" : "stackoverflow.com/questions/4364\u2026"
    }, {
      "indices" : [ 56, 76 ],
      "url" : "http://t.co/FrYF3VDD",
      "expanded_url" : "http://developer.yahoo.com/performance/rules.html#js_bottom",
      "display_url" : "developer.yahoo.com/performance/ru\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "273850468818632704",
  "geo" : {
  },
  "id_str" : "273851094134837248",
  "in_reply_to_user_id" : 39816942,
  "text" : "@alvarograves It's a best practice http://t.co/a1razvYI http://t.co/FrYF3VDD I feel so insulted reading the whole \"anywhere in &lt;head&gt;\" thing",
  "id" : 273851094134837248,
  "in_reply_to_status_id" : 273850468818632704,
  "created_at" : "Wed Nov 28 18:09:23 +0000 2012",
  "in_reply_to_screen_name" : "alvarograves",
  "in_reply_to_user_id_str" : "39816942",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "273850201469493248",
  "text" : "So many tutorial sites asking me to \"put this javascript anywhere in the &lt;head&gt;\" I'm like, LOL WAT? End of &lt;body&gt; tag .... Always !!",
  "id" : 273850201469493248,
  "created_at" : "Wed Nov 28 18:05:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/x23Z2x3f",
      "expanded_url" : "http://myshar.es/YjXFAP",
      "display_url" : "myshar.es/YjXFAP"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273841472900583426",
  "text" : "prettyCheckable http://t.co/x23Z2x3f",
  "id" : 273841472900583426,
  "created_at" : "Wed Nov 28 17:31:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/uZnLdONC",
      "expanded_url" : "http://myshar.es/YjXHIV",
      "display_url" : "myshar.es/YjXHIV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273823856538566656",
  "text" : "Proton++ http://t.co/uZnLdONC",
  "id" : 273823856538566656,
  "created_at" : "Wed Nov 28 16:21:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 7, 27 ],
      "url" : "http://t.co/OAgEHnpq",
      "expanded_url" : "http://myshar.es/YjXjtK",
      "display_url" : "myshar.es/YjXjtK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273820328856285184",
  "text" : "pXY.js http://t.co/OAgEHnpq",
  "id" : 273820328856285184,
  "created_at" : "Wed Nov 28 16:07:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/mFN2T1eh",
      "expanded_url" : "http://myshar.es/V2q2gp",
      "display_url" : "myshar.es/V2q2gp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273818943922253824",
  "text" : "TypeScript http://t.co/mFN2T1eh",
  "id" : 273818943922253824,
  "created_at" : "Wed Nov 28 16:01:38 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/VGTdBQj1",
      "expanded_url" : "http://myshar.es/YjXm8S",
      "display_url" : "myshar.es/YjXm8S"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273805227000475650",
  "text" : "Fokus Plugin http://t.co/VGTdBQj1",
  "id" : 273805227000475650,
  "created_at" : "Wed Nov 28 15:07:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 29, 32 ]
    } ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/ivpWGFBL",
      "expanded_url" : "http://myshar.es/YjXlSs",
      "display_url" : "myshar.es/YjXlSs"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273792388667961344",
  "text" : "XLSX.js http://t.co/ivpWGFBL #js",
  "id" : 273792388667961344,
  "created_at" : "Wed Nov 28 14:16:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 7, 27 ],
      "url" : "http://t.co/a3DmEW36",
      "expanded_url" : "http://myshar.es/V2q08g",
      "display_url" : "myshar.es/V2q08g"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273774771580321792",
  "text" : "DocPad http://t.co/a3DmEW36",
  "id" : 273774771580321792,
  "created_at" : "Wed Nov 28 13:06:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 25 ],
      "url" : "http://t.co/eMOXsReE",
      "expanded_url" : "http://myshar.es/V2q4oo",
      "display_url" : "myshar.es/V2q4oo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273762440389464064",
  "text" : "Tres http://t.co/eMOXsReE",
  "id" : 273762440389464064,
  "created_at" : "Wed Nov 28 12:17:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 25 ],
      "url" : "http://t.co/oRepqB0C",
      "expanded_url" : "http://myshar.es/YjXllu",
      "display_url" : "myshar.es/YjXllu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273758788870348801",
  "text" : "Meny http://t.co/oRepqB0C",
  "id" : 273758788870348801,
  "created_at" : "Wed Nov 28 12:02:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "package",
      "indices" : [ 27, 35 ]
    } ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/lqvQO7SE",
      "expanded_url" : "http://myshar.es/YjXkOy",
      "display_url" : "myshar.es/YjXkOy"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273751361777176576",
  "text" : "Bower http://t.co/lqvQO7SE #package manage all teh things ;)",
  "id" : 273751361777176576,
  "created_at" : "Wed Nov 28 11:33:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/e2lXeKut",
      "expanded_url" : "http://myshar.es/YjX8ie",
      "display_url" : "myshar.es/YjX8ie"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273749602052734976",
  "text" : "Strapdown.js http://t.co/e2lXeKut",
  "id" : 273749602052734976,
  "created_at" : "Wed Nov 28 11:26:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/jnlqHc9C",
      "expanded_url" : "http://myshar.es/V2pVl7",
      "display_url" : "myshar.es/V2pVl7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273747586417045504",
  "text" : "Forward http://t.co/jnlqHc9C",
  "id" : 273747586417045504,
  "created_at" : "Wed Nov 28 11:18:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/nJiBAdxz",
      "expanded_url" : "http://myshar.es/V2pVkT",
      "display_url" : "myshar.es/V2pVkT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273698005062914048",
  "text" : "Vanilla JS http://t.co/nJiBAdxz",
  "id" : 273698005062914048,
  "created_at" : "Wed Nov 28 08:01:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/mWfJ9MSc",
      "expanded_url" : "http://myshar.es/V2pV4v",
      "display_url" : "myshar.es/V2pV4v"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273566114942971904",
  "text" : "TideSDK http://t.co/mWfJ9MSc",
  "id" : 273566114942971904,
  "created_at" : "Tue Nov 27 23:16:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/hcSLoTJZ",
      "expanded_url" : "http://myshar.es/YjX5CX",
      "display_url" : "myshar.es/YjX5CX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273565613421629440",
  "text" : "jsPDF http://t.co/hcSLoTJZ",
  "id" : 273565613421629440,
  "created_at" : "Tue Nov 27 23:14:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 32, 35 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/rLyUEzvB",
      "expanded_url" : "http://myshar.es/V2pKpK",
      "display_url" : "myshar.es/V2pKpK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273554285059514369",
  "text" : "enquire.js http://t.co/rLyUEzvB #js",
  "id" : 273554285059514369,
  "created_at" : "Tue Nov 27 22:29:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 33, 36 ]
    } ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/U3qSnIw6",
      "expanded_url" : "http://myshar.es/YjWQYB",
      "display_url" : "myshar.es/YjWQYB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273540188184145921",
  "text" : "Cerebral.js http://t.co/U3qSnIw6 #js",
  "id" : 273540188184145921,
  "created_at" : "Tue Nov 27 21:33:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/SfBp8qYO",
      "expanded_url" : "http://myshar.es/V2pEP5",
      "display_url" : "myshar.es/V2pEP5"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273523828792102912",
  "text" : "Advanced File Upload Techniques \u2014 Chunked Uploading http://t.co/SfBp8qYO",
  "id" : 273523828792102912,
  "created_at" : "Tue Nov 27 20:28:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/sAkQF82t",
      "expanded_url" : "http://myshar.es/YjWPUv",
      "display_url" : "myshar.es/YjWPUv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273509936934309889",
  "text" : "BigScreen http://t.co/sAkQF82t",
  "id" : 273509936934309889,
  "created_at" : "Tue Nov 27 19:33:45 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 29, 32 ]
    }, {
      "text" : "ui",
      "indices" : [ 33, 36 ]
    } ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/3UFKoAXS",
      "expanded_url" : "http://myshar.es/V2pEyC",
      "display_url" : "myshar.es/V2pEyC"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273494460799803392",
  "text" : "Avgrund http://t.co/3UFKoAXS #js #ui",
  "id" : 273494460799803392,
  "created_at" : "Tue Nov 27 18:32:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 30, 33 ]
    } ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/UiwDu5Bj",
      "expanded_url" : "http://myshar.es/V2pE1A",
      "display_url" : "myshar.es/V2pE1A"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273491693087633408",
  "text" : "Chirp.js http://t.co/UiwDu5Bj #js",
  "id" : 273491693087633408,
  "created_at" : "Tue Nov 27 18:21:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/gi9K9SpF",
      "expanded_url" : "http://myshar.es/V2pGX8",
      "display_url" : "myshar.es/V2pGX8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273490681891266560",
  "text" : "oriDomi http://t.co/gi9K9SpF",
  "id" : 273490681891266560,
  "created_at" : "Tue Nov 27 18:17:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/hFJ5IYyQ",
      "expanded_url" : "http://myshar.es/YjWNvO",
      "display_url" : "myshar.es/YjWNvO"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273479106346229762",
  "text" : "HubSearch http://t.co/hFJ5IYyQ",
  "id" : 273479106346229762,
  "created_at" : "Tue Nov 27 17:31:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "adn",
      "indices" : [ 52, 56 ]
    }, {
      "text" : "appdotnet",
      "indices" : [ 57, 67 ]
    } ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/4ro8AJSw",
      "expanded_url" : "http://myshar.es/YjWP78",
      "display_url" : "myshar.es/YjWP78"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273461484254789633",
  "text" : "AppDotNet Post Link Extraction http://t.co/4ro8AJSw #adn #appdotnet",
  "id" : 273461484254789633,
  "created_at" : "Tue Nov 27 16:21:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/7X59QWJH",
      "expanded_url" : "http://myshar.es/YjWKjI",
      "display_url" : "myshar.es/YjWKjI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273457963174359040",
  "text" : "\"Labnotes\" http://t.co/7X59QWJH",
  "id" : 273457963174359040,
  "created_at" : "Tue Nov 27 16:07:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/dSbWz8ds",
      "expanded_url" : "http://myshar.es/YjNn3q",
      "display_url" : "myshar.es/YjNn3q"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273456579498627074",
  "text" : "David Higgins (davidhiggins) on Flattr http://t.co/dSbWz8ds",
  "id" : 273456579498627074,
  "created_at" : "Tue Nov 27 16:01:43 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 94 ],
      "url" : "http://t.co/zSdx7Rym",
      "expanded_url" : "http://myshar.es/YjNkEW",
      "display_url" : "myshar.es/YjNkEW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273442859854729216",
  "text" : "\u2605 Pinboard Linkdump. Quality Linkdump. Linklist. Link Collections. Links. http://t.co/zSdx7Rym",
  "id" : 273442859854729216,
  "created_at" : "Tue Nov 27 15:07:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 90, 93 ]
    }, {
      "text" : "html5",
      "indices" : [ 94, 100 ]
    } ],
    "urls" : [ {
      "indices" : [ 69, 89 ],
      "url" : "http://t.co/SHzAtsMS",
      "expanded_url" : "http://myshar.es/YjNeND",
      "display_url" : "myshar.es/YjNeND"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273430020515586048",
  "text" : "Fun with the &lt;progress&gt; element - Oscillating rotating message http://t.co/SHzAtsMS #js #html5",
  "id" : 273430020515586048,
  "created_at" : "Tue Nov 27 14:16:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ascii",
      "indices" : [ 38, 44 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/7pc1olTL",
      "expanded_url" : "http://myshar.es/YjNc8u",
      "display_url" : "myshar.es/YjNc8u"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273430020192624640",
  "text" : "ascii art museum http://t.co/7pc1olTL #ascii",
  "id" : 273430020192624640,
  "created_at" : "Tue Nov 27 14:16:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "mjex",
      "indices" : [ 48, 53 ]
    } ],
    "urls" : [ {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/ciL0y7ki",
      "expanded_url" : "http://myshar.es/V2kimY",
      "display_url" : "myshar.es/V2kimY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273412403264385024",
  "text" : "Messiah J &amp; The Expert http://t.co/ciL0y7ki #mjex",
  "id" : 273412403264385024,
  "created_at" : "Tue Nov 27 13:06:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 65 ],
      "url" : "http://t.co/iCW4966R",
      "expanded_url" : "http://myshar.es/YjN4Wp",
      "display_url" : "myshar.es/YjN4Wp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273396292556840960",
  "text" : "Asciimation has now moved to reverse the web http://t.co/iCW4966R",
  "id" : 273396292556840960,
  "created_at" : "Tue Nov 27 12:02:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 43, 46 ]
    }, {
      "text" : "json",
      "indices" : [ 47, 52 ]
    } ],
    "urls" : [ {
      "indices" : [ 22, 42 ],
      "url" : "http://t.co/A1oFr0Vp",
      "expanded_url" : "http://myshar.es/YjN0Gd",
      "display_url" : "myshar.es/YjN0Gd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273388993322815488",
  "text" : "JSON Select in action http://t.co/A1oFr0Vp #js #json",
  "id" : 273388993322815488,
  "created_at" : "Tue Nov 27 11:33:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 82 ],
      "url" : "http://t.co/7f54apKy",
      "expanded_url" : "http://myshar.es/YjLync",
      "display_url" : "myshar.es/YjLync"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273387232583352320",
  "text" : "Mastering SVG use for a retina web, fallbacks with PNG script http://t.co/7f54apKy",
  "id" : 273387232583352320,
  "created_at" : "Tue Nov 27 11:26:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 98 ],
      "url" : "http://t.co/zQhtQL5d",
      "expanded_url" : "http://myshar.es/V2j8Ia",
      "display_url" : "myshar.es/V2j8Ia"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273385219573305344",
  "text" : "LinkChecker | A handy bookmarklet and jS lib to check a page for broken links http://t.co/zQhtQL5d",
  "id" : 273385219573305344,
  "created_at" : "Tue Nov 27 11:18:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 94 ],
      "url" : "http://t.co/CtKhQjGj",
      "expanded_url" : "http://myshar.es/V2j76M",
      "display_url" : "myshar.es/V2j76M"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273335757471625216",
  "text" : "Mozilla Social API built for users first, and won't lead to Firefox bloat http://t.co/CtKhQjGj",
  "id" : 273335757471625216,
  "created_at" : "Tue Nov 27 08:01:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jo Nakashima",
      "screen_name" : "nakashima_jo",
      "indices" : [ 39, 52 ],
      "id_str" : "798694118",
      "id" : 798694118
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 105, 112 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/5XlzWlTI",
      "expanded_url" : "http://myshar.es/TqYCUc",
      "display_url" : "myshar.es/TqYCUc"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273221554173800448",
  "text" : "Very weird account I just discovered \u2192 @nakashima_jo http://t.co/5XlzWlTI 21,000 retweets all related to #webdev and only 172 followers \u2049",
  "id" : 273221554173800448,
  "created_at" : "Tue Nov 27 00:27:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 122, 142 ],
      "url" : "http://t.co/O9Ug5NtO",
      "expanded_url" : "http://myshar.es/YjLh3K",
      "display_url" : "myshar.es/YjLh3K"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273203738594922496",
  "text" : "Every ~5th GitHub project is JavaScript! Non interpreted languages only &lt;15%. Is this the TIOBE ranking of the future? http://t.co/O9Ug5NtO",
  "id" : 273203738594922496,
  "created_at" : "Mon Nov 26 23:17:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http://t.co/ctvLK2I5",
      "expanded_url" : "http://myshar.es/YjLfIZ",
      "display_url" : "myshar.es/YjLfIZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273203237291692032",
  "text" : "Codepoints (All about Unicode). Very useful site. http://t.co/ctvLK2I5",
  "id" : 273203237291692032,
  "created_at" : "Mon Nov 26 23:15:02 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Swipp",
      "screen_name" : "getswipp",
      "indices" : [ 59, 68 ],
      "id_str" : "474329934",
      "id" : 474329934
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http://t.co/t8NI9VzV",
      "expanded_url" : "http://myshar.es/XXj2sz",
      "display_url" : "myshar.es/XXj2sz"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273193239442501632",
  "text" : "Really looking forward to Swipp \u2192 http://t.co/t8NI9VzV cc: @getswipp",
  "id" : 273193239442501632,
  "created_at" : "Mon Nov 26 22:35:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "JS",
      "indices" : [ 12, 15 ]
    }, {
      "text" : "jQuery",
      "indices" : [ 48, 55 ]
    } ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http://t.co/L02CeKVt",
      "expanded_url" : "http://myshar.es/V2iXMS",
      "display_url" : "myshar.es/V2iXMS"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273191909676163072",
  "text" : "Interesting #JS micro-framework, alternative to #jQuery http://t.co/L02CeKVt",
  "id" : 273191909676163072,
  "created_at" : "Mon Nov 26 22:30:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http://t.co/ZKxQiOPR",
      "expanded_url" : "http://myshar.es/YjL5kW",
      "display_url" : "myshar.es/YjL5kW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273177814277189632",
  "text" : "jBar plugin getting a rewrite with tonnes of additional options.  http://t.co/ZKxQiOPR",
  "id" : 273177814277189632,
  "created_at" : "Mon Nov 26 21:34:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 119, 139 ],
      "url" : "http://t.co/zcwSrtwA",
      "expanded_url" : "http://myshar.es/V2iQkl",
      "display_url" : "myshar.es/V2iQkl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273161453568143361",
  "text" : "This is\u2026 crazy: d\u014Dmo lets you write HTML markup and CSS styles in JavaScript syntax, in the browser and on the server  http://t.co/zcwSrtwA",
  "id" : 273161453568143361,
  "created_at" : "Mon Nov 26 20:29:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/YnkqJ3QA",
      "expanded_url" : "http://myshar.es/V2iMkx",
      "display_url" : "myshar.es/V2iMkx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273147734821834753",
  "text" : "Re:Inventing Email  http://t.co/YnkqJ3QA",
  "id" : 273147734821834753,
  "created_at" : "Mon Nov 26 19:34:29 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 137 ],
      "url" : "http://t.co/xpchBSYq",
      "expanded_url" : "http://myshar.es/V2iN8g",
      "display_url" : "myshar.es/V2iN8g"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273132076411281408",
  "text" : "BREAKING NEWS: Facebook puts out an official response to the privacy notice everyone is posting on their profiles -  http://t.co/xpchBSYq",
  "id" : 273132076411281408,
  "created_at" : "Mon Nov 26 18:32:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http://t.co/87hkDz0n",
      "expanded_url" : "http://myshar.es/YjKPm3",
      "display_url" : "myshar.es/YjKPm3"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273129309697343488",
  "text" : "LESS Hat \u2013 bunch of smart and useful LESS mixins. Made by the CSSHat folks http://t.co/87hkDz0n",
  "id" : 273129309697343488,
  "created_at" : "Mon Nov 26 18:21:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/miz1wcTM",
      "expanded_url" : "http://myshar.es/YjKHTw",
      "display_url" : "myshar.es/YjKHTw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273128301374078977",
  "text" : "Great article about large scale CSS architecture http://t.co/miz1wcTM",
  "id" : 273128301374078977,
  "created_at" : "Mon Nov 26 18:17:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "instacrap",
      "indices" : [ 102, 112 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "273127324361306112",
  "text" : "The amount of Instagram and Flickr links I *do not* click on in my Twitter feed is  astoundingly low. #instacrap",
  "id" : 273127324361306112,
  "created_at" : "Mon Nov 26 18:13:23 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/4v0sqY0t",
      "expanded_url" : "http://myshar.es/Yj0su2",
      "display_url" : "myshar.es/Yj0su2"
    } ]
  },
  "geo" : {
  },
  "id_str" : "273066513425121280",
  "text" : "Welcome to Yearofmoo http://t.co/4v0sqY0t",
  "id" : 273066513425121280,
  "created_at" : "Mon Nov 26 14:11:44 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/4lTo3cg9",
      "expanded_url" : "http://myshar.es/Yh7dwg",
      "display_url" : "myshar.es/Yh7dwg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272860260442718211",
  "text" : "TL;DR http://t.co/4lTo3cg9",
  "id" : 272860260442718211,
  "created_at" : "Mon Nov 26 00:32:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Allardice",
      "screen_name" : "james_allardice",
      "indices" : [ 3, 19 ],
      "id_str" : "612821980",
      "id" : 612821980
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 84 ],
      "url" : "http://t.co/64L3Q0eq",
      "expanded_url" : "http://jslinterrors.com",
      "display_url" : "jslinterrors.com"
    }, {
      "indices" : [ 90, 110 ],
      "url" : "http://t.co/XXnvWZz3",
      "expanded_url" : "http://jshinterrors.com",
      "display_url" : "jshinterrors.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272792194371448832",
  "text" : "RT @james_allardice: JSHint error messages are now available on http://t.co/64L3Q0eq! And http://t.co/XXnvWZz3 redirects to the appropri ...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 43, 63 ],
        "url" : "http://t.co/64L3Q0eq",
        "expanded_url" : "http://jslinterrors.com",
        "display_url" : "jslinterrors.com"
      }, {
        "indices" : [ 69, 89 ],
        "url" : "http://t.co/XXnvWZz3",
        "expanded_url" : "http://jshinterrors.com",
        "display_url" : "jshinterrors.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "272646769807093760",
    "text" : "JSHint error messages are now available on http://t.co/64L3Q0eq! And http://t.co/XXnvWZz3 redirects to the appropriate page.",
    "id" : 272646769807093760,
    "created_at" : "Sun Nov 25 10:23:50 +0000 2012",
    "user" : {
      "name" : "James Allardice",
      "screen_name" : "james_allardice",
      "protected" : false,
      "id_str" : "612821980",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2323361802/69a215lalsv3gizzack1_normal.jpeg",
      "id" : 612821980,
      "verified" : false
    }
  },
  "id" : 272792194371448832,
  "created_at" : "Sun Nov 25 20:01:42 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "272755360882884609",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Thanks for the feedback on higg.im Also, I just recently hooked up myshar.es to bit.ly custom domains, so older links are dead ;)",
  "id" : 272755360882884609,
  "created_at" : "Sun Nov 25 17:35:20 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "lolwut",
      "indices" : [ 72, 79 ]
    } ],
    "urls" : [ {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/IL3urzZq",
      "expanded_url" : "http://myshar.es/Yg7Kyv",
      "display_url" : "myshar.es/Yg7Kyv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272754843733594114",
  "text" : "A lightweight jQuery dateinput picker http://t.co/IL3urzZq pickadate.js #lolwut, we have over 9000 of these already",
  "id" : 272754843733594114,
  "created_at" : "Sun Nov 25 17:33:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/QFerpgnw",
      "expanded_url" : "http://higg.im/",
      "display_url" : "higg.im"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272385323219173376",
  "text" : "Can somebody give feedback on this\u2049 http://t.co/QFerpgnw Spent the afternoon creating this. Just need thoughts, negative or otherwise. Thx",
  "id" : 272385323219173376,
  "created_at" : "Sat Nov 24 17:04:56 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/v8ByzuD6",
      "expanded_url" : "http://bit.ly/URKMYh",
      "display_url" : "bit.ly/URKMYh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272265987359903744",
  "text" : "print.css \u2014 only display what's needed http://t.co/v8ByzuD6",
  "id" : 272265987359903744,
  "created_at" : "Sat Nov 24 09:10:44 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adam Whitcroft",
      "screen_name" : "AdamWhitcroft",
      "indices" : [ 55, 69 ],
      "id_str" : "25791837",
      "id" : 25791837
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 92 ],
      "url" : "http://t.co/kBgXcCrh",
      "expanded_url" : "http://myshar.es/6",
      "display_url" : "myshar.es/6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272126190662324225",
  "text" : "Batch: 300 Icons for Web and User Interface Design via @adamwhitcroft \u2192 http://t.co/kBgXcCrh",
  "id" : 272126190662324225,
  "created_at" : "Fri Nov 23 23:55:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 100 ],
      "url" : "http://t.co/pyZwR89a",
      "expanded_url" : "http://icant.co.uk",
      "display_url" : "icant.co.uk"
    }, {
      "indices" : [ 103, 123 ],
      "url" : "http://t.co/7kp9Lrr5",
      "expanded_url" : "http://trashbat.co.ck/",
      "display_url" : "trashbat.co.ck"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272123088735461378",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 The Nathan Barley domain name trashbat.cock is still up. Reminds me of http://t.co/pyZwR89a \u2192 http://t.co/7kp9Lrr5",
  "id" : 272123088735461378,
  "created_at" : "Fri Nov 23 23:42:54 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/v8ByzuD6",
      "expanded_url" : "http://bit.ly/URKMYh",
      "display_url" : "bit.ly/URKMYh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272060428358651904",
  "text" : "print.css \u2014 only display what's needed http://t.co/v8ByzuD6",
  "id" : 272060428358651904,
  "created_at" : "Fri Nov 23 19:33:55 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 42 ],
      "url" : "http://t.co/bXDFv93i",
      "expanded_url" : "http://bit.ly/URKO25",
      "display_url" : "bit.ly/URKO25"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272044815192059904",
  "text" : "imo instant messenger http://t.co/bXDFv93i",
  "id" : 272044815192059904,
  "created_at" : "Fri Nov 23 18:31:52 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 86, 97 ],
      "id_str" : "104462925",
      "id" : 104462925
    }, {
      "name" : "integralist",
      "screen_name" : "integralist",
      "indices" : [ 98, 110 ],
      "id_str" : "18293449",
      "id" : 18293449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/QFerpgnw",
      "expanded_url" : "http://higg.im/",
      "display_url" : "higg.im"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272043648743526401",
  "text" : "Just made this \u2192 http://t.co/QFerpgnw For the express purpose of contacting me ;) cc: @techiejayk @integralist",
  "id" : 272043648743526401,
  "created_at" : "Fri Nov 23 18:27:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "linkdump",
      "indices" : [ 113, 122 ]
    } ],
    "urls" : [ {
      "indices" : [ 92, 112 ],
      "url" : "http://t.co/y9c8efxf",
      "expanded_url" : "http://isharefil.es/L6Sa",
      "display_url" : "isharefil.es/L6Sa"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272042405207887872",
  "text" : "Currently working on converting this to a website, and also adding more links to the list \u2192 http://t.co/y9c8efxf #linkdump",
  "id" : 272042405207887872,
  "created_at" : "Fri Nov 23 18:22:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/U7ZbcEGX",
      "expanded_url" : "http://isharefil.es/L7kx",
      "display_url" : "isharefil.es/L7kx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272040131710550016",
  "text" : "Always worth a read when you feel geeky: http://t.co/U7ZbcEGX",
  "id" : 272040131710550016,
  "created_at" : "Fri Nov 23 18:13:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/hQeWEeyA",
      "expanded_url" : "http://bit.ly/Ya21tX",
      "display_url" : "bit.ly/Ya21tX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272040014865637376",
  "text" : "Bootstrap-modal by jschr http://t.co/hQeWEeyA",
  "id" : 272040014865637376,
  "created_at" : "Fri Nov 23 18:12:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/8XsAQkHg",
      "expanded_url" : "http://bit.ly/Ya1TuB",
      "display_url" : "bit.ly/Ya1TuB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "272037352782823424",
  "text" : "TL;DR \u2014 Faster News http://t.co/8XsAQkHg",
  "id" : 272037352782823424,
  "created_at" : "Fri Nov 23 18:02:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Inspectlet",
      "screen_name" : "Inspectlet",
      "indices" : [ 0, 11 ],
      "id_str" : "282198792",
      "id" : 282198792
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 88 ],
      "url" : "http://t.co/WkazxLPc",
      "expanded_url" : "http://bit.ly/p7UbBG",
      "display_url" : "bit.ly/p7UbBG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271946130865135616",
  "in_reply_to_user_id" : 282198792,
  "text" : "@inspectlet \u2192 Website Heatmaps, Screen Capture, Real-time analytics http://t.co/WkazxLPc",
  "id" : 271946130865135616,
  "created_at" : "Fri Nov 23 11:59:44 +0000 2012",
  "in_reply_to_screen_name" : "Inspectlet",
  "in_reply_to_user_id_str" : "282198792",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WePay",
      "screen_name" : "WePay",
      "indices" : [ 49, 55 ],
      "id_str" : "16582289",
      "id" : 16582289
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/KlaDA9Kx",
      "expanded_url" : "http://bit.ly/TuJxh8",
      "display_url" : "bit.ly/TuJxh8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271903648781570048",
  "text" : "Payment Buttons - WePay http://t.co/KlaDA9Kx cc: @wepay",
  "id" : 271903648781570048,
  "created_at" : "Fri Nov 23 09:10:56 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 108 ],
      "url" : "http://t.co/MOo0F5Zr",
      "expanded_url" : "http://myshar.es/5",
      "display_url" : "myshar.es/5"
    } ]
  },
  "in_reply_to_status_id_str" : "271708060614017024",
  "geo" : {
  },
  "id_str" : "271721147131895809",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Get money get paid ;) wooot. Are you on Flattr \u2049 Sign up. Wanna donate to you. http://t.co/MOo0F5Zr",
  "id" : 271721147131895809,
  "in_reply_to_status_id" : 271708060614017024,
  "created_at" : "Thu Nov 22 21:05:44 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chad Whitacre",
      "screen_name" : "whit537",
      "indices" : [ 55, 63 ],
      "id_str" : "34175404",
      "id" : 34175404
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "github",
      "indices" : [ 64, 71 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/a1KbQNor",
      "expanded_url" : "http://bit.ly/LO2Uyd",
      "display_url" : "bit.ly/LO2Uyd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271702545880195074",
  "text" : "Gittip - Inspiring Generosity http://t.co/a1KbQNor cc: @whit537.#github",
  "id" : 271702545880195074,
  "created_at" : "Thu Nov 22 19:51:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Richard Banfield",
      "screen_name" : "freshtilledsoil",
      "indices" : [ 55, 71 ],
      "id_str" : "11317842",
      "id" : 11317842
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 72, 83 ]
    } ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/onsqlb81",
      "expanded_url" : "http://bit.ly/UROm4D",
      "display_url" : "bit.ly/UROm4D"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271677618267500544",
  "text" : "champagne.js http://t.co/onsqlb81 This is awesome! cc: @freshtilledsoil #javascript",
  "id" : 271677618267500544,
  "created_at" : "Thu Nov 22 18:12:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kit Cambridge",
      "screen_name" : "kitcambridge",
      "indices" : [ 33, 46 ],
      "id_str" : "136077128",
      "id" : 136077128
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 47, 50 ]
    } ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/vtAuyLqH",
      "expanded_url" : "http://bit.ly/Y0AX0m",
      "display_url" : "bit.ly/Y0AX0m"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271674915256676352",
  "text" : "evil.js http://t.co/vtAuyLqH cc: @kitcambridge #js Awesome!",
  "id" : 271674915256676352,
  "created_at" : "Thu Nov 22 18:02:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SpeedAwarenessMonth",
      "screen_name" : "SpeedMonth",
      "indices" : [ 77, 88 ],
      "id_str" : "632445769",
      "id" : 632445769
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/ULw6Qxc9",
      "expanded_url" : "http://bit.ly/Qmtvrl",
      "display_url" : "bit.ly/Qmtvrl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271666534580121602",
  "text" : "Speed Awareness Month - Make the Web a Faster Place http://t.co/ULw6Qxc9 cc: @speedmonth",
  "id" : 271666534580121602,
  "created_at" : "Thu Nov 22 17:28:43 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fight for the Future",
      "screen_name" : "fightfortheftr",
      "indices" : [ 49, 64 ],
      "id_str" : "382376904",
      "id" : 382376904
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/NTj473PQ",
      "expanded_url" : "http://bit.ly/t55DK1",
      "display_url" : "bit.ly/t55DK1"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271655726550761473",
  "text" : "I Work For The Internet http://t.co/NTj473PQ cc: @fightfortheftr",
  "id" : 271655726550761473,
  "created_at" : "Thu Nov 22 16:45:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/ya1y6Nk5",
      "expanded_url" : "http://bit.ly/Y0vK8P",
      "display_url" : "bit.ly/Y0vK8P"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271644658487152640",
  "text" : "RRR GGG BBB http://t.co/ya1y6Nk5",
  "id" : 271644658487152640,
  "created_at" : "Thu Nov 22 16:01:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/i9ZStaNV",
      "expanded_url" : "http://bit.ly/Y0vDKd",
      "display_url" : "bit.ly/Y0vDKd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271630938553860097",
  "text" : "A wild developer has appeared! http://t.co/i9ZStaNV",
  "id" : 271630938553860097,
  "created_at" : "Thu Nov 22 15:07:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/C81tc3Yn",
      "expanded_url" : "http://bit.ly/URLDrR",
      "display_url" : "bit.ly/URLDrR"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271618104742772736",
  "text" : "Garlic.js http://t.co/C81tc3Yn",
  "id" : 271618104742772736,
  "created_at" : "Thu Nov 22 14:16:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/tR10kHSo",
      "expanded_url" : "http://bit.ly/Y0vA1g",
      "display_url" : "bit.ly/Y0vA1g"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271600486254272513",
  "text" : "\u203E\u203E/\u203E\u203E http://t.co/tR10kHSo",
  "id" : 271600486254272513,
  "created_at" : "Thu Nov 22 13:06:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Frederik Hermann",
      "screen_name" : "netzkobold",
      "indices" : [ 0, 11 ],
      "id_str" : "611613",
      "id" : 611613
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "271418508225687552",
  "geo" : {
  },
  "id_str" : "271600306901614592",
  "in_reply_to_user_id" : 611613,
  "text" : "@netzkobold Hey. Yeah, thanks for the note. Thanks for passing on my proposal to the devs. Enjoy thanksgiving ;)",
  "id" : 271600306901614592,
  "in_reply_to_status_id" : 271418508225687552,
  "created_at" : "Thu Nov 22 13:05:33 +0000 2012",
  "in_reply_to_screen_name" : "netzkobold",
  "in_reply_to_user_id_str" : "611613",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/fwv1o2iG",
      "expanded_url" : "http://bit.ly/Y0vypY",
      "display_url" : "bit.ly/Y0vypY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271588025711079424",
  "text" : "Noooooooooooooo http://t.co/fwv1o2iG",
  "id" : 271588025711079424,
  "created_at" : "Thu Nov 22 12:16:45 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/6KQjmLTa",
      "expanded_url" : "http://bit.ly/Y0v5Ee",
      "display_url" : "bit.ly/Y0v5Ee"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271584375211184131",
  "text" : "TipTheWeb \u2014 Support Your Favorite Stuff Online http://t.co/6KQjmLTa",
  "id" : 271584375211184131,
  "created_at" : "Thu Nov 22 12:02:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dmitriy A.",
      "screen_name" : "jimaek",
      "indices" : [ 68, 75 ],
      "id_str" : "141747302",
      "id" : 141747302
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 76, 79 ]
    }, {
      "text" : "cdn",
      "indices" : [ 80, 84 ]
    } ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/1NO10RXj",
      "expanded_url" : "http://bit.ly/JJTXK0",
      "display_url" : "bit.ly/JJTXK0"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271583781415182336",
  "text" : "Free CDN for javascript and jQuery plugins http://t.co/1NO10RXj cc: @jimaek #js #cdn",
  "id" : 271583781415182336,
  "created_at" : "Thu Nov 22 11:59:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 34 ],
      "url" : "http://t.co/LRAx5V3d",
      "expanded_url" : "http://bit.ly/Y0v4QD",
      "display_url" : "bit.ly/Y0v4QD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271576954099081216",
  "text" : "UsabilityPost http://t.co/LRAx5V3d",
  "id" : 271576954099081216,
  "created_at" : "Thu Nov 22 11:32:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/vn9SHd1M",
      "expanded_url" : "http://bit.ly/URLsN2",
      "display_url" : "bit.ly/URLsN2"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271575312956010496",
  "text" : "DeadMouse: Surf the web with just your keyboard. http://t.co/vn9SHd1M",
  "id" : 271575312956010496,
  "created_at" : "Thu Nov 22 11:26:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/5AtIKtyQ",
      "expanded_url" : "http://bit.ly/KlmMeS",
      "display_url" : "bit.ly/KlmMeS"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271573176897650689",
  "text" : "dynamo.js http://t.co/5AtIKtyQ",
  "id" : 271573176897650689,
  "created_at" : "Thu Nov 22 11:17:45 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Crossrider",
      "screen_name" : "crossrider",
      "indices" : [ 85, 96 ],
      "id_str" : "244928743",
      "id" : 244928743
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http://t.co/M05m5nub",
      "expanded_url" : "http://bit.ly/L1xdEh",
      "display_url" : "bit.ly/L1xdEh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271541222701674496",
  "text" : "Build Cross Browser Extensions with JavaScript &amp; jQuery http://t.co/M05m5nub via @crossrider",
  "id" : 271541222701674496,
  "created_at" : "Thu Nov 22 09:10:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/8IVylVhC",
      "expanded_url" : "http://bit.ly/URLjt4",
      "display_url" : "bit.ly/URLjt4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271523716205268992",
  "text" : "ikiwiki http://t.co/8IVylVhC",
  "id" : 271523716205268992,
  "created_at" : "Thu Nov 22 08:01:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Frederik Hermann",
      "screen_name" : "netzkobold",
      "indices" : [ 59, 70 ],
      "id_str" : "611613",
      "id" : 611613
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 95 ],
      "url" : "https://t.co/TA1Xj1pG",
      "expanded_url" : "https://flattr.com/thing/226974/Frederik-Hermann",
      "display_url" : "flattr.com/thing/226974/F\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271412470441185280",
  "text" : "Frederik Hermann is awesome, so I sent a Flattr payment to @netzkobold -  https://t.co/TA1Xj1pG",
  "id" : 271412470441185280,
  "created_at" : "Thu Nov 22 00:39:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "271400588795863041",
  "text" : "Unicode continues to surprise me: N\u0338W\u0338A",
  "id" : 271400588795863041,
  "created_at" : "Wed Nov 21 23:51:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/jpBDhK8Q",
      "expanded_url" : "http://bit.ly/URLjt1",
      "display_url" : "bit.ly/URLjt1"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271391826764767232",
  "text" : "Rainbow - Javascript Code Syntax Highlighting http://t.co/jpBDhK8Q",
  "id" : 271391826764767232,
  "created_at" : "Wed Nov 21 23:17:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/2CPOSsVM",
      "expanded_url" : "http://bit.ly/Y0uXER",
      "display_url" : "bit.ly/Y0uXER"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271391323523776513",
  "text" : "d\u014Dmo: Markup, style, and code in one language. http://t.co/2CPOSsVM",
  "id" : 271391323523776513,
  "created_at" : "Wed Nov 21 23:15:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/CYJHHNMj",
      "expanded_url" : "http://bit.ly/Y0uRNo",
      "display_url" : "bit.ly/Y0uRNo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271379995262337026",
  "text" : "Fuel UX http://t.co/CYJHHNMj",
  "id" : 271379995262337026,
  "created_at" : "Wed Nov 21 22:30:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 3, 17 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/KC2nvjTl",
      "expanded_url" : "http://codepoints.net/",
      "display_url" : "codepoints.net"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271370349013258240",
  "text" : "RT @soaj1664ashar: http://t.co/KC2nvjTl (All about Unicode). Very useful site.",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 0, 20 ],
        "url" : "http://t.co/KC2nvjTl",
        "expanded_url" : "http://codepoints.net/",
        "display_url" : "codepoints.net"
      } ]
    },
    "geo" : {
    },
    "id_str" : "271350028822528000",
    "text" : "http://t.co/KC2nvjTl (All about Unicode). Very useful site.",
    "id" : 271350028822528000,
    "created_at" : "Wed Nov 21 20:31:02 +0000 2012",
    "user" : {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "protected" : false,
      "id_str" : "277735240",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3466042152/c41edb1dfd837f875f26ce4beb6b9543_normal.jpeg",
      "id" : 277735240,
      "verified" : false
    }
  },
  "id" : 271370349013258240,
  "created_at" : "Wed Nov 21 21:51:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/DAuP0atH",
      "expanded_url" : "http://bit.ly/Y0uJgZ",
      "display_url" : "bit.ly/Y0uJgZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271365902019219457",
  "text" : "Gmail Notifier http://t.co/DAuP0atH",
  "id" : 271365902019219457,
  "created_at" : "Wed Nov 21 21:34:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/tIj6ihSP",
      "expanded_url" : "http://bit.ly/URLbd2",
      "display_url" : "bit.ly/URLbd2"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271349541972873217",
  "text" : "Grid-A-Licious http://t.co/tIj6ihSP",
  "id" : 271349541972873217,
  "created_at" : "Wed Nov 21 20:29:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MarkupValidator",
      "screen_name" : "MarkupValidator",
      "indices" : [ 58, 74 ],
      "id_str" : "318431467",
      "id" : 318431467
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/AcKWlqYg",
      "expanded_url" : "http://bit.ly/URMusA",
      "display_url" : "bit.ly/URMusA"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271340163794345986",
  "text" : "Top 100 markup validation errors http://t.co/AcKWlqYg cc: @MarkupValidator Great tool ;)",
  "id" : 271340163794345986,
  "created_at" : "Wed Nov 21 19:51:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Kevin Dangoor",
      "screen_name" : "dangoor",
      "indices" : [ 9, 17 ],
      "id_str" : "8831572",
      "id" : 8831572
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "271299345532477440",
  "geo" : {
  },
  "id_str" : "271337287000936448",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 @dangoor No persona for basement dweller? (\u256F\u00B0\u25A1\u00B0)\u256F\uFE35 \u253B\u2501\u253B",
  "id" : 271337287000936448,
  "in_reply_to_status_id" : 271299345532477440,
  "created_at" : "Wed Nov 21 19:40:25 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/0YfJCYSI",
      "expanded_url" : "http://bit.ly/URL8Ol",
      "display_url" : "bit.ly/URL8Ol"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271335734282186752",
  "text" : "Tyler Smith http://t.co/0YfJCYSI",
  "id" : 271335734282186752,
  "created_at" : "Wed Nov 21 19:34:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/KH7Pu3iT",
      "expanded_url" : "http://bit.ly/INKpgp",
      "display_url" : "bit.ly/INKpgp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271320123003006976",
  "text" : "Elliott Kember http://t.co/KH7Pu3iT",
  "id" : 271320123003006976,
  "created_at" : "Wed Nov 21 18:32:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alvaro Graves",
      "screen_name" : "alvarograves",
      "indices" : [ 3, 16 ],
      "id_str" : "39816942",
      "id" : 39816942
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 106 ],
      "url" : "http://t.co/4j4yUhyD",
      "expanded_url" : "http://bit.ly/QYsLM4",
      "display_url" : "bit.ly/QYsLM4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271318965211512832",
  "text" : "RT @alvarograves: Interesting JS micro-framework, alternative to jQuery (via @_higg ) http://t.co/4j4yUhyD",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.echofon.com/\" rel=\"nofollow\">Echofon</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 68, 88 ],
        "url" : "http://t.co/4j4yUhyD",
        "expanded_url" : "http://bit.ly/QYsLM4",
        "display_url" : "bit.ly/QYsLM4"
      } ]
    },
    "geo" : {
    },
    "id_str" : "271256665041563648",
    "text" : "Interesting JS micro-framework, alternative to jQuery (via @_higg ) http://t.co/4j4yUhyD",
    "id" : 271256665041563648,
    "created_at" : "Wed Nov 21 14:20:03 +0000 2012",
    "user" : {
      "name" : "Alvaro Graves",
      "screen_name" : "alvarograves",
      "protected" : false,
      "id_str" : "39816942",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000051378376/1f8b0e46f61919ebd23ed63e52f92e8a_normal.jpeg",
      "id" : 39816942,
      "verified" : false
    }
  },
  "id" : 271318965211512832,
  "created_at" : "Wed Nov 21 18:27:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/QglfGuRE",
      "expanded_url" : "http://bit.ly/Y0uxOT",
      "display_url" : "bit.ly/Y0uxOT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271317356658163712",
  "text" : "Print My Postage http://t.co/QglfGuRE",
  "id" : 271317356658163712,
  "created_at" : "Wed Nov 21 18:21:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/6Ske171B",
      "expanded_url" : "http://bit.ly/Y0uxyd",
      "display_url" : "bit.ly/Y0uxyd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271316348645298176",
  "text" : "cdn.cx : /see/ /dee/ /en/ -dot- /see/ /eks/ : http://t.co/6Ske171B",
  "id" : 271316348645298176,
  "created_at" : "Wed Nov 21 18:17:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marcel du Preez",
      "screen_name" : "marceldupreez",
      "indices" : [ 87, 101 ],
      "id_str" : "19853523",
      "id" : 19853523
    }, {
      "name" : "Simon Smith",
      "screen_name" : "blinkdesign",
      "indices" : [ 102, 114 ],
      "id_str" : "19663655",
      "id" : 19663655
    }, {
      "name" : "Andrew Revell",
      "screen_name" : "llevera",
      "indices" : [ 115, 123 ],
      "id_str" : "71581534",
      "id" : 71581534
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/lw4DeQVG",
      "expanded_url" : "http://bit.ly/qV78Ii",
      "display_url" : "bit.ly/qV78Ii"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271315317278511105",
  "text" : "BoxJS - A simple package management service http://t.co/lw4DeQVG Excellent work ;) cc: @marceldupreez @blinkdesign @llevera",
  "id" : 271315317278511105,
  "created_at" : "Wed Nov 21 18:13:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 0, 12 ],
      "id_str" : "793830",
      "id" : 793830
    }, {
      "name" : "Shop Talk Show",
      "screen_name" : "ShopTalkShow",
      "indices" : [ 13, 26 ],
      "id_str" : "457747138",
      "id" : 457747138
    }, {
      "name" : "Jeffrey Zeldman",
      "screen_name" : "zeldman",
      "indices" : [ 27, 35 ],
      "id_str" : "61133",
      "id" : 61133
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 132, 136 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "271311965144621056",
  "geo" : {
  },
  "id_str" : "271314745380982784",
  "in_reply_to_user_id" : 793830,
  "text" : "@chriscoyier @ShopTalkShow @zeldman meh. In CSS there is more than one way to skin a cat, right? Why the dogma and the drama. Sigh\u2049 #css",
  "id" : 271314745380982784,
  "in_reply_to_status_id" : 271311965144621056,
  "created_at" : "Wed Nov 21 18:10:50 +0000 2012",
  "in_reply_to_screen_name" : "chriscoyier",
  "in_reply_to_user_id_str" : "793830",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DocHub",
      "screen_name" : "documenthub",
      "indices" : [ 63, 75 ],
      "id_str" : "414208246",
      "id" : 414208246
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/lBz4kRvM",
      "expanded_url" : "http://bit.ly/v00PAS",
      "display_url" : "bit.ly/v00PAS"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271312626783510528",
  "text" : "DocHub | Instant Documentation Search http://t.co/lBz4kRvM cc: @documenthub",
  "id" : 271312626783510528,
  "created_at" : "Wed Nov 21 18:02:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/MmZ0E4FJ",
      "expanded_url" : "http://bit.ly/URL8hl",
      "display_url" : "bit.ly/URL8hl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271304770919219200",
  "text" : "briks - Extending the Web http://t.co/MmZ0E4FJ",
  "id" : 271304770919219200,
  "created_at" : "Wed Nov 21 17:31:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Cianciullo",
      "screen_name" : "jonnyjon",
      "indices" : [ 76, 85 ],
      "id_str" : "3065931",
      "id" : 3065931
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http://t.co/fC20qeoQ",
      "expanded_url" : "http://bit.ly/nl6chT",
      "display_url" : "bit.ly/nl6chT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271304145196163072",
  "text" : "Real Time Search - Social Mention http://t.co/fC20qeoQ This is awesome! cc: @jonnyjon",
  "id" : 271304145196163072,
  "created_at" : "Wed Nov 21 17:28:43 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Giannattasio",
      "screen_name" : "attasi",
      "indices" : [ 63, 70 ],
      "id_str" : "30870167",
      "id" : 30870167
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/C2G2Z78B",
      "expanded_url" : "http://bit.ly/dKIBpx",
      "display_url" : "bit.ly/dKIBpx"
    }, {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/gzOfLDkB",
      "expanded_url" : "http://bit.ly/JyOgei",
      "display_url" : "bit.ly/JyOgei"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271293344167120896",
  "text" : "Pluck: a simpler http://t.co/C2G2Z78B http://t.co/gzOfLDkB via @attasi",
  "id" : 271293344167120896,
  "created_at" : "Wed Nov 21 16:45:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/y5GkqeBK",
      "expanded_url" : "http://bit.ly/URL69i",
      "display_url" : "bit.ly/URL69i"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271287149058482177",
  "text" : "Infinite Scroll \u00B7 jQuery Masonry http://t.co/y5GkqeBK",
  "id" : 271287149058482177,
  "created_at" : "Wed Nov 21 16:21:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/n6g1Fxts",
      "expanded_url" : "http://bit.ly/KjU9Mi",
      "display_url" : "bit.ly/KjU9Mi"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271283626065408000",
  "text" : "jQuery Super Labels Plugin http://t.co/n6g1Fxts",
  "id" : 271283626065408000,
  "created_at" : "Wed Nov 21 16:07:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/naLpylHe",
      "expanded_url" : "http://bit.ly/URL5SL",
      "display_url" : "bit.ly/URL5SL"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271282244507807744",
  "text" : "JavaScript by jg - Kippt http://t.co/naLpylHe",
  "id" : 271282244507807744,
  "created_at" : "Wed Nov 21 16:01:41 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/tFsADyVp",
      "expanded_url" : "http://bit.ly/URL5SE",
      "display_url" : "bit.ly/URL5SE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271268523332997120",
  "text" : "jQuery plugin for Avgrund concept popin http://t.co/tFsADyVp",
  "id" : 271268523332997120,
  "created_at" : "Wed Nov 21 15:07:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/uWNMij9U",
      "expanded_url" : "http://bit.ly/Y0uuSX",
      "display_url" : "bit.ly/Y0uuSX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271255684627185664",
  "text" : "kylebarrow/chibi http://t.co/uWNMij9U",
  "id" : 271255684627185664,
  "created_at" : "Wed Nov 21 14:16:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 90 ],
      "url" : "http://t.co/ZM6u2nlN",
      "expanded_url" : "http://bit.ly/URL1T6",
      "display_url" : "bit.ly/URL1T6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271238070798544896",
  "text" : "Write in the above field and hold key to access alternate characters. http://t.co/ZM6u2nlN (Long Press)",
  "id" : 271238070798544896,
  "created_at" : "Wed Nov 21 13:06:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/H9vqNHx0",
      "expanded_url" : "http://bit.ly/URL15v",
      "display_url" : "bit.ly/URL15v"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271225731953090561",
  "text" : "\"Pinterest boxes jquery plugin\" http://t.co/H9vqNHx0",
  "id" : 271225731953090561,
  "created_at" : "Wed Nov 21 12:17:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/aFbUZEbg",
      "expanded_url" : "http://bit.ly/URKXTh",
      "display_url" : "bit.ly/URKXTh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271221958140641280",
  "text" : "147 CSS Color Names http://t.co/aFbUZEbg",
  "id" : 271221958140641280,
  "created_at" : "Wed Nov 21 12:02:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Mahemoff",
      "screen_name" : "mahemoff",
      "indices" : [ 40, 49 ],
      "id_str" : "1112431",
      "id" : 1112431
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/MiDHxmPc",
      "expanded_url" : "http://buff.ly/URM19E",
      "display_url" : "buff.ly/URM19E"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271221356832624640",
  "text" : "List Of Tweets http://t.co/MiDHxmPc via @mahemoff",
  "id" : 271221356832624640,
  "created_at" : "Wed Nov 21 11:59:45 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CSS",
      "indices" : [ 35, 39 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/8uM2lqm8",
      "expanded_url" : "http://bit.ly/URKXCE",
      "display_url" : "bit.ly/URKXCE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271214655874162688",
  "text" : "CSSrefresh - automatically refresh #CSS files http://t.co/8uM2lqm8",
  "id" : 271214655874162688,
  "created_at" : "Wed Nov 21 11:33:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/ZwTlg1Ag",
      "expanded_url" : "http://bit.ly/URKTD6",
      "display_url" : "bit.ly/URKTD6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271212894165803008",
  "text" : "DropIt - Automatic File Organizer To Sort Your Files http://t.co/ZwTlg1Ag",
  "id" : 271212894165803008,
  "created_at" : "Wed Nov 21 11:26:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 42 ],
      "url" : "http://t.co/bXDFv93i",
      "expanded_url" : "http://bit.ly/URKO25",
      "display_url" : "bit.ly/URKO25"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271210879377358848",
  "text" : "imo instant messenger http://t.co/bXDFv93i",
  "id" : 271210879377358848,
  "created_at" : "Wed Nov 21 11:18:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Greg Wilson",
      "screen_name" : "gregsramblings",
      "indices" : [ 59, 74 ],
      "id_str" : "10520572",
      "id" : 10520572
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http://t.co/paf0fMMm",
      "expanded_url" : "http://buff.ly/URLUuZ",
      "display_url" : "buff.ly/URLUuZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271178842352263169",
  "text" : "LottaTweets \u2192 Fast Twitter Reader http://t.co/paf0fMMm via @gregsramblings",
  "id" : 271178842352263169,
  "created_at" : "Wed Nov 21 09:10:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/v8ByzuD6",
      "expanded_url" : "http://bit.ly/URKMYh",
      "display_url" : "bit.ly/URKMYh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271161425324560384",
  "text" : "print.css \u2014 only display what's needed http://t.co/v8ByzuD6",
  "id" : 271161425324560384,
  "created_at" : "Wed Nov 21 08:01:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "notetoself",
      "indices" : [ 125, 136 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "271052732767535104",
  "text" : "Have an idea. Gonna try it. \u2192 Signup for LOADS of tech / design newsletters and then forward the HTML emails into a website. #notetoself",
  "id" : 271052732767535104,
  "created_at" : "Wed Nov 21 00:49:42 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Xylitol",
      "screen_name" : "Xylit0l",
      "indices" : [ 3, 11 ],
      "id_str" : "80275945",
      "id" : 80275945
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Blackhole",
      "indices" : [ 13, 23 ]
    }, {
      "text" : "Nuclear",
      "indices" : [ 107, 115 ]
    }, {
      "text" : "ek",
      "indices" : [ 116, 119 ]
    }, {
      "text" : "panel",
      "indices" : [ 120, 126 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "271048314991030272",
  "text" : "RT @Xylit0l: #Blackhole hxxp://qchtvjpmyfo.info/bhadmin.php - hxxp://6878tyuiiyiuyiyiu.pro:34214/block.php #Nuclear #ek #panel",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Blackhole",
        "indices" : [ 0, 10 ]
      }, {
        "text" : "Nuclear",
        "indices" : [ 94, 102 ]
      }, {
        "text" : "ek",
        "indices" : [ 103, 106 ]
      }, {
        "text" : "panel",
        "indices" : [ 107, 113 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "271047473668161536",
    "text" : "#Blackhole hxxp://qchtvjpmyfo.info/bhadmin.php - hxxp://6878tyuiiyiuyiyiu.pro:34214/block.php #Nuclear #ek #panel",
    "id" : 271047473668161536,
    "created_at" : "Wed Nov 21 00:28:48 +0000 2012",
    "user" : {
      "name" : "Xylitol",
      "screen_name" : "Xylit0l",
      "protected" : false,
      "id_str" : "80275945",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1376716218/cat_normal.jpg",
      "id" : 80275945,
      "verified" : false
    }
  },
  "id" : 271048314991030272,
  "created_at" : "Wed Nov 21 00:32:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "lolwut",
      "indices" : [ 92, 99 ]
    } ],
    "urls" : [ {
      "indices" : [ 71, 91 ],
      "url" : "http://t.co/tluMYv3Z",
      "expanded_url" : "http://bit.ly/URROMr",
      "display_url" : "bit.ly/URROMr"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271039642957127680",
  "text" : "Mailbox names can be unicode, but they're transmitted as utf-7 in IMAP http://t.co/tluMYv3Z #lolwut !!!",
  "id" : 271039642957127680,
  "created_at" : "Tue Nov 20 23:57:41 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tommy Ryan",
      "screen_name" : "tallb0y",
      "indices" : [ 3, 11 ],
      "id_str" : "14689316",
      "id" : 14689316
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "freeandopen",
      "indices" : [ 127, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 126 ],
      "url" : "http://t.co/JRMJnyt9",
      "expanded_url" : "http://goo.gl/Hs9l2",
      "display_url" : "goo.gl/Hs9l2"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271033950057689088",
  "text" : "RT @tallb0y: A free and open world depends on a free and open web. And a free and open web depends on me. http://t.co/JRMJnyt9 #freeandopen",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "freeandopen",
        "indices" : [ 114, 126 ]
      } ],
      "urls" : [ {
        "indices" : [ 93, 113 ],
        "url" : "http://t.co/JRMJnyt9",
        "expanded_url" : "http://goo.gl/Hs9l2",
        "display_url" : "goo.gl/Hs9l2"
      } ]
    },
    "geo" : {
    },
    "id_str" : "270969202641358849",
    "text" : "A free and open world depends on a free and open web. And a free and open web depends on me. http://t.co/JRMJnyt9 #freeandopen",
    "id" : 270969202641358849,
    "created_at" : "Tue Nov 20 19:17:46 +0000 2012",
    "user" : {
      "name" : "Tommy Ryan",
      "screen_name" : "tallb0y",
      "protected" : false,
      "id_str" : "14689316",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3315822980/a6edd45725615c4d49419ecde2589e8e_normal.jpeg",
      "id" : 14689316,
      "verified" : false
    }
  },
  "id" : 271033950057689088,
  "created_at" : "Tue Nov 20 23:35:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yousef Soffar",
      "screen_name" : "YousefSoffar",
      "indices" : [ 3, 16 ],
      "id_str" : "311408709",
      "id" : 311408709
    }, {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 18, 21 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "271033747447631872",
  "text" : "RT @YousefSoffar: @YS Can I have your handle if you, God forbids, die?",
  "retweeted_status" : {
    "source" : "<a href=\"http://blackberry.com/twitter\" rel=\"nofollow\">Twitter for BlackBerry\u00AE</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Youssef",
        "screen_name" : "ys",
        "indices" : [ 0, 3 ],
        "id_str" : "19010677",
        "id" : 19010677
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "271010756441546752",
    "in_reply_to_user_id" : 19010677,
    "text" : "@YS Can I have your handle if you, God forbids, die?",
    "id" : 271010756441546752,
    "created_at" : "Tue Nov 20 22:02:54 +0000 2012",
    "in_reply_to_screen_name" : "ys",
    "in_reply_to_user_id_str" : "19010677",
    "user" : {
      "name" : "Yousef Soffar",
      "screen_name" : "YousefSoffar",
      "protected" : false,
      "id_str" : "311408709",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3557396111/d6804903469c54ab1638bfbed4277eba_normal.jpeg",
      "id" : 311408709,
      "verified" : false
    }
  },
  "id" : 271033747447631872,
  "created_at" : "Tue Nov 20 23:34:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/BEvhqFYg",
      "expanded_url" : "http://bit.ly/URKQHn",
      "display_url" : "bit.ly/URKQHn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271029398772281345",
  "text" : "Swish - Easy SFTP for Windows http://t.co/BEvhqFYg",
  "id" : 271029398772281345,
  "created_at" : "Tue Nov 20 23:16:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "owncloud",
      "indices" : [ 40, 49 ]
    } ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/BI9xrbbP",
      "expanded_url" : "http://bit.ly/Y0tPRr",
      "display_url" : "bit.ly/Y0tPRr"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271028894738575360",
  "text" : "Owncloud Providers http://t.co/BI9xrbbP #owncloud",
  "id" : 271028894738575360,
  "created_at" : "Tue Nov 20 23:14:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 97, 109 ],
      "id_str" : "15736190",
      "id" : 15736190
    }, {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 110, 118 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Tantek \u00C7elik",
      "screen_name" : "t",
      "indices" : [ 119, 121 ],
      "id_str" : "11628",
      "id" : 11628
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "semantics",
      "indices" : [ 122, 132 ]
    } ],
    "urls" : [ {
      "indices" : [ 72, 92 ],
      "url" : "http://t.co/SQHZdjA8",
      "expanded_url" : "http://bit.ly/URMWal",
      "display_url" : "bit.ly/URMWal"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271024973773164545",
  "text" : "Web Data Commons \u2192 Extracting Structured Data from the Common Web Crawl http://t.co/SQHZdjA8 cc: @smashingmag @codepo8 @t #semantics",
  "id" : 271024973773164545,
  "created_at" : "Tue Nov 20 22:59:23 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Abraham Williams",
      "screen_name" : "abraham",
      "indices" : [ 0, 8 ],
      "id_str" : "9436992",
      "id" : 9436992
    }, {
      "name" : "Addvocate",
      "screen_name" : "addvocate",
      "indices" : [ 9, 19 ],
      "id_str" : "482230308",
      "id" : 482230308
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "271005676384440320",
  "geo" : {
  },
  "id_str" : "271006727632220160",
  "in_reply_to_user_id" : 9436992,
  "text" : "@abraham @addvocate Okay but what's the backend? PHP / Ruby? I have setup my own URL shorteners before. Just want to know the rig\u2049",
  "id" : 271006727632220160,
  "in_reply_to_status_id" : 271005676384440320,
  "created_at" : "Tue Nov 20 21:46:53 +0000 2012",
  "in_reply_to_screen_name" : "abraham",
  "in_reply_to_user_id_str" : "9436992",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Abraham Williams",
      "screen_name" : "abraham",
      "indices" : [ 0, 8 ],
      "id_str" : "9436992",
      "id" : 9436992
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "curious",
      "indices" : [ 73, 81 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "271001822066507777",
  "geo" : {
  },
  "id_str" : "271002973231017984",
  "in_reply_to_user_id" : 9436992,
  "text" : "@abraham What software / rig are you using for the add.vc URL shortener? #curious",
  "id" : 271002973231017984,
  "in_reply_to_status_id" : 271001822066507777,
  "created_at" : "Tue Nov 20 21:31:58 +0000 2012",
  "in_reply_to_screen_name" : "abraham",
  "in_reply_to_user_id_str" : "9436992",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Abraham Williams",
      "screen_name" : "abraham",
      "indices" : [ 3, 11 ],
      "id_str" : "9436992",
      "id" : 9436992
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 101 ],
      "url" : "http://t.co/mowFMGAb",
      "expanded_url" : "http://add.vc/e06",
      "display_url" : "add.vc/e06"
    } ]
  },
  "geo" : {
  },
  "id_str" : "271002542467592192",
  "text" : "RT @abraham: Firefox 17 is more social and secure, but doesn\u2019t care for leopards http://t.co/mowFMGAb",
  "retweeted_status" : {
    "source" : "<a href=\"http://addvocate.com\" rel=\"nofollow\">Addvocate</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 68, 88 ],
        "url" : "http://t.co/mowFMGAb",
        "expanded_url" : "http://add.vc/e06",
        "display_url" : "add.vc/e06"
      } ]
    },
    "geo" : {
    },
    "id_str" : "271001822066507777",
    "text" : "Firefox 17 is more social and secure, but doesn\u2019t care for leopards http://t.co/mowFMGAb",
    "id" : 271001822066507777,
    "created_at" : "Tue Nov 20 21:27:24 +0000 2012",
    "user" : {
      "name" : "Abraham Williams",
      "screen_name" : "abraham",
      "protected" : false,
      "id_str" : "9436992",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3258886660/a2c58de63368c9438bd30b0f4ed2ff36_normal.jpeg",
      "id" : 9436992,
      "verified" : false
    }
  },
  "id" : 271002542467592192,
  "created_at" : "Tue Nov 20 21:30:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/q5ZInRIc",
      "expanded_url" : "http://bit.ly/PfldzD",
      "display_url" : "bit.ly/PfldzD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270987108431323136",
  "text" : "jQuery stickysectionheaders plugin http://t.co/q5ZInRIc",
  "id" : 270987108431323136,
  "created_at" : "Tue Nov 20 20:28:56 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Felix Nagel",
      "screen_name" : "felixnagel",
      "indices" : [ 3, 14 ],
      "id_str" : "27008587",
      "id" : 27008587
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 123 ],
      "url" : "https://t.co/bI2uEHLP",
      "expanded_url" : "https://github.com/fnagel/jquery-ui/",
      "display_url" : "github.com/fnagel/jquery-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270984222066495488",
  "text" : "RT @felixnagel: @_higg Please note that your link is outdated! Please use this GitHub branch instead: https://t.co/bI2uEHLP",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.hootsuite.com\" rel=\"nofollow\">HootSuite</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 86, 107 ],
        "url" : "https://t.co/bI2uEHLP",
        "expanded_url" : "https://github.com/fnagel/jquery-ui/",
        "display_url" : "github.com/fnagel/jquery-\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "270500957241098240",
    "geo" : {
    },
    "id_str" : "270974937316081665",
    "in_reply_to_user_id" : 468853739,
    "text" : "@_higg Please note that your link is outdated! Please use this GitHub branch instead: https://t.co/bI2uEHLP",
    "id" : 270974937316081665,
    "in_reply_to_status_id" : 270500957241098240,
    "created_at" : "Tue Nov 20 19:40:34 +0000 2012",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "Felix Nagel",
      "screen_name" : "felixnagel",
      "protected" : false,
      "id_str" : "27008587",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1113915084/1dc9dbd9b78cf7e920bc7922e035f6d1_normal.jpeg",
      "id" : 27008587,
      "verified" : false
    }
  },
  "id" : 270984222066495488,
  "created_at" : "Tue Nov 20 20:17:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/kIeGSihZ",
      "expanded_url" : "http://bit.ly/XJow8V",
      "display_url" : "bit.ly/XJow8V"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270973300245676032",
  "text" : "2012_SDTVx264r.nfo http://t.co/kIeGSihZ",
  "id" : 270973300245676032,
  "created_at" : "Tue Nov 20 19:34:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/bQZzRwKF",
      "expanded_url" : "http://bit.ly/UIPCqw",
      "display_url" : "bit.ly/UIPCqw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270957696289755136",
  "text" : "Can you view my source \u2049? http://t.co/bQZzRwKF",
  "id" : 270957696289755136,
  "created_at" : "Tue Nov 20 18:32:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 65 ],
      "url" : "http://t.co/V0iKIdBL",
      "expanded_url" : "http://bit.ly/UIPzuX",
      "display_url" : "bit.ly/UIPzuX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270954926849523712",
  "text" : "Programming, Motherfucker - Do you speak it? http://t.co/V0iKIdBL",
  "id" : 270954926849523712,
  "created_at" : "Tue Nov 20 18:21:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/luapsXSj",
      "expanded_url" : "http://bit.ly/UIPmIi",
      "display_url" : "bit.ly/UIPmIi"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270953919532908544",
  "text" : "jbar - A jQuery enhanced toolbar http://t.co/luapsXSj",
  "id" : 270953919532908544,
  "created_at" : "Tue Nov 20 18:17:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iframe",
      "indices" : [ 96, 103 ]
    }, {
      "text" : "xdrf",
      "indices" : [ 104, 109 ]
    } ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http://t.co/oGTfG1kf",
      "expanded_url" : "http://bit.ly/JWxgkr",
      "display_url" : "bit.ly/JWxgkr"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270942340804399104",
  "text" : "Porthole \u2192 JavaScript Library for Secure Cross Domain iFrame Communication http://t.co/oGTfG1kf #iframe #xdrf",
  "id" : 270942340804399104,
  "created_at" : "Tue Nov 20 17:31:02 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 98 ],
      "url" : "http://t.co/frJpq7bv",
      "expanded_url" : "http://bit.ly/S0vDF7",
      "display_url" : "bit.ly/S0vDF7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270924727256772608",
  "text" : "If you need a pixel.gif file, here's a Single Serving Site that gives you one http://t.co/frJpq7bv",
  "id" : 270924727256772608,
  "created_at" : "Tue Nov 20 16:21:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/TuctdsOh",
      "expanded_url" : "http://bit.ly/XJ7LuK",
      "display_url" : "bit.ly/XJ7LuK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270921199327006720",
  "text" : "http://t.co/TuctdsOh Nice list of cool bookmarklets",
  "id" : 270921199327006720,
  "created_at" : "Tue Nov 20 16:07:02 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 52, 56 ]
    } ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/Tlz23xhm",
      "expanded_url" : "http://bit.ly/XJ40Wa",
      "display_url" : "bit.ly/XJ40Wa"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270919818260144129",
  "text" : "XSS Filter Evasion Cheat Sheet http://t.co/Tlz23xhm #xss",
  "id" : 270919818260144129,
  "created_at" : "Tue Nov 20 16:01:32 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 93 ],
      "url" : "http://t.co/PyqUEPki",
      "expanded_url" : "http://bit.ly/UIBD46",
      "display_url" : "bit.ly/UIBD46"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270906100302372865",
  "text" : "SAC: Archivers, exe-compressors, archiver shells and other related utils http://t.co/PyqUEPki",
  "id" : 270906100302372865,
  "created_at" : "Tue Nov 20 15:07:02 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/lRpdIGsg",
      "expanded_url" : "http://bit.ly/KJoaJs",
      "display_url" : "bit.ly/KJoaJs"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270893260417929218",
  "text" : "Microformats Wiki \u2192 http://t.co/lRpdIGsg",
  "id" : 270893260417929218,
  "created_at" : "Tue Nov 20 14:16:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/6hZLkGvr",
      "expanded_url" : "http://bit.ly/XIXKNZ",
      "display_url" : "bit.ly/XIXKNZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270875642806026240",
  "text" : "Native Fullscreen JavaScript API http://t.co/6hZLkGvr",
  "id" : 270875642806026240,
  "created_at" : "Tue Nov 20 13:06:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 65 ],
      "url" : "http://t.co/IDuFctW0",
      "expanded_url" : "http://bit.ly/Lcv4s5",
      "display_url" : "bit.ly/Lcv4s5"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270863309438345216",
  "text" : "Retina.js \u2192 Retina graphics for your website http://t.co/IDuFctW0",
  "id" : 270863309438345216,
  "created_at" : "Tue Nov 20 12:17:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/SIcJqufZ",
      "expanded_url" : "http://bit.ly/PimutQ",
      "display_url" : "bit.ly/PimutQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270859786118111232",
  "text" : "Unicode Character Ranges http://t.co/SIcJqufZ",
  "id" : 270859786118111232,
  "created_at" : "Tue Nov 20 12:02:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/LvMc86Rk",
      "expanded_url" : "http://bit.ly/L4PdM7",
      "display_url" : "bit.ly/L4PdM7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270852234223431680",
  "text" : "Cool 404! http://t.co/LvMc86Rk",
  "id" : 270852234223431680,
  "created_at" : "Tue Nov 20 11:32:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hex",
      "indices" : [ 60, 64 ]
    } ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/aW3IVoZy",
      "expanded_url" : "http://bit.ly/XIRw0D",
      "display_url" : "bit.ly/XIRw0D"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270850473093918720",
  "text" : "Browser for words that make hex codes. http://t.co/aW3IVoZy #hex",
  "id" : 270850473093918720,
  "created_at" : "Tue Nov 20 11:25:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 127 ],
      "url" : "http://t.co/UCMiAAvr",
      "expanded_url" : "http://bit.ly/UIt09B",
      "display_url" : "bit.ly/UIt09B"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270848459626672128",
  "text" : "Gist It - lets you take a file from your GitHub repository and embed it into any webpage, just like a gist http://t.co/UCMiAAvr",
  "id" : 270848459626672128,
  "created_at" : "Tue Nov 20 11:17:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http://t.co/daGx1ime",
      "expanded_url" : "http://bit.ly/UIsVTn",
      "display_url" : "bit.ly/UIsVTn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270799001262112768",
  "text" : "Real Time Search - Social Mention http://t.co/daGx1ime",
  "id" : 270799001262112768,
  "created_at" : "Tue Nov 20 08:01:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "270679480765407233",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii  u on AIM brah?",
  "id" : 270679480765407233,
  "created_at" : "Tue Nov 20 00:06:31 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 3, 19 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http://t.co/nffErdQG",
      "expanded_url" : "http://jifasnif.com/",
      "display_url" : "jifasnif.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270674007077052416",
  "text" : "RT @jalbertbowdenii: JavaScript is Fun and so node.js is fun http://t.co/nffErdQG",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 40, 60 ],
        "url" : "http://t.co/nffErdQG",
        "expanded_url" : "http://jifasnif.com/",
        "display_url" : "jifasnif.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "270671082086879232",
    "text" : "JavaScript is Fun and so node.js is fun http://t.co/nffErdQG",
    "id" : 270671082086879232,
    "created_at" : "Mon Nov 19 23:33:09 +0000 2012",
    "user" : {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "protected" : false,
      "id_str" : "14465889",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3695791723/732f20561dd10d78f8cc88ee48d15ffa_normal.png",
      "id" : 14465889,
      "verified" : false
    }
  },
  "id" : 270674007077052416,
  "created_at" : "Mon Nov 19 23:44:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/yigSXqZn",
      "expanded_url" : "http://selfoss.aditu.de/",
      "display_url" : "selfoss.aditu.de"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270673785433235456",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii Haven't tried this but looks cool \u2192 http://t.co/yigSXqZn",
  "id" : 270673785433235456,
  "created_at" : "Mon Nov 19 23:43:53 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 82 ],
      "url" : "http://t.co/53EvqOuS",
      "expanded_url" : "http://bit.ly/XIMAbW",
      "display_url" : "bit.ly/XIMAbW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270666989285036034",
  "text" : "\"ScriptCam is a popular JQuery plugin to manipulate webcams.\" http://t.co/53EvqOuS",
  "id" : 270666989285036034,
  "created_at" : "Mon Nov 19 23:16:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/3X9SsEuT",
      "expanded_url" : "http://bit.ly/UIst7R",
      "display_url" : "bit.ly/UIst7R"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270666484752207872",
  "text" : "URL Shortening was a fucking awful idea http://t.co/3X9SsEuT",
  "id" : 270666484752207872,
  "created_at" : "Mon Nov 19 23:14:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 0, 11 ],
      "id_str" : "104462925",
      "id" : 104462925
    }, {
      "name" : "Dave DeSandro",
      "screen_name" : "desandro",
      "indices" : [ 64, 73 ],
      "id_str" : "1584511",
      "id" : 1584511
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 41, 48 ]
    }, {
      "text" : "pinterest",
      "indices" : [ 49, 59 ]
    } ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/bVbCRUsN",
      "expanded_url" : "http://kayschneider.github.com/pinbox/",
      "display_url" : "kayschneider.github.com/pinbox/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270655833623367680",
  "in_reply_to_user_id" : 104462925,
  "text" : "@techiejayk Pinbox: http://t.co/bVbCRUsN #jquery #pinterest cc: @desandro",
  "id" : 270655833623367680,
  "created_at" : "Mon Nov 19 22:32:33 +0000 2012",
  "in_reply_to_screen_name" : "techiejayk",
  "in_reply_to_user_id_str" : "104462925",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DDOS",
      "indices" : [ 64, 69 ]
    } ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/2UVLjMso",
      "expanded_url" : "http://bit.ly/UIrog6",
      "display_url" : "bit.ly/UIrog6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270655158805987328",
  "text" : "JS LOIC v0.1 http://t.co/2UVLjMso Low Orbit Ion Cannon in JS!!! #DDOS",
  "id" : 270655158805987328,
  "created_at" : "Mon Nov 19 22:29:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http://t.co/izE1rrEs",
      "expanded_url" : "http://bit.ly/MPwr0S",
      "display_url" : "bit.ly/MPwr0S"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270641063482503168",
  "text" : "\"kopimi (copyme), symbol showing that you want to be copied\" http://t.co/izE1rrEs",
  "id" : 270641063482503168,
  "created_at" : "Mon Nov 19 21:33:52 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stefan D\u00FChring",
      "screen_name" : "Autarc",
      "indices" : [ 3, 10 ],
      "id_str" : "52120068",
      "id" : 52120068
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cascadiajs",
      "indices" : [ 38, 49 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 104 ],
      "url" : "http://t.co/mn2djsgr",
      "expanded_url" : "http://cascadiajs.com/",
      "display_url" : "cascadiajs.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270639401699258368",
  "text" : "RT @Autarc: Just found the records of #cascadiajs - this will be a long night :D || http://t.co/mn2djsgr",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "cascadiajs",
        "indices" : [ 26, 37 ]
      } ],
      "urls" : [ {
        "indices" : [ 72, 92 ],
        "url" : "http://t.co/mn2djsgr",
        "expanded_url" : "http://cascadiajs.com/",
        "display_url" : "cascadiajs.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "270623860393668608",
    "text" : "Just found the records of #cascadiajs - this will be a long night :D || http://t.co/mn2djsgr",
    "id" : 270623860393668608,
    "created_at" : "Mon Nov 19 20:25:30 +0000 2012",
    "user" : {
      "name" : "Stefan D\u00FChring",
      "screen_name" : "Autarc",
      "protected" : false,
      "id_str" : "52120068",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2131565652/Stefan-PNG_normal.png",
      "id" : 52120068,
      "verified" : false
    }
  },
  "id" : 270639401699258368,
  "created_at" : "Mon Nov 19 21:27:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Todd Motto",
      "screen_name" : "toddmotto",
      "indices" : [ 3, 13 ],
      "id_str" : "222638700",
      "id" : 222638700
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 123 ],
      "url" : "http://t.co/WPP8e0qk",
      "expanded_url" : "http://www.toddmotto.com/jbar-plugin-the-jquery-call-to-action-bar",
      "display_url" : "toddmotto.com/jbar-plugin-th\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270639252432375812",
  "text" : "RT @toddmotto: jBar jQuery Plugin got a complete rewrite with tonnes of new options, have you updated? http://t.co/WPP8e0qk",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 88, 108 ],
        "url" : "http://t.co/WPP8e0qk",
        "expanded_url" : "http://www.toddmotto.com/jbar-plugin-the-jquery-call-to-action-bar",
        "display_url" : "toddmotto.com/jbar-plugin-th\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "270627143636099074",
    "text" : "jBar jQuery Plugin got a complete rewrite with tonnes of new options, have you updated? http://t.co/WPP8e0qk",
    "id" : 270627143636099074,
    "created_at" : "Mon Nov 19 20:38:33 +0000 2012",
    "user" : {
      "name" : "Todd Motto",
      "screen_name" : "toddmotto",
      "protected" : false,
      "id_str" : "222638700",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3719256882/e821009632ab2bdfcd0f748ca36174cb_normal.jpeg",
      "id" : 222638700,
      "verified" : false
    }
  },
  "id" : 270639252432375812,
  "created_at" : "Mon Nov 19 21:26:40 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "responsive",
      "indices" : [ 78, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 77 ],
      "url" : "http://t.co/ruDEam8z",
      "expanded_url" : "http://i.imgur.com/YkbaV.gif",
      "display_url" : "i.imgur.com/YkbaV.gif"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270638966577983489",
  "text" : "What the bulk of my internet surfing habit consists of \u2192 http://t.co/ruDEam8z #responsive",
  "id" : 270638966577983489,
  "created_at" : "Mon Nov 19 21:25:32 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/sFnrzgRV",
      "expanded_url" : "http://bit.ly/XIKfhe",
      "display_url" : "bit.ly/XIKfhe"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270624703696232448",
  "text" : "\"geek humor and other drivel\" http://t.co/sFnrzgRV",
  "id" : 270624703696232448,
  "created_at" : "Mon Nov 19 20:28:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 0, 11 ],
      "id_str" : "104462925",
      "id" : 104462925
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 53, 60 ]
    } ],
    "urls" : [ {
      "indices" : [ 71, 91 ],
      "url" : "http://t.co/vHkqzWUZ",
      "expanded_url" : "http://www.unheap.com/",
      "display_url" : "unheap.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270621831197761536",
  "in_reply_to_user_id" : 104462925,
  "text" : "@techiejayk Have you see this? Amazing collection of #jquery plugins \u2192 http://t.co/vHkqzWUZ",
  "id" : 270621831197761536,
  "created_at" : "Mon Nov 19 20:17:27 +0000 2012",
  "in_reply_to_screen_name" : "techiejayk",
  "in_reply_to_user_id_str" : "104462925",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 123 ],
      "url" : "http://t.co/SLahhJNu",
      "expanded_url" : "http://bit.ly/UIr7d3",
      "display_url" : "bit.ly/UIr7d3"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270610863256391680",
  "text" : "\"RetroShare is a Open Source cross-platform, private and secure decentralised communication platform.\" http://t.co/SLahhJNu",
  "id" : 270610863256391680,
  "created_at" : "Mon Nov 19 19:33:52 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/tbsIO999",
      "expanded_url" : "http://bit.ly/XIJEw1",
      "display_url" : "bit.ly/XIJEw1"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270595248898596864",
  "text" : "Mini Ajax - Highlighting Rich Experiences on the Web http://t.co/tbsIO999",
  "id" : 270595248898596864,
  "created_at" : "Mon Nov 19 18:31:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/2GY1NeCX",
      "expanded_url" : "http://bit.ly/XIJAwq",
      "display_url" : "bit.ly/XIJAwq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270592479135420417",
  "text" : "scripty2: for a more delicious web http://t.co/2GY1NeCX",
  "id" : 270592479135420417,
  "created_at" : "Mon Nov 19 18:20:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/PqJ4ulmd",
      "expanded_url" : "http://bit.ly/JM9s2U",
      "display_url" : "bit.ly/JM9s2U"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270591471986552832",
  "text" : "JavaScript Snippets http://t.co/PqJ4ulmd",
  "id" : 270591471986552832,
  "created_at" : "Mon Nov 19 18:16:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CSSDeck",
      "screen_name" : "cssdeck",
      "indices" : [ 51, 59 ],
      "id_str" : "417791200",
      "id" : 417791200
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 84 ],
      "url" : "https://t.co/lwzPgEZL",
      "expanded_url" : "https://flattr.com/thing/701630/Learn-HTML5-CSS3-JS",
      "display_url" : "flattr.com/thing/701630/L\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270586186056228865",
  "text" : "CSS Deck is awesome, so I sent a Flattr payment to @cssdeck -  https://t.co/lwzPgEZL",
  "id" : 270586186056228865,
  "created_at" : "Mon Nov 19 17:55:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/II5ugsMR",
      "expanded_url" : "http://bit.ly/XIJmp4",
      "display_url" : "bit.ly/XIJmp4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270579895749447680",
  "text" : "The Lightbox Clones Matrix http://t.co/II5ugsMR",
  "id" : 270579895749447680,
  "created_at" : "Mon Nov 19 17:30:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 99 ],
      "url" : "http://t.co/p7p4tzDr",
      "expanded_url" : "http://www.allmytweets.net/",
      "display_url" : "allmytweets.net"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270577534305972227",
  "text" : "All My Tweets now has the option where you can hide replies and hide retweets! http://t.co/p7p4tzDr",
  "id" : 270577534305972227,
  "created_at" : "Mon Nov 19 17:21:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/8sjuVc7w",
      "expanded_url" : "http://bit.ly/XIJjtn",
      "display_url" : "bit.ly/XIJjtn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270562273750286336",
  "text" : "Live Demo - jQuery EasyUI http://t.co/8sjuVc7w",
  "id" : 270562273750286336,
  "created_at" : "Mon Nov 19 16:20:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 34 ],
      "url" : "http://t.co/JIFDD4n2",
      "expanded_url" : "http://bit.ly/UIquAb",
      "display_url" : "bit.ly/UIquAb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270558881200099329",
  "text" : "jQuery Clip - http://t.co/JIFDD4n2 (JS Is real big in JPN)",
  "id" : 270558881200099329,
  "created_at" : "Mon Nov 19 16:07:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/KLoSOmxK",
      "expanded_url" : "http://bit.ly/UIqujF",
      "display_url" : "bit.ly/UIqujF"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270557490679271426",
  "text" : "smoke.js http://t.co/KLoSOmxK",
  "id" : 270557490679271426,
  "created_at" : "Mon Nov 19 16:01:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "notaddicted",
      "indices" : [ 53, 65 ]
    } ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/GChQ2rZc",
      "expanded_url" : "http://isharefil.es/L18N/o",
      "display_url" : "isharefil.es/L18N/o"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270547299619442688",
  "text" : "All the apps I use on Twitter \u2192 http://t.co/GChQ2rZc #notaddicted",
  "id" : 270547299619442688,
  "created_at" : "Mon Nov 19 15:21:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "prototype",
      "indices" : [ 28, 38 ]
    } ],
    "urls" : [ {
      "indices" : [ 7, 27 ],
      "url" : "http://t.co/i8uvyFI7",
      "expanded_url" : "http://bit.ly/XIJbKv",
      "display_url" : "bit.ly/XIJbKv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270543646464552961",
  "text" : "PWC OS http://t.co/i8uvyFI7 #prototype js",
  "id" : 270543646464552961,
  "created_at" : "Mon Nov 19 15:06:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AoS ",
      "screen_name" : "AnonOpsSweden",
      "indices" : [ 42, 56 ],
      "id_str" : "372922629",
      "id" : 372922629
    }, {
      "name" : "DBCOOPA",
      "screen_name" : "DBCOOPA",
      "indices" : [ 57, 65 ],
      "id_str" : "1411697929",
      "id" : 1411697929
    }, {
      "name" : "AnonymousZC",
      "screen_name" : "AnonymousZC",
      "indices" : [ 66, 78 ],
      "id_str" : "1075023535",
      "id" : 1075023535
    }, {
      "name" : "Anonymous",
      "screen_name" : "AnonIRC",
      "indices" : [ 79, 87 ],
      "id_str" : "336683669",
      "id" : 336683669
    }, {
      "name" : "Anonymous",
      "screen_name" : "opliberation1",
      "indices" : [ 88, 102 ],
      "id_str" : "365235743",
      "id" : 365235743
    }, {
      "name" : "GODMODE \u2606 ACTIVATED",
      "screen_name" : "VizFoSho",
      "indices" : [ 103, 112 ],
      "id_str" : "1429629487",
      "id" : 1429629487
    }, {
      "name" : "#OpIsrael",
      "screen_name" : "Op_Israel",
      "indices" : [ 113, 123 ],
      "id_str" : "476762447",
      "id" : 476762447
    }, {
      "name" : "PLF2012",
      "screen_name" : "PLF2012",
      "indices" : [ 124, 132 ],
      "id_str" : "1472069413",
      "id" : 1472069413
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "proxy",
      "indices" : [ 133, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/d9stF1qT",
      "expanded_url" : "http://isharefil.es/L0pk",
      "display_url" : "isharefil.es/L0pk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270539372401676289",
  "text" : "http://t.co/d9stF1qT Just helping out cc: @AnonOpsSweden @DBCOOPA @AnonymousZC @AnonIRC @OpLiberation1 @VizFoSho @Op_Israel @PLF2012 #proxy",
  "id" : 270539372401676289,
  "created_at" : "Mon Nov 19 14:49:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 91 ],
      "url" : "http://t.co/hfUfPX8K",
      "expanded_url" : "http://pastebin.com/uCRQcRUq",
      "display_url" : "pastebin.com/uCRQcRUq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270537576576872448",
  "text" : "\"Please Read our Piracy Policy\" should be at the end of every website. http://t.co/hfUfPX8K",
  "id" : 270537576576872448,
  "created_at" : "Mon Nov 19 14:42:39 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/bw9PBAso",
      "expanded_url" : "http://bit.ly/XIJ01M",
      "display_url" : "bit.ly/XIJ01M"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270530908757569536",
  "text" : "nanoScroller.js http://t.co/bw9PBAso",
  "id" : 270530908757569536,
  "created_at" : "Mon Nov 19 14:16:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/k4pES1bz",
      "expanded_url" : "http://bit.ly/XIIYa9",
      "display_url" : "bit.ly/XIIYa9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270513288662630400",
  "text" : "Repos.io - Search and manage code repositories http://t.co/k4pES1bz",
  "id" : 270513288662630400,
  "created_at" : "Mon Nov 19 13:06:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 94 ],
      "url" : "http://t.co/keOPi99s",
      "expanded_url" : "http://bit.ly/XIIMYu",
      "display_url" : "bit.ly/XIIMYu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270500957241098240",
  "text" : "jQuery UI Selectmenu: An ARIA-Accessible Plugin for Styling a Custom HTML http://t.co/keOPi99s",
  "id" : 270500957241098240,
  "created_at" : "Mon Nov 19 12:17:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/CeqtK6gd",
      "expanded_url" : "http://bit.ly/XCfkmF",
      "display_url" : "bit.ly/XCfkmF"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270497181427978241",
  "text" : "Lots of links \u00BB webnik.dk http://t.co/CeqtK6gd",
  "id" : 270497181427978241,
  "created_at" : "Mon Nov 19 12:02:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Phil Leggetter",
      "screen_name" : "leggetter",
      "indices" : [ 0, 10 ],
      "id_str" : "14455530",
      "id" : 14455530
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "270492082261606400",
  "geo" : {
  },
  "id_str" : "270495362882621441",
  "in_reply_to_user_id" : 14455530,
  "text" : "@leggetter Bit of a 0day this one. Despite the disclaimer, this can be used for to do CSRF and other loveliness like that. Fun times ;)",
  "id" : 270495362882621441,
  "in_reply_to_status_id" : 270492082261606400,
  "created_at" : "Mon Nov 19 11:54:54 +0000 2012",
  "in_reply_to_screen_name" : "leggetter",
  "in_reply_to_user_id_str" : "14455530",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Phil Leggetter",
      "screen_name" : "leggetter",
      "indices" : [ 3, 13 ],
      "id_str" : "14455530",
      "id" : 14455530
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/X5W7sEKG",
      "expanded_url" : "http://j.mp/S5uQ7n",
      "display_url" : "j.mp/S5uQ7n"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270494376617209856",
  "text" : "RT @leggetter: CORS Proxy http://t.co/X5W7sEKG Nice!",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 11, 31 ],
        "url" : "http://t.co/X5W7sEKG",
        "expanded_url" : "http://j.mp/S5uQ7n",
        "display_url" : "j.mp/S5uQ7n"
      } ]
    },
    "geo" : {
    },
    "id_str" : "270492082261606400",
    "text" : "CORS Proxy http://t.co/X5W7sEKG Nice!",
    "id" : 270492082261606400,
    "created_at" : "Mon Nov 19 11:41:52 +0000 2012",
    "user" : {
      "name" : "Phil Leggetter",
      "screen_name" : "leggetter",
      "protected" : false,
      "id_str" : "14455530",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3603838368/b60e200d7991f922efdcdca12010fdf2_normal.png",
      "id" : 14455530,
      "verified" : false
    }
  },
  "id" : 270494376617209856,
  "created_at" : "Mon Nov 19 11:50:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/Joq04fAV",
      "expanded_url" : "http://bit.ly/UF3aUb",
      "display_url" : "bit.ly/UF3aUb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270489882386890752",
  "text" : "Get Fake Credit Card Numbers for 'Testing' Purposes http://t.co/Joq04fAV",
  "id" : 270489882386890752,
  "created_at" : "Mon Nov 19 11:33:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Moor",
      "screen_name" : "tommoor",
      "indices" : [ 80, 88 ],
      "id_str" : "18775095",
      "id" : 18775095
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/xexPtGaD",
      "expanded_url" : "http://bit.ly/XysdhL",
      "display_url" : "bit.ly/XysdhL"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270488155277369344",
  "text" : "Crumble - Quirky jQuery feature tours for your website http://t.co/xexPtGaD via @tommoor",
  "id" : 270488155277369344,
  "created_at" : "Mon Nov 19 11:26:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 38 ],
      "url" : "http://t.co/Z3Spv16B",
      "expanded_url" : "http://bit.ly/XqThPN",
      "display_url" : "bit.ly/XqThPN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270486108205375489",
  "text" : "jQuery slimScroll http://t.co/Z3Spv16B",
  "id" : 270486108205375489,
  "created_at" : "Mon Nov 19 11:18:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/1Ye0zbgh",
      "expanded_url" : "http://bit.ly/ItMgm7",
      "display_url" : "bit.ly/ItMgm7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270436528302469120",
  "text" : "Shortmail http://t.co/1Ye0zbgh",
  "id" : 270436528302469120,
  "created_at" : "Mon Nov 19 08:01:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 34 ],
      "url" : "http://t.co/W2ThHPOR",
      "expanded_url" : "http://bit.ly/XqT8Mo",
      "display_url" : "bit.ly/XqT8Mo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270304636097933312",
  "text" : "The \u263A Licence http://t.co/W2ThHPOR",
  "id" : 270304636097933312,
  "created_at" : "Sun Nov 18 23:17:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/kDpedfvu",
      "expanded_url" : "http://bit.ly/UA0TcL",
      "display_url" : "bit.ly/UA0TcL"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270304134467567617",
  "text" : "Mini Social App http://t.co/kDpedfvu",
  "id" : 270304134467567617,
  "created_at" : "Sun Nov 18 23:15:02 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/xnFwVVol",
      "expanded_url" : "http://bit.ly/XqT0wv",
      "display_url" : "bit.ly/XqT0wv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270292807606996992",
  "text" : "Demystifying JSCrush http://t.co/xnFwVVol",
  "id" : 270292807606996992,
  "created_at" : "Sun Nov 18 22:30:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 25 ],
      "url" : "http://t.co/hvjKoKYb",
      "expanded_url" : "http://bit.ly/XqSYod",
      "display_url" : "bit.ly/XqSYod"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270278711788597248",
  "text" : "Bump http://t.co/hvjKoKYb",
  "id" : 270278711788597248,
  "created_at" : "Sun Nov 18 21:34:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/HXYqFsLa",
      "expanded_url" : "http://bit.ly/XqSXAO",
      "display_url" : "bit.ly/XqSXAO"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270262364023169025",
  "text" : "cssarrowplease http://t.co/HXYqFsLa",
  "id" : 270262364023169025,
  "created_at" : "Sun Nov 18 20:29:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/KZizko4l",
      "expanded_url" : "http://bit.ly/XqSUET",
      "display_url" : "bit.ly/XqSUET"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270248509058465792",
  "text" : "Unicode Art http://t.co/KZizko4l",
  "id" : 270248509058465792,
  "created_at" : "Sun Nov 18 19:34:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http://t.co/jsCBWkNl",
      "expanded_url" : "http://bit.ly/XqSTRi",
      "display_url" : "bit.ly/XqSTRi"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270232904708263936",
  "text" : "How to get more likes on Facebook http://t.co/jsCBWkNl",
  "id" : 270232904708263936,
  "created_at" : "Sun Nov 18 18:31:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/AN3NNUgD",
      "expanded_url" : "http://bit.ly/XqSOx1",
      "display_url" : "bit.ly/XqSOx1"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270230131040198656",
  "text" : "Optimize Google Sharing http://t.co/AN3NNUgD",
  "id" : 270230131040198656,
  "created_at" : "Sun Nov 18 18:20:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 42, 49 ]
    } ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/lexY3gpM",
      "expanded_url" : "http://bit.ly/PDG1Ry",
      "display_url" : "bit.ly/PDG1Ry"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270229125980106753",
  "text" : "The Basics of jQuery http://t.co/lexY3gpM #jquery",
  "id" : 270229125980106753,
  "created_at" : "Sun Nov 18 18:16:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/8c48SSTQ",
      "expanded_url" : "http://bit.ly/PDIfAg",
      "display_url" : "bit.ly/PDIfAg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270217672279613440",
  "text" : "Skrivr - Write freely. Publish beautifully. http://t.co/8c48SSTQ",
  "id" : 270217672279613440,
  "created_at" : "Sun Nov 18 17:31:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/9vcx9Pqz",
      "expanded_url" : "http://bit.ly/NdOW9w",
      "display_url" : "bit.ly/NdOW9w"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270199927727087618",
  "text" : "noUiSlider - jQuery Range Slider http://t.co/9vcx9Pqz",
  "id" : 270199927727087618,
  "created_at" : "Sun Nov 18 16:20:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/SaPWnd3M",
      "expanded_url" : "http://bit.ly/UA0zuA",
      "display_url" : "bit.ly/UA0zuA"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270196406327840769",
  "text" : "jQuery notification Plug in http://t.co/SaPWnd3M",
  "id" : 270196406327840769,
  "created_at" : "Sun Nov 18 16:06:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/RIg624na",
      "expanded_url" : "http://bit.ly/XqSa2M",
      "display_url" : "bit.ly/XqSa2M"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270195021855203328",
  "text" : "Three Point Line | Field Study http://t.co/RIg624na",
  "id" : 270195021855203328,
  "created_at" : "Sun Nov 18 16:01:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter van der Zee",
      "screen_name" : "kuvos",
      "indices" : [ 0, 6 ],
      "id_str" : "31151313",
      "id" : 31151313
    }, {
      "name" : "jsgoodies",
      "screen_name" : "jsgoodies",
      "indices" : [ 7, 17 ],
      "id_str" : "222057071",
      "id" : 222057071
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rss",
      "indices" : [ 111, 115 ]
    } ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http://t.co/hvRLgYDQ",
      "expanded_url" : "http://iwantaneff.in/bin/view/77ef0d21",
      "display_url" : "iwantaneff.in/bin/view/77ef0\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270192513195839488",
  "in_reply_to_user_id" : 31151313,
  "text" : "@kuvos @jsgoodies I am currently feasting on all these RSS feeds: http://t.co/hvRLgYDQ That's an almighty list #rss",
  "id" : 270192513195839488,
  "created_at" : "Sun Nov 18 15:51:29 +0000 2012",
  "in_reply_to_screen_name" : "kuvos",
  "in_reply_to_user_id_str" : "31151313",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CreativityKills, LLC",
      "screen_name" : "itsCatchphrase",
      "indices" : [ 3, 18 ],
      "id_str" : "572195083",
      "id" : 572195083
    }, {
      "name" : "SlanguageTranslation",
      "screen_name" : "Slanguage",
      "indices" : [ 21, 31 ],
      "id_str" : "540981721",
      "id" : 540981721
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "270185996933537793",
  "text" : "RT @itsCatchphrase: \"@Slanguage: \"Na mean\" = Do you comprehend my logic and reasoning.\"",
  "retweeted_status" : {
    "source" : "<a href=\"http://blackberry.com/twitter\" rel=\"nofollow\">Twitter for BlackBerry\u00AE</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SlanguageTranslation",
        "screen_name" : "Slanguage",
        "indices" : [ 1, 11 ],
        "id_str" : "540981721",
        "id" : 540981721
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "270185137919119360",
    "text" : "\"@Slanguage: \"Na mean\" = Do you comprehend my logic and reasoning.\"",
    "id" : 270185137919119360,
    "created_at" : "Sun Nov 18 15:22:11 +0000 2012",
    "user" : {
      "name" : "SuperNerd!",
      "screen_name" : "CreativityKills",
      "protected" : false,
      "id_str" : "148703997",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000242896225/03a73e8d7be8c789ed46fb1315ff377b_normal.jpeg",
      "id" : 148703997,
      "verified" : false
    }
  },
  "id" : 270185996933537793,
  "created_at" : "Sun Nov 18 15:25:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/NTy9awGz",
      "expanded_url" : "http://bit.ly/UA0y9Z",
      "display_url" : "bit.ly/UA0y9Z"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270181303264104448",
  "text" : "Html5 Icon http://t.co/NTy9awGz",
  "id" : 270181303264104448,
  "created_at" : "Sun Nov 18 15:06:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "integralist",
      "screen_name" : "integralist",
      "indices" : [ 0, 12 ],
      "id_str" : "18293449",
      "id" : 18293449
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "protip",
      "indices" : [ 129, 136 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "270167679392960513",
  "geo" : {
  },
  "id_str" : "270179231982579713",
  "in_reply_to_user_id" : 18293449,
  "text" : "@integralist Also, you should add people to a list, and read that instead. Then it's okay to let your following get out of hand. #protip",
  "id" : 270179231982579713,
  "in_reply_to_status_id" : 270167679392960513,
  "created_at" : "Sun Nov 18 14:58:43 +0000 2012",
  "in_reply_to_screen_name" : "integralist",
  "in_reply_to_user_id_str" : "18293449",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "integralist",
      "screen_name" : "integralist",
      "indices" : [ 0, 12 ],
      "id_str" : "18293449",
      "id" : 18293449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "270167679392960513",
  "geo" : {
  },
  "id_str" : "270179014268817408",
  "in_reply_to_user_id" : 18293449,
  "text" : "@integralist Well that's a relief. I thought it was something I said ;)",
  "id" : 270179014268817408,
  "in_reply_to_status_id" : 270167679392960513,
  "created_at" : "Sun Nov 18 14:57:51 +0000 2012",
  "in_reply_to_screen_name" : "integralist",
  "in_reply_to_user_id_str" : "18293449",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/sHo1qwqf",
      "expanded_url" : "http://bit.ly/UA0xD4",
      "display_url" : "bit.ly/UA0xD4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270168452105392128",
  "text" : "Why I Don't Sign Up To Your \"Coming Soon\" Page http://t.co/sHo1qwqf",
  "id" : 270168452105392128,
  "created_at" : "Sun Nov 18 14:15:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Parthiban.T",
      "screen_name" : "deepan2k5",
      "indices" : [ 3, 13 ],
      "id_str" : "52820635",
      "id" : 52820635
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http://t.co/xU21o0XB",
      "expanded_url" : "http://iwantaneff.in/repo/",
      "display_url" : "iwantaneff.in/repo/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270156862509432832",
  "text" : "RT @deepan2k5: @_higg Hi... U r site http://t.co/xU21o0XB is extremely well done works collection....  Keep Going...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 22, 42 ],
        "url" : "http://t.co/xU21o0XB",
        "expanded_url" : "http://iwantaneff.in/repo/",
        "display_url" : "iwantaneff.in/repo/"
      } ]
    },
    "geo" : {
    },
    "id_str" : "270030654354374657",
    "in_reply_to_user_id" : 468853739,
    "text" : "@_higg Hi... U r site http://t.co/xU21o0XB is extremely well done works collection....  Keep Going...",
    "id" : 270030654354374657,
    "created_at" : "Sun Nov 18 05:08:19 +0000 2012",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "Parthiban.T",
      "screen_name" : "deepan2k5",
      "protected" : true,
      "id_str" : "52820635",
      "profile_image_url_https" : "https://si0.twimg.com/sticky/default_profile_images/default_profile_0_normal.png",
      "id" : 52820635,
      "verified" : false
    }
  },
  "id" : 270156862509432832,
  "created_at" : "Sun Nov 18 13:29:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/aFMSIxA1",
      "expanded_url" : "http://bit.ly/XqS4bd",
      "display_url" : "bit.ly/XqS4bd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270150833499426816",
  "text" : "HTML5 Context Menus Example http://t.co/aFMSIxA1",
  "id" : 270150833499426816,
  "created_at" : "Sun Nov 18 13:05:52 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/i7fS5gDo",
      "expanded_url" : "http://bit.ly/LcSnSx",
      "display_url" : "bit.ly/LcSnSx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270138501843005440",
  "text" : "bootstrap-wysihtml5 http://t.co/i7fS5gDo",
  "id" : 270138501843005440,
  "created_at" : "Sun Nov 18 12:16:52 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/6g7i0EUP",
      "expanded_url" : "http://bit.ly/XqRWbN",
      "display_url" : "bit.ly/XqRWbN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270134724817727488",
  "text" : "Apple CSS3 Menu! http://t.co/6g7i0EUP",
  "id" : 270134724817727488,
  "created_at" : "Sun Nov 18 12:01:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/hfSx55lM",
      "expanded_url" : "http://bit.ly/UA0szc",
      "display_url" : "bit.ly/UA0szc"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270127425470472192",
  "text" : "Olin Shivers http://t.co/hfSx55lM",
  "id" : 270127425470472192,
  "created_at" : "Sun Nov 18 11:32:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 77 ],
      "url" : "http://t.co/ryq1fnz5",
      "expanded_url" : "http://yhoo.it/XqRNVW",
      "display_url" : "yhoo.it/XqRNVW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270125663686623232",
  "text" : "I would like to eat Panda meat. How do I get panda meat? http://t.co/ryq1fnz5",
  "id" : 270125663686623232,
  "created_at" : "Sun Nov 18 11:25:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CHROME",
      "indices" : [ 51, 58 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/4FEyKLVm",
      "expanded_url" : "http://bit.ly/XqRM4e",
      "display_url" : "bit.ly/XqRM4e"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270123650231971841",
  "text" : "100% Geek :: Goats Teleported http://t.co/4FEyKLVm #CHROME",
  "id" : 270123650231971841,
  "created_at" : "Sun Nov 18 11:17:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/TFXmp2tq",
      "expanded_url" : "http://bit.ly/XqRIBC",
      "display_url" : "bit.ly/XqRIBC"
    } ]
  },
  "geo" : {
  },
  "id_str" : "270074193780023296",
  "text" : "Sharrre - A plugin for sharing buttons http://t.co/TFXmp2tq",
  "id" : 270074193780023296,
  "created_at" : "Sun Nov 18 08:01:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/WVyIlcQA",
      "expanded_url" : "http://sla.ckers.org",
      "display_url" : "sla.ckers.org"
    }, {
      "indices" : [ 67, 87 ],
      "url" : "http://t.co/pWw5RXzl",
      "expanded_url" : "http://bit.ly/UA0o2n",
      "display_url" : "bit.ly/UA0o2n"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269942184390819840",
  "text" : "http://t.co/WVyIlcQA web application security forum :: Obfuscation http://t.co/pWw5RXzl",
  "id" : 269942184390819840,
  "created_at" : "Sat Nov 17 23:16:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/ApEwCuxt",
      "expanded_url" : "http://bit.ly/UA0lUg",
      "display_url" : "bit.ly/UA0lUg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269941805942964224",
  "text" : "Demo::jQuery Spin Button Plugin: Smart Spin http://t.co/ApEwCuxt",
  "id" : 269941805942964224,
  "created_at" : "Sat Nov 17 23:15:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 78 ],
      "url" : "http://t.co/9dRa5zlz",
      "expanded_url" : "http://bit.ly/UA0nvr",
      "display_url" : "bit.ly/UA0nvr"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269930355975409665",
  "text" : "PageSpeed Service - PageSpeed Service \u2014 Google Developers http://t.co/9dRa5zlz",
  "id" : 269930355975409665,
  "created_at" : "Sat Nov 17 22:29:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "integralist",
      "screen_name" : "integralist",
      "indices" : [ 3, 15 ],
      "id_str" : "18293449",
      "id" : 18293449
    }, {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 41, 49 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http://t.co/EmvVmv75",
      "expanded_url" : "http://coding.smashingmagazine.com/2012/11/13/the-vanilla-web-diet/",
      "display_url" : "coding.smashingmagazine.com/2012/11/13/the\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269929147088916480",
  "text" : "RT @integralist: The Vanilla Web Diet by @codepo8  http://t.co/EmvVmv75",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/read-it-later-pro/id309601447?mt=8&uo=4\" rel=\"nofollow\">Pocket for iOS</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Christian Heilmann ",
        "screen_name" : "codepo8",
        "indices" : [ 24, 32 ],
        "id_str" : "13567",
        "id" : 13567
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 34, 54 ],
        "url" : "http://t.co/EmvVmv75",
        "expanded_url" : "http://coding.smashingmagazine.com/2012/11/13/the-vanilla-web-diet/",
        "display_url" : "coding.smashingmagazine.com/2012/11/13/the\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "269926281284575233",
    "text" : "The Vanilla Web Diet by @codepo8  http://t.co/EmvVmv75",
    "id" : 269926281284575233,
    "created_at" : "Sat Nov 17 22:13:35 +0000 2012",
    "user" : {
      "name" : "integralist",
      "screen_name" : "integralist",
      "protected" : false,
      "id_str" : "18293449",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000182715022/364aef027a4b26d77bcad7ab0af3316b_normal.jpeg",
      "id" : 18293449,
      "verified" : false
    }
  },
  "id" : 269929147088916480,
  "created_at" : "Sat Nov 17 22:24:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    }, {
      "name" : "Name.com",
      "screen_name" : "namedotcom",
      "indices" : [ 16, 27 ],
      "id_str" : "14178648",
      "id" : 14178648
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "justsayin",
      "indices" : [ 56, 66 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "269920129603739648",
  "geo" : {
  },
  "id_str" : "269921304772239360",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog @namedotcom OVH are still far superior. #justsayin",
  "id" : 269921304772239360,
  "in_reply_to_status_id" : 269920129603739648,
  "created_at" : "Sat Nov 17 21:53:48 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/vee4cL2w",
      "expanded_url" : "http://bit.ly/XqRGcM",
      "display_url" : "bit.ly/XqRGcM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269916264183517184",
  "text" : "Zelda 25th Anniversary Amazing Artwork Making of Ag+ http://t.co/vee4cL2w",
  "id" : 269916264183517184,
  "created_at" : "Sat Nov 17 21:33:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/V3dRjw67",
      "expanded_url" : "http://bit.ly/I5SR9D",
      "display_url" : "bit.ly/I5SR9D"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269899899775111168",
  "text" : "OMFGDOGS http://t.co/V3dRjw67",
  "id" : 269899899775111168,
  "created_at" : "Sat Nov 17 20:28:45 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fortune",
      "indices" : [ 88, 96 ]
    } ],
    "urls" : [ {
      "indices" : [ 108, 128 ],
      "url" : "http://t.co/dUt2qblf",
      "expanded_url" : "http://fortunappnet.appspot.com",
      "display_url" : "fortunappnet.appspot.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269893986309771264",
  "text" : "You will pay for your sins.  If you have already paid, please disregard\nthis message. //#fortune powered by http://t.co/dUt2qblf",
  "id" : 269893986309771264,
  "created_at" : "Sat Nov 17 20:05:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ADN",
      "indices" : [ 5, 9 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "269889472336896000",
  "text" : "Fuck #ADN in the ass if they want $100.00 just to have access to their API. Lame as fuck. Just let me code without paying a fucking tonne :(",
  "id" : 269889472336896000,
  "created_at" : "Sat Nov 17 19:47:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 38 ],
      "url" : "http://t.co/6XGdScR6",
      "expanded_url" : "http://bit.ly/XqRwlG",
      "display_url" : "bit.ly/XqRwlG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269886186024218624",
  "text" : "Home :: Emojicons http://t.co/6XGdScR6",
  "id" : 269886186024218624,
  "created_at" : "Sat Nov 17 19:34:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "dailyjs",
      "screen_name" : "dailyjs",
      "indices" : [ 51, 59 ],
      "id_str" : "87709774",
      "id" : 87709774
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/NgqzeWj2",
      "expanded_url" : "http://flattr.com/t/456257",
      "display_url" : "flattr.com/t/456257"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269878708981100545",
  "text" : "I just flattred \"DailyJS\" http://t.co/NgqzeWj2 cc: @dailyjs",
  "id" : 269878708981100545,
  "created_at" : "Sat Nov 17 19:04:32 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/EfkQpGzL",
      "expanded_url" : "http://bit.ly/PnL0VI",
      "display_url" : "bit.ly/PnL0VI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269870577496510465",
  "text" : "Social URL Creator http://t.co/EfkQpGzL",
  "id" : 269870577496510465,
  "created_at" : "Sat Nov 17 18:32:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/CTgMBg0P",
      "expanded_url" : "http://bit.ly/XqRquu",
      "display_url" : "bit.ly/XqRquu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269867810757427200",
  "text" : "Little Big Details http://t.co/CTgMBg0P",
  "id" : 269867810757427200,
  "created_at" : "Sat Nov 17 18:21:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/MAIMyFx0",
      "expanded_url" : "http://bit.ly/XqRquk",
      "display_url" : "bit.ly/XqRquk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269866678874152960",
  "text" : "Pears http://t.co/MAIMyFx0",
  "id" : 269866678874152960,
  "created_at" : "Sat Nov 17 18:16:44 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Lane",
      "screen_name" : "mlane",
      "indices" : [ 3, 9 ],
      "id_str" : "12304722",
      "id" : 12304722
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/c2thotoJ",
      "expanded_url" : "http://mln.im/UIOCsb",
      "display_url" : "mln.im/UIOCsb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269857998845931521",
  "text" : "RT @mlane: 23 Ancient Websites That Are Still Alive http://t.co/c2thotoJ",
  "id" : 269857998845931521,
  "created_at" : "Sat Nov 17 17:42:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/DZsuWITM",
      "expanded_url" : "http://bit.ly/UA0dnE",
      "display_url" : "bit.ly/UA0dnE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269855227304357889",
  "text" : "Turn.js: Make a flipbook with HTML5 http://t.co/DZsuWITM",
  "id" : 269855227304357889,
  "created_at" : "Sat Nov 17 17:31:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "269854366821912576",
  "text" : "Super tip of the day. Put .qrcode at the end of a bitly link to get a PNG QRCode of the bitly link!",
  "id" : 269854366821912576,
  "created_at" : "Sat Nov 17 17:27:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/7yCq1XFZ",
      "expanded_url" : "http://bit.ly/UA0ebl",
      "display_url" : "bit.ly/UA0ebl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269837607620456448",
  "text" : "Dave DeSandro (desandro) on Twitter http://t.co/7yCq1XFZ",
  "id" : 269837607620456448,
  "created_at" : "Sat Nov 17 16:21:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 89 ],
      "url" : "http://t.co/d7yI9gcN",
      "expanded_url" : "http://bit.ly/UA0ebk",
      "display_url" : "bit.ly/UA0ebk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269834084275060738",
  "text" : "TinyNav.js \u00B7 Convert navigation to a select dropdown on small screen http://t.co/d7yI9gcN",
  "id" : 269834084275060738,
  "created_at" : "Sat Nov 17 16:07:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/I6R2ICn6",
      "expanded_url" : "http://on.mash.to/INK8dE",
      "display_url" : "on.mash.to/INK8dE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269832575504232448",
  "text" : "20 Twitter Badges to Show Off Your Tweets http://t.co/I6R2ICn6",
  "id" : 269832575504232448,
  "created_at" : "Sat Nov 17 16:01:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/qzqqDMjj",
      "expanded_url" : "http://bit.ly/UA07wf",
      "display_url" : "bit.ly/UA07wf"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269818984562581504",
  "text" : "Code Tidy - Pastebin http://t.co/qzqqDMjj",
  "id" : 269818984562581504,
  "created_at" : "Sat Nov 17 15:07:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/c8IPDiEg",
      "expanded_url" : "http://bit.ly/XqReLM",
      "display_url" : "bit.ly/XqReLM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269806210511941633",
  "text" : "MooTools Scrollbars with Arrows http://t.co/c8IPDiEg",
  "id" : 269806210511941633,
  "created_at" : "Sat Nov 17 14:16:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 37, 44 ]
    } ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/3PrgusDt",
      "expanded_url" : "http://pinboard.in/u:jonnott/t:webdev-people/",
      "display_url" : "pinboard.in/u:jonnott/t:we\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269794250143723521",
  "text" : "WebDev People \u2192 http://t.co/3PrgusDt #webdev Nice roundup of all the main players in our little 'scene'",
  "id" : 269794250143723521,
  "created_at" : "Sat Nov 17 13:28:56 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/eJH0kecH",
      "expanded_url" : "http://bit.ly/UA07wl",
      "display_url" : "bit.ly/UA07wl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269788586637934592",
  "text" : "mooSocialize - ajax based social bookmark widget http://t.co/eJH0kecH",
  "id" : 269788586637934592,
  "created_at" : "Sat Nov 17 13:06:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/hI9S6g6j",
      "expanded_url" : "http://bit.ly/XqRf27",
      "display_url" : "bit.ly/XqRf27"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269776124672892928",
  "text" : "B3TA : WE LOVE THE WEB http://t.co/hI9S6g6j",
  "id" : 269776124672892928,
  "created_at" : "Sat Nov 17 12:16:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http://t.co/jDlZg0yA",
      "expanded_url" : "http://bit.ly/XqRhar",
      "display_url" : "bit.ly/XqRhar"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269772354903740416",
  "text" : "Stylized Graffiti Rendering | kode80 http://t.co/jDlZg0yA",
  "id" : 269772354903740416,
  "created_at" : "Sat Nov 17 12:01:56 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/YsY5FDUl",
      "expanded_url" : "http://bit.ly/JM9gk9",
      "display_url" : "bit.ly/JM9gk9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269765048677847040",
  "text" : "Destroydrop \u00BB Javascripts \u00BB Tree http://t.co/YsY5FDUl",
  "id" : 269765048677847040,
  "created_at" : "Sat Nov 17 11:32:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/YVKHNQfx",
      "expanded_url" : "http://bit.ly/UA07MM",
      "display_url" : "bit.ly/UA07MM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269763290006167552",
  "text" : "Fullsize : A New IMG Tag Attribute http://t.co/YVKHNQfx",
  "id" : 269763290006167552,
  "created_at" : "Sat Nov 17 11:25:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/hpUivoMG",
      "expanded_url" : "http://bit.ly/JM9rMq",
      "display_url" : "bit.ly/JM9rMq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269761364745150464",
  "text" : "bytefx :: simple effects in few bytes http://t.co/hpUivoMG",
  "id" : 269761364745150464,
  "created_at" : "Sat Nov 17 11:18:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 38 ],
      "url" : "http://t.co/d9wUUie9",
      "expanded_url" : "http://bit.ly/InLV9h",
      "display_url" : "bit.ly/InLV9h"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269711778739523585",
  "text" : "Web UI Components http://t.co/d9wUUie9",
  "id" : 269711778739523585,
  "created_at" : "Sat Nov 17 08:01:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/ZPX9QPH3",
      "expanded_url" : "http://mzl.la/UA097p",
      "display_url" : "mzl.la/UA097p"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269579843652427776",
  "text" : "document.createComment - MDN http://t.co/ZPX9QPH3 LOLWUT",
  "id" : 269579843652427776,
  "created_at" : "Fri Nov 16 23:16:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 78 ],
      "url" : "http://t.co/7O8kBEgh",
      "expanded_url" : "http://bit.ly/XqR1Z4",
      "display_url" : "bit.ly/XqR1Z4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269579337152462850",
  "text" : "Subtle Patterns | Free textures for your next web project http://t.co/7O8kBEgh",
  "id" : 269579337152462850,
  "created_at" : "Fri Nov 16 23:14:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/wFeL3ASo",
      "expanded_url" : "http://bit.ly/JM9bgb",
      "display_url" : "bit.ly/JM9bgb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269568021004173312",
  "text" : "Ninja UI http://t.co/wFeL3ASo",
  "id" : 269568021004173312,
  "created_at" : "Fri Nov 16 22:29:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/jaXtNAhg",
      "expanded_url" : "http://bit.ly/UA0181",
      "display_url" : "bit.ly/UA0181"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269553913219403776",
  "text" : "The Ultimate jQuery List http://t.co/jaXtNAhg",
  "id" : 269553913219403776,
  "created_at" : "Fri Nov 16 21:33:55 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/mKUYlLhp",
      "expanded_url" : "http://bit.ly/XqR2Ml",
      "display_url" : "bit.ly/XqR2Ml"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269537550580973568",
  "text" : "Memu Demo http://t.co/mKUYlLhp",
  "id" : 269537550580973568,
  "created_at" : "Fri Nov 16 20:28:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http://t.co/YNikFByZ",
      "expanded_url" : "http://bit.ly/XqQZAr",
      "display_url" : "bit.ly/XqQZAr"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269523706903146496",
  "text" : "Snippet :: jQuery Syntax Highlighter http://t.co/YNikFByZ",
  "id" : 269523706903146496,
  "created_at" : "Fri Nov 16 19:33:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "XSS",
      "indices" : [ 12, 16 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "269521698045452289",
  "text" : "Lots of new #XSS vulns found in IE10. Wooot!",
  "id" : 269521698045452289,
  "created_at" : "Fri Nov 16 19:25:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 38 ],
      "url" : "http://t.co/atdamAet",
      "expanded_url" : "http://bit.ly/InLJXI",
      "display_url" : "bit.ly/InLJXI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269508098090221570",
  "text" : "JavaScript Garden http://t.co/atdamAet",
  "id" : 269508098090221570,
  "created_at" : "Fri Nov 16 18:31:52 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/Tr5HYV5f",
      "expanded_url" : "http://bit.ly/INKSzi",
      "display_url" : "bit.ly/INKSzi"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269505329757298688",
  "text" : "ObfuscateJS - A Free Javascript Obfuscator http://t.co/Tr5HYV5f",
  "id" : 269505329757298688,
  "created_at" : "Fri Nov 16 18:20:52 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/J3LozYqJ",
      "expanded_url" : "http://bit.ly/XqQZQQ",
      "display_url" : "bit.ly/XqQZQQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269504321547280384",
  "text" : "Welcome to Square Bracket - Open JS Grid v2 http://t.co/J3LozYqJ",
  "id" : 269504321547280384,
  "created_at" : "Fri Nov 16 18:16:52 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/ZAhhvFYZ",
      "expanded_url" : "http://bit.ly/UA003L",
      "display_url" : "bit.ly/UA003L"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269492868681453568",
  "text" : "Lettering.JS http://t.co/ZAhhvFYZ",
  "id" : 269492868681453568,
  "created_at" : "Fri Nov 16 17:31:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nils Fischer",
      "screen_name" : "schwadroneur",
      "indices" : [ 16, 29 ],
      "id_str" : "45612491",
      "id" : 45612491
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/V4FY6v2q",
      "expanded_url" : "http://flattr.com/t/394517",
      "display_url" : "flattr.com/t/394517"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269492754743181314",
  "text" : "I just flattred @schwadroneur http://t.co/V4FY6v2q",
  "id" : 269492754743181314,
  "created_at" : "Fri Nov 16 17:30:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andy Li",
      "screen_name" : "andy_li",
      "indices" : [ 61, 69 ],
      "id_str" : "17513591",
      "id" : 17513591
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 94 ],
      "url" : "https://t.co/7mR6SsLS",
      "expanded_url" : "https://flattr.com/thing/205054/andyli-on-Flattr",
      "display_url" : "flattr.com/thing/205054/a\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269485961291902976",
  "text" : "\"andyli on Flattr\" is awesome, so I sent a Flattr payment to @andy_li -  https://t.co/7mR6SsLS",
  "id" : 269485961291902976,
  "created_at" : "Fri Nov 16 17:03:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/KX6e9BRU",
      "expanded_url" : "http://isharefil.es/Kwxs",
      "display_url" : "isharefil.es/Kwxs"
    }, {
      "indices" : [ 119, 140 ],
      "url" : "https://t.co/r6A4LCqL",
      "expanded_url" : "https://alpha.app.net/dh/post/1569840",
      "display_url" : "alpha.app.net/dh/post/1569840"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269478900197908482",
  "text" : "Proxies for hacking: http://t.co/KX6e9BRU BTW - I pay for these. Feel free to trade them on BBS sites for VIP status,\u2026 https://t.co/r6A4LCqL",
  "id" : 269478900197908482,
  "created_at" : "Fri Nov 16 16:35:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "embeducation",
      "screen_name" : "AWSOMEDEVSIGNER",
      "indices" : [ 68, 84 ],
      "id_str" : "83013622",
      "id" : 83013622
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 109 ],
      "url" : "https://t.co/ARy8LCYm",
      "expanded_url" : "https://flattr.com/thing/810148/AWSOMEDEVSIGNER-on-Flattr",
      "display_url" : "flattr.com/thing/810148/A\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269475722278617088",
  "text" : "AWSOMEDEVSIGNER on Flattr is awesome, so I sent a Flattr payment to @awsomedevsigner -  https://t.co/ARy8LCYm",
  "id" : 269475722278617088,
  "created_at" : "Fri Nov 16 16:23:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/VTORtXgO",
      "expanded_url" : "http://bit.ly/XqR3zZ",
      "display_url" : "bit.ly/XqR3zZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269475123709476864",
  "text" : "JavaScript md5 function - php.js http://t.co/VTORtXgO",
  "id" : 269475123709476864,
  "created_at" : "Fri Nov 16 16:20:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/qdZPnCzt",
      "expanded_url" : "http://bit.ly/INJGfl",
      "display_url" : "bit.ly/INJGfl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269471597331632129",
  "text" : "\u2665 Treehouse! http://t.co/qdZPnCzt",
  "id" : 269471597331632129,
  "created_at" : "Fri Nov 16 16:06:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/qyG9xKwC",
      "expanded_url" : "http://bit.ly/XqQVQZ",
      "display_url" : "bit.ly/XqQVQZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269470213576216576",
  "text" : "Livefyre | We Make Your Site Social http://t.co/qyG9xKwC",
  "id" : 269470213576216576,
  "created_at" : "Fri Nov 16 16:01:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/LfyzkUdw",
      "expanded_url" : "http://bit.ly/JM98Rw",
      "display_url" : "bit.ly/JM98Rw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269456494158827521",
  "text" : "The BEST Designed Free Website Templates http://t.co/LfyzkUdw",
  "id" : 269456494158827521,
  "created_at" : "Fri Nov 16 15:06:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "embeducation",
      "screen_name" : "AWSOMEDEVSIGNER",
      "indices" : [ 0, 16 ],
      "id_str" : "83013622",
      "id" : 83013622
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "269451260493713408",
  "geo" : {
  },
  "id_str" : "269451589411020800",
  "in_reply_to_user_id" : 83013622,
  "text" : "@AWSOMEDEVSIGNER What?",
  "id" : 269451589411020800,
  "in_reply_to_status_id" : 269451260493713408,
  "created_at" : "Fri Nov 16 14:47:19 +0000 2012",
  "in_reply_to_screen_name" : "AWSOMEDEVSIGNER",
  "in_reply_to_user_id_str" : "83013622",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 48 ],
      "url" : "https://t.co/ZvK5MsZG",
      "expanded_url" : "https://history.google.com/history",
      "display_url" : "history.google.com/history"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269451334502203392",
  "text" : "I found this cool new site https://t.co/ZvK5MsZG (LoL)",
  "id" : 269451334502203392,
  "created_at" : "Fri Nov 16 14:46:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http://t.co/o3345xtf",
      "expanded_url" : "http://bit.ly/INKGjx",
      "display_url" : "bit.ly/INKGjx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269443784138448896",
  "text" : "Devlisting - Web developer resources http://t.co/o3345xtf",
  "id" : 269443784138448896,
  "created_at" : "Fri Nov 16 14:16:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/bn9urIpA",
      "expanded_url" : "http://bit.ly/INKrFg",
      "display_url" : "bit.ly/INKrFg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269426161120071680",
  "text" : "HTTP Archive http://t.co/bn9urIpA",
  "id" : 269426161120071680,
  "created_at" : "Fri Nov 16 13:06:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 0, 12 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/oxTplQ3K",
      "expanded_url" : "http://muffinresearch.co.uk/code/javascript/DOMTool/",
      "display_url" : "muffinresearch.co.uk/code/javascrip\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "269423067841847296",
  "geo" : {
  },
  "id_str" : "269425411140747265",
  "in_reply_to_user_id" : 15736190,
  "text" : "@smashingmag Reminds me of Dom Tool http://t.co/oxTplQ3K Except d\u014Dmo is JS to HTML, Not HTML to JS ;)",
  "id" : 269425411140747265,
  "in_reply_to_status_id" : 269423067841847296,
  "created_at" : "Fri Nov 16 13:03:18 +0000 2012",
  "in_reply_to_screen_name" : "smashingmag",
  "in_reply_to_user_id_str" : "15736190",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/wCjqfzW7",
      "expanded_url" : "http://bit.ly/UzZWkN",
      "display_url" : "bit.ly/UzZWkN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269413701357019136",
  "text" : "Easy Usable Pastebin http://t.co/wCjqfzW7",
  "id" : 269413701357019136,
  "created_at" : "Fri Nov 16 12:16:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/8rjRdVdQ",
      "expanded_url" : "http://bit.ly/UzZV09",
      "display_url" : "bit.ly/UzZV09"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269410049418473472",
  "text" : "jQuery Waypoints http://t.co/8rjRdVdQ",
  "id" : 269410049418473472,
  "created_at" : "Fri Nov 16 12:02:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/JeQU243R",
      "expanded_url" : "http://inv.lv/XqQTIV",
      "display_url" : "inv.lv/XqQTIV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269402748817113088",
  "text" : "Involver - Mission Control For Social Media Management http://t.co/JeQU243R",
  "id" : 269402748817113088,
  "created_at" : "Fri Nov 16 11:33:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/qhhakrKt",
      "expanded_url" : "http://bit.ly/XqQTZk",
      "display_url" : "bit.ly/XqQTZk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269400987666624512",
  "text" : "pastebin - Type, paste, share. http://t.co/qhhakrKt",
  "id" : 269400987666624512,
  "created_at" : "Fri Nov 16 11:26:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/FaVBlAP2",
      "expanded_url" : "http://bit.ly/UzZHpO",
      "display_url" : "bit.ly/UzZHpO"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269398974157422593",
  "text" : "DocumentUp http://t.co/FaVBlAP2",
  "id" : 269398974157422593,
  "created_at" : "Fri Nov 16 11:18:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 34 ],
      "url" : "http://t.co/lbTJLuYA",
      "expanded_url" : "http://bit.ly/UzZGSD",
      "display_url" : "bit.ly/UzZGSD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269349383542288384",
  "text" : "JS Compressor http://t.co/lbTJLuYA",
  "id" : 269349383542288384,
  "created_at" : "Fri Nov 16 08:01:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nico Garcia Boccia",
      "screen_name" : "nicogarcia",
      "indices" : [ 0, 11 ],
      "id_str" : "14561409",
      "id" : 14561409
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "269201480278482944",
  "geo" : {
  },
  "id_str" : "269225002790973440",
  "in_reply_to_user_id" : 14561409,
  "text" : "@nicogarcia So awesome!",
  "id" : 269225002790973440,
  "in_reply_to_status_id" : 269201480278482944,
  "created_at" : "Thu Nov 15 23:46:57 +0000 2012",
  "in_reply_to_screen_name" : "nicogarcia",
  "in_reply_to_user_id_str" : "14561409",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nico Garcia Boccia",
      "screen_name" : "nicogarcia",
      "indices" : [ 3, 14 ],
      "id_str" : "14561409",
      "id" : 14561409
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/4yhVSDXU",
      "expanded_url" : "http://www.ReinventingEmail.com",
      "display_url" : "ReinventingEmail.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269224777087074304",
  "text" : "RT @nicogarcia: Recently launched: http://t.co/4yhVSDXU",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 19, 39 ],
        "url" : "http://t.co/4yhVSDXU",
        "expanded_url" : "http://www.ReinventingEmail.com",
        "display_url" : "ReinventingEmail.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "269201480278482944",
    "text" : "Recently launched: http://t.co/4yhVSDXU",
    "id" : 269201480278482944,
    "created_at" : "Thu Nov 15 22:13:29 +0000 2012",
    "user" : {
      "name" : "Nico Garcia Boccia",
      "screen_name" : "nicogarcia",
      "protected" : false,
      "id_str" : "14561409",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3449740912/bf1ef6f84a8a3c690a90dbcd1ced19ff_normal.png",
      "id" : 14561409,
      "verified" : false
    }
  },
  "id" : 269224777087074304,
  "created_at" : "Thu Nov 15 23:46:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Martin Th\u00F6rnkvist",
      "screen_name" : "thornkvist",
      "indices" : [ 65, 76 ],
      "id_str" : "5687962",
      "id" : 5687962
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 101 ],
      "url" : "https://t.co/Aq3UOwka",
      "expanded_url" : "https://flattr.com/thing/187267/thornkvist-on-Flattr",
      "display_url" : "flattr.com/thing/187267/t\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269220899985707009",
  "text" : "\"thornkvist on Flattr\" is awesome, so I sent a Flattr payment to @thornkvist -  https://t.co/Aq3UOwka",
  "id" : 269220899985707009,
  "created_at" : "Thu Nov 15 23:30:39 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Siim Teller",
      "screen_name" : "teller",
      "indices" : [ 66, 73 ],
      "id_str" : "11165452",
      "id" : 11165452
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 98 ],
      "url" : "https://t.co/LiwxtNsk",
      "expanded_url" : "https://flattr.com/thing/366260/Siim-Teller-on-Flattr",
      "display_url" : "flattr.com/thing/366260/S\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269220795945988098",
  "text" : "\"Siim Teller on Flattr\" is awesome, so I sent a Flattr payment to @teller -  https://t.co/LiwxtNsk",
  "id" : 269220795945988098,
  "created_at" : "Thu Nov 15 23:30:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Till Zoppke",
      "screen_name" : "kinnla",
      "indices" : [ 61, 68 ],
      "id_str" : "19803555",
      "id" : 19803555
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 93 ],
      "url" : "https://t.co/1lp3DVeM",
      "expanded_url" : "https://flattr.com/thing/212227/kinnla-on-Flattr",
      "display_url" : "flattr.com/thing/212227/k\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269220647765438465",
  "text" : "\"kinnla on Flattr\" is awesome, so I sent a Flattr payment to @kinnla -  https://t.co/1lp3DVeM",
  "id" : 269220647765438465,
  "created_at" : "Thu Nov 15 23:29:38 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lembit Kivisik",
      "screen_name" : "lembit",
      "indices" : [ 61, 68 ],
      "id_str" : "14849205",
      "id" : 14849205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 93 ],
      "url" : "https://t.co/SzQD0ep9",
      "expanded_url" : "https://flattr.com/thing/659163/lembit-on-Flattr",
      "display_url" : "flattr.com/thing/659163/l\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269220515762302976",
  "text" : "\"lembit on Flattr\" is awesome, so I sent a Flattr payment to @lembit -  https://t.co/SzQD0ep9",
  "id" : 269220515762302976,
  "created_at" : "Thu Nov 15 23:29:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Ashworth",
      "screen_name" : "phuunet",
      "indices" : [ 59, 67 ],
      "id_str" : "7054122",
      "id" : 7054122
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 92 ],
      "url" : "https://t.co/XhA8AvfL",
      "expanded_url" : "https://flattr.com/thing/855521/phuu-on-Flattr",
      "display_url" : "flattr.com/thing/855521/p\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269220406399991809",
  "text" : "\"phuu on Flattr\" is awesome, so I sent a Flattr payment to @phuunet -  https://t.co/XhA8AvfL",
  "id" : 269220406399991809,
  "created_at" : "Thu Nov 15 23:28:41 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Ashworth",
      "screen_name" : "phuunet",
      "indices" : [ 50, 58 ],
      "id_str" : "7054122",
      "id" : 7054122
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 83 ],
      "url" : "https://t.co/DzVWCz3v",
      "expanded_url" : "https://flattr.com/thing/863029/Twapp",
      "display_url" : "flattr.com/thing/863029/T\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269220174559854592",
  "text" : "\"Twapp\" is awesome, so I sent a Flattr payment to @phuunet -  https://t.co/DzVWCz3v",
  "id" : 269220174559854592,
  "created_at" : "Thu Nov 15 23:27:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/XMuIq6ZI",
      "expanded_url" : "http://bit.ly/UzZEKI",
      "display_url" : "bit.ly/UzZEKI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269217377722462208",
  "text" : "Plunker http://t.co/XMuIq6ZI",
  "id" : 269217377722462208,
  "created_at" : "Thu Nov 15 23:16:39 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/GNWZRWg9",
      "expanded_url" : "http://bit.ly/XqQndT",
      "display_url" : "bit.ly/XqQndT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269216872145252354",
  "text" : "Twitstat Mobile http://t.co/GNWZRWg9",
  "id" : 269216872145252354,
  "created_at" : "Thu Nov 15 23:14:38 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 74 ],
      "url" : "http://t.co/msvlV2Bh",
      "expanded_url" : "http://bit.ly/Q8F6rM",
      "display_url" : "bit.ly/Q8F6rM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269205526007656448",
  "text" : "SparkleShare - Self hosted, instant, secure file sync http://t.co/msvlV2Bh",
  "id" : 269205526007656448,
  "created_at" : "Thu Nov 15 22:29:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eileen Burbidge",
      "screen_name" : "eileentso",
      "indices" : [ 58, 68 ],
      "id_str" : "22301442",
      "id" : 22301442
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 93 ],
      "url" : "https://t.co/HEGXdvDW",
      "expanded_url" : "https://flattr.com/thing/187376/eileentso-on-Flattr",
      "display_url" : "flattr.com/thing/187376/e\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269197566288220160",
  "text" : "Eileen Burbidge is awesome, so I sent a Flattr payment to @eileentso -  https://t.co/HEGXdvDW",
  "id" : 269197566288220160,
  "created_at" : "Thu Nov 15 21:57:55 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Linus Olsson",
      "screen_name" : "bonq",
      "indices" : [ 55, 60 ],
      "id_str" : "16663874",
      "id" : 16663874
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 85 ],
      "url" : "https://t.co/Zx5jAJ0O",
      "expanded_url" : "https://flattr.com/thing/187263/bonq-on-Flattr",
      "display_url" : "flattr.com/thing/187263/b\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269197320007077888",
  "text" : "Linus Olsson is awesome, so I sent a Flattr payment to @bonq -  https://t.co/Zx5jAJ0O",
  "id" : 269197320007077888,
  "created_at" : "Thu Nov 15 21:56:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter Sunde",
      "screen_name" : "brokep",
      "indices" : [ 75, 82 ],
      "id_str" : "19033675",
      "id" : 19033675
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 107 ],
      "url" : "https://t.co/TDhGQ0Iq",
      "expanded_url" : "https://flattr.com/thing/187262/brokep-on-Flattr",
      "display_url" : "flattr.com/thing/187262/b\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269197150351671297",
  "text" : "Peter Sunde Kolmisoppi on Flattr is awesome, so I sent a Flattr payment to @brokep -  https://t.co/TDhGQ0Iq",
  "id" : 269197150351671297,
  "created_at" : "Thu Nov 15 21:56:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/mksEP73D",
      "expanded_url" : "http://bit.ly/Q8Egep",
      "display_url" : "bit.ly/Q8Egep"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269191557998071808",
  "text" : "Winsplit Revolution http://t.co/mksEP73D",
  "id" : 269191557998071808,
  "created_at" : "Thu Nov 15 21:34:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http://t.co/WbxH0o6e",
      "expanded_url" : "http://bit.ly/NseLHU",
      "display_url" : "bit.ly/NseLHU"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269175070465875968",
  "text" : "jQuery Plugins for Styling Checkbox &amp; Radio Buttons http://t.co/WbxH0o6e",
  "id" : 269175070465875968,
  "created_at" : "Thu Nov 15 20:28:32 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/SXDdXqGP",
      "expanded_url" : "http://bit.ly/XqQdTW",
      "display_url" : "bit.ly/XqQdTW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269161231322071040",
  "text" : "Twitter ShareButton. http://t.co/SXDdXqGP",
  "id" : 269161231322071040,
  "created_at" : "Thu Nov 15 19:33:32 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 113 ],
      "url" : "http://t.co/FsBMWi1a",
      "expanded_url" : "http://tympanus.net/Tutorials/SwatchBook/index.html",
      "display_url" : "tympanus.net/Tutorials/Swat\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269152119280529408",
  "text" : "Silly Tympanus / Codrops demos are silly. Very sexy and shiney, but useless as f**k Example: http://t.co/FsBMWi1a \u2026 So useless!",
  "id" : 269152119280529408,
  "created_at" : "Thu Nov 15 18:57:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "cody lindley",
      "screen_name" : "codylindley",
      "indices" : [ 3, 15 ],
      "id_str" : "12078532",
      "id" : 12078532
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 132 ],
      "url" : "http://t.co/Z4MVMeiJ",
      "expanded_url" : "http://domenlightenment.com/#12",
      "display_url" : "domenlightenment.com/#12"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269150981432958977",
  "text" : "RT @codylindley: Chapter 12, DOM Enlightenment book roughly done, details a foundational modern jQuery-like lib http://t.co/Z4MVMeiJ / h ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.tweetdeck.com\" rel=\"nofollow\">TweetDeck</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 115 ],
        "url" : "http://t.co/Z4MVMeiJ",
        "expanded_url" : "http://domenlightenment.com/#12",
        "display_url" : "domenlightenment.com/#12"
      }, {
        "indices" : [ 118, 139 ],
        "url" : "https://t.co/au20kced",
        "expanded_url" : "https://github.com/codylindley/domjs/blob/master/builds/dom.js",
        "display_url" : "github.com/codylindley/do\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "269144736449847297",
    "text" : "Chapter 12, DOM Enlightenment book roughly done, details a foundational modern jQuery-like lib http://t.co/Z4MVMeiJ / https://t.co/au20kced",
    "id" : 269144736449847297,
    "created_at" : "Thu Nov 15 18:28:00 +0000 2012",
    "user" : {
      "name" : "cody lindley",
      "screen_name" : "codylindley",
      "protected" : false,
      "id_str" : "12078532",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000218069250/91222084dc1959ba699460c09c132032_normal.jpeg",
      "id" : 12078532,
      "verified" : false
    }
  },
  "id" : 269150981432958977,
  "created_at" : "Thu Nov 15 18:52:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "niftylettuce",
      "screen_name" : "niftylettuce",
      "indices" : [ 3, 16 ],
      "id_str" : "214358209",
      "id" : 214358209
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 110 ],
      "url" : "https://t.co/xDD1A6ar",
      "expanded_url" : "https://pinpigeon.com",
      "display_url" : "pinpigeon.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269149745237344256",
  "text" : "RT @niftylettuce: PinPigeon - Send pins as printed &amp; shipped postcards for only 1.95 https://t.co/xDD1A6ar via @PinPigeon",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 92 ],
        "url" : "https://t.co/xDD1A6ar",
        "expanded_url" : "https://pinpigeon.com",
        "display_url" : "pinpigeon.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "266734033948528640",
    "text" : "PinPigeon - Send pins as printed &amp; shipped postcards for only 1.95 https://t.co/xDD1A6ar via @PinPigeon",
    "id" : 266734033948528640,
    "created_at" : "Fri Nov 09 02:48:44 +0000 2012",
    "user" : {
      "name" : "niftylettuce",
      "screen_name" : "niftylettuce",
      "protected" : false,
      "id_str" : "214358209",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1541432016/lettuce_normal.png",
      "id" : 214358209,
      "verified" : false
    }
  },
  "id" : 269149745237344256,
  "created_at" : "Thu Nov 15 18:47:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/ENc0rI9N",
      "expanded_url" : "http://bit.ly/XqPTVj",
      "display_url" : "bit.ly/XqPTVj"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269145624082010112",
  "text" : "Pitch Free WordPress Theme |SiteOrigin http://t.co/ENc0rI9N",
  "id" : 269145624082010112,
  "created_at" : "Thu Nov 15 18:31:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http://t.co/sVF3mvhn",
      "expanded_url" : "http://bit.ly/UzZrab",
      "display_url" : "bit.ly/UzZrab"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269143481602146304",
  "text" : "quick, painless, javascript-free baseline overlays http://t.co/sVF3mvhn",
  "id" : 269143481602146304,
  "created_at" : "Thu Nov 15 18:23:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/ZvNj0CRa",
      "expanded_url" : "http://bit.ly/XqPSjZ",
      "display_url" : "bit.ly/XqPSjZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269142853983289346",
  "text" : "CSS Lint http://t.co/ZvNj0CRa",
  "id" : 269142853983289346,
  "created_at" : "Thu Nov 15 18:20:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Juho Veps\u00E4l\u00E4inen",
      "screen_name" : "bebraw",
      "indices" : [ 50, 57 ],
      "id_str" : "167793781",
      "id" : 167793781
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/VYVLk09D",
      "expanded_url" : "http://flattr.com/t/600560",
      "display_url" : "flattr.com/t/600560"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269139581851557888",
  "text" : "I just flattred \"JSwiki\" http://t.co/VYVLk09D cc: @bebraw",
  "id" : 269139581851557888,
  "created_at" : "Thu Nov 15 18:07:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon H\u00F8jberg",
      "screen_name" : "shojberg",
      "indices" : [ 0, 9 ],
      "id_str" : "685533",
      "id" : 685533
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 93 ],
      "url" : "http://t.co/PSBUmaOo",
      "expanded_url" : "http://icreateui.com/",
      "display_url" : "icreateui.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269136664713113603",
  "in_reply_to_user_id" : 685533,
  "text" : "@shojberg You might want to include the new Twitter widget on your site (http://t.co/PSBUmaOo) Seeing a perma-throbber on my end.",
  "id" : 269136664713113603,
  "created_at" : "Thu Nov 15 17:55:55 +0000 2012",
  "in_reply_to_screen_name" : "shojberg",
  "in_reply_to_user_id_str" : "685533",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/iCVtPEwM",
      "expanded_url" : "http://bit.ly/UzZrac",
      "display_url" : "bit.ly/UzZrac"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269130397575696385",
  "text" : "Webhosting : Indexhibit http://t.co/iCVtPEwM",
  "id" : 269130397575696385,
  "created_at" : "Thu Nov 15 17:31:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/U7FPzuVy",
      "expanded_url" : "http://i.imgur.com/lolwut.gif",
      "display_url" : "i.imgur.com/lolwut.gif"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269127624901668864",
  "text" : "Hacking imgur's URL schema, I found this: http://t.co/U7FPzuVy 'lolwut.gif' is very much indeed a lolwut image.",
  "id" : 269127624901668864,
  "created_at" : "Thu Nov 15 17:20:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 136 ],
      "url" : "https://t.co/eMhYtOoV",
      "expanded_url" : "https://alpha.app.net/dh/post/1554308",
      "display_url" : "alpha.app.net/dh/post/1554308"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269125738572177408",
  "text" : "Hexspeak, like leetspeak, is a novelty form of variant English spelling using the hexadecimal numbers. Created by\u2026 https://t.co/eMhYtOoV",
  "id" : 269125738572177408,
  "created_at" : "Thu Nov 15 17:12:30 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "openweb",
      "indices" : [ 41, 49 ]
    } ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/gic3ZTMU",
      "expanded_url" : "http://pfefferle.github.com/openwebicons/",
      "display_url" : "pfefferle.github.com/openwebicons/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269113908260925441",
  "text" : "Oh yes, so awesome: http://t.co/gic3ZTMU #openweb",
  "id" : 269113908260925441,
  "created_at" : "Thu Nov 15 16:25:30 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/6StMhHjw",
      "expanded_url" : "http://bit.ly/UzZrae",
      "display_url" : "bit.ly/UzZrae"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269112776566398976",
  "text" : "MitchellMcKenna/LifePress \u00B7 GitHub http://t.co/6StMhHjw",
  "id" : 269112776566398976,
  "created_at" : "Thu Nov 15 16:21:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/10FXgOmS",
      "expanded_url" : "http://bit.ly/UzZsLn",
      "display_url" : "bit.ly/UzZsLn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269109124896280576",
  "text" : "Tumblr Tag Clouds | Heather Rivers http://t.co/10FXgOmS",
  "id" : 269109124896280576,
  "created_at" : "Thu Nov 15 16:06:29 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/Ytaaq7MW",
      "expanded_url" : "http://bit.ly/UzZsLq",
      "display_url" : "bit.ly/UzZsLq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269107740792745984",
  "text" : "niftylettuce/element-capture \u00B7 GitHub http://t.co/Ytaaq7MW",
  "id" : 269107740792745984,
  "created_at" : "Thu Nov 15 16:00:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WTF",
      "indices" : [ 89, 93 ]
    }, {
      "text" : "Hack",
      "indices" : [ 94, 99 ]
    } ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/dYuf8LhL",
      "expanded_url" : "http://bit.ly/UIwdGp",
      "display_url" : "bit.ly/UIwdGp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269103462552313856",
  "text" : "http://t.co/dYuf8LhL Look at the f**king source. Insane embedded PNG mixed in with HTML. #WTF #Hack",
  "id" : 269103462552313856,
  "created_at" : "Thu Nov 15 15:43:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/6jsAVFWa",
      "expanded_url" : "http://bit.ly/XqPZfD",
      "display_url" : "bit.ly/XqPZfD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269094024038711296",
  "text" : "position:sticky; polyfill/shim \u00B7 CodePen http://t.co/6jsAVFWa",
  "id" : 269094024038711296,
  "created_at" : "Thu Nov 15 15:06:29 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 139 ],
      "url" : "https://t.co/gAmSW3MT",
      "expanded_url" : "https://alpha.app.net/dh/post/1552340",
      "display_url" : "alpha.app.net/dh/post/1552340"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269092892302262272",
  "text" : "Poster Image let's you put an image in front of your embedded video. Another benefit, video's are hidden untill they\u2026 https://t.co/gAmSW3MT",
  "id" : 269092892302262272,
  "created_at" : "Thu Nov 15 15:01:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "269089369892196352",
  "text" : "Collect, test, and experiment with interface pattern pairings of CSS",
  "id" : 269089369892196352,
  "created_at" : "Thu Nov 15 14:47:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 88 ],
      "url" : "http://t.co/2GEz2zG3",
      "expanded_url" : "http://bit.ly/XqPZMH",
      "display_url" : "bit.ly/XqPZMH"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269081190076645376",
  "text" : "Entitifier \u2014 Convert obscure characters to the proper HTML entities http://t.co/2GEz2zG3",
  "id" : 269081190076645376,
  "created_at" : "Thu Nov 15 14:15:29 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/uUQaw617",
      "expanded_url" : "http://bit.ly/UzZsLs",
      "display_url" : "bit.ly/UzZsLs"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269063572833849345",
  "text" : "The GOOD 100, or so http://t.co/uUQaw617",
  "id" : 269063572833849345,
  "created_at" : "Thu Nov 15 13:05:29 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http://t.co/HXD3glGG",
      "expanded_url" : "http://bit.ly/UzZrar",
      "display_url" : "bit.ly/UzZrar"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269051239835258881",
  "text" : "Don\u2019t call me DOM \u00BB Named versus Numeric Entities http://t.co/HXD3glGG",
  "id" : 269051239835258881,
  "created_at" : "Thu Nov 15 12:16:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/g28KVeud",
      "expanded_url" : "http://bit.ly/UzZt1U",
      "display_url" : "bit.ly/UzZt1U"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269047593139589120",
  "text" : "Tattly\u2122 Designy Temporary Tattoos \u2014 Welcome http://t.co/g28KVeud",
  "id" : 269047593139589120,
  "created_at" : "Thu Nov 15 12:01:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/8D822xF6",
      "expanded_url" : "http://bit.ly/XqPUZ9",
      "display_url" : "bit.ly/XqPUZ9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269040165484363776",
  "text" : "Jack Smith \u2013 Portfolio and Journal http://t.co/8D822xF6",
  "id" : 269040165484363776,
  "created_at" : "Thu Nov 15 11:32:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/S1WtHfoh",
      "expanded_url" : "http://bit.ly/UzZt1X",
      "display_url" : "bit.ly/UzZt1X"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269038403813785600",
  "text" : "Confabulation - Wikipedia, the free encyclopedia http://t.co/S1WtHfoh",
  "id" : 269038403813785600,
  "created_at" : "Thu Nov 15 11:25:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 84 ],
      "url" : "http://t.co/xxYT8SKM",
      "expanded_url" : "http://bit.ly/XqPUZo",
      "display_url" : "bit.ly/XqPUZo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "269036389373444096",
  "text" : "List of most expensive films - Wikipedia, the free encyclopedia http://t.co/xxYT8SKM",
  "id" : 269036389373444096,
  "created_at" : "Thu Nov 15 11:17:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 78 ],
      "url" : "http://t.co/ta97vOZI",
      "expanded_url" : "http://bit.ly/XqPVfD",
      "display_url" : "bit.ly/XqPVfD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268986933999460353",
  "text" : "List of lists of lists - Wikipedia, the free encyclopedia http://t.co/ta97vOZI",
  "id" : 268986933999460353,
  "created_at" : "Thu Nov 15 08:00:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/pG7xj0o2",
      "expanded_url" : "http://bit.ly/XqPW3e",
      "display_url" : "bit.ly/XqPW3e"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268855047549493248",
  "text" : "Mozilla Firefox Cheat Sheet http://t.co/pG7xj0o2",
  "id" : 268855047549493248,
  "created_at" : "Wed Nov 14 23:16:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/sSKdW6nD",
      "expanded_url" : "http://bit.ly/U45zNy",
      "display_url" : "bit.ly/U45zNy"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268854546044968960",
  "text" : "Rogie's Blog http://t.co/sSKdW6nD",
  "id" : 268854546044968960,
  "created_at" : "Wed Nov 14 23:14:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/RkhUHDNE",
      "expanded_url" : "http://bit.ly/Q6bnPW",
      "display_url" : "bit.ly/Q6bnPW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268843219847106560",
  "text" : "rstacruz/css-condense \u00B7 GitHub http://t.co/RkhUHDNE",
  "id" : 268843219847106560,
  "created_at" : "Wed Nov 14 22:29:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 3, 6 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "268830154057478144",
  "text" : "RT @ys: I just paid $7 for a promoted post on Facebook. I've honestly no idea what that got me.",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "268825174638465024",
    "text" : "I just paid $7 for a promoted post on Facebook. I've honestly no idea what that got me.",
    "id" : 268825174638465024,
    "created_at" : "Wed Nov 14 21:18:10 +0000 2012",
    "user" : {
      "name" : "Youssef",
      "screen_name" : "ys",
      "protected" : false,
      "id_str" : "19010677",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000122676370/327da5878e48b810986df4829ea25ea6_normal.jpeg",
      "id" : 19010677,
      "verified" : false
    }
  },
  "id" : 268830154057478144,
  "created_at" : "Wed Nov 14 21:37:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http://t.co/WFJ6cf0X",
      "expanded_url" : "http://bit.ly/Q6bprj",
      "display_url" : "bit.ly/Q6bprj"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268829024812425216",
  "text" : "Lightglass Tumblr theme by rickerlr | Theme Cloud http://t.co/WFJ6cf0X",
  "id" : 268829024812425216,
  "created_at" : "Wed Nov 14 21:33:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Prinzhorn",
      "screen_name" : "Prinzhorn",
      "indices" : [ 3, 13 ],
      "id_str" : "187226449",
      "id" : 187226449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 98 ],
      "url" : "http://t.co/dy5M6QOu",
      "expanded_url" : "http://prinzhorn.github.com/skrollr/examples/foldscroll.html",
      "display_url" : "prinzhorn.github.com/skrollr/exampl\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268821823481851904",
  "text" : "RT @Prinzhorn: @_higg 30 minutes later there's a skrollr proof of concept ;-) http://t.co/dy5M6QOu",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 63, 83 ],
        "url" : "http://t.co/dy5M6QOu",
        "expanded_url" : "http://prinzhorn.github.com/skrollr/examples/foldscroll.html",
        "display_url" : "prinzhorn.github.com/skrollr/exampl\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "268812656452964353",
    "geo" : {
    },
    "id_str" : "268821462712987648",
    "in_reply_to_user_id" : 468853739,
    "text" : "@_higg 30 minutes later there's a skrollr proof of concept ;-) http://t.co/dy5M6QOu",
    "id" : 268821462712987648,
    "in_reply_to_status_id" : 268812656452964353,
    "created_at" : "Wed Nov 14 21:03:25 +0000 2012",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "Alexander Prinzhorn",
      "screen_name" : "Prinzhorn",
      "protected" : false,
      "id_str" : "187226449",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3429728285/70fdba6aa325b363a9827c6dba163e97_normal.jpeg",
      "id" : 187226449,
      "verified" : false
    }
  },
  "id" : 268821823481851904,
  "created_at" : "Wed Nov 14 21:04:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/Nay5MyFv",
      "expanded_url" : "http://bit.ly/Q6c242",
      "display_url" : "bit.ly/Q6c242"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268812656452964353",
  "text" : "FoldScroll http://t.co/Nay5MyFv",
  "id" : 268812656452964353,
  "created_at" : "Wed Nov 14 20:28:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 0, 10 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DIY",
      "indices" : [ 121, 125 ]
    }, {
      "text" : "WEB",
      "indices" : [ 126, 130 ]
    }, {
      "text" : "INDIEWEB",
      "indices" : [ 131, 140 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "268806524493758465",
  "geo" : {
  },
  "id_str" : "268809251999330304",
  "in_reply_to_user_id" : 9866582,
  "text" : "@film_girl Thats why the Indie Web and having your own voice in Clay Shirky's \"here comes everybody\" is super important. #DIY #WEB #INDIEWEB",
  "id" : 268809251999330304,
  "in_reply_to_status_id" : 268806524493758465,
  "created_at" : "Wed Nov 14 20:14:54 +0000 2012",
  "in_reply_to_screen_name" : "film_girl",
  "in_reply_to_user_id_str" : "9866582",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "268807560314884096",
  "text" : "'Responsive' Sites with apx ~2MB of data loading on a horribly slow 3G connection is the ultimate fail. Adapt to bandwidth not screen size!",
  "id" : 268807560314884096,
  "created_at" : "Wed Nov 14 20:08:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 48, 62 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 94 ],
      "url" : "http://t.co/rsxp1t9B",
      "expanded_url" : "http://iwantaneff.in/bin/view/raw/7f38c7e7",
      "display_url" : "iwantaneff.in/bin/view/raw/7\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268798894358556672",
  "text" : "50 awesome XSS vectors that I have tweeted (via @soaj1664ashar) over time http://t.co/rsxp1t9B",
  "id" : 268798894358556672,
  "created_at" : "Wed Nov 14 19:33:45 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/fFcue8Zs",
      "expanded_url" : "http://bit.ly/Q6ckb9",
      "display_url" : "bit.ly/Q6ckb9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268798813878239232",
  "text" : "HubInfo | A Github Repo Widget http://t.co/fFcue8Zs",
  "id" : 268798813878239232,
  "created_at" : "Wed Nov 14 19:33:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 101 ],
      "url" : "http://t.co/afTJyanB",
      "expanded_url" : "http://html5sec.org",
      "display_url" : "html5sec.org"
    } ]
  },
  "in_reply_to_status_id_str" : "268793060727599104",
  "geo" : {
  },
  "id_str" : "268793906865508352",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar Specifically, I referring to error messages executing HTML+JS But http://t.co/afTJyanB / is more than enough?\u2049",
  "id" : 268793906865508352,
  "in_reply_to_status_id" : 268793060727599104,
  "created_at" : "Wed Nov 14 19:13:56 +0000 2012",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "268790674718085121",
  "geo" : {
  },
  "id_str" : "268792765201129472",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar  hxxp://someserver.com/&lt;script&gt;alert(1)&lt;/script&gt; executes on *some* servers. Won't disclose examples, as that's silly lol ;)",
  "id" : 268792765201129472,
  "in_reply_to_status_id" : 268790674718085121,
  "created_at" : "Wed Nov 14 19:09:23 +0000 2012",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 68, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/ECo25Nt9",
      "expanded_url" : "http://myshar.es/2",
      "display_url" : "myshar.es/2"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268788823276785664",
  "text" : "Damn you Apache, you thought this one out well http://t.co/ECo25Nt9 #xss",
  "id" : 268788823276785664,
  "created_at" : "Wed Nov 14 18:53:43 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "skype",
      "indices" : [ 67, 73 ]
    }, {
      "text" : "security",
      "indices" : [ 74, 83 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/C8gorH68",
      "expanded_url" : "http://myshar.es/1",
      "display_url" : "myshar.es/1"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268784129343098880",
  "text" : "The Skype Vulnerability - How to achieve it \u2192 http://t.co/C8gorH68 #skype #security",
  "id" : 268784129343098880,
  "created_at" : "Wed Nov 14 18:35:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/U5s2dUTI",
      "expanded_url" : "http://simpl.info",
      "display_url" : "simpl.info"
    }, {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/AViZJZ22",
      "expanded_url" : "http://bit.ly/NqKUj3",
      "display_url" : "bit.ly/NqKUj3"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268783209263796224",
  "text" : "http://t.co/U5s2dUTI http://t.co/AViZJZ22",
  "id" : 268783209263796224,
  "created_at" : "Wed Nov 14 18:31:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Skype",
      "screen_name" : "Skype",
      "indices" : [ 3, 9 ],
      "id_str" : "2459371",
      "id" : 2459371
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "268781950305714177",
  "text" : "RT @Skype: We are aware of a new security issue. As user safety is our #1 concern, we've temporarily disabled password reset: http://t.c ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.exacttarget.com/social\" rel=\"nofollow\">SocialEngage</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 135 ],
        "url" : "http://t.co/NZvAtsWt",
        "expanded_url" : "http://bit.ly/TGbioM",
        "display_url" : "bit.ly/TGbioM"
      } ]
    },
    "geo" : {
    },
    "id_str" : "268723987478102018",
    "text" : "We are aware of a new security issue. As user safety is our #1 concern, we've temporarily disabled password reset: http://t.co/NZvAtsWt",
    "id" : 268723987478102018,
    "created_at" : "Wed Nov 14 14:36:05 +0000 2012",
    "user" : {
      "name" : "Skype",
      "screen_name" : "Skype",
      "protected" : false,
      "id_str" : "2459371",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3338666689/8fc44ba83964393f4bfa68ddb629178f_normal.jpeg",
      "id" : 2459371,
      "verified" : true
    }
  },
  "id" : 268781950305714177,
  "created_at" : "Wed Nov 14 18:26:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http://t.co/WutlhV25",
      "expanded_url" : "http://bit.ly/NqL2ig",
      "display_url" : "bit.ly/NqL2ig"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268780439144128513",
  "text" : "Detect Mobile Browsers - Open source mobile phone detection http://t.co/WutlhV25",
  "id" : 268780439144128513,
  "created_at" : "Wed Nov 14 18:20:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tiago Duarte",
      "screen_name" : "HelloTiago",
      "indices" : [ 0, 11 ],
      "id_str" : "177738636",
      "id" : 177738636
    }, {
      "name" : "Patrik Larsson",
      "screen_name" : "LarssonPatrik",
      "indices" : [ 12, 26 ],
      "id_str" : "68577718",
      "id" : 68577718
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http://t.co/lwbmW7Wm",
      "expanded_url" : "http://html5sec.org/noid?xss=%3Ciframe%20width=0%20height=0%20src=%22data:text/html;base64,PHNjcmlwdD50b3Aud2luZG93LnRlc3Q9J2FsZXJ0KDEpJzwvc2NyaXB0Pg==%22%3E%3C/iframe%3E",
      "display_url" : "html5sec.org/noid?xss=%3Cif\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "268773587874361345",
  "geo" : {
  },
  "id_str" : "268779809809780736",
  "in_reply_to_user_id" : 177738636,
  "text" : "@HelloTiago @LarssonPatrik It's handy for doing things like this: http://t.co/lwbmW7Wm Base64 HTML is a joy.",
  "id" : 268779809809780736,
  "in_reply_to_status_id" : 268773587874361345,
  "created_at" : "Wed Nov 14 18:17:55 +0000 2012",
  "in_reply_to_screen_name" : "HelloTiago",
  "in_reply_to_user_id_str" : "177738636",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/DRmPyFKn",
      "expanded_url" : "http://bit.ly/Q6f600",
      "display_url" : "bit.ly/Q6f600"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268779432871870464",
  "text" : "NexMonitor http://t.co/DRmPyFKn",
  "id" : 268779432871870464,
  "created_at" : "Wed Nov 14 18:16:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tiago Duarte",
      "screen_name" : "HelloTiago",
      "indices" : [ 0, 11 ],
      "id_str" : "177738636",
      "id" : 177738636
    }, {
      "name" : "Patrik Larsson",
      "screen_name" : "LarssonPatrik",
      "indices" : [ 12, 26 ],
      "id_str" : "68577718",
      "id" : 68577718
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "268773587874361345",
  "geo" : {
  },
  "id_str" : "268779382284369921",
  "in_reply_to_user_id" : 177738636,
  "text" : "@HelloTiago @LarssonPatrik I think you should extend it to cater for different file types. You can have any file as a data URI. E.G HTML/PDF",
  "id" : 268779382284369921,
  "in_reply_to_status_id" : 268773587874361345,
  "created_at" : "Wed Nov 14 18:16:13 +0000 2012",
  "in_reply_to_screen_name" : "HelloTiago",
  "in_reply_to_user_id_str" : "177738636",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tiago Duarte",
      "screen_name" : "HelloTiago",
      "indices" : [ 0, 11 ],
      "id_str" : "177738636",
      "id" : 177738636
    }, {
      "name" : "Patrik Larsson",
      "screen_name" : "LarssonPatrik",
      "indices" : [ 12, 26 ],
      "id_str" : "68577718",
      "id" : 68577718
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "268703990974406656",
  "geo" : {
  },
  "id_str" : "268772698350563328",
  "in_reply_to_user_id" : 177738636,
  "text" : "@HelloTiago @LarssonPatrik It's sexy alright, I just dislike the server-side aspect, and the limitations on the file types you must use.",
  "id" : 268772698350563328,
  "in_reply_to_status_id" : 268703990974406656,
  "created_at" : "Wed Nov 14 17:49:39 +0000 2012",
  "in_reply_to_screen_name" : "HelloTiago",
  "in_reply_to_user_id_str" : "177738636",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/FYgKpBkl",
      "expanded_url" : "http://bit.ly/Q6f6Nq",
      "display_url" : "bit.ly/Q6f6Nq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268767980672937984",
  "text" : "Toast | A simple CSS framework http://t.co/FYgKpBkl",
  "id" : 268767980672937984,
  "created_at" : "Wed Nov 14 17:30:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/IJHQTrVl",
      "expanded_url" : "http://bit.ly/NqL6yw",
      "display_url" : "bit.ly/NqL6yw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268750615721672704",
  "text" : "Run any application on demand http://t.co/IJHQTrVl",
  "id" : 268750615721672704,
  "created_at" : "Wed Nov 14 16:21:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/JQLB0SH5",
      "expanded_url" : "http://bit.ly/NqL4H6",
      "display_url" : "bit.ly/NqL4H6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268746838084038657",
  "text" : "HowTo: What is rss.js? http://t.co/JQLB0SH5",
  "id" : 268746838084038657,
  "created_at" : "Wed Nov 14 16:06:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/itZgrpvb",
      "expanded_url" : "http://bit.ly/NqL8qn",
      "display_url" : "bit.ly/NqL8qn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268745204511698944",
  "text" : "Black Widow | Dustin Curtis http://t.co/itZgrpvb",
  "id" : 268745204511698944,
  "created_at" : "Wed Nov 14 16:00:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/bfNIxF8d",
      "expanded_url" : "http://bit.ly/NqLfC7",
      "display_url" : "bit.ly/NqLfC7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268731613238661120",
  "text" : "Browzr.js | A jQuery Plugin http://t.co/bfNIxF8d",
  "id" : 268731613238661120,
  "created_at" : "Wed Nov 14 15:06:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/Pbp4YGvU",
      "expanded_url" : "http://bit.ly/NqLlK1",
      "display_url" : "bit.ly/NqLlK1"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268718779419201536",
  "text" : "MemStash - Stop forgetting. Remember anything. http://t.co/Pbp4YGvU",
  "id" : 268718779419201536,
  "created_at" : "Wed Nov 14 14:15:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "simurai",
      "screen_name" : "simurai",
      "indices" : [ 0, 8 ],
      "id_str" : "6896972",
      "id" : 6896972
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/ZivgxEwW",
      "expanded_url" : "http://minimal.be/lab/iScroll/",
      "display_url" : "minimal.be/lab/iScroll/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268703845629165572",
  "in_reply_to_user_id" : 6896972,
  "text" : "@simurai http://t.co/ZivgxEwW Interesting ;)",
  "id" : 268703845629165572,
  "created_at" : "Wed Nov 14 13:16:03 +0000 2012",
  "in_reply_to_screen_name" : "simurai",
  "in_reply_to_user_id_str" : "6896972",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Patrik Larsson",
      "screen_name" : "LarssonPatrik",
      "indices" : [ 0, 14 ],
      "id_str" : "68577718",
      "id" : 68577718
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/1jYQLJr4",
      "expanded_url" : "http://iwantaneff.in/dataurl/",
      "display_url" : "iwantaneff.in/dataurl/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268703206249480192",
  "in_reply_to_user_id" : 68577718,
  "text" : "@LarssonPatrik http://t.co/1jYQLJr4 is better :p",
  "id" : 268703206249480192,
  "created_at" : "Wed Nov 14 13:13:31 +0000 2012",
  "in_reply_to_screen_name" : "LarssonPatrik",
  "in_reply_to_user_id_str" : "68577718",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/dBbzUsCe",
      "expanded_url" : "http://bit.ly/NqLoWk",
      "display_url" : "bit.ly/NqLoWk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268701158850297856",
  "text" : "&amp;what; -- Discover Your Character http://t.co/dBbzUsCe",
  "id" : 268701158850297856,
  "created_at" : "Wed Nov 14 13:05:23 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 84 ],
      "url" : "http://t.co/Rv7Em3Kn",
      "expanded_url" : "http://bit.ly/Q8PDmG",
      "display_url" : "bit.ly/Q8PDmG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268688824115277825",
  "text" : "Twit Buddy | Check if you two are (still) following each other. http://t.co/Rv7Em3Kn",
  "id" : 268688824115277825,
  "created_at" : "Wed Nov 14 12:16:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/Pp52YgZu",
      "expanded_url" : "http://bit.ly/UzZcfk",
      "display_url" : "bit.ly/UzZcfk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268685048516059136",
  "text" : "Vinh Nguyen http://t.co/Pp52YgZu",
  "id" : 268685048516059136,
  "created_at" : "Wed Nov 14 12:01:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/4gY0tU2E",
      "expanded_url" : "http://bit.ly/Qg0z1Q",
      "display_url" : "bit.ly/Qg0z1Q"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268677750431313920",
  "text" : "Wavii http://t.co/4gY0tU2E",
  "id" : 268677750431313920,
  "created_at" : "Wed Nov 14 11:32:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/C6zWaBDc",
      "expanded_url" : "http://bit.ly/NxDjPv",
      "display_url" : "bit.ly/NxDjPv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268675987296878592",
  "text" : "BootTheme http://t.co/C6zWaBDc",
  "id" : 268675987296878592,
  "created_at" : "Wed Nov 14 11:25:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/tRY3GmzV",
      "expanded_url" : "http://bit.ly/PITqHN",
      "display_url" : "bit.ly/PITqHN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268673973775134721",
  "text" : "Jetstrap - The Bootstrap Interface Builder http://t.co/tRY3GmzV",
  "id" : 268673973775134721,
  "created_at" : "Wed Nov 14 11:17:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/TsJ7qxoo",
      "expanded_url" : "http://bit.ly/U45iKJ",
      "display_url" : "bit.ly/U45iKJ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268624392303558656",
  "text" : "slayeroffice | key code tool http://t.co/TsJ7qxoo",
  "id" : 268624392303558656,
  "created_at" : "Wed Nov 14 08:00:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/YGHtShEd",
      "expanded_url" : "http://bit.ly/UzZ8My",
      "display_url" : "bit.ly/UzZ8My"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268492506411433984",
  "text" : "URL Decoder/Encoder http://t.co/YGHtShEd",
  "id" : 268492506411433984,
  "created_at" : "Tue Nov 13 23:16:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/vNr1k4F7",
      "expanded_url" : "http://bit.ly/XqPiTm",
      "display_url" : "bit.ly/XqPiTm"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268492005246648320",
  "text" : "Octopress http://t.co/vNr1k4F7",
  "id" : 268492005246648320,
  "created_at" : "Tue Nov 13 23:14:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http://t.co/hgUwbOKx",
      "expanded_url" : "http://bit.ly/XqPimx",
      "display_url" : "bit.ly/XqPimx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268480676087619584",
  "text" : "Free Wordpress Themes and Plugins http://t.co/hgUwbOKx",
  "id" : 268480676087619584,
  "created_at" : "Tue Nov 13 22:29:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/jNDJwWTH",
      "expanded_url" : "http://bit.ly/PIT2Je",
      "display_url" : "bit.ly/PIT2Je"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268466637806641153",
  "text" : "HubPages http://t.co/jNDJwWTH",
  "id" : 268466637806641153,
  "created_at" : "Tue Nov 13 21:33:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 134 ],
      "url" : "https://t.co/YxEmPEcS",
      "expanded_url" : "https://alpha.app.net/dh/post/1524865",
      "display_url" : "alpha.app.net/dh/post/1524865"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268463746370912257",
  "text" : "RFCs are documents produced by The Internet Engineering Task Force, and many are official standards for various\u2026 https://t.co/YxEmPEcS",
  "id" : 268463746370912257,
  "created_at" : "Tue Nov 13 21:21:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 7, 27 ],
      "url" : "http://t.co/7N2lyJAZ",
      "expanded_url" : "http://codepen.io/images/404.png",
      "display_url" : "codepen.io/images/404.png"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268462485663793153",
  "text" : "LOLWUT http://t.co/7N2lyJAZ",
  "id" : 268462485663793153,
  "created_at" : "Tue Nov 13 21:16:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "maria m m",
      "screen_name" : "PistachioPony",
      "indices" : [ 126, 140 ],
      "id_str" : "360848817",
      "id" : 360848817
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 122 ],
      "url" : "http://t.co/tSdSCMWk",
      "expanded_url" : "http://apc.io/about/",
      "display_url" : "apc.io/about/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268461507757633539",
  "text" : "\"Like Jobs and Gates, we believe the PC is one of the most remarkable tools humans have ever created\" http://t.co/tSdSCMWk cc @PistachioPony",
  "id" : 268461507757633539,
  "created_at" : "Tue Nov 13 21:13:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 0, 12 ],
      "id_str" : "793830",
      "id" : 793830
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http://t.co/tOGyqjO9",
      "expanded_url" : "http://codepen.io/anon/details/6252",
      "display_url" : "codepen.io/anon/details/6\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "268460306219540481",
  "geo" : {
  },
  "id_str" : "268460818612498433",
  "in_reply_to_user_id" : 793830,
  "text" : "@chriscoyier I like this 'super discount' one too: http://t.co/tOGyqjO9",
  "id" : 268460818612498433,
  "in_reply_to_status_id" : 268460306219540481,
  "created_at" : "Tue Nov 13 21:10:21 +0000 2012",
  "in_reply_to_screen_name" : "chriscoyier",
  "in_reply_to_user_id_str" : "793830",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "268460264754651136",
  "text" : "Copyright (c) 2011 === I have an unbridled license to pillage and steal your content.",
  "id" : 268460264754651136,
  "created_at" : "Tue Nov 13 21:08:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 125, 140 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "268459384819027968",
  "text" : "I love when somebody's exalted and on Twitter at the same time. I know what its like when you're trending. Great feeling! cc @davidwalshblog",
  "id" : 268459384819027968,
  "created_at" : "Tue Nov 13 21:04:39 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/Ss5f98k7",
      "expanded_url" : "http://seolinks.w3schools.com/",
      "display_url" : "seolinks.w3schools.com"
    }, {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/laMbz2Ap",
      "expanded_url" : "http://cheapcialis.w3schools.com/",
      "display_url" : "cheapcialis.w3schools.com"
    }, {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/SRfwxElp",
      "expanded_url" : "http://fake-gucci-buy.w3schools.com/",
      "display_url" : "fake-gucci-buy.w3schools.com"
    }, {
      "indices" : [ 69, 89 ],
      "url" : "http://t.co/Bf90w0yw",
      "expanded_url" : "http://ganja.w3schools.com/",
      "display_url" : "ganja.w3schools.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268459087241555968",
  "text" : "Fun \u2192 http://t.co/Ss5f98k7 http://t.co/laMbz2Ap\nhttp://t.co/SRfwxElp\nhttp://t.co/Bf90w0yw",
  "id" : 268459087241555968,
  "created_at" : "Tue Nov 13 21:03:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/zJFZVwCE",
      "expanded_url" : "http://bit.ly/XqPi5M",
      "display_url" : "bit.ly/XqPi5M"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268450275902242817",
  "text" : "correct.li // let users correct your mistakes http://t.co/zJFZVwCE",
  "id" : 268450275902242817,
  "created_at" : "Tue Nov 13 20:28:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tim Holman",
      "screen_name" : "twholman",
      "indices" : [ 0, 9 ],
      "id_str" : "284347056",
      "id" : 284347056
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/ZUizpPII",
      "expanded_url" : "http://smileyfor.eu/",
      "display_url" : "smileyfor.eu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268443398858481664",
  "in_reply_to_user_id" : 284347056,
  "text" : "@twholman Can you add this to The Useless Web? http://t.co/ZUizpPII Thanks ;)",
  "id" : 268443398858481664,
  "created_at" : "Tue Nov 13 20:01:08 +0000 2012",
  "in_reply_to_screen_name" : "twholman",
  "in_reply_to_user_id_str" : "284347056",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/ymuYuc5O",
      "expanded_url" : "http://bit.ly/UzZ67D",
      "display_url" : "bit.ly/UzZ67D"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268436433734348800",
  "text" : "http://t.co/ymuYuc5O",
  "id" : 268436433734348800,
  "created_at" : "Tue Nov 13 19:33:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/46Bct9i9",
      "expanded_url" : "http://bit.ly/U44K7A",
      "display_url" : "bit.ly/U44K7A"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268420828260098049",
  "text" : "Striking.ly http://t.co/46Bct9i9",
  "id" : 268420828260098049,
  "created_at" : "Tue Nov 13 18:31:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/WhUEsaIr",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/AWCfXC1k",
      "expanded_url" : "http://bit.ly/XqPgek",
      "display_url" : "bit.ly/XqPgek"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268418062003363840",
  "text" : "http://t.co/WhUEsaIr secure messaging proposal http://t.co/AWCfXC1k",
  "id" : 268418062003363840,
  "created_at" : "Tue Nov 13 18:20:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 34 ],
      "url" : "http://t.co/EDpaV7uu",
      "expanded_url" : "http://bit.ly/UzZ3bT",
      "display_url" : "bit.ly/UzZ3bT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268417050832150528",
  "text" : "What the Hex? http://t.co/EDpaV7uu",
  "id" : 268417050832150528,
  "created_at" : "Tue Nov 13 18:16:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http://t.co/goVLpiNO",
      "expanded_url" : "http://bit.ly/XqP5Qh",
      "display_url" : "bit.ly/XqP5Qh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268405472044929025",
  "text" : "Wibiya | The all new and improved wibiya bar experience http://t.co/goVLpiNO",
  "id" : 268405472044929025,
  "created_at" : "Tue Nov 13 17:30:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/uDscGgHv",
      "expanded_url" : "http://bit.ly/XqP3Yw",
      "display_url" : "bit.ly/XqP3Yw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268387852147109889",
  "text" : "CSS3 patterned buttons | Hey, designer! http://t.co/uDscGgHv",
  "id" : 268387852147109889,
  "created_at" : "Tue Nov 13 16:20:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/3ZVJGRpe",
      "expanded_url" : "http://bit.ly/UzZ3bY",
      "display_url" : "bit.ly/UzZ3bY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268384325538570240",
  "text" : "bpierre/scri.ch \u00B7 GitHub http://t.co/3ZVJGRpe",
  "id" : 268384325538570240,
  "created_at" : "Tue Nov 13 16:06:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "268382815949844480",
  "text" : "\"jimpunk's shared items\"",
  "id" : 268382815949844480,
  "created_at" : "Tue Nov 13 16:00:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/tgKheSqv",
      "expanded_url" : "http://bit.ly/XqOQ7Q",
      "display_url" : "bit.ly/XqOQ7Q"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268369223246565378",
  "text" : "Envy Labs http://t.co/tgKheSqv",
  "id" : 268369223246565378,
  "created_at" : "Tue Nov 13 15:06:23 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/PyTngic1",
      "expanded_url" : "http://bit.ly/UzYWNw",
      "display_url" : "bit.ly/UzYWNw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268356384872075265",
  "text" : "reBlog by Eyebeam R&amp;D http://t.co/PyTngic1",
  "id" : 268356384872075265,
  "created_at" : "Tue Nov 13 14:15:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/4k1UIPtu",
      "expanded_url" : "http://bit.ly/XqOTjV",
      "display_url" : "bit.ly/XqOTjV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268338764982652928",
  "text" : "ReView. The Responsive Viewport. http://t.co/4k1UIPtu",
  "id" : 268338764982652928,
  "created_at" : "Tue Nov 13 13:05:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 111 ],
      "url" : "http://t.co/xf1Tgp6a",
      "expanded_url" : "http://mzl.la/UzYXkp",
      "display_url" : "mzl.la/UzYXkp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268326430537027584",
  "text" : "New Firefox Command Line helps you develop faster \u2729 Mozilla Hacks \u2013 the Web developer blog http://t.co/xf1Tgp6a",
  "id" : 268326430537027584,
  "created_at" : "Tue Nov 13 12:16:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/4lgmpXz3",
      "expanded_url" : "http://bit.ly/U44uph",
      "display_url" : "bit.ly/U44uph"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268322654908448769",
  "text" : "BaseDemo - HorizontalNav http://t.co/4lgmpXz3",
  "id" : 268322654908448769,
  "created_at" : "Tue Nov 13 12:01:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/MbP6eplV",
      "expanded_url" : "http://bit.ly/UzYXks",
      "display_url" : "bit.ly/UzYXks"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268315358283321344",
  "text" : "a piece of paper in the cloud http://t.co/MbP6eplV",
  "id" : 268315358283321344,
  "created_at" : "Tue Nov 13 11:32:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/mfKnLxi5",
      "expanded_url" : "http://bit.ly/UzYTRV",
      "display_url" : "bit.ly/UzYTRV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268313593756413954",
  "text" : "Stratus 2 Guides http://t.co/mfKnLxi5",
  "id" : 268313593756413954,
  "created_at" : "Tue Nov 13 11:25:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/Gn5g3w6q",
      "expanded_url" : "http://bit.ly/PISFOW",
      "display_url" : "bit.ly/PISFOW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268311579165077505",
  "text" : "Stratus 2 BETA http://t.co/Gn5g3w6q",
  "id" : 268311579165077505,
  "created_at" : "Tue Nov 13 11:17:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/GyIbQ0Oz",
      "expanded_url" : "http://bit.ly/U44llw",
      "display_url" : "bit.ly/U44llw"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268262119399641088",
  "text" : "Hashtags - Twubs http://t.co/GyIbQ0Oz",
  "id" : 268262119399641088,
  "created_at" : "Tue Nov 13 08:00:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http://t.co/CFb4XxAr",
      "expanded_url" : "http://bit.ly/UzYTBk",
      "display_url" : "bit.ly/UzYTBk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268130095007408129",
  "text" : "htmlUI \u00AB 5 Obscure Facts About HTML5 LocalStorage http://t.co/CFb4XxAr",
  "id" : 268130095007408129,
  "created_at" : "Mon Nov 12 23:16:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 28 ],
      "url" : "http://t.co/Y0HXmWqd",
      "expanded_url" : "http://bit.ly/U44fdJ",
      "display_url" : "bit.ly/U44fdJ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268129589853814784",
  "text" : ".htTool http://t.co/Y0HXmWqd",
  "id" : 268129589853814784,
  "created_at" : "Mon Nov 12 23:14:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremy Selier",
      "screen_name" : "jerem",
      "indices" : [ 0, 6 ],
      "id_str" : "7011892",
      "id" : 7011892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "268067784683032576",
  "geo" : {
  },
  "id_str" : "268126338706194432",
  "in_reply_to_user_id" : 7011892,
  "text" : "@jerem I Flattrd it, as suggested on G+",
  "id" : 268126338706194432,
  "in_reply_to_status_id" : 268067784683032576,
  "created_at" : "Mon Nov 12 23:01:15 +0000 2012",
  "in_reply_to_screen_name" : "jerem",
  "in_reply_to_user_id_str" : "7011892",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan",
      "screen_name" : "eataudio",
      "indices" : [ 0, 9 ],
      "id_str" : "21089184",
      "id" : 21089184
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 101 ],
      "url" : "https://t.co/kjAsUQjE",
      "expanded_url" : "https://gist.github.com/2032253",
      "display_url" : "gist.github.com/2032253"
    } ]
  },
  "in_reply_to_status_id_str" : "268123744181030912",
  "geo" : {
  },
  "id_str" : "268124223254429696",
  "in_reply_to_user_id" : 21089184,
  "text" : "@eataudio No problem. Did you see my bookmarklet that converts it to NYAN cat?\u2047 https://t.co/kjAsUQjE",
  "id" : 268124223254429696,
  "in_reply_to_status_id" : 268123744181030912,
  "created_at" : "Mon Nov 12 22:52:50 +0000 2012",
  "in_reply_to_screen_name" : "eataudio",
  "in_reply_to_user_id_str" : "21089184",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "XSS",
      "indices" : [ 58, 62 ]
    }, {
      "text" : "utf8",
      "indices" : [ 74, 79 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "268123515788611585",
  "text" : "alert('\\uFF41\\uFF4C\\uFF45\\uFF52\\uFF54\\u1455\\uFF11\\u1450') #XSS inception! #utf8",
  "id" : 268123515788611585,
  "created_at" : "Mon Nov 12 22:50:02 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan",
      "screen_name" : "eataudio",
      "indices" : [ 53, 62 ],
      "id_str" : "21089184",
      "id" : 21089184
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "flattr",
      "indices" : [ 88, 95 ]
    } ],
    "urls" : [ {
      "indices" : [ 66, 87 ],
      "url" : "https://t.co/mncuKzIs",
      "expanded_url" : "https://flattr.com/thing/716382/OMFGDOGS",
      "display_url" : "flattr.com/thing/716382/O\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268122426527854592",
  "text" : "\"OMFGDOGS\" is awesome, so I sent a Flattr payment to @eataudio -  https://t.co/mncuKzIs #flattr",
  "id" : 268122426527854592,
  "created_at" : "Mon Nov 12 22:45:42 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/QnsxjisC",
      "expanded_url" : "http://bit.ly/XqONbV",
      "display_url" : "bit.ly/XqONbV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268118262578049024",
  "text" : "edna piranha http://t.co/QnsxjisC",
  "id" : 268118262578049024,
  "created_at" : "Mon Nov 12 22:29:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http://t.co/M204pfu0",
      "expanded_url" : "http://bit.ly/PISfYS",
      "display_url" : "bit.ly/PISfYS"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268104171520135168",
  "text" : "jquer.in \u00BB Collection of jquery plugins and javascript libraries. http://t.co/M204pfu0",
  "id" : 268104171520135168,
  "created_at" : "Mon Nov 12 21:33:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CDN",
      "indices" : [ 69, 73 ]
    } ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/NicKAKNT",
      "expanded_url" : "http://bit.ly/UzYOxt",
      "display_url" : "bit.ly/UzYOxt"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268087826942414848",
  "text" : "WordPress \u203A CDN Linker lite \u00AB WordPress Plugins http://t.co/NicKAKNT #CDN",
  "id" : 268087826942414848,
  "created_at" : "Mon Nov 12 20:28:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 7, 27 ],
      "url" : "http://t.co/t7QrqXah",
      "expanded_url" : "http://bit.ly/Ptg0UB",
      "display_url" : "bit.ly/Ptg0UB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268073986016038912",
  "text" : "Domvas http://t.co/t7QrqXah",
  "id" : 268073986016038912,
  "created_at" : "Mon Nov 12 19:33:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http://t.co/GaM65krz",
      "expanded_url" : "http://bit.ly/UzYOxq",
      "display_url" : "bit.ly/UzYOxq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268058387621744640",
  "text" : "Scan your permissions... Find out who gained access to your personal info. http://t.co/GaM65krz",
  "id" : 268058387621744640,
  "created_at" : "Mon Nov 12 18:31:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/VBLIJbuY",
      "expanded_url" : "http://bit.ly/UzYNJS",
      "display_url" : "bit.ly/UzYNJS"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268055613475545088",
  "text" : "Survs - Pricing &amp; Sign Up http://t.co/VBLIJbuY",
  "id" : 268055613475545088,
  "created_at" : "Mon Nov 12 18:20:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/ldnK3QSq",
      "expanded_url" : "http://bit.ly/PIS6EP",
      "display_url" : "bit.ly/PIS6EP"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268054605072564227",
  "text" : "Track your twitter unfollowers http://t.co/ldnK3QSq",
  "id" : 268054605072564227,
  "created_at" : "Mon Nov 12 18:16:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/B7DoSiyD",
      "expanded_url" : "http://www.theuselessweb.com/",
      "display_url" : "theuselessweb.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268053096607928320",
  "text" : "http://t.co/B7DoSiyD The Useless Web",
  "id" : 268053096607928320,
  "created_at" : "Mon Nov 12 18:10:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/fgEHNJxk",
      "expanded_url" : "http://bit.ly/XqOCgY",
      "display_url" : "bit.ly/XqOCgY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268043155952259073",
  "text" : "Home - PHP Directory Lister http://t.co/fgEHNJxk",
  "id" : 268043155952259073,
  "created_at" : "Mon Nov 12 17:30:43 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/xFj0lEbr",
      "expanded_url" : "http://bit.ly/XqOCgI",
      "display_url" : "bit.ly/XqOCgI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268025412121149440",
  "text" : "KaeruCT/CuteViewer \u00B7 GitHub http://t.co/xFj0lEbr",
  "id" : 268025412121149440,
  "created_at" : "Mon Nov 12 16:20:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/jYbasQsI",
      "expanded_url" : "http://bit.ly/UzYNtv",
      "display_url" : "bit.ly/UzYNtv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268021887421001728",
  "text" : "sunny/pouce \u00B7 GitHub http://t.co/jYbasQsI",
  "id" : 268021887421001728,
  "created_at" : "Mon Nov 12 16:06:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/L16H4iqK",
      "expanded_url" : "http://imdb.to/XqOpdB",
      "display_url" : "imdb.to/XqOpdB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268020378507567105",
  "text" : "IMDb: The Best/Top 100 Irish films http://t.co/L16H4iqK",
  "id" : 268020378507567105,
  "created_at" : "Mon Nov 12 16:00:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cracking",
      "indices" : [ 44, 53 ]
    } ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/AjDodKm5",
      "expanded_url" : "http://isharefil.es/Kq5O",
      "display_url" : "isharefil.es/Kq5O"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268014085071855616",
  "text" : "Some working proxies \u2192 http://t.co/AjDodKm5 #cracking",
  "id" : 268014085071855616,
  "created_at" : "Mon Nov 12 15:35:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/9ZGBXS4S",
      "expanded_url" : "http://bit.ly/PIS5kn",
      "display_url" : "bit.ly/PIS5kn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "268007162423414785",
  "text" : "tylersticka/Indexy \u00B7 GitHub http://t.co/9ZGBXS4S",
  "id" : 268007162423414785,
  "created_at" : "Mon Nov 12 15:07:41 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/WYz5ypIo",
      "expanded_url" : "http://bit.ly/UzYK0C",
      "display_url" : "bit.ly/UzYK0C"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267993950508441600",
  "text" : "Sorting JavaScript Arrays in Numerical Order \u2014 Gist http://t.co/WYz5ypIo",
  "id" : 267993950508441600,
  "created_at" : "Mon Nov 12 14:15:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/mTxPGbsF",
      "expanded_url" : "http://bit.ly/UzYK0z",
      "display_url" : "bit.ly/UzYK0z"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267976335840927744",
  "text" : "Legends and Headings \u2014 kizu.ru http://t.co/mTxPGbsF",
  "id" : 267976335840927744,
  "created_at" : "Mon Nov 12 13:05:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 3, 11 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "wtf",
      "indices" : [ 117, 121 ]
    } ],
    "urls" : [ {
      "indices" : [ 96, 116 ],
      "url" : "http://t.co/Ih3JtbZM",
      "expanded_url" : "http://my.opera.com/hallvors/blog/2012/11/12/microsoft-sends-two-million-null-characters-hangs-opera",
      "display_url" : "my.opera.com/hallvors/blog/\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267969984632868864",
  "text" : "RT @mathias: Bizarre \u2014 Microsoft\u2019s SkyDrive website sends two million NULL characters to Opera: http://t.co/Ih3JtbZM #wtf",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "wtf",
        "indices" : [ 104, 108 ]
      } ],
      "urls" : [ {
        "indices" : [ 83, 103 ],
        "url" : "http://t.co/Ih3JtbZM",
        "expanded_url" : "http://my.opera.com/hallvors/blog/2012/11/12/microsoft-sends-two-million-null-characters-hangs-opera",
        "display_url" : "my.opera.com/hallvors/blog/\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "267961244114358272",
    "text" : "Bizarre \u2014 Microsoft\u2019s SkyDrive website sends two million NULL characters to Opera: http://t.co/Ih3JtbZM #wtf",
    "id" : 267961244114358272,
    "created_at" : "Mon Nov 12 12:05:13 +0000 2012",
    "user" : {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "protected" : false,
      "id_str" : "532923",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1255767431/kung-fu_normal.jpg",
      "id" : 532923,
      "verified" : false
    }
  },
  "id" : 267969984632868864,
  "created_at" : "Mon Nov 12 12:39:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 34 ],
      "url" : "http://t.co/FhqbCHYE",
      "expanded_url" : "http://bit.ly/UzYIps",
      "display_url" : "bit.ly/UzYIps"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267964003161088000",
  "text" : "Jixop \u00BB About http://t.co/FhqbCHYE",
  "id" : 267964003161088000,
  "created_at" : "Mon Nov 12 12:16:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/iucbUSCv",
      "expanded_url" : "http://bit.ly/PIS0Nn",
      "display_url" : "bit.ly/PIS0Nn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267960352845750273",
  "text" : "string.js http://t.co/iucbUSCv",
  "id" : 267960352845750273,
  "created_at" : "Mon Nov 12 12:01:41 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/aq1p3bl8",
      "expanded_url" : "http://bit.ly/UzYEWZ",
      "display_url" : "bit.ly/UzYEWZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267952924271988736",
  "text" : "News International R&amp;D Laboratories http://t.co/aq1p3bl8",
  "id" : 267952924271988736,
  "created_at" : "Mon Nov 12 11:32:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/JBZO879i",
      "expanded_url" : "http://bit.ly/XqOiPa",
      "display_url" : "bit.ly/XqOiPa"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267951163134074880",
  "text" : "Index of /demo/2011/ http://t.co/JBZO879i",
  "id" : 267951163134074880,
  "created_at" : "Mon Nov 12 11:25:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/39DHZdVF",
      "expanded_url" : "http://bit.ly/XqOhe5",
      "display_url" : "bit.ly/XqOhe5"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267949153533980672",
  "text" : "Cross Cross Coffee Cup by X-PRIME GROUPE - Cup http://t.co/39DHZdVF",
  "id" : 267949153533980672,
  "created_at" : "Mon Nov 12 11:17:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/JNmCG5GG",
      "expanded_url" : "http://bit.ly/XqOfD4",
      "display_url" : "bit.ly/XqOfD4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267899695412703232",
  "text" : "jaysalvat/image2css \u00B7 GitHub http://t.co/JNmCG5GG",
  "id" : 267899695412703232,
  "created_at" : "Mon Nov 12 08:00:39 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/8VMIVnDV",
      "expanded_url" : "http://bit.ly/XqOiyO",
      "display_url" : "bit.ly/XqOiyO"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267767680625410049",
  "text" : "About \u2013 Paravel http://t.co/8VMIVnDV",
  "id" : 267767680625410049,
  "created_at" : "Sun Nov 11 23:16:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/zqfssOml",
      "expanded_url" : "http://bit.ly/XqOa2h",
      "display_url" : "bit.ly/XqOa2h"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267767303951769601",
  "text" : "When in git http://t.co/zqfssOml",
  "id" : 267767303951769601,
  "created_at" : "Sun Nov 11 23:14:34 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/RB7KAn60",
      "expanded_url" : "http://bit.ly/XqO8Hy",
      "display_url" : "bit.ly/XqO8Hy"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267755977464492033",
  "text" : "CarotDAV - A WebDAV client for Windows OS. http://t.co/RB7KAn60",
  "id" : 267755977464492033,
  "created_at" : "Sun Nov 11 22:29:34 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/T0kGNiCx",
      "expanded_url" : "http://bit.ly/UzYzTd",
      "display_url" : "bit.ly/UzYzTd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267741759948345344",
  "text" : "5apps - \"One codebase to rule them all\" http://t.co/T0kGNiCx",
  "id" : 267741759948345344,
  "created_at" : "Sun Nov 11 21:33:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/Vqbi2QxG",
      "expanded_url" : "http://bit.ly/XqO4HU",
      "display_url" : "bit.ly/XqO4HU"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267725421351677955",
  "text" : "The Dribbble Draffft http://t.co/Vqbi2QxG",
  "id" : 267725421351677955,
  "created_at" : "Sun Nov 11 20:28:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/EpRQldFH",
      "expanded_url" : "http://bit.ly/UzYBdH",
      "display_url" : "bit.ly/UzYBdH"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267711578382684161",
  "text" : "Draft.im, helping you get in the game. http://t.co/EpRQldFH",
  "id" : 267711578382684161,
  "created_at" : "Sun Nov 11 19:33:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/Nr8AeMZy",
      "expanded_url" : "http://bit.ly/XqO3Un",
      "display_url" : "bit.ly/XqO3Un"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267695972744847360",
  "text" : "Kippt API by karrisaarinen - Kippt http://t.co/Nr8AeMZy",
  "id" : 267695972744847360,
  "created_at" : "Sun Nov 11 18:31:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/m3JZuYeJ",
      "expanded_url" : "http://bit.ly/XqO3DC",
      "display_url" : "bit.ly/XqO3DC"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267693331377713152",
  "text" : "The JavaScript Way http://t.co/m3JZuYeJ",
  "id" : 267693331377713152,
  "created_at" : "Sun Nov 11 18:20:38 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/F3KvDZbA",
      "expanded_url" : "http://bit.ly/XqO36H",
      "display_url" : "bit.ly/XqO36H"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267692197334351872",
  "text" : "Andrew Cantino http://t.co/F3KvDZbA",
  "id" : 267692197334351872,
  "created_at" : "Sun Nov 11 18:16:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http://t.co/yI1mccbg",
      "expanded_url" : "http://bit.ly/UzYxe1",
      "display_url" : "bit.ly/UzYxe1"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267680746079133696",
  "text" : "Welcome to Thoughtback - Program your mind, one thought at a time http://t.co/yI1mccbg",
  "id" : 267680746079133696,
  "created_at" : "Sun Nov 11 17:30:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/KH7Pu3iT",
      "expanded_url" : "http://bit.ly/INKpgp",
      "display_url" : "bit.ly/INKpgp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267663128865677313",
  "text" : "Elliott Kember dot Com http://t.co/KH7Pu3iT",
  "id" : 267663128865677313,
  "created_at" : "Sun Nov 11 16:20:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/aH8Q9wdi",
      "expanded_url" : "http://bit.ly/UzYvmo",
      "display_url" : "bit.ly/UzYvmo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267659604454944769",
  "text" : "kahlil/frontenddevwiki \u00B7 GitHub http://t.co/aH8Q9wdi",
  "id" : 267659604454944769,
  "created_at" : "Sun Nov 11 16:06:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/jTytJEcP",
      "expanded_url" : "http://bit.ly/UzYwGV",
      "display_url" : "bit.ly/UzYwGV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267658097567358976",
  "text" : "idiot/Unslider \u00B7 GitHub http://t.co/jTytJEcP",
  "id" : 267658097567358976,
  "created_at" : "Sun Nov 11 16:00:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/b6xKO05V",
      "expanded_url" : "http://bit.ly/UzYuPh",
      "display_url" : "bit.ly/UzYuPh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267644377973792769",
  "text" : "Bijou \u2014 tiny, 10 pixel icons for free http://t.co/b6xKO05V",
  "id" : 267644377973792769,
  "created_at" : "Sun Nov 11 15:06:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/4Ma3Z91K",
      "expanded_url" : "http://bit.ly/XqNXvO",
      "display_url" : "bit.ly/XqNXvO"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267631667273932800",
  "text" : "Personal Work - Bodge / Digital Creative http://t.co/4Ma3Z91K",
  "id" : 267631667273932800,
  "created_at" : "Sun Nov 11 14:15:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 77 ],
      "url" : "http://t.co/WHHakajp",
      "expanded_url" : "http://bit.ly/XqNRnZ",
      "display_url" : "bit.ly/XqNRnZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267614048235950081",
  "text" : "\"SieveFiltering of your RSS feeds has never been easier\" http://t.co/WHHakajp",
  "id" : 267614048235950081,
  "created_at" : "Sun Nov 11 13:05:35 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 110 ],
      "url" : "http://t.co/P1uDxAcb",
      "expanded_url" : "http://bit.ly/UzYq1X",
      "display_url" : "bit.ly/UzYq1X"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267601591547342848",
  "text" : "\"Use TwentyFeet, your metrics aggregator, and send a weekly performance post on twitter.\" http://t.co/P1uDxAcb",
  "id" : 267601591547342848,
  "created_at" : "Sun Nov 11 12:16:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/LwSRg0Gx",
      "expanded_url" : "http://bit.ly/UzYoY0",
      "display_url" : "bit.ly/UzYoY0"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267597941072613377",
  "text" : "Besquare, web conferences on your desktop. http://t.co/LwSRg0Gx",
  "id" : 267597941072613377,
  "created_at" : "Sun Nov 11 12:01:35 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 32, 35 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/ItIpz523",
      "expanded_url" : "http://bit.ly/UzYnTY",
      "display_url" : "bit.ly/UzYnTY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267590517502640129",
  "text" : "Numeral.js http://t.co/ItIpz523 #js",
  "id" : 267590517502640129,
  "created_at" : "Sun Nov 11 11:32:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/3yX87jW4",
      "expanded_url" : "http://bit.ly/XqNJVI",
      "display_url" : "bit.ly/XqNJVI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267588756868055042",
  "text" : "Sly - jQuery plugin http://t.co/3yX87jW4",
  "id" : 267588756868055042,
  "created_at" : "Sun Nov 11 11:25:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/WhUEsaIr",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/XhVZnLq6",
      "expanded_url" : "http://bit.ly/UzYnDj",
      "display_url" : "bit.ly/UzYnDj"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267586869942943744",
  "text" : "http://t.co/WhUEsaIr Directory http://t.co/XhVZnLq6",
  "id" : 267586869942943744,
  "created_at" : "Sun Nov 11 11:17:35 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/uGFtb96p",
      "expanded_url" : "http://bit.ly/XqNGJr",
      "display_url" : "bit.ly/XqNGJr"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267537289989718017",
  "text" : "Floating scrollbars in Firefox \u2014 Gist http://t.co/uGFtb96p",
  "id" : 267537289989718017,
  "created_at" : "Sun Nov 11 08:00:35 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Abraham Williams",
      "screen_name" : "abraham",
      "indices" : [ 0, 8 ],
      "id_str" : "9436992",
      "id" : 9436992
    }, {
      "name" : "David Hauser",
      "screen_name" : "dh",
      "indices" : [ 132, 135 ],
      "id_str" : "352553",
      "id" : 352553
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ADN",
      "indices" : [ 124, 128 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "267416041909530626",
  "in_reply_to_user_id" : 9436992,
  "text" : "@abraham Hey mate. You should follow me, if at least for this month. I will be posting lots of random (tech) loveliness. On #ADN as @dh",
  "id" : 267416041909530626,
  "created_at" : "Sat Nov 10 23:58:47 +0000 2012",
  "in_reply_to_screen_name" : "abraham",
  "in_reply_to_user_id_str" : "9436992",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/0f4hpwIb",
      "expanded_url" : "http://bit.ly/XqNGt0",
      "display_url" : "bit.ly/XqNGt0"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267405403720151040",
  "text" : "laterstars blog - Laterstars is Shutting Down http://t.co/0f4hpwIb",
  "id" : 267405403720151040,
  "created_at" : "Sat Nov 10 23:16:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 85 ],
      "url" : "http://t.co/jbAhudaQ",
      "expanded_url" : "http://bit.ly/XqNGsI",
      "display_url" : "bit.ly/XqNGsI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267404898319081473",
  "text" : "Dark Loading Bar | Ui Parade \u2013 User Interface Design Inspiration http://t.co/jbAhudaQ",
  "id" : 267404898319081473,
  "created_at" : "Sat Nov 10 23:14:30 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/2vhhrdfV",
      "expanded_url" : "http://bit.ly/XtdeFL",
      "display_url" : "bit.ly/XtdeFL"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267403895062528000",
  "text" : "Underscores | A Starter Theme for WordPress http://t.co/2vhhrdfV",
  "id" : 267403895062528000,
  "created_at" : "Sat Nov 10 23:10:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 96 ],
      "url" : "http://t.co/E4jnLdj6",
      "expanded_url" : "http://bit.ly/UAZe6D",
      "display_url" : "bit.ly/UAZe6D"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267403640216633345",
  "text" : "jPanelMenu: Slick Navigation-Sidebar With CSS Animations And jQuery - noupe http://t.co/E4jnLdj6",
  "id" : 267403640216633345,
  "created_at" : "Sat Nov 10 23:09:30 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http://t.co/uJi45jVW",
      "expanded_url" : "http://bit.ly/XqNDgI",
      "display_url" : "bit.ly/XqNDgI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267393597148631040",
  "text" : "Richard Tabor's Krazy Download Button - HTML/CSS'd http://t.co/uJi45jVW",
  "id" : 267393597148631040,
  "created_at" : "Sat Nov 10 22:29:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremy Brown",
      "screen_name" : "jeremybrown",
      "indices" : [ 0, 12 ],
      "id_str" : "14167219",
      "id" : 14167219
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 99 ],
      "url" : "http://t.co/KunsY1sv",
      "expanded_url" : "http://net.tutsplus.com/tutorials/javascript-ajax/quick-tip-working-with-the-official-jquery-templating-plugin/",
      "display_url" : "net.tutsplus.com/tutorials/java\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "267363786468519936",
  "geo" : {
  },
  "id_str" : "267389167779074049",
  "in_reply_to_user_id" : 14167219,
  "text" : "@jeremybrown My personal favorite is the official jQuery templating plugin ... http://t.co/KunsY1sv But t.js is still cool ;)",
  "id" : 267389167779074049,
  "in_reply_to_status_id" : 267363786468519936,
  "created_at" : "Sat Nov 10 22:12:00 +0000 2012",
  "in_reply_to_screen_name" : "jeremybrown",
  "in_reply_to_user_id_str" : "14167219",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ui",
      "indices" : [ 45, 48 ]
    }, {
      "text" : "ux",
      "indices" : [ 49, 52 ]
    }, {
      "text" : "css3",
      "indices" : [ 53, 58 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/t5rZdULZ",
      "expanded_url" : "http://bit.ly/UzYlez",
      "display_url" : "bit.ly/UzYlez"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267379513695027200",
  "text" : "Facebook Button Concept http://t.co/t5rZdULZ #ui #ux #css3",
  "id" : 267379513695027200,
  "created_at" : "Sat Nov 10 21:33:38 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/igHDRQoH",
      "expanded_url" : "http://bit.ly/Q8BwxD",
      "display_url" : "bit.ly/Q8BwxD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267363139455180801",
  "text" : "jasonmoo/t.js \u00B7 GitHub http://t.co/igHDRQoH",
  "id" : 267363139455180801,
  "created_at" : "Sat Nov 10 20:28:34 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/lth0Rdty",
      "expanded_url" : "http://bit.ly/UzYij6",
      "display_url" : "bit.ly/UzYij6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267349170912890880",
  "text" : "Floater Plugin Example - v0.5a http://t.co/lth0Rdty",
  "id" : 267349170912890880,
  "created_at" : "Sat Nov 10 19:33:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 77 ],
      "url" : "http://t.co/Tj30kJUn",
      "expanded_url" : "http://bit.ly/UzYhvu",
      "display_url" : "bit.ly/UzYhvu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267333689380323328",
  "text" : "BigVideo.js - The jQuery Plugin for Big Background Video http://t.co/Tj30kJUn",
  "id" : 267333689380323328,
  "created_at" : "Sat Nov 10 18:31:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 79 ],
      "url" : "http://t.co/zx7cPvWh",
      "expanded_url" : "http://bit.ly/XqNxFW",
      "display_url" : "bit.ly/XqNxFW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267330925577240577",
  "text" : "cloudlist - Easily manage all your social network settings http://t.co/zx7cPvWh",
  "id" : 267330925577240577,
  "created_at" : "Sat Nov 10 18:20:34 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http://t.co/mw3SYzON",
      "expanded_url" : "http://bit.ly/UzYfUo",
      "display_url" : "bit.ly/UzYfUo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267329917769879552",
  "text" : "Introducing HTML5, by Bruce Lawson and Remy Sharp http://t.co/mw3SYzON",
  "id" : 267329917769879552,
  "created_at" : "Sat Nov 10 18:16:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 136 ],
      "url" : "https://t.co/A5q3nRs3",
      "expanded_url" : "https://alpha.app.net/dh/post/1481007",
      "display_url" : "alpha.app.net/dh/post/1481007"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267324629734027265",
  "text" : "If you're shipping very elaborate .htaccess files with your code, you're doing it wrong. Always expect an endless\u2026 https://t.co/A5q3nRs3",
  "id" : 267324629734027265,
  "created_at" : "Sat Nov 10 17:55:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http://t.co/wiajtYuc",
      "expanded_url" : "http://bit.ly/UzYfDY",
      "display_url" : "bit.ly/UzYfDY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267318335073566720",
  "text" : "jQuery Selectable Plugin - Turn anything into selectable/checkable element http://t.co/wiajtYuc",
  "id" : 267318335073566720,
  "created_at" : "Sat Nov 10 17:30:32 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 0, 12 ],
      "id_str" : "15736190",
      "id" : 15736190
    }, {
      "name" : "Miroslav Popovic",
      "screen_name" : "miroslavpopovic",
      "indices" : [ 113, 129 ],
      "id_str" : "11563202",
      "id" : 11563202
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/BYQYGPy8",
      "expanded_url" : "http://miroslavpopovic.com/resources/web/",
      "display_url" : "miroslavpopovic.com/resources/web/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267308473497288704",
  "in_reply_to_user_id" : 15736190,
  "text" : "@smashingmag A very comprehensive list: http://t.co/BYQYGPy8 And a nice design too. Web Development Resources by @miroslavpopovic",
  "id" : 267308473497288704,
  "created_at" : "Sat Nov 10 16:51:21 +0000 2012",
  "in_reply_to_screen_name" : "smashingmag",
  "in_reply_to_user_id_str" : "15736190",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http://t.co/YyDR6nxZ",
      "expanded_url" : "http://bit.ly/XqMqWL",
      "display_url" : "bit.ly/XqMqWL"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267300714454327296",
  "text" : "Dan Eden, Moving The Web on Vimeo http://t.co/YyDR6nxZ",
  "id" : 267300714454327296,
  "created_at" : "Sat Nov 10 16:20:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/lbQ5JnO4",
      "expanded_url" : "http://bit.ly/UzXRW3",
      "display_url" : "bit.ly/UzXRW3"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267297191515807746",
  "text" : "Web Development Resources by Miroslav Popovic http://t.co/lbQ5JnO4",
  "id" : 267297191515807746,
  "created_at" : "Sat Nov 10 16:06:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/ZPXrjwEH",
      "expanded_url" : "http://bit.ly/XqMq93",
      "display_url" : "bit.ly/XqMq93"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267295805738729473",
  "text" : "Abraham Williams http://t.co/ZPXrjwEH",
  "id" : 267295805738729473,
  "created_at" : "Sat Nov 10 16:01:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/VeBnemCt",
      "expanded_url" : "http://bit.ly/UzXOJH",
      "display_url" : "bit.ly/UzXOJH"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267282091333529601",
  "text" : "Computers Club Drawing Society http://t.co/VeBnemCt",
  "id" : 267282091333529601,
  "created_at" : "Sat Nov 10 15:06:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 37, 40 ]
    } ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/NaxNbYuT",
      "expanded_url" : "http://bit.ly/UzXRoY",
      "display_url" : "bit.ly/UzXRoY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267269252023742464",
  "text" : "Telescopic Text http://t.co/NaxNbYuT #js",
  "id" : 267269252023742464,
  "created_at" : "Sat Nov 10 14:15:29 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/bIKSMaLe",
      "expanded_url" : "http://bit.ly/UzXQBn",
      "display_url" : "bit.ly/UzXQBn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267251629735161857",
  "text" : "CSS3 Checkboxes and Radiobuttons http://t.co/bIKSMaLe",
  "id" : 267251629735161857,
  "created_at" : "Sat Nov 10 13:05:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/2EfzLdpH",
      "expanded_url" : "http://bit.ly/XqMeqq",
      "display_url" : "bit.ly/XqMeqq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267239549854445568",
  "text" : "Interaction Design Blog | Hover States http://t.co/2EfzLdpH",
  "id" : 267239549854445568,
  "created_at" : "Sat Nov 10 12:17:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/4D1vKqFf",
      "expanded_url" : "http://bit.ly/XqMd5Z",
      "display_url" : "bit.ly/XqMd5Z"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267235521116389376",
  "text" : "Bookmarklet for making browser HTML content editable http://t.co/4D1vKqFf",
  "id" : 267235521116389376,
  "created_at" : "Sat Nov 10 12:01:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 102 ],
      "url" : "http://t.co/NkUoIWTv",
      "expanded_url" : "http://bit.ly/XqM5Dt",
      "display_url" : "bit.ly/XqM5Dt"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267228223736262658",
  "text" : "A steady supply of Emergency Compliments to be used at times of great insecurity. http://t.co/NkUoIWTv",
  "id" : 267228223736262658,
  "created_at" : "Sat Nov 10 11:32:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/sr1JUZqq",
      "expanded_url" : "http://bit.ly/UzXJ8W",
      "display_url" : "bit.ly/UzXJ8W"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267226459561668609",
  "text" : "Webshell - The API of APIs http://t.co/sr1JUZqq",
  "id" : 267226459561668609,
  "created_at" : "Sat Nov 10 11:25:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/4KDJmNEq",
      "expanded_url" : "http://bit.ly/XqM16u",
      "display_url" : "bit.ly/XqM16u"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267224445075202048",
  "text" : "Managed Rails Development http://t.co/4KDJmNEq",
  "id" : 267224445075202048,
  "created_at" : "Sat Nov 10 11:17:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/JHerzsqJ",
      "expanded_url" : "http://bit.ly/UzXI4X",
      "display_url" : "bit.ly/UzXI4X"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267174859178651649",
  "text" : "How to develop with Disqus on localhost \u00AB Ray\u2019s Weblog http://t.co/JHerzsqJ",
  "id" : 267174859178651649,
  "created_at" : "Sat Nov 10 08:00:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 137 ],
      "url" : "https://t.co/3ONYVxkz",
      "expanded_url" : "https://alpha.app.net/dh/post/1473101",
      "display_url" : "alpha.app.net/dh/post/1473101"
    } ]
  },
  "geo" : {
  },
  "id_str" : "267049250536443904",
  "text" : "With support for Delicious, Instapaper, Pinboard, Pocket and Readability, you can use reading-list-mover to switch\u2026 https://t.co/3ONYVxkz",
  "id" : 267049250536443904,
  "created_at" : "Fri Nov 09 23:41:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/RzYhd6yS",
      "expanded_url" : "http://www.yellowshoe.com.au/standards/",
      "display_url" : "yellowshoe.com.au/standards/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266995530637402112",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 http://t.co/RzYhd6yS First image + quote ... it is true ;)",
  "id" : 266995530637402112,
  "created_at" : "Fri Nov 09 20:07:49 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "App.net",
      "screen_name" : "AppDotNet",
      "indices" : [ 3, 13 ],
      "id_str" : "104558396",
      "id" : 104558396
    }, {
      "name" : "The Verge",
      "screen_name" : "verge",
      "indices" : [ 132, 138 ],
      "id_str" : "275686563",
      "id" : 275686563
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/lcL3xMAx",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 107, 127 ],
      "url" : "http://t.co/EtvHQv12",
      "expanded_url" : "http://vrge.co/ZerWPt",
      "display_url" : "vrge.co/ZerWPt"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266993863267319809",
  "text" : "RT @AppDotNet: The Verge is on http://t.co/lcL3xMAx: Engage: follow The Verge on all major social networks http://t.co/EtvHQv12 via @verge",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Verge",
        "screen_name" : "verge",
        "indices" : [ 117, 123 ],
        "id_str" : "275686563",
        "id" : 275686563
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 16, 36 ],
        "url" : "http://t.co/lcL3xMAx",
        "expanded_url" : "http://App.net",
        "display_url" : "App.net"
      }, {
        "indices" : [ 92, 112 ],
        "url" : "http://t.co/EtvHQv12",
        "expanded_url" : "http://vrge.co/ZerWPt",
        "display_url" : "vrge.co/ZerWPt"
      } ]
    },
    "geo" : {
    },
    "id_str" : "266990222758277120",
    "text" : "The Verge is on http://t.co/lcL3xMAx: Engage: follow The Verge on all major social networks http://t.co/EtvHQv12 via @verge",
    "id" : 266990222758277120,
    "created_at" : "Fri Nov 09 19:46:44 +0000 2012",
    "user" : {
      "name" : "App.net",
      "screen_name" : "AppDotNet",
      "protected" : false,
      "id_str" : "104558396",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000173471968/3e29d2f6ae34138d34d62dbb50fcc900_normal.png",
      "id" : 104558396,
      "verified" : false
    }
  },
  "id" : 266993863267319809,
  "created_at" : "Fri Nov 09 20:01:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Josh Susser",
      "screen_name" : "joshsusser",
      "indices" : [ 3, 14 ],
      "id_str" : "35954885",
      "id" : 35954885
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "266993734925836289",
  "text" : "RT @joshsusser: If you don't hate time zones, you're not a real programmer.",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "257725572275376128",
    "text" : "If you don't hate time zones, you're not a real programmer.",
    "id" : 257725572275376128,
    "created_at" : "Mon Oct 15 06:12:19 +0000 2012",
    "user" : {
      "name" : "Josh Susser",
      "screen_name" : "joshsusser",
      "protected" : false,
      "id_str" : "35954885",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3467390000/83de72f0fda7f2ebd7e60658bb6257f2_normal.png",
      "id" : 35954885,
      "verified" : false
    }
  },
  "id" : 266993734925836289,
  "created_at" : "Fri Nov 09 20:00:41 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "awesome",
      "indices" : [ 21, 29 ]
    } ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/Za6ZoDgv",
      "expanded_url" : "http://files.edanhewitt.com/",
      "display_url" : "files.edanhewitt.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266967387406995456",
  "text" : "http://t.co/Za6ZoDgv #awesome",
  "id" : 266967387406995456,
  "created_at" : "Fri Nov 09 18:15:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "adn",
      "indices" : [ 74, 78 ]
    }, {
      "text" : "appdotnet",
      "indices" : [ 79, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/nDvk3Qhe",
      "expanded_url" : "http://r3versin.com/blog/extracting-appdotnet-post-links/",
      "display_url" : "r3versin.com/blog/extractin\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266956433701101569",
  "text" : "My new blogpost: \"Extracting AppDotNet Post Links\" \u2192 http://t.co/nDvk3Qhe #adn #appdotnet",
  "id" : 266956433701101569,
  "created_at" : "Fri Nov 09 17:32:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/2mTNk3ld",
      "expanded_url" : "http://licorize.com/read/5073576",
      "display_url" : "licorize.com/read/5073576"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266955174780739584",
  "text" : "Hacking an Android Banking Application http://t.co/2mTNk3ld",
  "id" : 266955174780739584,
  "created_at" : "Fri Nov 09 17:27:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "h5bp",
      "indices" : [ 34, 39 ]
    } ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/RRn9xKXN",
      "expanded_url" : "http://www.amazon.com/HTML5-Boilerplate-Web-Development-ebook/dp/B009RR0IFE/",
      "display_url" : "amazon.com/HTML5-Boilerpl\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266932520099147776",
  "text" : "H5BP E-book: http://t.co/RRn9xKXN #h5bp By Divya Manian ... teh awesome",
  "id" : 266932520099147776,
  "created_at" : "Fri Nov 09 15:57:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "proxy",
      "indices" : [ 34, 40 ]
    } ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/PBpH15bl",
      "expanded_url" : "http://isharefil.es/KnlE",
      "display_url" : "isharefil.es/KnlE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266920309628735489",
  "text" : "More proxies http://t.co/PBpH15bl #proxy",
  "id" : 266920309628735489,
  "created_at" : "Fri Nov 09 15:08:55 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/MPuzooqt",
      "expanded_url" : "http://post.ly/9k1dr",
      "display_url" : "post.ly/9k1dr"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266625718207774720",
  "text" : "Next time someone asks you to \"make it go viral\" http://t.co/MPuzooqt",
  "id" : 266625718207774720,
  "created_at" : "Thu Nov 08 19:38:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http://t.co/rxL7iRZL",
      "expanded_url" : "http://post.ly/9k1e7",
      "display_url" : "post.ly/9k1e7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266625594463227905",
  "text" : "Hackers steal Kim Dotcom's me.ga domain and asks for Bitcoin http://t.co/rxL7iRZL",
  "id" : 266625594463227905,
  "created_at" : "Thu Nov 08 19:37:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 4, 24 ],
      "url" : "http://t.co/DMJ6nVsC",
      "expanded_url" : "http://bt1.archive.org/hotlist.php",
      "display_url" : "bt1.archive.org/hotlist.php"
    }, {
      "indices" : [ 45, 65 ],
      "url" : "http://t.co/pUKVAuHd",
      "expanded_url" : "http://archive.org",
      "display_url" : "archive.org"
    }, {
      "indices" : [ 82, 102 ],
      "url" : "http://t.co/4m7JgTnV",
      "expanded_url" : "http://post.ly/9k1eK",
      "display_url" : "post.ly/9k1eK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266625471393976320",
  "text" : "hmm http://t.co/DMJ6nVsC doesn't really make http://t.co/pUKVAuHd look so good :( http://t.co/4m7JgTnV",
  "id" : 266625471393976320,
  "created_at" : "Thu Nov 08 19:37:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 105 ],
      "url" : "http://t.co/BsDYQM2M",
      "expanded_url" : "http://post.ly/9k1eY",
      "display_url" : "post.ly/9k1eY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266625343979413504",
  "text" : "SheetClip.js: allows you to copy/paste data from your HTML5 web app to a spreadsheet http://t.co/BsDYQM2M",
  "id" : 266625343979413504,
  "created_at" : "Thu Nov 08 19:36:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Twitter API",
      "screen_name" : "twitterapi",
      "indices" : [ 3, 14 ],
      "id_str" : "6253282",
      "id" : 6253282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 136 ],
      "url" : "https://t.co/pDyHa1kX",
      "expanded_url" : "https://dev.twitter.com/discussions/12429",
      "display_url" : "dev.twitter.com/discussions/12\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266623975814230016",
  "text" : "RT @twitterapi: We're currently experiencing an issue with Embeddable Timelines, and are working to resolve this - https://t.co/pDyHa1kX ...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 99, 120 ],
        "url" : "https://t.co/pDyHa1kX",
        "expanded_url" : "https://dev.twitter.com/discussions/12429",
        "display_url" : "dev.twitter.com/discussions/12\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "266615565169332226",
    "text" : "We're currently experiencing an issue with Embeddable Timelines, and are working to resolve this - https://t.co/pDyHa1kX  ^JC",
    "id" : 266615565169332226,
    "created_at" : "Thu Nov 08 18:57:58 +0000 2012",
    "user" : {
      "name" : "Twitter API",
      "screen_name" : "twitterapi",
      "protected" : false,
      "id_str" : "6253282",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2284174872/7df3h38zabcvjylnyfe3_normal.png",
      "id" : 6253282,
      "verified" : true
    }
  },
  "id" : 266623975814230016,
  "created_at" : "Thu Nov 08 19:31:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 38 ],
      "url" : "http://t.co/88juED3g",
      "expanded_url" : "http://post.ly/9k1dg",
      "display_url" : "post.ly/9k1dg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266608977624846336",
  "text" : "Microsoft Fanboys http://t.co/88juED3g",
  "id" : 266608977624846336,
  "created_at" : "Thu Nov 08 18:31:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/8wxtDJL1",
      "expanded_url" : "http://post.ly/9k1dE",
      "display_url" : "post.ly/9k1dE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266608475868635137",
  "text" : "What's New in jQuery UI 1.9 http://t.co/8wxtDJL1",
  "id" : 266608475868635137,
  "created_at" : "Thu Nov 08 18:29:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/BgOS1Fgp",
      "expanded_url" : "http://post.ly/9k1cb",
      "display_url" : "post.ly/9k1cb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266608348848349185",
  "text" : "Google Sets, Still Available http://t.co/BgOS1Fgp",
  "id" : 266608348848349185,
  "created_at" : "Thu Nov 08 18:29:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Taylor Jasko",
      "screen_name" : "tjasko",
      "indices" : [ 3, 10 ],
      "id_str" : "17503752",
      "id" : 17503752
    }, {
      "name" : "NetDNA.com",
      "screen_name" : "NetDNA",
      "indices" : [ 56, 63 ],
      "id_str" : "56632986",
      "id" : 56632986
    }, {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "indices" : [ 64, 71 ],
      "id_str" : "98478119",
      "id" : 98478119
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 115 ],
      "url" : "http://t.co/XW2qtWfk",
      "expanded_url" : "http://www.impressjscdn.com",
      "display_url" : "impressjscdn.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266606676474478593",
  "text" : "RT @tjasko: impress.js CDN - impress your audience with @NetDNA/@MaxCDN's super fast Network.  http://t.co/XW2qtWfk",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/tweetbot\" rel=\"nofollow\">Tweetbot for iOS</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "NetDNA.com",
        "screen_name" : "NetDNA",
        "indices" : [ 44, 51 ],
        "id_str" : "56632986",
        "id" : 56632986
      }, {
        "name" : "MaxCDN.com",
        "screen_name" : "MaxCDN",
        "indices" : [ 52, 59 ],
        "id_str" : "98478119",
        "id" : 98478119
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 83, 103 ],
        "url" : "http://t.co/XW2qtWfk",
        "expanded_url" : "http://www.impressjscdn.com",
        "display_url" : "impressjscdn.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "266580340204789760",
    "text" : "impress.js CDN - impress your audience with @NetDNA/@MaxCDN's super fast Network.  http://t.co/XW2qtWfk",
    "id" : 266580340204789760,
    "created_at" : "Thu Nov 08 16:38:00 +0000 2012",
    "user" : {
      "name" : "Taylor Jasko",
      "screen_name" : "tjasko",
      "protected" : false,
      "id_str" : "17503752",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1177887607/c4291dd2586acaed3498f2946c9afe1c_normal.png",
      "id" : 17503752,
      "verified" : false
    }
  },
  "id" : 266606676474478593,
  "created_at" : "Thu Nov 08 18:22:39 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/3neSDvsQ",
      "expanded_url" : "http://rlemon.github.com/lememe/",
      "display_url" : "rlemon.github.com/lememe/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266600671376203776",
  "text" : "leMEME - Open source meme generator - http://t.co/3neSDvsQ",
  "id" : 266600671376203776,
  "created_at" : "Thu Nov 08 17:58:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hakim El Hattab",
      "screen_name" : "hakimel",
      "indices" : [ 85, 93 ],
      "id_str" : "73339662",
      "id" : 73339662
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http://t.co/6pn1mg5L",
      "expanded_url" : "http://flattr.com/t/44570",
      "display_url" : "flattr.com/t/44570"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266290322479386625",
  "text" : "I just flattred \"The Visual Experiments of Hakim El Hattab\" http://t.co/6pn1mg5L cc: @hakimel",
  "id" : 266290322479386625,
  "created_at" : "Wed Nov 07 21:25:34 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 76 ],
      "url" : "https://t.co/TPaRLRYu",
      "expanded_url" : "https://flattr.com/profile/davidhiggins",
      "display_url" : "flattr.com/profile/davidh\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266280752616980481",
  "text" : "Anyone on Flattr? Mind following? I love this service. https://t.co/TPaRLRYu",
  "id" : 266280752616980481,
  "created_at" : "Wed Nov 07 20:47:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 139 ],
      "url" : "https://t.co/r6WjaRxB",
      "expanded_url" : "https://alpha.app.net/dh/post/1436934",
      "display_url" : "alpha.app.net/dh/post/1436934"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266280501285896192",
  "text" : "Diving into the Flattr thing at the moment. Slapping buttons on everything. Never thought of slapping the buttons in\u2026 https://t.co/r6WjaRxB",
  "id" : 266280501285896192,
  "created_at" : "Wed Nov 07 20:46:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "badassery",
      "indices" : [ 90, 100 ]
    } ],
    "urls" : [ {
      "indices" : [ 69, 89 ],
      "url" : "http://t.co/EOxBEpKv",
      "expanded_url" : "http://cssdeck.com/labs/nice-sparkle-progress-bars",
      "display_url" : "cssdeck.com/labs/nice-spar\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266277990843957248",
  "text" : "Every now and then I tweet a super badass link. This is such a link: http://t.co/EOxBEpKv #badassery I want them somewhere in my app!",
  "id" : 266277990843957248,
  "created_at" : "Wed Nov 07 20:36:34 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 94 ],
      "url" : "http://t.co/r7MmntFX",
      "expanded_url" : "http://isharefil.es/Kk47",
      "display_url" : "isharefil.es/Kk47"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266276095698010112",
  "text" : "Here's some proxies if like cracking, and general internets Skullduggery: http://t.co/r7MmntFX",
  "id" : 266276095698010112,
  "created_at" : "Wed Nov 07 20:29:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lars Jung",
      "screen_name" : "lrsjng",
      "indices" : [ 0, 7 ],
      "id_str" : "91685967",
      "id" : 91685967
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "266254265331380225",
  "geo" : {
  },
  "id_str" : "266266019801223170",
  "in_reply_to_user_id" : 91685967,
  "text" : "@lrsjng No probs man. Really dig your stuff.",
  "id" : 266266019801223170,
  "in_reply_to_status_id" : 266254265331380225,
  "created_at" : "Wed Nov 07 19:49:00 +0000 2012",
  "in_reply_to_screen_name" : "lrsjng",
  "in_reply_to_user_id_str" : "91685967",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 64, 71 ]
    }, {
      "text" : "plugins",
      "indices" : [ 72, 80 ]
    } ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/epWmCJmT",
      "expanded_url" : "http://iwantaneff.in/repo/",
      "display_url" : "iwantaneff.in/repo/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266249287778852865",
  "text" : "The Handpicked jQuery Plugins Repository \u2192 http://t.co/epWmCJmT #jquery #plugins",
  "id" : 266249287778852865,
  "created_at" : "Wed Nov 07 18:42:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lars Jung",
      "screen_name" : "lrsjng",
      "indices" : [ 101, 108 ],
      "id_str" : "91685967",
      "id" : 91685967
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 133 ],
      "url" : "https://t.co/lkGJBRGs",
      "expanded_url" : "https://flattr.com/thing/398691/wepp-a-node-based-LessCSS-and-JavaScript-Preprocessor",
      "display_url" : "flattr.com/thing/398691/w\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266244308582162433",
  "text" : "\"wepp \u00B7 a node based Less/CSS and JavaScript Preprocessor\" is awesome, so I sent a Flattr payment to @lrsjng -  https://t.co/lkGJBRGs",
  "id" : 266244308582162433,
  "created_at" : "Wed Nov 07 18:22:44 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 115, 131 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 110 ],
      "url" : "https://t.co/sy6eeIVz",
      "expanded_url" : "https://flattr.com/thing/233571/jalbertbowdenii-on-Flattr",
      "display_url" : "flattr.com/thing/233571/j\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266240389084753920",
  "text" : "\"jalbertbowdenii on Flattr\" is awesome, so I sent a Flattr payment to jalbertbowdenii -  https://t.co/sy6eeIVz cc: @jalbertbowdenii",
  "id" : 266240389084753920,
  "created_at" : "Wed Nov 07 18:07:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 84 ],
      "url" : "http://t.co/v7bkItkz",
      "expanded_url" : "http://www.thingiverse.com/jalbertbowdenii",
      "display_url" : "thingiverse.com/jalbertbowdenii"
    } ]
  },
  "geo" : {
  },
  "id_str" : "266167767043014656",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii Surprised you're not registered on this too ;) http://t.co/v7bkItkz",
  "id" : 266167767043014656,
  "created_at" : "Wed Nov 07 13:18:35 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "embeducation",
      "screen_name" : "AWSOMEDEVSIGNER",
      "indices" : [ 0, 16 ],
      "id_str" : "83013622",
      "id" : 83013622
    }, {
      "name" : "Justin Angel",
      "screen_name" : "JustinAngel",
      "indices" : [ 17, 29 ],
      "id_str" : "15977110",
      "id" : 15977110
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "265854455612841984",
  "geo" : {
  },
  "id_str" : "265954636022808576",
  "in_reply_to_user_id" : 83013622,
  "text" : "@AWSOMEDEVSIGNER @JustinAngel What malware is brave enough to try and bypass file+folder security policies and access rights\u203DNot easy to do\u2049",
  "id" : 265954636022808576,
  "in_reply_to_status_id" : 265854455612841984,
  "created_at" : "Tue Nov 06 23:11:41 +0000 2012",
  "in_reply_to_screen_name" : "AWSOMEDEVSIGNER",
  "in_reply_to_user_id_str" : "83013622",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "265946901457817600",
  "geo" : {
  },
  "id_str" : "265947748862410752",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog LOLWUT. Looks like it's still going. Last post is Thursday, November 1st, 2012 I think it's more \uD835\uDE2A\uD835\uDE2F\uD835\uDE22\uD835\uDE24\uD835\uDE35\uD835\uDE2A\uD835\uDE37\uD835\uDE26 than gone",
  "id" : 265947748862410752,
  "in_reply_to_status_id" : 265946901457817600,
  "created_at" : "Tue Nov 06 22:44:19 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Orbuch",
      "screen_name" : "orbuch",
      "indices" : [ 0, 7 ],
      "id_str" : "432171732",
      "id" : 432171732
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/PWEqO2rg",
      "expanded_url" : "http://isharefil.es/KhMN/o/",
      "display_url" : "isharefil.es/KhMN/o/"
    } ]
  },
  "in_reply_to_status_id_str" : "265938050671058944",
  "geo" : {
  },
  "id_str" : "265945878722924544",
  "in_reply_to_user_id" : 432171732,
  "text" : "@orbuch Here's your answer: http://t.co/PWEqO2rg Doesn't make a difference ;)",
  "id" : 265945878722924544,
  "in_reply_to_status_id" : 265938050671058944,
  "created_at" : "Tue Nov 06 22:36:53 +0000 2012",
  "in_reply_to_screen_name" : "orbuch",
  "in_reply_to_user_id_str" : "432171732",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 138 ],
      "url" : "https://t.co/vEs8H9tu",
      "expanded_url" : "https://alpha.app.net/dh/post/1419831",
      "display_url" : "alpha.app.net/dh/post/1419831"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265939039729876992",
  "text" : "Noticed on Twitter when you mention bitcoin, a torrent of followers start following you. So lets say it three times\u2026 https://t.co/vEs8H9tu",
  "id" : 265939039729876992,
  "created_at" : "Tue Nov 06 22:09:42 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/TCVzhVzU",
      "expanded_url" : "http://davidhiggins.me/adn/",
      "display_url" : "davidhiggins.me/adn/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265936394118119424",
  "text" : "Program that generates a list of all ADN post URLs: http://t.co/TCVzhVzU",
  "id" : 265936394118119424,
  "created_at" : "Tue Nov 06 21:59:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SHnews",
      "indices" : [ 112, 119 ]
    } ],
    "urls" : [ {
      "indices" : [ 91, 111 ],
      "url" : "http://t.co/PiOq14mI",
      "expanded_url" : "http://post.ly/9j9C5",
      "display_url" : "post.ly/9j9C5"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265909082056179712",
  "text" : "Westinghouse picks McAfee to protect nuclear control systems \u00AB Waterfall Security Solution http://t.co/PiOq14mI #SHnews",
  "id" : 265909082056179712,
  "created_at" : "Tue Nov 06 20:10:40 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 87 ],
      "url" : "http://t.co/LyYOOGwN",
      "expanded_url" : "http://post.ly/9j9G0",
      "display_url" : "post.ly/9j9G0"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265908833703055362",
  "text" : "Magic keywords on Google and the consequences of tailoring results http://t.co/LyYOOGwN",
  "id" : 265908833703055362,
  "created_at" : "Tue Nov 06 20:09:40 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 121 ],
      "url" : "http://t.co/cfFoQatt",
      "expanded_url" : "http://www.snapfiles.com/featured/525552-113255.html",
      "display_url" : "snapfiles.com/featured/52555\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265854016708280320",
  "text" : "Start8 brings back the traditional Start Menu and Start button that have been omitted from Windows 8 http://t.co/cfFoQatt",
  "id" : 265854016708280320,
  "created_at" : "Tue Nov 06 16:31:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/vfRuF8Pc",
      "expanded_url" : "http://airbnb.github.com/infinity/#",
      "display_url" : "airbnb.github.com/infinity/#"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265846087775051778",
  "text" : "Infinity.js http://t.co/vfRuF8Pc",
  "id" : 265846087775051778,
  "created_at" : "Tue Nov 06 16:00:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/WhUEsaIr",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/fKvoWPeY",
      "expanded_url" : "http://voidfiles.github.com/share-on-adn/",
      "display_url" : "voidfiles.github.com/share-on-adn/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265845835261173761",
  "text" : "Share links on http://t.co/WhUEsaIr http://t.co/fKvoWPeY",
  "id" : 265845835261173761,
  "created_at" : "Tue Nov 06 15:59:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 122 ],
      "url" : "http://t.co/7zgeDrM2",
      "expanded_url" : "http://post.ly/9j9CM",
      "display_url" : "post.ly/9j9CM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265841932385779714",
  "text" : "Do people still not know where to go vote at? Then you are in luck because Google saves the day again http://t.co/7zgeDrM2",
  "id" : 265841932385779714,
  "created_at" : "Tue Nov 06 15:43:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Chrome",
      "indices" : [ 14, 21 ]
    }, {
      "text" : "Developer",
      "indices" : [ 22, 32 ]
    }, {
      "text" : "Tools",
      "indices" : [ 33, 39 ]
    } ],
    "urls" : [ {
      "indices" : [ 95, 115 ],
      "url" : "http://t.co/HxG1079K",
      "expanded_url" : "http://post.ly/9j9Dp",
      "display_url" : "post.ly/9j9Dp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265841806619594753",
  "text" : "Short post on #Chrome #Developer #Tools: Search or navigate to files, methods and line numbers http://t.co/HxG1079K",
  "id" : 265841806619594753,
  "created_at" : "Tue Nov 06 15:43:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/C7ES0XZH",
      "expanded_url" : "http://post.ly/9j9De",
      "display_url" : "post.ly/9j9De"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265841681608343552",
  "text" : "Bring the Start menu back to Windows 8 http://t.co/C7ES0XZH",
  "id" : 265841681608343552,
  "created_at" : "Tue Nov 06 15:42:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/I2a4yjLi",
      "expanded_url" : "http://post.ly/9j9EN",
      "display_url" : "post.ly/9j9EN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265841681457369088",
  "text" : "Stolen Money on Gittip http://t.co/I2a4yjLi",
  "id" : 265841681457369088,
  "created_at" : "Tue Nov 06 15:42:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/iUOqNKaI",
      "expanded_url" : "http://post.ly/9j9Fg",
      "display_url" : "post.ly/9j9Fg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265841557243047937",
  "text" : "Web MIDI API - native nostalgic support http://t.co/iUOqNKaI",
  "id" : 265841557243047937,
  "created_at" : "Tue Nov 06 15:42:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http://t.co/1Plsdy9g",
      "expanded_url" : "http://post.ly/9j9GK",
      "display_url" : "post.ly/9j9GK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265841556324483072",
  "text" : "Icon Moon icon fonts http://t.co/1Plsdy9g",
  "id" : 265841556324483072,
  "created_at" : "Tue Nov 06 15:42:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 117 ],
      "url" : "http://t.co/PV7p0tHq",
      "expanded_url" : "http://post.ly/9j9Gc",
      "display_url" : "post.ly/9j9Gc"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265841430327607297",
  "text" : "Remember that the Mozilla Marketplace is now *OPEN*. The sooner you get your app in, the better! http://t.co/PV7p0tHq",
  "id" : 265841430327607297,
  "created_at" : "Tue Nov 06 15:41:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christina Warren",
      "screen_name" : "film_girl",
      "indices" : [ 77, 87 ],
      "id_str" : "9866582",
      "id" : 9866582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/7B5XS7f2",
      "expanded_url" : "http://post.ly/9j9Gl",
      "display_url" : "post.ly/9j9Gl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265841257253847040",
  "text" : "Bitcoin: How the Internet Created Its Own Currency  http://t.co/7B5XS7f2 via @film_girl",
  "id" : 265841257253847040,
  "created_at" : "Tue Nov 06 15:41:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/5t5CwYMr",
      "expanded_url" : "http://post.ly/9j9Gz",
      "display_url" : "post.ly/9j9Gz"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265841052185944064",
  "text" : "Remote File Inclusion Explained http://t.co/5t5CwYMr",
  "id" : 265841052185944064,
  "created_at" : "Tue Nov 06 15:40:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 78 ],
      "url" : "https://t.co/VnfTNBUD",
      "expanded_url" : "https://www.detectify.com/",
      "display_url" : "detectify.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265837531004821504",
  "text" : "Detectify analyzes the level of security of your website https://t.co/VnfTNBUD",
  "id" : 265837531004821504,
  "created_at" : "Tue Nov 06 15:26:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "facebook",
      "indices" : [ 85, 94 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "265834634057756672",
  "text" : "If I see fb.me in the URL I never click, as it leads into the marshland of Facebook. #facebook",
  "id" : 265834634057756672,
  "created_at" : "Tue Nov 06 15:14:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/4Kl7MBhY",
      "expanded_url" : "http://davidhiggins.me/",
      "display_url" : "davidhiggins.me"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265518050630041601",
  "text" : "Need some feedback on the new design \u203D http://t.co/4Kl7MBhY",
  "id" : 265518050630041601,
  "created_at" : "Mon Nov 05 18:16:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/e4y4KXoF",
      "expanded_url" : "http://syn.edanhewitt.com/post/33195603595/iwantaneff-in",
      "display_url" : "syn.edanhewitt.com/post/331956035\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265502947310460930",
  "text" : "All the tools on The Motherfucking Webdev Toolset \u2192 http://t.co/e4y4KXoF",
  "id" : 265502947310460930,
  "created_at" : "Mon Nov 05 17:16:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/xNa8i8g4",
      "expanded_url" : "http://syn.edanhewitt.com/post/33195511116/various-projects",
      "display_url" : "syn.edanhewitt.com/post/331955111\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265502698093285376",
  "text" : "Various Projects http://t.co/xNa8i8g4",
  "id" : 265502698093285376,
  "created_at" : "Mon Nov 05 17:15:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/yeMca78J",
      "expanded_url" : "http://syn.edanhewitt.com/post/33195359666/profiles-and-such",
      "display_url" : "syn.edanhewitt.com/post/331953596\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265502569634361344",
  "text" : "All my profiles: http://t.co/yeMca78J",
  "id" : 265502569634361344,
  "created_at" : "Mon Nov 05 17:15:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/546zxT1A",
      "expanded_url" : "http://iwantaneff.in/bin/view/raw/a12217d3",
      "display_url" : "iwantaneff.in/bin/view/raw/a\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265502319444103168",
  "text" : "I subscribe to all these RSS feeds: http://t.co/546zxT1A",
  "id" : 265502319444103168,
  "created_at" : "Mon Nov 05 17:14:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Julien Chaumond",
      "screen_name" : "julien_c",
      "indices" : [ 0, 9 ],
      "id_str" : "16141659",
      "id" : 16141659
    }, {
      "name" : "Circular",
      "screen_name" : "CircularIO",
      "indices" : [ 10, 21 ],
      "id_str" : "722209052",
      "id" : 722209052
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AppDotNet",
      "indices" : [ 52, 62 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "265499311301869568",
  "in_reply_to_user_id" : 16141659,
  "text" : "@julien_c @CircularIO Maybe you can add support for #AppDotNet ? I would love that ;)",
  "id" : 265499311301869568,
  "created_at" : "Mon Nov 05 17:02:23 +0000 2012",
  "in_reply_to_screen_name" : "julien_c",
  "in_reply_to_user_id_str" : "16141659",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 138 ],
      "url" : "https://t.co/Fi6oPUfc",
      "expanded_url" : "https://alpha.app.net/dh/post/1395959",
      "display_url" : "alpha.app.net/dh/post/1395959"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265498797021466624",
  "text" : "URL Shorteners ow\uFE12ly | lnk\uFE12co | g\uFE12co | youtu\uFE12be | to\uFE12ly | hex\uFE12io | yi\uFE12tl | bit\uFE12do | v\uFE12gd | awe\uFE12sm| 3\uFE12ly | mtro\uFE12us |\u2026 https://t.co/Fi6oPUfc",
  "id" : 265498797021466624,
  "created_at" : "Mon Nov 05 17:00:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 139 ],
      "url" : "https://t.co/WFuVUFdC",
      "expanded_url" : "https://alpha.app.net/dh/post/1395932",
      "display_url" : "alpha.app.net/dh/post/1395932"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265498542322364416",
  "text" : "URL Shorteners (Part 1) ow\uFE12ly | lnk\uFE12co | g\uFE12co | youtu\uFE12be | to\uFE12ly | hex\uFE12io | yi\uFE12tl | bit\uFE12do | v\uFE12gd | awe\uFE12sm| 3\uFE12ly\nURL\u2026 https://t.co/WFuVUFdC",
  "id" : 265498542322364416,
  "created_at" : "Mon Nov 05 16:59:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html5",
      "indices" : [ 69, 75 ]
    }, {
      "text" : "jquery",
      "indices" : [ 76, 83 ]
    } ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/J5wEmx8K",
      "expanded_url" : "http://ericleads.com/h5validate/",
      "display_url" : "ericleads.com/h5validate/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265497535701983234",
  "text" : "h5Validate - HTML5 Form Validation for jQuery - http://t.co/J5wEmx8K #html5 #jquery",
  "id" : 265497535701983234,
  "created_at" : "Mon Nov 05 16:55:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twapp.phuu.net\" rel=\"nofollow\">Twapp for App.net</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 46 ],
      "url" : "https://t.co/9XYlDMUP",
      "expanded_url" : "https://directory.app.net/app/81/firepost/",
      "display_url" : "directory.app.net/app/81/firepos\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265491620508606465",
  "text" : "Really digging Firepost: https://t.co/9XYlDMUP",
  "id" : 265491620508606465,
  "created_at" : "Mon Nov 05 16:31:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Ashworth",
      "screen_name" : "phuunet",
      "indices" : [ 60, 68 ],
      "id_str" : "7054122",
      "id" : 7054122
    }, {
      "name" : "Gumroad",
      "screen_name" : "gumroad",
      "indices" : [ 72, 80 ],
      "id_str" : "276271004",
      "id" : 276271004
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/WhUEsaIr",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 82, 102 ],
      "url" : "http://t.co/Im5eK6XX",
      "expanded_url" : "http://gum.co/UVxX",
      "display_url" : "gum.co/UVxX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265490073120804864",
  "text" : "I just got Twapp: from http://t.co/WhUEsaIr to Twitter from @phuunet on @Gumroad: http://t.co/Im5eK6XX",
  "id" : 265490073120804864,
  "created_at" : "Mon Nov 05 16:25:40 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Julien Chaumond",
      "screen_name" : "julien_c",
      "indices" : [ 0, 9 ],
      "id_str" : "16141659",
      "id" : 16141659
    }, {
      "name" : "Circular",
      "screen_name" : "CircularIO",
      "indices" : [ 10, 21 ],
      "id_str" : "722209052",
      "id" : 722209052
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "265461425928015872",
  "in_reply_to_user_id" : 16141659,
  "text" : "@julien_c @CircularIO Well done on Circular. It is so awesome. Saves me a lot of money not having to buy a plan on Bufferapp. +1",
  "id" : 265461425928015872,
  "created_at" : "Mon Nov 05 14:31:50 +0000 2012",
  "in_reply_to_screen_name" : "julien_c",
  "in_reply_to_user_id_str" : "16141659",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/L1qqk77e",
      "expanded_url" : "http://post.ly/9ihXM",
      "display_url" : "post.ly/9ihXM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "265429027882409984",
  "text" : "Skittles Sorting Machine \u2192 http://t.co/L1qqk77e",
  "id" : 265429027882409984,
  "created_at" : "Mon Nov 05 12:23:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex Russell",
      "screen_name" : "slightlylate",
      "indices" : [ 3, 16 ],
      "id_str" : "229237555",
      "id" : 229237555
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "265428490084548608",
  "text" : "RT @slightlylate: Dear WebAPI designers: no, other languages do not want crap versions of your API. Can we stop screwing JS users now? h ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 137 ],
        "url" : "http://t.co/Ptyn55G7",
        "expanded_url" : "http://lists.w3.org/Archives/Public/public-script-coord/2012OctDec/0120.html",
        "display_url" : "lists.w3.org/Archives/Publi\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "265425772951511041",
    "text" : "Dear WebAPI designers: no, other languages do not want crap versions of your API. Can we stop screwing JS users now? http://t.co/Ptyn55G7",
    "id" : 265425772951511041,
    "created_at" : "Mon Nov 05 12:10:10 +0000 2012",
    "user" : {
      "name" : "Alex Russell",
      "screen_name" : "slightlylate",
      "protected" : false,
      "id_str" : "229237555",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3358034410/9c6ec42107a2a4492cdc916b8289564d_normal.jpeg",
      "id" : 229237555,
      "verified" : false
    }
  },
  "id" : 265428490084548608,
  "created_at" : "Mon Nov 05 12:20:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maximiliano Firtman",
      "screen_name" : "firt",
      "indices" : [ 3, 8 ],
      "id_str" : "17509972",
      "id" : 17509972
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "265426730372722689",
  "text" : "RT @firt: Confirmed that the iPad Mini User Agent is exactly the same as the iPad; no way to recognize if the user is on a Mini",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/software/tweetbot/mac\" rel=\"nofollow\">Tweetbot for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "264766828864208896",
    "text" : "Confirmed that the iPad Mini User Agent is exactly the same as the iPad; no way to recognize if the user is on a Mini",
    "id" : 264766828864208896,
    "created_at" : "Sat Nov 03 16:31:45 +0000 2012",
    "user" : {
      "name" : "Maximiliano Firtman",
      "screen_name" : "firt",
      "protected" : false,
      "id_str" : "17509972",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000126040749/f24b1d5414d4a6eedd225796b7229b75_normal.jpeg",
      "id" : 17509972,
      "verified" : false
    }
  },
  "id" : 265426730372722689,
  "created_at" : "Mon Nov 05 12:13:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Xylitol",
      "screen_name" : "Xylit0l",
      "indices" : [ 3, 11 ],
      "id_str" : "80275945",
      "id" : 80275945
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "265209363113844736",
  "text" : "RT @Xylit0l: hmm when i see all these leaked crimewares on underground boards... 2012 is the year of leaked stuff.",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "265208076502695936",
    "text" : "hmm when i see all these leaked crimewares on underground boards... 2012 is the year of leaked stuff.",
    "id" : 265208076502695936,
    "created_at" : "Sun Nov 04 21:45:07 +0000 2012",
    "user" : {
      "name" : "Xylitol",
      "screen_name" : "Xylit0l",
      "protected" : false,
      "id_str" : "80275945",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1376716218/cat_normal.jpg",
      "id" : 80275945,
      "verified" : false
    }
  },
  "id" : 265209363113844736,
  "created_at" : "Sun Nov 04 21:50:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "265191900095590400",
  "text" : "When you don't understand a system as complex as the modern day OS, your un-tech-savvy family will botnet you quicker than the word McAfee",
  "id" : 265191900095590400,
  "created_at" : "Sun Nov 04 20:40:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "265191524810235904",
  "text" : "TIL - Windows and Linux are not built for Everyday Joe families. All sorts of bad shit happens on the OS. Give families an iPad == Happiness",
  "id" : 265191524810235904,
  "created_at" : "Sun Nov 04 20:39:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "265162714693914624",
  "geo" : {
  },
  "id_str" : "265189971160338433",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii Yeah. Once you get your head around Shadow DOM, you could potentially work for the Chrome Team, and upgloss thyself.",
  "id" : 265189971160338433,
  "in_reply_to_status_id" : 265162714693914624,
  "created_at" : "Sun Nov 04 20:33:10 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pippinsplugins",
      "screen_name" : "pippinsplugins",
      "indices" : [ 0, 15 ],
      "id_str" : "294079511",
      "id" : 294079511
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "264865753046601730",
  "geo" : {
  },
  "id_str" : "265048463971057664",
  "in_reply_to_user_id" : 294079511,
  "text" : "@pippinsplugins I took it down. So sorry about that. You really should release it properly on CC though ;)",
  "id" : 265048463971057664,
  "in_reply_to_status_id" : 264865753046601730,
  "created_at" : "Sun Nov 04 11:10:52 +0000 2012",
  "in_reply_to_screen_name" : "pippinsplugins",
  "in_reply_to_user_id_str" : "294079511",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pippinsplugins",
      "screen_name" : "pippinsplugins",
      "indices" : [ 0, 15 ],
      "id_str" : "294079511",
      "id" : 294079511
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/CPiFin91",
      "expanded_url" : "http://iwantaneff.in/bin/view/raw/afe4d34e",
      "display_url" : "iwantaneff.in/bin/view/raw/a\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "264501279450091520",
  "geo" : {
  },
  "id_str" : "264864978148945920",
  "in_reply_to_user_id" : 294079511,
  "text" : "@pippinsplugins http://t.co/CPiFin91",
  "id" : 264864978148945920,
  "in_reply_to_status_id" : 264501279450091520,
  "created_at" : "Sat Nov 03 23:01:46 +0000 2012",
  "in_reply_to_screen_name" : "pippinsplugins",
  "in_reply_to_user_id_str" : "294079511",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "laterstars",
      "screen_name" : "laterstars",
      "indices" : [ 3, 14 ],
      "id_str" : "105892117",
      "id" : 105892117
    }, {
      "name" : "Phil Ripperger",
      "screen_name" : "pdsphil",
      "indices" : [ 64, 72 ],
      "id_str" : "641213",
      "id" : 641213
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "264842898535743488",
  "text" : "RT @laterstars: welp, the time has come to start shutting down. @pdsphil wrote a post about why we're doing it and how it's going down.  ...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Phil Ripperger",
        "screen_name" : "pdsphil",
        "indices" : [ 48, 56 ],
        "id_str" : "641213",
        "id" : 641213
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 120, 140 ],
        "url" : "http://t.co/xJbQRBYk",
        "expanded_url" : "http://blog.laterstars.com/post/29658178255/laterstars-is-shutting-down",
        "display_url" : "blog.laterstars.com/post/296581782\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "236632657277091840",
    "text" : "welp, the time has come to start shutting down. @pdsphil wrote a post about why we're doing it and how it's going down. http://t.co/xJbQRBYk",
    "id" : 236632657277091840,
    "created_at" : "Sat Aug 18 01:16:36 +0000 2012",
    "user" : {
      "name" : "laterstars",
      "screen_name" : "laterstars",
      "protected" : false,
      "id_str" : "105892117",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/956505520/fluidicon_normal.png",
      "id" : 105892117,
      "verified" : false
    }
  },
  "id" : 264842898535743488,
  "created_at" : "Sat Nov 03 21:34:02 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "maria m m",
      "screen_name" : "PistachioPony",
      "indices" : [ 43, 57 ],
      "id_str" : "360848817",
      "id" : 360848817
    }, {
      "name" : "FuckingPollingPlace",
      "screen_name" : "fnpollingplace",
      "indices" : [ 58, 73 ],
      "id_str" : "210909057",
      "id" : 210909057
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 38 ],
      "url" : "http://t.co/i40sd9Wm",
      "expanded_url" : "http://post.ly/9i8nQ",
      "display_url" : "post.ly/9i8nQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "264834207354875904",
  "text" : "IT IS FUCKING ON! http://t.co/i40sd9Wm via @PistachioPony @fnpollingplace",
  "id" : 264834207354875904,
  "created_at" : "Sat Nov 03 20:59:30 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Taylor Jasko",
      "screen_name" : "tjasko",
      "indices" : [ 108, 115 ],
      "id_str" : "17503752",
      "id" : 17503752
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 103 ],
      "url" : "http://t.co/e5d4hDKZ",
      "expanded_url" : "http://post.ly/9i8nJ",
      "display_url" : "post.ly/9i8nJ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "264834002848997376",
  "text" : "First pack of stamps I have ever bought in my life! Guess what I'm using them for? http://t.co/e5d4hDKZ via @tjasko",
  "id" : 264834002848997376,
  "created_at" : "Sat Nov 03 20:58:41 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Irish",
      "screen_name" : "paul_irish",
      "indices" : [ 56, 67 ],
      "id_str" : "1671811",
      "id" : 1671811
    }, {
      "name" : "Abraham Williams",
      "screen_name" : "abraham",
      "indices" : [ 68, 76 ],
      "id_str" : "9436992",
      "id" : 9436992
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/V4sckxBQ",
      "expanded_url" : "http://post.ly/9i8ll",
      "display_url" : "post.ly/9i8ll"
    } ]
  },
  "geo" : {
  },
  "id_str" : "264833636128403458",
  "text" : "Chrome Canary for Developers \u2192 http://t.co/V4sckxBQ via @paul_irish @abraham",
  "id" : 264833636128403458,
  "created_at" : "Sat Nov 03 20:57:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Abraham Williams",
      "screen_name" : "abraham",
      "indices" : [ 65, 73 ],
      "id_str" : "9436992",
      "id" : 9436992
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/nDDICAMT",
      "expanded_url" : "http://post.ly/9i8km",
      "display_url" : "post.ly/9i8km"
    } ]
  },
  "geo" : {
  },
  "id_str" : "264833216320516096",
  "text" : "What Makes a Great API? The Five Keys \u2192 http://t.co/nDDICAMT via @abraham",
  "id" : 264833216320516096,
  "created_at" : "Sat Nov 03 20:55:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 55 ],
      "url" : "https://t.co/tnpaj1zO",
      "expanded_url" : "https://twitter.com/favorites",
      "display_url" : "twitter.com/favorites"
    } ]
  },
  "geo" : {
  },
  "id_str" : "264832704414117888",
  "text" : "I just found this cool new site \u2192 https://t.co/tnpaj1zO",
  "id" : 264832704414117888,
  "created_at" : "Sat Nov 03 20:53:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "264831050662961153",
  "text" : "My perfect command of the English language means; I can forever win every argument you put forth to me, however fortified your pleb slang ;)",
  "id" : 264831050662961153,
  "created_at" : "Sat Nov 03 20:46:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Taylor Jasko",
      "screen_name" : "tjasko",
      "indices" : [ 0, 7 ],
      "id_str" : "17503752",
      "id" : 17503752
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "264810489035509762",
  "geo" : {
  },
  "id_str" : "264828065882976256",
  "in_reply_to_user_id" : 17503752,
  "text" : "@tjasko Ironic tweet + photo is ironic.",
  "id" : 264828065882976256,
  "in_reply_to_status_id" : 264810489035509762,
  "created_at" : "Sat Nov 03 20:35:05 +0000 2012",
  "in_reply_to_screen_name" : "tjasko",
  "in_reply_to_user_id_str" : "17503752",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "integralist",
      "screen_name" : "integralist",
      "indices" : [ 0, 12 ],
      "id_str" : "18293449",
      "id" : 18293449
    }, {
      "name" : "James Padolsey",
      "screen_name" : "padolsey",
      "indices" : [ 75, 84 ],
      "id_str" : "17107025",
      "id" : 17107025
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "264773038980096002",
  "geo" : {
  },
  "id_str" : "264799168592224256",
  "in_reply_to_user_id" : 18293449,
  "text" : "@integralist Nice! Love the design and content. The BG image reminds me of @padolsey's site ;)",
  "id" : 264799168592224256,
  "in_reply_to_status_id" : 264773038980096002,
  "created_at" : "Sat Nov 03 18:40:16 +0000 2012",
  "in_reply_to_screen_name" : "integralist",
  "in_reply_to_user_id_str" : "18293449",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jan Odvarko",
      "screen_name" : "firebugnews",
      "indices" : [ 3, 15 ],
      "id_str" : "21410654",
      "id" : 21410654
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 101 ],
      "url" : "http://t.co/lVuqsCap",
      "expanded_url" : "http://bit.ly/Vjlpnn",
      "display_url" : "bit.ly/Vjlpnn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "264488658944548864",
  "text" : "RT @firebugnews: Firebug 1.11 alpha 6 released. Check out new include() command! http://t.co/lVuqsCap",
  "retweeted_status" : {
    "source" : "<a href=\"http://bitly.com\" rel=\"nofollow\">bitly</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 84 ],
        "url" : "http://t.co/lVuqsCap",
        "expanded_url" : "http://bit.ly/Vjlpnn",
        "display_url" : "bit.ly/Vjlpnn"
      } ]
    },
    "geo" : {
    },
    "id_str" : "264422730865201152",
    "text" : "Firebug 1.11 alpha 6 released. Check out new include() command! http://t.co/lVuqsCap",
    "id" : 264422730865201152,
    "created_at" : "Fri Nov 02 17:44:26 +0000 2012",
    "user" : {
      "name" : "Jan Odvarko",
      "screen_name" : "firebugnews",
      "protected" : false,
      "id_str" : "21410654",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1110465903/firebug32_normal.png",
      "id" : 21410654,
      "verified" : false
    }
  },
  "id" : 264488658944548864,
  "created_at" : "Fri Nov 02 22:06:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Evan P",
      "screen_name" : "nth_degree",
      "indices" : [ 3, 14 ],
      "id_str" : "13692462",
      "id" : 13692462
    }, {
      "name" : "App.net",
      "screen_name" : "AppDotNet",
      "indices" : [ 54, 64 ],
      "id_str" : "104558396",
      "id" : 104558396
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "264454228859289600",
  "text" : "RT @nth_degree: One of the things I am enjoying about @AppDotNet is the ability to express ideas without having to write in some cryptic ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.echofon.com/\" rel=\"nofollow\">Echofon</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "App.net",
        "screen_name" : "AppDotNet",
        "indices" : [ 38, 48 ],
        "id_str" : "104558396",
        "id" : 104558396
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "264443379629309952",
    "text" : "One of the things I am enjoying about @AppDotNet is the ability to express ideas without having to write in some cryptic shorthand.",
    "id" : 264443379629309952,
    "created_at" : "Fri Nov 02 19:06:29 +0000 2012",
    "user" : {
      "name" : "Evan P",
      "screen_name" : "nth_degree",
      "protected" : false,
      "id_str" : "13692462",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1541072042/droidberg_normal.png",
      "id" : 13692462,
      "verified" : false
    }
  },
  "id" : 264454228859289600,
  "created_at" : "Fri Nov 02 19:49:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
} ]