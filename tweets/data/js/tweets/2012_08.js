Grailbird.data.tweets_2012_08 = 
 [ {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 0, 12 ],
      "id_str" : "793830",
      "id" : 793830
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241666453001097217",
  "geo" : {
  },
  "id_str" : "241667872676519936",
  "in_reply_to_user_id" : 793830,
  "text" : "@chriscoyier I see. I am certainly for the raw Unicode approach. But there are times I like to handle it with kids gloves, i.e escaping it",
  "id" : 241667872676519936,
  "in_reply_to_status_id" : 241666453001097217,
  "created_at" : "Fri Aug 31 22:44:45 +0000 2012",
  "in_reply_to_screen_name" : "chriscoyier",
  "in_reply_to_user_id_str" : "793830",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 0, 12 ],
      "id_str" : "793830",
      "id" : 793830
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 134 ],
      "url" : "https://t.co/VbSExXoI",
      "expanded_url" : "https://twitter.com/_higg_/status/241610861213540352",
      "display_url" : "twitter.com/_higg_/status/\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241665442962350080",
  "in_reply_to_user_id" : 793830,
  "text" : "@chriscoyier Hey mate. Seems like you're broadcasting tweets, which is fine. But a quick reply would be dandy ;) https://t.co/VbSExXoI",
  "id" : 241665442962350080,
  "created_at" : "Fri Aug 31 22:35:06 +0000 2012",
  "in_reply_to_screen_name" : "chriscoyier",
  "in_reply_to_user_id_str" : "793830",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "indices" : [ 3, 10 ],
      "id_str" : "98478119",
      "id" : 98478119
    }, {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "indices" : [ 73, 80 ],
      "id_str" : "98478119",
      "id" : 98478119
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "241661750477479936",
  "text" : "RT @MaxCDN: Please RT! It's your last day to claim your free terabyte at @MaxCDN because web speed matters . Get it now! http://t.co/a36 ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.hootsuite.com\" rel=\"nofollow\">HootSuite</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "MaxCDN.com",
        "screen_name" : "MaxCDN",
        "indices" : [ 61, 68 ],
        "id_str" : "98478119",
        "id" : 98478119
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "webperf",
        "indices" : [ 130, 138 ]
      } ],
      "urls" : [ {
        "indices" : [ 109, 129 ],
        "url" : "http://t.co/a36UdqHN",
        "expanded_url" : "http://ow.ly/d6HNZ",
        "display_url" : "ow.ly/d6HNZ"
      } ]
    },
    "geo" : {
    },
    "id_str" : "241632737549762561",
    "text" : "Please RT! It's your last day to claim your free terabyte at @MaxCDN because web speed matters . Get it now! http://t.co/a36UdqHN #webperf",
    "id" : 241632737549762561,
    "created_at" : "Fri Aug 31 20:25:08 +0000 2012",
    "user" : {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "protected" : false,
      "id_str" : "98478119",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3659080717/7d2ffc9641acae25c136ad8cface3105_normal.png",
      "id" : 98478119,
      "verified" : false
    }
  },
  "id" : 241661750477479936,
  "created_at" : "Fri Aug 31 22:20:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 96, 104 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "posterous",
      "indices" : [ 105, 115 ]
    } ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/c5Ct77ks",
      "expanded_url" : "http://post.ly/976fZ",
      "display_url" : "post.ly/976fZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241661437632737281",
  "text" : "Favorited \u2605 http://t.co/c5Ct77ks \"what\u2019s the app you're using to broadcast your favorites?\" via @tompntn #posterous",
  "id" : 241661437632737281,
  "created_at" : "Fri Aug 31 22:19:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 39, 47 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ifttt",
      "indices" : [ 48, 54 ]
    } ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/DwEjxyEg",
      "expanded_url" : "http://post.ly/97673",
      "display_url" : "post.ly/97673"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241661181729841152",
  "text" : "Favorited \u2605 http://t.co/DwEjxyEg cc:// @tompntn #ifttt",
  "id" : 241661181729841152,
  "created_at" : "Fri Aug 31 22:18:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241654689609945088",
  "geo" : {
  },
  "id_str" : "241657402154418177",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn It's kind of automated ?!",
  "id" : 241657402154418177,
  "in_reply_to_status_id" : 241654689609945088,
  "created_at" : "Fri Aug 31 22:03:09 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "twitter",
      "indices" : [ 67, 75 ]
    }, {
      "text" : "bootstrap",
      "indices" : [ 76, 86 ]
    }, {
      "text" : "buttons",
      "indices" : [ 87, 95 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/NKtfyFud",
      "expanded_url" : "http://bit.ly/NdO3h7",
      "display_url" : "bit.ly/NdO3h7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241656888905834496",
  "text" : "\"Beautiful Buttons for Twitter Bootstrappers\" http://t.co/NKtfyFud #twitter #bootstrap #buttons",
  "id" : 241656888905834496,
  "created_at" : "Fri Aug 31 22:01:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "spam",
      "indices" : [ 32, 37 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 77 ],
      "url" : "http://t.co/Gx0hKn26",
      "expanded_url" : "http://ifttt.com",
      "display_url" : "ifttt.com"
    }, {
      "indices" : [ 120, 140 ],
      "url" : "http://t.co/CNuujAhi",
      "expanded_url" : "http://isharefil.es/J9G2",
      "display_url" : "isharefil.es/J9G2"
    } ]
  },
  "in_reply_to_status_id_str" : "241652097685274624",
  "geo" : {
  },
  "id_str" : "241654678880923650",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn You're right about the #spam So many just abuse http://t.co/Gx0hKn26 Its starting to infect Pinboard massively http://t.co/CNuujAhi",
  "id" : 241654678880923650,
  "in_reply_to_status_id" : 241652097685274624,
  "created_at" : "Fri Aug 31 21:52:19 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241652097685274624",
  "geo" : {
  },
  "id_str" : "241653023946973185",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn My solution was to 'handpick' those favorites that end up on the blog. I only post favorites that I like on that blog. ;)",
  "id" : 241653023946973185,
  "in_reply_to_status_id" : 241652097685274624,
  "created_at" : "Fri Aug 31 21:45:45 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "colindpritchard",
      "screen_name" : "colindpritchard",
      "indices" : [ 0, 16 ],
      "id_str" : "17592145",
      "id" : 17592145
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 137 ],
      "url" : "https://t.co/GHOXGqz7",
      "expanded_url" : "https://alpha.app.net/dh",
      "display_url" : "alpha.app.net/dh"
    } ]
  },
  "in_reply_to_status_id_str" : "241650671298965504",
  "geo" : {
  },
  "id_str" : "241652413990313984",
  "in_reply_to_user_id" : 17592145,
  "text" : "@colindpritchard If you want to be an early adopter === lots of followers. If you want a short username get it now! https://t.co/GHOXGqz7",
  "id" : 241652413990313984,
  "in_reply_to_status_id" : 241650671298965504,
  "created_at" : "Fri Aug 31 21:43:19 +0000 2012",
  "in_reply_to_screen_name" : "colindpritchard",
  "in_reply_to_user_id_str" : "17592145",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/Gx0hKn26",
      "expanded_url" : "http://ifttt.com",
      "display_url" : "ifttt.com"
    } ]
  },
  "in_reply_to_status_id_str" : "241645147899711488",
  "geo" : {
  },
  "id_str" : "241646089143472128",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn I use http://t.co/Gx0hKn26 to do many things. I don't trust it though, as it it's not premium, however, they plan on adding premium",
  "id" : 241646089143472128,
  "in_reply_to_status_id" : 241645147899711488,
  "created_at" : "Fri Aug 31 21:18:11 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/Gx0hKn26",
      "expanded_url" : "http://ifttt.com",
      "display_url" : "ifttt.com"
    } ]
  },
  "in_reply_to_status_id_str" : "241645147899711488",
  "geo" : {
  },
  "id_str" : "241645774503555073",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn http://t.co/Gx0hKn26 Is *very* powerful. As in you could favorite \u2192 Gets re-tweeted to 9000+ accounts. Those accounts \u2192 9000+ blogs",
  "id" : 241645774503555073,
  "in_reply_to_status_id" : 241645147899711488,
  "created_at" : "Fri Aug 31 21:16:56 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "personal",
      "indices" : [ 119, 128 ]
    } ],
    "urls" : [ {
      "indices" : [ 98, 118 ],
      "url" : "http://t.co/Gx0hKn26",
      "expanded_url" : "http://ifttt.com",
      "display_url" : "ifttt.com"
    } ]
  },
  "in_reply_to_status_id_str" : "241644089819426817",
  "geo" : {
  },
  "id_str" : "241645048700207104",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn I loved, then hated Posterous. Then I fell back in love with Posterous when I discovered http://t.co/Gx0hKn26 #personal preference",
  "id" : 241645048700207104,
  "in_reply_to_status_id" : 241644089819426817,
  "created_at" : "Fri Aug 31 21:14:03 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/DyJVQ7ss",
      "expanded_url" : "http://laterstars.com/_higg_",
      "display_url" : "laterstars.com/_higg_"
    } ]
  },
  "in_reply_to_status_id_str" : "241644089819426817",
  "geo" : {
  },
  "id_str" : "241644623267766273",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn It seems laterstars is for you then? http://t.co/DyJVQ7ss",
  "id" : 241644623267766273,
  "in_reply_to_status_id" : 241644089819426817,
  "created_at" : "Fri Aug 31 21:12:22 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 98 ],
      "url" : "http://t.co/Gx0hKn26",
      "expanded_url" : "http://ifttt.com",
      "display_url" : "ifttt.com"
    } ]
  },
  "in_reply_to_status_id_str" : "241643205727227904",
  "geo" : {
  },
  "id_str" : "241643547734970368",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Just dive into Posterous first. See how it works,etc Then figure out http://t.co/Gx0hKn26 Let me know when you've rigged the two up",
  "id" : 241643547734970368,
  "in_reply_to_status_id" : 241643205727227904,
  "created_at" : "Fri Aug 31 21:08:05 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "protip",
      "indices" : [ 51, 58 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241641351568044032",
  "geo" : {
  },
  "id_str" : "241643067763986432",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn You have to register with Posterous first #protip",
  "id" : 241643067763986432,
  "in_reply_to_status_id" : 241641351568044032,
  "created_at" : "Fri Aug 31 21:06:11 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "useful",
      "indices" : [ 101, 108 ]
    } ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/Zz45o7dE",
      "expanded_url" : "http://Posterous.com",
      "display_url" : "Posterous.com"
    }, {
      "indices" : [ 80, 100 ],
      "url" : "http://t.co/Gx0hKn26",
      "expanded_url" : "http://ifttt.com",
      "display_url" : "ifttt.com"
    } ]
  },
  "in_reply_to_status_id_str" : "241636606514171904",
  "geo" : {
  },
  "id_str" : "241640690763837440",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn The s\u0336e\u0336r\u0336v\u0336i\u0336c\u0336e\u0336 app I use is http://t.co/Zz45o7dE I combine it with http://t.co/Gx0hKn26 #useful ;)",
  "id" : 241640690763837440,
  "in_reply_to_status_id" : 241636606514171904,
  "created_at" : "Fri Aug 31 20:56:44 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : ".mario",
      "screen_name" : "0x6D6172696F",
      "indices" : [ 0, 13 ],
      "id_str" : "18780838",
      "id" : 18780838
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "java",
      "indices" : [ 107, 112 ]
    }, {
      "text" : "driveby",
      "indices" : [ 113, 121 ]
    }, {
      "text" : "downloads",
      "indices" : [ 122, 132 ]
    }, {
      "text" : "scary",
      "indices" : [ 133, 139 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241628778483298304",
  "geo" : {
  },
  "id_str" : "241629774076854272",
  "in_reply_to_user_id" : 18780838,
  "text" : "@0x6D6172696F Very scared, clicking on that link, when I'm in my un-sandboxed machine viewing your tweets. #java #driveby #downloads #scary",
  "id" : 241629774076854272,
  "in_reply_to_status_id" : 241628778483298304,
  "created_at" : "Fri Aug 31 20:13:22 +0000 2012",
  "in_reply_to_screen_name" : "0x6D6172696F",
  "in_reply_to_user_id_str" : "18780838",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elijah Manor",
      "screen_name" : "elijahmanor",
      "indices" : [ 60, 72 ],
      "id_str" : "9453872",
      "id" : 9453872
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jQuery",
      "indices" : [ 33, 40 ]
    } ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/e9X4A5SZ",
      "expanded_url" : "http://post.ly/972z6",
      "display_url" : "post.ly/972z6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241629081152679936",
  "text" : "Favorited \u2605 http://t.co/e9X4A5SZ #jQuery 1.8.1 Released via @elijahmanor",
  "id" : 241629081152679936,
  "created_at" : "Fri Aug 31 20:10:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Victor Lontoh",
      "screen_name" : "vlizco",
      "indices" : [ 92, 99 ],
      "id_str" : "41031976",
      "id" : 41031976
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/TbL4iRjx",
      "expanded_url" : "http://post.ly/972zN",
      "display_url" : "post.ly/972zN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241628962663583744",
  "text" : "Favorited \u2605 http://t.co/TbL4iRjx ShortScroll - A jQuery UI Google Wave style scroll bar via @vlizco",
  "id" : 241628962663583744,
  "created_at" : "Fri Aug 31 20:10:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mahroof ali",
      "screen_name" : "mahroof_ali",
      "indices" : [ 76, 88 ],
      "id_str" : "1386763519",
      "id" : 1386763519
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jQuery",
      "indices" : [ 56, 63 ]
    } ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/DNrFuRsH",
      "expanded_url" : "http://post.ly/972zW",
      "display_url" : "post.ly/972zW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241628831830654976",
  "text" : "Favorited \u2605 http://t.co/DNrFuRsH Ultimate Collection of #jQuery Plugins via @mahroof_ali",
  "id" : 241628831830654976,
  "created_at" : "Fri Aug 31 20:09:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/g7LJjsU4",
      "expanded_url" : "http://pixelhunter.me/post/30235797823/jquery-avgrund-js",
      "display_url" : "pixelhunter.me/post/302357978\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241626417136295936",
  "text" : "Pixelhunter | Front Dev | Voronianski blog http://t.co/g7LJjsU4",
  "id" : 241626417136295936,
  "created_at" : "Fri Aug 31 20:00:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dmitri Voronianski",
      "screen_name" : "voronianski",
      "indices" : [ 75, 87 ],
      "id_str" : "250538696",
      "id" : 250538696
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 50, 57 ]
    }, {
      "text" : "avgrund",
      "indices" : [ 58, 66 ]
    }, {
      "text" : "js",
      "indices" : [ 67, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/pmmz2sUO",
      "expanded_url" : "http://labs.voronianski.com/jquery.avgrund.js/",
      "display_url" : "labs.voronianski.com/jquery.avgrund\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241626332990164993",
  "text" : "Avgrund Modals jQuery Plugin http://t.co/pmmz2sUO #jquery #avgrund #js via @voronianski",
  "id" : 241626332990164993,
  "created_at" : "Fri Aug 31 19:59:41 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chef Gaby",
      "screen_name" : "DolcePixel",
      "indices" : [ 3, 14 ],
      "id_str" : "92959511",
      "id" : 92959511
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jQuery",
      "indices" : [ 16, 23 ]
    } ],
    "urls" : [ {
      "indices" : [ 62, 82 ],
      "url" : "http://t.co/Mvlb9OMb",
      "expanded_url" : "http://z8.ro/dv8",
      "display_url" : "z8.ro/dv8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241626100063694848",
  "text" : "RT @DolcePixel: #jQuery plugin for cool looking modal windows http://t.co/Mvlb9OMb",
  "retweeted_status" : {
    "source" : "<a href=\"http://z8.ro/\" rel=\"nofollow\">z8</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "jQuery",
        "indices" : [ 0, 7 ]
      } ],
      "urls" : [ {
        "indices" : [ 46, 66 ],
        "url" : "http://t.co/Mvlb9OMb",
        "expanded_url" : "http://z8.ro/dv8",
        "display_url" : "z8.ro/dv8"
      } ]
    },
    "geo" : {
    },
    "id_str" : "241502938009313281",
    "text" : "#jQuery plugin for cool looking modal windows http://t.co/Mvlb9OMb",
    "id" : 241502938009313281,
    "created_at" : "Fri Aug 31 11:49:21 +0000 2012",
    "user" : {
      "name" : "Chef Gaby",
      "screen_name" : "DolcePixel",
      "protected" : false,
      "id_str" : "92959511",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1523730439/avatar_normal.jpg",
      "id" : 92959511,
      "verified" : false
    }
  },
  "id" : 241626100063694848,
  "created_at" : "Fri Aug 31 19:58:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 101 ],
      "url" : "http://t.co/HnqhnCmD",
      "expanded_url" : "http://www.youtube.com/watch?v=eeudcFVYiPc",
      "display_url" : "youtube.com/watch?v=eeudcF\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241623672555700224",
  "text" : "Is it me, or does this song reverberate heavily with Twitter's \"Follow\" feature? http://t.co/HnqhnCmD",
  "id" : 241623672555700224,
  "created_at" : "Fri Aug 31 19:49:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 122, 134 ],
      "id_str" : "793830",
      "id" : 793830
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/oM6AAkKU",
      "expanded_url" : "http://post.ly/971Am",
      "display_url" : "post.ly/971Am"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241622318152024064",
  "text" : "Favorited \u2605 http://t.co/oM6AAkKU Duplex - Mac app that scales images up to 2x when you don\u2019t have higher res original via @chriscoyier",
  "id" : 241622318152024064,
  "created_at" : "Fri Aug 31 19:43:44 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Garry Aylott",
      "screen_name" : "GarryAylott",
      "indices" : [ 0, 12 ],
      "id_str" : "19903140",
      "id" : 19903140
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Bourne",
      "indices" : [ 42, 49 ]
    }, {
      "text" : "Legacy",
      "indices" : [ 50, 57 ]
    }, {
      "text" : "torrents",
      "indices" : [ 68, 77 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241615434149924864",
  "geo" : {
  },
  "id_str" : "241622000093777922",
  "in_reply_to_user_id" : 19903140,
  "text" : "@GarryAylott A very bad, jumpy version of #Bourne #Legacy exists on #torrents. Searching Twitter for reviews has not instilled confidence ;)",
  "id" : 241622000093777922,
  "in_reply_to_status_id" : 241615434149924864,
  "created_at" : "Fri Aug 31 19:42:28 +0000 2012",
  "in_reply_to_screen_name" : "GarryAylott",
  "in_reply_to_user_id_str" : "19903140",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "indices" : [ 3, 10 ],
      "id_str" : "98478119",
      "id" : 98478119
    }, {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "indices" : [ 73, 80 ],
      "id_str" : "98478119",
      "id" : 98478119
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "241621220989206528",
  "text" : "RT @MaxCDN: Please RT! It's your last day to claim your free terabyte at @MaxCDN because web speed matters . Get it now! http://t.co/TSB ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.hootsuite.com\" rel=\"nofollow\">HootSuite</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "MaxCDN.com",
        "screen_name" : "MaxCDN",
        "indices" : [ 61, 68 ],
        "id_str" : "98478119",
        "id" : 98478119
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "webperf",
        "indices" : [ 130, 138 ]
      } ],
      "urls" : [ {
        "indices" : [ 109, 129 ],
        "url" : "http://t.co/TSBtcKy6",
        "expanded_url" : "http://ow.ly/d6HPh",
        "display_url" : "ow.ly/d6HPh"
      } ]
    },
    "geo" : {
    },
    "id_str" : "241601258971070464",
    "text" : "Please RT! It's your last day to claim your free terabyte at @MaxCDN because web speed matters . Get it now! http://t.co/TSBtcKy6 #webperf",
    "id" : 241601258971070464,
    "created_at" : "Fri Aug 31 18:20:03 +0000 2012",
    "user" : {
      "name" : "MaxCDN.com",
      "screen_name" : "MaxCDN",
      "protected" : false,
      "id_str" : "98478119",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3659080717/7d2ffc9641acae25c136ad8cface3105_normal.png",
      "id" : 98478119,
      "verified" : false
    }
  },
  "id" : 241621220989206528,
  "created_at" : "Fri Aug 31 19:39:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 0, 11 ],
      "id_str" : "104462925",
      "id" : 104462925
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241617178082832384",
  "geo" : {
  },
  "id_str" : "241619263155228672",
  "in_reply_to_user_id" : 104462925,
  "text" : "@techiejayk Hey! You got there before me :) Thinking of monetizing the HPJQ Repo. Will alert you when it happens. Love the site BTW.",
  "id" : 241619263155228672,
  "in_reply_to_status_id" : 241617178082832384,
  "created_at" : "Fri Aug 31 19:31:36 +0000 2012",
  "in_reply_to_screen_name" : "techiejayk",
  "in_reply_to_user_id_str" : "104462925",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 0, 12 ],
      "id_str" : "793830",
      "id" : 793830
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 124, 128 ]
    }, {
      "text" : "unicode",
      "indices" : [ 129, 137 ]
    } ],
    "urls" : [ {
      "indices" : [ 103, 123 ],
      "url" : "http://t.co/bCnUOsoW",
      "expanded_url" : "http://iwantaneff.in/escaper/",
      "display_url" : "iwantaneff.in/escaper/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241610861213540352",
  "in_reply_to_user_id" : 793830,
  "text" : "@chriscoyier The CSS Content Property Escaper now supports more than one character, and gets upgrades: http://t.co/bCnUOsoW #css #unicode",
  "id" : 241610861213540352,
  "created_at" : "Fri Aug 31 18:58:12 +0000 2012",
  "in_reply_to_screen_name" : "chriscoyier",
  "in_reply_to_user_id_str" : "793830",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 0, 12 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdev",
      "indices" : [ 127, 134 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/Hb8b744r",
      "expanded_url" : "http://iwantaneff.in/toolset/",
      "display_url" : "iwantaneff.in/toolset/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241609856094703616",
  "in_reply_to_user_id" : 15736190,
  "text" : "@smashingmag The Web Developer Toolset just went 2.0 \u2192 http://t.co/Hb8b744r Not a contrived + Spammy tweet. PPL will use this! #webdev",
  "id" : 241609856094703616,
  "created_at" : "Fri Aug 31 18:54:13 +0000 2012",
  "in_reply_to_screen_name" : "smashingmag",
  "in_reply_to_user_id_str" : "15736190",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Rodr\u00EDguez",
      "screen_name" : "sadasant",
      "indices" : [ 0, 9 ],
      "id_str" : "101949871",
      "id" : 101949871
    }, {
      "name" : "embeducation",
      "screen_name" : "AWSOMEDEVSIGNER",
      "indices" : [ 10, 26 ],
      "id_str" : "83013622",
      "id" : 83013622
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "teamFollowback",
      "indices" : [ 43, 58 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241602851024027649",
  "geo" : {
  },
  "id_str" : "241605074466512896",
  "in_reply_to_user_id" : 101949871,
  "text" : "@sadasant @awsomedevsigner Is this the new #teamFollowback hashtag? If so, I have to wade through a lot of Firefox and Fun Friday tweets",
  "id" : 241605074466512896,
  "in_reply_to_status_id" : 241602851024027649,
  "created_at" : "Fri Aug 31 18:35:13 +0000 2012",
  "in_reply_to_screen_name" : "sadasant",
  "in_reply_to_user_id_str" : "101949871",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 70, 81 ],
      "id_str" : "104462925",
      "id" : 104462925
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/xU0y3Us2",
      "expanded_url" : "http://post.ly/96xJu",
      "display_url" : "post.ly/96xJu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241603630698991617",
  "text" : "Favorited \u2605 http://t.co/xU0y3Us2 Loving the Firefox Tweet Machine via @techiejayk",
  "id" : 241603630698991617,
  "created_at" : "Fri Aug 31 18:29:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "indices" : [ 3, 14 ],
      "id_str" : "464887553",
      "id" : 464887553
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 115 ],
      "url" : "http://t.co/6CsVW7sI",
      "expanded_url" : "http://lcd-fixer.app-o-trap.com/run?hl=en-US",
      "display_url" : "lcd-fixer.app-o-trap.com/run?hl=en-US"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241597578804858880",
  "text" : "RT @EdanHewitt: Am I the only one that hacks the URL of sites to reveal more content? Example: http://t.co/6CsVW7sI Now becomes this htt ...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 79, 99 ],
        "url" : "http://t.co/6CsVW7sI",
        "expanded_url" : "http://lcd-fixer.app-o-trap.com/run?hl=en-US",
        "display_url" : "lcd-fixer.app-o-trap.com/run?hl=en-US"
      }, {
        "indices" : [ 117, 137 ],
        "url" : "http://t.co/VkztJZh0",
        "expanded_url" : "http://www.app-o-trap.com/?hl=en",
        "display_url" : "app-o-trap.com/?hl=en"
      } ]
    },
    "geo" : {
    },
    "id_str" : "241597278731780096",
    "text" : "Am I the only one that hacks the URL of sites to reveal more content? Example: http://t.co/6CsVW7sI Now becomes this http://t.co/VkztJZh0",
    "id" : 241597278731780096,
    "created_at" : "Fri Aug 31 18:04:14 +0000 2012",
    "user" : {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "protected" : false,
      "id_str" : "464887553",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3646301696/1956e43caf6c2d6981f2e222e4fbd6ba_normal.png",
      "id" : 464887553,
      "verified" : false
    }
  },
  "id" : 241597578804858880,
  "created_at" : "Fri Aug 31 18:05:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 111 ],
      "url" : "http://t.co/Y8T98w2B",
      "expanded_url" : "http://iwantaneff.in/meme/facebook-friends-post-ancient-internet-memes.jpg",
      "display_url" : "iwantaneff.in/meme/facebook-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241591193576816640",
  "text" : "The same goes for ARS Technica posting articles on hacking that are just re-hashes of 2600 http://t.co/Y8T98w2B Be original and fresh PPL!",
  "id" : 241591193576816640,
  "created_at" : "Fri Aug 31 17:40:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http://t.co/F9a3ndhz",
      "expanded_url" : "http://bit.ly/PxeLFu",
      "display_url" : "bit.ly/PxeLFu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241588689703165952",
  "text" : "\"puush is a super-quick way to share screenshots and files.\" http://t.co/F9a3ndhz",
  "id" : 241588689703165952,
  "created_at" : "Fri Aug 31 17:30:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    }, {
      "name" : "Branch",
      "screen_name" : "branch",
      "indices" : [ 106, 113 ],
      "id_str" : "494518813",
      "id" : 494518813
    }, {
      "name" : "Medium",
      "screen_name" : "Medium",
      "indices" : [ 118, 125 ],
      "id_str" : "571202103",
      "id" : 571202103
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "protip",
      "indices" : [ 131, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "241584423160672257",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn I think you're right about Tumblr. Engagement there is terrible. Might ditch it. Gonna dive into @branch and @medium soon #protip",
  "id" : 241584423160672257,
  "created_at" : "Fri Aug 31 17:13:09 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 79 ],
      "url" : "http://t.co/Gj6wHFnQ",
      "expanded_url" : "http://bit.ly/PxezGu",
      "display_url" : "bit.ly/PxezGu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241583651219968001",
  "text" : "\"Pictos - Serving Custom Built Icon Fonts To Your Website\" http://t.co/Gj6wHFnQ",
  "id" : 241583651219968001,
  "created_at" : "Fri Aug 31 17:10:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241567798827028480",
  "geo" : {
  },
  "id_str" : "241581260382806016",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii Yeah. TLAs are full of ex-h4xx0rz We're all in this cabal together. That's how they get hired. I'ma headhunting for l33ters",
  "id" : 241581260382806016,
  "in_reply_to_status_id" : 241567798827028480,
  "created_at" : "Fri Aug 31 17:00:35 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nic Aitch",
      "screen_name" : "nicinabox",
      "indices" : [ 97, 107 ],
      "id_str" : "14099524",
      "id" : 14099524
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/uTRerQla",
      "expanded_url" : "http://post.ly/95KSo",
      "display_url" : "post.ly/95KSo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241576893894254594",
  "text" : "Favorited \u2605 http://t.co/uTRerQla \u2192 I thought you might be interested in a project I maintain via @nicinabox",
  "id" : 241576893894254594,
  "created_at" : "Fri Aug 31 16:43:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AdPacks.com",
      "screen_name" : "adpacks",
      "indices" : [ 50, 58 ],
      "id_str" : "186182105",
      "id" : 186182105
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "241576159136067584",
  "text" : "Right, time to monetize all my sites. Diving into @adpacks at the moment. Will let you know how I get along ;) #$$$",
  "id" : 241576159136067584,
  "created_at" : "Fri Aug 31 16:40:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "embeducation",
      "screen_name" : "AWSOMEDEVSIGNER",
      "indices" : [ 0, 16 ],
      "id_str" : "83013622",
      "id" : 83013622
    }, {
      "name" : "Buffer",
      "screen_name" : "bufferapp",
      "indices" : [ 69, 79 ],
      "id_str" : "1510622484",
      "id" : 1510622484
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241474436870578176",
  "geo" : {
  },
  "id_str" : "241572605650419712",
  "in_reply_to_user_id" : 83013622,
  "text" : "@AWSOMEDEVSIGNER Also, your tweets would be ideal for a service like @bufferapp Can't wait for your next mad frenzy of tweets ;)",
  "id" : 241572605650419712,
  "in_reply_to_status_id" : 241474436870578176,
  "created_at" : "Fri Aug 31 16:26:12 +0000 2012",
  "in_reply_to_screen_name" : "AWSOMEDEVSIGNER",
  "in_reply_to_user_id_str" : "83013622",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "embeducation",
      "screen_name" : "AWSOMEDEVSIGNER",
      "indices" : [ 0, 16 ],
      "id_str" : "83013622",
      "id" : 83013622
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "FF",
      "indices" : [ 24, 27 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241474436870578176",
  "geo" : {
  },
  "id_str" : "241572069333139457",
  "in_reply_to_user_id" : 83013622,
  "text" : "@AWSOMEDEVSIGNER What's #FF mean. That could mean anything 1!?",
  "id" : 241572069333139457,
  "in_reply_to_status_id" : 241474436870578176,
  "created_at" : "Fri Aug 31 16:24:04 +0000 2012",
  "in_reply_to_screen_name" : "AWSOMEDEVSIGNER",
  "in_reply_to_user_id_str" : "83013622",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "colindpritchard",
      "screen_name" : "colindpritchard",
      "indices" : [ 68, 84 ],
      "id_str" : "17592145",
      "id" : 17592145
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/oc9ywJl0",
      "expanded_url" : "http://post.ly/96AwW",
      "display_url" : "post.ly/96AwW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241571687416602627",
  "text" : "Favorited \u2605 http://t.co/oc9ywJl0 \u2192 Handpicked jQuery Plugins v2 via @colindpritchard",
  "id" : 241571687416602627,
  "created_at" : "Fri Aug 31 16:22:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 37, 53 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/ROgbFFhq",
      "expanded_url" : "http://post.ly/95MIy",
      "display_url" : "post.ly/95MIy"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241571499113328640",
  "text" : "Favorited \u2605 http://t.co/ROgbFFhq via @jalbertbowdenii \u2192 the sounds of coding",
  "id" : 241571499113328640,
  "created_at" : "Fri Aug 31 16:21:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 133 ],
      "url" : "http://t.co/TKEad0JX",
      "expanded_url" : "http://isharefil.es/J8lA",
      "display_url" : "isharefil.es/J8lA"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241563791014363138",
  "text" : "Just letting y'all know - I've Metro-fied and bootstrap-ified my Effin Toolset. Also - Some tools were adjusted. http://t.co/TKEad0JX",
  "id" : 241563791014363138,
  "created_at" : "Fri Aug 31 15:51:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html5",
      "indices" : [ 70, 76 ]
    }, {
      "text" : "caniuse",
      "indices" : [ 77, 85 ]
    } ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/Ghxlvh81",
      "expanded_url" : "http://bit.ly/Pxexye",
      "display_url" : "bit.ly/Pxexye"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241558477972062209",
  "text" : "HTML5 Please - Use the new and shiny responsibly http://t.co/Ghxlvh81 #html5 #caniuse",
  "id" : 241558477972062209,
  "created_at" : "Fri Aug 31 15:30:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "appnet",
      "indices" : [ 74, 81 ]
    }, {
      "text" : "decentralize",
      "indices" : [ 104, 117 ]
    } ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/w5pJRDPB",
      "expanded_url" : "http://bit.ly/Pxetyu",
      "display_url" : "bit.ly/Pxetyu"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241553452411518976",
  "text" : "Open Source Distributed Micro-blogging http://t.co/w5pJRDPB We don't need #appnet This has you covered. #decentralize all the things",
  "id" : 241553452411518976,
  "created_at" : "Fri Aug 31 15:10:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 99 ],
      "url" : "http://t.co/hZBXhM6H",
      "expanded_url" : "http://bit.ly/NdNRhW",
      "display_url" : "bit.ly/NdNRhW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241294544149303296",
  "text" : "\"A simple Twitter Bot written in PHP5, allowing to search and retweet things.\" http://t.co/hZBXhM6H",
  "id" : 241294544149303296,
  "created_at" : "Thu Aug 30 22:01:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http://t.co/JIZJBjVB",
      "expanded_url" : "http://davidhiggins.me/lab",
      "display_url" : "davidhiggins.me/lab"
    } ]
  },
  "in_reply_to_status_id_str" : "241272480998772737",
  "geo" : {
  },
  "id_str" : "241273088375943169",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog Love that effect. Have it implemented here: http://t.co/JIZJBjVB",
  "id" : 241273088375943169,
  "in_reply_to_status_id" : 241272480998772737,
  "created_at" : "Thu Aug 30 20:36:01 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sougata Nair",
      "screen_name" : "maxxon15",
      "indices" : [ 0, 9 ],
      "id_str" : "242255953",
      "id" : 242255953
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 81 ],
      "url" : "https://t.co/PDNl99He",
      "expanded_url" : "https://github.com/h5bp/html5-boilerplate/blob/master/.htaccess#L105",
      "display_url" : "github.com/h5bp/html5-boi\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "241257399921569792",
  "geo" : {
  },
  "id_str" : "241266078209175552",
  "in_reply_to_user_id" : 242255953,
  "text" : "@maxxon15 Set the mime-type correctly in a .htaccess file \u2192 https://t.co/PDNl99He",
  "id" : 241266078209175552,
  "in_reply_to_status_id" : 241257399921569792,
  "created_at" : "Thu Aug 30 20:08:10 +0000 2012",
  "in_reply_to_screen_name" : "maxxon15",
  "in_reply_to_user_id_str" : "242255953",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Guillermo Latorre",
      "screen_name" : "Superwillyfoc",
      "indices" : [ 0, 14 ],
      "id_str" : "7258292",
      "id" : 7258292
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "241099303274311680",
  "geo" : {
  },
  "id_str" : "241263961499115520",
  "in_reply_to_user_id" : 7258292,
  "text" : "@Superwillyfoc Thanks for the blog post. I fixed any issues this had with the design. The text is no longer \"ugly as hell\" LoL.",
  "id" : 241263961499115520,
  "in_reply_to_status_id" : 241099303274311680,
  "created_at" : "Thu Aug 30 19:59:45 +0000 2012",
  "in_reply_to_screen_name" : "Superwillyfoc",
  "in_reply_to_user_id_str" : "7258292",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 0, 12 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "240389535211606017",
  "geo" : {
  },
  "id_str" : "241263318671699969",
  "in_reply_to_user_id" : 15736190,
  "text" : "@smashingmag Well done. The community needs this so much. So thankful for the tweet ;)",
  "id" : 241263318671699969,
  "in_reply_to_status_id" : 240389535211606017,
  "created_at" : "Thu Aug 30 19:57:12 +0000 2012",
  "in_reply_to_screen_name" : "smashingmag",
  "in_reply_to_user_id_str" : "15736190",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Antony Gelberg",
      "screen_name" : "antgel",
      "indices" : [ 0, 7 ],
      "id_str" : "20787096",
      "id" : 20787096
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "240390478900637696",
  "geo" : {
  },
  "id_str" : "241262140281995264",
  "in_reply_to_user_id" : 20787096,
  "text" : "@antgel Yes, even putting it on Cloudflare, on a dedicated server, with hotlinking disabled, it still fails. Welcome to being a webmaster ;)",
  "id" : 241262140281995264,
  "in_reply_to_status_id" : 240390478900637696,
  "created_at" : "Thu Aug 30 19:52:31 +0000 2012",
  "in_reply_to_screen_name" : "antgel",
  "in_reply_to_user_id_str" : "20787096",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "backups",
      "indices" : [ 121, 129 ]
    }, {
      "text" : "archiving",
      "indices" : [ 130, 140 ]
    } ],
    "urls" : [ {
      "indices" : [ 100, 120 ],
      "url" : "http://t.co/bJEwfeB3",
      "expanded_url" : "http://bit.ly/NdNP9Q",
      "display_url" : "bit.ly/NdNP9Q"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241226296061997056",
  "text" : "\"ThinkUp archives and analyzes your social media activity on Twitter, Facebook, Google+ and beyond\" http://t.co/bJEwfeB3 #backups #archiving",
  "id" : 241226296061997056,
  "created_at" : "Thu Aug 30 17:30:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "antidebugging",
      "indices" : [ 114, 128 ]
    } ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http://t.co/S7YjAwgF",
      "expanded_url" : "http://bit.ly/PxediY",
      "display_url" : "bit.ly/PxediY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241221262075121665",
  "text" : "\"Detect firebug opening and closing\" http://t.co/S7YjAwgF Stop content stealers inspecting your page with Firebug #antidebugging",
  "id" : 241221262075121665,
  "created_at" : "Thu Aug 30 17:10:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 120, 140 ],
      "url" : "http://t.co/8EGkEGfE",
      "expanded_url" : "http://bit.ly/PxebHY",
      "display_url" : "bit.ly/PxebHY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241196093252202496",
  "text" : "\" ([][(![]+[])[+[]]+([![]]+[][[]])[+!+[]+[+[]]]+(![]+[])[!+[]+!+[]]+(!+[]+[])[+[]]+(!+[]+[])[!+[]+!+[]+!+[]]+(!+[]+[])\u2026 http://t.co/8EGkEGfE",
  "id" : 241196093252202496,
  "created_at" : "Thu Aug 30 15:30:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 64, 75 ]
    } ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/HnkM1gMz",
      "expanded_url" : "http://bit.ly/NdNFPJ",
      "display_url" : "bit.ly/NdNFPJ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "241191060347441152",
  "text" : "\"NETEYE Transform &amp; Transition Plugin\" http://t.co/HnkM1gMz #javascript",
  "id" : 241191060347441152,
  "created_at" : "Thu Aug 30 15:10:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/1B6KI3ax",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240939301997780992",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 What's your thoughts on http://t.co/1B6KI3ax? You can 140-Char me, or maybe do a blog-post? \u2665 - Dave",
  "id" : 240939301997780992,
  "created_at" : "Wed Aug 29 22:29:40 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "canvas",
      "indices" : [ 70, 77 ]
    }, {
      "text" : "screenshot",
      "indices" : [ 78, 89 ]
    }, {
      "text" : "html2canvas",
      "indices" : [ 90, 102 ]
    } ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/t2P5Mh2z",
      "expanded_url" : "http://bit.ly/PxdkHb",
      "display_url" : "bit.ly/PxdkHb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240932122825744384",
  "text" : "Home - html2canvas - Screenshots with JavaScript http://t.co/t2P5Mh2z #canvas #screenshot #html2canvas",
  "id" : 240932122825744384,
  "created_at" : "Wed Aug 29 22:01:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Carlos Moreira",
      "screen_name" : "cmoreira",
      "indices" : [ 0, 9 ],
      "id_str" : "11070972",
      "id" : 11070972
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "240402793129709568",
  "geo" : {
  },
  "id_str" : "240903353327042560",
  "in_reply_to_user_id" : 11070972,
  "text" : "@cmoreira Thanks. I spent 3-4 days on and off doing the Metro theme. Plus a few tools were removed and others added. Also 'bootstrapped' it",
  "id" : 240903353327042560,
  "in_reply_to_status_id" : 240402793129709568,
  "created_at" : "Wed Aug 29 20:06:49 +0000 2012",
  "in_reply_to_screen_name" : "cmoreira",
  "in_reply_to_user_id_str" : "11070972",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nic Aitch",
      "screen_name" : "nicinabox",
      "indices" : [ 0, 10 ],
      "id_str" : "14099524",
      "id" : 14099524
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "240497875338031104",
  "geo" : {
  },
  "id_str" : "240892816165851136",
  "in_reply_to_user_id" : 14099524,
  "text" : "@nicinabox That's an awesome tool. I am in the process of adding a bunch of such nice scripts to the handpicked repo. This will be added ;)",
  "id" : 240892816165851136,
  "in_reply_to_status_id" : 240497875338031104,
  "created_at" : "Wed Aug 29 19:24:57 +0000 2012",
  "in_reply_to_screen_name" : "nicinabox",
  "in_reply_to_user_id_str" : "14099524",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Kammerer",
      "screen_name" : "mkammerer",
      "indices" : [ 0, 10 ],
      "id_str" : "13226232",
      "id" : 13226232
    }, {
      "name" : "AdPacks.com",
      "screen_name" : "adpacks",
      "indices" : [ 11, 19 ],
      "id_str" : "186182105",
      "id" : 186182105
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "240499060090490880",
  "geo" : {
  },
  "id_str" : "240886243322368000",
  "in_reply_to_user_id" : 13226232,
  "text" : "@mkammerer @adpacks Hey. Yeah that sounds good. My addy is david@higg.in ;)",
  "id" : 240886243322368000,
  "in_reply_to_status_id" : 240499060090490880,
  "created_at" : "Wed Aug 29 18:58:50 +0000 2012",
  "in_reply_to_screen_name" : "mkammerer",
  "in_reply_to_user_id_str" : "13226232",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 7, 27 ],
      "url" : "http://t.co/sFFsjLoQ",
      "expanded_url" : "http://bit.ly/PxdhuY",
      "display_url" : "bit.ly/PxdhuY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240863910159335424",
  "text" : "0to255 http://t.co/sFFsjLoQ",
  "id" : 240863910159335424,
  "created_at" : "Wed Aug 29 17:30:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "lsd",
      "indices" : [ 63, 67 ]
    } ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/IjbTyo3J",
      "expanded_url" : "http://bit.ly/Pxdhex",
      "display_url" : "bit.ly/Pxdhex"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240858868551397376",
  "text" : "LSD: The Geek's Wonder Drug | Hacker News http://t.co/IjbTyo3J #lsd",
  "id" : 240858868551397376,
  "created_at" : "Wed Aug 29 17:10:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/ovAxUwGz",
      "expanded_url" : "http://bit.ly/Pxdgr5",
      "display_url" : "bit.ly/Pxdgr5"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240833711220858880",
  "text" : "Killing the Buddha http://t.co/ovAxUwGz",
  "id" : 240833711220858880,
  "created_at" : "Wed Aug 29 15:30:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/8aatfimH",
      "expanded_url" : "http://bit.ly/PxddeN",
      "display_url" : "bit.ly/PxddeN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240828679830241280",
  "text" : "Noted &gt; Daniel Rehn http://t.co/8aatfimH",
  "id" : 240828679830241280,
  "created_at" : "Wed Aug 29 15:10:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "gifs",
      "indices" : [ 73, 78 ]
    }, {
      "text" : "gifart",
      "indices" : [ 79, 86 ]
    } ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/ullmSfF2",
      "expanded_url" : "http://bit.ly/NdN5Sa",
      "display_url" : "bit.ly/NdN5Sa"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240569724621172736",
  "text" : "Our Guide To Enjoying (And Making Your Own) GIF Art http://t.co/ullmSfF2 #gifs #gifart",
  "id" : 240569724621172736,
  "created_at" : "Tue Aug 28 22:01:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "enavu",
      "indices" : [ 50, 56 ]
    }, {
      "text" : "jquery",
      "indices" : [ 57, 64 ]
    }, {
      "text" : "defer",
      "indices" : [ 65, 71 ]
    }, {
      "text" : "plugin",
      "indices" : [ 72, 79 ]
    } ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/O1nFNHjB",
      "expanded_url" : "http://bit.ly/PxdaQ7",
      "display_url" : "bit.ly/PxdaQ7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240501527368839169",
  "text" : "jQuery Defer Plugin by enavu http://t.co/O1nFNHjB #enavu #jquery #defer #plugin",
  "id" : 240501527368839169,
  "created_at" : "Tue Aug 28 17:30:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 47, 54 ]
    } ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/jDSWX9tB",
      "expanded_url" : "http://bit.ly/NdN1Su",
      "display_url" : "bit.ly/NdN1Su"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240496485580566529",
  "text" : "jQuery - jCollapse plugin http://t.co/jDSWX9tB #jquery",
  "id" : 240496485580566529,
  "created_at" : "Tue Aug 28 17:10:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/J9lRWwAe",
      "expanded_url" : "http://bit.ly/NdN0he",
      "display_url" : "bit.ly/NdN0he"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240471319857221632",
  "text" : "enavu - web development experts http://t.co/J9lRWwAe",
  "id" : 240471319857221632,
  "created_at" : "Tue Aug 28 15:30:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 51, 54 ]
    }, {
      "text" : "javascript",
      "indices" : [ 55, 66 ]
    }, {
      "text" : "packer",
      "indices" : [ 67, 74 ]
    }, {
      "text" : "obfuscate",
      "indices" : [ 75, 85 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/QMSlFeXG",
      "expanded_url" : "http://bit.ly/NdMU9k",
      "display_url" : "bit.ly/NdMU9k"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240466298390323201",
  "text" : "JavaScript Crush by Aivo Paas http://t.co/QMSlFeXG #js #javascript #packer #obfuscate",
  "id" : 240466298390323201,
  "created_at" : "Tue Aug 28 15:10:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 0, 8 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 119 ],
      "url" : "http://t.co/o2PjpGqk",
      "expanded_url" : "http://bynens.xn--rmw.jp",
      "display_url" : "bynens.xn--rmw.jp"
    }, {
      "indices" : [ 120, 140 ],
      "url" : "http://t.co/X2RPRziK",
      "expanded_url" : "http://iwantaneff.in/glitcher/",
      "display_url" : "iwantaneff.in/glitcher/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240283715983519744",
  "in_reply_to_user_id" : 532923,
  "text" : "@mathias mathi bynens *:..\u3002o\u25CB\u2606\u25CBo\u3002mat\u2193hia\u2191s \uFF3C\u026F\u0250\u0287\u0265\u0131\u0250s b\u028En\u01DDns\uFF0F\u2193\u301Ci\u2191a\u301C\u2193s\u301C b\u301C\u2191yne\u2193n\u2191s \u2193ma\u2191thias bynen\u2193s\u2191 http://t.co/o2PjpGqk http://t.co/X2RPRziK",
  "id" : 240283715983519744,
  "created_at" : "Tue Aug 28 03:04:36 +0000 2012",
  "in_reply_to_screen_name" : "mathias",
  "in_reply_to_user_id_str" : "532923",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : ".mario",
      "screen_name" : "0x6D6172696F",
      "indices" : [ 126, 139 ],
      "id_str" : "18780838",
      "id" : 18780838
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fuzzing",
      "indices" : [ 30, 38 ]
    }, {
      "text" : "xss",
      "indices" : [ 102, 106 ]
    } ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http://t.co/X2RPRziK",
      "expanded_url" : "http://iwantaneff.in/glitcher/",
      "display_url" : "iwantaneff.in/glitcher/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240283056106258433",
  "text" : "My glitcher tool is great for #fuzzing alert\u301C(\u301C1) http://t.co/X2RPRziK aalleerrtt((11)) \uFF08*\uFF9F\uFF70\uFF9F*)(*\uFF61\u30F3\u30A6\u30F3 #xss :\uFF65alert(1)\uFF65:+\uFF65 cc: @0x6D6172696F",
  "id" : 240283056106258433,
  "created_at" : "Tue Aug 28 03:01:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pujun Li",
      "screen_name" : "jackmasa",
      "indices" : [ 0, 9 ],
      "id_str" : "256454626",
      "id" : 256454626
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 44, 48 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/X2RPRziK",
      "expanded_url" : "http://iwantaneff.in/glitcher/",
      "display_url" : "iwantaneff.in/glitcher/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240282488314920960",
  "in_reply_to_user_id" : 256454626,
  "text" : "@jackmasa This is for fun only. None of the #xss works http://t.co/X2RPRziK http://\u2500\u2500\u2500\u2500a/l/e/r/t/(/1/).\u6B7B\u306C.jp",
  "id" : 240282488314920960,
  "created_at" : "Tue Aug 28 02:59:44 +0000 2012",
  "in_reply_to_screen_name" : "jackmasa",
  "in_reply_to_user_id_str" : "256454626",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http://t.co/8ZMiCNpw",
      "expanded_url" : "http://isharefil.es/J34f/jquery.versus.js.html",
      "display_url" : "isharefil.es/J34f/jquery.ve\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240282031513300993",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog You might dig this if you like your plain JS http://t.co/8ZMiCNpw Found it buried in my HD. I figured I'd put in on cloudapp",
  "id" : 240282031513300993,
  "created_at" : "Tue Aug 28 02:57:55 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "240281174134624256",
  "geo" : {
  },
  "id_str" : "240281614985351168",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog I see. There seems to be a big move away from jQuery, now with the likes of VanillaJS preaching the word of vanilla to all",
  "id" : 240281614985351168,
  "in_reply_to_status_id" : 240281174134624256,
  "created_at" : "Tue Aug 28 02:56:15 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http://t.co/Sm6kiV1O",
      "expanded_url" : "http://www.azoffdesign.com/autoresize",
      "display_url" : "azoffdesign.com/autoresize"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240280750992277505",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog Whilst you're in the mood for replying: http://t.co/Sm6kiV1O I mentioned that already. You said you wanted this. Thoughts?",
  "id" : 240280750992277505,
  "created_at" : "Tue Aug 28 02:52:49 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "240278603940655104",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog Strange the way you reply instantly to others but avoid me like the plague ;) Is there something you're not telling me?",
  "id" : 240278603940655104,
  "created_at" : "Tue Aug 28 02:44:17 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bocoup",
      "screen_name" : "bocoup",
      "indices" : [ 0, 7 ],
      "id_str" : "98303566",
      "id" : 98303566
    }, {
      "name" : "Ben Alman",
      "screen_name" : "cowboy",
      "indices" : [ 8, 15 ],
      "id_str" : "6798592",
      "id" : 6798592
    }, {
      "name" : "Boaz Sender",
      "screen_name" : "BoazSender",
      "indices" : [ 16, 27 ],
      "id_str" : "15347596",
      "id" : 15347596
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 119 ],
      "url" : "http://t.co/4zEStMnG",
      "expanded_url" : "http://iwantaneff.in/dataurl/index.html",
      "display_url" : "iwantaneff.in/dataurl/index.\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240278020324200448",
  "in_reply_to_user_id" : 98303566,
  "text" : "@bocoup @cowboy @boazsender You are aware your Data URI tool is removed? It lives on here though \u2192 http://t.co/4zEStMnG Longlive great tools",
  "id" : 240278020324200448,
  "created_at" : "Tue Aug 28 02:41:58 +0000 2012",
  "in_reply_to_screen_name" : "bocoup",
  "in_reply_to_user_id_str" : "98303566",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Alman",
      "screen_name" : "cowboy",
      "indices" : [ 0, 7 ],
      "id_str" : "6798592",
      "id" : 6798592
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 113 ],
      "url" : "http://t.co/22eCSEVw",
      "expanded_url" : "http://iwantaneff.in/packer/",
      "display_url" : "iwantaneff.in/packer/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240277036080431105",
  "in_reply_to_user_id" : 6798592,
  "text" : "@cowboy Your packer script seems to work in Chrome now. Again, kudos for such a great tool \u2192 http://t.co/22eCSEVw",
  "id" : 240277036080431105,
  "created_at" : "Tue Aug 28 02:38:04 +0000 2012",
  "in_reply_to_screen_name" : "cowboy",
  "in_reply_to_user_id_str" : "6798592",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 133 ],
      "url" : "http://t.co/Hb8b744r",
      "expanded_url" : "http://iwantaneff.in/toolset/",
      "display_url" : "iwantaneff.in/toolset/"
    } ]
  },
  "in_reply_to_status_id_str" : "240275726228660224",
  "geo" : {
  },
  "id_str" : "240276215758475264",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog Speaking of Bootstrap. Just made my fuggin' Toolset all bootstrap-ey. Let me know your thoughts? http://t.co/Hb8b744r",
  "id" : 240276215758475264,
  "in_reply_to_status_id" : 240275726228660224,
  "created_at" : "Tue Aug 28 02:34:48 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "chromeless",
      "indices" : [ 76, 87 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/y1SRLWfL",
      "expanded_url" : "http://bit.ly/PxcFFK",
      "display_url" : "bit.ly/PxcFFK"
    }, {
      "indices" : [ 88, 108 ],
      "url" : "http://t.co/IHwNhFtb",
      "expanded_url" : "http://bit.ly/LeAbYI",
      "display_url" : "bit.ly/LeAbYI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240207416652877824",
  "text" : "Fullscreen http://t.co/y1SRLWfL The full-screen specs. Now to write one for #chromeless http://t.co/IHwNhFtb",
  "id" : 240207416652877824,
  "created_at" : "Mon Aug 27 22:01:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "license",
      "indices" : [ 76, 84 ]
    }, {
      "text" : "licensing",
      "indices" : [ 85, 95 ]
    }, {
      "text" : "wtfpl",
      "indices" : [ 96, 102 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/kOleAPpW",
      "expanded_url" : "http://bit.ly/NdMIXE",
      "display_url" : "bit.ly/NdMIXE"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240139183111417857",
  "text" : "\"WTFPL - Do What The Fuck You Want To Public License \" http://t.co/kOleAPpW #license #licensing #wtfpl",
  "id" : 240139183111417857,
  "created_at" : "Mon Aug 27 17:30:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Padolsey",
      "screen_name" : "padolsey",
      "indices" : [ 0, 9 ],
      "id_str" : "17107025",
      "id" : 17107025
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "240135526915637248",
  "in_reply_to_user_id" : 17107025,
  "text" : "@padolsey Love the Hue-Rotation effect on your blog. Nice work ;)",
  "id" : 240135526915637248,
  "created_at" : "Mon Aug 27 17:15:45 +0000 2012",
  "in_reply_to_screen_name" : "padolsey",
  "in_reply_to_user_id_str" : "17107025",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "unicode",
      "indices" : [ 49, 57 ]
    } ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/wCXJlvme",
      "expanded_url" : "http://bit.ly/NdMHTI",
      "display_url" : "bit.ly/NdMHTI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240134153562095616",
  "text" : "\"Unicode Whoes And Wonders\" http://t.co/wCXJlvme #unicode",
  "id" : 240134153562095616,
  "created_at" : "Mon Aug 27 17:10:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Buffer",
      "screen_name" : "bufferapp",
      "indices" : [ 0, 10 ],
      "id_str" : "1510622484",
      "id" : 1510622484
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "suggestion",
      "indices" : [ 129, 140 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "240128861856210945",
  "in_reply_to_user_id" : 197962366,
  "text" : "@bufferapp I dislike when you charge for another month behind my back. Let my plan stop and then ask me if I want another month. #suggestion",
  "id" : 240128861856210945,
  "created_at" : "Mon Aug 27 16:49:16 +0000 2012",
  "in_reply_to_screen_name" : "buffer",
  "in_reply_to_user_id_str" : "197962366",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Buffer",
      "screen_name" : "bufferapp",
      "indices" : [ 0, 10 ],
      "id_str" : "1510622484",
      "id" : 1510622484
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "240128175500300288",
  "in_reply_to_user_id" : 197962366,
  "text" : "@bufferapp Let people know a few days in advance, as some people only pay for a month here and there. I hate remembering dates. :)",
  "id" : 240128175500300288,
  "created_at" : "Mon Aug 27 16:46:33 +0000 2012",
  "in_reply_to_screen_name" : "buffer",
  "in_reply_to_user_id_str" : "197962366",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Buffer",
      "screen_name" : "bufferapp",
      "indices" : [ 0, 10 ],
      "id_str" : "1510622484",
      "id" : 1510622484
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "240127951151181824",
  "in_reply_to_user_id" : 197962366,
  "text" : "@bufferapp I had to cancel my plan, a few days in advance of my next monthly fee.",
  "id" : 240127951151181824,
  "created_at" : "Mon Aug 27 16:45:39 +0000 2012",
  "in_reply_to_screen_name" : "buffer",
  "in_reply_to_user_id_str" : "197962366",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "appnet",
      "indices" : [ 131, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "240124521896615936",
  "text" : "Great thing about Twitter is; Lady Gaga has loads of followers, and I have few. But we're both ON TWITTER. Same cannot be said for #appnet",
  "id" : 240124521896615936,
  "created_at" : "Mon Aug 27 16:32:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/ZbxdGTF1",
      "expanded_url" : "http://isharefil.es/J2Xp",
      "display_url" : "isharefil.es/J2Xp"
    }, {
      "indices" : [ 70, 90 ],
      "url" : "http://t.co/Rd313teH",
      "expanded_url" : "http://www.reddit.com/r/memes/comments/yv4wc/one_does_not_simply_gather_over_3000_memes_and/",
      "display_url" : "reddit.com/r/memes/commen\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240117739681873921",
  "text" : "My meme-dump post to Reddit is sorta trending :) http://t.co/ZbxdGTF1 http://t.co/Rd313teH 120 uniques. That's a lot of meme consumption !!1",
  "id" : 240117739681873921,
  "created_at" : "Mon Aug 27 16:05:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Wastl",
      "screen_name" : "topaz2078",
      "indices" : [ 0, 10 ],
      "id_str" : "24329014",
      "id" : 24329014
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "vanillajs",
      "indices" : [ 115, 125 ]
    } ],
    "urls" : [ {
      "indices" : [ 94, 114 ],
      "url" : "http://t.co/8ZMiCNpw",
      "expanded_url" : "http://isharefil.es/J34f/jquery.versus.js.html",
      "display_url" : "isharefil.es/J34f/jquery.ve\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240112217117302785",
  "in_reply_to_user_id" : 24329014,
  "text" : "@topaz2078 You should add more examples regarding JS vs jQuery. Here's something you'll enjoy http://t.co/8ZMiCNpw #vanillajs",
  "id" : 240112217117302785,
  "created_at" : "Mon Aug 27 15:43:08 +0000 2012",
  "in_reply_to_screen_name" : "topaz2078",
  "in_reply_to_user_id_str" : "24329014",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/u50zWN0b",
      "expanded_url" : "http://bit.ly/Pxcudp",
      "display_url" : "bit.ly/Pxcudp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240108949733519360",
  "text" : "\"Do You Suffer From the Dunning-Kruger Effect? \" http://t.co/u50zWN0b",
  "id" : 240108949733519360,
  "created_at" : "Mon Aug 27 15:30:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/aMQjVsDA",
      "expanded_url" : "http://bit.ly/Pxcie8",
      "display_url" : "bit.ly/Pxcie8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "240103901536346113",
  "text" : "SF Dok - 360\u00B0 Langstrasse Z\u00FCrich http://t.co/aMQjVsDA Always a classic. Up there with the best websites ever made IMHO",
  "id" : 240103901536346113,
  "created_at" : "Mon Aug 27 15:10:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 137, 140 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/8ZMiCNpw",
      "expanded_url" : "http://isharefil.es/J34f/jquery.versus.js.html",
      "display_url" : "isharefil.es/J34f/jquery.ve\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "240075559101685760",
  "geo" : {
  },
  "id_str" : "240099504869765120",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Reminds me of: http://t.co/8ZMiCNpw jQuery versus plain JS. Been meaning to create a lib called 'agnostic.js' for a while now.  #js",
  "id" : 240099504869765120,
  "in_reply_to_status_id" : 240075559101685760,
  "created_at" : "Mon Aug 27 14:52:37 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jsconf",
      "indices" : [ 51, 58 ]
    }, {
      "text" : "dom",
      "indices" : [ 59, 63 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/LIMQ8gLB",
      "expanded_url" : "http://bit.ly/Pxc9HT",
      "display_url" : "bit.ly/Pxc9HT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239844943449956352",
  "text" : "DOM implementation techniques http://t.co/LIMQ8gLB #jsconf #dom",
  "id" : 239844943449956352,
  "created_at" : "Sun Aug 26 22:01:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Licorize News",
      "screen_name" : "licorizenews",
      "indices" : [ 92, 105 ],
      "id_str" : "131183696",
      "id" : 131183696
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "licorize",
      "indices" : [ 78, 87 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 77 ],
      "url" : "http://t.co/bVzVZYOC",
      "expanded_url" : "http://licorize.com/projects/davidhiggins/Reverse-The-Web",
      "display_url" : "licorize.com/projects/david\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239833188841689089",
  "text" : "Exploring Licorize booklet David Higgins Reverse The Web http://t.co/bVzVZYOC #licorize via @licorizeNews",
  "id" : 239833188841689089,
  "created_at" : "Sun Aug 26 21:14:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "licorize",
      "indices" : [ 69, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/OnRsEzwF",
      "expanded_url" : "http://licorize.com/projects/davidhiggins/jQuery-Plugins?list",
      "display_url" : "licorize.com/projects/david\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239832739363307522",
  "text" : "Also, there is a \"list\" version of the Booklet: http://t.co/OnRsEzwF #licorize",
  "id" : 239832739363307522,
  "created_at" : "Sun Aug 26 21:12:35 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "licorize",
      "indices" : [ 120, 129 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/yCRWoWx3",
      "expanded_url" : "http://licorize.com/projects/davidhiggins/jQuery-Plugins",
      "display_url" : "licorize.com/projects/david\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239832311196184576",
  "text" : "The Handpicked jQuery Plugins Repo is now on Licorize! http://t.co/yCRWoWx3 Cool 'Booklet' app for viewing the plugins. #licorize",
  "id" : 239832311196184576,
  "created_at" : "Sun Aug 26 21:10:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Prinzhorn",
      "screen_name" : "Prinzhorn",
      "indices" : [ 0, 10 ],
      "id_str" : "187226449",
      "id" : 187226449
    }, {
      "name" : "Lars Jung",
      "screen_name" : "lrsjng",
      "indices" : [ 11, 18 ],
      "id_str" : "91685967",
      "id" : 91685967
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 115 ],
      "url" : "http://t.co/yfGzFXuC",
      "expanded_url" : "http://iwantaneff.in/stream/",
      "display_url" : "iwantaneff.in/stream/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239827194350796800",
  "in_reply_to_user_id" : 187226449,
  "text" : "@Prinzhorn @lrsjng You were included in the Stream. Thanks for providing such delicious tweets http://t.co/yfGzFXuC",
  "id" : 239827194350796800,
  "created_at" : "Sun Aug 26 20:50:33 +0000 2012",
  "in_reply_to_screen_name" : "Prinzhorn",
  "in_reply_to_user_id_str" : "187226449",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Conor Organ",
      "screen_name" : "conorgan",
      "indices" : [ 0, 9 ],
      "id_str" : "356114318",
      "id" : 356114318
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http://t.co/yfGzFXuC",
      "expanded_url" : "http://iwantaneff.in/stream/",
      "display_url" : "iwantaneff.in/stream/"
    } ]
  },
  "in_reply_to_status_id_str" : "239822908736823297",
  "geo" : {
  },
  "id_str" : "239826752778674176",
  "in_reply_to_user_id" : 356114318,
  "text" : "@conorgan Did you do nerdnews.me ? Impressive ;) Here's something similar: http://t.co/yfGzFXuC",
  "id" : 239826752778674176,
  "in_reply_to_status_id" : 239822908736823297,
  "created_at" : "Sun Aug 26 20:48:48 +0000 2012",
  "in_reply_to_screen_name" : "conorgan",
  "in_reply_to_user_id_str" : "356114318",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 119 ],
      "url" : "http://t.co/yOrmswjr",
      "expanded_url" : "http://www.reddit.com/r/funny/comments/yv493/just_going_to_leave_this_with_you_guys_nothing/",
      "display_url" : "reddit.com/r/funny/commen\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239803879708033024",
  "text" : "And ... It's posted. Does anyone know how / why some posts go 'viral' on Reddit? Can I haz ProTip? http://t.co/yOrmswjr",
  "id" : 239803879708033024,
  "created_at" : "Sun Aug 26 19:17:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 135 ],
      "url" : "http://t.co/kfMbbqUJ",
      "expanded_url" : "http://iwantaneff.in/meme/",
      "display_url" : "iwantaneff.in/meme/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239802915131383809",
  "text" : "I'm just about to post this on Reddit. My little 'experiment' I will let you know how it goes. (Probably terrible) http://t.co/kfMbbqUJ",
  "id" : 239802915131383809,
  "created_at" : "Sun Aug 26 19:14:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/HBM1MAZG",
      "expanded_url" : "http://iwantaneff.in/meme/have-you-come-forever-alone.jpg",
      "display_url" : "iwantaneff.in/meme/have-you-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239801924063469568",
  "text" : "Forevaaar alone: http://t.co/HBM1MAZG",
  "id" : 239801924063469568,
  "created_at" : "Sun Aug 26 19:10:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "239797894767603714",
  "geo" : {
  },
  "id_str" : "239800671338119168",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii He he! U online on AIM brah?",
  "id" : 239800671338119168,
  "in_reply_to_status_id" : 239797894767603714,
  "created_at" : "Sun Aug 26 19:05:09 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 8, 16 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/xTTNHyEV",
      "expanded_url" : "http://janetter.net/",
      "display_url" : "janetter.net"
    }, {
      "indices" : [ 104, 124 ],
      "url" : "http://t.co/TReMwRCM",
      "expanded_url" : "http://iwantaneff.in/twitter",
      "display_url" : "iwantaneff.in/twitter"
    } ]
  },
  "in_reply_to_status_id_str" : "239793776833482752",
  "geo" : {
  },
  "id_str" : "239794164823371776",
  "in_reply_to_user_id" : 468853739,
  "text" : "@_higg_ @tompntn Wait...??? No http://t.co/xTTNHyEV ain't mine, mon frere. But...The dump. (All mine) \u2192 http://t.co/TReMwRCM ;)",
  "id" : 239794164823371776,
  "in_reply_to_status_id" : 239793776833482752,
  "created_at" : "Sun Aug 26 18:39:18 +0000 2012",
  "in_reply_to_screen_name" : "alphenic",
  "in_reply_to_user_id_str" : "468853739",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "239793348897026049",
  "geo" : {
  },
  "id_str" : "239793776833482752",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Yup. Purely an @_higg_ creation. I refer back to it nearly daily now for resources. It's CTRL+F Friendly",
  "id" : 239793776833482752,
  "in_reply_to_status_id" : 239793348897026049,
  "created_at" : "Sun Aug 26 18:37:46 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 120, 140 ],
      "url" : "http://t.co/TReMwRCM",
      "expanded_url" : "http://iwantaneff.in/twitter",
      "display_url" : "iwantaneff.in/twitter"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239792712168120322",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn That said, I was just looking through my Tweetdump thing. I might be wrong about \uD835\uDE2F\uD835\uDE30\uD835\uDE35 being addicted to Twitter http://t.co/TReMwRCM",
  "id" : 239792712168120322,
  "created_at" : "Sun Aug 26 18:33:32 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    }, {
      "name" : "Buffer",
      "screen_name" : "bufferapp",
      "indices" : [ 15, 25 ],
      "id_str" : "1510622484",
      "id" : 1510622484
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 135 ],
      "url" : "http://t.co/xTTNHyEV",
      "expanded_url" : "http://janetter.net/",
      "display_url" : "janetter.net"
    } ]
  },
  "in_reply_to_status_id_str" : "239791136191950848",
  "geo" : {
  },
  "id_str" : "239792208507711488",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn I use @bufferapp, so it might seem like I'm very busy on Twitter when infact I'm not. BTW, have you seen: http://t.co/xTTNHyEV",
  "id" : 239792208507711488,
  "in_reply_to_status_id" : 239791136191950848,
  "created_at" : "Sun Aug 26 18:31:32 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    }, {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "indices" : [ 9, 20 ],
      "id_str" : "464887553",
      "id" : 464887553
    }, {
      "name" : "Pluggio",
      "screen_name" : "Pluggio",
      "indices" : [ 21, 29 ],
      "id_str" : "121945320",
      "id" : 121945320
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "239788373613617152",
  "geo" : {
  },
  "id_str" : "239790709664796672",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn @EdanHewitt @Pluggio Yeah I saw it. Seems like overkill. Unless you're addicted to Twitter, I wouldn't use it IMHO",
  "id" : 239790709664796672,
  "in_reply_to_status_id" : 239788373613617152,
  "created_at" : "Sun Aug 26 18:25:34 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "github",
      "indices" : [ 47, 54 ]
    } ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/CFlJA9g1",
      "expanded_url" : "http://bit.ly/Pxc6M4",
      "display_url" : "bit.ly/Pxc6M4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239776753420230656",
  "text" : "troufster (Stefan Pataky) http://t.co/CFlJA9g1 #github Nice repos",
  "id" : 239776753420230656,
  "created_at" : "Sun Aug 26 17:30:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 88, 99 ]
    } ],
    "urls" : [ {
      "indices" : [ 67, 87 ],
      "url" : "http://t.co/R6JSNeDj",
      "expanded_url" : "http://bit.ly/Pxc4E2",
      "display_url" : "bit.ly/Pxc4E2"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239771739641483264",
  "text" : "\"LightningJS: safe, fast, and asynchronous third-party Javascript\" http://t.co/R6JSNeDj #javascript",
  "id" : 239771739641483264,
  "created_at" : "Sun Aug 26 17:10:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 67, 70 ]
    }, {
      "text" : "demo",
      "indices" : [ 71, 76 ]
    }, {
      "text" : "effect",
      "indices" : [ 77, 84 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/MFPem4YN",
      "expanded_url" : "http://bit.ly/PxbRRb",
      "display_url" : "bit.ly/PxbRRb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239746626128850945",
  "text" : "Creating a modern gallery with Raphael - Demo http://t.co/MFPem4YN #js #demo #effect",
  "id" : 239746626128850945,
  "created_at" : "Sun Aug 26 15:30:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 63, 70 ]
    }, {
      "text" : "technique",
      "indices" : [ 71, 81 ]
    }, {
      "text" : "lazyloading",
      "indices" : [ 82, 94 ]
    } ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/4EaokW00",
      "expanded_url" : "http://bit.ly/NdMgJd",
      "display_url" : "bit.ly/NdMgJd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239741526211702786",
  "text" : "Lazy Load Enabled on Horizontal Container http://t.co/4EaokW00 #jquery #technique #lazyloading",
  "id" : 239741526211702786,
  "created_at" : "Sun Aug 26 15:10:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "resource",
      "indices" : [ 125, 134 ]
    } ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/2fqGfNwN",
      "expanded_url" : "http://bit.ly/NdMbF7",
      "display_url" : "bit.ly/NdMbF7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239482607019122688",
  "text" : "Alltop (Web Design) http://t.co/2fqGfNwN Otherwise known in dev circles as \"the list\" Scraped and reblogged over 9000 times. #resource",
  "id" : 239482607019122688,
  "created_at" : "Sat Aug 25 22:01:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "progress",
      "indices" : [ 82, 91 ]
    }, {
      "text" : "html5",
      "indices" : [ 92, 98 ]
    } ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http://t.co/xrE6N20z",
      "expanded_url" : "http://bit.ly/PxbCWh",
      "display_url" : "bit.ly/PxbCWh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239414368968769537",
  "text" : "The HTML progress element in Firefox - Mounir Lamouri's Blog http://t.co/xrE6N20z #progress #html5",
  "id" : 239414368968769537,
  "created_at" : "Sat Aug 25 17:30:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "parallax",
      "indices" : [ 56, 65 ]
    }, {
      "text" : "scrolling",
      "indices" : [ 66, 76 ]
    }, {
      "text" : "animation",
      "indices" : [ 77, 87 ]
    } ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/qb9xgwlz",
      "expanded_url" : "http://bit.ly/NdM38K",
      "display_url" : "bit.ly/NdM38K"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239409341181095938",
  "text" : "Making the Perfect Listing - Gidsy http://t.co/qb9xgwlz #parallax #scrolling #animation You can never get enough JS parallax demos ;)",
  "id" : 239409341181095938,
  "created_at" : "Sat Aug 25 17:10:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://img.ly\" rel=\"nofollow\">img.ly</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/Rg1U1Pxn",
      "expanded_url" : "http://img.ly/mt4a",
      "display_url" : "img.ly/mt4a"
    }, {
      "indices" : [ 81, 101 ],
      "url" : "http://t.co/4k45T5vr",
      "expanded_url" : "http://davidhiggins.me/museum/",
      "display_url" : "davidhiggins.me/museum/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239387519131734017",
  "text" : "http://t.co/Rg1U1Pxn The Tux to my Chrome Experiments Tie - The ASCII Art Museum http://t.co/4k45T5vr",
  "id" : 239387519131734017,
  "created_at" : "Sat Aug 25 15:43:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hash",
      "indices" : [ 25, 30 ]
    } ],
    "urls" : [ {
      "indices" : [ 59, 79 ],
      "url" : "http://t.co/NO4dBJ67",
      "expanded_url" : "http://bit.ly/Pxbrua",
      "display_url" : "bit.ly/Pxbrua"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239384168235147265",
  "text" : "\"How can you check for a #hash in a URL using JavaScript?\" http://t.co/NO4dBJ67",
  "id" : 239384168235147265,
  "created_at" : "Sat Aug 25 15:30:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 98, 102 ]
    }, {
      "text" : "selectors",
      "indices" : [ 103, 113 ]
    } ],
    "urls" : [ {
      "indices" : [ 77, 97 ],
      "url" : "http://t.co/xAllo38g",
      "expanded_url" : "http://bit.ly/NdLYSn",
      "display_url" : "bit.ly/NdLYSn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239379118532202496",
  "text" : "\"Dust-Me Selectors is a Firefox extension that finds unused CSS selectors. \" http://t.co/xAllo38g #css #selectors",
  "id" : 239379118532202496,
  "created_at" : "Sat Aug 25 15:10:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ascii",
      "indices" : [ 102, 108 ]
    }, {
      "text" : "art",
      "indices" : [ 109, 113 ]
    } ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http://t.co/4k45T5vr",
      "expanded_url" : "http://davidhiggins.me/museum/",
      "display_url" : "davidhiggins.me/museum/"
    }, {
      "indices" : [ 81, 101 ],
      "url" : "http://t.co/Rg1U1Pxn",
      "expanded_url" : "http://img.ly/mt4a",
      "display_url" : "img.ly/mt4a"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239358756754964480",
  "text" : "The Tux to my Chrome Experiments Tie - The ASCII Art Museum http://t.co/4k45T5vr http://t.co/Rg1U1Pxn #ascii #art",
  "id" : 239358756754964480,
  "created_at" : "Sat Aug 25 13:49:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/NagddQTw",
      "expanded_url" : "http://bit.ly/PxbhTy",
      "display_url" : "bit.ly/PxbhTy"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239120260194779136",
  "text" : "Soulwire \u00BB Experiments in Code http://t.co/NagddQTw Oh yes people, Oh very merilly yes",
  "id" : 239120260194779136,
  "created_at" : "Fri Aug 24 22:01:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "url",
      "indices" : [ 52, 56 ]
    }, {
      "text" : "shotener",
      "indices" : [ 57, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/H2gfiPHj",
      "expanded_url" : "http://bit.ly/NdLOun",
      "display_url" : "bit.ly/NdLOun"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239051977584177152",
  "text" : "YOURLS: Your Own URL Shortener http://t.co/H2gfiPHj #url #shotener",
  "id" : 239051977584177152,
  "created_at" : "Fri Aug 24 17:30:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/4vkjMy8j",
      "expanded_url" : "http://bit.ly/PxaRwj",
      "display_url" : "bit.ly/PxaRwj"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239046969815875584",
  "text" : "Giant URLs - where giant is better! http://t.co/4vkjMy8j",
  "id" : 239046969815875584,
  "created_at" : "Fri Aug 24 17:10:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css3",
      "indices" : [ 51, 56 ]
    }, {
      "text" : "jsfiddle",
      "indices" : [ 57, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/MJ2vQE2H",
      "expanded_url" : "http://bit.ly/PxaQsg",
      "display_url" : "bit.ly/PxaQsg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239021862590496769",
  "text" : "CSS3 Curved Ribbon - jsFiddle http://t.co/MJ2vQE2H #css3 #jsfiddle",
  "id" : 239021862590496769,
  "created_at" : "Fri Aug 24 15:30:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cloudapp",
      "indices" : [ 76, 85 ]
    }, {
      "text" : "client",
      "indices" : [ 86, 93 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/GFT4TmAV",
      "expanded_url" : "http://bit.ly/PxaNgb",
      "display_url" : "bit.ly/PxaNgb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "239016751201939456",
  "text" : "\"BlueNube is (currently) an iPad client for CloudApp.\" http://t.co/GFT4TmAV #cloudapp #client",
  "id" : 239016751201939456,
  "created_at" : "Fri Aug 24 15:10:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 130, 137 ]
    } ],
    "urls" : [ {
      "indices" : [ 109, 129 ],
      "url" : "http://t.co/F7PpLC4U",
      "expanded_url" : "http://bit.ly/Pxat10",
      "display_url" : "bit.ly/Pxat10"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238757833942917121",
  "text" : "\"AutoResize is a very basic jQuery plug-in that provides an \"automatic resize\" effect on textarea elements.\" http://t.co/F7PpLC4U #jquery",
  "id" : 238757833942917121,
  "created_at" : "Thu Aug 23 22:01:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 68, 75 ]
    }, {
      "text" : "googlewave",
      "indices" : [ 76, 87 ]
    }, {
      "text" : "embed",
      "indices" : [ 88, 94 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/FoVYRck8",
      "expanded_url" : "http://bit.ly/Pxakui",
      "display_url" : "bit.ly/Pxakui"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238689662267449344",
  "text" : "\"jWave | Easily embed Google Wave with jQuery\" http://t.co/FoVYRck8 #jquery #googlewave #embed YES, Wave is still around people!",
  "id" : 238689662267449344,
  "created_at" : "Thu Aug 23 17:30:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iphone",
      "indices" : [ 117, 124 ]
    }, {
      "text" : "emulation",
      "indices" : [ 125, 135 ]
    }, {
      "text" : "jq",
      "indices" : [ 136, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 96, 116 ],
      "url" : "http://t.co/3CYSo95b",
      "expanded_url" : "http://bit.ly/NdLfkd",
      "display_url" : "bit.ly/NdLfkd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238684552099397632",
  "text" : "\"Overscroll is a jQuery plug-in that mimics the iphone/ipad scrolling experience in a browser.\" http://t.co/3CYSo95b #iphone #emulation #jq",
  "id" : 238684552099397632,
  "created_at" : "Thu Aug 23 17:10:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webalon",
      "indices" : [ 101, 109 ]
    }, {
      "text" : "webdesign",
      "indices" : [ 110, 120 ]
    } ],
    "urls" : [ {
      "indices" : [ 80, 100 ],
      "url" : "http://t.co/eerDq7oQ",
      "expanded_url" : "http://bit.ly/Pxadim",
      "display_url" : "bit.ly/Pxadim"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238659405904764928",
  "text" : "\"At Webalon, we love nothing more than hearing about the ideas that excite you\" http://t.co/eerDq7oQ #webalon #webdesign",
  "id" : 238659405904764928,
  "created_at" : "Thu Aug 23 15:30:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 71, 75 ]
    }, {
      "text" : "animation",
      "indices" : [ 76, 86 ]
    } ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http://t.co/Dch8fpbN",
      "expanded_url" : "http://bit.ly/Pxa5zx",
      "display_url" : "bit.ly/Pxa5zx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238654348924698624",
  "text" : "Ceaser - CSS Easing Animation Tool - Matthew Lein http://t.co/Dch8fpbN #css #animation",
  "id" : 238654348924698624,
  "created_at" : "Thu Aug 23 15:10:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "php",
      "indices" : [ 43, 47 ]
    }, {
      "text" : "datauri",
      "indices" : [ 48, 56 ]
    }, {
      "text" : "css",
      "indices" : [ 57, 61 ]
    }, {
      "text" : "images",
      "indices" : [ 62, 69 ]
    } ],
    "urls" : [ {
      "indices" : [ 22, 42 ],
      "url" : "http://t.co/ggsoesSU",
      "expanded_url" : "http://bit.ly/NdL0Wn",
      "display_url" : "bit.ly/NdL0Wn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238395414154448896",
  "text" : "PHP Datauri Convertor http://t.co/ggsoesSU #php #datauri #css #images",
  "id" : 238395414154448896,
  "created_at" : "Wed Aug 22 22:01:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css3",
      "indices" : [ 48, 53 ]
    }, {
      "text" : "framework",
      "indices" : [ 54, 64 ]
    }, {
      "text" : "buttons",
      "indices" : [ 65, 73 ]
    } ],
    "urls" : [ {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/PI7VgXQR",
      "expanded_url" : "http://bit.ly/Px9EVM",
      "display_url" : "bit.ly/Px9EVM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238327241308790785",
  "text" : "\"The Buttonize Framework \" http://t.co/PI7VgXQR #css3 #framework #buttons",
  "id" : 238327241308790785,
  "created_at" : "Wed Aug 22 17:30:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 84 ],
      "url" : "http://t.co/jCrBgyue",
      "expanded_url" : "http://bit.ly/Px9A8w",
      "display_url" : "bit.ly/Px9A8w"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238322160853188608",
  "text" : "Free Website Builder - Moonfruit - Total website design control http://t.co/jCrBgyue",
  "id" : 238322160853188608,
  "created_at" : "Wed Aug 22 17:10:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "french",
      "indices" : [ 75, 82 ]
    }, {
      "text" : "hacking",
      "indices" : [ 83, 91 ]
    } ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/mJKzWwV9",
      "expanded_url" : "http://bit.ly/NdKw2n",
      "display_url" : "bit.ly/NdKw2n"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238297010925621249",
  "text" : "S\u00E9b koreth BAUDRU (cowreth) on Twitter http://t.co/mJKzWwV9 French Hacker. #french #hacking",
  "id" : 238297010925621249,
  "created_at" : "Wed Aug 22 15:30:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html5",
      "indices" : [ 62, 68 ]
    }, {
      "text" : "css3",
      "indices" : [ 69, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/iVtBdWBF",
      "expanded_url" : "http://bit.ly/Px9nlM",
      "display_url" : "bit.ly/Px9nlM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238291970911522817",
  "text" : "HTML5 logo made with CSS3 - demo updated http://t.co/iVtBdWBF #html5 #css3",
  "id" : 238291970911522817,
  "created_at" : "Wed Aug 22 15:10:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/dnjiLaqe",
      "expanded_url" : "http://bit.ly/PFvfeO",
      "display_url" : "bit.ly/PFvfeO"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238270752783212544",
  "text" : "Chirp.js \u2014\u00A0Tweets on your website, simply. http://t.co/dnjiLaqe",
  "id" : 238270752783212544,
  "created_at" : "Wed Aug 22 13:45:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kasper Mikiewicz",
      "screen_name" : "Idered",
      "indices" : [ 0, 7 ],
      "id_str" : "49586611",
      "id" : 49586611
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "237961432082305024",
  "geo" : {
  },
  "id_str" : "238254035034050561",
  "in_reply_to_user_id" : 49586611,
  "text" : "@Idered Hi there. I replied to your comment.",
  "id" : 238254035034050561,
  "in_reply_to_status_id" : 237961432082305024,
  "created_at" : "Wed Aug 22 12:39:23 +0000 2012",
  "in_reply_to_screen_name" : "Idered",
  "in_reply_to_user_id_str" : "49586611",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css3",
      "indices" : [ 57, 62 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/zcZICUpg",
      "expanded_url" : "http://bit.ly/Px9luc",
      "display_url" : "bit.ly/Px9luc"
    } ]
  },
  "geo" : {
  },
  "id_str" : "238033031925993472",
  "text" : "Sexy CSS3 menu demo - RedTeamDesign http://t.co/zcZICUpg #css3",
  "id" : 238033031925993472,
  "created_at" : "Tue Aug 21 22:01:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "divya",
      "indices" : [ 38, 44 ]
    }, {
      "text" : "manian",
      "indices" : [ 45, 52 ]
    }, {
      "text" : "css",
      "indices" : [ 53, 57 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/cNi0TxEp",
      "expanded_url" : "http://bit.ly/NdKlUX",
      "display_url" : "bit.ly/NdKlUX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237964821583253505",
  "text" : "\"CSS Vocabulary\" http://t.co/cNi0TxEp #divya #manian #css",
  "id" : 237964821583253505,
  "created_at" : "Tue Aug 21 17:30:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css3",
      "indices" : [ 67, 72 ]
    }, {
      "text" : "omg",
      "indices" : [ 73, 77 ]
    }, {
      "text" : "text",
      "indices" : [ 78, 83 ]
    }, {
      "text" : "effect",
      "indices" : [ 84, 91 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/gWsZfzUj",
      "expanded_url" : "http://bit.ly/NdKjMG",
      "display_url" : "bit.ly/NdKjMG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237959781661437954",
  "text" : "\"OMG TEXT! RIDICULOUSLY AWESOME TEXT SHADOWS\" http://t.co/gWsZfzUj #css3 #omg #text #effect",
  "id" : 237959781661437954,
  "created_at" : "Tue Aug 21 17:10:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "pupius",
      "indices" : [ 83, 90 ]
    }, {
      "text" : "oldskool",
      "indices" : [ 91, 100 ]
    }, {
      "text" : "dhtml",
      "indices" : [ 101, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 62, 82 ],
      "url" : "http://t.co/7V4cOqJU",
      "expanded_url" : "http://bit.ly/Px8ZDY",
      "display_url" : "bit.ly/Px8ZDY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237934634837626880",
  "text" : "\"PHP wrapper for the Google Closure JS Compiler web service.\" http://t.co/7V4cOqJU #pupius #oldskool #dhtml'er",
  "id" : 237934634837626880,
  "created_at" : "Tue Aug 21 15:30:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "github",
      "indices" : [ 46, 53 ]
    }, {
      "text" : "help",
      "indices" : [ 54, 59 ]
    } ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/XOmIBQjp",
      "expanded_url" : "http://bit.ly/NdJWSe",
      "display_url" : "bit.ly/NdJWSe"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237929580667752448",
  "text" : "Set Up Git \u00B7 github:help http://t.co/XOmIBQjp #github #help",
  "id" : 237929580667752448,
  "created_at" : "Tue Aug 21 15:10:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 68, 75 ]
    }, {
      "text" : "css3",
      "indices" : [ 76, 81 ]
    }, {
      "text" : "animation",
      "indices" : [ 82, 92 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/Rhl6wqEc",
      "expanded_url" : "http://bit.ly/OPRuwb",
      "display_url" : "bit.ly/OPRuwb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237670765993013248",
  "text" : "\"GFX - a 3D CSS3 animation library for jQuery\" http://t.co/Rhl6wqEc #jquery #css3 #animation",
  "id" : 237670765993013248,
  "created_at" : "Mon Aug 20 22:01:40 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 123, 132 ],
      "id_str" : "14139773",
      "id" : 14139773
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bootstrap",
      "indices" : [ 21, 31 ]
    }, {
      "text" : "cdn",
      "indices" : [ 32, 36 ]
    } ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/6kynMNrE",
      "expanded_url" : "http://bootstrapcdn.blogspot.com/2012/08/hello-world-and-updates.html",
      "display_url" : "bootstrapcdn.blogspot.com/2012/08/hello-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237633993166712832",
  "text" : "http://t.co/6kynMNrE #bootstrap #cdn To date we now have had over 21 million requests and 187.55 GB transferred. Well done @jdorfman +9000",
  "id" : 237633993166712832,
  "created_at" : "Mon Aug 20 19:35:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/J4CqwwLU",
      "expanded_url" : "http://bit.ly/OPRk7N",
      "display_url" : "bit.ly/OPRk7N"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237602434967564288",
  "text" : "Text -&gt; HTML Entities Encoder http://t.co/J4CqwwLU",
  "id" : 237602434967564288,
  "created_at" : "Mon Aug 20 17:30:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "php",
      "indices" : [ 47, 51 ]
    }, {
      "text" : "conversion",
      "indices" : [ 52, 63 ]
    }, {
      "text" : "html2pdf",
      "indices" : [ 64, 73 ]
    } ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/jZf6uIh9",
      "expanded_url" : "http://bit.ly/NdJNye",
      "display_url" : "bit.ly/NdJNye"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237597705944326144",
  "text" : "\"HTML to PDF API for PHP\" http://t.co/jZf6uIh9 #php #conversion #html2pdf",
  "id" : 237597705944326144,
  "created_at" : "Mon Aug 20 17:11:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html5",
      "indices" : [ 62, 68 ]
    } ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/Z8VSOMoy",
      "expanded_url" : "http://bit.ly/Px8I3G",
      "display_url" : "bit.ly/Px8I3G"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237572289116377088",
  "text" : "\"The Web platform: Browser technologies\" http://t.co/Z8VSOMoy #html5",
  "id" : 237572289116377088,
  "created_at" : "Mon Aug 20 15:30:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 64, 75 ]
    }, {
      "text" : "obfuscate",
      "indices" : [ 76, 86 ]
    } ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/kzEjvZ2n",
      "expanded_url" : "http://bit.ly/NdJIe3",
      "display_url" : "bit.ly/NdJIe3"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237567341687361536",
  "text" : "\"Javascript Code Encrypter And Obfuscator\" http://t.co/kzEjvZ2n #javascript #obfuscate",
  "id" : 237567341687361536,
  "created_at" : "Mon Aug 20 15:10:42 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 58 ],
      "url" : "http://t.co/Gx0hKn26",
      "expanded_url" : "http://ifttt.com",
      "display_url" : "ifttt.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237342114432839680",
  "text" : "I was just thinking, with the service http://t.co/Gx0hKn26, you could make a virus with it.Hook up all the services, and do serious flooding",
  "id" : 237342114432839680,
  "created_at" : "Mon Aug 20 00:15:44 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 89 ],
      "url" : "http://t.co/Sm6kiV1O",
      "expanded_url" : "http://www.azoffdesign.com/autoresize",
      "display_url" : "azoffdesign.com/autoresize"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237327618993704961",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog You said a while back you wanted this. Here you go - http://t.co/Sm6kiV1O Don't re-invent the wheel",
  "id" : 237327618993704961,
  "created_at" : "Sun Aug 19 23:18:08 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 79 ],
      "url" : "http://t.co/mF9CstU3",
      "expanded_url" : "http://userscripts.org/scripts/show/139155",
      "display_url" : "userscripts.org/scripts/show/1\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "237309340476899328",
  "geo" : {
  },
  "id_str" : "237314197225107456",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys Ahh Quora has done an Experts Exchange on us. Fires up http://t.co/mF9CstU3 Walled garden of data, no more!",
  "id" : 237314197225107456,
  "in_reply_to_status_id" : 237309340476899328,
  "created_at" : "Sun Aug 19 22:24:48 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stefan D\u00FChring",
      "screen_name" : "Autarc",
      "indices" : [ 83, 90 ],
      "id_str" : "52120068",
      "id" : 52120068
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/zbUcyIqy",
      "expanded_url" : "http://post.ly/8uGZG",
      "display_url" : "post.ly/8uGZG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237298162639978496",
  "text" : "Favorited \u2605 http://t.co/zbUcyIqy Pasteboard \u2192 a simple image uploading service via @Autarc",
  "id" : 237298162639978496,
  "created_at" : "Sun Aug 19 21:21:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "vendor",
      "indices" : [ 110, 117 ]
    }, {
      "text" : "prefixes",
      "indices" : [ 118, 127 ]
    } ],
    "urls" : [ {
      "indices" : [ 89, 109 ],
      "url" : "http://t.co/wNXpjZEC",
      "expanded_url" : "http://bit.ly/P5s1ii",
      "display_url" : "bit.ly/P5s1ii"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237184667407761408",
  "text" : "\"You hate writing vendor prefixes for all browsers? Just let CSSPrefixer do it for you!\" http://t.co/wNXpjZEC #vendor #prefixes",
  "id" : 237184667407761408,
  "created_at" : "Sun Aug 19 13:50:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "kuvos",
      "indices" : [ 32, 38 ]
    }, {
      "text" : "linkdump",
      "indices" : [ 39, 48 ]
    }, {
      "text" : "js",
      "indices" : [ 49, 52 ]
    }, {
      "text" : "javascript",
      "indices" : [ 53, 64 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/mriORYRI",
      "expanded_url" : "http://bit.ly/P5rXiF",
      "display_url" : "bit.ly/P5rXiF"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237179633785778176",
  "text" : "JS GooDies http://t.co/mriORYRI #kuvos #linkdump #js #javascript",
  "id" : 237179633785778176,
  "created_at" : "Sun Aug 19 13:30:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 50, 57 ]
    }, {
      "text" : "plugin",
      "indices" : [ 58, 65 ]
    } ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/85Pn02ff",
      "expanded_url" : "http://bit.ly/RqP7WA",
      "display_url" : "bit.ly/RqP7WA"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237174636889124864",
  "text" : "Demo::Pushup! Content Slider http://t.co/85Pn02ff #jquery #plugin",
  "id" : 237174636889124864,
  "created_at" : "Sun Aug 19 13:10:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fonts",
      "indices" : [ 47, 53 ]
    } ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/hZoMrHdl",
      "expanded_url" : "http://bit.ly/P5rHAb",
      "display_url" : "bit.ly/P5rHAb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237169589845377024",
  "text" : "Aver\u00EDa \u2013 The Average Font http://t.co/hZoMrHdl #fonts",
  "id" : 237169589845377024,
  "created_at" : "Sun Aug 19 12:50:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "yetanother",
      "indices" : [ 51, 62 ]
    }, {
      "text" : "slider",
      "indices" : [ 63, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/faoV3eKW",
      "expanded_url" : "http://bit.ly/RqOSdS",
      "display_url" : "bit.ly/RqOSdS"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237164600292954113",
  "text" : "Rhinoslider: slider/slideshow http://t.co/faoV3eKW #yetanother #slider But good. An actual _good_ jquery slider. Oh my gothse",
  "id" : 237164600292954113,
  "created_at" : "Sun Aug 19 12:30:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stefan D\u00FChring",
      "screen_name" : "Autarc",
      "indices" : [ 3, 10 ],
      "id_str" : "52120068",
      "id" : 52120068
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http://t.co/7wHQYcvt",
      "expanded_url" : "http://pasteboard.co/",
      "display_url" : "pasteboard.co"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237144195205316610",
  "text" : "RT @Autarc: pasteboard - a simple image uploading service || http://t.co/7wHQYcvt",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 49, 69 ],
        "url" : "http://t.co/7wHQYcvt",
        "expanded_url" : "http://pasteboard.co/",
        "display_url" : "pasteboard.co"
      } ]
    },
    "geo" : {
    },
    "id_str" : "237139603990118400",
    "text" : "pasteboard - a simple image uploading service || http://t.co/7wHQYcvt",
    "id" : 237139603990118400,
    "created_at" : "Sun Aug 19 10:51:02 +0000 2012",
    "user" : {
      "name" : "Stefan D\u00FChring",
      "screen_name" : "Autarc",
      "protected" : false,
      "id_str" : "52120068",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2131565652/Stefan-PNG_normal.png",
      "id" : 52120068,
      "verified" : false
    }
  },
  "id" : 237144195205316610,
  "created_at" : "Sun Aug 19 11:09:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 98 ],
      "url" : "http://t.co/mS9dNv0z",
      "expanded_url" : "http://branch.com/",
      "display_url" : "branch.com"
    }, {
      "indices" : [ 118, 139 ],
      "url" : "https://t.co/ERvbKMMa",
      "expanded_url" : "https://medium.com/",
      "display_url" : "medium.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237119937909510144",
  "text" : "Two new publishing platforms to sink our teeth into in late 2012 / early 2013 http://t.co/mS9dNv0z Branch, and Medium https://t.co/ERvbKMMa",
  "id" : 237119937909510144,
  "created_at" : "Sun Aug 19 09:32:53 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 81, 85 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "237119192560701440",
  "text" : "&lt;\u2193sc\u2191ri\u2193ptt\u2191&gt;ale\u2193rt('\u21911\u2193') +.\u2193\uFF9F(*\u00B4\u2191\u2200`)b\uFF9F\u2193+\u2191.\uFF9F \uFF6E\uFF9B\uFF81\u2193\uFF78\u2191\uFF69 (Firefox 13.00 only) #xss",
  "id" : 237119192560701440,
  "created_at" : "Sun Aug 19 09:29:55 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 103, 107 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "237119098197250049",
  "text" : "/a/l/e/r/t/(/'/1/'/)/ / \u2192 /a/ /l/\u301C/ /e/ /r/ /t/ /(/ /'/\u301C/ /1/ /'/ /)/ / *.:.\uFF61.\uFF61\u3002\u2192 \u222Ealert('1')\u222E\u3002\uFF61.\uFF61.:.* #xss",
  "id" : 237119098197250049,
  "created_at" : "Sun Aug 19 09:29:33 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 37, 41 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "237118863706300416",
  "text" : "&lt;&gt;aalleerrtt((''11''))&lt;&gt; #xss",
  "id" : 237118863706300416,
  "created_at" : "Sun Aug 19 09:28:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 50, 61 ],
      "id_str" : "104462925",
      "id" : 104462925
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/2u3GqAdx",
      "expanded_url" : "http://post.ly/8tGCa",
      "display_url" : "post.ly/8tGCa"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237113328734973953",
  "text" : "Favorited \u2605 http://t.co/2u3GqAdx Metro UI CSS via @techiejayk",
  "id" : 237113328734973953,
  "created_at" : "Sun Aug 19 09:06:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kyle",
      "screen_name" : "getify",
      "indices" : [ 117, 124 ],
      "id_str" : "16686076",
      "id" : 16686076
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/7xFuiSwL",
      "expanded_url" : "http://post.ly/8tUVC",
      "display_url" : "post.ly/8tUVC"
    } ]
  },
  "geo" : {
  },
  "id_str" : "237113185088446464",
  "text" : "Favorited \u2605 http://t.co/7xFuiSwL \"crockford is so predictable that he could now be replaced with a shell script\" via @getify",
  "id" : 237113185088446464,
  "created_at" : "Sun Aug 19 09:06:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Licorize News",
      "screen_name" : "licorizenews",
      "indices" : [ 0, 13 ],
      "id_str" : "131183696",
      "id" : 131183696
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/6QXqy24u",
      "expanded_url" : "http://licorize.com",
      "display_url" : "licorize.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236995988928229377",
  "in_reply_to_user_id" : 131183696,
  "text" : "@licorizenews Can you please tell me why http://t.co/6QXqy24u is down. Very frustrating! :(",
  "id" : 236995988928229377,
  "created_at" : "Sun Aug 19 01:20:21 +0000 2012",
  "in_reply_to_screen_name" : "licorizenews",
  "in_reply_to_user_id_str" : "131183696",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Irish",
      "screen_name" : "paul_irish",
      "indices" : [ 0, 11 ],
      "id_str" : "1671811",
      "id" : 1671811
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "opml",
      "indices" : [ 58, 63 ]
    }, {
      "text" : "wordpress",
      "indices" : [ 64, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http://t.co/EEYJffm4",
      "expanded_url" : "http://isharefil.es/Ir5k/developers-we-admire.xml",
      "display_url" : "isharefil.es/Ir5k/developer\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236991326795735040",
  "in_reply_to_user_id" : 1671811,
  "text" : "@paul_irish OPML for fun and profit. http://t.co/EEYJffm4 #opml #wordpress",
  "id" : 236991326795735040,
  "created_at" : "Sun Aug 19 01:01:50 +0000 2012",
  "in_reply_to_screen_name" : "paul_irish",
  "in_reply_to_user_id_str" : "1671811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "lunatic",
      "indices" : [ 60, 68 ]
    } ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/jbWOOZJp",
      "expanded_url" : "http://EdanHewitt.com",
      "display_url" : "EdanHewitt.com"
    }, {
      "indices" : [ 119, 139 ],
      "url" : "http://t.co/tr9ealkM",
      "expanded_url" : "http://edanhewitt.com/",
      "display_url" : "edanhewitt.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236957823492313088",
  "text" : "http://t.co/jbWOOZJp Just keeps getting upgrades. A sincere #lunatic in the making. Ladies and gents, I give you Edan \u2192http://t.co/tr9ealkM",
  "id" : 236957823492313088,
  "created_at" : "Sat Aug 18 22:48:42 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/BvMqGcl5",
      "expanded_url" : "http://iwantaneff.in/linkdump/",
      "display_url" : "iwantaneff.in/linkdump/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236957412815417344",
  "text" : "http://t.co/BvMqGcl5 Linkdumps FTW",
  "id" : 236957412815417344,
  "created_at" : "Sat Aug 18 22:47:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "spam",
      "indices" : [ 107, 112 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/VMRK9Lkx",
      "expanded_url" : "http://isharefil.es/IrBX",
      "display_url" : "isharefil.es/IrBX"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236943917663256576",
  "text" : "10:52 PM on August 18th, 2012 http://t.co/VMRK9Lkx The best time to prune spam accounts from your Twitterz #spam",
  "id" : 236943917663256576,
  "created_at" : "Sat Aug 18 21:53:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Irish",
      "screen_name" : "paul_irish",
      "indices" : [ 0, 11 ],
      "id_str" : "1671811",
      "id" : 1671811
    }, {
      "name" : "niftylettuce",
      "screen_name" : "niftylettuce",
      "indices" : [ 12, 25 ],
      "id_str" : "214358209",
      "id" : 214358209
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/Qr4xyUyr",
      "expanded_url" : "http://iwantaneff.in/bin/view/ee173a1c",
      "display_url" : "iwantaneff.in/bin/view/ee173\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "236927209166749696",
  "geo" : {
  },
  "id_str" : "236932304591790081",
  "in_reply_to_user_id" : 1671811,
  "text" : "@paul_irish @niftylettuce Here's all their websites. http://t.co/Qr4xyUyr",
  "id" : 236932304591790081,
  "in_reply_to_status_id" : 236927209166749696,
  "created_at" : "Sat Aug 18 21:07:18 +0000 2012",
  "in_reply_to_screen_name" : "paul_irish",
  "in_reply_to_user_id_str" : "1671811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Irish",
      "screen_name" : "paul_irish",
      "indices" : [ 0, 11 ],
      "id_str" : "1671811",
      "id" : 1671811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "236927177919193088",
  "geo" : {
  },
  "id_str" : "236929209556140032",
  "in_reply_to_user_id" : 1671811,
  "text" : "@paul_irish No probs. ;)",
  "id" : 236929209556140032,
  "in_reply_to_status_id" : 236927177919193088,
  "created_at" : "Sat Aug 18 20:55:00 +0000 2012",
  "in_reply_to_screen_name" : "paul_irish",
  "in_reply_to_user_id_str" : "1671811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Alman",
      "screen_name" : "cowboy",
      "indices" : [ 44, 51 ],
      "id_str" : "6798592",
      "id" : 6798592
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/OGBI7L9h",
      "expanded_url" : "http://iwantaneff.in/twitter/",
      "display_url" : "iwantaneff.in/twitter/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236919223908192257",
  "text" : "Tweetdump. *New* http://t.co/OGBI7L9h cc:// @cowboy Linkification works a charm here. +1",
  "id" : 236919223908192257,
  "created_at" : "Sat Aug 18 20:15:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/iTuVy5Bd",
      "expanded_url" : "http://laterstars.com/_higg_/all",
      "display_url" : "laterstars.com/_higg_/all"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236913606355202049",
  "text" : "My Laterstars Fave-dump - http://t.co/iTuVy5Bd If I didn't favorite it, it didn't happen.",
  "id" : 236913606355202049,
  "created_at" : "Sat Aug 18 19:53:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 37, 48 ],
      "id_str" : "104462925",
      "id" : 104462925
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/2u3GqAdx",
      "expanded_url" : "http://post.ly/8tGCa",
      "display_url" : "post.ly/8tGCa"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236913049636847616",
  "text" : "Favorited \u2605 http://t.co/2u3GqAdx via @techiejayk Metro UI CSS",
  "id" : 236913049636847616,
  "created_at" : "Sat Aug 18 19:50:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 68, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/n6sTw5xU",
      "expanded_url" : "http://bit.ly/RqOMTP",
      "display_url" : "bit.ly/RqOMTP"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236897863878180865",
  "text" : "Roman Komarov (Experiment) \u2192 Scrolling shadows http://t.co/n6sTw5xU #css",
  "id" : 236897863878180865,
  "created_at" : "Sat Aug 18 18:50:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 74 ],
      "url" : "http://t.co/SvaAifbn",
      "expanded_url" : "http://bit.ly/P5rvRl",
      "display_url" : "bit.ly/P5rvRl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236893003774566400",
  "text" : "Cloud9 IDE | Online IDE \u2013 Your code anywhere, anytime http://t.co/SvaAifbn",
  "id" : 236893003774566400,
  "created_at" : "Sat Aug 18 18:31:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "obfuscate",
      "indices" : [ 57, 67 ]
    }, {
      "text" : "antispam",
      "indices" : [ 68, 77 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/wIJT6VJz",
      "expanded_url" : "http://bit.ly/P5rrBg",
      "display_url" : "bit.ly/P5rrBg"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236887724672557056",
  "text" : "Hide email address in source code \u2192 http://t.co/wIJT6VJz #obfuscate #antispam",
  "id" : 236887724672557056,
  "created_at" : "Sat Aug 18 18:10:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 3, 14 ],
      "id_str" : "104462925",
      "id" : 104462925
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/JY8n9k61",
      "expanded_url" : "http://j.mp/OWmU3N",
      "display_url" : "j.mp/OWmU3N"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236885675213996032",
  "text" : "RT @techiejayk: Metro UI CSS http://t.co/JY8n9k61",
  "retweeted_status" : {
    "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 13, 33 ],
        "url" : "http://t.co/JY8n9k61",
        "expanded_url" : "http://j.mp/OWmU3N",
        "display_url" : "j.mp/OWmU3N"
      } ]
    },
    "geo" : {
    },
    "id_str" : "236885379704320000",
    "text" : "Metro UI CSS http://t.co/JY8n9k61",
    "id" : 236885379704320000,
    "created_at" : "Sat Aug 18 18:00:50 +0000 2012",
    "user" : {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "protected" : false,
      "id_str" : "104462925",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3050293497/f330d8ba9496279d1ad37559cdf81a06_normal.jpeg",
      "id" : 104462925,
      "verified" : false
    }
  },
  "id" : 236885675213996032,
  "created_at" : "Sat Aug 18 18:02:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/UMGqHnM0",
      "expanded_url" : "http://www.downforeveryoneorjustme.com/downforeveryoneorjustme.com",
      "display_url" : "downforeveryoneorjustme.com/downforeveryon\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236884156326486016",
  "text" : "Down for everyone or just me is not down \u2192 http://t.co/UMGqHnM0 I just broke the internets!",
  "id" : 236884156326486016,
  "created_at" : "Sat Aug 18 17:55:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fonts",
      "indices" : [ 38, 44 ]
    }, {
      "text" : "typeography",
      "indices" : [ 45, 57 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/hWZOnabL",
      "expanded_url" : "http://bit.ly/RqOwnB",
      "display_url" : "bit.ly/RqOwnB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236882748390920192",
  "text" : "WebINK Web Fonts http://t.co/hWZOnabL #fonts #typeography",
  "id" : 236882748390920192,
  "created_at" : "Sat Aug 18 17:50:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Steeber",
      "screen_name" : "MichaelSteeber",
      "indices" : [ 37, 52 ],
      "id_str" : "198732594",
      "id" : 198732594
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/wykI0KUg",
      "expanded_url" : "http://post.ly/8tDdR",
      "display_url" : "post.ly/8tDdR"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236878836237733888",
  "text" : "Favorited \u2605 http://t.co/wykI0KUg via @MichaelSteeber",
  "id" : 236878836237733888,
  "created_at" : "Sat Aug 18 17:34:50 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "internetofthings",
      "indices" : [ 46, 63 ]
    } ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/khvPU6ex",
      "expanded_url" : "http://bit.ly/RqOo7C",
      "display_url" : "bit.ly/RqOo7C"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236877745613836289",
  "text" : "The Internet of Things \u2192 http://t.co/khvPU6ex #internetofthings",
  "id" : 236877745613836289,
  "created_at" : "Sat Aug 18 17:30:30 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 77 ],
      "url" : "http://t.co/E9C2rmno",
      "expanded_url" : "http://bit.ly/RonAFf",
      "display_url" : "bit.ly/RonAFf"
    }, {
      "indices" : [ 105, 125 ],
      "url" : "http://t.co/Fkwhv8Bm",
      "expanded_url" : "http://good.net",
      "display_url" : "good.net"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236876475121426435",
  "text" : "\"This site will cease operations after August 31, 2012.\" http://t.co/E9C2rmno Nooo, my favorite resource http://t.co/Fkwhv8Bm going bust :(",
  "id" : 236876475121426435,
  "created_at" : "Sat Aug 18 17:25:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Licorize News",
      "screen_name" : "licorizenews",
      "indices" : [ 0, 13 ],
      "id_str" : "131183696",
      "id" : 131183696
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/nfscUMHz",
      "expanded_url" : "http://www.downforeveryoneorjustme.com/licorize.com",
      "display_url" : "downforeveryoneorjustme.com/licorize.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236875291098742784",
  "in_reply_to_user_id" : 131183696,
  "text" : "@licorizenews Licorize is down for some reason? http://t.co/nfscUMHz",
  "id" : 236875291098742784,
  "created_at" : "Sat Aug 18 17:20:44 +0000 2012",
  "in_reply_to_screen_name" : "licorizenews",
  "in_reply_to_user_id_str" : "131183696",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matteo Campofiorito",
      "screen_name" : "matteoca",
      "indices" : [ 3, 12 ],
      "id_str" : "284703",
      "id" : 284703
    }, {
      "name" : "XSS Vector",
      "screen_name" : "XSSVector",
      "indices" : [ 17, 27 ],
      "id_str" : "608318510",
      "id" : 608318510
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "236874087480315904",
  "text" : "RT @matteoca: RT @XSSVector: Firefox cookie xss: with(document)cookie='\u223C\u2269\u226D\u2267\u222F\u2273\u2272\u2263\u223D\u2278\u2278\u223A\u2278\u2220\u226F\u226E\u2265\u2272\u2272\u226F\u2272\u223D\u2261\u226C\u2265\u2272\u2274\u2228\u2231\u2229\u223E',write(cookie);   http://t.co/w1t ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/tweetbot\" rel=\"nofollow\">Tweetbot for iOS</a>",
    "entities" : {
      "user_mentions" : [ {
        "name" : "XSS Vector",
        "screen_name" : "XSSVector",
        "indices" : [ 3, 13 ],
        "id_str" : "608318510",
        "id" : 608318510
      }, {
        "name" : "Pujun Li",
        "screen_name" : "jackmasa",
        "indices" : [ 131, 140 ],
        "id_str" : "256454626",
        "id" : 256454626
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 107, 127 ],
        "url" : "http://t.co/w1tYr4xy",
        "expanded_url" : "http://jsbin.com/ecarel",
        "display_url" : "jsbin.com/ecarel"
      } ]
    },
    "geo" : {
    },
    "id_str" : "236865707076382721",
    "text" : "RT @XSSVector: Firefox cookie xss: with(document)cookie='\u223C\u2269\u226D\u2267\u222F\u2273\u2272\u2263\u223D\u2278\u2278\u223A\u2278\u2220\u226F\u226E\u2265\u2272\u2272\u226F\u2272\u223D\u2261\u226C\u2265\u2272\u2274\u2228\u2231\u2229\u223E',write(cookie);   http://t.co/w1tYr4xy by @jackmasa",
    "id" : 236865707076382721,
    "created_at" : "Sat Aug 18 16:42:39 +0000 2012",
    "user" : {
      "name" : "Matteo Campofiorito",
      "screen_name" : "matteoca",
      "protected" : false,
      "id_str" : "284703",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/565221978/foto_normal.jpg",
      "id" : 284703,
      "verified" : false
    }
  },
  "id" : 236874087480315904,
  "created_at" : "Sat Aug 18 17:15:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "polyfill",
      "indices" : [ 63, 72 ]
    }, {
      "text" : "leaverou",
      "indices" : [ 73, 82 ]
    } ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/CG4jQO7T",
      "expanded_url" : "http://bit.ly/P5rdtT",
      "display_url" : "bit.ly/P5rdtT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236872662163537922",
  "text" : "Polyfill for the HTML5 PROGRESS element \u2192 http://t.co/CG4jQO7T #polyfill #leaverou",
  "id" : 236872662163537922,
  "created_at" : "Sat Aug 18 17:10:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "windows",
      "indices" : [ 128, 136 ]
    } ],
    "urls" : [ {
      "indices" : [ 107, 127 ],
      "url" : "http://t.co/GAs0SIhb",
      "expanded_url" : "http://bit.ly/P3HgZd",
      "display_url" : "bit.ly/P3HgZd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236852499477323777",
  "text" : "SHELL EXTENSION CITY, millions of free Windows power tools, explorer enhancements, windows add-ons, tweaks http://t.co/GAs0SIhb #windows",
  "id" : 236852499477323777,
  "created_at" : "Sat Aug 18 15:50:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sidetap",
      "indices" : [ 44, 52 ]
    }, {
      "text" : "mobile",
      "indices" : [ 53, 60 ]
    }, {
      "text" : "plugin",
      "indices" : [ 61, 68 ]
    } ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/1hVQa3a1",
      "expanded_url" : "http://bit.ly/RooCkx",
      "display_url" : "bit.ly/RooCkx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236847529646489601",
  "text" : "Sidetap Mobile Library http://t.co/1hVQa3a1 #sidetap #mobile #plugin",
  "id" : 236847529646489601,
  "created_at" : "Sat Aug 18 15:30:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 39, 46 ]
    }, {
      "text" : "awesome",
      "indices" : [ 47, 55 ]
    } ],
    "urls" : [ {
      "indices" : [ 18, 38 ],
      "url" : "http://t.co/JbHEjCov",
      "expanded_url" : "http://bit.ly/P3H8ZG",
      "display_url" : "bit.ly/P3H8ZG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236842452454084609",
  "text" : "Roundabout Plugin http://t.co/JbHEjCov #jquery #awesome",
  "id" : 236842452454084609,
  "created_at" : "Sat Aug 18 15:10:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Irish",
      "screen_name" : "paul_irish",
      "indices" : [ 0, 11 ],
      "id_str" : "1671811",
      "id" : 1671811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 47 ],
      "url" : "https://t.co/WSp5qD5K",
      "expanded_url" : "https://twitter.com/#!/_higg_/developers-we-admire",
      "display_url" : "twitter.com/#!/_higg_/deve\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236840264050802688",
  "in_reply_to_user_id" : 1671811,
  "text" : "@paul_irish Holler Gurrl: https://t.co/WSp5qD5K Thanks for this list! This is handy for checking up on what's fresh ;) +1",
  "id" : 236840264050802688,
  "created_at" : "Sat Aug 18 15:01:33 +0000 2012",
  "in_reply_to_screen_name" : "paul_irish",
  "in_reply_to_user_id_str" : "1671811",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "new",
      "indices" : [ 66, 70 ]
    }, {
      "text" : "social",
      "indices" : [ 71, 78 ]
    }, {
      "text" : "newtork",
      "indices" : [ 79, 87 ]
    }, {
      "text" : "appnet",
      "indices" : [ 88, 95 ]
    }, {
      "text" : "alternative",
      "indices" : [ 96, 108 ]
    } ],
    "urls" : [ {
      "indices" : [ 45, 65 ],
      "url" : "http://t.co/c8ZXh2gc",
      "expanded_url" : "http://bit.ly/RooqBT",
      "display_url" : "bit.ly/RooqBT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236837379942526976",
  "text" : "Elgg - Open Source Social Networking Engine. http://t.co/c8ZXh2gc #new #social #newtork #appnet #alternative",
  "id" : 236837379942526976,
  "created_at" : "Sat Aug 18 14:50:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jsfiddle",
      "indices" : [ 48, 57 ]
    }, {
      "text" : "login",
      "indices" : [ 58, 64 ]
    }, {
      "text" : "form",
      "indices" : [ 65, 70 ]
    }, {
      "text" : "css3",
      "indices" : [ 71, 76 ]
    } ],
    "urls" : [ {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/CFdq44Sk",
      "expanded_url" : "http://bit.ly/RonXQ8",
      "display_url" : "bit.ly/RonXQ8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236832380260847616",
  "text" : "Dark Login Form - jsFiddle http://t.co/CFdq44Sk #jsfiddle #login #form #css3",
  "id" : 236832380260847616,
  "created_at" : "Sat Aug 18 14:30:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jsfiddle",
      "indices" : [ 65, 74 ]
    }, {
      "text" : "css",
      "indices" : [ 75, 79 ]
    }, {
      "text" : "minifier",
      "indices" : [ 80, 89 ]
    }, {
      "text" : "css3",
      "indices" : [ 90, 95 ]
    } ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/e2c7TYiN",
      "expanded_url" : "http://bit.ly/RonPjF",
      "display_url" : "bit.ly/RonPjF"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236827317006106624",
  "text" : "CSS Minifier \u03B2 - jsFiddle demo by ethertank http://t.co/e2c7TYiN #jsfiddle #css #minifier #css3",
  "id" : 236827317006106624,
  "created_at" : "Sat Aug 18 14:10:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Blackhat",
      "indices" : [ 44, 53 ]
    }, {
      "text" : "hacking",
      "indices" : [ 60, 68 ]
    }, {
      "text" : "presentations",
      "indices" : [ 69, 83 ]
    }, {
      "text" : "download",
      "indices" : [ 84, 93 ]
    } ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/E9C2rmno",
      "expanded_url" : "http://bit.ly/RonAFf",
      "display_url" : "bit.ly/RonAFf"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236822287683289088",
  "text" : "Index of blackhat-2008 http://t.co/E9C2rmno #Blackhat #2008 #hacking #presentations #download",
  "id" : 236822287683289088,
  "created_at" : "Sat Aug 18 13:50:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ASCII",
      "indices" : [ 47, 53 ]
    }, {
      "text" : "unicode",
      "indices" : [ 54, 62 ]
    }, {
      "text" : "demo",
      "indices" : [ 63, 68 ]
    }, {
      "text" : "experimentalists",
      "indices" : [ 69, 86 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/qXYjYFI3",
      "expanded_url" : "http://bit.ly/P3G1cf",
      "display_url" : "bit.ly/P3G1cf"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236817264790355969",
  "text" : "ascii art museum http://t.co/qXYjYFI3 New demo #ASCII #unicode #demo #experimentalists",
  "id" : 236817264790355969,
  "created_at" : "Sat Aug 18 13:30:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 81, 88 ]
    }, {
      "text" : "plugin",
      "indices" : [ 89, 96 ]
    } ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http://t.co/oScCSoKi",
      "expanded_url" : "http://bit.ly/Ronq0C",
      "display_url" : "bit.ly/Ronq0C"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236812217855664128",
  "text" : "Supersized - Full Screen Background Slideshow jQuery Plugin http://t.co/oScCSoKi #jquery #plugin",
  "id" : 236812217855664128,
  "created_at" : "Sat Aug 18 13:10:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "free",
      "indices" : [ 74, 79 ]
    }, {
      "text" : "typeface",
      "indices" : [ 80, 89 ]
    }, {
      "text" : "typography",
      "indices" : [ 96, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/tbZEXWSL",
      "expanded_url" : "http://bit.ly/RonhKD",
      "display_url" : "bit.ly/RonhKD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236807187400376321",
  "text" : "TypeFront \u00BB Sign up http://t.co/tbZEXWSL Host one font of your liking for #free #typeface fonts #typography",
  "id" : 236807187400376321,
  "created_at" : "Sat Aug 18 12:50:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "codecanyon",
      "indices" : [ 38, 49 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/MNuvF6pG",
      "expanded_url" : "http://bit.ly/Ron9uo",
      "display_url" : "bit.ly/Ron9uo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236802280760934401",
  "text" : "attentionGrabber http://t.co/MNuvF6pG #codecanyon",
  "id" : 236802280760934401,
  "created_at" : "Sat Aug 18 12:30:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/7urQNSbc",
      "expanded_url" : "http://pinterest.com/pin/115967759125638649/",
      "display_url" : "pinterest.com/pin/1159677591\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236610273656573953",
  "text" : "Current status: http://t.co/7urQNSbc Excuse the lack of enthusiastic tweets.",
  "id" : 236610273656573953,
  "created_at" : "Fri Aug 17 23:47:39 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stefan D\u00FChring",
      "screen_name" : "Autarc",
      "indices" : [ 82, 89 ],
      "id_str" : "52120068",
      "id" : 52120068
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/ypp3t2ng",
      "expanded_url" : "http://post.ly/8ryvU",
      "display_url" : "post.ly/8ryvU"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236563318456856577",
  "text" : "Favorited \u2605 http://t.co/ypp3t2ng svg-edit - a rich SVG editor in your browser via @autarc",
  "id" : 236563318456856577,
  "created_at" : "Fri Aug 17 20:41:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://www.twylah.com\" rel=\"nofollow\">Twylah</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 120, 140 ],
      "url" : "http://t.co/e7R1wykM",
      "expanded_url" : "http://twy.la/OnkWL8",
      "display_url" : "twy.la/OnkWL8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236560272930791426",
  "text" : "2012 spells a new era, a new paradigm for us, as web developers to shine, to innovate, and to inspire others. Here's o\u2026 http://t.co/e7R1wykM",
  "id" : 236560272930791426,
  "created_at" : "Fri Aug 17 20:28:58 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stefan D\u00FChring",
      "screen_name" : "Autarc",
      "indices" : [ 35, 42 ],
      "id_str" : "52120068",
      "id" : 52120068
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/MLDNUnLP",
      "expanded_url" : "http://post.ly/8rxQO",
      "display_url" : "post.ly/8rxQO"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236559226791669760",
  "text" : "Favorited http://t.co/MLDNUnLP via @autarc",
  "id" : 236559226791669760,
  "created_at" : "Fri Aug 17 20:24:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "236547662202155008",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Respond to my friend request, mon frere. I am Dublin Dance Classics, FYI David Higgins === Dublin Dance Classics. Long time no IM!",
  "id" : 236547662202155008,
  "created_at" : "Fri Aug 17 19:38:52 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "236531609438937088",
  "text" : "The joys of stealth vandalizing QR code stickers on lamp-posts in Dublin with a black permanent marker. My single black square ruins you!",
  "id" : 236531609438937088,
  "created_at" : "Fri Aug 17 18:35:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anil Dash",
      "screen_name" : "anildash",
      "indices" : [ 0, 9 ],
      "id_str" : "36823",
      "id" : 36823
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "confusing",
      "indices" : [ 115, 125 ]
    }, {
      "text" : "ui",
      "indices" : [ 126, 129 ]
    }, {
      "text" : "ux",
      "indices" : [ 132, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "236528042946744322",
  "in_reply_to_user_id" : 36823,
  "text" : "@anildash Why are all the links on your blog, and Twitter purple? Surely I didn't look at all those links of yours #confusing #ui / #ux",
  "id" : 236528042946744322,
  "created_at" : "Fri Aug 17 18:20:54 +0000 2012",
  "in_reply_to_screen_name" : "anildash",
  "in_reply_to_user_id_str" : "36823",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Support",
      "screen_name" : "Support",
      "indices" : [ 0, 8 ],
      "id_str" : "17874544",
      "id" : 17874544
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "teamfollowback",
      "indices" : [ 34, 49 ]
    }, {
      "text" : "140help",
      "indices" : [ 57, 65 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "236509566697885696",
  "geo" : {
  },
  "id_str" : "236527093201137664",
  "in_reply_to_user_id" : 17874544,
  "text" : "@Support Don't start off with the #teamfollowback crowd. #140help",
  "id" : 236527093201137664,
  "in_reply_to_status_id" : 236509566697885696,
  "created_at" : "Fri Aug 17 18:17:08 +0000 2012",
  "in_reply_to_screen_name" : "Support",
  "in_reply_to_user_id_str" : "17874544",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "downloading",
      "indices" : [ 128, 140 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "236525580936413184",
  "geo" : {
  },
  "id_str" : "236526512696852480",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog Download Folder Zero\u2122 is the new Inbox Zero ;) I have never had an empty download folder since I got broadband. #downloading",
  "id" : 236526512696852480,
  "in_reply_to_status_id" : 236525580936413184,
  "created_at" : "Fri Aug 17 18:14:49 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Scott Jehl",
      "screen_name" : "scottjehl",
      "indices" : [ 0, 10 ],
      "id_str" : "237918766",
      "id" : 237918766
    }, {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 44, 52 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "236521385051189248",
  "geo" : {
  },
  "id_str" : "236524135495376896",
  "in_reply_to_user_id" : 13567,
  "text" : "@scottjehl It is a good service. I remember @codepo8 commenting the fact you'd have x amount of Japanese sources watching your video. :(",
  "id" : 236524135495376896,
  "in_reply_to_status_id" : 236521385051189248,
  "created_at" : "Fri Aug 17 18:05:22 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Scott Jehl",
      "screen_name" : "scottjehl",
      "indices" : [ 9, 19 ],
      "id_str" : "237918766",
      "id" : 237918766
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 103 ],
      "url" : "http://t.co/WOAdu03H",
      "expanded_url" : "http://i.higg.in/video/toolset.projects.mp4",
      "display_url" : "i.higg.in/video/toolset.\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "236498995621531648",
  "geo" : {
  },
  "id_str" : "236521164661473280",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 @scottjehl I upload my screencasts to a CDN service. As demonstrated here http://t.co/WOAdu03H I hand-encoded the different formats",
  "id" : 236521164661473280,
  "in_reply_to_status_id" : 236498995621531648,
  "created_at" : "Fri Aug 17 17:53:34 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    }, {
      "name" : "Scott Jehl",
      "screen_name" : "scottjehl",
      "indices" : [ 9, 19 ],
      "id_str" : "237918766",
      "id" : 237918766
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 89 ],
      "url" : "http://t.co/G1pUyKSf",
      "expanded_url" : "http://encoding.com",
      "display_url" : "encoding.com"
    } ]
  },
  "in_reply_to_status_id_str" : "236498995621531648",
  "geo" : {
  },
  "id_str" : "236520723798179841",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 @scottjehl vid.ly can be expensive. They're a subsidiary of http://t.co/G1pUyKSf Thus \u2192 expensive.",
  "id" : 236520723798179841,
  "in_reply_to_status_id" : 236498995621531648,
  "created_at" : "Fri Aug 17 17:51:49 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "appnet",
      "indices" : [ 111, 118 ]
    } ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/pjZOjVHm",
      "expanded_url" : "http://ihave50dollars.com/",
      "display_url" : "ihave50dollars.com"
    }, {
      "indices" : [ 89, 110 ],
      "url" : "https://t.co/GHOXGqz7",
      "expanded_url" : "https://alpha.app.net/dh",
      "display_url" : "alpha.app.net/dh"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236519707312783360",
  "text" : "http://t.co/pjZOjVHm \u2192 I backed, knowing the price will go down substantially next year. https://t.co/GHOXGqz7 #appnet",
  "id" : 236519707312783360,
  "created_at" : "Fri Aug 17 17:47:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u27A8 Kelly Kim",
      "screen_name" : "Twylah",
      "indices" : [ 27, 34 ],
      "id_str" : "122984888",
      "id" : 122984888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http://t.co/0CllcFKJ",
      "expanded_url" : "http://t.higg.in/",
      "display_url" : "t.higg.in"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236516216636841984",
  "text" : "As promised - Here you go. @twylah subdomain now setup! http://t.co/0CllcFKJ",
  "id" : 236516216636841984,
  "created_at" : "Fri Aug 17 17:33:54 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u27A8 Kelly Kim",
      "screen_name" : "Twylah",
      "indices" : [ 18, 25 ],
      "id_str" : "122984888",
      "id" : 122984888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 136 ],
      "url" : "http://t.co/VEA6GjfC",
      "expanded_url" : "http://www.twylah.com/_higg_/",
      "display_url" : "twylah.com/_higg_/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236513391697600512",
  "text" : "Just got notified @Twylah now has the option for custom domains! I'll let you know when I've set it up. Meanwhile - http://t.co/VEA6GjfC",
  "id" : 236513391697600512,
  "created_at" : "Fri Aug 17 17:22:41 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gustavo Fischer",
      "screen_name" : "Gusfischer",
      "indices" : [ 0, 11 ],
      "id_str" : "3957911",
      "id" : 3957911
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/jYBHPH98",
      "expanded_url" : "http://davidhiggins.me/museum",
      "display_url" : "davidhiggins.me/museum"
    } ]
  },
  "in_reply_to_status_id_str" : "236474915807711232",
  "geo" : {
  },
  "id_str" : "236480539043971072",
  "in_reply_to_user_id" : 3957911,
  "text" : "@Gusfischer http://t.co/jYBHPH98 Latest demo",
  "id" : 236480539043971072,
  "in_reply_to_status_id" : 236474915807711232,
  "created_at" : "Fri Aug 17 15:12:08 +0000 2012",
  "in_reply_to_screen_name" : "Gusfischer",
  "in_reply_to_user_id_str" : "3957911",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 0, 15 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "236459441434804224",
  "geo" : {
  },
  "id_str" : "236474190000177152",
  "in_reply_to_user_id" : 15759583,
  "text" : "@davidwalshblog Put a rule in Adblock Plus to kill the script. Cookie policy notifications are getting as invasive as cookies themselves",
  "id" : 236474190000177152,
  "in_reply_to_status_id" : 236459441434804224,
  "created_at" : "Fri Aug 17 14:46:55 +0000 2012",
  "in_reply_to_screen_name" : "davidwalshblog",
  "in_reply_to_user_id_str" : "15759583",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "236469579256299520",
  "geo" : {
  },
  "id_str" : "236471265697882113",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Comments about not using onload=\"init()\" are redundant because generated source (Not view source) breaks all rules regarding inline",
  "id" : 236471265697882113,
  "in_reply_to_status_id" : 236469579256299520,
  "created_at" : "Fri Aug 17 14:35:17 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "236469579256299520",
  "geo" : {
  },
  "id_str" : "236470968627908610",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Generated source spits out loads of onmouseover=\"foo()\" as attributes on elements. Not so evil as you would imagine.",
  "id" : 236470968627908610,
  "in_reply_to_status_id" : 236469579256299520,
  "created_at" : "Fri Aug 17 14:34:06 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 38 ],
      "url" : "https://t.co/HeIKnpUv",
      "expanded_url" : "https://gist.github.com/2252648",
      "display_url" : "gist.github.com/2252648"
    } ]
  },
  "in_reply_to_status_id_str" : "236191870752010240",
  "geo" : {
  },
  "id_str" : "236206293994569728",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii https://t.co/HeIKnpUv",
  "id" : 236206293994569728,
  "in_reply_to_status_id" : 236191870752010240,
  "created_at" : "Thu Aug 16 21:02:23 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/DyJVQ7ss",
      "expanded_url" : "http://laterstars.com/_higg_",
      "display_url" : "laterstars.com/_higg_"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236195213851369474",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii http://t.co/DyJVQ7ss",
  "id" : 236195213851369474,
  "created_at" : "Thu Aug 16 20:18:21 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "vpn",
      "indices" : [ 98, 102 ]
    }, {
      "text" : "anonymous",
      "indices" : [ 103, 113 ]
    } ],
    "urls" : [ {
      "indices" : [ 77, 97 ],
      "url" : "http://t.co/a6ppoFQm",
      "expanded_url" : "http://bit.ly/MYmX3q",
      "display_url" : "bit.ly/MYmX3q"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236117673350660096",
  "text" : "OkayFreedom \u2192 A simple VPN service enabling private, uncensored web surfing. http://t.co/a6ppoFQm #vpn #anonymous",
  "id" : 236117673350660096,
  "created_at" : "Thu Aug 16 15:10:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ultracool",
      "indices" : [ 115, 125 ]
    }, {
      "text" : "editor",
      "indices" : [ 126, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/zs59peAN",
      "expanded_url" : "http://bit.ly/MYhppB",
      "display_url" : "bit.ly/MYhppB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236112599899594752",
  "text" : "Texts \u2192 Content editor for Mac OS X and Windows http://t.co/zs59peAN Probably the coolest program ever. Seriously! #ultracool #editor",
  "id" : 236112599899594752,
  "created_at" : "Thu Aug 16 14:50:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/6yCeKUPM",
      "expanded_url" : "http://bit.ly/PnLfQz",
      "display_url" : "bit.ly/PnLfQz"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236107658959134720",
  "text" : "Entypo - 100+ carefully crafted pictograms http://t.co/6yCeKUPM",
  "id" : 236107658959134720,
  "created_at" : "Thu Aug 16 14:30:27 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rss",
      "indices" : [ 80, 84 ]
    }, {
      "text" : "reader",
      "indices" : [ 85, 92 ]
    }, {
      "text" : "feeds",
      "indices" : [ 93, 99 ]
    } ],
    "urls" : [ {
      "indices" : [ 59, 79 ],
      "url" : "http://t.co/zztz1Ftp",
      "expanded_url" : "http://bit.ly/PnLgEl",
      "display_url" : "bit.ly/PnLgEl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236102557511069697",
  "text" : "\"rsslounge aggregator is a free web based rss feed reader\" http://t.co/zztz1Ftp #rss #reader #feeds",
  "id" : 236102557511069697,
  "created_at" : "Thu Aug 16 14:10:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "youtube",
      "indices" : [ 47, 55 ]
    } ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/UsbpuvDg",
      "expanded_url" : "http://bit.ly/MYh9qH",
      "display_url" : "bit.ly/MYh9qH"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236097508328230913",
  "text" : "Structural Trip - YouTube http://t.co/UsbpuvDg #youtube",
  "id" : 236097508328230913,
  "created_at" : "Thu Aug 16 13:50:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 133, 140 ]
    } ],
    "urls" : [ {
      "indices" : [ 112, 132 ],
      "url" : "http://t.co/HZCOKxB9",
      "expanded_url" : "http://bit.ly/PnL4F8",
      "display_url" : "bit.ly/PnL4F8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236092525209862145",
  "text" : "TextExt is a plugin for jQuery which is designed to provide functionality such as tag input and autocomplete. \u2192 http://t.co/HZCOKxB9 #jquery",
  "id" : 236092525209862145,
  "created_at" : "Thu Aug 16 13:30:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sharing",
      "indices" : [ 50, 58 ]
    }, {
      "text" : "github",
      "indices" : [ 59, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/EfkQpGzL",
      "expanded_url" : "http://bit.ly/PnL0VI",
      "display_url" : "bit.ly/PnL0VI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236087441763749888",
  "text" : "\"Social Share URL Generator\" http://t.co/EfkQpGzL #sharing #github",
  "id" : 236087441763749888,
  "created_at" : "Thu Aug 16 13:10:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "nice",
      "indices" : [ 32, 37 ]
    }, {
      "text" : "css3",
      "indices" : [ 38, 43 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/vopq3pic",
      "expanded_url" : "http://bit.ly/PnKUNY",
      "display_url" : "bit.ly/PnKUNY"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236082406526943233",
  "text" : "CSS Ribbon http://t.co/vopq3pic #nice #css3",
  "id" : 236082406526943233,
  "created_at" : "Thu Aug 16 12:50:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css3",
      "indices" : [ 32, 37 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/znHXfHay",
      "expanded_url" : "http://bit.ly/MYgLIU",
      "display_url" : "bit.ly/MYgLIU"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236077581630324737",
  "text" : "CSS3 Watch http://t.co/znHXfHay #css3",
  "id" : 236077581630324737,
  "created_at" : "Thu Aug 16 12:30:56 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Phil Ripperger",
      "screen_name" : "pdsphil",
      "indices" : [ 0, 8 ],
      "id_str" : "641213",
      "id" : 641213
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "235932820726489088",
  "geo" : {
  },
  "id_str" : "236050765129601024",
  "in_reply_to_user_id" : 641213,
  "text" : "@pdsphil Fantastic. Thanks ;)",
  "id" : 236050765129601024,
  "in_reply_to_status_id" : 235932820726489088,
  "created_at" : "Thu Aug 16 10:44:22 +0000 2012",
  "in_reply_to_screen_name" : "pdsphil",
  "in_reply_to_user_id_str" : "641213",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "codecanyon",
      "indices" : [ 15, 26 ]
    } ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/BlG854jm",
      "expanded_url" : "http://codecanyon.net/item/the-love-button/2835724",
      "display_url" : "codecanyon.net/item/the-love-\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "236049516497874944",
  "text" : "Just made this #codecanyon item. http://t.co/BlG854jm The Love Button.",
  "id" : 236049516497874944,
  "created_at" : "Thu Aug 16 10:39:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "funny",
      "indices" : [ 31, 37 ]
    } ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/C8VOWUPu",
      "expanded_url" : "http://on.fb.me/PnKQxG",
      "display_url" : "on.fb.me/PnKQxG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235810632585322497",
  "text" : "Saucesome http://t.co/C8VOWUPu #funny",
  "id" : 235810632585322497,
  "created_at" : "Wed Aug 15 18:50:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css3",
      "indices" : [ 45, 50 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/v52BoXNO",
      "expanded_url" : "http://bit.ly/MYgvtf",
      "display_url" : "bit.ly/MYgvtf"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235805617460219904",
  "text" : "CSS3 Facebook Buttons \u2192 http://t.co/v52BoXNO #css3",
  "id" : 235805617460219904,
  "created_at" : "Wed Aug 15 18:30:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/W5tR0zWv",
      "expanded_url" : "http://bit.ly/MYguFC",
      "display_url" : "bit.ly/MYguFC"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235800546555076609",
  "text" : "pichaus \u2192 shared pictures! http://t.co/W5tR0zWv",
  "id" : 235800546555076609,
  "created_at" : "Wed Aug 15 18:10:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/b1O8e4GW",
      "expanded_url" : "http://bit.ly/MYgju8",
      "display_url" : "bit.ly/MYgju8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235795725978595328",
  "text" : "Quickly compress PNG images - Neil Turner's Blog http://t.co/b1O8e4GW",
  "id" : 235795725978595328,
  "created_at" : "Wed Aug 15 17:50:56 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "l33tsp33k",
      "indices" : [ 88, 98 ]
    }, {
      "text" : "l33t",
      "indices" : [ 99, 104 ]
    } ],
    "urls" : [ {
      "indices" : [ 67, 87 ],
      "url" : "http://t.co/uBZUBad8",
      "expanded_url" : "http://bit.ly/PnKE1x",
      "display_url" : "bit.ly/PnKE1x"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235790636719144961",
  "text" : "Leet - 140byt.es Converts a latin character string into leetspeak. http://t.co/uBZUBad8 #l33tsp33k #l33t",
  "id" : 235790636719144961,
  "created_at" : "Wed Aug 15 17:30:43 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css3",
      "indices" : [ 56, 61 ]
    } ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/tPY7APsc",
      "expanded_url" : "http://bit.ly/PnKwPo",
      "display_url" : "bit.ly/PnKwPo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235785456539803648",
  "text" : "OMG!!! Rainbow Dividers in CSS3!!! http://t.co/tPY7APsc #css3",
  "id" : 235785456539803648,
  "created_at" : "Wed Aug 15 17:10:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "github",
      "indices" : [ 94, 101 ]
    }, {
      "text" : "js",
      "indices" : [ 102, 105 ]
    }, {
      "text" : "javascript",
      "indices" : [ 106, 117 ]
    }, {
      "text" : "thomasfuchs",
      "indices" : [ 118, 130 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 93 ],
      "url" : "http://t.co/Z3v61yIk",
      "expanded_url" : "http://bit.ly/PnKveo",
      "display_url" : "bit.ly/PnKveo"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235780421298835456",
  "text" : "\"A simple micro-library for defining and dispatching keyboard shortcuts\" http://t.co/Z3v61yIk #github #js #javascript #thomasfuchs",
  "id" : 235780421298835456,
  "created_at" : "Wed Aug 15 16:50:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "USD",
      "indices" : [ 109, 113 ]
    }, {
      "text" : "EUR",
      "indices" : [ 114, 118 ]
    } ],
    "urls" : [ {
      "indices" : [ 88, 108 ],
      "url" : "http://t.co/IWHTzYEV",
      "expanded_url" : "http://bit.ly/PnKdnS",
      "display_url" : "bit.ly/PnKdnS"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235775374536101889",
  "text" : "\"Deutsche Bank, I know Unicode is hard and all, but you are not inspiring confidence: \" http://t.co/IWHTzYEV #USD #EUR",
  "id" : 235775374536101889,
  "created_at" : "Wed Aug 15 16:30:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "demo",
      "indices" : [ 58, 63 ]
    } ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http://t.co/6ghvDXGJ",
      "expanded_url" : "http://bit.ly/PnK9o6",
      "display_url" : "bit.ly/PnK9o6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235770344126939138",
  "text" : "Rainbow Text Highlighting with CSS \u2192 http://t.co/6ghvDXGJ #demo Try highlighting the text!",
  "id" : 235770344126939138,
  "created_at" : "Wed Aug 15 16:10:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fonts",
      "indices" : [ 84, 90 ]
    } ],
    "urls" : [ {
      "indices" : [ 63, 83 ],
      "url" : "http://t.co/9BV3v2BA",
      "expanded_url" : "http://bit.ly/PnK5Ve",
      "display_url" : "bit.ly/PnK5Ve"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235765357586046977",
  "text" : "WhatFont Tool - The easiest way to inspect fonts in webpages \u2192 http://t.co/9BV3v2BA #fonts",
  "id" : 235765357586046977,
  "created_at" : "Wed Aug 15 15:50:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "aerotwist",
      "indices" : [ 43, 53 ]
    }, {
      "text" : "demo",
      "indices" : [ 54, 59 ]
    }, {
      "text" : "css3d",
      "indices" : [ 60, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 22, 42 ],
      "url" : "http://t.co/LMf7Cysu",
      "expanded_url" : "http://bit.ly/MYftxp",
      "display_url" : "bit.ly/MYftxp"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235760276291584001",
  "text" : "Spikes | CSS 3D Shape http://t.co/LMf7Cysu #aerotwist #demo #css3d",
  "id" : 235760276291584001,
  "created_at" : "Wed Aug 15 15:30:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hacktheplanet",
      "indices" : [ 31, 45 ]
    }, {
      "text" : "hack",
      "indices" : [ 46, 51 ]
    } ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/0Kgb2dJc",
      "expanded_url" : "http://bit.ly/MYfem3",
      "display_url" : "bit.ly/MYfem3"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235755291612958722",
  "text" : "Firesheep http://t.co/0Kgb2dJc #hacktheplanet #hack",
  "id" : 235755291612958722,
  "created_at" : "Wed Aug 15 15:10:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html5",
      "indices" : [ 119, 125 ]
    }, {
      "text" : "canvas",
      "indices" : [ 126, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 98, 118 ],
      "url" : "http://t.co/1ZsAvP4v",
      "expanded_url" : "http://bit.ly/MYfcL1#js",
      "display_url" : "bit.ly/MYfcL1#js"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235750246783782912",
  "text" : "vintageJS - add some awesome retro and vintage style to your images with the HTML5 canvas element http://t.co/1ZsAvP4v #html5 #canvas",
  "id" : 235750246783782912,
  "created_at" : "Wed Aug 15 14:50:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 57, 68 ]
    }, {
      "text" : "texttospeech",
      "indices" : [ 69, 82 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/jzYfwBwi",
      "expanded_url" : "http://bit.ly/MYf7qI",
      "display_url" : "bit.ly/MYf7qI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235745250038124544",
  "text" : "speak.js: Text-to-Speech on the Web http://t.co/jzYfwBwi #javascript #texttospeech",
  "id" : 235745250038124544,
  "created_at" : "Wed Aug 15 14:30:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/VgUOa7R2",
      "expanded_url" : "http://bit.ly/MYf3ag",
      "display_url" : "bit.ly/MYf3ag"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235740228386709504",
  "text" : "Fran\u00E7ois Robichet - Front-End Web Developer http://t.co/VgUOa7R2",
  "id" : 235740228386709504,
  "created_at" : "Wed Aug 15 14:10:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "js",
      "indices" : [ 77, 80 ]
    } ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http://t.co/AijkQKvM",
      "expanded_url" : "http://bit.ly/JgyDdV",
      "display_url" : "bit.ly/JgyDdV"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235735114737385473",
  "text" : "\"js MessageCustom notifications, alerts, confirmations\" http://t.co/AijkQKvM #js",
  "id" : 235735114737385473,
  "created_at" : "Wed Aug 15 13:50:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 72, 76 ]
    } ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http://t.co/o7CpcWia",
      "expanded_url" : "http://bit.ly/PnJweb",
      "display_url" : "bit.ly/PnJweb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235730258442207232",
  "text" : "\"Peculiar is a free icon package made only in CSS\" http://t.co/o7CpcWia #css",
  "id" : 235730258442207232,
  "created_at" : "Wed Aug 15 13:30:47 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jsperf",
      "indices" : [ 45, 52 ]
    }, {
      "text" : "js",
      "indices" : [ 53, 56 ]
    }, {
      "text" : "performance",
      "indices" : [ 57, 69 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/KfzaJ9Pi",
      "expanded_url" : "http://bit.ly/PnJu5V",
      "display_url" : "bit.ly/PnJu5V"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235725616463360001",
  "text" : "Mega trim test \u00B7 jsPerf http://t.co/KfzaJ9Pi #jsperf #js #performance",
  "id" : 235725616463360001,
  "created_at" : "Wed Aug 15 13:12:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RSS",
      "indices" : [ 76, 80 ]
    } ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/HfMFe3uB",
      "expanded_url" : "http://bit.ly/MQYrRS",
      "display_url" : "bit.ly/MQYrRS"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235720020796272643",
  "text" : "ChimpFeedr RSS Feed Aggregator http://t.co/HfMFe3uB Combine all the things! #RSS is not dead.",
  "id" : 235720020796272643,
  "created_at" : "Wed Aug 15 12:50:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 120 ],
      "url" : "http://t.co/vpOyMuWL",
      "expanded_url" : "http://bit.ly/MQmgt8",
      "display_url" : "bit.ly/MQmgt8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235715044195848192",
  "text" : "Picplz is a photo sharing app that makes it easy for you to share your mobile pictures on the Web \u2026 http://t.co/vpOyMuWL",
  "id" : 235715044195848192,
  "created_at" : "Wed Aug 15 12:30:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Roman David DeSilva",
      "screen_name" : "MetroHeads",
      "indices" : [ 3, 14 ],
      "id_str" : "296389103",
      "id" : 296389103
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 97 ],
      "url" : "http://t.co/33rCtdWV",
      "expanded_url" : "http://zd.net/R0pNnL",
      "display_url" : "zd.net/R0pNnL"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235534180900024320",
  "text" : "RT @MetroHeads: Microsoft's SkyDrive storage service gets a facelift | ZDNet http://t.co/33rCtdWV",
  "retweeted_status" : {
    "source" : "<a href=\"http://carbonwp7.com\" rel=\"nofollow\">Carbon for Windows Phone</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 81 ],
        "url" : "http://t.co/33rCtdWV",
        "expanded_url" : "http://zd.net/R0pNnL",
        "display_url" : "zd.net/R0pNnL"
      } ]
    },
    "geo" : {
    },
    "id_str" : "235533355016409088",
    "text" : "Microsoft's SkyDrive storage service gets a facelift | ZDNet http://t.co/33rCtdWV",
    "id" : 235533355016409088,
    "created_at" : "Wed Aug 15 00:28:22 +0000 2012",
    "user" : {
      "name" : "Roman David DeSilva",
      "screen_name" : "MetroHeads",
      "protected" : false,
      "id_str" : "296389103",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3607441668/9741b8e60230daad4958c50a1cf3bd08_normal.jpeg",
      "id" : 296389103,
      "verified" : false
    }
  },
  "id" : 235534180900024320,
  "created_at" : "Wed Aug 15 00:31:39 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fear",
      "indices" : [ 130, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235533020927496192",
  "text" : "The awful panic-struck moment when Google's 2 step verification SMS arrives randomly on your phone! And you didn't go near G-Mail #fear",
  "id" : 235533020927496192,
  "created_at" : "Wed Aug 15 00:27:02 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liv Madsen",
      "screen_name" : "livmadsen",
      "indices" : [ 0, 10 ],
      "id_str" : "119191210",
      "id" : 119191210
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "malware",
      "indices" : [ 33, 41 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 32 ],
      "url" : "https://t.co/j8bUBQEQ",
      "expanded_url" : "https://twitter.com/_higg_/status/235497110672400384",
      "display_url" : "twitter.com/_higg_/status/\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "235495169267466240",
  "geo" : {
  },
  "id_str" : "235497680338550784",
  "in_reply_to_user_id" : 119191210,
  "text" : "@livmadsen https://t.co/j8bUBQEQ #malware alert. Installs trojan horses that steal your data.",
  "id" : 235497680338550784,
  "in_reply_to_status_id" : 235495169267466240,
  "created_at" : "Tue Aug 14 22:06:36 +0000 2012",
  "in_reply_to_screen_name" : "livmadsen",
  "in_reply_to_user_id_str" : "119191210",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Munro",
      "screen_name" : "justinmunro",
      "indices" : [ 0, 12 ],
      "id_str" : "38914906",
      "id" : 38914906
    }, {
      "name" : "Alec Perkins",
      "screen_name" : "alecperkins",
      "indices" : [ 13, 25 ],
      "id_str" : "14848078",
      "id" : 14848078
    }, {
      "name" : "Lea Verou",
      "screen_name" : "LeaVerou",
      "indices" : [ 26, 35 ],
      "id_str" : "22199970",
      "id" : 22199970
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 57 ],
      "url" : "https://t.co/j8bUBQEQ",
      "expanded_url" : "https://twitter.com/_higg_/status/235497110672400384",
      "display_url" : "twitter.com/_higg_/status/\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "235495773293404161",
  "geo" : {
  },
  "id_str" : "235497550956855296",
  "in_reply_to_user_id" : 38914906,
  "text" : "@justinmunro @alecperkins @LeaVerou https://t.co/j8bUBQEQ",
  "id" : 235497550956855296,
  "in_reply_to_status_id" : 235495773293404161,
  "created_at" : "Tue Aug 14 22:06:06 +0000 2012",
  "in_reply_to_screen_name" : "justinmunro",
  "in_reply_to_user_id_str" : "38914906",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tim Harbour",
      "screen_name" : "timharbour",
      "indices" : [ 0, 11 ],
      "id_str" : "16577487",
      "id" : 16577487
    }, {
      "name" : "Visual Idiot",
      "screen_name" : "idiot",
      "indices" : [ 12, 18 ],
      "id_str" : "202571491",
      "id" : 202571491
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 40 ],
      "url" : "https://t.co/j8bUBQEQ",
      "expanded_url" : "https://twitter.com/_higg_/status/235497110672400384",
      "display_url" : "twitter.com/_higg_/status/\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "235496569145806848",
  "geo" : {
  },
  "id_str" : "235497521873563650",
  "in_reply_to_user_id" : 16577487,
  "text" : "@timharbour @idiot https://t.co/j8bUBQEQ",
  "id" : 235497521873563650,
  "in_reply_to_status_id" : 235496569145806848,
  "created_at" : "Tue Aug 14 22:05:59 +0000 2012",
  "in_reply_to_screen_name" : "timharbour",
  "in_reply_to_user_id_str" : "16577487",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alec Perkins",
      "screen_name" : "alecperkins",
      "indices" : [ 0, 12 ],
      "id_str" : "14848078",
      "id" : 14848078
    }, {
      "name" : "Lea Verou",
      "screen_name" : "LeaVerou",
      "indices" : [ 13, 22 ],
      "id_str" : "22199970",
      "id" : 22199970
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "constellation7",
      "indices" : [ 106, 121 ]
    }, {
      "text" : "malware",
      "indices" : [ 122, 130 ]
    } ],
    "urls" : [ {
      "indices" : [ 85, 105 ],
      "url" : "http://t.co/euGPRkc0",
      "expanded_url" : "http://isharefil.es/IlHD/Image%202012-08-14%20at%2011.01.16%20PM.png",
      "display_url" : "isharefil.es/IlHD/Image%202\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "235488530510540800",
  "geo" : {
  },
  "id_str" : "235497110672400384",
  "in_reply_to_user_id" : 14848078,
  "text" : "@alecperkins @LeaVerou I decompiled the code of heurot.jar Not good. Not good at all http://t.co/euGPRkc0 #constellation7 #malware",
  "id" : 235497110672400384,
  "in_reply_to_status_id" : 235488530510540800,
  "created_at" : "Tue Aug 14 22:04:21 +0000 2012",
  "in_reply_to_screen_name" : "alecperkins",
  "in_reply_to_user_id_str" : "14848078",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alec Perkins",
      "screen_name" : "alecperkins",
      "indices" : [ 0, 12 ],
      "id_str" : "14848078",
      "id" : 14848078
    }, {
      "name" : "Lea Verou",
      "screen_name" : "LeaVerou",
      "indices" : [ 13, 22 ],
      "id_str" : "22199970",
      "id" : 22199970
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 129 ],
      "url" : "http://t.co/zuyHRDmZ",
      "expanded_url" : "http://isharefil.es/IlHD",
      "display_url" : "isharefil.es/IlHD"
    } ]
  },
  "in_reply_to_status_id_str" : "235488530510540800",
  "geo" : {
  },
  "id_str" : "235496799903809537",
  "in_reply_to_user_id" : 14848078,
  "text" : "@alecperkins @LeaVerou The JAVA Applet that runs on this site installs malware that robs all your p4sswords. http://t.co/zuyHRDmZ",
  "id" : 235496799903809537,
  "in_reply_to_status_id" : 235488530510540800,
  "created_at" : "Tue Aug 14 22:03:07 +0000 2012",
  "in_reply_to_screen_name" : "alecperkins",
  "in_reply_to_user_id_str" : "14848078",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 55, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 119, 140 ],
      "url" : "https://t.co/ojRrK4UD",
      "expanded_url" : "https://plus.google.com/u/0/118252903045381283898/posts/c85tMa26xMm",
      "display_url" : "plus.google.com/u/0/1182529030\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235487871476318209",
  "text" : "So I check my Pinboard.in for hawt links, searched for #javascript on Twitter. Hmm, what else? Oh yeah, let's check G+ https://t.co/ojRrK4UD",
  "id" : 235487871476318209,
  "created_at" : "Tue Aug 14 21:27:38 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SpeedAwarenessMonth",
      "screen_name" : "SpeedMonth",
      "indices" : [ 83, 94 ],
      "id_str" : "632445769",
      "id" : 632445769
    }, {
      "name" : "Justin Dorfman",
      "screen_name" : "jdorfman",
      "indices" : [ 101, 110 ],
      "id_str" : "14139773",
      "id" : 14139773
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "protip",
      "indices" : [ 50, 57 ]
    }, {
      "text" : "netdna",
      "indices" : [ 111, 118 ]
    } ],
    "urls" : [ {
      "indices" : [ 58, 78 ],
      "url" : "http://t.co/uus0Nj8E",
      "expanded_url" : "http://higg.in/aq",
      "display_url" : "higg.in/aq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235478621454745600",
  "text" : "\"Three ways to save money on your bandwidth bill\" #protip http://t.co/uus0Nj8E via @speedmonth cc:// @jdorfman #netdna",
  "id" : 235478621454745600,
  "created_at" : "Tue Aug 14 20:50:52 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Wordpress",
      "indices" : [ 45, 55 ]
    }, {
      "text" : "Drupal",
      "indices" : [ 57, 64 ]
    }, {
      "text" : "Joomla",
      "indices" : [ 66, 73 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 104 ],
      "url" : "http://t.co/ELg6mMu0",
      "expanded_url" : "http://bit.ly/PeREh0",
      "display_url" : "bit.ly/PeREh0"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235448229129494528",
  "text" : "Instant Install - Quickly and easily install #Wordpress, #Drupal, #Joomla, and more http://t.co/ELg6mMu0",
  "id" : 235448229129494528,
  "created_at" : "Tue Aug 14 18:50:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/tpsLeViO",
      "expanded_url" : "http://bit.ly/MQlCM4",
      "display_url" : "bit.ly/MQlCM4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235443211177431040",
  "text" : "Perfect &lt;PRE&gt; Tags : Perishable Press http://t.co/tpsLeViO",
  "id" : 235443211177431040,
  "created_at" : "Tue Aug 14 18:30:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/NFRGYfhC",
      "expanded_url" : "http://bit.ly/MQlts7",
      "display_url" : "bit.ly/MQlts7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235438229120237568",
  "text" : "Stephen Brooks's Website \u00A0: :\u00A0 Unicode http://t.co/NFRGYfhC",
  "id" : 235438229120237568,
  "created_at" : "Tue Aug 14 18:10:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "antifuzzing",
      "indices" : [ 22, 34 ]
    }, {
      "text" : "xss",
      "indices" : [ 103, 107 ]
    }, {
      "text" : "fuzzing",
      "indices" : [ 108, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235435649665228800",
  "text" : "TIL - Some sites have #antifuzzing engines built into them. alert('1') executes, but alert(1) doesn't. #xss #fuzzing",
  "id" : 235435649665228800,
  "created_at" : "Tue Aug 14 18:00:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "funny",
      "indices" : [ 43, 49 ]
    } ],
    "urls" : [ {
      "indices" : [ 22, 42 ],
      "url" : "http://t.co/LhqHVWHM",
      "expanded_url" : "http://bit.ly/MQlufG",
      "display_url" : "bit.ly/MQlufG"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235433125981876225",
  "text" : "Mail::RFC822::Address http://t.co/LhqHVWHM #funny Regular Expression",
  "id" : 235433125981876225,
  "created_at" : "Tue Aug 14 17:50:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "pinterest",
      "indices" : [ 71, 81 ]
    } ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http://t.co/P8MvjUnc",
      "expanded_url" : "http://bit.ly/PeQzFU",
      "display_url" : "bit.ly/PeQzFU"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235428098559066112",
  "text" : "This is a utility belt for working with Pinterest http://t.co/P8MvjUnc #pinterest",
  "id" : 235428098559066112,
  "created_at" : "Tue Aug 14 17:30:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235425049128091648",
  "text" : "URL Shorteners (Part 3) yu\uFE12my | goo\uFE12gl | lnkd\uFE12in | x\uFE12vu | plus\uFE12ly | gplus\uFE12to | cl\uFE12ly | mlkshk\uFE12com | things\uFE12ly | gg\uFE12gg",
  "id" : 235425049128091648,
  "created_at" : "Tue Aug 14 17:18:00 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235424721271918592",
  "text" : "URL Shorteners (Part 2) mtro\uFE12us | zio\uFE12in | rso\uFE12lv | pho\uFE12to | su\uFE12pr | is\uFE12gd | sn\uFE12im",
  "id" : 235424721271918592,
  "created_at" : "Tue Aug 14 17:16:42 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235424481475190784",
  "text" : "URL Shorteners (Part 1) ow\uFE12ly | lnk\uFE12co | g\uFE12co | youtu\uFE12be | to\uFE12ly | hex\uFE12io | yi\uFE12tl | bit\uFE12do | v\uFE12gd | awe\uFE12sm | 3\uFE12ly",
  "id" : 235424481475190784,
  "created_at" : "Tue Aug 14 17:15:45 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 38 ],
      "url" : "http://t.co/eILeCeZl",
      "expanded_url" : "http://bit.ly/PeQaTZ",
      "display_url" : "bit.ly/PeQaTZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235423081114513408",
  "text" : "Top 20 laterstars http://t.co/eILeCeZl",
  "id" : 235423081114513408,
  "created_at" : "Tue Aug 14 17:10:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "productivity",
      "indices" : [ 61, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/mRybYdQw",
      "expanded_url" : "http://bit.ly/PeFzIy",
      "display_url" : "bit.ly/PeFzIy"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235418028995665920",
  "text" : "Tech &amp; Tools http://t.co/mRybYdQw Boatload of neat tools #productivity",
  "id" : 235418028995665920,
  "created_at" : "Tue Aug 14 16:50:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 42 ],
      "url" : "http://t.co/Sn9aeSm0",
      "expanded_url" : "http://bit.ly/PdR5nL",
      "display_url" : "bit.ly/PdR5nL"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235412994161532928",
  "text" : "\u2605 Frontend Linkdump \u2605 http://t.co/Sn9aeSm0",
  "id" : 235412994161532928,
  "created_at" : "Tue Aug 14 16:30:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "links",
      "indices" : [ 49, 55 ]
    } ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/PyzIHoR4",
      "expanded_url" : "http://bit.ly/InnsB4",
      "display_url" : "bit.ly/InnsB4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235408230321364993",
  "text" : "RSS Feed aggrgation service http://t.co/PyzIHoR4 #links to all the things !!!",
  "id" : 235408230321364993,
  "created_at" : "Tue Aug 14 16:11:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/caU9FzjL",
      "expanded_url" : "http://bit.ly/MPzVAs",
      "display_url" : "bit.ly/MPzVAs"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235402992415809537",
  "text" : "Animated jQuery progressbar | Script tutorials http://t.co/caU9FzjL",
  "id" : 235402992415809537,
  "created_at" : "Tue Aug 14 15:50:21 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 0, 4 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 31 ],
      "url" : "http://t.co/q834Ikok",
      "expanded_url" : "http://apple.com",
      "display_url" : "apple.com"
    }, {
      "indices" : [ 35, 55 ],
      "url" : "http://t.co/sFztDQ9g",
      "expanded_url" : "http://support.apple.com/kb/ht1318",
      "display_url" : "support.apple.com/kb/ht1318"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235401356347850753",
  "text" : "#xss in my http://t.co/q834Ikok ?? http://t.co/sFztDQ9g Seems like the list goes on and on. A reason to find more vulns it is!",
  "id" : 235401356347850753,
  "created_at" : "Tue Aug 14 15:43:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "aws",
      "indices" : [ 36, 40 ]
    }, {
      "text" : "download",
      "indices" : [ 41, 50 ]
    } ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/k8OrSfsc",
      "expanded_url" : "http://bit.ly/Je8RlB",
      "display_url" : "bit.ly/Je8RlB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235397915206631424",
  "text" : "Bucket listing http://t.co/k8OrSfsc #aws #download",
  "id" : 235397915206631424,
  "created_at" : "Tue Aug 14 15:30:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/SZmslCp0",
      "expanded_url" : "http://bit.ly/MPzGpd",
      "display_url" : "bit.ly/MPzGpd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235392902640050176",
  "text" : "CSS Crush \u2014 An extensible PHP based CSS preprocessor http://t.co/SZmslCp0",
  "id" : 235392902640050176,
  "created_at" : "Tue Aug 14 15:10:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 65 ],
      "url" : "http://t.co/q4MRqVPE",
      "expanded_url" : "http://bit.ly/PdQySK",
      "display_url" : "bit.ly/PdQySK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235387824445206528",
  "text" : "effingallery | webdev | html5 | openweb |dev http://t.co/q4MRqVPE",
  "id" : 235387824445206528,
  "created_at" : "Tue Aug 14 14:50:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/VQ7jaV4g",
      "expanded_url" : "http://bit.ly/PdQwKC",
      "display_url" : "bit.ly/PdQwKC"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235382786654085120",
  "text" : "ObfuscateJS - A Free Javascript Obfuscator http://t.co/VQ7jaV4g",
  "id" : 235382786654085120,
  "created_at" : "Tue Aug 14 14:30:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/IHwNhFtb",
      "expanded_url" : "http://bit.ly/LeAbYI",
      "display_url" : "bit.ly/LeAbYI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235377751241158657",
  "text" : "Let's Make The Web a More Beautiful Place http://t.co/IHwNhFtb",
  "id" : 235377751241158657,
  "created_at" : "Tue Aug 14 14:10:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/IKTOS8xm",
      "expanded_url" : "http://bit.ly/PdQr9M",
      "display_url" : "bit.ly/PdQr9M"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235372717744992256",
  "text" : "slang.js http://t.co/IKTOS8xm",
  "id" : 235372717744992256,
  "created_at" : "Tue Aug 14 13:50:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "poem",
      "indices" : [ 81, 86 ]
    } ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http://t.co/1cEwYpUh",
      "expanded_url" : "http://bit.ly/PdQjHn",
      "display_url" : "bit.ly/PdQjHn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235367818894397440",
  "text" : "\"I saw the best minds of my generation destroyed by tumblr\" http://t.co/1cEwYpUh #poem",
  "id" : 235367818894397440,
  "created_at" : "Tue Aug 14 13:30:35 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 69, 73 ]
    } ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/i1SLqDSB",
      "expanded_url" : "http://bit.ly/PdPs9n",
      "display_url" : "bit.ly/PdPs9n"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235362841182494720",
  "text" : "Sort CSS Properties In Specific Order \u2014 CSScomb http://t.co/i1SLqDSB #css",
  "id" : 235362841182494720,
  "created_at" : "Tue Aug 14 13:10:48 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "parallax",
      "indices" : [ 77, 86 ]
    }, {
      "text" : "scrolling",
      "indices" : [ 87, 97 ]
    } ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/vlXbLPHE",
      "expanded_url" : "http://bit.ly/PdPgXN",
      "display_url" : "bit.ly/PdPgXN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235357656989184001",
  "text" : "ACTIVATE http://t.co/vlXbLPHE Parallax scrolling sites are sooo last 2 days. #parallax #scrolling",
  "id" : 235357656989184001,
  "created_at" : "Tue Aug 14 12:50:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/u33y7CHy",
      "expanded_url" : "http://bit.ly/PdP76A",
      "display_url" : "bit.ly/PdP76A"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235352604601888768",
  "text" : "Awesome And Customizable Gauges \u2014 justGage http://t.co/u33y7CHy",
  "id" : 235352604601888768,
  "created_at" : "Tue Aug 14 12:30:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235124259947696128",
  "text" : "\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617\u060F\u0610\u0611\u0612\u0613\u0614\u0615\u0616\u0617",
  "id" : 235124259947696128,
  "created_at" : "Mon Aug 13 21:22:46 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 85 ],
      "url" : "http://t.co/bORHSH8W",
      "expanded_url" : "http://panda-meat.com/",
      "display_url" : "panda-meat.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235124001402404864",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys Actually, I stand corrected. The domain has a hypen in it... http://t.co/bORHSH8W Click \"Where to buy\" and get epically trolled ;)",
  "id" : 235124001402404864,
  "created_at" : "Mon Aug 13 21:21:44 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 4, 24 ],
      "url" : "http://t.co/OhWLbQ0h",
      "expanded_url" : "http://Pandameat.com",
      "display_url" : "Pandameat.com"
    }, {
      "indices" : [ 119, 139 ],
      "url" : "http://t.co/jTolQ2iW",
      "expanded_url" : "http://www.quora.com/Is-panda-meat-tasty",
      "display_url" : "quora.com/Is-panda-meat-\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "235118646798725120",
  "geo" : {
  },
  "id_str" : "235123667015696384",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys http://t.co/OhWLbQ0h site is down. Used to be really funny But ... The farmer was sentenced to two years in prison http://t.co/jTolQ2iW",
  "id" : 235123667015696384,
  "in_reply_to_status_id" : 235118646798725120,
  "created_at" : "Mon Aug 13 21:20:25 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "235114953944752128",
  "geo" : {
  },
  "id_str" : "235120975044308992",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn LaterBro is da bomb",
  "id" : 235120975044308992,
  "in_reply_to_status_id" : 235114953944752128,
  "created_at" : "Mon Aug 13 21:09:43 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ditigalrain",
      "indices" : [ 50, 62 ]
    }, {
      "text" : "matrix",
      "indices" : [ 63, 70 ]
    }, {
      "text" : "js",
      "indices" : [ 71, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/cV0ldaU1",
      "expanded_url" : "http://bit.ly/MPy9PN",
      "display_url" : "bit.ly/MPy9PN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235085829192708097",
  "text" : "\"Javascript MATRIX 1.2 beta\" http://t.co/cV0ldaU1 #ditigalrain #matrix #js Nice 'Digital Rain' effect ;)",
  "id" : 235085829192708097,
  "created_at" : "Mon Aug 13 18:50:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/HaYWV8eL",
      "expanded_url" : "http://bit.ly/PdOTMQ",
      "display_url" : "bit.ly/PdOTMQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235080839539597312",
  "text" : "Bret Victor, beast of burden http://t.co/HaYWV8eL Love the design. jQuery - powered site. So awesome!",
  "id" : 235080839539597312,
  "created_at" : "Mon Aug 13 18:30:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "favicon",
      "indices" : [ 54, 62 ]
    } ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/WsvzOtFu",
      "expanded_url" : "http://bit.ly/MPxVIy",
      "display_url" : "bit.ly/MPxVIy"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235075779103436800",
  "text" : "Pie Charts For Favicons \u2014 Piecon http://t.co/WsvzOtFu #favicon",
  "id" : 235075779103436800,
  "created_at" : "Mon Aug 13 18:10:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/5PUYv1yh",
      "expanded_url" : "http://bit.ly/PdNdTx",
      "display_url" : "bit.ly/PdNdTx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235070746395807744",
  "text" : "Shuffle Text Effect with jQuery http://t.co/5PUYv1yh",
  "id" : 235070746395807744,
  "created_at" : "Mon Aug 13 17:50:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/lGuw4UQ5",
      "expanded_url" : "http://bit.ly/MPwKc9",
      "display_url" : "bit.ly/MPwKc9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235065704347496448",
  "text" : "Demo::Password Strength Checker http://t.co/lGuw4UQ5",
  "id" : 235065704347496448,
  "created_at" : "Mon Aug 13 17:30:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bookmarklet",
      "indices" : [ 41, 53 ]
    } ],
    "urls" : [ {
      "indices" : [ 20, 40 ],
      "url" : "http://t.co/wXTzVDQ3",
      "expanded_url" : "http://bit.ly/PdN3LZ",
      "display_url" : "bit.ly/PdN3LZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235060683132243968",
  "text" : "Bookmarklet Builder http://t.co/wXTzVDQ3 #bookmarklet So cool. Build your own bookmarklets",
  "id" : 235060683132243968,
  "created_at" : "Mon Aug 13 17:10:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "piracy",
      "indices" : [ 57, 64 ]
    }, {
      "text" : "kopimi",
      "indices" : [ 65, 72 ]
    }, {
      "text" : "downloadallthethings",
      "indices" : [ 73, 94 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/izE1rrEs",
      "expanded_url" : "http://bit.ly/MPwr0S",
      "display_url" : "bit.ly/MPwr0S"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235055646582661120",
  "text" : "/| copyme /| (All the kopimi Logos) http://t.co/izE1rrEs #piracy #kopimi #downloadallthethings Include one on your site for phun and profit",
  "id" : 235055646582661120,
  "created_at" : "Mon Aug 13 16:50:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http://t.co/suO2v0Df",
      "expanded_url" : "http://bit.ly/PdMLF0",
      "display_url" : "bit.ly/PdMLF0"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235050635685400576",
  "text" : "changing location.hash with jquery ui tabs - Stack Overflow http://t.co/suO2v0Df",
  "id" : 235050635685400576,
  "created_at" : "Mon Aug 13 16:30:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/P3piupLm",
      "expanded_url" : "http://bit.ly/PdMvWD",
      "display_url" : "bit.ly/PdMvWD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235045602382209027",
  "text" : "High Res Browser Logos | Reverse The Web http://t.co/P3piupLm",
  "id" : 235045602382209027,
  "created_at" : "Mon Aug 13 16:10:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "stat30fbliss",
      "screen_name" : "stat30fbliss",
      "indices" : [ 3, 16 ],
      "id_str" : "38628528",
      "id" : 38628528
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/2dxbGC20",
      "expanded_url" : "http://lungojs.com/",
      "display_url" : "lungojs.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235044413913575424",
  "text" : "RT @stat30fbliss: @_higg_ I used http://t.co/2dxbGC20 for the mobile site, but I am working on my own mobile framework now, which'll be  ...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 15, 35 ],
        "url" : "http://t.co/2dxbGC20",
        "expanded_url" : "http://lungojs.com/",
        "display_url" : "lungojs.com"
      } ]
    },
    "in_reply_to_status_id_str" : "235040659483025408",
    "geo" : {
    },
    "id_str" : "235042897257455616",
    "in_reply_to_user_id" : 468853739,
    "text" : "@_higg_ I used http://t.co/2dxbGC20 for the mobile site, but I am working on my own mobile framework now, which'll be taking it's place soon",
    "id" : 235042897257455616,
    "in_reply_to_status_id" : 235040659483025408,
    "created_at" : "Mon Aug 13 15:59:28 +0000 2012",
    "in_reply_to_screen_name" : "alphenic",
    "in_reply_to_user_id_str" : "468853739",
    "user" : {
      "name" : "stat30fbliss",
      "screen_name" : "stat30fbliss",
      "protected" : false,
      "id_str" : "38628528",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2907219012/9134ff5d7d4c3e91baccb48341595e1f_normal.jpeg",
      "id" : 38628528,
      "verified" : false
    }
  },
  "id" : 235044413913575424,
  "created_at" : "Mon Aug 13 16:05:29 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "stat30fbliss",
      "screen_name" : "stat30fbliss",
      "indices" : [ 0, 13 ],
      "id_str" : "38628528",
      "id" : 38628528
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 132 ],
      "url" : "http://t.co/C8w4pEpF",
      "expanded_url" : "http://m.robabby.com/",
      "display_url" : "m.robabby.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235040659483025408",
  "in_reply_to_user_id" : 38628528,
  "text" : "@stat30fbliss Love the site. I hacked the URL, adding an 'm' subdomain (my new hobby) Is this version finished? http://t.co/C8w4pEpF",
  "id" : 235040659483025408,
  "created_at" : "Mon Aug 13 15:50:34 +0000 2012",
  "in_reply_to_screen_name" : "stat30fbliss",
  "in_reply_to_user_id_str" : "38628528",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/30tD06JV",
      "expanded_url" : "http://bit.ly/PdMsd9",
      "display_url" : "bit.ly/PdMsd9"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235040651702583296",
  "text" : "Oldskool DHTML | Reverse The Web http://t.co/30tD06JV",
  "id" : 235040651702583296,
  "created_at" : "Mon Aug 13 15:50:32 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 79, 87 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 74 ],
      "url" : "http://t.co/iXNq4nFq",
      "expanded_url" : "http://dearcomputer.nl/extras/bugs/bug.php?x=001101101100110000101100100011010",
      "display_url" : "dearcomputer.nl/extras/bugs/bu\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235037995701501952",
  "text" : "Hack the URL of this to find a new bug-like creature: http://t.co/iXNq4nFq cc: @mathias",
  "id" : 235037995701501952,
  "created_at" : "Mon Aug 13 15:39:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sixsixsix",
      "indices" : [ 42, 52 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235037099441659904",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar The devil follows you next #sixsixsix",
  "id" : 235037099441659904,
  "created_at" : "Mon Aug 13 15:36:25 +0000 2012",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/ssqCYZyi",
      "expanded_url" : "http://bit.ly/LkhTW4",
      "display_url" : "bit.ly/LkhTW4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235035549897981954",
  "text" : "Lab - Windows 8 logo in CSS3 http://t.co/ssqCYZyi",
  "id" : 235035549897981954,
  "created_at" : "Mon Aug 13 15:30:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gmail",
      "screen_name" : "gmail",
      "indices" : [ 122, 128 ],
      "id_str" : "38679388",
      "id" : 38679388
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 0, 4 ]
    } ],
    "urls" : [ {
      "indices" : [ 81, 101 ],
      "url" : "http://t.co/nLc2iI0A",
      "expanded_url" : "http://isemail.info/%22%27alert%281%29%3BOR%201%3D1--%22%40gmail.com",
      "display_url" : "isemail.info/%22%27alert%28\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235034327342268416",
  "text" : "#xss in my email? For sure? Yeah I checked it against the official RFC standard: http://t.co/nLc2iI0A \"'alert(1);OR 1=1--\"@gmail.com",
  "id" : 235034327342268416,
  "created_at" : "Mon Aug 13 15:25:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gmail",
      "screen_name" : "gmail",
      "indices" : [ 71, 77 ],
      "id_str" : "38679388",
      "id" : 38679388
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 82, 86 ]
    }, {
      "text" : "techniques",
      "indices" : [ 87, 98 ]
    }, {
      "text" : "notetoself",
      "indices" : [ 99, 110 ]
    } ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/kn7o55vM",
      "expanded_url" : "http://isemail.info/%22%27OR%201%3D1--%22%40gmail.com",
      "display_url" : "isemail.info/%22%27OR%201%3\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235031850987442177",
  "text" : "Yep folks, it's a valid E-Mail address: http://t.co/kn7o55vM 'OR 1=1--\"@gmail.com #xss #techniques #notetoself",
  "id" : 235031850987442177,
  "created_at" : "Mon Aug 13 15:15:34 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/RBiD65ZO",
      "expanded_url" : "http://bit.ly/PdMhPc",
      "display_url" : "bit.ly/PdMhPc"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235030474853064706",
  "text" : "Examples - AjaxML | The new standard to implement AJAX http://t.co/RBiD65ZO",
  "id" : 235030474853064706,
  "created_at" : "Mon Aug 13 15:10:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "phunandprofit",
      "indices" : [ 119, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/fQSeUdID",
      "expanded_url" : "http://code.google.com/p/sweetcron/",
      "display_url" : "code.google.com/p/sweetcron/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235028080106803200",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar More about Sweetcron here: http://t.co/fQSeUdID The admin has to add your Twitter ID though. But still! #phunandprofit !",
  "id" : 235028080106803200,
  "created_at" : "Mon Aug 13 15:00:35 +0000 2012",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    }, {
      "name" : "Pujun Li",
      "screen_name" : "jackmasa",
      "indices" : [ 86, 95 ],
      "id_str" : "256454626",
      "id" : 256454626
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 41, 45 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http://t.co/yfGzFXuC",
      "expanded_url" : "http://iwantaneff.in/stream/",
      "display_url" : "iwantaneff.in/stream/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235027860899901440",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar With Sweetcron you can do #xss http://t.co/yfGzFXuC I mistakenly added @jackmasa to that, and I found an alert(1) executing",
  "id" : 235027860899901440,
  "created_at" : "Mon Aug 13 14:59:43 +0000 2012",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 3, 17 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "10alert",
      "indices" : [ 59, 67 ]
    }, {
      "text" : "XSS",
      "indices" : [ 99, 103 ]
    }, {
      "text" : "Vector",
      "indices" : [ 104, 111 ]
    }, {
      "text" : "notetoself",
      "indices" : [ 112, 123 ]
    } ],
    "urls" : [ {
      "indices" : [ 77, 97 ],
      "url" : "http://t.co/wb4kKQ06",
      "expanded_url" : "http://jsfiddle.net/WpxMc/57/",
      "display_url" : "jsfiddle.net/WpxMc/57/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235025680168017920",
  "text" : "RT @soaj1664ashar: &lt;body/onload=&amp;lt;!--&amp;gt;&amp;#10alert(1)&gt;\u200B {http://t.co/wb4kKQ06} #XSS #Vector #notetoself",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "10alert",
        "indices" : [ 40, 48 ]
      }, {
        "text" : "XSS",
        "indices" : [ 80, 84 ]
      }, {
        "text" : "Vector",
        "indices" : [ 85, 92 ]
      }, {
        "text" : "notetoself",
        "indices" : [ 93, 104 ]
      } ],
      "urls" : [ {
        "indices" : [ 58, 78 ],
        "url" : "http://t.co/wb4kKQ06",
        "expanded_url" : "http://jsfiddle.net/WpxMc/57/",
        "display_url" : "jsfiddle.net/WpxMc/57/"
      } ]
    },
    "geo" : {
    },
    "id_str" : "235022512239558657",
    "text" : "&lt;body/onload=&amp;lt;!--&amp;gt;&amp;#10alert(1)&gt;\u200B {http://t.co/wb4kKQ06} #XSS #Vector #notetoself",
    "id" : 235022512239558657,
    "created_at" : "Mon Aug 13 14:38:28 +0000 2012",
    "user" : {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "protected" : false,
      "id_str" : "277735240",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3466042152/c41edb1dfd837f875f26ce4beb6b9543_normal.jpeg",
      "id" : 277735240,
      "verified" : false
    }
  },
  "id" : 235025680168017920,
  "created_at" : "Mon Aug 13 14:51:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/wgh8370N",
      "expanded_url" : "http://bit.ly/PdMguD",
      "display_url" : "bit.ly/PdMguD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235025463658033154",
  "text" : "Squidoo : Welcome to Squidoo http://t.co/wgh8370N",
  "id" : 235025463658033154,
  "created_at" : "Mon Aug 13 14:50:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235023510253219842",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar &lt;script&gt;[]===alert(/1/);&lt;/script&gt;",
  "id" : 235023510253219842,
  "created_at" : "Mon Aug 13 14:42:26 +0000 2012",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235023389411127296",
  "text" : "&lt;script&gt;[]===alert(/1/);&lt;/script&gt;",
  "id" : 235023389411127296,
  "created_at" : "Mon Aug 13 14:41:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "rnathias bynens",
      "screen_name" : "rnathias",
      "indices" : [ 102, 111 ],
      "id_str" : "556715613",
      "id" : 556715613
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "vbscript",
      "indices" : [ 81, 90 ]
    }, {
      "text" : "xss",
      "indices" : [ 91, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235022262326149120",
  "text" : "&lt;script language=&gt;vbscript:/ \uFF58\uFF53\uFF53 \uFF14 \uFF50\uFF48\uFF55\uFF4E \uFF06 \uFF50\uFF52\uFF4F\uFF46\uFF49\uFF54 /+alert(1)&lt;/script&gt; #vbscript #xss cc:// @rnathias",
  "id" : 235022262326149120,
  "created_at" : "Mon Aug 13 14:37:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "vbscript",
      "indices" : [ 81, 90 ]
    }, {
      "text" : "xss",
      "indices" : [ 91, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235020690292633600",
  "text" : "&lt;script language=&gt;vbscript:/ \uFF58\uFF53\uFF53 \uFF14 \uFF50\uFF48\uFF55\uFF4E \uFF06 \uFF50\uFF52\uFF4F\uFF46\uFF49\uFF54 /+alert(1)&lt;/script&gt; #vbscript #xss",
  "id" : 235020690292633600,
  "created_at" : "Mon Aug 13 14:31:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 71, 75 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235020513666273281",
  "text" : "&lt;script&gt;vbscript:/ \uFF58\uFF53\uFF53 \uFF14 \uFF50\uFF48\uFF55\uFF4E \uFF06 \uFF50\uFF52\uFF4F\uFF46\uFF49\uFF54 /+alert(1)&lt;/script&gt; #xss",
  "id" : 235020513666273281,
  "created_at" : "Mon Aug 13 14:30:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cdn77",
      "indices" : [ 91, 97 ]
    }, {
      "text" : "cdn",
      "indices" : [ 98, 102 ]
    } ],
    "urls" : [ {
      "indices" : [ 70, 90 ],
      "url" : "http://t.co/QYUtzECM",
      "expanded_url" : "http://bit.ly/MPvXb6",
      "display_url" : "bit.ly/MPvXb6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235020485841272832",
  "text" : "Creating a CDN powered Directory Listing with CDN77 | Reverse The Web http://t.co/QYUtzECM #cdn77 #cdn",
  "id" : 235020485841272832,
  "created_at" : "Mon Aug 13 14:30:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/Jv1iWrYy",
      "expanded_url" : "http://bit.ly/MPvYfd",
      "display_url" : "bit.ly/MPvYfd"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235015362331164672",
  "text" : "Blaster.js | Reverse The Web http://t.co/Jv1iWrYy",
  "id" : 235015362331164672,
  "created_at" : "Mon Aug 13 14:10:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235013697267978240",
  "text" : "javascript:prompt('','\u2192');void(0); My new friend on the internet!",
  "id" : 235013697267978240,
  "created_at" : "Mon Aug 13 14:03:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "235013325702955008",
  "text" : "Nice bookmarklet to get that right arrow character I use all the time: javascript:prompt('','%E2%86%92');void(0);",
  "id" : 235013325702955008,
  "created_at" : "Mon Aug 13 14:01:57 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 33, 40 ]
    } ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/BxWcrhrl",
      "expanded_url" : "http://bit.ly/MPvINl",
      "display_url" : "bit.ly/MPvINl"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235010341887676417",
  "text" : "RefineSlide http://t.co/BxWcrhrl #jquery Plugin.",
  "id" : 235010341887676417,
  "created_at" : "Mon Aug 13 13:50:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html5",
      "indices" : [ 50, 56 ]
    } ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/YN1MNatE",
      "expanded_url" : "http://bit.ly/MPvdCK",
      "display_url" : "bit.ly/MPvdCK"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235005360790986752",
  "text" : "Oscillating rotating message http://t.co/YN1MNatE #html5 &lt;PROGRESS&gt; element demo",
  "id" : 235005360790986752,
  "created_at" : "Mon Aug 13 13:30:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Robert Nyman",
      "screen_name" : "robertnyman",
      "indices" : [ 0, 12 ],
      "id_str" : "8414132",
      "id" : 8414132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/JAUPl4gt",
      "expanded_url" : "http://iwantaneff.in/meme",
      "display_url" : "iwantaneff.in/meme"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235001064821239809",
  "in_reply_to_user_id" : 8414132,
  "text" : "@robertnyman Hey what's your thoughts on this? http://t.co/JAUPl4gt I know you love funny images for presentations and that. Maybe it helps?",
  "id" : 235001064821239809,
  "created_at" : "Mon Aug 13 13:13:14 +0000 2012",
  "in_reply_to_screen_name" : "robertnyman",
  "in_reply_to_user_id_str" : "8414132",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "piracy",
      "indices" : [ 42, 49 ]
    }, {
      "text" : "github",
      "indices" : [ 50, 57 ]
    } ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/RwaRx7kf",
      "expanded_url" : "http://bit.ly/PdKYzQ",
      "display_url" : "bit.ly/PdKYzQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "235000277617496065",
  "text" : "http://t.co/RwaRx7kf That is all. KTHXBAI #piracy #github",
  "id" : 235000277617496065,
  "created_at" : "Mon Aug 13 13:10:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Robert Nyman",
      "screen_name" : "robertnyman",
      "indices" : [ 0, 12 ],
      "id_str" : "8414132",
      "id" : 8414132
    }, {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 54, 62 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "234997596479967232",
  "geo" : {
  },
  "id_str" : "234998318344835073",
  "in_reply_to_user_id" : 8414132,
  "text" : "@robertnyman Or just disable comments altogether like @codepo8 does and force peeps to reply to long ranty blog-posts in 140 chars ;)",
  "id" : 234998318344835073,
  "in_reply_to_status_id" : 234997596479967232,
  "created_at" : "Mon Aug 13 13:02:19 +0000 2012",
  "in_reply_to_screen_name" : "robertnyman",
  "in_reply_to_user_id_str" : "8414132",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Robert Nyman",
      "screen_name" : "robertnyman",
      "indices" : [ 0, 12 ],
      "id_str" : "8414132",
      "id" : 8414132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 110 ],
      "url" : "http://t.co/KYHYITMh",
      "expanded_url" : "http://wtfjs.com/",
      "display_url" : "wtfjs.com"
    } ]
  },
  "in_reply_to_status_id_str" : "234997069327237120",
  "geo" : {
  },
  "id_str" : "234997497213362176",
  "in_reply_to_user_id" : 8414132,
  "text" : "@robertnyman There is also a nice trick. Do a little quiz on the form. Use something from http://t.co/KYHYITMh to bewilder them ;)",
  "id" : 234997497213362176,
  "in_reply_to_status_id" : 234997069327237120,
  "created_at" : "Mon Aug 13 12:59:04 +0000 2012",
  "in_reply_to_screen_name" : "robertnyman",
  "in_reply_to_user_id_str" : "8414132",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Robert Nyman",
      "screen_name" : "robertnyman",
      "indices" : [ 0, 12 ],
      "id_str" : "8414132",
      "id" : 8414132
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "countryban",
      "indices" : [ 114, 125 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "234990710321995776",
  "geo" : {
  },
  "id_str" : "234996943108050944",
  "in_reply_to_user_id" : 8414132,
  "text" : "@robertnyman No captcha on the form? Although even with them, folk still post their crap. Just IP-Range ban them. #countryban",
  "id" : 234996943108050944,
  "in_reply_to_status_id" : 234990710321995776,
  "created_at" : "Mon Aug 13 12:56:51 +0000 2012",
  "in_reply_to_screen_name" : "robertnyman",
  "in_reply_to_user_id_str" : "8414132",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html5",
      "indices" : [ 78, 84 ]
    }, {
      "text" : "polyfills",
      "indices" : [ 85, 95 ]
    }, {
      "text" : "linkdump",
      "indices" : [ 96, 105 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 77 ],
      "url" : "http://t.co/BJgp4lBa",
      "expanded_url" : "http://bit.ly/MPulOs",
      "display_url" : "bit.ly/MPulOs"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234995249062244352",
  "text" : "HTML5 Cross Browser Polyfills \u00B7 Modernizr/Modernizr Wiki http://t.co/BJgp4lBa #html5 #polyfills #linkdump",
  "id" : 234995249062244352,
  "created_at" : "Mon Aug 13 12:50:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 77 ],
      "url" : "http://t.co/DAttshc0",
      "expanded_url" : "http://bit.ly/MPuhyv",
      "display_url" : "bit.ly/MPuhyv"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234992853057695744",
  "text" : "prettyCheckboxes | St\u00E9phane Caron \u2013 No Margin For Errors http://t.co/DAttshc0",
  "id" : 234992853057695744,
  "created_at" : "Mon Aug 13 12:40:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Thomas Fuchs",
      "screen_name" : "thomasfuchs",
      "indices" : [ 0, 12 ],
      "id_str" : "6927562",
      "id" : 6927562
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/fV25uIBi",
      "expanded_url" : "http://isharefil.es/BvQx",
      "display_url" : "isharefil.es/BvQx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234978434156208128",
  "in_reply_to_user_id" : 6927562,
  "text" : "@thomasfuchs http://t.co/fV25uIBi Old DHTML JS from the golden halcyon years.",
  "id" : 234978434156208128,
  "created_at" : "Mon Aug 13 11:43:19 +0000 2012",
  "in_reply_to_screen_name" : "thomasfuchs",
  "in_reply_to_user_id_str" : "6927562",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/fV25uIBi",
      "expanded_url" : "http://isharefil.es/BvQx",
      "display_url" : "isharefil.es/BvQx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234978210977312768",
  "text" : "Hehe! http://t.co/fV25uIBi",
  "id" : 234978210977312768,
  "created_at" : "Mon Aug 13 11:42:25 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/Qpp9B8z9",
      "expanded_url" : "http://isharefil.es/Busk",
      "display_url" : "isharefil.es/Busk"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234977518803898368",
  "text" : "http://t.co/Qpp9B8z9",
  "id" : 234977518803898368,
  "created_at" : "Mon Aug 13 11:39:40 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://geekli.st\" rel=\"nofollow\">Geeklist Inc</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 7, 27 ],
      "url" : "http://t.co/WhUEsaIr",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/aRtcFTVy",
      "expanded_url" : "http://gkl.st/JTzzT",
      "display_url" : "gkl.st/JTzzT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234903381825712128",
  "text" : "\"dh on http://t.co/WhUEsaIr\"  http://t.co/aRtcFTVy",
  "id" : 234903381825712128,
  "created_at" : "Mon Aug 13 06:45:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "4chan",
      "indices" : [ 104, 110 ]
    }, {
      "text" : "raid",
      "indices" : [ 111, 116 ]
    }, {
      "text" : "habbohotel",
      "indices" : [ 117, 128 ]
    } ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/2pccJdUy",
      "expanded_url" : "http://dubthedew.com/gallery-of-names/",
      "display_url" : "dubthedew.com/gallery-of-nam\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234896592384163840",
  "text" : "The great Dub the Dew 4chan raid of 2012  http://t.co/2pccJdUy Better than the Habbo Hotel one, by far. #4chan #raid #habbohotel",
  "id" : 234896592384163840,
  "created_at" : "Mon Aug 13 06:18:06 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 0, 8 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "unicode",
      "indices" : [ 30, 38 ]
    } ],
    "urls" : [ {
      "indices" : [ 9, 29 ],
      "url" : "http://t.co/CToPHrT6",
      "expanded_url" : "http://www.reddit.com/r/funny/comments/y1dei/never_doubt_the_power_of_youtube/",
      "display_url" : "reddit.com/r/funny/commen\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234882086425743360",
  "in_reply_to_user_id" : 532923,
  "text" : "@mathias http://t.co/CToPHrT6 #unicode",
  "id" : 234882086425743360,
  "created_at" : "Mon Aug 13 05:20:27 +0000 2012",
  "in_reply_to_screen_name" : "mathias",
  "in_reply_to_user_id_str" : "532923",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 130 ],
      "url" : "http://t.co/Mn2dqP5d",
      "expanded_url" : "http://iwantaneff.in/rss/",
      "display_url" : "iwantaneff.in/rss/"
    } ]
  },
  "in_reply_to_status_id_str" : "234847968883257344",
  "geo" : {
  },
  "id_str" : "234855533637689344",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii Yeah. Magpie is a bit heavy for my liking. I have the Chimp thing hopping along nicely here: http://t.co/Mn2dqP5d",
  "id" : 234855533637689344,
  "in_reply_to_status_id" : 234847968883257344,
  "created_at" : "Mon Aug 13 03:34:57 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Phil Ripperger",
      "screen_name" : "pdsphil",
      "indices" : [ 0, 8 ],
      "id_str" : "641213",
      "id" : 641213
    }, {
      "name" : "toby sterrett",
      "screen_name" : "takeo",
      "indices" : [ 9, 15 ],
      "id_str" : "616673",
      "id" : 616673
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 117 ],
      "url" : "http://t.co/ZQbHgGXo",
      "expanded_url" : "http://isharefil.es/IhYx",
      "display_url" : "isharefil.es/IhYx"
    } ]
  },
  "in_reply_to_status_id_str" : "234846013762977792",
  "geo" : {
  },
  "id_str" : "234854866693017600",
  "in_reply_to_user_id" : 641213,
  "text" : "@pdsphil @takeo Still not working :( I keep faving links just to test, but keep on getting this: http://t.co/ZQbHgGXo",
  "id" : 234854866693017600,
  "in_reply_to_status_id" : 234846013762977792,
  "created_at" : "Mon Aug 13 03:32:18 +0000 2012",
  "in_reply_to_screen_name" : "pdsphil",
  "in_reply_to_user_id_str" : "641213",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rss",
      "indices" : [ 62, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/CDZP4fiQ",
      "expanded_url" : "http://chimpfeedr.com/",
      "display_url" : "chimpfeedr.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234847348117880832",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii http://t.co/CDZP4fiQ Combine all the things! #rss",
  "id" : 234847348117880832,
  "created_at" : "Mon Aug 13 03:02:25 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/Mn2dqP5d",
      "expanded_url" : "http://iwantaneff.in/rss/",
      "display_url" : "iwantaneff.in/rss/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234842270254370816",
  "text" : "This: http://t.co/Mn2dqP5d",
  "id" : 234842270254370816,
  "created_at" : "Mon Aug 13 02:42:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "toby sterrett",
      "screen_name" : "takeo",
      "indices" : [ 0, 6 ],
      "id_str" : "616673",
      "id" : 616673
    }, {
      "name" : "Phil Ripperger",
      "screen_name" : "pdsphil",
      "indices" : [ 7, 15 ],
      "id_str" : "641213",
      "id" : 641213
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234831426946727937",
  "in_reply_to_user_id" : 616673,
  "text" : "@takeo @pdsphil I'm favoriting links on Twitter, but they're not showing up on my laterstars profile. Why?",
  "id" : 234831426946727937,
  "created_at" : "Mon Aug 13 01:59:09 +0000 2012",
  "in_reply_to_screen_name" : "takeo",
  "in_reply_to_user_id_str" : "616673",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 0, 12 ],
      "id_str" : "793830",
      "id" : 793830
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 52, 56 ]
    } ],
    "urls" : [ {
      "indices" : [ 65, 85 ],
      "url" : "http://t.co/yfGzFXuC",
      "expanded_url" : "http://iwantaneff.in/stream/",
      "display_url" : "iwantaneff.in/stream/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234815204632977408",
  "in_reply_to_user_id" : 793830,
  "text" : "@chriscoyier You were included for having such cool #css tweets: http://t.co/yfGzFXuC",
  "id" : 234815204632977408,
  "created_at" : "Mon Aug 13 00:54:42 +0000 2012",
  "in_reply_to_screen_name" : "chriscoyier",
  "in_reply_to_user_id_str" : "793830",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "beer",
      "indices" : [ 25, 30 ]
    } ],
    "urls" : [ {
      "indices" : [ 119, 139 ],
      "url" : "http://t.co/VoZI2fh7",
      "expanded_url" : "http://pinterest.com/0x0/my-favorite-beers/",
      "display_url" : "pinterest.com/0x0/my-favorit\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234805835556085760",
  "text" : "It's wrong posting about #beer on a Monday. But teh web developer's lifestyle is a world un-akin to your typical 9-5er http://t.co/VoZI2fh7",
  "id" : 234805835556085760,
  "created_at" : "Mon Aug 13 00:17:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "appnet",
      "indices" : [ 132, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 111, 131 ],
      "url" : "http://t.co/fVGdQ23y",
      "expanded_url" : "http://www.getcreditcardnumbers.com/",
      "display_url" : "getcreditcardnumbers.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234775025415237633",
  "text" : "Credit Card Numbers, Anyone? 4532666056332775 4929968020427447 4716188842909822 For testing purposes only, LoL http://t.co/fVGdQ23y #appnet",
  "id" : 234775025415237633,
  "created_at" : "Sun Aug 12 22:15:02 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "appnet",
      "indices" : [ 36, 43 ]
    }, {
      "text" : "appnet",
      "indices" : [ 132, 139 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234774644337545216",
  "text" : "The lulzy awkward moment when +9000 #appnet backers signup with disposable CC numbers with $0.01 in the balance. Get ready 4 monday #appnet",
  "id" : 234774644337545216,
  "created_at" : "Sun Aug 12 22:13:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234770036261261313",
  "text" : "John Lennon singing \"Imagine there's no money\"? Closing Ceremony was on in the background. \"Money\" was nevaar mentioned in the original song",
  "id" : 234770036261261313,
  "created_at" : "Sun Aug 12 21:55:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "appnet",
      "indices" : [ 7, 14 ]
    } ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/oWO7dPGn",
      "expanded_url" : "http://bit.ly/PeQp1h",
      "display_url" : "bit.ly/PeQp1h"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234763532393127936",
  "text" : "Get an #appnet style 'Infinite Scroll' http://t.co/oWO7dPGn script for your site.",
  "id" : 234763532393127936,
  "created_at" : "Sun Aug 12 21:29:22 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 34 ],
      "url" : "http://t.co/Nrsv1w34",
      "expanded_url" : "http://bit.ly/IPijxI",
      "display_url" : "bit.ly/IPijxI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234762187980296192",
  "text" : "david higgins http://t.co/Nrsv1w34",
  "id" : 234762187980296192,
  "created_at" : "Sun Aug 12 21:24:01 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Buffer",
      "screen_name" : "bufferapp",
      "indices" : [ 0, 10 ],
      "id_str" : "1510622484",
      "id" : 1510622484
    }, {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 124, 132 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/1B6KI3ax",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    }, {
      "indices" : [ 88, 108 ],
      "url" : "http://t.co/sn2iNQTG",
      "expanded_url" : "http://isharefil.es/Ihrk",
      "display_url" : "isharefil.es/Ihrk"
    } ]
  },
  "in_reply_to_status_id_str" : "234760557893386242",
  "geo" : {
  },
  "id_str" : "234761318790492160",
  "in_reply_to_user_id" : 496946720,
  "text" : "@bufferapp Just connected it my http://t.co/1B6KI3ax account. This spells pure awesome. http://t.co/sn2iNQTG +1 for the tip @tompntn",
  "id" : 234761318790492160,
  "in_reply_to_status_id" : 234760557893386242,
  "created_at" : "Sun Aug 12 21:20:34 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    }, {
      "name" : "Buffer",
      "screen_name" : "bufferapp",
      "indices" : [ 9, 19 ],
      "id_str" : "1510622484",
      "id" : 1510622484
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 114 ],
      "url" : "http://t.co/1B6KI3ax",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    } ]
  },
  "in_reply_to_status_id_str" : "234759931260182528",
  "geo" : {
  },
  "id_str" : "234760412271345664",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn @bufferapp For realz? Oh that's lovely. Best news I've heard all day. Well that, and http://t.co/1B6KI3ax getting their 500K",
  "id" : 234760412271345664,
  "in_reply_to_status_id" : 234759931260182528,
  "created_at" : "Sun Aug 12 21:16:58 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/Gx0hKn26",
      "expanded_url" : "http://ifttt.com",
      "display_url" : "ifttt.com"
    }, {
      "indices" : [ 70, 90 ],
      "url" : "http://t.co/1B6KI3ax",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    } ]
  },
  "in_reply_to_status_id_str" : "234745368733159424",
  "geo" : {
  },
  "id_str" : "234757844405198848",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Well there is http://t.co/Gx0hKn26 But they've yet to add an http://t.co/1B6KI3ax recipe to it. When it gets popular they'll add it",
  "id" : 234757844405198848,
  "in_reply_to_status_id" : 234745368733159424,
  "created_at" : "Sun Aug 12 21:06:46 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 37, 44 ]
    } ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/y1oHSx4N",
      "expanded_url" : "http://bit.ly/PeFF2X",
      "display_url" : "bit.ly/PeFF2X"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234750145655361537",
  "text" : "jQuery PowerTip http://t.co/y1oHSx4N #jquery",
  "id" : 234750145655361537,
  "created_at" : "Sun Aug 12 20:36:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "javascript",
      "indices" : [ 54, 65 ]
    }, {
      "text" : "brainfuck",
      "indices" : [ 66, 76 ]
    }, {
      "text" : "obfuscation",
      "indices" : [ 77, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http://t.co/sl2x5EXD",
      "expanded_url" : "http://bit.ly/MQdFXa",
      "display_url" : "bit.ly/MQdFXa"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234750019159326723",
  "text" : "Hieroglyphy | Patricio Palladino http://t.co/sl2x5EXD #javascript #brainfuck #obfuscation",
  "id" : 234750019159326723,
  "created_at" : "Sun Aug 12 20:35:40 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http://t.co/JTjeIr6Q",
      "expanded_url" : "http://bit.ly/MQdD1D",
      "display_url" : "bit.ly/MQdD1D"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234749924061892609",
  "text" : "jQuery DB http://t.co/JTjeIr6Q",
  "id" : 234749924061892609,
  "created_at" : "Sun Aug 12 20:35:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "appnet",
      "indices" : [ 40, 47 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234739235566006272",
  "text" : "I had to say it, is everyone who backed #appnet ready for Monday when they rob $50.00 dollars from your bank account that has nothing in it?",
  "id" : 234739235566006272,
  "created_at" : "Sun Aug 12 19:52:49 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30A2\u30F3\u30C7\u30C3\u30C9\u306E\u72AC",
      "screen_name" : "gersty",
      "indices" : [ 3, 10 ],
      "id_str" : "28030104",
      "id" : 28030104
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234736786713231360",
  "text" : "RT @gersty: email is beautiful bc it's not a central hub. I use gmail, my friend uses yahoo, we can still talk to each other. not the sa ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/tweetbot\" rel=\"nofollow\">Tweetbot for iOS</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Appnet",
        "indices" : [ 132, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
    },
    "id_str" : "234721948440068097",
    "text" : "email is beautiful bc it's not a central hub. I use gmail, my friend uses yahoo, we can still talk to each other. not the same with #Appnet.",
    "id" : 234721948440068097,
    "created_at" : "Sun Aug 12 18:44:08 +0000 2012",
    "user" : {
      "name" : "\u30A2\u30F3\u30C7\u30C3\u30C9\u306E\u72AC",
      "screen_name" : "gersty",
      "protected" : false,
      "id_str" : "28030104",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000284787019/9cda43a404b752e63a784dd0c139d0aa_normal.png",
      "id" : 28030104,
      "verified" : false
    }
  },
  "id" : 234736786713231360,
  "created_at" : "Sun Aug 12 19:43:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "caffrey",
      "screen_name" : "caffrey",
      "indices" : [ 0, 8 ],
      "id_str" : "801662214",
      "id" : 801662214
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 105 ],
      "url" : "https://t.co/3wPPGBWC",
      "expanded_url" : "https://github.com/rvagg/CAPSLOCKSCRIPT",
      "display_url" : "github.com/rvagg/CAPSLOCK\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234735422822035456",
  "in_reply_to_user_id" : 102566129,
  "text" : "@caffrey Heh. JavaScript jargonist. Love it. You might add this to your vocab then? https://t.co/3wPPGBWC",
  "id" : 234735422822035456,
  "created_at" : "Sun Aug 12 19:37:40 +0000 2012",
  "in_reply_to_screen_name" : "emilysipiora",
  "in_reply_to_user_id_str" : "102566129",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "indices" : [ 3, 9 ],
      "id_str" : "2367111",
      "id" : 2367111
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/MXVK3yMx",
      "expanded_url" : "http://awe.sm/e2ncW",
      "display_url" : "awe.sm/e2ncW"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234735053815562240",
  "text" : "RT @assaf: JAVASCRIPT: T-H-E L-O-U-D P-A-R-T-S  http://t.co/MXVK3yMx bringing JS closer to FORTRAN, write all JS in CAPSLOCK",
  "retweeted_status" : {
    "source" : "<a href=\"http://timely.is\" rel=\"nofollow\">Timely by Demandforce</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 37, 57 ],
        "url" : "http://t.co/MXVK3yMx",
        "expanded_url" : "http://awe.sm/e2ncW",
        "display_url" : "awe.sm/e2ncW"
      } ]
    },
    "geo" : {
    },
    "id_str" : "234709406288334848",
    "text" : "JAVASCRIPT: T-H-E L-O-U-D P-A-R-T-S  http://t.co/MXVK3yMx bringing JS closer to FORTRAN, write all JS in CAPSLOCK",
    "id" : 234709406288334848,
    "created_at" : "Sun Aug 12 17:54:17 +0000 2012",
    "user" : {
      "name" : "Assaf Arkin",
      "screen_name" : "assaf",
      "protected" : false,
      "id_str" : "2367111",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000045323076/8355e0bb4b3b27a90ee35497cf8c34e5_normal.jpeg",
      "id" : 2367111,
      "verified" : false
    }
  },
  "id" : 234735053815562240,
  "created_at" : "Sun Aug 12 19:36:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SocialBro App",
      "screen_name" : "SocialBro",
      "indices" : [ 19, 29 ],
      "id_str" : "233631354",
      "id" : 233631354
    }, {
      "name" : "Buffer",
      "screen_name" : "bufferapp",
      "indices" : [ 74, 84 ],
      "id_str" : "1510622484",
      "id" : 1510622484
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234734379644104707",
  "text" : "I only purchased a @SocialBro account to get a nice buffering pattern for @bufferapp I'm going to keep the pattern. Doubt it changes much.",
  "id" : 234734379644104707,
  "created_at" : "Sun Aug 12 19:33:31 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/aXGKpL3x",
      "expanded_url" : "http://bit.ly/PdKHNi",
      "display_url" : "bit.ly/PdKHNi"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234723440207212544",
  "text" : "Carbonmade : Your online portfolio. http://t.co/aXGKpL3x",
  "id" : 234723440207212544,
  "created_at" : "Sun Aug 12 18:50:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html5",
      "indices" : [ 46, 52 ]
    }, {
      "text" : "games",
      "indices" : [ 53, 59 ]
    } ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/UTudqOTt",
      "expanded_url" : "http://bit.ly/PdKDx5",
      "display_url" : "bit.ly/PdKDx5"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234718410905362432",
  "text" : "\"Games Powered by HTML5\" http://t.co/UTudqOTt #html5 #games",
  "id" : 234718410905362432,
  "created_at" : "Sun Aug 12 18:30:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 38, 41 ],
      "id_str" : "19010677",
      "id" : 19010677
    }, {
      "name" : "Dalton Caldwell",
      "screen_name" : "daltonc",
      "indices" : [ 63, 71 ],
      "id_str" : "9151842",
      "id" : 9151842
    }, {
      "name" : "Tantek \u00C7elik",
      "screen_name" : "t",
      "indices" : [ 119, 121 ],
      "id_str" : "11628",
      "id" : 11628
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "microblogging",
      "indices" : [ 72, 86 ]
    }, {
      "text" : "syndication",
      "indices" : [ 87, 99 ]
    }, {
      "text" : "open",
      "indices" : [ 100, 105 ]
    }, {
      "text" : "webcamp",
      "indices" : [ 106, 114 ]
    } ],
    "urls" : [ {
      "indices" : [ 13, 33 ],
      "url" : "http://t.co/1B6KI3ax",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    }, {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/J0ERVN5m",
      "expanded_url" : "http://iwantaneff.in/blog/?action=view&url=i-just-backed-appnet",
      "display_url" : "iwantaneff.in/blog/?action=v\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234715908550057984",
  "text" : "Why I backed http://t.co/1B6KI3ax cc: @ys http://t.co/J0ERVN5m @daltonc #microblogging #syndication #open #webcamp cc: @t",
  "id" : 234715908550057984,
  "created_at" : "Sun Aug 12 18:20:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "appnet",
      "indices" : [ 76, 83 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http://t.co/7Z4GvcHP",
      "expanded_url" : "http://iwantaneff.in/blog/",
      "display_url" : "iwantaneff.in/blog/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234715489832673281",
  "text" : "Might have to rename my Twitter clone to &gt;240 chars http://t.co/7Z4GvcHP #appnet",
  "id" : 234715489832673281,
  "created_at" : "Sun Aug 12 18:18:28 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/bpatfpej",
      "expanded_url" : "http://bit.ly/MPuMbB",
      "display_url" : "bit.ly/MPuMbB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234713408363188225",
  "text" : "david higgins labs http://t.co/bpatfpej",
  "id" : 234713408363188225,
  "created_at" : "Sun Aug 12 18:10:11 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dalton Caldwell",
      "screen_name" : "daltonc",
      "indices" : [ 3, 11 ],
      "id_str" : "9151842",
      "id" : 9151842
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http://t.co/K3xsQKpP",
      "expanded_url" : "http://bit.ly/OTfqP4",
      "display_url" : "bit.ly/OTfqP4"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234713398284271616",
  "text" : "RT @daltonc: \"We did it.\" -&gt; http://t.co/K3xsQKpP",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 19, 39 ],
        "url" : "http://t.co/K3xsQKpP",
        "expanded_url" : "http://bit.ly/OTfqP4",
        "display_url" : "bit.ly/OTfqP4"
      } ]
    },
    "geo" : {
    },
    "id_str" : "234713210295574528",
    "text" : "\"We did it.\" -&gt; http://t.co/K3xsQKpP",
    "id" : 234713210295574528,
    "created_at" : "Sun Aug 12 18:09:24 +0000 2012",
    "user" : {
      "name" : "Dalton Caldwell",
      "screen_name" : "daltonc",
      "protected" : false,
      "id_str" : "9151842",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2603466928/nlwz0yfz5j55ap21t3dz_normal.jpeg",
      "id" : 9151842,
      "verified" : false
    }
  },
  "id" : 234713398284271616,
  "created_at" : "Sun Aug 12 18:10:09 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 22 ],
      "url" : "http://t.co/D5Ggse9w",
      "expanded_url" : "http://bit.ly/PdKjhB",
      "display_url" : "bit.ly/PdKjhB"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234708385520558080",
  "text" : "\u263A http://t.co/D5Ggse9w",
  "id" : 234708385520558080,
  "created_at" : "Sun Aug 12 17:50:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bootstrap",
      "indices" : [ 91, 101 ]
    } ],
    "urls" : [ {
      "indices" : [ 70, 90 ],
      "url" : "http://t.co/mtmhl0DL",
      "expanded_url" : "http://bit.ly/MPuAJF",
      "display_url" : "bit.ly/MPuAJF"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234703473881513984",
  "text" : "Font Awesome, the iconic font designed for use with Twitter Bootstrap http://t.co/mtmhl0DL #bootstrap",
  "id" : 234703473881513984,
  "created_at" : "Sun Aug 12 17:30:43 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Hauser",
      "screen_name" : "dh",
      "indices" : [ 100, 103 ],
      "id_str" : "352553",
      "id" : 352553
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "appnet",
      "indices" : [ 104, 111 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 74 ],
      "url" : "https://t.co/Mj7Mr3cg",
      "expanded_url" : "https://alpha.app.net/dh/post/31900",
      "display_url" : "alpha.app.net/dh/post/31900"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234702365213745152",
  "text" : "Another Social network to syndicate all my links to: https://t.co/Mj7Mr3cg (My first post) My ID is @dh #appnet",
  "id" : 234702365213745152,
  "created_at" : "Sun Aug 12 17:26:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/VihMBwvO",
      "expanded_url" : "http://bit.ly/IOuvPf",
      "display_url" : "bit.ly/IOuvPf"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234698369598107649",
  "text" : "The Github Pirates http://t.co/VihMBwvO",
  "id" : 234698369598107649,
  "created_at" : "Sun Aug 12 17:10:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "utf8",
      "indices" : [ 62, 67 ]
    }, {
      "text" : "unicode",
      "indices" : [ 68, 76 ]
    } ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/Nhcle2HZ",
      "expanded_url" : "http://bit.ly/MPunWJ",
      "display_url" : "bit.ly/MPunWJ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234693303319273472",
  "text" : "Miscellaneous Symbols ( Unicode / UTF8 ) http://t.co/Nhcle2HZ #utf8 #unicode",
  "id" : 234693303319273472,
  "created_at" : "Sun Aug 12 16:50:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 127 ],
      "url" : "http://t.co/ChQMytFN",
      "expanded_url" : "http://bit.ly/MPumC7",
      "display_url" : "bit.ly/MPumC7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234688243407413248",
  "text" : "\"ExplicitWeb is a regularly recorded podcast all about the front lines of website design and development.\" http://t.co/ChQMytFN",
  "id" : 234688243407413248,
  "created_at" : "Sun Aug 12 16:30:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://twitter.com/tweetbutton\" rel=\"nofollow\">Tweet Button</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 119, 139 ],
      "url" : "http://t.co/ZHENT3My",
      "expanded_url" : "http://r3versin.com/blog/blaster-js/",
      "display_url" : "r3versin.com/blog/blaster-j\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234663340197158913",
  "text" : "@voidfles Read your Bookmarklet post. Have you seen blaster.js - Combines various Social Media bookmarklets into one:  http://t.co/ZHENT3My",
  "id" : 234663340197158913,
  "created_at" : "Sun Aug 12 14:51:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/BFxtCdVV",
      "expanded_url" : "http://bit.ly/MLNNM5",
      "display_url" : "bit.ly/MLNNM5"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234658023707144192",
  "text" : "Terms of Service; Didn't Read http://t.co/BFxtCdVV",
  "id" : 234658023707144192,
  "created_at" : "Sun Aug 12 14:30:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kim Asendorf",
      "screen_name" : "kimasendorf",
      "indices" : [ 114, 126 ],
      "id_str" : "28815445",
      "id" : 28815445
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "glitchart",
      "indices" : [ 80, 90 ]
    }, {
      "text" : "glitch",
      "indices" : [ 102, 109 ]
    } ],
    "urls" : [ {
      "indices" : [ 54, 74 ],
      "url" : "http://t.co/o6qZFLoP",
      "expanded_url" : "http://bit.ly/MLLtEU",
      "display_url" : "bit.ly/MLLtEU"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234653026986647552",
  "text" : "\"Uniform Spectrum - World's most weird image format.\" http://t.co/o6qZFLoP Nice #glitchart technique. #glitch via @kimasendorf",
  "id" : 234653026986647552,
  "created_at" : "Sun Aug 12 14:10:15 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 49, 56 ]
    }, {
      "text" : "plugin",
      "indices" : [ 57, 64 ]
    } ],
    "urls" : [ {
      "indices" : [ 28, 48 ],
      "url" : "http://t.co/l0UtpRKp",
      "expanded_url" : "http://bit.ly/MLLo46",
      "display_url" : "bit.ly/MLLo46"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234647995071746048",
  "text" : "jQuery notification Plug in http://t.co/l0UtpRKp #jquery #plugin",
  "id" : 234647995071746048,
  "created_at" : "Sun Aug 12 13:50:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Bentley",
      "screen_name" : "jasonbentley",
      "indices" : [ 81, 94 ],
      "id_str" : "4073941",
      "id" : 4073941
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http://t.co/VLeC0eVV",
      "expanded_url" : "http://scr.bi/MLL6dD",
      "display_url" : "scr.bi/MLL6dD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234642975840415744",
  "text" : "The \"Holy Crap!\" Impressively Big List of Web 2.0 Sites http://t.co/VLeC0eVV via @jasonbentley Heh. Most of these 404. Still good though.",
  "id" : 234642975840415744,
  "created_at" : "Sun Aug 12 13:30:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 58, 62 ]
    }, {
      "text" : "css3",
      "indices" : [ 63, 68 ]
    }, {
      "text" : "buttons",
      "indices" : [ 69, 77 ]
    } ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http://t.co/mxeGgXmN",
      "expanded_url" : "http://bit.ly/ImVRxI",
      "display_url" : "bit.ly/ImVRxI"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234637876225269760",
  "text" : "Just some other awesome CSS3 buttons http://t.co/mxeGgXmN #css #css3 #buttons",
  "id" : 234637876225269760,
  "created_at" : "Sun Aug 12 13:10:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lars Jung",
      "screen_name" : "lrsjng",
      "indices" : [ 112, 119 ],
      "id_str" : "91685967",
      "id" : 91685967
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 104 ],
      "url" : "http://t.co/NNq4bqTd",
      "expanded_url" : "http://bit.ly/P8Ql2Y",
      "display_url" : "bit.ly/P8Ql2Y"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234632857904099328",
  "text" : "jQuery.twinkle \u271C jQuery plug in to get the visitor's attention via visual effects \u00B7 http://t.co/NNq4bqTd via:// @lrsjng",
  "id" : 234632857904099328,
  "created_at" : "Sun Aug 12 12:50:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jsfiddle",
      "indices" : [ 104, 113 ]
    }, {
      "text" : "js",
      "indices" : [ 114, 117 ]
    }, {
      "text" : "javascript",
      "indices" : [ 118, 129 ]
    }, {
      "text" : "utf8",
      "indices" : [ 130, 135 ]
    } ],
    "urls" : [ {
      "indices" : [ 29, 49 ],
      "url" : "http://t.co/QGkEW88Q",
      "expanded_url" : "http://bit.ly/OQlWWP",
      "display_url" : "bit.ly/OQlWWP"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234627830133186561",
  "text" : "Unicode Table Demo by \"Paul\" http://t.co/QGkEW88Q Who is this guy, Paul? I want to know more about him! #jsfiddle #js #javascript #utf8",
  "id" : 234627830133186561,
  "created_at" : "Sun Aug 12 12:30:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 3, 17 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "JavaScript",
      "indices" : [ 30, 41 ]
    } ],
    "urls" : [ {
      "indices" : [ 69, 89 ],
      "url" : "http://t.co/TRFsj8Sq",
      "expanded_url" : "http://utf-8.jp/public/jsfuck.html",
      "display_url" : "utf-8.jp/public/jsfuck.\u2026"
    }, {
      "indices" : [ 100, 120 ],
      "url" : "http://t.co/3m6i9lAq",
      "expanded_url" : "http://utf-8.jp/public/jjencode.html",
      "display_url" : "utf-8.jp/public/jjencod\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234411837570048001",
  "text" : "RT @soaj1664ashar: Encode any #JavaScript using Symbols ---  JSF*ck (http://t.co/TRFsj8Sq)+jjencode(http://t.co/3m6i9lAq)+Hieroglyphy(ht ...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "JavaScript",
        "indices" : [ 11, 22 ]
      } ],
      "urls" : [ {
        "indices" : [ 50, 70 ],
        "url" : "http://t.co/TRFsj8Sq",
        "expanded_url" : "http://utf-8.jp/public/jsfuck.html",
        "display_url" : "utf-8.jp/public/jsfuck.\u2026"
      }, {
        "indices" : [ 81, 101 ],
        "url" : "http://t.co/3m6i9lAq",
        "expanded_url" : "http://utf-8.jp/public/jjencode.html",
        "display_url" : "utf-8.jp/public/jjencod\u2026"
      }, {
        "indices" : [ 115, 135 ],
        "url" : "http://t.co/FWQR1wRk",
        "expanded_url" : "http://www.reddit.com/tb/xzvjv",
        "display_url" : "reddit.com/tb/xzvjv"
      } ]
    },
    "geo" : {
    },
    "id_str" : "234410245198323713",
    "text" : "Encode any #JavaScript using Symbols ---  JSF*ck (http://t.co/TRFsj8Sq)+jjencode(http://t.co/3m6i9lAq)+Hieroglyphy(http://t.co/FWQR1wRk)",
    "id" : 234410245198323713,
    "created_at" : "Sat Aug 11 22:05:32 +0000 2012",
    "user" : {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "protected" : false,
      "id_str" : "277735240",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3466042152/c41edb1dfd837f875f26ce4beb6b9543_normal.jpeg",
      "id" : 277735240,
      "verified" : false
    }
  },
  "id" : 234411837570048001,
  "created_at" : "Sat Aug 11 22:11:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "xss",
      "indices" : [ 119, 123 ]
    } ],
    "urls" : [ {
      "indices" : [ 27, 47 ],
      "url" : "http://t.co/nCL3LZaY",
      "expanded_url" : "http://utf-8.jp/public/jjencode.html",
      "display_url" : "utf-8.jp/public/jjencod\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "234325137502126080",
  "geo" : {
  },
  "id_str" : "234408743985614848",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 Kind of similar \u2192 http://t.co/nCL3LZaY For some reason, it fails in latest FF, but I used this heavily in the #xss days of old ;)",
  "id" : 234408743985614848,
  "in_reply_to_status_id" : 234325137502126080,
  "created_at" : "Sat Aug 11 21:59:34 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234405499813126144",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys You've got mail ;)",
  "id" : 234405499813126144,
  "created_at" : "Sat Aug 11 21:46:40 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u2605 Peter Jaric \u2605",
      "screen_name" : "peterjaric",
      "indices" : [ 0, 11 ],
      "id_str" : "17488615",
      "id" : 17488615
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "notspam",
      "indices" : [ 91, 99 ]
    }, {
      "text" : "spamtheplanet",
      "indices" : [ 100, 114 ]
    }, {
      "text" : "webdev",
      "indices" : [ 115, 122 ]
    } ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234391548949848064",
  "in_reply_to_user_id" : 17488615,
  "text" : "@peterjaric I love your tweets. I do a bit of the ol' h@xxoring myself. Y U NO FOLLW BACK? #notspam #spamtheplanet #webdev etc ;)",
  "id" : 234391548949848064,
  "created_at" : "Sat Aug 11 20:51:14 +0000 2012",
  "in_reply_to_screen_name" : "peterjaric",
  "in_reply_to_user_id_str" : "17488615",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashley Reed",
      "screen_name" : "AshleyReed53",
      "indices" : [ 0, 13 ],
      "id_str" : "299746187",
      "id" : 299746187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234390863172755456",
  "in_reply_to_user_id" : 299746187,
  "text" : "@AshleyReed53 Ahh, the Twitter bot. Well thanks to TinEye, I can find your stock photo model Profile image and name and shame you.",
  "id" : 234390863172755456,
  "created_at" : "Sat Aug 11 20:48:31 +0000 2012",
  "in_reply_to_screen_name" : "AshleyReed53",
  "in_reply_to_user_id_str" : "299746187",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashar Javed",
      "screen_name" : "soaj1664ashar",
      "indices" : [ 0, 14 ],
      "id_str" : "277735240",
      "id" : 277735240
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "antigovernment",
      "indices" : [ 68, 83 ]
    }, {
      "text" : "dearFBI",
      "indices" : [ 87, 95 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "234381347030974464",
  "geo" : {
  },
  "id_str" : "234386342849675264",
  "in_reply_to_user_id" : 277735240,
  "text" : "@soaj1664ashar That's because they don't hash-tag their tweets with #antigovernment or #dearFBI Pastebins are where you find \uD835\uDE35\uD835\uDE26\uD835\uDE29 \uD835\uDE28\uD835\uDE30\uD835\uDE2D\uD835\uDE25 :)",
  "id" : 234386342849675264,
  "in_reply_to_status_id" : 234381347030974464,
  "created_at" : "Sat Aug 11 20:30:33 +0000 2012",
  "in_reply_to_screen_name" : "soaj1664ashar",
  "in_reply_to_user_id_str" : "277735240",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dalton Caldwell",
      "screen_name" : "daltonc",
      "indices" : [ 0, 8 ],
      "id_str" : "9151842",
      "id" : 9151842
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234379726398038016",
  "in_reply_to_user_id" : 9151842,
  "text" : "@daltonc Is there a public forum where we can discuss this further? Twitter is limited for discussing the future business models of Social",
  "id" : 234379726398038016,
  "created_at" : "Sat Aug 11 20:04:15 +0000 2012",
  "in_reply_to_screen_name" : "daltonc",
  "in_reply_to_user_id_str" : "9151842",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http://t.co/BJ9TV3Fs",
      "expanded_url" : "http://bit.ly/LbcCv9",
      "display_url" : "bit.ly/LbcCv9"
    } ]
  },
  "in_reply_to_status_id_str" : "234369043509964800",
  "geo" : {
  },
  "id_str" : "234376734361923584",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn Why Windows 8 Scares Me - YouTube http://t.co/BJ9TV3Fs  This explains it very well",
  "id" : 234376734361923584,
  "in_reply_to_status_id" : 234369043509964800,
  "created_at" : "Sat Aug 11 19:52:22 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 128 ],
      "url" : "https://t.co/VPBvtURU",
      "expanded_url" : "https://alpha.app.net/global/",
      "display_url" : "alpha.app.net/global/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234376170903306240",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys Sorry bro. I can't join. Just redirects to spammy give us money page. \"Sign up\" button is broke. Page: https://t.co/VPBvtURU",
  "id" : 234376170903306240,
  "created_at" : "Sat Aug 11 19:50:08 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    }, {
      "name" : "Dalton Caldwell",
      "screen_name" : "daltonc",
      "indices" : [ 4, 12 ],
      "id_str" : "9151842",
      "id" : 9151842
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 34 ],
      "url" : "https://t.co/2eEV99cD",
      "expanded_url" : "https://twitter.com/_higg_/status/234374202369642496",
      "display_url" : "twitter.com/_higg_/status/\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "234372134040907776",
  "geo" : {
  },
  "id_str" : "234375663694512128",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys @daltonc https://t.co/2eEV99cD",
  "id" : 234375663694512128,
  "in_reply_to_status_id" : 234372134040907776,
  "created_at" : "Sat Aug 11 19:48:07 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    }, {
      "name" : "Tantek \u00C7elik",
      "screen_name" : "t",
      "indices" : [ 4, 6 ],
      "id_str" : "11628",
      "id" : 11628
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "indie",
      "indices" : [ 65, 71 ]
    }, {
      "text" : "webcamp",
      "indices" : [ 72, 80 ]
    } ],
    "urls" : [ {
      "indices" : [ 7, 28 ],
      "url" : "https://t.co/2eEV99cD",
      "expanded_url" : "https://twitter.com/_higg_/status/234374202369642496",
      "display_url" : "twitter.com/_higg_/status/\u2026"
    }, {
      "indices" : [ 44, 64 ],
      "url" : "http://t.co/1B6KI3ax",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234374903812468736",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys @t https://t.co/2eEV99cD My thoughts on http://t.co/1B6KI3ax #indie #webcamp is mentioned Let's do away with centralized Social networks",
  "id" : 234374903812468736,
  "created_at" : "Sat Aug 11 19:45:06 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dalton Caldwell",
      "screen_name" : "daltonc",
      "indices" : [ 0, 8 ],
      "id_str" : "9151842",
      "id" : 9151842
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/1B6KI3ax",
      "expanded_url" : "http://app.net",
      "display_url" : "app.net"
    }, {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/imNuHw1d",
      "expanded_url" : "http://10k.aneventapart.com/2/Uploads/517/#This+is+not+a+complaint%2C+it%27s+my+thoughts+on+app%2enet%3A%0A%0AGiven+the+time%2C+I+could+create+my+own+app%2enet+with+very+little+money%2e+The+tools+are+there%2e+CDNs%2C+Rackspace%2C+cheap+hosting+providers+like+Tumblr%2e+%28Upon+which+Twitter%27s+blog+is+hosted%29%2e++If+it%27s+huge+elastic+databases+you%27re+after%3A+Amazon%27s+AWS+is+the+place+to+be%2e+This+practice+is+known+as+off-loading%2e+All+that+aside+-+I+admire+anyone+who+ventures+to+change+the+anti-pattern+that+exists+in+the+Social+Media+scene+-+the+pattern+of+making+the+user+the+product%2C+not+the+customer%2e+An+example+of+this%2C+is+Promoted+Tweets%2C+and+Facebook+injecting+crap+into+your+timeline+that+you%27re+reluctant+to+read%2C+and+have+to+use+Adblock+to+avoid%2e+I+also+admire+the+ethos+of+having+to+pay+for+app%2enet%2e+Just+like+licorize%2ecom+-+A+paid+service%3B+I+happily+pay+for+that+service%2C+knowing+they+won%27t+be+a+has-been%2C+or+die-out+due+to+a+lack+of+a+sensible+revenue+model%2e+Another+service+-+letscrate%2ecom%2C+is+a+purely+paid-for+service+I+happily+use%2C+in+the+safe+knowledge+it+won%27t+die+out+either%2e+Also+worth+noting+is+%40t%27s+Open+Webcamp+project+that+proposes+everyone+should+create+their+own%2C+independent+website%2C+and+syndicate+all+their+recent+posts+%2F+pages+to+the+various+Social+networks%2e+Please+correct+me+if+I%27m+wrong%2C+but+can+I+have+my+own+app%2enet%2C+on+my+own+server%2C+without+having+to+rely+on+a+CENTRALIZED+service%3F+And+also%2C+an+API+is+useless+if+app%2enet+goes+down%2C+and+I+can%27t+use+it%2e+The+optimist+person+would+reply+-+it%27s+100%25+uptime%2e+But+will+you+be+saying+100%25+uptime+when+there%27s+a+new+World+War%2C+or+a+Tsunami+hits+a+major+Continent%3F+Let+me+host+app%2enet+on+my+own+domain",
      "display_url" : "10k.aneventapart.com/2/Uploads/517/\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234374202369642496",
  "in_reply_to_user_id" : 9151842,
  "text" : "@daltonc My thoughts on http://t.co/1B6KI3ax : http://t.co/imNuHw1d!",
  "id" : 234374202369642496,
  "created_at" : "Sat Aug 11 19:42:18 +0000 2012",
  "in_reply_to_screen_name" : "daltonc",
  "in_reply_to_user_id_str" : "9151842",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Pointon",
      "screen_name" : "tompntn",
      "indices" : [ 0, 8 ],
      "id_str" : "496946720",
      "id" : 496946720
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "234368241714200576",
  "geo" : {
  },
  "id_str" : "234368798273191936",
  "in_reply_to_user_id" : 496946720,
  "text" : "@tompntn It's good. I can see why people get upset because the UI/UX is so daring, The \"Store\" is the main selling point. I now have Skitch!",
  "id" : 234368798273191936,
  "in_reply_to_status_id" : 234368241714200576,
  "created_at" : "Sat Aug 11 19:20:50 +0000 2012",
  "in_reply_to_screen_name" : "tompntn",
  "in_reply_to_user_id_str" : "496946720",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Robert Hernandez",
      "screen_name" : "webjournalist",
      "indices" : [ 3, 17 ],
      "id_str" : "20398450",
      "id" : 20398450
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GeekOut",
      "indices" : [ 50, 58 ]
    }, {
      "text" : "UNITY12",
      "indices" : [ 70, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 79, 99 ],
      "url" : "http://t.co/NFg4JUsZ",
      "expanded_url" : "http://sync.in/unity12",
      "display_url" : "sync.in/unity12"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234361879378677760",
  "text" : "RT @webjournalist: Mega list generated by today's #GeekOut session at #UNITY12 http://t.co/NFg4JUsZ and my tech &amp; tools page: http:/ ...",
  "retweeted_status" : {
    "source" : "<a href=\"http://www.echofon.com/\" rel=\"nofollow\">Echofon</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GeekOut",
        "indices" : [ 31, 39 ]
      }, {
        "text" : "UNITY12",
        "indices" : [ 51, 59 ]
      }, {
        "text" : "jtech",
        "indices" : [ 132, 138 ]
      } ],
      "urls" : [ {
        "indices" : [ 60, 80 ],
        "url" : "http://t.co/NFg4JUsZ",
        "expanded_url" : "http://sync.in/unity12",
        "display_url" : "sync.in/unity12"
      }, {
        "indices" : [ 111, 131 ],
        "url" : "http://t.co/YGOlithu",
        "expanded_url" : "http://bit.ly/techandtools",
        "display_url" : "bit.ly/techandtools"
      } ]
    },
    "geo" : {
    },
    "id_str" : "231874604874993664",
    "text" : "Mega list generated by today's #GeekOut session at #UNITY12 http://t.co/NFg4JUsZ and my tech &amp; tools page: http://t.co/YGOlithu #jtech",
    "id" : 231874604874993664,
    "created_at" : "Sat Aug 04 22:09:48 +0000 2012",
    "user" : {
      "name" : "Robert Hernandez",
      "screen_name" : "webjournalist",
      "protected" : false,
      "id_str" : "20398450",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3612637724/b24a578dd9adb752ed2e836b26a6476c_normal.jpeg",
      "id" : 20398450,
      "verified" : false
    }
  },
  "id" : 234361879378677760,
  "created_at" : "Sat Aug 11 18:53:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "occupy",
      "indices" : [ 106, 113 ]
    } ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http://t.co/E9YiVinc",
      "expanded_url" : "http://bit.ly/OQlyrx",
      "display_url" : "bit.ly/OQlyrx"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234361060264652800",
  "text" : "Occupy The Web \u2192 Hacking for the 99% http://t.co/E9YiVinc Yes, Occupy has never left teh stadium. And no, #occupy has not died. Prepare!",
  "id" : 234361060264652800,
  "created_at" : "Sat Aug 11 18:50:05 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "geekout",
      "indices" : [ 114, 122 ]
    }, {
      "text" : "geekmafia",
      "indices" : [ 123, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http://t.co/sXASkkzB",
      "expanded_url" : "http://isharefil.es/IfJQ/o",
      "display_url" : "isharefil.es/IfJQ/o"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234360106110816257",
  "text" : "Three Windows operating systems running all at once. (WIN7 + WIN8 + WINXP) http://t.co/sXASkkzB I have no life :( #geekout #geekmafia",
  "id" : 234360106110816257,
  "created_at" : "Sat Aug 11 18:46:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "indices" : [ 3, 14 ],
      "id_str" : "104462925",
      "id" : 104462925
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 85 ],
      "url" : "http://t.co/Sbe5u9Xo",
      "expanded_url" : "http://j.mp/MHyu7j",
      "display_url" : "j.mp/MHyu7j"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234357443126235137",
  "text" : "RT @techiejayk: HubSearch | An advanced search Engine for Github http://t.co/Sbe5u9Xo",
  "retweeted_status" : {
    "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 49, 69 ],
        "url" : "http://t.co/Sbe5u9Xo",
        "expanded_url" : "http://j.mp/MHyu7j",
        "display_url" : "j.mp/MHyu7j"
      } ]
    },
    "geo" : {
    },
    "id_str" : "234351022619906048",
    "text" : "HubSearch | An advanced search Engine for Github http://t.co/Sbe5u9Xo",
    "id" : 234351022619906048,
    "created_at" : "Sat Aug 11 18:10:12 +0000 2012",
    "user" : {
      "name" : "jay kanakiya",
      "screen_name" : "techiejayk",
      "protected" : false,
      "id_str" : "104462925",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3050293497/f330d8ba9496279d1ad37559cdf81a06_normal.jpeg",
      "id" : 104462925,
      "verified" : false
    }
  },
  "id" : 234357443126235137,
  "created_at" : "Sat Aug 11 18:35:43 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "234356381430448128",
  "geo" : {
  },
  "id_str" : "234357035834150912",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii For windows, try Windows Key + Tab. If nothing happens, you're a hipster bastard who has Windows Classic theme setup ;)",
  "id" : 234357035834150912,
  "in_reply_to_status_id" : 234356381430448128,
  "created_at" : "Sat Aug 11 18:34:06 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 136 ],
      "url" : "http://t.co/FvsJRdnf",
      "expanded_url" : "http://bit.ly/OQlo39",
      "display_url" : "bit.ly/OQlo39"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234356057739259904",
  "text" : "\"There's a movement growing across this country. Thousands of people are saying that they've had enough of Wall St\" http://t.co/FvsJRdnf",
  "id" : 234356057739259904,
  "created_at" : "Sat Aug 11 18:30:12 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "openweb",
      "indices" : [ 45, 53 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 44 ],
      "url" : "http://t.co/ZzWtLRSc",
      "expanded_url" : "http://bit.ly/MIDjNJ",
      "display_url" : "bit.ly/MIDjNJ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234351004903153664",
  "text" : "The Open Web Foundation http://t.co/ZzWtLRSc #openweb Heh. Get involved. If you aren't involved, you are teh lamez. True story.",
  "id" : 234351004903153664,
  "created_at" : "Sat Aug 11 18:10:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "toby sterrett",
      "screen_name" : "takeo",
      "indices" : [ 122, 128 ],
      "id_str" : "616673",
      "id" : 616673
    }, {
      "name" : "Phil Ripperger",
      "screen_name" : "pdsphil",
      "indices" : [ 131, 139 ],
      "id_str" : "641213",
      "id" : 641213
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 26 ],
      "url" : "http://t.co/EWVhUxNu",
      "expanded_url" : "http://bit.ly/OQkUdq",
      "display_url" : "bit.ly/OQkUdq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234345999877959680",
  "text" : "Cool: http://t.co/EWVhUxNu I just \u2665 LaterStars. So esoteric and hidden from the wider dev community, but so worth it. via @takeo + @pdsphil",
  "id" : 234345999877959680,
  "created_at" : "Sat Aug 11 17:50:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "oldskool",
      "indices" : [ 44, 53 ]
    }, {
      "text" : "html",
      "indices" : [ 54, 59 ]
    }, {
      "text" : "hacker",
      "indices" : [ 60, 67 ]
    } ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http://t.co/RRqYWLjG",
      "expanded_url" : "http://bit.ly/MIx3VZ",
      "display_url" : "bit.ly/MIx3VZ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234340979145052160",
  "text" : "Henri Sivonen's pages! http://t.co/RRqYWLjG #oldskool #html #hacker Always worth re-visiting after a long stretch of lazy frontend H@xxR'ing",
  "id" : 234340979145052160,
  "created_at" : "Sat Aug 11 17:30:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/NcmDD4ts",
      "expanded_url" : "http://bit.ly/OQi5ZS",
      "display_url" : "bit.ly/OQi5ZS"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234336336461570048",
  "text" : "branch++ \u2014 Dear Computer, \u2013 scripted aesthetics http://t.co/NcmDD4ts",
  "id" : 234336336461570048,
  "created_at" : "Sat Aug 11 17:11:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 38, 42 ]
    }, {
      "text" : "css3",
      "indices" : [ 43, 48 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/Sjl3zRcJ",
      "expanded_url" : "http://bit.ly/OQi1JL",
      "display_url" : "bit.ly/OQi1JL"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234330911527796736",
  "text" : "CSS Ribbons demo http://t.co/Sjl3zRcJ #css #css3",
  "id" : 234330911527796736,
  "created_at" : "Sat Aug 11 16:50:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tumblr",
      "indices" : [ 119, 126 ]
    } ],
    "urls" : [ {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/Lxb1E4CB",
      "expanded_url" : "http://bit.ly/OQhVl7",
      "display_url" : "bit.ly/OQhVl7"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234325865654259712",
  "text" : "Tumblr Infinite Scrolling Instructions http://t.co/Lxb1E4CB Every Tumblr skiddie has read this post at least once, LoL #tumblr",
  "id" : 234325865654259712,
  "created_at" : "Sat Aug 11 16:30:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 69, 73 ]
    }, {
      "text" : "hangout",
      "indices" : [ 74, 82 ]
    }, {
      "text" : "chat",
      "indices" : [ 83, 88 ]
    } ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http://t.co/TZcY5nDo",
      "expanded_url" : "http://bit.ly/OQhJCq",
      "display_url" : "bit.ly/OQhJCq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234320988840546304",
  "text" : "Oh! So I see you're quite teh conversationlist. http://t.co/TZcY5nDo #css #hangout #chat",
  "id" : 234320988840546304,
  "created_at" : "Sat Aug 11 16:10:51 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "polyfill",
      "indices" : [ 38, 47 ]
    }, {
      "text" : "github",
      "indices" : [ 48, 55 ]
    }, {
      "text" : "js",
      "indices" : [ 56, 59 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http://t.co/fGHdT96K",
      "expanded_url" : "http://bit.ly/OQhHux",
      "display_url" : "bit.ly/OQhHux"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234315847508836354",
  "text" : "Details Polyfill http://t.co/fGHdT96K #polyfill #github #js",
  "id" : 234315847508836354,
  "created_at" : "Sat Aug 11 15:50:26 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 131 ],
      "url" : "http://t.co/iIq5QJsr",
      "expanded_url" : "http://bit.ly/OQhFTq",
      "display_url" : "bit.ly/OQhFTq"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234310993273057280",
  "text" : "The Handpicked jQuery Plugins Repository. Supercharge your website! A handpicked collection of jQuery plugins. http://t.co/iIq5QJsr",
  "id" : 234310993273057280,
  "created_at" : "Sat Aug 11 15:31:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "doitright",
      "indices" : [ 97, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 76, 96 ],
      "url" : "http://t.co/cCmg1ZSt",
      "expanded_url" : "http://bit.ly/MIkOce",
      "display_url" : "bit.ly/MIkOce"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234305744562954244",
  "text" : "Why nobody cares about your blog. Still a classic read after all this time. http://t.co/cCmg1ZSt #doitright",
  "id" : 234305744562954244,
  "created_at" : "Sat Aug 11 15:10:17 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Walsh",
      "screen_name" : "davidwalshblog",
      "indices" : [ 59, 74 ],
      "id_str" : "15759583",
      "id" : 15759583
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css",
      "indices" : [ 75, 79 ]
    } ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http://t.co/sSgtrzek",
      "expanded_url" : "http://bit.ly/OQ6nhT",
      "display_url" : "bit.ly/OQ6nhT"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234300707342270464",
  "text" : "\"GitHub-Style Sliding Links Demo\" http://t.co/sSgtrzek via @davidwalshblog #css",
  "id" : 234300707342270464,
  "created_at" : "Sat Aug 11 14:50:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "webdevelopment",
      "indices" : [ 55, 70 ]
    }, {
      "text" : "freebies",
      "indices" : [ 71, 80 ]
    } ],
    "urls" : [ {
      "indices" : [ 16, 36 ],
      "url" : "http://t.co/5LR757Fz",
      "expanded_url" : "http://iwantaneff.in/shared/",
      "display_url" : "iwantaneff.in/shared/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234299306520240128",
  "in_reply_to_user_id" : 384742042,
  "text" : "@webdevelopblog http://t.co/5LR757Fz Take all you want #webdevelopment #freebies",
  "id" : 234299306520240128,
  "created_at" : "Sat Aug 11 14:44:42 +0000 2012",
  "in_reply_to_screen_name" : "fontanalorenz",
  "in_reply_to_user_id_str" : "384742042",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 121 ],
      "url" : "http://t.co/5LR757Fz",
      "expanded_url" : "http://iwantaneff.in/shared/",
      "display_url" : "iwantaneff.in/shared/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234298896929669120",
  "text" : "It's okay. The folder is there for sharing... This is what it's meant for... Stealing feels good ... http://t.co/5LR757Fz",
  "id" : 234298896929669120,
  "created_at" : "Sat Aug 11 14:43:04 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "blogging",
      "indices" : [ 85, 94 ]
    } ],
    "urls" : [ {
      "indices" : [ 62, 82 ],
      "url" : "http://t.co/3vyPAtYH",
      "expanded_url" : "http://bit.ly/OPTjJs",
      "display_url" : "bit.ly/OPTjJs"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234295730171420673",
  "text" : "Just because you can blog, it doesn't mean you should, right? http://t.co/3vyPAtYH \u273D #blogging",
  "id" : 234295730171420673,
  "created_at" : "Sat Aug 11 14:30:29 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tantek \u00C7elik",
      "screen_name" : "t",
      "indices" : [ 50, 52 ],
      "id_str" : "11628",
      "id" : 11628
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "indie",
      "indices" : [ 53, 59 ]
    }, {
      "text" : "webcamp",
      "indices" : [ 60, 68 ]
    } ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http://t.co/9LhMuc3C",
      "expanded_url" : "http://bit.ly/OPTi8l",
      "display_url" : "bit.ly/OPTi8l"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234290627968393216",
  "text" : "IndieWebCamp on Plancast http://t.co/9LhMuc3C via @t #indie #webcamp",
  "id" : 234290627968393216,
  "created_at" : "Sat Aug 11 14:10:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "apache",
      "indices" : [ 40, 47 ]
    }, {
      "text" : "indexof",
      "indices" : [ 90, 98 ]
    } ],
    "urls" : [ {
      "indices" : [ 19, 39 ],
      "url" : "http://t.co/UcvCREe8",
      "expanded_url" : "http://bit.ly/OPTg08",
      "display_url" : "bit.ly/OPTg08"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234285569860067329",
  "text" : "Index of /articles http://t.co/UcvCREe8 #apache Directory listings always yield yumminess #indexof",
  "id" : 234285569860067329,
  "created_at" : "Sat Aug 11 13:50:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    }, {
      "name" : "HYPERTEXTUALITY",
      "screen_name" : "mentioning",
      "indices" : [ 11, 22 ],
      "id_str" : "70323213",
      "id" : 70323213
    }, {
      "name" : "Tantek \u00C7elik",
      "screen_name" : "t",
      "indices" : [ 86, 88 ],
      "id_str" : "11628",
      "id" : 11628
    }, {
      "name" : "jacob",
      "screen_name" : "fat",
      "indices" : [ 93, 97 ],
      "id_str" : "16521996",
      "id" : 16521996
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234285485969772545",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys I love @mentioning you because you save me characters in the tweet. Same goes for @t and @fat",
  "id" : 234285485969772545,
  "created_at" : "Sat Aug 11 13:49:47 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "l33t",
      "indices" : [ 47, 52 ]
    }, {
      "text" : "b4d455",
      "indices" : [ 53, 60 ]
    } ],
    "urls" : [ {
      "indices" : [ 26, 46 ],
      "url" : "http://t.co/pvx0IFjH",
      "expanded_url" : "http://bit.ly/OPTc0q",
      "display_url" : "bit.ly/OPTc0q"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234280549479481344",
  "text" : "Ned Batchelder: Hex words http://t.co/pvx0IFjH #l33t #b4d455",
  "id" : 234280549479481344,
  "created_at" : "Sat Aug 11 13:30:10 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Heilmann ",
      "screen_name" : "codepo8",
      "indices" : [ 0, 8 ],
      "id_str" : "13567",
      "id" : 13567
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 30 ],
      "url" : "https://t.co/kjAsUQjE",
      "expanded_url" : "https://gist.github.com/2032253",
      "display_url" : "gist.github.com/2032253"
    }, {
      "indices" : [ 39, 59 ],
      "url" : "http://t.co/y8kHNzMj",
      "expanded_url" : "http://OMFGDOGS.com",
      "display_url" : "OMFGDOGS.com"
    } ]
  },
  "in_reply_to_status_id_str" : "233934466958114816",
  "geo" : {
  },
  "id_str" : "234277069943619584",
  "in_reply_to_user_id" : 13567,
  "text" : "@codepo8 https://t.co/kjAsUQjE Hijacks http://t.co/y8kHNzMj, and injects some Nyan Loveliness in there instead",
  "id" : 234277069943619584,
  "in_reply_to_status_id" : 233934466958114816,
  "created_at" : "Sat Aug 11 13:16:20 +0000 2012",
  "in_reply_to_screen_name" : "codepo8",
  "in_reply_to_user_id_str" : "13567",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Indam's Site",
      "screen_name" : "indaamcom",
      "indices" : [ 61, 71 ],
      "id_str" : "934650967",
      "id" : 934650967
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "css3",
      "indices" : [ 72, 77 ]
    }, {
      "text" : "shapes",
      "indices" : [ 78, 85 ]
    }, {
      "text" : "css",
      "indices" : [ 86, 90 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/5j5ifaW4",
      "expanded_url" : "http://bit.ly/L1VFBn",
      "display_url" : "bit.ly/L1VFBn"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234275573709549568",
  "text" : "Lots of CSS3 shapes you can achieve http://t.co/5j5ifaW4 via @indaamcom #css3 #shapes #css cool \u2714",
  "id" : 234275573709549568,
  "created_at" : "Sat Aug 11 13:10:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Goodfilms",
      "screen_name" : "goodfilmshq",
      "indices" : [ 87, 99 ],
      "id_str" : "252535878",
      "id" : 252535878
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "films",
      "indices" : [ 100, 106 ]
    }, {
      "text" : "movies",
      "indices" : [ 107, 114 ]
    } ],
    "urls" : [ {
      "indices" : [ 62, 82 ],
      "url" : "http://t.co/npxtNtRQ",
      "expanded_url" : "http://bit.ly/OPRQCU",
      "display_url" : "bit.ly/OPRQCU"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234270469610672128",
  "text" : "Goodfilms  - Rate, review, and share films with your friends. http://t.co/npxtNtRQ via @goodfilmshq #films #movies",
  "id" : 234270469610672128,
  "created_at" : "Sat Aug 11 12:50:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JonMagic",
      "screen_name" : "jonmagic",
      "indices" : [ 78, 87 ],
      "id_str" : "1182631",
      "id" : 1182631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http://t.co/YEeOacsL",
      "expanded_url" : "http://bit.ly/PmjjNN",
      "display_url" : "bit.ly/PmjjNN"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234265436701732865",
  "text" : "Scriptular  - a javascript regular expression editor http://t.co/YEeOacsL via @jonmagic",
  "id" : 234265436701732865,
  "created_at" : "Sat Aug 11 12:30:07 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stefan D\u00FChring",
      "screen_name" : "Autarc",
      "indices" : [ 0, 7 ],
      "id_str" : "52120068",
      "id" : 52120068
    }, {
      "name" : "\u3089\u308B\u304F\u3059",
      "screen_name" : "ralxz",
      "indices" : [ 8, 14 ],
      "id_str" : "1372192164",
      "id" : 1372192164
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 125 ],
      "url" : "http://t.co/4U61IgJ6",
      "expanded_url" : "http://isharefil.es/Igbb/o",
      "display_url" : "isharefil.es/Igbb/o"
    } ]
  },
  "in_reply_to_status_id_str" : "234259994420338689",
  "geo" : {
  },
  "id_str" : "234265246984990720",
  "in_reply_to_user_id" : 52120068,
  "text" : "@Autarc @ralxz Looks like I won't be getting any work done. This is Flash-only. Can I haz HTML5 Version? http://t.co/4U61IgJ6",
  "id" : 234265246984990720,
  "in_reply_to_status_id" : 234259994420338689,
  "created_at" : "Sat Aug 11 12:29:21 +0000 2012",
  "in_reply_to_screen_name" : "Autarc",
  "in_reply_to_user_id_str" : "52120068",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "indices" : [ 3, 14 ],
      "id_str" : "464887553",
      "id" : 464887553
    }, {
      "name" : "Pittsburgh Pirates",
      "screen_name" : "Pirates",
      "indices" : [ 71, 79 ],
      "id_str" : "37947138",
      "id" : 37947138
    }, {
      "name" : "\u25B2",
      "screen_name" : "hackers",
      "indices" : [ 80, 88 ],
      "id_str" : "15032423",
      "id" : 15032423
    }, {
      "name" : "Anonymous",
      "screen_name" : "YourAnonNews",
      "indices" : [ 89, 102 ],
      "id_str" : "279390084",
      "id" : 279390084
    }, {
      "name" : "Anonymous Press",
      "screen_name" : "AnonymousPress",
      "indices" : [ 103, 118 ],
      "id_str" : "230527949",
      "id" : 230527949
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hacktheplanet",
      "indices" : [ 119, 133 ]
    }, {
      "text" : "s",
      "indices" : [ 134, 136 ]
    } ],
    "urls" : [ {
      "indices" : [ 42, 63 ],
      "url" : "https://t.co/HfVu3QRn",
      "expanded_url" : "https://letscrate.com/f/img/files/proxies.zip",
      "display_url" : "letscrate.com/f/img/files/pr\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234253161785872384",
  "text" : "RT @EdanHewitt: Here's some fresh proxies https://t.co/HfVu3QRn \u2820\u2835 cc: @pirates @hackers @YourAnonNews @AnonymousPress #hacktheplanet #s ...",
  "retweeted_status" : {
    "source" : "web",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Pittsburgh Pirates",
        "screen_name" : "Pirates",
        "indices" : [ 55, 63 ],
        "id_str" : "37947138",
        "id" : 37947138
      }, {
        "name" : "\u25B2",
        "screen_name" : "hackers",
        "indices" : [ 64, 72 ],
        "id_str" : "15032423",
        "id" : 15032423
      }, {
        "name" : "Anonymous",
        "screen_name" : "YourAnonNews",
        "indices" : [ 73, 86 ],
        "id_str" : "279390084",
        "id" : 279390084
      }, {
        "name" : "Anonymous Press",
        "screen_name" : "AnonymousPress",
        "indices" : [ 87, 102 ],
        "id_str" : "230527949",
        "id" : 230527949
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "hacktheplanet",
        "indices" : [ 103, 117 ]
      }, {
        "text" : "spamtheplanet",
        "indices" : [ 118, 132 ]
      } ],
      "urls" : [ {
        "indices" : [ 26, 47 ],
        "url" : "https://t.co/HfVu3QRn",
        "expanded_url" : "https://letscrate.com/f/img/files/proxies.zip",
        "display_url" : "letscrate.com/f/img/files/pr\u2026"
      } ]
    },
    "geo" : {
    },
    "id_str" : "234250709871255553",
    "text" : "Here's some fresh proxies https://t.co/HfVu3QRn \u2820\u2835 cc: @pirates @hackers @YourAnonNews @AnonymousPress #hacktheplanet #spamtheplanet",
    "id" : 234250709871255553,
    "created_at" : "Sat Aug 11 11:31:36 +0000 2012",
    "user" : {
      "name" : "edan hewitt",
      "screen_name" : "EdanHewitt",
      "protected" : false,
      "id_str" : "464887553",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/3646301696/1956e43caf6c2d6981f2e222e4fbd6ba_normal.png",
      "id" : 464887553,
      "verified" : false
    }
  },
  "id" : 234253161785872384,
  "created_at" : "Sat Aug 11 11:41:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 20 ],
      "url" : "http://t.co/WhUEsaIr",
      "expanded_url" : "http://App.net",
      "display_url" : "App.net"
    }, {
      "indices" : [ 66, 86 ],
      "url" : "http://t.co/bh4RBfdi",
      "expanded_url" : "http://lnch.is/Mn4vjt",
      "display_url" : "lnch.is/Mn4vjt"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234227474958020609",
  "text" : "http://t.co/WhUEsaIr now 70% funded by 5K+ h\u0336a\u0336c\u0336k\u0336e\u0336r\u0336s\u0336 backers http://t.co/bh4RBfdi",
  "id" : 234227474958020609,
  "created_at" : "Sat Aug 11 09:59:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Victor \u2603",
      "screen_name" : "_victa",
      "indices" : [ 3, 10 ],
      "id_str" : "47319826",
      "id" : 47319826
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http://t.co/uv9QBo4b",
      "expanded_url" : "http://buildwindows.com",
      "display_url" : "buildwindows.com"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234227054705512448",
  "text" : "RT @_victa: http://t.co/uv9QBo4b",
  "retweeted_status" : {
    "source" : "<a href=\"http://itunes.apple.com/us/app/twitter/id409789998?mt=12\" rel=\"nofollow\">Twitter for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 0, 20 ],
        "url" : "http://t.co/uv9QBo4b",
        "expanded_url" : "http://buildwindows.com",
        "display_url" : "buildwindows.com"
      } ]
    },
    "geo" : {
    },
    "id_str" : "234211790622449665",
    "text" : "http://t.co/uv9QBo4b",
    "id" : 234211790622449665,
    "created_at" : "Sat Aug 11 08:56:56 +0000 2012",
    "user" : {
      "name" : "Victor \u2603",
      "screen_name" : "_victa",
      "protected" : false,
      "id_str" : "47319826",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/2538689839/6wmc0w7lgchqetttyxa6_normal.jpeg",
      "id" : 47319826,
      "verified" : false
    }
  },
  "id" : 234227054705512448,
  "created_at" : "Sat Aug 11 09:57:36 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Youssef",
      "screen_name" : "ys",
      "indices" : [ 0, 3 ],
      "id_str" : "19010677",
      "id" : 19010677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 4, 24 ],
      "url" : "http://t.co/Sjmt1n0v",
      "expanded_url" : "http://dontsave.com/+1_my_like_button/",
      "display_url" : "dontsave.com/+1_my_like_but\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "234059684229812224",
  "geo" : {
  },
  "id_str" : "234061751774232577",
  "in_reply_to_user_id" : 19010677,
  "text" : "@ys http://t.co/Sjmt1n0v",
  "id" : 234061751774232577,
  "in_reply_to_status_id" : 234059684229812224,
  "created_at" : "Fri Aug 10 23:00:44 +0000 2012",
  "in_reply_to_screen_name" : "ys",
  "in_reply_to_user_id_str" : "19010677",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://iwantaneff.in/chromeless/\" rel=\"nofollow\">We Want Chromeless Now</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mozilla UX",
      "screen_name" : "MozillaUX",
      "indices" : [ 12, 22 ],
      "id_str" : "570035416",
      "id" : 570035416
    }, {
      "name" : "Mozilla",
      "screen_name" : "mozilla",
      "indices" : [ 23, 31 ],
      "id_str" : "106682853",
      "id" : 106682853
    }, {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 90, 106 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "chromeless",
      "indices" : [ 73, 84 ]
    } ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http://t.co/WhyZW1dK",
      "expanded_url" : "http://iwantaneff.in/chromeless/",
      "display_url" : "iwantaneff.in/chromeless/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234052251491135488",
  "text" : "@paulrouget @MozillaUX @mozilla We Want Chromeless. http://t.co/WhyZW1dK #chromeless  cc: @jalbertbowdenii",
  "id" : 234052251491135488,
  "created_at" : "Fri Aug 10 22:22:59 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://iwantaneff.in/chromeless/\" rel=\"nofollow\">We Want Chromeless Now</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Opera",
      "screen_name" : "opera",
      "indices" : [ 0, 6 ],
      "id_str" : "1541461",
      "id" : 1541461
    }, {
      "name" : "Ruar\u00ED \u00D8degaard",
      "screen_name" : "ruari",
      "indices" : [ 7, 13 ],
      "id_str" : "14618686",
      "id" : 14618686
    }, {
      "name" : "Daniel Aleksandersen",
      "screen_name" : "Aeyoun",
      "indices" : [ 14, 21 ],
      "id_str" : "189313275",
      "id" : 189313275
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "chromeless",
      "indices" : [ 63, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http://t.co/WhyZW1dK",
      "expanded_url" : "http://iwantaneff.in/chromeless/",
      "display_url" : "iwantaneff.in/chromeless/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234052198324121600",
  "in_reply_to_user_id" : 1541461,
  "text" : "@opera @ruari @Aeyoun We Want Chromeless. http://t.co/WhyZW1dK #chromeless",
  "id" : 234052198324121600,
  "created_at" : "Fri Aug 10 22:22:47 +0000 2012",
  "in_reply_to_screen_name" : "opera",
  "in_reply_to_user_id_str" : "1541461",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://iwantaneff.in/chromeless/\" rel=\"nofollow\">We Want Chromeless Now</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WHATWG",
      "screen_name" : "WHATWG",
      "indices" : [ 0, 7 ],
      "id_str" : "3963051",
      "id" : 3963051
    }, {
      "name" : "W3C Team",
      "screen_name" : "w3c",
      "indices" : [ 8, 12 ],
      "id_str" : "35761106",
      "id" : 35761106
    }, {
      "name" : "html5",
      "screen_name" : "html5",
      "indices" : [ 13, 19 ],
      "id_str" : "14895831",
      "id" : 14895831
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "chromeless",
      "indices" : [ 61, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 40, 60 ],
      "url" : "http://t.co/WhyZW1dK",
      "expanded_url" : "http://iwantaneff.in/chromeless/",
      "display_url" : "iwantaneff.in/chromeless/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234052182348017664",
  "in_reply_to_user_id" : 3963051,
  "text" : "@whatwg @w3c @html5 We Want Chromeless. http://t.co/WhyZW1dK #chromeless",
  "id" : 234052182348017664,
  "created_at" : "Fri Aug 10 22:22:43 +0000 2012",
  "in_reply_to_screen_name" : "WHATWG",
  "in_reply_to_user_id_str" : "3963051",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://iwantaneff.in/chromeless/\" rel=\"nofollow\">We Want Chromeless Now</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 0, 12 ],
      "id_str" : "15736190",
      "id" : 15736190
    }, {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 13, 25 ],
      "id_str" : "793830",
      "id" : 793830
    }, {
      "name" : "EnvatoWebDev",
      "screen_name" : "envatowebdev",
      "indices" : [ 26, 39 ],
      "id_str" : "223770428",
      "id" : 223770428
    }, {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 98, 114 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "chromeless",
      "indices" : [ 81, 92 ]
    } ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http://t.co/WhyZW1dK",
      "expanded_url" : "http://iwantaneff.in/chromeless/",
      "display_url" : "iwantaneff.in/chromeless/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234052160017551360",
  "in_reply_to_user_id" : 15736190,
  "text" : "@smashingmag @chriscoyier @envatowebdev We Want Chromeless. http://t.co/WhyZW1dK #chromeless  cc: @jalbertbowdenii",
  "id" : 234052160017551360,
  "created_at" : "Fri Aug 10 22:22:38 +0000 2012",
  "in_reply_to_screen_name" : "smashingmag",
  "in_reply_to_user_id_str" : "15736190",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://iwantaneff.in/chromeless/\" rel=\"nofollow\">We Want Chromeless Now</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Mahemoff",
      "screen_name" : "mahemoff",
      "indices" : [ 0, 9 ],
      "id_str" : "1112431",
      "id" : 1112431
    }, {
      "name" : "Jonathan Beri",
      "screen_name" : "beriberikix",
      "indices" : [ 10, 22 ],
      "id_str" : "16871366",
      "id" : 16871366
    }, {
      "name" : "Ilya Grigorik",
      "screen_name" : "igrigorik",
      "indices" : [ 23, 33 ],
      "id_str" : "9980812",
      "id" : 9980812
    }, {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 91, 107 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "chromeless",
      "indices" : [ 75, 86 ]
    } ],
    "urls" : [ {
      "indices" : [ 54, 74 ],
      "url" : "http://t.co/WhyZW1dK",
      "expanded_url" : "http://iwantaneff.in/chromeless/",
      "display_url" : "iwantaneff.in/chromeless/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234052136571383809",
  "in_reply_to_user_id" : 1112431,
  "text" : "@mahemoff @beriberikix @igrigorik We Want Chromeless. http://t.co/WhyZW1dK #chromeless cc: @jalbertbowdenii",
  "id" : 234052136571383809,
  "created_at" : "Fri Aug 10 22:22:32 +0000 2012",
  "in_reply_to_screen_name" : "mahemoff",
  "in_reply_to_user_id_str" : "1112431",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 120, 140 ],
      "url" : "http://t.co/WhyZW1dK",
      "expanded_url" : "http://iwantaneff.in/chromeless/",
      "display_url" : "iwantaneff.in/chromeless/"
    } ]
  },
  "in_reply_to_status_id_str" : "234050010747125761",
  "geo" : {
  },
  "id_str" : "234051698988040192",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii Cool. Use the We Want Chromeless App for fun and profit. Thanks for teh subscription to my newsletter. http://t.co/WhyZW1dK",
  "id" : 234051698988040192,
  "in_reply_to_status_id" : 234050010747125761,
  "created_at" : "Fri Aug 10 22:20:48 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jalbertbowdenii",
      "screen_name" : "jalbertbowdenii",
      "indices" : [ 0, 16 ],
      "id_str" : "14465889",
      "id" : 14465889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 102 ],
      "url" : "http://t.co/5rrNbZGf",
      "expanded_url" : "http://www.flickr.com/photos/jalbertbowdenii/page3/",
      "display_url" : "flickr.com/photos/jalbert\u2026"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234049114264977408",
  "in_reply_to_user_id" : 14465889,
  "text" : "@jalbertbowdenii Was going to tweet pics to this, but they're old. Update or DIE. http://t.co/5rrNbZGf The mouse-mat couch is cool though.",
  "id" : 234049114264977408,
  "created_at" : "Fri Aug 10 22:10:31 +0000 2012",
  "in_reply_to_screen_name" : "jalbertbowdenii",
  "in_reply_to_user_id_str" : "14465889",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234041739206340608",
  "text" : "A classic tweet I done a while back: \u0714\u0722\u071C\u0714\u1DA0\u1DB8\u1D9C\u1D4F\u1D67\u2092\u1D64 \u04B3\u0338\u04B2\u0338\u04B3 My Signature, Fuck you Code-points, and Triple XXX. If you can't see it upgrade bitch",
  "id" : 234041739206340608,
  "created_at" : "Fri Aug 10 21:41:13 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : {
  },
  "id_str" : "234020121906122752",
  "text" : "Something annoys me about Tumblr. You post / reblog over 9000 posts that you like, and get like 2 unique visitors per day. WTF!",
  "id" : 234020121906122752,
  "created_at" : "Fri Aug 10 20:15:19 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 3, 15 ],
      "id_str" : "793830",
      "id" : 793830
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 91 ],
      "url" : "https://t.co/xfHsPSFU",
      "expanded_url" : "https://showoff.io/",
      "display_url" : "showoff.io"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234017088061206528",
  "text" : "RT @chriscoyier: \u201CThe easiest way to share localhost over the web.\u201D : https://t.co/xfHsPSFU",
  "retweeted_status" : {
    "source" : "<a href=\"http://tapbots.com/software/tweetbot/mac\" rel=\"nofollow\">Tweetbot for Mac</a>",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 74 ],
        "url" : "https://t.co/xfHsPSFU",
        "expanded_url" : "https://showoff.io/",
        "display_url" : "showoff.io"
      } ]
    },
    "geo" : {
    },
    "id_str" : "234015187781439488",
    "text" : "\u201CThe easiest way to share localhost over the web.\u201D : https://t.co/xfHsPSFU",
    "id" : 234015187781439488,
    "created_at" : "Fri Aug 10 19:55:43 +0000 2012",
    "user" : {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "protected" : false,
      "id_str" : "793830",
      "profile_image_url_https" : "https://si0.twimg.com/profile_images/1668225011/Gravatar2_normal.png",
      "id" : 793830,
      "verified" : false
    }
  },
  "id" : 234017088061206528,
  "created_at" : "Fri Aug 10 20:03:16 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cloudapp",
      "indices" : [ 70, 79 ]
    } ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http://t.co/PaWPsR0m",
      "expanded_url" : "http://bit.ly/MIdywQ",
      "display_url" : "bit.ly/MIdywQ"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234016929701056512",
  "text" : "Cloudapp Roulette. \"Surf\" the Cloudapp Universe: http://t.co/PaWPsR0m #cloudapp",
  "id" : 234016929701056512,
  "created_at" : "Fri Aug 10 20:02:38 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Bynens",
      "screen_name" : "mathias",
      "indices" : [ 0, 8 ],
      "id_str" : "532923",
      "id" : 532923
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "UTF8",
      "indices" : [ 124, 129 ]
    } ],
    "urls" : [ {
      "indices" : [ 101, 121 ],
      "url" : "http://t.co/ykbOQRUi",
      "expanded_url" : "http://r3versin.com/twitter/",
      "display_url" : "r3versin.com/twitter/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234013681338421249",
  "in_reply_to_user_id" : 532923,
  "text" : "@mathias Don't ask me how I preserved all them code-points. Rishida's conversion tool came in handy: http://t.co/ykbOQRUi \u2605 #UTF8 tweets FTW",
  "id" : 234013681338421249,
  "created_at" : "Fri Aug 10 19:49:44 +0000 2012",
  "in_reply_to_screen_name" : "mathias",
  "in_reply_to_user_id_str" : "532923",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 123 ],
      "url" : "http://t.co/ykbOQRUi",
      "expanded_url" : "http://r3versin.com/twitter/",
      "display_url" : "r3versin.com/twitter/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234012900329672704",
  "text" : "The joys of deleting all your Tweets, backing them up, and then uploading them to an independent host: http://t.co/ykbOQRUi \u273D",
  "id" : 234012900329672704,
  "created_at" : "Fri Aug 10 19:46:37 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "web",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 107 ],
      "url" : "http://t.co/YYqO51mW",
      "expanded_url" : "http://isharefil.es/IeSw/higgins.txt",
      "display_url" : "isharefil.es/IeSw/higgins.t\u2026"
    }, {
      "indices" : [ 118, 138 ],
      "url" : "http://t.co/ouEXHtav",
      "expanded_url" : "http://dchang.mit.edu/artwork/",
      "display_url" : "dchang.mit.edu/artwork/"
    } ]
  },
  "geo" : {
  },
  "id_str" : "234009321606881280",
  "text" : "I just used an Image to ASCII convertor powered by Filepicker.io Here's a photo of me: http://t.co/YYqO51mW The tool: http://t.co/ouEXHtav",
  "id" : 234009321606881280,
  "created_at" : "Fri Aug 10 19:32:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "frontend",
      "indices" : [ 119, 128 ]
    }, {
      "text" : "linkdump",
      "indices" : [ 129, 138 ]
    } ],
    "urls" : [ {
      "indices" : [ 15, 35 ],
      "url" : "http://t.co/WXoQbT0v",
      "expanded_url" : "http://bit.ly/OPRyM8",
      "display_url" : "bit.ly/OPRyM8"
    } ]
  },
  "geo" : {
  },
  "id_str" : "233998683077750785",
  "text" : "Frontend links http://t.co/WXoQbT0v Doing a bit of Web Development? Here's a few classic resources to get you started. #frontend #linkdump",
  "id" : 233998683077750785,
  "created_at" : "Fri Aug 10 18:50:08 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 68, 75 ]
    }, {
      "text" : "awesome",
      "indices" : [ 76, 84 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http://t.co/Rhl6wqEc",
      "expanded_url" : "http://bit.ly/OPRuwb",
      "display_url" : "bit.ly/OPRuwb"
    } ]
  },
  "geo" : {
  },
  "id_str" : "233993691654672384",
  "text" : "\"GFX - a 3D CSS3 animation library for jQuery\" http://t.co/Rhl6wqEc #jquery #awesome",
  "id" : 233993691654672384,
  "created_at" : "Fri Aug 10 18:30:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "html",
      "indices" : [ 51, 56 ]
    }, {
      "text" : "entities",
      "indices" : [ 57, 66 ]
    }, {
      "text" : "encode",
      "indices" : [ 67, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http://t.co/J4CqwwLU",
      "expanded_url" : "http://bit.ly/OPRk7N",
      "display_url" : "bit.ly/OPRk7N"
    } ]
  },
  "geo" : {
  },
  "id_str" : "233988661912432640",
  "text" : "Text to HTML Entities Encoder http://t.co/J4CqwwLU #html #entities #encode Nice tool",
  "id" : 233988661912432640,
  "created_at" : "Fri Aug 10 18:10:18 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "slidedeck",
      "indices" : [ 52, 62 ]
    }, {
      "text" : "javascript",
      "indices" : [ 63, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/9Lzm2lIc",
      "expanded_url" : "http://bit.ly/MHpOO6",
      "display_url" : "bit.ly/MHpOO6"
    } ]
  },
  "geo" : {
  },
  "id_str" : "233983564255989760",
  "text" : "List of JavaScript Slide-Decks http://t.co/9Lzm2lIc #slidedeck #javascript",
  "id" : 233983564255989760,
  "created_at" : "Fri Aug 10 17:50:03 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Web.AppStorm",
      "screen_name" : "webappstorm",
      "indices" : [ 66, 78 ],
      "id_str" : "58675227",
      "id" : 58675227
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tumblr",
      "indices" : [ 79, 86 ]
    }, {
      "text" : "tools",
      "indices" : [ 87, 93 ]
    } ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http://t.co/EKZgUS4H",
      "expanded_url" : "http://bit.ly/MHpy1N",
      "display_url" : "bit.ly/MHpy1N"
    } ]
  },
  "geo" : {
  },
  "id_str" : "233978620400525312",
  "text" : "15 Apps to Enhance the Tumblr Experience http://t.co/EKZgUS4H via @webappstorm #tumblr #tools",
  "id" : 233978620400525312,
  "created_at" : "Fri Aug 10 17:30:24 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Windle",
      "screen_name" : "soulwire",
      "indices" : [ 97, 106 ],
      "id_str" : "16389046",
      "id" : 16389046
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http://t.co/LQ31dszL",
      "expanded_url" : "http://bit.ly/MHp5fM",
      "display_url" : "bit.ly/MHp5fM"
    } ]
  },
  "geo" : {
  },
  "id_str" : "233973662280216576",
  "text" : "Soulwire \u00BB Experiments in Code http://t.co/LQ31dszL One of my favorite JS experiements Well done @soulwire",
  "id" : 233973662280216576,
  "created_at" : "Fri Aug 10 17:10:42 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jquery",
      "indices" : [ 57, 64 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 56 ],
      "url" : "http://t.co/8mZzuld5",
      "expanded_url" : "http://bit.ly/MHoYkD",
      "display_url" : "bit.ly/MHoYkD"
    } ]
  },
  "geo" : {
  },
  "id_str" : "233968511381282817",
  "text" : "jQuery Sauce Tasty jQuery Goodness! http://t.co/8mZzuld5 #jquery",
  "id" : 233968511381282817,
  "created_at" : "Fri Aug 10 16:50:14 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
}, {
  "source" : "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ascii",
      "indices" : [ 55, 61 ]
    }, {
      "text" : "art",
      "indices" : [ 62, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http://t.co/CRrZRoVg",
      "expanded_url" : "http://bit.ly/OPRYSP",
      "display_url" : "bit.ly/OPRYSP"
    } ]
  },
  "geo" : {
  },
  "id_str" : "233967276808892416",
  "text" : "Ascii Art Dictionary / Collection http://t.co/CRrZRoVg #ascii #art",
  "id" : 233967276808892416,
  "created_at" : "Fri Aug 10 16:45:20 +0000 2012",
  "user" : {
    "name" : "David H.",
    "screen_name" : "alphenic",
    "protected" : false,
    "id_str" : "468853739",
    "profile_image_url_https" : "https://si0.twimg.com/profile_images/378800000217540979/03e6cf018309ba516872be351a583b00_normal.jpeg",
    "id" : 468853739,
    "verified" : false
  }
} ]