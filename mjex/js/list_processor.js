function baddy(){
	}
/*
  Title:	List Processor
  Description:	This is a really cool little script that will parse the tree of a HTML list
                and convert it into a nice, easy to use nested object.  Call it on the UL or OL
                then use toString() to alert out the resultant object.

  Creator:      Dan Pupius <http://pupius.co.uk>
  Date:         2005-02-02
  Rights:       Copyright (c)2002-2005 Thirteenth Parallel
                You are free to use the scripts for non-commercial projects,
		as long as you leave these copyright statements intact
*/


//============================================================================================
// DOM NodeType Constants
//============================================================================================
var NODE_ELEMENT = 1;
var NODE_ATTRIBUTE = 2;
var NODE_TEXT = 3;
var NODE_CDATA_SECTION = 4;
var NODE_ENTITY_REFERENCE = 5;
var NODE_ENTITY = 6;
var NODE_PROCESSING_INSTRUCTION = 7;
var NODE_COMMENT = 8;
var NODE_DOCUMENT = 9;
var NODE_DOCUMENT_TYPE = 10;
var NODE_DOCUMENT_FRAGMENT = 11;
var NODE_NOTATION = 12;
//============================================================================================


//============================================================================================
// ListObject - Creates a JS object to represent a HTML list
//============================================================================================
var ListObject = function(node) {


	function ListItem() {
		this.caption = "";
		this.className = "";
		this.href = "";
		this.subList = null;
		this.click = null;
		this.mouseover = null;
		this.mouseout = null;

		return this;
	}

	this.toString = function(linebreak, indent) {
		if(!linebreak) linebreak = "\n";
		if(!indent) indent = "     ";
		var str = "";
		for(var i=0;i<this.items.length;i++) {
			str += indent + "caption:     " + this.items[i].caption + linebreak;
			str += indent + "className:     " + this.items[i].className + linebreak;
			str += indent + "href:        " + this.items[i].href + linebreak;
			str += indent + "click:       " + this.items[i].click + linebreak;
			str += indent + "mouseover:   " + this.items[i].mouseover + linebreak;
			str += indent + "mouseout:   " + this.items[i].mouseout + linebreak;
			if(this.items[i].subList!=null) str += indent + "children:" + linebreak + this.items[i].subList.toString(linebreak, indent + indent);
			str += linebreak;
		}

		return str;
	}

	this.root = node;
	this.items = new Array();


	for(var i=0;i<node.childNodes.length;i++) {
		var el = node.childNodes[i];
		if(typeof el.tagName == "string" && el.tagName.toUpperCase()=="LI") {
			var tmp = new ListItem();
			tmp.className = el.className;

			for(var j=0;j<el.childNodes.length;j++) {
				switch(el.childNodes[j].nodeType) {
					case NODE_TEXT:
						if(el.childNodes[j].nodeValue != "") tmp.caption += el.childNodes[j].nodeValue;
						break;
					case NODE_ELEMENT:
						if(el.childNodes[j].tagName.toUpperCase()=="UL" || el.childNodes[j].tagName.toUpperCase()=="OL") tmp.subList = new ListObject(el.childNodes[j]);
						else if(el.childNodes[j].tagName.toUpperCase()=="A") {
							tmp.caption += el.childNodes[j].firstChild.nodeValue;
							tmp.href = el.childNodes[j].href;

							if(typeof el.childNodes[j].onclick != "undefined") tmp.click = el.childNodes[j].onclick;
							else tmp.click = null;

							if(typeof el.childNodes[j].onmouseover != "undefined") tmp.mouseover = el.childNodes[j].onmouseover;
							else tmp.mouseover = null;

							if(typeof el.childNodes[j].onmouseout != "undefined") tmp.mouseout = el.childNodes[j].onmouseout;
							else tmp.mouseout = null;
						}
						break;
					default:
						baddy();
				}
			}
			if(tmp.caption || tmp.subList) this.items.push(tmp);
		}
	}

	return this;
}