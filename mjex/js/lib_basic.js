// dhtml library by rod.o2theory.com | soloplay@msn.com
// keep these two lines and you're free to use this code

// last updated: 07.04.2003

// lib namespace
var lib = { 
	// utilities
	px: function(n) { return (typeof n == 'number') ? n + 'px' : 0; },
	rand: function(x, y) { return (Math.round(Math.random() * (y - x)) + x); },
	
	// global reference to the dhtml object
	registry: new Array(),
	
	// object constructor
	makeObject: function(id)
	{
		this.el = document.getElementById ? document.getElementById(id) : null;
		if (!this.el) return alert(id + ' not found.');
		this.css = this.el.style;
		this.i = lib.registry.length; lib.registry[this.i] = this;
		this.w = this.el.offsetWidth ? this.el.offsetWidth : 0;
		this.h = this.el.offsetHeight ? this.el.offsetHeight : 0;
		this.x = this.el.offsetLeft ? this.el.offsetLeft : 0;
		this.y = this.el.offsetTop ? this.el.offsetTop : 0;
		this.o = 100;
	}
}

// lib.makeObject dhtml methods
lib.makeObject.prototype = {
	// update object values
	update: function()
	{
		this.w = this.el.offsetWidth ? this.el.offsetWidth : 0;
		this.h = this.el.offsetHeight ? this.el.offsetHeight : 0;
		this.x = this.el.offsetLeft ? this.el.offsetLeft : 0;
		this.y = this.el.offsetTop ? this.el.offsetTop : 0;
	},

	// moves a layer to (x, y) pixels
	moveTo: function(x, y)
	{
		if (x != null && typeof x == 'number') { this.x = x; this.css.left = x + 'px'; }
		if (y != null && typeof y == 'number') { this.y = y; this.css.top = y + 'px'; }
	},

	// moves a layer by (x, y) pixels
	moveBy: function(x, y) { this.moveTo(this.x + x, this.y + y); },

	// visibility
	show: function() { this.css.visibility = 'visible'; },
	hide: function() { this.css.visibility = 'hidden'; },

	// resize layer to (width, height) pixels
	setSize: function(w, h)
	{
		if (w != null && typeof w == 'number') { this.w = w; this.css.width = w + 'px'; }
		if (h != null && typeof h == 'number') { this.h = h; this.css.height = h + 'px'; }
	},

	// write content to layer
	write: function(text)
	{
		if (typeof this.el.innerHTML != 'undefined')
		{
			this.el.innerHTML = text;
			this.update();
		}
	},

	// set layer opacity (in percent, between 0 and 100)
	setOpacity: function(o)
	{
		if (typeof o == 'number')
		{
			if (typeof this.css.MozOpacity != 'undefined') this.css.MozOpacity = (o / 100);
			else if (this.el.filters) this.css.filter = 'alpha(opacity = ' + o + ')';
			this.o = o;
		}
	}
	
}
// end of lib.makeObject dhtml methods


// event object
var events = {
	init: function(e)
	{
		viewPort.get();
		e = window.event && !e ? window.event : e;
		this.mouseX = typeof e.clientX != 'undefined' ? e.clientX + viewPort.scrollx : 0;
		this.mouseY = typeof e.clientY != 'undefined' ? e.clientY + viewPort.scrolly : 0;
		this.layerX = typeof e.offsetX != 'undefined' ? e.offsetX : typeof e.layerX != 'undefined' ? e.layerX : 0;
		this.layerY = typeof e.offsetY != 'undefined' ? e.offsetY : typeof e.layerY != 'undefined' ? e.layerY : 0;
		this.type = e.type;
		this.target = e.srcElement ? e.srcElement : e.target ? e.target : null;
		if (this.target.nodeType == 3 || this.target.tagName.toLowerCase() == 'img') this.target = this.target.parentNode;
	},

	// prevents event default action
	preventDefault: function(e)
	{
		if (window.event) window.event.returnValue = false;
		else if (e.preventDefault) e.preventDefault();
	},

	// cancels event bubbling to parent elements
	cancelBubble: function(e)
	{
		if (window.event) window.event.cancelBubble = true;
		else if (e.stopPropagation) e.stopPropagation();
	}
}


// get width, height and scroll offsets
var viewPort = {
	get: function()
	{
		if (document.documentElement && document.documentElement.scrollLeft) this.scrollx = document.documentElement.scrollLeft;
		else if (document.body && document.body.scrollLeft) this.scrollx = document.body.scrollLeft;
		else if (window.scrollX) this.scrollx = window.scrollX;
		else if (window.pageXOffset) this.scrollx = window.pageXOffset;
		else this.scrollx = 0;

		if (document.documentElement && document.documentElement.scrollTop) this.scrolly = document.documentElement.scrollTop;
		else if (document.body && document.body.scrollTop) this.scrolly = document.body.scrollTop;
		else if (window.scrollY) this.scrolly = window.scrollY;
		else if (window.pageYOffset) this.scrolly = window.pageYOffset;
		else this.scrolly = 0;
		
		if (document.documentElement && document.documentElement.clientWidth) this.width = document.documentElement.clientWidth;
		else if (document.body && document.body.clientWidth) this.width = document.body.clientWidth;
		else if (window.innerWidth) this.width = window.innerWidth;
		else this.width = 0;

		if (document.documentElement && document.documentElement.clientHeight) this.height = document.documentElement.clientHeight;
		else if (document.body && document.body.clientHeight) this.height = document.body.clientHeight;
		else if (window.innerHeight) this.height = window.innerHeight;
		else this.height = 0;
		
		this.w = this.width + this.scrollx;
		this.h = this.height + this.scrolly;
	}
}


// event listener
var eventListener = {
	// attach event to an element
	add: function(obj, et, fn, capture)
	{
		if (obj.addEventListener) { obj.addEventListener(et, fn, capture); return true; }
		else if (obj.attachEvent) { var ae = obj.attachEvent('on' + et, fn); return ae; }
		// else { obj['on' + et] = fn; }
	},
	// detach event from an element
	remove: function(obj, et, fn, capture)
	{
		if (obj.removeEventListener) { obj.removeEventListener(et, fn, capture); return true; }
		else if (obj.detachEvent) { var re = obj.detachEvent('on' + et, fn); return re; }
		// else { obj['on' + et] = null; }
	}
}


// browser check (needed for hacks, bug fixes and special features in specific browsers)
function browserCheck()
{
	this.ua = navigator.userAgent.toLowerCase();
	this.dom = document.getElementById ? 1 : 0;
	this.op7 = (this.dom && this.ua.indexOf('opera 7') > -1 || this.ua.indexOf('opera/7') > -1) ? 1 : 0;
	this.ie5 = (this.dom && this.ua.indexOf('msie 5') > -1) ? 1 : 0;
	this.ie6 = (this.dom && this.ua.indexOf('msie 6') > -1) ? 1 : 0;
	this.moz = (this.dom && this.ua.indexOf('mozilla') > -1 && this.ua.indexOf('gecko') > -1) ? 1 : 0;
}

// browser check variable
var bw = new browserCheck();