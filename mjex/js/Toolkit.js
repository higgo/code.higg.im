/*
  Title:	Thirteenth Parallel Toolkit - Core - ALPHA Version

  Publisher:	Thirteenth Parallel <http://13thparallel.org>
  Date:         2005-02-02
  Rights:       Copyright (c)2002-2005 Thirteenth Parallel
                You are free to use the scripts for non-commercial projects,
		as long as you leave these copyright statements intact
*/


// HTML version
var Toolkit = {
	Sandbox : window,
	Canvas : document,
	version : 1.0,
	Log : {} // defined elsewhere
};

// end