/*************************************************************\
*                                                             *
*                       PUPIUS.CO.UK MENU                     *
*                                                             *
* THIS SCRIPT IS MESSY!!!  But it works, it breaks down the   *
* list and builds a nice dynamic menu for it.  Note if JS is  *
* is disabled CSS should handle it quite gracefully.          *
*                                                             *
* creator:     Daniel Pupius <http://pupius.co.uk/contact/>   *
* date:        April 2004  (release version, 02/02/2005)      *
* version:     1.2                                            *
* rights:      copyright(c)2004 Dan Pupius www.pupius.co.uk   *
*                                                             *
* You are free to use the scripts for non-commercial projects,*
* as long as you leave these copyright statements intact.      *
*                                                             *
\*************************************************************/

var MENU_TOP_OFFSET = 198	// this needs to be hard coded as I was being a bit lazy when I wrote the script.

var Site = {

	menuitems:	new Array(),
	tracer:		null,
	active:		0,
	selected:	-1,
	tracerAnim:	null,
	timer:		null,

	setup:		function() {
				if(!document.getElementsByTagName || !document.createElement || !document.appendChild) return false;

				//grab list, hide it and create a list object from it
				var navbar = document.getElementById("nav");
				var list = navbar.getElementsByTagName("ul")[0];
				list.style.display = "none";
				var menu = new ListObject(list);

				//create the tracer element which will show the current menu item
				this.tracer = document.createElement("div");
				this.tracer.className = "menutracer";
				navbar.appendChild(this.tracer);

				this.menuitems[-1] = null;

				//process first level menu items
				for(var i=0;i<menu.items.length;i++) {
					
					//create a new element for this item
					var item = document.createElement("div");
					item.className = "menuitem";
					item.index = i;
					item.href = menu.items[i].href;
					item.appendChild(document.createTextNode(menu.items[i].caption));
					if(menu.items[i].className=="selected") this.selected=i; 

					//append item to navbar
					navbar.appendChild(item);

					//create events
					Toolkit.Events.addListener(item,"onmouseover",function() { if(Site.timer) window.clearTimeout(Site.timer); Site.setActive(this.index); Site.show_menu(this.index); window.status = this.href; },item);
					Toolkit.Events.addListener(item,"onmouseout",function() {if(!this.menuholder) Site.setActive(Site.selected); else Site.timer = setTimeout("Site.setActive(Site.selected); Site.show_menu(-1)",750); window.status = ""; },item);
					Toolkit.Events.addListener(item,"onclick",function() { location.href = this.href; },item);

					//if this item has sub-items, render them now
					if(menu.items[i].subList) {
						var sub = document.createElement("div");
						sub.className = "menuback";
						sub.index = i;

						item.className = "menuitem x";
						item.menuholder = sub;
						
						Toolkit.Events.addListener(sub,"onmouseover",function() { if(Site.timer) window.clearTimeout(Site.timer); Site.setActive(this.index);  },sub);
						Toolkit.Events.addListener(sub,"onmouseout",function() { Site.timer = setTimeout("Site.setActive(Site.selected); Site.show_menu(-1)",750); },sub);
						

						//add the content to the menu (using a table)
						var table = document.createElement("table");
						var tb = document.createElement("tbody");
						var tr = document.createElement("tr");
						table.appendChild(tb);
						tb.appendChild(tr);
						sub.appendChild(table);
						document.getElementById("canvas").appendChild(sub);
						
						//create columns for each level (max of 5) ** ultra-messy bit **
						var level1 = document.createElement("td");
						level1.id = "td_" + i + "_1";level1.className="td1";
						tr.appendChild(level1);

						var level2 = document.createElement("td");
						level2.id = "td_" + i + "_2";level2.className="td2";
						tr.appendChild(level2);

						var level3 = document.createElement("td");
						level3.id = "td_" + i + "_3";level3.className="td3";
						tr.appendChild(level3);

						var level4= document.createElement("td");
						level4.id = "td_" + i + "_4";level4.className="td4";
						tr.appendChild(level4);

						var level5 = document.createElement("td");
						level5.id = "td_" + i + "_5";level5.className="td5";
						tr.appendChild(level5);



						//add level one items and recurse on other levels
						for(var k=0;k<menu.items[i].subList.items.length;k++) {
							var d = document.createElement("a");
							d.href = menu.items[i].subList.items[k].href;
							
							var caption = menu.items[i].subList.items[k].caption
							if(menu.items[i].subList.items[k].className=="selected") caption = "--> " + caption;

							d.appendChild(document.createTextNode(caption));
							level1.appendChild(d);
							
							if(menu.items[i].subList.items[k].subList) {
								d.className = "menulink sub";
								d.menu = i;
								d.level = 2;
								d.index = menu.items[i].subList.items[k].caption;
								Toolkit.Events.addListener(d,"onmouseover",function() { Site.show_sub_menu(this.menu,this.level,this.index); this.className = "menulink sub active";  },d);
					
								Site.recurseMenus(menu.items[i].subList.items[k].subList,i,2,menu.items[i].subList.items[k].caption);

							} else {
								d.className = "menulink";
								d.menu = i;
								d.level = 2;
								d.index = "";
								Toolkit.Events.addListener(d,"onmouseover",function() { Site.show_sub_menu(this.menu,this.level,this.index); },d);
							}
						}

					} else item.menuholder = null;

					//keep track of this item
					this.menuitems.push(item);
				}


				Toolkit.Events.addListener(document,"onclick",function() { if(Site.timer) window.clearTimeout(Site.timer); Site.setActive(Site.selected); Site.show_menu(-1); },item);


				//position primary elements
				var cursor = 0;
				for(i=0;i<this.menuitems.length;i++) { // (i already defined reuse it for this loop)
					this.menuitems[i].style.marginLeft = cursor + "px";
					cursor += this.menuitems[i].offsetWidth;

					if(this.menuitems[i].menuholder) {
						this.menuitems[i].menuholder.style.left = (navbar.offsetLeft + this.menuitems[i].offsetLeft - 1) + "px";
						this.menuitems[i].menuholder.style.top = MENU_TOP_OFFSET + "px";
					}
				}
				Site.tracer.style.marginLeft = "0px";  
				Site.tracer.style.width = "1px";       

				//set the active menu item;
				Site.setActive(Site.selected);
				Site.show_menu(-1);

				return true;
			},

	recurseMenus:	function(list,menu,level,parent) {
				for(var k=0;k<list.items.length;k++) {
					var d = document.createElement("a");
					d.href = list.items[k].href;
					d.parent = parent;
					
					var caption = list.items[k].caption;
					if(list.items[k].className=="selected") caption = "--> " + caption;

					d.appendChild(document.createTextNode(caption));
					document.getElementById("td_"+menu+"_"+level).appendChild(d);
					
					if(list.items[k].subList) {
						d.className = "menulink sub";
						Site.recurseMenus(list.items[k].subList,menu,level+1,list.items[k].caption);
						d.menu = menu;
						d.level = level+1;
						d.index = list.items[k].caption;
						Toolkit.Events.addListener(d,"onmouseover",function() { Site.show_sub_menu(this.menu,this.level,this.index); this.className = "menulink sub active"; },d);
					
					} else {
						d.className = "menulink";
						d.menu = menu;
						d.level = level+1;
						d.index = "";
						Toolkit.Events.addListener(d,"onmouseover",function() { Site.show_sub_menu(this.menu,this.level,this.index); },d);

					}
				}
			},

	show_menu:	function(n) {
				for(var i=0;i<Site.menuitems.length;i++) {
					if(i==n && Site.menuitems[i].menuholder) Site.menuitems[i].menuholder.style.visibility = "visible";
					else if(Site.menuitems[i].menuholder) Site.menuitems[i].menuholder.style.visibility = "hidden";
				}
				Site.show_sub_menu(n,2,"");
			},

	//note this is super messy as no references are used, this is to try
	//and help avoid memory leaks in IE
	show_sub_menu:	function(menu,level,index) {
				if(Site.menuitems[menu] && Site.menuitems[menu].menuholder) {
					if(index != "") {
						if(typeof document.all != "undefined" ) document.getElementById("td_"+menu+"_"+level).style.display = "inline";
						else document.getElementById("td_"+menu+"_"+level).style.display = "table-cell";
					
						for(var x=0;x<document.getElementById("td_"+menu+"_"+level).getElementsByTagName("a").length;x++) {
							if(document.getElementById("td_"+menu+"_"+level).getElementsByTagName("a")[x].parent==index) document.getElementById("td_"+menu+"_"+level).getElementsByTagName("a")[x].style.display = "block";
							else document.getElementById("td_"+menu+"_"+level).getElementsByTagName("a")[x].style.display = "none";
							if(document.getElementById("td_"+menu+"_"+level).getElementsByTagName("a")[x].className=="menulink sub active") {
								document.getElementById("td_"+menu+"_"+level).getElementsByTagName("a")[x].className="menulink sub";
							}
						}

					} else document.getElementById("td_"+menu+"_"+level).style.display = "none";
				}

				for(var j=level+1;j<=5;j++) if(document.getElementById("td_"+menu+"_"+j)) document.getElementById("td_"+menu+"_"+j).style.display = "none";

				if(document.getElementById("td_"+menu+"_"+(level-1))) {
					for(var y=0;y<document.getElementById("td_"+menu+"_"+(level-1)).getElementsByTagName("a").length;y++) {
						if(document.getElementById("td_"+menu+"_"+(level-1)).getElementsByTagName("a")[y].className=="menulink sub active") {
							document.getElementById("td_"+menu+"_"+(level-1)).getElementsByTagName("a")[y].className="menulink sub";
						}	
					}
				}
			},

	setActive:	function(n) {
				this.active = n;

				//if an anmation is already running, stop it
				if(Site.tracerAnim != null) Site.tracerAnim.stop();

				//calculate start position
				var startX = parseInt(Site.tracer.style.marginLeft);
				var startW = Site.tracer.offsetWidth;

				//calculate end position
				var endX = -2;
				var endW = 1;
				if(n!=-1) {
					endX = parseInt(Site.menuitems[n].style.marginLeft);
					endW = Site.menuitems[n].offsetWidth;
				}

				//create 2D line for the movement (not in xy space but xw :)
				var line = new Toolkit.Drawing.Curves.Line([startX,startW],[endX,endW]);

				//create animation object
				Site.tracerAnim = new Toolkit.Drawing.Animation(line,500,0.999);
				Toolkit.Events.addListener(Site.tracerAnim,"onanimate",Site.drawTracer);
				Toolkit.Events.addListener(Site.tracerAnim,"onend",Site.stopTracer);

				//start animation
				Site.tracerAnim.start();
			},

	drawTracer:	function(animobj) {
				var x = Math.round(animobj.coordinates[0]);
				var w =  Math.round(animobj.coordinates[1]);
				
				if(!isNaN(x)) Site.tracer.style.marginLeft = x + "px";
				if(!isNaN(w)) Site.tracer.style.width = w + "px";
			},

	stopTracer:	function(){
				Site.tracerAnim = null;
			}
}

Toolkit.Events.addListener(window,"onload",function() {Site.setup();});
//onload = function() { Site.setup(); }



function openPopup(url,w,h) {
	var x = (screen.width / 2) - (w / 2);
	var y = (screen.height / 2) - (2* h / 3);

	var NewWindow = window.open(url, "_blank", "status=0,scrollbars=1,resizable=0,toolbar=0,menubar=0,width="+w+",height="+h+",left="+x+",top="+y);

	if(NewWindow) return false;
	else return true;
}