// scroller2 by rod.o2theory.com | soloplay@msn.com
// keep these two lines and you're free to use this code

var scroller = {
	scrollables: 17, // set the number of scrollable scrollContent
	active: 1, // set the default page visible onload
	step: 30, // set the timeout between each scroll
	speed: 5, // set the scroll speed
	wheelSpeed: 15, // set the scroll speed when using the mouse wheel
	wheelScrolled: false,
	timer: 0,
	scrollContentY: 0,
	dragY: 0,
	clickY: 0,

	mouseDown: function(e)
	{
		events.init(e);
		scroller.wheelScrolled = false;
		if (events.target.id == scroller.bar.el.id)
		{
			scroller.bar.grab = true;
			scroller.clickY = events.mouseY - scroller.bar.y;
			events.preventDefault(e);
		}
		else if (events.target.id == scroller.up.el.id)
		{
			scroller.up.grab = true;
			scroller.scroll(scroller.active, scroller.speed);
			events.preventDefault(e);
		}
		else if (events.target.id == scroller.down.el.id)
		{
			scroller.down.grab = true;
			scroller.scroll(scroller.active, -scroller.speed);
			events.preventDefault(e);
		}
		else if (events.target.id == scroller.track.el.id)
		{
			scroller.track.grab = true;
			events.preventDefault(e);
		}
	},

	mouseUp: function(e)
	{
		scroller.bar.grab = false;
		scroller.track.grab = false;
		scroller.up.grab = false;
		scroller.down.grab = false;
		clearTimeout(scroller.timer);
	},

	mouseMove: function(e)
	{
		events.init(e);
		if (scroller.bar.grab && scroller.content[scroller.active].h > scroller.container.h)
		{
			scroller.dragY = events.mouseY - scroller.clickY;
			if (scroller.dragY < 0) scroller.dragY = 0;
			if (scroller.dragY > scroller.track.h - scroller.bar.h) scroller.dragY = scroller.track.h - scroller.bar.h;
			scroller.contentY = 0 - (scroller.dragY * (scroller.content[scroller.active].h - scroller.container.h) / Math.round(scroller.track.h - scroller.bar.h));
			scroller.content[scroller.active].moveTo(0, scroller.contentY);
			scroller.bar.moveTo(0, scroller.dragY);
			events.preventDefault(e);
		}
	},

	scroll: function(num, speed)
	{
		if (scroller.content[num].y < 0 || scroller.content[num].y > -scroller.content[num].h + scroller.container.h)
		{
			scroller.contentY = scroller.content[num].y + speed;
			if (scroller.contentY < -(scroller.content[num].h - scroller.container.h)) scroller.contentY = -scroller.content[num].h + scroller.container.h;
			else if (scroller.contentY > 0) scroller.contentY = 0;
			scroller.content[num].moveTo(0, scroller.contentY);
			scroller.dragY = 0 - (scroller.content[num].y * Math.round(scroller.track.h - scroller.bar.h) / (scroller.content[num].h - scroller.container.h));
			scroller.bar.moveTo(0, scroller.dragY);
			if (!scroller.wheelScrolled) scroller.timer = setTimeout('scroller.scroll(' + num + ', ' + speed + ')', scroller.step);
		}
	},

	wheelScroll: function(e)
	{
		events.init(e);
		var el = events.target;
		if (el.id.slice(0, el.id.length - 1) != 'scrollContent')
		{
			do el = el.parentNode;
			while (el.tagName.toLowerCase() != 'div' || el.id.slice(0, el.id.length - 1) != 'scrollContent')
		}
		var i = el.id.charAt(el.id.length - 1);
		if (window.event.wheelDelta <= -120)
		{
			scroller.wheelScrolled = true;
			scroller.scroll(i, -scroller.wheelSpeed);
		}
		else if (window.event.wheelDelta >= 120)
		{
			scroller.wheelScrolled = true;
			scroller.scroll(i, scroller.wheelSpeed);
		}
		events.preventDefault(e);
	},

	swapTo: function(num)
	{
		if (num != scroller.active)
		{
			scroller.content[scroller.active].hide();
			scroller.active = num;
			scroller.bar.moveTo(0, 0);
			scroller.content[scroller.active].moveTo(0, 0);
			scroller.content[scroller.active].show();
		}
	},
	
	init: function()
	{
		scroller.bar = new lib.makeObject('scrollBar');
		scroller.bar.grab = false;
		scroller.track = new lib.makeObject('scrollTrack');
		scroller.track.grab = false;
		scroller.up = new lib.makeObject('scrollUp');
		scroller.up.grab = false;
		scroller.down = new lib.makeObject('scrollDown');
		scroller.down.grab = false;
		scroller.container = new lib.makeObject('scrollContainer');
		scroller.content = new Array();
		for (var i = 1; i <= scroller.scrollables; i++)
		{
			scroller.content[i] = new lib.makeObject('scrollContent' + i);
			if (bw.ie6) eventListener.add(scroller.content[i].el, 'mousewheel', scroller.wheelScroll, false);
		}
		scroller.content[scroller.active].show();
		eventListener.add(document, 'mousemove', scroller.mouseMove, false);
		eventListener.add(document, 'mousedown', scroller.mouseDown, false);
		eventListener.add(document, 'mouseup', scroller.mouseUp, false);
	}
}
